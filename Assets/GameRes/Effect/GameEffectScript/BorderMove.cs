﻿using UnityEngine;
using System.Collections;

public class BorderMove : MonoBehaviour 
{
    public int speed = 20;
    public int borderWMin = -100;
    public int borderWMax = 100;
    public int borderHMin = -100;
    public int borderHMax = 100;


    private int step = 1;
    private float timeMoveX = 0;
    private float timeMoveY = 0;

	void Start ()
    {
        timeMoveX = borderWMin;
        timeMoveY = borderHMin;
        step = 1;
        Vector3 newPos = new Vector3(timeMoveX, timeMoveY, 0);
        transform.localPosition = newPos;
	}
	
	void Update () 
    {

        int mx = 0, my = 0;

        if (step == 1)
        {
            mx = speed;            
        }
        else if (step == 2)
        {
            my = speed;
        }
        else if (step == 3)
        {
            mx = -speed;
        }
        else if (step == 4)
        {
            my = -speed;
        }
        timeMoveX = timeMoveX + mx * Time.deltaTime;
        timeMoveY = timeMoveY + my * Time.deltaTime;
        if (step == 1)
        {
            if (timeMoveX >= borderWMax)
           {
               timeMoveX = borderWMax;
               step = 2;
            }
        }
        else if (step == 2)
        {
            if (timeMoveY >= borderHMax)
            {
                timeMoveY = borderHMax;
                step = 3;
            }
        }
        else if (step == 3)
        {
            if (timeMoveX <= borderWMin )
            {
                timeMoveX = borderWMin;
                step = 4;
            }
        }
        else if (step == 4)
        {
            if (timeMoveY <= borderHMin)
            {
                timeMoveY = borderHMin;
                step = 1;
            }
        }

        Vector3 newPos = new Vector3(timeMoveX, timeMoveY, 0);
        transform.localPosition = newPos;
	}

}