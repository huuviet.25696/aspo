﻿using UnityEngine;
using System.Collections;

public class RotateCircle : MonoBehaviour
{
	public float speed = 200;
    private Transform transform = null;


	void Start ()
    {
        transform = GetComponent<Transform>();
	}
	

	void Update () 
    {
        Vector3 pos = new Vector3(0, 0, speed * Time.deltaTime);
        transform.Rotate(pos);
	}
}
