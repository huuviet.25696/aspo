using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;


public class AudioImportSetting : AssetPostprocessor
{
	public void OnPreprocessAudio()
    {
        AudioImporter importer = (AudioImporter)AudioImporter.GetAtPath(assetPath);
        AudioImporterSampleSettings importerSetting = importer.defaultSampleSettings;
        importerSetting.compressionFormat = AudioCompressionFormat.Vorbis;
        importerSetting.loadType = AudioClipLoadType.CompressedInMemory;

        if (assetPath.Contains("Music"))
        {
            importerSetting.sampleRateSetting = AudioSampleRateSetting.OverrideSampleRate;
            importerSetting.sampleRateOverride = 44100;
        }
        else
        {
            importerSetting.sampleRateSetting = AudioSampleRateSetting.OverrideSampleRate;
            importerSetting.sampleRateOverride = 22050;
        }
        importer.defaultSampleSettings = importerSetting;
    }
}