using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;


public class TextureImportSetting : AssetPostprocessor
{
    public void OnPostprocessTexture(Texture2D texture)
    {
		if (assetPath.IndexOf("Assets/GameRes/Texture/TrueColor") >= 0)
		{
			TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
			importer.textureType = TextureImporterType.Default;
			importer.spriteImportMode = SpriteImportMode.None;
			importer.mipmapEnabled = false;
			importer.isReadable = false;
			importer.anisoLevel = -1;
			importer.filterMode = FilterMode.Bilinear;
			importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;
			return;
		}
        if (assetPath.IndexOf("Assets/GameRes/Model/Character/") >= 0)
        {
            //TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
            //importer.textureType = TextureImporterType.Default;
            //importer.spriteImportMode = SpriteImportMode.None;
            //importer.wrapMode = TextureWrapMode.Clamp;
            //importer.mipmapEnabled = false;
            //importer.isReadable = false;
            //importer.anisoLevel = -1;
            //importer.filterMode = FilterMode.Bilinear;
            //importer.textureFormat = TextureImporterFormat.AutomaticCompressed;
            return;
        }


        if (assetPath.IndexOf("Assets/GameRes/Effect/") >= 0)
        {
            if (assetPath.IndexOf("Assets/GameRes/Effect/commons/Textures/pingmu_401") >= 0)
            {
                TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
                importer.textureType = TextureImporterType.Default;
                importer.spriteImportMode = SpriteImportMode.None;
                importer.mipmapEnabled = false;
                importer.isReadable = false;
                importer.anisoLevel = -1;
                importer.filterMode = FilterMode.Bilinear;
                importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;       
            }
            else
            {
                TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
                importer.textureType = TextureImporterType.Default;
                importer.spriteImportMode = SpriteImportMode.None;
                importer.mipmapEnabled = false;
                importer.isReadable = false;
                importer.anisoLevel = -1;
                importer.filterMode = FilterMode.Bilinear;
                importer.textureFormat = TextureImporterFormat.AutomaticCompressed;
            }
        }

        if (assetPath.IndexOf("Assets/GameRes/Atlas/") >= 0)
        {
            TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
            importer.textureType = TextureImporterType.Default;
            importer.spriteImportMode = SpriteImportMode.None;
            importer.alphaIsTransparency = true;
            importer.wrapMode = TextureWrapMode.Repeat;
            importer.mipmapEnabled = false;
            importer.anisoLevel = -1;
            importer.filterMode = FilterMode.Bilinear;
        }

        //if (assetPath.IndexOf("Assets/GameRes/Spine/") >= 0)
        //{
        //    TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
        //    importer.textureType = TextureImporterType.Advanced;
        //    importer.spriteImportMode = SpriteImportMode.None;
        //    importer.alphaIsTransparency = true;
        //    importer.wrapMode = TextureWrapMode.Repeat;
        //    importer.mipmapEnabled = false;
        //    importer.anisoLevel = -1;
        //    importer.filterMode = FilterMode.Bilinear;
        //    importer.maxTextureSize = 512;
        //    importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;
        //}

        if (assetPath.IndexOf("Assets/GameRes/Map2d/") >= 0)
        {
            if (assetPath.IndexOf("minimap_") >= 0)
            {
                //TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
                //importer.textureType = TextureImporterType.Default;
                //importer.spriteImportMode = SpriteImportMode.None;
                //importer.alphaIsTransparency = true;
                //importer.wrapMode = TextureWrapMode.Clamp;
                //importer.mipmapEnabled = false;
                //importer.anisoLevel = -1;
                //importer.filterMode = FilterMode.Bilinear;
                //importer.textureFormat = TextureImporterFormat.RGB16;
            }
            else
            {
                //TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
                //importer.textureType = TextureImporterType.Default;
                //importer.spriteImportMode = SpriteImportMode.None;
                //importer.alphaIsTransparency = true;
                //importer.wrapMode = TextureWrapMode.Clamp;
                //importer.mipmapEnabled = false;
                //importer.anisoLevel = -1;
                //importer.filterMode = FilterMode.Bilinear;
                //importer.textureFormat = TextureImporterFormat.AutomaticCompressed;
            }
        }

        if (assetPath.IndexOf("Assets/GameRes/Map3d/") >= 0)
        {
            //TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
            //importer.textureType = TextureImporterType.Default;
            //importer.spriteImportMode = SpriteImportMode.None;
            //importer.alphaIsTransparency = true;
            //importer.wrapMode = TextureWrapMode.Repeat;
            //importer.mipmapEnabled = false;
            //importer.anisoLevel = -1;
            //importer.filterMode = FilterMode.Bilinear;
        }

        //if (assetPath.IndexOf("Assets/GameRes/Live2d") >= 0)
        //{
        //    TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
        //    importer.textureType = TextureImporterType.Advanced;
        //    importer.spriteImportMode = SpriteImportMode.None;
        //    importer.alphaIsTransparency = false;
        //    importer.wrapMode = TextureWrapMode.Repeat;
        //    importer.mipmapEnabled = false;
        //    importer.anisoLevel = -1;
        //    importer.filterMode = FilterMode.Bilinear;
        //    importer.textureFormat = TextureImporterFormat.AutomaticCompressed;
        //    //importer.SetPlatformTextureSettings("Android", 2048, TextureImporterFormat.ETC2_RGBA8);
        //}

        if (assetPath.IndexOf("Assets/GameRes/Spine") >= 0)
        {
            TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
            importer.textureType = TextureImporterType.Default;
            importer.spriteImportMode = SpriteImportMode.None;
            importer.alphaIsTransparency = false;
            importer.wrapMode = TextureWrapMode.Repeat;
            importer.mipmapEnabled = false;
            importer.anisoLevel = -1;
            importer.filterMode = FilterMode.Bilinear;
            importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;
            importer.SetPlatformTextureSettings("Android", 2048, TextureImporterFormat.ETC2_RGBA8);
            importer.SetPlatformTextureSettings("iPhone", 2048, TextureImporterFormat.ASTC_6x6);
        }

        if (assetPath.IndexOf("Assets/GameRes/Texture") >= 0)
        {
            if (assetPath.IndexOf("Assets/GameRes/Texture/Photo/full_") >= 0 ||
                (assetPath.IndexOf("Assets/GameRes/Texture/ChapterFuBen") >= 0))
            {
                TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
                importer.textureType = TextureImporterType.Default;
                importer.npotScale = TextureImporterNPOTScale.None;
                importer.spriteImportMode = SpriteImportMode.None;
                importer.alphaIsTransparency = true;
                importer.wrapMode = TextureWrapMode.Clamp;
                importer.mipmapEnabled = false;
                importer.isReadable = false;
                importer.anisoLevel = -1;
                importer.filterMode = FilterMode.Bilinear;
                importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;
                importer.ClearPlatformTextureSettings("Standalone");
                importer.ClearPlatformTextureSettings("Android");
                importer.ClearPlatformTextureSettings("iPhone");
            }
            else if ((assetPath.IndexOf("Assets/GameRes/Texture/Photo") >= 0) ||
                (assetPath.IndexOf("Assets/GameRes/Texture/Skin") >= 0))
            {
                TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
                importer.textureType = TextureImporterType.Default;
                importer.npotScale = TextureImporterNPOTScale.None;
                importer.spriteImportMode = SpriteImportMode.None;
                importer.alphaIsTransparency = true;
                importer.wrapMode = TextureWrapMode.Clamp;
                importer.mipmapEnabled = false;
                importer.isReadable = false;
                importer.anisoLevel = -1;
                importer.filterMode = FilterMode.Bilinear;
                importer.textureFormat = TextureImporterFormat.AutomaticTruecolor;
                importer.npotScale = TextureImporterNPOTScale.None;
                importer.SetPlatformTextureSettings("Android", 2048, TextureImporterFormat.ETC2_RGBA8);
                importer.SetPlatformTextureSettings("iPhone", 2048, TextureImporterFormat.ASTC_6x6);
            }
            else
            {
                TextureImporter importer = (TextureImporter)TextureImporter.GetAtPath(assetPath);
                importer.textureType = TextureImporterType.Default;
                importer.spriteImportMode = SpriteImportMode.None;
                importer.alphaIsTransparency = true;
                importer.wrapMode = TextureWrapMode.Clamp;
                importer.mipmapEnabled = false;
                importer.isReadable = false;
                importer.anisoLevel = -1;
                importer.filterMode = FilterMode.Bilinear;
                importer.textureFormat = TextureImporterFormat.AutomaticCompressed;
                importer.npotScale = TextureImporterNPOTScale.ToNearest;
            }
        }
    }
    
    public static void ConverMapTileToJPG(string filePath)
    {
        //string filePath = texConverInfo.filePath;
        byte[] pngBytes = File.ReadAllBytes(filePath);
        if (!JPGTexTool.CheckIsPNG(pngBytes))
            return;
        Texture2D pngTexture2D = new Texture2D(0, 0);
        pngTexture2D.LoadImage(pngBytes);

        string path = filePath.Replace(".png", ".bytes").Replace("tilemap_", "jpg_");
        string dir = Path.GetDirectoryName(path);
        if(!Directory.Exists(dir))
        {
            Directory.CreateDirectory(dir);
        }

        byte[] jpgBytes = pngTexture2D.EncodeToJPG(75);
        File.WriteAllBytes(path, jpgBytes);
        //string json = JsonMapper.ToJson(texConverInfo);
        //string jsonFileName = Path.GetFileName(jpgFilePath + ".json");
        //File.WriteAllText(Path.Combine(GetExportPath(), jsonFileName), json);
    }

    private static IEnumerable<string> GetSelectAssets<T>(string typeName) where T : UnityEngine.Object
    {
        string[] guids = Selection.assetGUIDs;
        List<string> assetsGuids = new List<string>();
        string filter = string.Format("t:{0}", typeName);
        foreach (string guid in guids)
        {
            string path = AssetDatabase.GUIDToAssetPath(guid);
            if (File.Exists(path))
            {
                T asset = AssetDatabase.LoadAssetAtPath<T>(path);
                if (asset != null)
                {
                    assetsGuids.Add(guid);
                }
            }
            else
            {
                string[] modelGuids = AssetDatabase.FindAssets(filter, new string[] { path });
                assetsGuids.AddRange(modelGuids);
            }
        }
        IEnumerable<string> modelGuidss = assetsGuids.Distinct();
        return modelGuidss;
    }

    private static IEnumerable<string> GetSelectTexture()
    {
        return GetSelectAssets<Texture>("Texture");
    }

    public static void StripSelectTextureAlpha()
    {
        var enumerable = GetSelectTexture().Select(item => AssetDatabase.GUIDToAssetPath(item));
        foreach (var path in enumerable)
        {
            Texture2D texture = new Texture2D(0, 0);
            texture.LoadImage(File.ReadAllBytes(path));
            if (texture.format == TextureFormat.RGB24)
                continue;

            Color32[] color32 = texture.GetPixels32();
            if (!color32.All(item => item.a == 255))
            {
                Texture2D newTexture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGB24, false);
                newTexture2D.SetPixels32(color32);
                File.WriteAllBytes(Path.ChangeExtension(path, ".png"), newTexture2D.EncodeToPNG());
                UnityEngine.Object.DestroyImmediate(newTexture2D);
            }
            UnityEngine.Object.DestroyImmediate(texture);
        }
        AssetDatabase.Refresh();
    }

    public static void SetTextureDiv2()
    {
        //string[] guids = Selection.assetGUIDs;
        //List<string> modelGuidList = new List<string>();

        //foreach (string guid in guids)
        //{
        //    string path = AssetDatabase.GUIDToAssetPath(guid);
        //    if (File.Exists(path))
        //    {
        //        Debug.Log(path);
        //    }
        //}

        string resPath = "Assets/GameRes/Texture/Photo";
        string[] files = Directory.GetFiles(resPath, "*.png", SearchOption.AllDirectories);

        foreach (string fname in files)
        {
            SetTextureDivBy2(fname);
            //Debug.Log(fname);
        }

        //var enumerable = GetSelectTexture().Select(item => AssetDatabase.GUIDToAssetPath(item));
        //foreach (var path in enumerable)
        //{
        //    SetTextureDivBy2(path);
        //    //Texture2D texture = new Texture2D(0, 0);
        //    //texture.LoadImage(File.ReadAllBytes(path));
        //    //if (texture.format == TextureFormat.RGB24)
        //    //    continue;

        //    //Color32[] color32 = texture.GetPixels32();
        //    //if (!color32.All(item => item.a == 255))
        //    //{
        //    //    Texture2D newTexture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGB24, false);
        //    //    newTexture2D.SetPixels32(color32);
        //    //    File.WriteAllBytes(Path.ChangeExtension(path, ".png"), newTexture2D.EncodeToPNG());
        //    //    UnityEngine.Object.DestroyImmediate(newTexture2D);
        //    //}
        //    //UnityEngine.Object.DestroyImmediate(texture);
        //}
        AssetDatabase.Refresh();
    }


    public static bool SetTextureDivBy2(string texPath)
    {
        string mainTexPath = string.Empty;
        Texture2D sourcetex = null;

        //string assetRelativePath = GetRelativeAssetPath(texPath);
        byte[] sourcebytes = File.ReadAllBytes(texPath);

        sourcetex = new Texture2D(0, 0);
        sourcetex.LoadImage(sourcebytes, false);
        Color[] colors2rdLevel = sourcetex.GetPixels();

        int width, rawWidth, height, rawHegiht;
        width = rawWidth = sourcetex.width;
        height = rawHegiht = sourcetex.height;

        if (rawWidth % 4 != 0)
        {
            width = rawWidth + rawWidth % 4;
        }
        if (rawHegiht % 4 != 0)
        {
            height = rawHegiht + rawHegiht % 4;
        }

 
        //width = height = Mathf.Max(rawWidth, rawHegiht);
        Texture2D rgbTex = new Texture2D(width, height, TextureFormat.RGBA32, false);

        Color c =  new Color(0,0,0,0);

        for (int w = 0; w < width; w++)
        {
            for(int h = 0; h < height; h++)
            {
                rgbTex.SetPixel(w, h, c);
            }
        }
        rgbTex.Apply();

        rgbTex.SetPixels(
            0,
            rawHegiht >= rawWidth ? 0 : height - rawHegiht,
            rawWidth,
            rawHegiht,
            sourcetex.GetPixels());

        byte[] bytes = rgbTex.EncodeToPNG();
        //mainTexPath = GetRGBTexPath(texPath);

        File.WriteAllBytes(texPath, bytes);

        return true;

    }

}

