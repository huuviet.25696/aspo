﻿using UnityEngine;
using System;
using System.Collections.Generic;
using LuaInterface;
using UnityEditor;

using BindType = ToLuaMenu.BindType;
using System.Reflection;

public static class CustomSettings
{
    public static string saveDir = Application.dataPath + "/Scripts/Generate/";    
    public static string toluaBaseType = Application.dataPath + "/Standard Assets/ToLua/BaseType/";
    public static string baseLuaDir = Application.dataPath + "/Standard Assets/ToLua/Lua/";
    public static string injectionFilesPath = Application.dataPath + "/Standard Assets/ToLua/Injection/";

    //导出时强制做为静态类的类型(注意customTypeList 还要添加这个类型才能导出)
    //unity 有些类作为sealed class, 其实完全等价于静态类
    public static List<Type> staticClassTypes = new List<Type>
    {
        //typeof(UnityEngine.AnchoredJoint2D),
        //typeof(UnityEngine.AnimationBlendMode),
        //typeof(UnityEngine.AnimationClip),
        //typeof(UnityEngine.AnimationClipPair),
        //typeof(UnityEngine.AnimationCurve),
        //typeof(UnityEngine.AnimationState),
        //typeof(UnityEngine.Animation),
        //typeof(UnityEngine.AnimatorOverrideController),
        //typeof(UnityEngine.Animator),
        //typeof(UnityEngine.Application),
        //typeof(UnityEngine.AssetBundle),
        //typeof(UnityEngine.AsyncOperation),
        //typeof(UnityEngine.AudioClip),
        //typeof(UnityEngine.AudioListener),
        //typeof(UnityEngine.AudioSource),
        //typeof(UnityEngine.Behaviour),
        ////typeof(UnityEngine.BlendWeights), replace with
        //typeof(UnityEngine.SkinWeights),
        //typeof(UnityEngine.BoxCollider),
        //typeof(UnityEngine.Camera),
        //typeof(UnityEngine.CameraClearFlags),
        //typeof(UnityEngine.CapsuleCollider),
        //typeof(UnityEngine.CharacterController),
        //typeof(UnityEngine.Collider),
        //typeof(UnityEngine.Component),
        //typeof(UnityEngine.CustomYieldInstruction),
        //typeof(UnityEngine.GameObject),
        //typeof(UnityEngine.GUIContent),
        //typeof(UnityEngine.GUILayout),
        //typeof(UnityEngine.GUILayoutOption),
        //typeof(UnityEngine.GUILayoutUtility),
        //typeof(UnityEngine.GUIStyle),
        //typeof(UnityEngine.HingeJoint2D),
        //typeof(UnityEngine.Input),
        //typeof(UnityEngine.Joint2D),
        //typeof(UnityEngine.KeyCode),
        //typeof(UnityEngine.Keyframe),
        //typeof(UnityEngine.Light),
        //typeof(UnityEngine.LightType),
        //typeof(UnityEngine.LineRenderer),
        //typeof(UnityEngine.Material),
        //typeof(UnityEngine.MeshCollider),
        //typeof(UnityEngine.MeshRenderer),
        //typeof(UnityEngine.MonoBehaviour),
        //typeof(UnityEngine.Motion),
        //typeof(UnityEngine.Networking.DownloadHandler),
        //typeof(UnityEngine.Networking.UnityWebRequest),
        //typeof(UnityEngine.Networking.UploadHandler),
        ////typeof(UnityEngine.NetworkPlayer),
        ////typeof(UnityEngine.Network),
        ////typeof(UnityEngine.ParticleAnimator),
        ////typeof(UnityEngine.ParticleEmitter),
        ////typeof(UnityEngine.ParticleRenderer),
        //typeof(UnityEngine.ParticleSystem),
        //typeof(UnityEngine.Physics),
        //typeof(UnityEngine.PlayerPrefs),
        //typeof(UnityEngine.PlayMode),
        //typeof(UnityEngine.QualitySettings),
        //typeof(UnityEngine.GL),
        //typeof(UnityEngine.QueueMode),
        //typeof(UnityEngine.Random),
        //typeof(UnityEngine.Rect),
        //typeof(UnityEngine.RectTransform),
        //typeof(UnityEngine.Renderer),
        //typeof(UnityEngine.RenderSettings),
        //typeof(UnityEngine.RenderTexture),
        //typeof(UnityEngine.Resources),
        //typeof(UnityEngine.Rigidbody),
        //typeof(UnityEngine.Rigidbody2D),
        //typeof(UnityEngine.RuntimeAnimatorController),
        //typeof(UnityEngine.SceneManagement.Scene),
        //typeof(UnityEngine.SceneManagement.SceneManager),
        //typeof(UnityEngine.Screen),
        //typeof(UnityEngine.ScriptableObject),
        //typeof(UnityEngine.Shader),
        //typeof(UnityEngine.SkinnedMeshRenderer),
        //typeof(UnityEngine.SleepTimeout),
        //typeof(UnityEngine.Space),
        //typeof(UnityEngine.SphereCollider),
        //typeof(UnityEngine.SystemInfo),
        //typeof(UnityEngine.Texture),
        //typeof(UnityEngine.Texture2D),
        //typeof(UnityEngine.Time),
        //typeof(UnityEngine.TrackedReference),
        //typeof(UnityEngine.TrailRenderer),
        //typeof(UnityEngine.Transform),
        //typeof(UnityEngine.WebCamTexture),
        //typeof(UnityEngine.WrapMode),
        //typeof(UnityEngine.WWW),
        //typeof(UnityEngine.WWWForm),




        typeof(UnityEngine.Application),
        typeof(UnityEngine.Time),
        typeof(UnityEngine.Screen),
        typeof(UnityEngine.SleepTimeout),
        typeof(UnityEngine.Input),
        typeof(UnityEngine.Resources),
        typeof(UnityEngine.Physics),
        typeof(UnityEngine.RenderSettings),
        typeof(UnityEngine.QualitySettings),
        typeof(UnityEngine.GL),
        typeof(UnityEngine.Graphics),
        typeof(UnityEngine.GUIContent),

    };

    //附加导出委托类型(在导出委托时, customTypeList 中牵扯的委托类型都会导出， 无需写在这里)
    public static DelegateType[] customDelegateList = 
    {        
        _DT(typeof(Action)),                
        _DT(typeof(UnityEngine.Events.UnityAction)),
        _DT(typeof(System.Predicate<int>)),
        _DT(typeof(System.Action<int>)),
        _DT(typeof(System.Comparison<int>)),
        _DT(typeof(System.Func<int, int>)),
    };

    //在这里添加你要导出注册到lua的类型列表
    public static BindType[] customTypeList =
    {                
        //------------------------为例子导出--------------------------------
        //_GT(typeof(TestEventListener)),
        //_GT(typeof(TestProtol)),
        //_GT(typeof(TestAccount)),
        //_GT(typeof(Dictionary<int, TestAccount>)).SetLibName("AccountMap"),
        //_GT(typeof(KeyValuePair<int, TestAccount>)),
        //_GT(typeof(Dictionary<int, TestAccount>.KeyCollection)),
        //_GT(typeof(Dictionary<int, TestAccount>.ValueCollection)),
        //_GT(typeof(TestExport)),
        //_GT(typeof(TestExport.Space)),
        //-------------------------------------------------------------------        
                        
        _GT(typeof(LuaInjectionStation)),
        _GT(typeof(InjectType)),
        _GT(typeof(Debugger)).SetNameSpace(null),          

#if USING_DOTWEENING
        _GT(typeof(DG.Tweening.DOTween)),
        _GT(typeof(DG.Tweening.Tween)).SetBaseType(typeof(System.Object)).AddExtendType(typeof(DG.Tweening.TweenExtensions)),
        _GT(typeof(DG.Tweening.Sequence)).AddExtendType(typeof(DG.Tweening.TweenSettingsExtensions)),
        _GT(typeof(DG.Tweening.Tweener)).AddExtendType(typeof(DG.Tweening.TweenSettingsExtensions)),
        _GT(typeof(DG.Tweening.LoopType)),
        _GT(typeof(DG.Tweening.PathMode)),
        _GT(typeof(DG.Tweening.PathType)),
        _GT(typeof(DG.Tweening.RotateMode)),
        _GT(typeof(Component)).AddExtendType(typeof(DG.Tweening.ShortcutExtensions)),
        _GT(typeof(Transform)).AddExtendType(typeof(DG.Tweening.ShortcutExtensions)),
        _GT(typeof(Light)).AddExtendType(typeof(DG.Tweening.ShortcutExtensions)),
        _GT(typeof(Material)).AddExtendType(typeof(DG.Tweening.ShortcutExtensions)),
        _GT(typeof(Rigidbody)).AddExtendType(typeof(DG.Tweening.ShortcutExtensions)),
        _GT(typeof(Camera)).AddExtendType(typeof(DG.Tweening.ShortcutExtensions)),
        _GT(typeof(AudioSource)).AddExtendType(typeof(DG.Tweening.ShortcutExtensions)),
        //_GT(typeof(LineRenderer)).AddExtendType(typeof(DG.Tweening.ShortcutExtensions)),
        //_GT(typeof(TrailRenderer)).AddExtendType(typeof(DG.Tweening.ShortcutExtensions)),    
#else
                                         
        _GT(typeof(Component)),
        _GT(typeof(Transform)),
        _GT(typeof(Material)),
        _GT(typeof(Light)),
        _GT(typeof(Rigidbody)),
        _GT(typeof(Rigidbody2D)),
        _GT(typeof(Camera)),
        _GT(typeof(AudioSource)),
        //_GT(typeof(LineRenderer))
        //_GT(typeof(TrailRenderer))
#endif






        _GT(typeof(Behaviour)),
        _GT(typeof(MonoBehaviour)),        
        _GT(typeof(GameObject)),
        _GT(typeof(TrackedReference)),
        _GT(typeof(Application)),
        _GT(typeof(Physics)),
        _GT(typeof(Collider)),
        _GT(typeof(Time)),        
        _GT(typeof(Texture)),
        _GT(typeof(Texture2D)),
        _GT(typeof(Shader)),        
        _GT(typeof(Renderer)),
        _GT(typeof(WWW)),
        _GT(typeof(Screen)),        
        _GT(typeof(CameraClearFlags)),
        _GT(typeof(AudioClip)),        
        _GT(typeof(AssetBundle)),
        _GT(typeof(ParticleSystem)),
        _GT(typeof(AsyncOperation)).SetBaseType(typeof(System.Object)),        
        _GT(typeof(LightType)),
        _GT(typeof(SleepTimeout)),
#if UNITY_5_3_OR_NEWER && !UNITY_5_6_OR_NEWER
        _GT(typeof(UnityEngine.Experimental.Director.DirectorPlayer)),
#endif
        _GT(typeof(Animator)),
        _GT(typeof(Input)),
        _GT(typeof(KeyCode)),
        _GT(typeof(SkinnedMeshRenderer)),
        _GT(typeof(Space)),      
       

        _GT(typeof(MeshRenderer)),
#if !UNITY_5_4_OR_NEWER
        _GT(typeof(ParticleEmitter)),
        _GT(typeof(ParticleRenderer)),
        _GT(typeof(ParticleAnimator)), 
#endif

        _GT(typeof(BoxCollider)),
        _GT(typeof(MeshCollider)),
        _GT(typeof(SphereCollider)),        
        _GT(typeof(CharacterController)),
        _GT(typeof(CapsuleCollider)),
        
        _GT(typeof(Animation)),        
        _GT(typeof(AnimationClip)).SetBaseType(typeof(UnityEngine.Object)),        
        _GT(typeof(AnimationState)),
        _GT(typeof(AnimationBlendMode)),
        _GT(typeof(QueueMode)),  
        _GT(typeof(PlayMode)),
        _GT(typeof(WrapMode)),

        _GT(typeof(QualitySettings)),
        _GT(typeof(RenderSettings)),                                                   
        _GT(typeof(SkinWeights)),           
        _GT(typeof(RenderTexture)),
        _GT(typeof(Resources)),     
        _GT(typeof(LuaProfiler)),


        //NGUI
        _GT(typeof(UI2DSprite)),
        _GT(typeof(UIAtlas)),
        _GT(typeof(UIBasicSprite)),
        _GT(typeof(UIButtonColor)),
        _GT(typeof(UIButtonScale)),
        _GT(typeof(UIButton)),
        _GT(typeof(UICamera)),
        _GT(typeof(UICenterOnChild)),

        _GT(typeof(UIFont)),
        _GT(typeof(UIGrid)),
        _GT(typeof(UIInput)),
        _GT(typeof(UILabel)),
        _GT(typeof(UIPanel)),
        _GT(typeof(UIPopupList)),
        _GT(typeof(UIProgressBar)),
        _GT(typeof(UIRect)),
        _GT(typeof(UIAnchor)),
        _GT(typeof(UIRoot)),
        _GT(typeof(UIScrollView)),
        _GT(typeof(UISlider)),
        _GT(typeof(UIScrollBar)),

        //_GT(typeof(UISpriteAnimation)),
        _GT(typeof(UISpriteData)),
        _GT(typeof(UITable)),
        _GT(typeof(UITexture)),

        _GT(typeof(UIToggle)),
        _GT(typeof(UITweener)),
        _GT(typeof(UIWidget)),

        _GT(typeof(UIDragScrollView)),
        _GT(typeof(UISprite)),
        _GT(typeof(UISpriteAnimation)),

        //Game
        _GT(typeof(AnimEffectInfo)),
        _GT(typeof(AnimEffect)),

        //_GT(typeof(AttachCameraHandler)),
        _GT(typeof(BindGuideObject)),
        _GT(typeof(BorderMove)),

        _GT(typeof(CameraPathAnimator)),
        _GT(typeof(CameraPathControlPoint)),
        _GT(typeof(CameraPathOrientationList)),
        _GT(typeof(CameraPathOrientation)),
        _GT(typeof(CameraPathPointList)),
        _GT(typeof(CameraPathPoint)),
        _GT(typeof(CameraPath)),

        _GT(typeof(CGPlayer)),

        _GT(typeof(ChainEffect)),
        _GT(typeof(DataContainer)),
        //_GT(typeof(Debugger)),
        _GT(typeof(FrameSyncHandler)),
        _GT(typeof(GameObjectContainer)),
        _GT(typeof(GraySceneHandler)),
        _GT(typeof(Hud)),

        _GT(typeof(live2d.ALive2DModel)),
        _GT(typeof(live2d.AMotion)),
        _GT(typeof(live2d.framework.L2DPhysics)),
        _GT(typeof(live2d.Live2DModelUnity)),
        _GT(typeof(live2d.Live2DMotion)),
        _GT(typeof(live2d.MotionQueueManager)),
        _GT(typeof(Live2dHandler)),


        //_GT(typeof(LuaInterface.Debugger)),
        //_GT(typeof(MD5Hashing)),

        _GT(typeof(NGUIMath)),
        //_GT(typeof(NGUITools)),

        //_GT(typeof(PhotoReaderManager)),
        _GT(typeof(PlatformAPI)),
        _GT(typeof(RenderObjectHandler)),

        _GT(typeof(SpineHandler)),
        _GT(typeof(SpringPanel)),

        _GT(typeof(System.Diagnostics.Stopwatch)),
        //_GT(typeof(System.IO.Directory)),
        //_GT(typeof(System.IO.File)),
        _GT(typeof(System.IO.Path)),

        _GT(typeof(TweenAlpha)),
        _GT(typeof(TweenFill)),
        _GT(typeof(TweenHeight)),
        _GT(typeof(TweenPosition)),
        _GT(typeof(TweenRotation)),
        _GT(typeof(TweenScale)),
        _GT(typeof(TweenWidth)),
        //_GT(typeof(UITweener)),

        _GT(typeof(WalkerEventHandler)),
        //_GT(typeof(WebCamTextureHelper)),

        /////////////////////////////
        /////////////////////////////
        _GT(typeof(AssetDatabase)),
        _GT(typeof(AnimationUtility)),
        _GT(typeof(AnimationClipCurveData)),
        _GT(typeof(EditorGUILayout)),
        _GT(typeof( EditorWindow)),
        _GT(typeof(EditorUtility)),
        _GT(typeof(PrefabUtility)),
        /////////////////////////////
        /////////////////////////////
        _GT(typeof(AnchoredJoint2D)),
        //_GT(typeof(AnimationBlendMode)),
        //_GT(typeof(AnimationClip)),
        _GT(typeof(AnimationClipPair)),
        _GT(typeof(AnimationCurve)),
        //_GT(typeof(AnimationState)),
        //_GT(typeof(Animation)),
        _GT(typeof(AnimatorOverrideController)),
        //_GT(typeof(Animator)),
        //_GT(typeof(Application)),
        //_GT(typeof(AssetBundle)),
        //_GT(typeof(AsyncOperation)),
        //_GT(typeof(AudioClip)),
        _GT(typeof(AudioListener)),
        //_GT(typeof(AudioSource)),
        //_GT(typeof(Behaviour)),
        //_GT(typeof(BlendWeights)), replace with
        //_GT(typeof(SkinWeights)),
        //_GT(typeof(BoxCollider)),
        //_GT(typeof(Camera)),
        //_GT(typeof(CameraClearFlags)),
        //_GT(typeof(CapsuleCollider)),
        //_GT(typeof(CharacterController)),
        //_GT(typeof(Collider)),
        //_GT(typeof(Component)),
        _GT(typeof(CustomYieldInstruction)),
        //_GT(typeof(GameObject)),
        _GT(typeof(GUIContent)),
        _GT(typeof(GUILayout)),
        _GT(typeof(GUILayoutOption)),
        _GT(typeof(GUILayoutUtility)),
        _GT(typeof(GUIStyle)),
        _GT(typeof(HingeJoint2D)),
        //_GT(typeof(Input)),
        _GT(typeof(Joint2D)),
        //_GT(typeof(KeyCode)),
        _GT(typeof(Keyframe)),
        //_GT(typeof(Light)),
        //_GT(typeof(LightType)),
        _GT(typeof(LineRenderer)),
        //_GT(typeof(Material)),
        //_GT(typeof(MeshCollider)),
        //_GT(typeof(MeshRenderer)),
        //_GT(typeof(MonoBehaviour)),
        _GT(typeof(Motion)),
        _GT(typeof(UnityEngine.Networking.DownloadHandler)),
        _GT(typeof(UnityEngine.Networking.UnityWebRequest)),
        _GT(typeof(UnityEngine.Networking.UploadHandler)),
        //_GT(typeof(NetworkPlayer)),
        //_GT(typeof(Network)),
        //_GT(typeof(ParticleAnimator)),
        //_GT(typeof(ParticleEmitter)),
        //_GT(typeof(ParticleRenderer)),
        //_GT(typeof(ParticleSystem)),
        //_GT(typeof(Physics)),
        _GT(typeof(PlayerPrefs)),
        //_GT(typeof(PlayMode)),
        //_GT(typeof(QualitySettings)),
        _GT(typeof(GL)),
        //_GT(typeof(QueueMode)),
        _GT(typeof(UnityEngine.Random)),
        _GT(typeof(Rect)),
        _GT(typeof(RectTransform)),
        //_GT(typeof(Renderer)),
        //_GT(typeof(RenderSettings)),
        //_GT(typeof(RenderTexture)),
        //_GT(typeof(Resources)),
        //_GT(typeof(Rigidbody)),
        //_GT(typeof(Rigidbody2D)),
        _GT(typeof(RuntimeAnimatorController)),
        _GT(typeof(UnityEngine.SceneManagement.Scene)),
        _GT(typeof(UnityEngine.SceneManagement.SceneManager)),
        //_GT(typeof(Screen)),
        _GT(typeof(ScriptableObject)),
        //_GT(typeof(Shader)),
        //_GT(typeof(SkinnedMeshRenderer)),
        //_GT(typeof(SleepTimeout)),
        //_GT(typeof(Space)),
        //_GT(typeof(SphereCollider)),
        _GT(typeof(SystemInfo)),
        //_GT(typeof(Texture)),
        //_GT(typeof(Texture2D)),
        //_GT(typeof(Time)),
        //_GT(typeof(TrackedReference)),
        _GT(typeof(TrailRenderer)),
        //_GT(typeof(Transform)),
        _GT(typeof(WebCamTexture)),
        //_GT(typeof(WrapMode)),
        //_GT(typeof(WWW)),
        _GT(typeof(WWWForm)),
        //_GT(typeof(AudioSource)),
        //_GT(typeof(Behaviour)),
        //_GT(typeof(SkinWeights)),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        //_GT(typeof()),
        

    };

    public static List<Type> dynamicList = new List<Type>()
    {
        typeof(MeshRenderer),
#if !UNITY_5_4_OR_NEWER
        typeof(ParticleEmitter),
        typeof(ParticleRenderer),
        typeof(ParticleAnimator),
#endif

        typeof(BoxCollider),
        typeof(MeshCollider),
        typeof(SphereCollider),
        typeof(CharacterController),
        typeof(CapsuleCollider),

        typeof(Animation),
        typeof(AnimationClip),
        typeof(AnimationState),

        typeof(SkinWeights),
        typeof(RenderTexture),
        typeof(Rigidbody),
    };

    //重载函数，相同参数个数，相同位置out参数匹配出问题时, 需要强制匹配解决
    //使用方法参见例子14
    public static List<Type> outList = new List<Type>()
    {
        
    };
        
    //ngui优化，下面的类没有派生类，可以作为sealed class
    public static List<Type> sealedList = new List<Type>()
    {
        /*typeof(Transform),
        typeof(UIRoot),
        typeof(UICamera),
        typeof(UIViewport),
        typeof(UIPanel),
        typeof(UILabel),
        typeof(UIAnchor),
        typeof(UIAtlas),
        typeof(UIFont),
        typeof(UITexture),
        typeof(UISprite),
        typeof(UIGrid),
        typeof(UITable),
        typeof(UIWrapGrid),
        typeof(UIInput),
        typeof(UIScrollView),
        typeof(UIEventListener),
        typeof(UIScrollBar),
        typeof(UICenterOnChild),
        typeof(UIScrollView),        
        typeof(UIButton),
        typeof(UITextList),
        typeof(UIPlayTween),
        typeof(UIDragScrollView),
        typeof(UISpriteAnimation),
        typeof(UIWrapContent),
        typeof(TweenWidth),
        typeof(TweenAlpha),
        typeof(TweenColor),
        typeof(TweenRotation),
        typeof(TweenPosition),
        typeof(TweenScale),
        typeof(TweenHeight),
        typeof(TypewriterEffect),
        typeof(UIToggle),
        typeof(Localization),*/
    };

    public static BindType _GT(Type t)
    {
        return new BindType(t);
    }

    public static DelegateType _DT(Type t)
    {
        return new DelegateType(t);
    }    


    [MenuItem("Lua/Attach Profiler", false, 151)]
    static void AttachProfiler()
    {
        if (!Application.isPlaying)
        {
            EditorUtility.DisplayDialog("Warning", "Please execute this function at runtime", "OK");
            return;
        }

        LuaClient.Instance.AttachProfiler();
    }

    [MenuItem("Lua/Detach Profiler", false, 152)]
    static void DetachProfiler()
    {
        if (!Application.isPlaying)
        {            
            return;
        }

        LuaClient.Instance.DetachProfiler();
    }
}
