using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using System.IO;
using AssetPipeline;

public class EditorToolsMenu
{
	[MenuItem("Tools/Open AssetDataPath", false, 1)]
    static void OpenAssetDataPath()
    {
        System.Diagnostics.Process.Start (Application.dataPath);
    }

	[MenuItem("Tools/LuaWrap Tools", false, 51)]
    static void ExportLuaWrap()
    {
        ExportWrapWindow.Init();
    }

	[MenuItem("Tools/Proto tools", false, 52)]
    public static void OpenProtoTools()
    {
        ProtoToolsWindow.OpenProtoTools();
    }

	[MenuItem("Tools/Animator tools", false, 53)]
    public static void OpenAnimatorTools()
    {
        AnimatorToolsWindow.OpenAnimatorTools();
    }

	[MenuItem("Tools/UI tools", false, 54)]
	public static void OpenUITools()
	{
		UIToolsWindow.OpenUITools();
	}

	[MenuItem("Tools/Test Lua editing interface", false, 54)]
	public static void OpenLuaEditor()
	{
		LuaEditorWindow.OpenLuaEditor("CTestEditorWindow");
	}

    //[MenuItem("工具/导出Lua接口/导出CustomSetting中的LuaWrap", false, 52)]
    //public static void ExportCustomSettingLuaWrap()
    //{
    //    ToLuaMenu.GenerateClassWraps();
    //}

	[MenuItem("Packaging/Lua loading mode/DisableLuaZipMode", false, 11)]
    public static void DisableLuaZipMode()
    {
        EditorPrefs.SetBool("LuaZipMode", false);
    }

	[MenuItem("Packaging/Lua loading mode/DisableLuaZipModeState", true, 11)]
    public static bool DisableLuaZipModeState()
    {
        return EditorPrefs.GetBool("LuaZipMode", false);
    }

	[MenuItem("Packaging/Lua loading mode/EnableLuaZipMode", false, 12)]
    public static void EnableLuaZipMode()
    {
        EditorPrefs.SetBool("LuaZipMode", true);
    }

	[MenuItem("Packaging/Lua loading mode/EnableLuaZipModeState", true, 12)]
    public static bool EnableLuaZipModeState()
    {
        return !EditorPrefs.GetBool("LuaZipMode", false);
    }


	[MenuItem("Packaging/Res loading mode/EnableResLocalMode", false, 21)]
    public static void EnableResLocalMode()
    {
        EditorPrefs.SetInt("ResLoadMode", (int)AssetManager.LoadMode.EditorLocal);
    }

	[MenuItem("Packaging/Res loading mode/CheckResLocalMode", true, 21)]
    public static bool CheckResLocalMode()
    {
        return EditorPrefs.GetInt("ResLoadMode", 0) != (int)AssetManager.LoadMode.EditorLocal;
    }

	[MenuItem("Packaging/Res loading mode/EnableAssetbundle", false, 22)]
    public static void EnableAssetbundle()
    {
        EditorPrefs.SetInt("ResLoadMode", (int)AssetManager.LoadMode.Assetbundle);
    }

	[MenuItem("Packaging/Res loading mode/CheckAssetbundle", true, 22)]
    public static bool CheckAssetbundle()
    {
        return EditorPrefs.GetInt("ResLoadMode", 0) != (int)AssetManager.LoadMode.Assetbundle;
    }

	[MenuItem("Packaging/Lua packaging tool", false, 51)]
    public static void OpenLuaTools()
    {
        LuaToolsWindow.OpenLuaTools();
    }

	[MenuItem("Packaging/AssetBundle packaging tool", false, 52)]
    public static void ShowAssetBundleBuilder()
    {
        AssetPipeline.AssetBundleBuilder.ShowWindow();
    }

	[MenuItem("Packaging/Release packaging tools", false, 52)]
    public static void ShowPlayerSettingTool()
    {
        PlayerSettingTool.ShowWindow();
    }
#if UNITY_STANDALONE_WIN
	[MenuItem("Packaging/One-click BuildWin", false, 101)]
    public static void BuildWinTest()
    {
        AssetBundleBuilder.BuildWinTest();
    }
#endif

	[MenuItem("Check tool/Check GameObject Reference Lost")]
    public static void FindMissScripts()
    {
        FindMissingComponent.FindMissScripts();
    }

	[MenuItem("Check tool/Check StandardShader material")]
    public static void CheckStandardShader()
    {
        FindStandardShader.CheckStandardShader();
    }

	[MenuItem("Check tool/check non-ref atlas reference")]
    public static void FindNotRefAtlas()
    {
        UIAtlasCheckMenu.FindNotRefAtlas();
    }

	[MenuItem("Check tool/replace selected UILabel Text style")]
    public static void LabelStyleUpdate()
    {
        UILabelStyleMenu.LabelStyleUpdate();
    }

	[MenuItem("Check tool/check for missing UIEventHandler")]
    public static void EventHandlerCheck()
    {
        UIEventHandlerCheck.Run();
    }

	[MenuItem("Check tool/remove picture Alpha channel")]
    private static void StripSelectTextureAlpha()
    {
        TextureImportSetting.StripSelectTextureAlpha();
    }

	[MenuItem("Check tool/set picture size to even number")]
    private static void SetTextureDiv2()
    {
        TextureImportSetting.SetTextureDiv2();
    }

	[MenuItem("Check tool/add rigid bodies to all panels")]
    private static void AllPanelAddRigidbody()
    {
        UIPanelCheckRigidbody.PanelAddRigidbody();
    }

	[MenuItem("Check tool/adjust FontStyle of UILabel")]
    private static void AllLabelClearBold()
    {
        UILabelTools.LabelToolsWindow();
    }

	[MenuItem("Check tool/model inspection tool")]
    private static void ModelCheck()
    {
        ModelCheckTools.ModelCheckToolsWindow();
    }

    //[MenuItem("检查工具/减少动作文件精度")]
    //private static void ReduceAnimationClip()
    //{
    //    AnimatorTools.ReduceAnimationClip();
    //}
}

