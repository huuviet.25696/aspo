using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Text;
using UITools;
using LuaInterface;

namespace UITools
{
	public class SerialData 
	{
		public Dictionary<string, RefInfo> sprRefDict = new Dictionary<string, RefInfo>();
		public Dictionary<string, RefInfo> texRefDict = new Dictionary<string, RefInfo>();
		public List<string> sprPrefixList = new List<string>();
		public List<string> unusedSpriteList = new List<string>();
		public List<string> unusedTextureList = new List<string>();
		public List<string> selImgList = new List<string>();
		public List<string> ingoreStrList = new List<string>();
		public List<string> moveCommonList = new List<string>();
		public SerialData(){}
	}
	public class RefInfo
	{
		public RefInfo(){}
		public Dictionary<string, int> refDict = new Dictionary<string, int>();
		public RefInfo(string path)
		{
			refDict.Add(path, 1);
		}
		public void AddRef(string path)
		{
			int cnt;
			if (refDict.TryGetValue(path, out cnt))
			{
				refDict.Remove(path);
				cnt++;
				refDict.Add(path, cnt);
			}
			else
			{
				refDict.Add(path, 1);
			}
		}
		public bool IsNeedMoveToCommon()
		{
			var i = 0;
			foreach (var path in refDict.Keys) 
			{
				if ((path.IndexOf("_Editor") == 0) && (path.IndexOf("_Common") == 0))
				{
					return false;
				}
				else
				{
					i++;
				}
			}
			return (i>1);
		}
	}

	public static class UIPath
	{
		public static string texturePath = Application.dataPath + "/GameRes/Texture/";
		public static string staticAtlasPath = Application.dataPath + "/GameRes/Atlas/StaticAtlas/";
		public static string dynamicAtlasPath = Application.dataPath + "/GameRes/Atlas/DynamicAtlas/";
		public static bool IsViewPrefab(this string path)
		{
			return (path.IndexOf("View") > 0 && path.IndexOf("prefab") > 0);
		}
		public static bool IsHudPrefab(this string path)
		{
			return (path.IndexOf("Hud") > 0 && path.IndexOf("prefab") > 0); ;
		}
		public static bool IsStaticAtlasName(this string atlas)
		{
			return atlas.EndsWith("Atlas");
		}
		public static string Key2AtlasType(this string key)
		{
			string atlasname = key.Split(new string[1] { "->" }, StringSplitOptions.None)[0];
			return atlasname.IsStaticAtlasName() ? "StaticAtlas" : "DynamicAtlas";
		}
		public static string AtlasNameType(this string atlas)
		{
			return atlas.IsStaticAtlasName() ? "StaticAtlas" : "DynamicAtlas";
		}
	}
}

public class UIToolsWindow : EditorWindow
{
	Dictionary<string, RefInfo> sprRefDict = new Dictionary<string, RefInfo>();
	Dictionary<string, RefInfo> texRefDict = new Dictionary<string, RefInfo>();
	List<string> sprPrefixList = new List<string>();
	List<string> unusedSpriteList = new List<string>();
	List<string> unusedTextureList = new List<string>();
	List<string> selImgList = new List<string>();
	List<string> displayImgList = new List<string>();
	List<string> selTexList = new List<string>();
	List<string> displayTexList = new List<string>();
	List<string> ingoreStrList = new List<string>();
	List<string> moveCommonList = new List<string>();
	static bool _quickSet = true;
	static UIAtlas _atlas;
	static bool hasCheckAll = false;
	static Vector2 _scrollPos;
	static Vector2 _scrollPos2;
	static Vector2 _scrollPos3;
	static Vector2 _scrollPos4;
	static string _searchFilter= "";
	static string _ingnoreInput = "";
	static string _oldSpriteName = "";
	static UIAtlas _newAtlas;
	static string _newSpriteName = "";
	Dictionary<Action, long> delayActionDict = new Dictionary<Action, long> ();
	public static void OpenUITools()
	{
		UIToolsWindow window = (UIToolsWindow)EditorWindow.GetWindow(typeof(UIToolsWindow));
		window.minSize = new Vector2(640f, 480f);
		window.Show();
		window.LoadJson();
	}
	void Update()
	{
		List<Action> delList = new List<Action>();
		var now = DateTime.Now.Ticks / 10000000;
		foreach (var kvp in delayActionDict)
		{
			var action = kvp.Key;
			var callTime = kvp.Value;
			if (now >= callTime)
			{
				action();
				delList.Add(action);
			}
		}
		foreach (var action in delList)
		{
			delayActionDict.Remove(action);
		}
	}
	static public UnityEngine.Object[] GetAllAtlas()
	{
		Func<string, List<UnityEngine.Object>, List<UnityEngine.Object>> getAtlasList = (string path,  List<UnityEngine.Object> list) =>
		{
			var files = Directory.GetFiles(path, "*.prefab", SearchOption.AllDirectories);
			foreach (var filepath in files)
			{
				var fileName = Path.GetFileNameWithoutExtension(filepath);
				if (fileName.Contains("Atlas"))
				{
					var asset = GetAtlas(fileName);
					list.Add(asset as UnityEngine.Object);
				}
			}
			return list;
		};

		List<UnityEngine.Object> objList = new List<UnityEngine.Object>();
		objList = getAtlasList(UITools.UIPath.staticAtlasPath, objList);
		objList = getAtlasList(UITools.UIPath.dynamicAtlasPath, objList);
		return objList.ToArray();
	}
	void OnGUI()
	{
		EditorGUILayout.BeginHorizontal();
		//left
		EditorGUILayout.BeginVertical(GUILayout.MaxWidth(320f));
		{

			//ComponentSelector.Draw<UIAtlas>("Atlas", _atlas, OnSelectAtlas, GetAllAtlas, GUILayout.MinWidth(80f));
			ComponentSelector.Draw<UIAtlas>("Atlas", _atlas, OnSelectAtlas, GetAllAtlas, GUILayout.MinWidth(80f));

			if (GUILayout.Button("重打图集", "LargeButton", GUILayout.Width(120f)))
			{
				AtlasRebuild(_atlas.name);
			}

			EditorGUILayout.BeginHorizontal();
			GUILayout.Label("图片改名:", GUILayout.Width(100f));
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			GUILayout.Label("旧spriteName:", GUILayout.Width(100f));
			_oldSpriteName = EditorGUILayout.TextField("", _oldSpriteName);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			GUILayout.Label("新Atlas:", GUILayout.Width(100f));
			ComponentSelector.Draw<UIAtlas>("Atlas", _newAtlas, OnSelectNewAtlas, GetAllAtlas, GUILayout.MinWidth(120f));
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.BeginHorizontal();
			GUILayout.Label("新spriteName:", GUILayout.Width(100f));
			_newSpriteName = EditorGUILayout.TextField("", _newSpriteName);
			EditorGUILayout.EndHorizontal();


			EditorGUILayout.BeginHorizontal();
			if (GUILayout.Button("指定单个修改", GUILayout.Width(88f)))
			{
				if (_atlas != null && _newAtlas !=null) MoveSprite(_atlas.name, _oldSpriteName, _newAtlas.name, _newSpriteName);
			}

			if (GUILayout.Button("匹配所有修改", GUILayout.Width(88f)))
			{
				if (_atlas != null && _newAtlas != null) MoveMatchSprite(_atlas.name, _oldSpriteName, _newAtlas.name, _newSpriteName);
			}

			if (GUILayout.Button("查找引用", GUILayout.Width(88f)))
			{
				if (_atlas != null && _oldSpriteName != null) FindRefSprite(_atlas.name, _oldSpriteName);
			}
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			_quickSet = GUILayout.Toggle(_quickSet, "quickSet");
			EditorGUILayout.EndHorizontal();
			
			GUILayout.Space(48f);

			GUILayout.Label("图片整理:");
			
			
			if (GUILayout.Button("检查所有prefab", "LargeButton", GUILayout.Width(200f)))
			{
				CheckUnUse();
				SaveJson();
				hasCheckAll = true;
			}

			DrawIgnoreList();
			GUILayout.Space(20f);

			DrawMoveCommonList();

		}
		EditorGUILayout.EndVertical();
		
		EditorGUILayout.Space();
		
		//center
		EditorGUILayout.BeginVertical();
		{
			DrawSprUnuseList();
		}

		EditorGUILayout.EndVertical();
		//right
		EditorGUILayout.BeginVertical();
		{
			DrawTextureUnuseList();
		}
		EditorGUILayout.EndVertical();
		

		EditorGUILayout.EndHorizontal();
	}
	public void DrawIgnoreList()
	{
		GUILayout.Label("忽略列表:");
		GUILayout.BeginHorizontal();
		_ingnoreInput = EditorGUILayout.TextField("", _ingnoreInput);
		if (GUILayout.Button("添加", GUILayout.Width(48f)))
		{
			ingoreStrList.Add(_ingnoreInput);
			SaveJson();
		}
		GUILayout.EndHorizontal();
		_scrollPos2 = EditorGUILayout.BeginScrollView(_scrollPos2, GUILayout.MinHeight(88f));
		List<string> delList = new List<string>();
		for (int i = 0; i < ingoreStrList.Count; i++)
		{
			var s = ingoreStrList[i];
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("x", GUILayout.Width(22f)))
			{
				delList.Add(s);
			}
			GUILayout.Label(s, "BoldLabel", GUILayout.Height(20f));
			GUILayout.EndHorizontal();
		}

		foreach (var s in delList)
		{
			ingoreStrList.Remove(s);
		}
		EditorGUILayout.EndScrollView();
	}
	public void DrawSprUnuseList()
	{
		if (unusedSpriteList.Count > 0)
		{
			GUILayout.BeginHorizontal();
			{
				if (GUILayout.Button("全选", GUILayout.Width(48f)))
				{
					selImgList = new List<string>(displayImgList);
				}
				if (GUILayout.Button("反选", GUILayout.Width(48f)))
				{
					List<string> tempList = new List<string>(selImgList);
					selImgList.Clear();
					foreach(var s in displayImgList)
					{
						if (!tempList.Contains(s))
						{
							selImgList.Add(s);
						}
					}
				}
				_searchFilter = EditorGUILayout.TextField("", _searchFilter, "SearchTextField");

				if (GUILayout.Button("x", "SearchCancelButton", GUILayout.Width(18f)))
				{
					_searchFilter = "";
					GUIUtility.keyboardControl = 0;
				}

			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("清除选中", GUILayout.Width(88f)))
			{
				if (selImgList.Count > 0)
				{
					CleanSelImg();
					SaveJson();
				}
			}
			if (GUILayout.Button("忽略选中", GUILayout.Width(88f)))
			{
				if (selImgList.Count > 0)
				{
					foreach (var k in selImgList)
					{
						if (!ingoreStrList.Contains(k)) ingoreStrList.Add(k);
					}
					selImgList.Clear();
					SaveJson();
				}
			}

			GUILayout.EndHorizontal();
			GUILayout.Label("Sprite未被prefab引用列表(代码中可能使用):");

			_scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
			{
				displayImgList.Clear();
				for (int i = 0; i < unusedSpriteList.Count; i++)
				{
					var s = unusedSpriteList[i];
					if (IsValidKey(s))
					{
						EditorGUILayout.BeginHorizontal();
						if (selImgList.Contains(s))
						{
							if (GUILayout.Button("√", GUILayout.Width(22f)))
							{
								selImgList.Remove(s);
							}
						}
						else
						{
							if (GUILayout.Button(" ", GUILayout.Width(22f)))
							{
								selImgList.Add(s);
							}
						}
						displayImgList.Add(s);
						GUIStyle style = new GUIStyle();
						style.alignment = TextAnchor.MiddleLeft;
						style.normal = new GUIStyleState();
						style.normal.textColor = Color.white;
						if (GUILayout.Button(s, style, GUILayout.Height(20f)))
						{
							SelectAtalsSprite(GetAtlas(Key2AtlasName(s)), Key2SpriteName(s));
						}
						EditorGUILayout.EndHorizontal();
					}
				}
			}
			EditorGUILayout.EndScrollView();
		}
		else
		{
			GUILayout.Label("请先检查未使用Sprite");
		}
	}
	public void DrawTextureUnuseList()
	{
		if (unusedTextureList.Count > 0)
		{
			GUILayout.BeginHorizontal();
			{
				if (GUILayout.Button("全选", GUILayout.Width(48f)))
				{
					selTexList = new List<string>(displayTexList);
				}
				if (GUILayout.Button("反选", GUILayout.Width(48f)))
				{
					List<string> tempList = new List<string>(selTexList);
					selTexList.Clear();
					foreach (var s in displayTexList)
					{
						if (!tempList.Contains(s))
						{
							selTexList.Add(s);
						}
					}
				}
				_searchFilter = EditorGUILayout.TextField("", _searchFilter, "SearchTextField");

				if (GUILayout.Button("x", "SearchCancelButton", GUILayout.Width(18f)))
				{
					_searchFilter = "";
					GUIUtility.keyboardControl = 0;
				}

			}
			GUILayout.EndHorizontal();

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("清除选中", GUILayout.Width(88f)))
			{
				if (selTexList.Count > 0)
				{
					CleanSelTex();
					SaveJson();
				}
			}
			if (GUILayout.Button("忽略选中", GUILayout.Width(88f)))
			{
				if (selTexList.Count > 0)
				{
					foreach (var k in selTexList)
					{
						if (!ingoreStrList.Contains(k)) ingoreStrList.Add(k);
					}
					selTexList.Clear();
					SaveJson();
				}
			}

			GUILayout.EndHorizontal();
			GUILayout.Label("Texture未被prefab引用列表(代码中可能使用):");

			_scrollPos4 = EditorGUILayout.BeginScrollView(_scrollPos4);
			{
				displayTexList.Clear();
				for (int i = 0; i < unusedTextureList.Count; i++)
				{
					var s = unusedTextureList[i];
					if (IsValidKey(s))
					{
						EditorGUILayout.BeginHorizontal();
						if (selTexList.Contains(s))
						{
							if (GUILayout.Button("√", GUILayout.Width(22f)))
							{
								selTexList.Remove(s);
							}
						}
						else
						{
							if (GUILayout.Button(" ", GUILayout.Width(22f)))
							{
								selTexList.Add(s);
							}
						}
						displayTexList.Add(s);
						GUIStyle style = new GUIStyle();
						style.alignment = TextAnchor.MiddleLeft;
						style.normal = new GUIStyleState();
						style.normal.textColor = Color.white;
						if (GUILayout.Button(s, style, GUILayout.Height(20f)))
						{
							SelectTexture(s);
						}
						EditorGUILayout.EndHorizontal();
					}
				}
			}
			EditorGUILayout.EndScrollView();
		}
		else
		{
			GUILayout.Label("请先检查未使用Texture");
		}
	}
	static public void SelectAtalsSprite(UIAtlas atlas, string spriteName)
	{
		Selection.activeObject = null;
		if (atlas != null)
		{
			NGUISettings.atlas = atlas;
			NGUIEditorTools.Select(atlas.gameObject);
		}
		NGUISettings.selectedSprite = spriteName;
	
		if (UIAtlasInspector.instance != null) UIAtlasInspector.instance.Repaint();
		if (_quickSet)
		{
			if (atlas != null)
			{
				_atlas = atlas;
				_newAtlas = atlas;
			}
			_oldSpriteName = spriteName;
			_newSpriteName = spriteName;
		}
	}

	static public void SelectTexture(string path)
	{
		Selection.activeGameObject = null;
		Texture texture = AssetDatabase.LoadAssetAtPath<Texture>("Assets/GameRes/Texture/" + path);
		NGUISettings.texture = texture;
		Selection.activeObject = texture;
		if (UITextureInspector.instance != null) UITextureInspector.instance.Repaint();
		
	}
	public void DrawMoveCommonList()
	{
		GUILayout.Label("以下图片多处被引用，是否移到CommonAtlas:");
		_scrollPos3 = EditorGUILayout.BeginScrollView(_scrollPos3);
		{
			var foreachList = new List<string>(moveCommonList);
			for (int i = 0; i < foreachList.Count; i++)
			{
				var key = foreachList[i];
				EditorGUILayout.BeginHorizontal();

				if (GUILayout.Button("移动", GUILayout.Width(48f)))
				{
					var atlasName = Key2AtlasName(key);
					var sprName = Key2SpriteName(key);
					MoveSprite(atlasName, sprName, "CommonAtlas", sprName);
				}

				RefInfo refInfo;
				if (sprRefDict.TryGetValue(key, out refInfo))
				{
					GUILayout.Label("次数:" + refInfo.refDict.Count, GUILayout.Width(48f));
				}

				GUIStyle style = new GUIStyle();
				style.alignment = TextAnchor.MiddleLeft;
				style.normal = new GUIStyleState();
				style.normal.textColor = Color.white;
				if (GUILayout.Button(key, style, GUILayout.Height(20f)))
				{
					SelectAtalsSprite(GetAtlas(Key2AtlasName(key)), Key2SpriteName(key));
				}
				EditorGUILayout.EndHorizontal();
			}
		}
		EditorGUILayout.EndScrollView();
	}
	void AddDelayAction(Action action, long delayTime)
	{
		var callTime = DateTime.Now.Ticks / 10000000 + delayTime;
		delayActionDict.Add(action, callTime);
	}
	public bool IsValidKey(string key)
	{
		if (string.IsNullOrEmpty(_searchFilter) || key.IndexOf(_searchFilter, StringComparison.OrdinalIgnoreCase) >= 0)
		{
			foreach (var s in ingoreStrList)
			{
				if (key.Contains(s))
				{
					return false;
				}
			}
			return true;
		}
		return false;
	}
	public void ChekRef()
	{
		sprPrefixList.Clear();
		sprRefDict.Clear();
		moveCommonList.Clear();
		texRefDict.Clear();
		string[] GUIDs = AssetDatabase.FindAssets("t:Prefab", AssetPipeline.BuildBundlePath.UIFolder);
		for (var i = 0; i < GUIDs.Length; i++)
		{
			string resPath = AssetDatabase.GUIDToAssetPath(GUIDs[i]);
			if (resPath.IsViewPrefab() || resPath.IsHudPrefab())
			{
				var prefab = AssetDatabase.LoadAssetAtPath<GameObject>(resPath);
				var clone = GameObject.Instantiate(prefab);
				var comps = GetComponentsInGameObejct<UISprite>(clone);

				foreach (var sprite in comps)
				{
					if ((sprite.atlas != null) && (sprite.spriteName != null))
					{
						var key = GetKey(GetAtlasName(sprite.atlas as UIAtlas), sprite.spriteName);
						RefInfo refInfo;
						if (sprRefDict.TryGetValue(key, out refInfo))
						{
							refInfo.AddRef(resPath);
							if (refInfo.IsNeedMoveToCommon())
							{
								var atlasName = GetAtlasName(sprite.atlas as UIAtlas);
								if (atlasName.EndsWith("Atlas") && (atlasName != "CommonAtlas"))
								{
									if (!moveCommonList.Contains(key))
									{
										moveCommonList.Add(key);
									}
								}
							}

						}
						else
						{
							sprRefDict.Add(key, new RefInfo(resPath));
						}
					}
				}
				var sprAnimations = GetComponentsInGameObejct<UISpriteAnimation>(clone);
				foreach (var spriteAnimation in sprAnimations)
				{
					if (!sprPrefixList.Contains(spriteAnimation.namePrefix))
					{
						sprPrefixList.Add(spriteAnimation.namePrefix);
					}
				}

				var textures = GetComponentsInGameObejct<UITexture>(clone);
				foreach (UITexture tex in textures)
				{
					if (tex.mainTexture != null)
					{
						RefInfo refInfo;
						var key = tex.mainTexture.name;
						if (key == "pic_yuling_diwen")
						{
							UnityEngine.Debug.Log("CheckRef->" + key);
						}
						if (texRefDict.TryGetValue(key, out refInfo))
						{
							refInfo.AddRef(resPath);
						}
						else
						{
							texRefDict.Add(key, new RefInfo(resPath));
						}
					}
				}
				GameObject.DestroyImmediate(clone);
			}
			EditorUtility.DisplayProgressBar("引用检查中", string.Format(" {0} / {1} ", i, GUIDs.Length),
i / (float)GUIDs.Length);
		}
		EditorUtility.ClearProgressBar();
		SortKeyByRefCount(ref moveCommonList);
	}
	public void CheckSprite()
	{
		unusedSpriteList.Clear();
		var dirs = Directory.GetDirectories(UIPath.staticAtlasPath);
		foreach (var dir in dirs)
		{
			//D:/workspaces/N1/client/trunk/Assets/GameRes/Atlas/StaticAtlas/WorldBossAtlas
			var imgFloderPath = dir + "/Image/";
			var atlasName = Path.GetFileName(dir);
			foreach (var imgpath in Directory.GetFiles(imgFloderPath, "*.png"))
			{
				var sprName = Path.GetFileName(imgpath).Replace(".png", "");
				var key = GetKey(atlasName, sprName);
				if (!sprRefDict.ContainsKey(key) && !unusedSpriteList.Contains(key))
				{
					bool del = true;
					foreach (var prefix in sprPrefixList)
					{
						if (sprName.Contains(prefix))
						{
							del = false;
							continue;
						}
					}
					if (del)
					{
						unusedSpriteList.Add(key);
					}
				}
			}
		}
	}
	public void CheckTexture()
	{
		unusedTextureList.Clear();
		var files = Directory.GetFiles(UIPath.texturePath, "*.png",  SearchOption.AllDirectories);
		foreach (var filePath in files)
		{
			var sortPath = filePath.Replace(UIPath.texturePath, "").Replace("\\", "/");
			var filename = Path.GetFileNameWithoutExtension(filePath);
			if (!texRefDict.ContainsKey(filename))
			{
				if (!unusedTextureList.Contains(sortPath))
				{
					unusedTextureList.Add(sortPath);
				}
			}

			else 
			{
				//if (filename == "pic_yuling_diwen")
				//{
				//	UnityEngine.Debug.Log("In Use->" + filename);
				//}
			}
		}
	}
	public void CheckUnUse()
	{
		ChekRef();
		CheckSprite();
		CheckTexture();
	}
	public T[] GetComponentsInGameObejct<T>(GameObject go)
	{
		var compsInRoot = go.GetComponents<T>();
		var compsInChildren = go.GetComponentsInChildren<T>(true);
		var comps = new T[compsInRoot.Length + compsInChildren.Length];
		Array.Copy(compsInRoot, comps, compsInRoot.Length);
		Array.Copy(compsInChildren, 0, comps, compsInRoot.Length, compsInChildren.Length);
		return comps;
	}
	public string GetKey(string _atlas, string sprite)
	{
		return _atlas + "->" + sprite;
	}
	public string Key2AtlasName(string key)
	{
		return key.Split(new string[1]{"->"}, StringSplitOptions.None)[0];
	}
	public string Key2SpriteName(string key)
	{
		return key.Split(new string[1] { "->" }, StringSplitOptions.None)[1];
	}
	public string Key2Path(string key)
	{
		var arr = key.Split(new string[1]{"->"}, StringSplitOptions.None);
		return string.Format("{0}/GameRes/Atlas/{1}/{2}/Image/{3}.png", Application.dataPath, key.Key2AtlasType(),
			arr[0], arr[1]);
	}
	public string GetAtlasName(UIAtlas _atlas)
	{
		if (_atlas == null)
		{
			return "";
		}
		if (_atlas.replacement != null)
		{
			return (_atlas.replacement as UIAtlas).name;
		}
		else
		{
			return _atlas.name;
		}
	}
	public void CleanSelImg()
	{
		List<string> rebuildList = new List<string>();
		foreach (var s in selImgList)
		{
			var path = Key2Path(s);
			IOHelper.FUNCTAG_Delete(path);
			IOHelper.FUNCTAG_Delete(path+".meta");
			var atlasName = Key2AtlasName(s);
			if (!rebuildList.Contains(atlasName))
			{
				rebuildList.Add(atlasName);
			}
			if (unusedSpriteList.Contains(s))
			{
				unusedSpriteList.Remove(s);
			}
		}
		foreach (var atlasName in rebuildList)
		{
			AtlasRebuild(atlasName, true);
		}
	}
	public void CleanSelTex()
	{
		foreach (var s in selTexList)
		{
			var path = UIPath.texturePath + s;
			IOHelper.FUNCTAG_Delete(path);
			IOHelper.FUNCTAG_Delete(path + ".meta");
			if (unusedTextureList.Contains(s))
			{
				unusedTextureList.Remove(s);
			}
			var filename = Path.GetFileNameWithoutExtension(path);
			if (!texRefDict.ContainsKey(filename))
			{
				texRefDict.Remove(filename);
			}
		}
		AssetDatabase.Refresh();
	}
	void OnSelectAtlas(UnityEngine.Object obj)
	{
		if (obj == null) 
			_atlas = null; 
		else
			_atlas = obj as UIAtlas;
	}
	void OnSelectNewAtlas(UnityEngine.Object obj)
	{
		if (obj == null)
			_newAtlas = null;
		else
			_newAtlas = obj as UIAtlas;
	}
	static UIAtlas GetAtlas(string atlasName)
	{
		var atlasPath = string.Format("Assets/GameRes/Atlas/{0}/{1}/{2}.prefab", atlasName.AtlasNameType(), atlasName, atlasName);
		var obj = AssetDatabase.LoadAssetAtPath<GameObject>(atlasPath);
		if (obj)
		{
			return obj.GetComponent<UIAtlas>();
		}
		return null;
	}
	static UIAtlas GetRefAtlas(string atlasName)
	{

		var atlasPath = string.Format("Assets/GameRes/Atlas/Ref{0}.prefab", atlasName.Replace("Atlas.*", "Atlas"));
		var obj = AssetDatabase.LoadAssetAtPath<GameObject>(atlasPath);
		var _atlas = obj.GetComponent<UIAtlas>();
		return _atlas;
	}
	Texture GetSpriteImg(string atlasName, string sprName)
	{
		var atlasPath = string.Format("Assets/GameRes/Atlas/{0}/{1}/Image/{2}.png", atlasName.AtlasNameType(), atlasName, sprName);
		var texture = AssetDatabase.LoadAssetAtPath<Texture>(atlasPath) ;
		return texture;
	}
	public void AtlasAddSprite(string atlasName, string sprName)
	{
		List<string> sprList = new List<string>() { sprName };
		AtlasAddSprites(atlasName, sprList);
	}
	public void AtlasAddSprites(string atlasName, List<string> sprList)
	{
		EditorWindow.GetWindow<UIAtlasMaker>(false, "Atlas Maker", true).Show(true);
		_atlas = GetAtlas(atlasName);
		NGUISettings.atlas = _atlas;
		List<Texture> texList = new List<Texture>();
		foreach (var sprName in sprList)
		{
			texList.Add(GetSpriteImg(atlasName, sprName));
		}
		Selection.objects = texList.ToArray();
		UIAtlasMaker.instance.forceUpdate = true;
	}
	//重打图集
	public void AtlasRebuild(string atlasName, bool onlyDel = false)
	{
		EditorWindow.GetWindow<UIAtlasMaker>(false, "Atlas Maker", true).Show(true);
		_atlas = GetAtlas(atlasName);
		NGUISettings.atlas = _atlas;
		
		//选中所有image下的图片
		var imgPath = string.Format("Assets/GameRes/Atlas/{0}/{1}/Image", atlasName.AtlasNameType(),atlasName);
		List<Texture> tetures = new List<Texture>();
		AssetDatabase.Refresh();
		string[] GUIDs = AssetDatabase.FindAssets("t:Texture", new string[] { imgPath });
		List<string> tetureNameList = new List<string>();
		for (var i = 0; i < GUIDs.Length; i++)
		{
			string resPath = AssetDatabase.GUIDToAssetPath(GUIDs[i]);
			var texture = AssetDatabase.LoadAssetAtPath<Texture>(resPath);
			tetureNameList.Add(texture.name);
			tetures.Add(texture);
		}
		if (!onlyDel)
		{
			Selection.objects = tetures.ToArray();
		}
		//删除所有非image下的图片
		BetterList<string> spriteNames = NGUISettings.atlas.GetListOfSprites();
		List<string> delList = new List<string>();
		foreach (string sp in spriteNames)
		{
			if (!tetureNameList.Contains(sp))
			{
				delList.Add(sp);
			}

		}
		UIAtlasMaker.instance.forceDelNames = delList;
		UIAtlasMaker.instance.forceUpdate = true;
	}
	public void MoveSprFile(string atlasNameOld, string sprNameOld, string atlasNameNew, string sprNameNew)
	{
		var keyOld = GetKey(atlasNameOld, sprNameOld);
		var keyNew = GetKey(atlasNameNew, sprNameNew); 
		RefInfo refInfo;
		if (sprRefDict.TryGetValue(keyOld, out refInfo))
		{
			var cnt = 0;
			var allCnt = refInfo.refDict.Count;
			foreach (var assetPath in refInfo.refDict.Keys)
			{
				var asset = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
				GameObject go = UnityEngine.Object.Instantiate(asset);
				var comps = GetComponentsInGameObejct<UISprite>(go);
				var atlasOld = GetAtlas(atlasNameOld);
				var atlasNew = GetRefAtlas(atlasNameNew);
				foreach (var comp in comps)
				{
					if ((GetAtlasName(comp.atlas as UIAtlas) == atlasNameOld) && (comp.spriteName == sprNameOld))
					{
						comp.atlas = atlasNew;
						comp.spriteName = sprNameNew;

					}

				}
				PrefabUtility.ReplacePrefab(go, asset);
				UnityEngine.Object.DestroyImmediate(go);
				UnityEngine.Object.DestroyImmediate(asset);
				cnt++;
				EditorUtility.DisplayProgressBar("替换中:" + Path.GetFileName(assetPath), string.Format("{0} / {1} ", cnt, allCnt), cnt / (float)allCnt);
			}
		}
		IOHelper.FUNCTAG_Move(Key2Path(keyOld), Key2Path(keyNew));

				if (moveCommonList.Contains(keyOld)) moveCommonList.Remove(keyOld);
		RefInfo oldRefInfo;
		if (sprRefDict.TryGetValue(keyOld, out oldRefInfo))
		{
			RefInfo newRefInfo;
			if (sprRefDict.TryGetValue(keyNew, out newRefInfo))
			{
				foreach(var k in oldRefInfo.refDict.Keys)
				{
					newRefInfo.AddRef(k);
				}
			}
			else
			{
				sprRefDict.Add(keyNew, oldRefInfo);
			}
				
			sprRefDict.Remove(keyOld);
		}
		if (unusedSpriteList.Contains(keyOld))
		{
			unusedSpriteList.Remove(keyOld);
			unusedSpriteList.Add(keyNew);
		}
	}
	public void MoveMatchSprite(string atlasNameOld, string sprMatchOld, string atlasNameNew, string sprMatchNew)
	{
		var _atlas = GetAtlas(atlasNameOld);
		List<string> sprList = new List<string>();
		foreach (var sprName in _atlas.GetListOfSprites())
		{
			if (sprName.Contains(sprMatchOld))
			{
				var newSprName = sprName.Replace(sprMatchOld, sprMatchNew);
				MoveSprFile(atlasNameOld, sprName, atlasNameNew, newSprName);
				sprList.Add(newSprName);
			}
		}
		SaveJson();
		//这一帧删除
		AtlasRebuild(atlasNameOld, true);
		//稍后增加
		AddDelayAction(() => { AtlasAddSprites(atlasNameNew, sprList); }, 1);
		AssetDatabase.Refresh();
		
	}
	//移动图片位置
	public void MoveSprite(string atlasNameOld, string sprNameOld, string atlasNameNew, string sprNameNew)
	{
		MoveSprFile(atlasNameOld, sprNameOld, atlasNameNew, sprNameNew);
		SaveJson();
		//这一帧删除
		AtlasRebuild(atlasNameOld, true);	
	
		//稍后增加
		AddDelayAction(() => { AtlasAddSprite(atlasNameNew, sprNameNew); }, 1);
		
		AssetDatabase.Refresh();
	}
	static public void SetActiveChildren(GameObject go, bool active)
	{
		go.SetActive(active);
		Transform t = go.transform;
		for (int i = 0, imax = t.childCount; i < imax; ++i)
		{
			Transform child = t.GetChild(i);
			SetActiveChildren(child.gameObject, active);
		}
	}
	public void SortKeyByRefCount(ref List<string> keyList)
	{
		
		keyList.Sort((string key1, string key2) 
			=> {
				int cnt1 = 0;
				int cnt2 = 0;
				RefInfo refInfo1;
				if (sprRefDict.TryGetValue(key1, out refInfo1)) cnt1 = refInfo1.refDict.Count;
				RefInfo refInfo2;
				if (sprRefDict.TryGetValue(key2, out refInfo2)) cnt2 = refInfo2.refDict.Count;
				if (cnt2 == cnt1) return key2.CompareTo(key1);
				return cnt2.CompareTo(cnt1);
			}
		);
	}
	public void FindRefSprite(string atlasName, string sprName)
	{
		var key = GetKey(atlasName, sprName);

		RefInfo refInfo;
		if (sprRefDict.TryGetValue(key, out refInfo))
		{
			var cnt = 0;
			var allCnt = refInfo.refDict.Count;
			foreach (var assetPath in refInfo.refDict.Keys)
			{
				Debugger.Log(key + "-> " + assetPath);
			}
		}
	}
	public void LoadJson()
	{
		var data = CLASSTAG_FileHelper.ReadJsonFile<SerialData>(Application.dataPath + "/Editor/Custom/UITool/UIToolsConfig.json");
		if (data != null)
		{
			sprRefDict = data.sprRefDict;
			sprPrefixList = data.sprPrefixList;
			texRefDict = data.texRefDict;
			unusedSpriteList = data.unusedSpriteList;
			ingoreStrList = data.ingoreStrList;
			moveCommonList = data.moveCommonList;
			unusedTextureList = data.unusedTextureList;
			hasCheckAll = true;
		}
	}
	public void SaveJson()
	{
		var data = new SerialData();
		data.sprRefDict = sprRefDict;
		data.sprPrefixList = sprPrefixList;
		data.texRefDict = texRefDict;
		data.unusedSpriteList = unusedSpriteList;
		data.ingoreStrList = ingoreStrList;
		data.moveCommonList = moveCommonList;
		data.unusedTextureList = unusedTextureList;
		CLASSTAG_FileHelper.SaveJsonObj(data, Application.dataPath + "/Editor/Custom/UITool/UIToolsConfig.json", false, true);
	}
}