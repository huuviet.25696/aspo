using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Text;
using SimpleJson;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Debug = UnityEngine.Debug;

public class LuaToolsWindow : EditorWindow
{
    private static readonly string SETTING_FILE = Application.dataPath + "/Editor/setting.json";
    private static readonly string clientProtoPath = LuaConst.pbDir + "/proto/";
    private static int scriptVersion;

    private static int oldVersion;
    private static int newVersion;

    private static int curVersion;
    private static int patchVersion;

    
    public static void OpenLuaTools()
    {
        LuaToolsWindow window = (LuaToolsWindow)EditorWindow.GetWindow(typeof(LuaToolsWindow));
        window.Show();
    }

    void OnGUI()
    {
        scriptVersion = EditorGUILayout.IntField("Version number", scriptVersion);

        if (GUILayout.Button("Packaging script"))
        {
            //if (scriptVersion == 0)
            //{
            //    Debug.LogError("请输入scriptVersion");
            //    return;
            //}

            BuildLuaScript(scriptVersion);
        }

        GUILayout.Space(40);
        oldVersion = EditorGUILayout.IntField("oldVersion", oldVersion);
        newVersion = EditorGUILayout.IntField("newVersion", newVersion);
        if (GUILayout.Button("Make script patch"))
        {
            //if (oldVersion == 0)
            //{
            //    Debug.LogError("请输入oldVersion");
            //    return;
            //}

            //if (newVersion == 0)
            //{
            //    Debug.LogError("请输入newVersion");
            //    return;
            //}

            if (newVersion <= oldVersion)
            {
                Debug.LogError("error：newVersion <= oldVersion");
                return;
            }
            MakeScriptPatch(oldVersion, newVersion);
        }

        GUILayout.Space(40);
        curVersion = EditorGUILayout.IntField("curVersion", curVersion);
        patchVersion = EditorGUILayout.IntField("patchVersion", patchVersion);
        if (GUILayout.Button("Upgrade patch"))
        {
            //if (curVersion == 0)
            //{
            //    Debug.LogError("请输入curVersion");
            //    return;
            //}

            //if (patchVersion == 0)
            //{
            //    Debug.LogError("请输入patchVersion");
            //    return;
            //}

            if (patchVersion <= curVersion)
            {
                Debug.LogError("error：patchVersion <= curVersion");
                return;
            }
            MergeScriptPatch(curVersion, patchVersion);
        }
    }

    public static void BuildLuaScript(int version)
    {
        string srcDir = LuaConst.luaDir; // Application.dataPath + "/Lua_cn";
        string dstFile = Application.streamingAssetsPath + "/script_" + version;
        string dstDir = Path.GetDirectoryName(dstFile);
        if (!Directory.Exists(dstDir))
        {
            Directory.CreateDirectory(dstDir);
        }

        CLASSTAG_LuaScript scriptPack = new CLASSTAG_LuaScript();
        scriptPack.FUNCTAG_LoadFromDir(srcDir);
        scriptPack.SaveToFile(dstFile, version);
        Debug.Log(string.Format("Packaging script succeeded path={0} version number={1}", dstFile, version));
    }

    //public static void MakeScriptPatch(int version)
    //{
    //    string srcDir = LuaConst.luaDir;//Application.dataPath + "/Lua";
    //    string dstFile = Application.streamingAssetsPath + "/script_" + version;
    //    string dstDir = Path.GetDirectoryName(dstFile);
    //    if (!Directory.Exists(dstDir))
    //    {
    //        Directory.CreateDirectory(dstDir);
    //    }

    //    LuaScript scriptPack = new LuaScript();
    //    scriptPack.LoadFromDir(srcDir);
    //    scriptPack.SaveToFile(dstFile, version);
    //    Debug.Log(string.Format("打包脚本成功 path={0} 版本号={1}", dstFile, version));
    //}

    public static void MakeScriptPatch(int oldVersion, int newVersion)
    {
        string oldFile = Application.streamingAssetsPath + "/script_" + oldVersion;
        string newFile = Application.streamingAssetsPath + "/script_" + newVersion;

        CLASSTAG_LuaScript oldZip = new CLASSTAG_LuaScript();
        oldZip.LoadFrom(oldFile);

        CLASSTAG_LuaScript newZip = new CLASSTAG_LuaScript();
        newZip.LoadFrom(newFile);

        string patchFile = Application.streamingAssetsPath + "/patch_" + newVersion;

        CLASSTAG_LuaScript patchZip = CLASSTAG_LuaScript.MakePatch(oldZip, newZip);
        patchZip.SaveToFile(patchFile, newVersion);

        Debug.Log(string.Format("MakeScriptPatch Done! path={0} version={1}", patchFile, newVersion));
    }


    public static void MergeScriptPatch(int curVersion, int patchVersion)
    {
        string oldFile = Application.streamingAssetsPath + "/script_" + curVersion;
        string newFile = Application.streamingAssetsPath + "/patch_" + patchVersion;

        CLASSTAG_LuaScript oldZip = new CLASSTAG_LuaScript();
        oldZip.LoadFrom(oldFile);

        CLASSTAG_LuaScript newZip = new CLASSTAG_LuaScript();
        newZip.LoadFrom(newFile);

        string patchFile = Application.streamingAssetsPath + "/new_script_" + patchVersion;

        oldZip.MergePatch(newZip);

        //LuaScript patchZip = LuaScript.MakePatch(oldZip, newZip);
        oldZip.SaveToFile(patchFile, patchVersion);

        Debug.Log(string.Format("MergeScriptPatch Done! path={0} version number={1}", patchFile, patchVersion));
    }



}


