using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;
using LuaInterface;

public class LuaEditorWindow : EditorWindow
{
	delegate void PuashArgAction(LuaFunction func);
	private string windowName;
	public LuaFunction luaOnGUI;

	private static LuaState _luaStateInstance;
	public static LuaState LuaStateInstace
	{
		get
		{
			if (_luaStateInstance == null)
			{
				if (LuaMain.Instance != null)
				{
					_luaStateInstance = LuaMain.Instance.luaState;
				}
				else
				{
					_luaStateInstance = NewLuaState();
				}
			}
			return _luaStateInstance;
		}
	}

	private static LuaState NewLuaState()
	{
		var luaState = new LuaState();
		luaState.OpenLibs(LuaDLL.luaopen_protobuf_c);
		luaState.LuaGetField(LuaIndexes.LUA_REGISTRYINDEX, "_LOADED");
		luaState.OpenLibs(LuaDLL.luaopen_cjson);
		luaState.LuaSetField(-2, "cjson");
		luaState.LuaSetTop(0);
		LuaBinder.Bind(luaState);
		luaState.Start();
		return luaState;
	}

	public static void OpenLuaEditor(string name)
	{
		Call(GetLuaFunction("main", "RequireModule"));
		LuaEditorWindow window = (LuaEditorWindow)EditorWindow.GetWindow(typeof(LuaEditorWindow));
		window.Show();
		window.windowName = name;

		LuaFunction func = GetLuaFunction("/logic/editor/editorgui/CEditorWindowBase", "ShowWindow");
		Call(func, (LuaFunction luafunc) => {
			luafunc.Push(name);
			luafunc.Push(window);
		});
		
		window.luaOnGUI = GetLuaFunction("/logic/editor/editorgui/CEditorWindowBase", "WindowGUI");

	}
	static LuaFunction GetLuaFunction(string fileName, string funcName)
	{
		return GetLuaFunction(LuaStateInstace, fileName, funcName);
	}

	static LuaFunction  GetLuaFunction(LuaState luaState, string fileName, string funcName)
	{
        object[] array = luaState.DoFile(fileName);
		LuaTable luaTable = array[0] as LuaTable;
		return luaTable[funcName] as LuaFunction;
	}
	static void Call(LuaFunction luafunc)
	{
		Call(luafunc, (LuaFunction func) => { });
	}

	static void Call(LuaFunction luafunc, PuashArgAction action)
	{
		luafunc.BeginPCall();
		luafunc.Push();
		action(luafunc);
		luafunc.PCall();
		luafunc.EndPCall();
	}


    private void OnGUI()
    {
		if (this.luaOnGUI != null)
		{
			Call(this.luaOnGUI, (LuaFunction luafunc) =>
			{
				luafunc.Push(this.windowName);
			});
		}
    }

	void OnDestroy()
	{
		LuaFunction func = GetLuaFunction("/logic/editor/editorgui/CEditorWindowBase", "CloseWindow");
		Call(func, (LuaFunction luafunc) =>
		{
			luafunc.Push(this.windowName);
		});
	}

}
