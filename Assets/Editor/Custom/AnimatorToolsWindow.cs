using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Text;
using SimpleJson;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Debug = UnityEngine.Debug;


public class AnimatorToolsWindow : EditorWindow
{
    private static readonly string characterPath = "Assets/GameRes/Model/Character";
    private static readonly string baseOverrideAnimatorPath = "Assets/GameRes/Model/Template/CharacterAnim/Base/Animator.overrideController";
    private string modelStr = "";
    static Dictionary<string, string> replaceDict = new Dictionary<string, string>();

    [MenuItem("Tools/Animator tools", false, 61)]
    public static void OpenAnimatorTools()
    {
        if (replaceDict.Count == 0)
        {
            replaceDict.Add("magic", "attack1");
            replaceDict.Add("idleWar", "idleCity");
            replaceDict.Add("attack2", "attack1");
            replaceDict.Add("attack3", "attack1");
            replaceDict.Add("attack4", "attack1");
            replaceDict.Add("attack5", "attack1");
			replaceDict.Add("show", "win");
        }

        AnimatorToolsWindow window = (AnimatorToolsWindow)EditorWindow.GetWindow(typeof(AnimatorToolsWindow));
        window.Show();
    }

	static public bool HasSocialAnim(int modelType)
	{
		if (130 <= modelType && modelType <= 160)
			return true;
		return false;
	}

    public void CreateOne(string sModel)
    {
        var dir = string.Format("{0}/{1}/Anim/", characterPath, sModel);
        var anims = System.IO.Directory.GetFiles(dir, "*.anim", 0);
        var list = new List<string>();
        list.Add("");
        foreach (var anim in anims)
        {
            var animName = System.IO.Path.GetFileNameWithoutExtension(anim);
            var splts = animName.Split('_');
            if (splts.Length == 2)
            {
				var sub = '_' + splts[1];
				if (! list.Contains(sub))
				{
					list.Add(sub);
				}
                
            }
        }
        for (int i=0; i < list.Count; i++)
        {
			var sub = list[i];
			//EditorUtility.DisplayProgressBar(sModel + "导出中:" + sub, string.Format("{0} / {1} ", i, list.Count), i / (float)list.Count);
            CreateOne(sModel, sub);
        }
		Debug.Log(sModel + ",动作导出完成!");
		EditorUtility.ClearProgressBar();
    }

    public void CreateOne(string sModel, string sub)
    {
		var animator = CreateOne(sModel, sub, "Base");
		if (HasSocialAnim(int.Parse(sModel)))
		{
			CreateOne(sModel, sub, "Social");
		}
		
		if (sub == "")
		{
			var path = string.Format("{0}/{1}/Prefabs/model{2}.prefab", characterPath, sModel, sModel);
			GameObject oldGo = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
			GameObject go = UnityEngine.Object.Instantiate(oldGo);
			var replaceceAnimator = go.GetMissingComponent<Animator>() as Animator;
			replaceceAnimator.runtimeAnimatorController = animator;
			PrefabUtility.ReplacePrefab(go, oldGo);
			UnityEngine.Object.DestroyImmediate(oldGo);
			UnityEngine.Object.DestroyImmediate(go);
		}

    }

	public AnimatorOverrideController  CreateOne(string sModel, string sub, string animatorType)
	{
		string overrideControllerPath = string.Format("Assets/GameRes/Model/Template/CharacterAnim/{0}/Animator.overrideController", animatorType);
		AnimatorOverrideController ori = AssetDatabase.LoadAssetAtPath(overrideControllerPath, typeof(AnimatorOverrideController)) as AnimatorOverrideController;
		AnimatorOverrideController animator = GameObject.Instantiate<AnimatorOverrideController>(ori);
		AnimationClipPair[] newclips = new AnimationClipPair[animator.clips.Length];
		for (int i = 0; i < animator.clips.Length; i++)
		{
			AnimationClipPair clipPair = animator.clips[i];
			string animOverridePath = string.Format("{0}/{1}/Anim/{2}{3}.anim", characterPath, sModel, clipPair.originalClip.name, sub);
			AnimationClip clipOverride = AssetDatabase.LoadAssetAtPath(animOverridePath, typeof(AnimationClip)) as AnimationClip;

			if (clipOverride == null)
			{
				if (replaceDict.ContainsKey(clipPair.originalClip.name))
				{
					string p = string.Format("{0}/{1}/Anim/{2}{3}.anim", characterPath, sModel, replaceDict[clipPair.originalClip.name], sub);
					clipOverride = AssetDatabase.LoadAssetAtPath(p, typeof(AnimationClip)) as AnimationClip;
					//Debug.Log("找到替换" + clipPair.originalClip.name + ".anim");
				}
				if (clipOverride == null && sub != "")
				{
					animOverridePath = string.Format("{0}/{1}/Anim/{2}.anim", characterPath, sModel, clipPair.originalClip.name);
					clipOverride = AssetDatabase.LoadAssetAtPath(animOverridePath, typeof(AnimationClip)) as AnimationClip;
				}
				if (clipOverride == null)
				{
					//Debug.Log("缺少" + clipPair.originalClip.name + ".anim");
				}

			}
			clipPair.overrideClip = clipOverride;
			newclips[i] = clipPair;
			EditorUtility.DisplayProgressBar(animatorType + "动作导出中:" + sModel + sub, string.Format("{0} / {1} ", i, animator.clips.Length), i / (float)animator.clips.Length);
		}
		animator.clips = newclips;
		string fileName = Path.GetFileNameWithoutExtension(overrideControllerPath);
		string prefixName = animatorType == "Base" ? "" : animatorType;
		string savePath = string.Format("{0}/{1}/Anim/{2}Animator{3}{4}.overrideController", characterPath, sModel, prefixName, sModel, sub);
		AssetDatabase.DeleteAsset(savePath);
		AssetDatabase.CreateAsset(animator, savePath);
		return animator;
	}

    public void ExportBindNode(string sModel)
    {
        var path = string.Format("{0}/{1}/Prefabs/model{2}.prefab", characterPath, sModel, sModel);
        GameObject oldGo = AssetDatabase.LoadAssetAtPath(path, typeof(GameObject)) as GameObject;
        GameObject go = UnityEngine.Object.Instantiate(oldGo);
        var dict = new Dictionary<int, Transform> ();
        FindInChilds(go.transform, ref dict, (Transform t) =>
        {
            var match = "Bip001 Prop";
            var index = t.name.ToLower().IndexOf(match.ToLower());
            if (index >= 0)
            {
                var sub = t.name.Substring(index+match.Length);
                var i = int.Parse(sub);
                if  (i> 0)
                {
                    return i;
                }
            }
            return  -1;
         });
        var container = go.GetMissingComponent<GameObjectContainer>();
        foreach(KeyValuePair<int, Transform> kvp in dict)
        {
            var p = kvp.Value.gameObject.transform;
            var gameObejct = new GameObject();
            gameObejct.name = "mount_" + kvp.Key;
            gameObejct.transform.SetParent(p, true);
            container.Add(kvp.Key, gameObejct, true);
        }
        PrefabUtility.ReplacePrefab(go, oldGo);
        UnityEngine.Object.DestroyImmediate(oldGo);
        UnityEngine.Object.DestroyImmediate(go);
        Debug.Log("导出绑定节点完成" + sModel);
    }

    public delegate int FilterFunc(Transform t);
    public void FindInChilds(Transform t, ref Dictionary<int, Transform> dict, FilterFunc f)
    {
        if (t.childCount == 0)
        {
            return;
        }
        for (int i = 0; i <= t.childCount - 1; i++)
        {
            var child = t.GetChild(i);
            var key = f(child);
            if (key > 0)
            {
                dict.Add(key, child);
            }
            FindInChilds(child, ref dict, f);
        }
    }

    void OnGUI()
    {
        modelStr = EditorGUILayout.TextField("模型编号", modelStr);
		if (GUILayout.Button("模型导出", "LargeButton"))
        {
			EditorUtil.RunLuaFunc("EditorGUITools.ExportModel", modelStr);
        }

		if (GUILayout.Button("导出全部模型", "LargeButton"))
        {
			if (EditorUtility.DisplayDialog("提示", "你确定要导出全部模型吗？ ", "确定", "取消"))
			{
				EditorUtil.RunLuaFunc("EditorGUITools.ExportModelAll");
			}
        }

		GUILayout.Label("导出绑定节点", EditorStyles.boldLabel);
		if (GUILayout.Button("导出绑定节点", "LargeButton"))
		{
			ExportBindNode(modelStr);
		}

        GUILayout.Label("更新动作时间文件", EditorStyles.boldLabel);
        if (GUILayout.Button("导出", "LargeButton"))
        {
			EditorUtil.RunLuaFunc("EditorGUITools.GenAnimTimeData");
        }

		//GUILayout.Label("动作名修改", EditorStyles.boldLabel);
		//if (GUILayout.Button("单个", "LargeButton"))
		//{
		//	EditorUtil.RunLuaFunc("EditorGUITools.ReplaceAnimName", modelStr);
		//}

		//if (GUILayout.Button("全部", "LargeButton"))
		//{
		//	if (EditorUtility.DisplayDialog("提示", "修改全部模型吗？ ", "确定", "取消"))
		//	{
		//		EditorUtil.RunLuaFunc("EditorGUITools.ReplacAniNameAll");
		//	}
		//}

		//GUILayout.Label("生成动作文件", EditorStyles.boldLabel);
		//if (GUILayout.Button("生成组合动作文件"))
		//{
		//	EditorUtil.RunLuaFunc("EditorGUITools.GenAllCombActAnim");
		//}

		//GUILayout.Label("更新材质", EditorStyles.boldLabel);
		//if (GUILayout.Button("更新", "LargeButton"))
		//{
		//	ReplaceMatHelper.Run();
		//}
    }
}


