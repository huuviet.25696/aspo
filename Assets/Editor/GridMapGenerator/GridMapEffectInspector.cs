using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(GridMapEffectEditor))]
public class GridMapEffectInspector : Editor
{
    private static GUIStyle greenDot = "sv_label_3";
    private static GUIStyle orangeDot = "sv_label_5";
    static int s_Hash = "GridMapEffect".GetHashCode();
    private Vector3 mStartPos;
    private float mStartRotate;
    private float mStartHeight;
    private float mStartWidth;
    private Vector2 mStartOffset;
    private Vector3 mStartCenter;
    private int index = -1;
    private Action action = Action.None;
    void OnSceneGUI()
    {
        GridMapEffectEditor t = target as GridMapEffectEditor;

        Vector3[] vector3s = new Vector3[4];
        CaculatePoin(t, vector3s);

        for (int i = 0; i < 3; i++)
        {
            DrawLine(vector3s[i], vector3s[i + 1]);
        }
        DrawLine(vector3s[3], vector3s[0]);

        Event e = Event.current;
        int id = GUIUtility.GetControlID(s_Hash, FocusType.Passive);
        EventType type = e.GetTypeForControl(id);
        if (EventType.Repaint == type)
        {
            for (int i = 0; i < vector3s.Length; i++)
            {
                Handles.BeginGUI();
                {
                    DrawDot(vector3s[i], false, id);

                }
                Handles.EndGUI();
            }
        }
        else if (EventType.MouseDown == type)
        {
            Vector3 hit;
            if (e.button == 0 && e.isMouse && Raycast(vector3s, out hit))
            {
                e.Use();
                GUIUtility.hotControl = GUIUtility.keyboardControl = id;
                mStartPos = hit;
                mStartRotate = t.rotate;
                mStartWidth = t.width;
                mStartHeight = t.height;
                mStartOffset = t.offset;
                mStartCenter = t.transform.position + (Vector3)t.offset;
                action = GetAction(hit, vector3s, out index);
            }
        }
        else if (EventType.MouseDrag == type)
        {
            if (GUIUtility.hotControl == id && e.button == 0)
            {
                e.Use();
                Vector3 hit;
                if (Raycast(vector3s, out hit))
                {
                    Vector3 moveDelta = hit - mStartPos;
                    if (action == Action.Move)
                    {
                        t.offset = (Vector3)mStartOffset + moveDelta;
                    }
                    else if (action == Action.Rotate)
                    {
                        Vector3 startDir = mStartPos - (Vector3)mStartCenter;
                        Vector3 endDir =  hit - (Vector3)mStartCenter;
                        startDir.z = 0;
                        endDir.z = 0;
                        float angle = Vector2.Angle(startDir, endDir);
                        angle *= Mathf.Sign(Vector3.Dot(Vector3.Cross(startDir, endDir), Vector3.forward));
                        t.rotate = mStartRotate + angle;

                    }
                    else if (action == Action.Scale && index >= 0 && index <= 3)
                    {
                        Matrix4x4 rotateMatrix4X4 = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0f, 0f, -mStartRotate), Vector3.one);
                        Vector3 newMoveDelta = rotateMatrix4X4.MultiplyPoint3x4(moveDelta);
                        Rect rect = new Rect(0f, 0f, mStartWidth, mStartHeight);
                        Vector2 oldCenter = rect.center;
                        if (index == 0)
                        {
                            rect.xMin += newMoveDelta.x;
                            rect.yMax += newMoveDelta.y;
                        }
                        else if (index == 1)
                        {
                            rect.max += (Vector2)newMoveDelta;
                        }
                        else if (index == 2)
                        {
                            rect.xMax += newMoveDelta.x;
                            rect.yMin += newMoveDelta.y;
                        }
                        else if (index == 3)
                        {
                            rect.min += (Vector2)newMoveDelta;
                        }
                        Vector2 centerOffset = rect.center - oldCenter;
                        centerOffset = rotateMatrix4X4.inverse.MultiplyPoint3x4(centerOffset);
                        t.width = rect.width;
                        t.height = rect.height;
                        t.offset = mStartOffset + centerOffset;
                    }
                }
            }
        }
        else if (EventType.MouseUp == type)
        {
            if (GUIUtility.hotControl == id && e.button == 0)
            {
                GUIUtility.hotControl = GUIUtility.keyboardControl = 0;
                e.Use();
                action = Action.None; 

            }
        }
        CaculateMouseAction(vector3s);

    }

    private void CaculatePoin(GridMapEffectEditor t, Vector3[] vector3s)
    {
        //TopLeft为零，顺时针
        for (int i = 0; i < vector3s.Length; i++)
        {
            vector3s[i] = Vector3.zero;
        }
        t.width = Mathf.Max(0.5f, t.width);
        t.height = Mathf.Max(0.5f, t.height);
        float tWidth = t.width / 2;
        float tHeight = t.height / 2;

        vector3s[0].x = vector3s[0].x - tWidth;
        vector3s[0].y = vector3s[0].y + tHeight;

        vector3s[1].x = vector3s[1].x + tWidth;
        vector3s[1].y = vector3s[1].y + tHeight;

        vector3s[2].x = vector3s[2].x + tWidth;
        vector3s[2].y = vector3s[2].y - tHeight;

        vector3s[3].x = vector3s[3].x - tWidth;
        vector3s[3].y = vector3s[3].y - tHeight;

        Matrix4x4 matrixRotate = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0f, 0f, t.rotate), Vector3.one);

        for (int i = 0; i < vector3s.Length; i++)
        {
            vector3s[i] = matrixRotate.MultiplyPoint3x4(vector3s[i]);
            vector3s[i] = vector3s[i] + t.transform.position + new Vector3(t.offset.x, t.offset.y, 0f);
        }

    }
    private Action GetAction(Vector3 hitVector3, Vector3[] worldVector3s, out int poinIndex)
    {
        poinIndex = -1;
        {
            for (int i = 0; i < worldVector3s.Length; i++)
            {
                Bounds b = new Bounds(HandleUtility.WorldToGUIPoint(worldVector3s[i]), Vector3.one * 40);
                Vector3 screenHit = HandleUtility.WorldToGUIPoint(hitVector3);
                if (b.Contains(screenHit))
                {
                    poinIndex = i;
                    break;
                }
            }
            if (poinIndex != -1)
            {
                return Action.Scale;
            }
        }
        {
            Bounds b = new Bounds(worldVector3s[0], Vector3.zero);
            for (int i = 1; i < worldVector3s.Length; i++)
            {
                b.Encapsulate(worldVector3s[i]);
            }
            if (b.Contains(hitVector3))
                return Action.Move;
        }
        {
            float oldDistance = float.MaxValue;
            for (int i = 0; i < worldVector3s.Length; i++)
            {
                Vector3 screenHit = HandleUtility.WorldToGUIPoint(hitVector3);
                float distance = Vector2.Distance(HandleUtility.WorldToGUIPoint(worldVector3s[i]), screenHit);
                if (distance < oldDistance)
                {
                    oldDistance = distance;
                    poinIndex = i;
                }
            }

            if (oldDistance < 60f)
            {
                return Action.Rotate;
            }
        }
        return Action.None;
    }

    private void CaculateMouseAction(Vector3[] vector3s)
    {
        int index;
        Vector3 hit;
        Raycast(vector3s, out hit);
        Action action = this.action == Action.None ? GetAction(hit, vector3s, out index) : this.action;
        switch (action)
        {
            case Action.Move:
                DrawMouse(vector3s, MouseCursor.MoveArrow);
                break;
            case Action.Rotate:
                DrawMouse(vector3s, MouseCursor.RotateArrow);
                break;
            case Action.Scale:
                DrawMouse(vector3s, MouseCursor.ScaleArrow);
                break;
            case Action.None:
                DrawMouse(vector3s, MouseCursor.Arrow);
                break;

        }
    }
    private void DrawLine(Vector3 start, Vector3 end)
    {
        Handles.DrawLine(start, end);
    }

    private void DrawDot(Vector3 point, bool selected, int id)
    {
        Vector2 screenPoint = HandleUtility.WorldToGUIPoint(point);
        Rect rect = new Rect(screenPoint.x - 5f, screenPoint.y - 9f, 14f, 14f);
        if (selected)
            orangeDot.Draw(rect, GUIContent.none, id);
        else
            greenDot.Draw(rect, GUIContent.none, id);

    }

    private void DrawMouse(Vector3[] worldPos, MouseCursor cursor)
    {
        Vector2[] screenPos = new Vector2[worldPos.Length];
        for (int i = 0; i < worldPos.Length; ++i) screenPos[i] = HandleUtility.WorldToGUIPoint(worldPos[i]);

        Bounds b = new Bounds(screenPos[0], Vector3.zero);
        for (int i = 1; i < screenPos.Length; ++i) b.Encapsulate(screenPos[i]);
        Vector2 min = b.min;
        Vector2 max = b.max;

        min.x -= 60f;
        max.x += 60f;
        min.y -= 60f;
        max.y += 60f;

        Rect rect = new Rect(min.x, min.y, max.x - min.x, max.y - min.y);
        EditorGUIUtility.AddCursorRect(rect, cursor);

    }

    private bool Raycast(Vector3[] corners, out Vector3 hit)
    {
        Plane plane = new Plane(corners[0], corners[1], corners[2]);
        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        float dist = 0f;
        bool isHit = plane.Raycast(ray, out dist);
        hit = isHit ? ray.GetPoint(dist) : Vector3.zero;
        return isHit;
    }


    enum Action
    {
        None,
        Move,
        Scale,
        Rotate,
    }
}