using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using AssetPipeline;
using LITJson;
using UnityEditor;
using UnityEngine;
using LuaInterface;
#if UNITY_2020_1_OR_NEWER
    using UnityEditor.Build.Reporting;
#endif
public class PlayerSettingTool : EditorWindow
{
    private static PlayerSettingTool instance = null;

    public static void ShowWindow()
    {
        if (instance == null)
        {
            PlayerSettingTool window = (PlayerSettingTool)EditorWindow.GetWindow(typeof(PlayerSettingTool));
            window.minSize = new Vector2(562, 562);
            window.Show();
            PlayerSettingTool.instance = window;
        }
        else
        {
            PlayerSettingTool.instance.Close();
        }
    }

    private void OnEnable()
    {
        PlayerSettingTool.instance = this;
        _minResBuild = IsMinResBuild();
    }

    private void OnDisable()
    {
        PlayerSettingTool.instance = null;
    }

    private CLASSTAG_GameSettingData _gameSettingData = null;
    private ChannelConfig _channelConfig = null;



    private string oriClassTag = "CLASS-TAG_".Replace("-", "");
    private string replaceClassTag = "";
    private string oriFuncTag = "FUNC-TAG_".Replace("-", "");
    private string replaceFuncTag = "";

    private bool _updateLog = false;
    private bool _developmentBuild = false;
    private bool _minResBuild = false;
    private bool _enableJSB = false;
    private bool _useJsz = false;
	
    private void GetGameSettingData()
    {
        _gameSettingData = CLASSTAG_GameSetting.FUNCTAG_LoadGameSettingData();
        _channelConfig = ChannelConfig.LoadChannelConfig(_gameSettingData.configSuffix);
    }

    private string _lastSelectGameType;
    private int _lastSelectGameTypeIndex = 0;
    private int _lastSelectDomainIndex = 0;
    private int _lastSelectChannelIndex = 0;

    private void SaveGameSettingData()
    {
        if (_gameSettingData != null)
        {
            //FileHelper.SaveJsonObj(_gameSettingData, GameSetting.Config_WritePathV2, false, true);
            //保存一份到streamAsset下
            CLASSTAG_FileHelper.SaveJsonObj(_gameSettingData, CLASSTAG_GameSetting.Config_WritePathV2, false, true);
        }
        AssetDatabase.Refresh();
    }


    private static string garbageCount = "4000";
    private Vector2 _scrollPos;
    private string _buildToFileName = "";
    private string _buildProjmods = "";
	private string _bundleVersionCode = "";
    private void OnGUI()
    {
        if (_gameSettingData == null)
        {
            GetGameSettingData();
        }

        _scrollPos = EditorGUILayout.BeginScrollView(_scrollPos);
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField("ProductName ： " + _gameSettingData.gamename);
            EditorGUILayout.LabelField("Bundle Identifier ： " + PlayerSettings.applicationIdentifier);
            EditorGUILayout.LabelField("Bundle Version ： " + PlayerSettings.bundleVersion);
#if UNITY_ANDROID
            EditorGUILayout.LabelField("Bundle Version Code： " + PlayerSettings.Android.bundleVersionCode);
#elif UNITY_IPHONE
            EditorGUILayout.LabelField("Bundle Version Code： " + PlayerSettings.iOS.buildNumber);
#endif
            EditorGUILayout.LabelField("HttpRoot： " + _gameSettingData.httpRoot);
            EditorGUILayout.LabelField("TestHttpRoot： " + _gameSettingData.testHttpRoot);
            EditorGUILayout.LabelField("BuildToFile： " + _buildToFileName);
            EditorGUILayout.LabelField("projmods： " + _buildProjmods);
            EditorGUILayout.Space();

            //GameType选项
            int selectGameTypeIndex = EditorGUILayout.IntPopup("GameType :", _lastSelectGameTypeIndex, _channelConfig._gameTypeValues, _channelConfig._gameTypeKeys);

            if (_lastSelectGameTypeIndex != selectGameTypeIndex)
            {
                _lastSelectGameTypeIndex = selectGameTypeIndex;
                _gameSettingData.gameType = _channelConfig._gameTypeValues[_lastSelectGameTypeIndex];
                GameInfo gameInfo = _channelConfig._gameInfoDic[_gameSettingData.gameType];
                _gameSettingData.gamename = gameInfo.gamename;
                _gameSettingData.configSuffix = gameInfo.configSuffix;
                _channelConfig.LoadSPChannelConfig(_gameSettingData.configSuffix);
            }

            //if (_gameInfoDic.ContainsKey(_gameSettingData.gameType) == false)
            //{
            //    EditorGUILayout.LabelField(string.Format("GameType {0} no support!!!", _gameSettingData.gameType));
            //    EditorGUILayout.EndScrollView();                
            //    return;
            //}

            ////Domain类型选项
            _lastSelectDomainIndex = _channelConfig.UpdateDomainList(_gameSettingData.gameType, _gameSettingData.domainType);
            _lastSelectDomainIndex = EditorGUILayout.IntPopup("DomainType : ", _lastSelectDomainIndex, _channelConfig._domainValues, _channelConfig._domainKeys);

            _gameSettingData.domainType = _channelConfig.GetDomianType(_gameSettingData.gameType, _lastSelectDomainIndex);

            DomainInfo domainInfo = _channelConfig.GetDomainInfo(_gameSettingData.gameType, _gameSettingData.domainType);
            if (domainInfo == null)
            {
                EditorGUILayout.LabelField(string.Format("DomainType {0} no support!!!", _gameSettingData.domainType));
                EditorGUILayout.EndScrollView();
                return;
            }
            _gameSettingData.httpRoot = domainInfo.httproot;
            _gameSettingData.testHttpRoot = domainInfo.testhttproot;
            _gameSettingData.resdir = domainInfo.type;

            _lastSelectChannelIndex = _channelConfig.UpdateSpSdkList(_gameSettingData.domainType, _gameSettingData.channel);
            _lastSelectChannelIndex = EditorGUILayout.IntPopup("Channel : ", _lastSelectChannelIndex, _channelConfig._channelValues, _channelConfig._channelKeys);

            _gameSettingData.updateMode = (CLASSTAG_GameSetting.UpdateMode)EditorGUILayout.EnumPopup("Patch update form :", _gameSettingData.updateMode);

            //调试选项
            EditorGUILayout.Space();
            //_gameSettingData.checkUpdate = EditorGUILayout.Toggle("检查资源更新 : ", _gameSettingData.checkUpdate);
            //_gameSettingData.showUpdateSetting = EditorGUILayout.Toggle("内开发选择补丁途径 : ", _gameSettingData.showUpdateSetting);
            _gameSettingData.channel = _channelConfig._channelValues[_lastSelectChannelIndex];
            GameInfo gameInfo2 = _channelConfig._gameInfoDic[_gameSettingData.gameType];

            _gameSettingData.gamename = gameInfo2.gamename;
            //if (_gameSettingData.updateMode == GameSetting.UpdateMode.NoUpdate)
            //{
            //    _gameSettingData.gamename = gameInfo2.gamename + "_不更新";
            //}
            //else 
            if (_gameSettingData.updateMode == CLASSTAG_GameSetting.UpdateMode.TestUpdate)
            {
                _gameSettingData.gamename = gameInfo2.gamename + "_beta version";
            }
            else
            {
                _gameSettingData.gamename = gameInfo2.gamename;
            }
			
            _gameSettingData.showUpdateLog = EditorGUILayout.Toggle("Output update log : ", _gameSettingData.showUpdateLog);
            _developmentBuild = EditorGUILayout.Toggle("Development Build : ", _developmentBuild);
            _gameSettingData.loginTipText = EditorGUILayout.TextField("Login Prompt", _gameSettingData.loginTipText);


            //EditorGUILayout.Toggle("MinRes Build : ", _minResBuild);
            //_gameSettingData.logType = (GameSetting.DebugInfoType)EditorGUILayout.EnumPopup("调试信息类型 :", _gameSettingData.logType);
            _bundleVersionCode = EditorGUILayout.TextField("Custom bundleVersionCode", _bundleVersionCode);
            EditorGUILayout.Space();

            EditorGUILayout.Space();
            GUI.color = Color.yellow;
            if (GUILayout.Button("Export GameSettingData file", GUILayout.Height(40)))
            {
                if (!CheckIsCompiling())
                {
                    SaveComeFromConfig();
                }
            }

#if UNITY_ANDROID
            EditorGUILayout.Space();
            if (GUILayout.Button("Export Android Project", GUILayout.Height(40)))
            {
                string outPutPath = EditorUtility.OpenFolderPanel("Export Android project directory", "", "");
                if (!string.IsNullOrEmpty(outPutPath))
                {
                    EditorHelper.Run(() => ExportAndroidProject(outPutPath), true, false);
                }
            }

            EditorGUILayout.Space();
            if (GUILayout.Button("Packaging APK", GUILayout.Height(40)))
            {
                if (EditorUtility.DisplayDialog("Package Confirmation", "Are you sure to package APK?", "Confirm packaging", "Cancel"))
                {
                    EditorApplication.delayCall += () => BuildAndroid();
                }
            }
#endif

#if UNITY_IPHONE
            EditorGUILayout.Space();
            if (GUILayout.Button("Expor2XCODE", GUILayout.Height(40)))
            {
                if (EditorUtility.DisplayDialog("Export Confirmation", "Are you sure you want to export XCODE?", "Confirm", "Cancel"))
                {
                    EditorApplication.delayCall += () =>
                    {
                        string applicationPath = Application.dataPath.Replace("/Assets", "/../..");
                        string target_dir = EditorUtility.OpenFolderPanel("Export directory", applicationPath, "xcode");
                        BuildIOS(target_dir);
                    };
                }
            }

            //EditorGUILayout.Space ();
            //if (GUILayout.Button ("ExportAllIpa", GUILayout.Height (40))) {
            //     EditorApplication.delayCall += () => BuildAllIpa();
            //}
#endif

#if UNITY_STANDALONE_WIN && !UNITY_IPHONE
            EditorGUILayout.Space();
            if (GUILayout.Button("One button Build Win", GUILayout.Height(40)))
            {
                if (EditorUtility.DisplayDialog("Package Confirmation", "Are you sure to pack the Win version?", "Confirm packaging", "Cancel"))
                {
                    EditorApplication.delayCall += () => BuildPC();
                }
            }
#endif
            //EditorGUILayout.Space();
            //GUI.color = Color.green;
            //if (GUILayout.Button("清除本地数据和PlayerPrefs", GUILayout.Height(40)))
            //{
            //    EditorApplication.delayCall += () =>
            //    {
            //        Debug.Log("清除本地数据和PlayerPrefs");
            //        PlayerPrefs.DeleteAll();
            //        FileUtil.DeleteFileOrDirectory(Application.persistentDataPath);
            //    };
            //}
            //GUI.color = Color.white;

            //EditorGUILayout.Space();
            //if (GUILayout.Button("复制Dll", GUILayout.Height(40)))
            //{
            //    CopyDll();
            //}

            //EditorGUILayout.Space();
            //if (GUILayout.Button("一键打包", GUILayout.Height(40)))
            //{
            //    OneKeyBuild();
            //}
        }
        GUI.color = Color.white;

        EditorGUILayout.Space();
        EditorGUILayout.Space();
        EditorGUILayout.Space();
        
        oriClassTag = EditorGUILayout.TextField("C# Class name tag", oriClassTag);
        replaceClassTag = EditorGUILayout.TextField("Replace with", replaceClassTag);
        oriFuncTag = EditorGUILayout.TextField("C# Function tag", oriFuncTag);
        replaceFuncTag = EditorGUILayout.TextField("Replace with", replaceFuncTag);

        if (GUILayout.Button("Replace C# type and function name", GUILayout.Height(40)))
        {
            if (oriClassTag != "")
            {
                ReplaceCode(oriClassTag, replaceClassTag);
            }
            if (oriFuncTag != "")
            {
                ReplaceCode(oriFuncTag, replaceFuncTag);

            }
        }

        garbageCount = EditorGUILayout.TextField("Number of spam codes generated:", garbageCount);
        if (GUILayout.Button("Generate C# garbage code", GUILayout.Height(40)))
        {
            GenGarbageCode();
        }


        EditorGUILayout.EndScrollView();
    }

    private void GenGarbageCode()
    {
        string garbagePath = Application.dataPath + @"/Scripts/Other/";
        if (Directory.Exists(garbagePath))
        {
            var oldFiles = Directory.GetFiles(garbagePath, "*.*", SearchOption.AllDirectories);
            for (int i = 0; i < oldFiles.Length; i++)
            {
                var file = oldFiles[i];
                File.Delete(file);
            }
        }
        Directory.CreateDirectory(garbagePath);
        //string cmd = string.Format("{0} {1} -o {2} {3}", Application.dataPath + "../Luajit/luajit.exe", Application.dataPath + "../Tools/garbage_code_generater.lua", garbagePath, "-c 1");
        //var argList = ProcessHelper.CreateArgumentsContainer();
        //argList.Add(Application.dataPath + @"/../Luajit/luajit.exe");
        //argList.Add(Application.dataPath + @"/../Tools/GarbageCode/garbage_code_generater.lua");
        //argList.Add("-w");
        //argList.Add(Application.dataPath + @"/../Tools/GarbageCode/Word");
        //argList.Add("-o");
        //argList.Add(garbagePath);
        //argList.Add("-c");
        LuaState luaState = new LuaState();
        luaState.OpenLibs(LuaDLL.luaopen_protobuf_c);
        luaState.OpenLibs(LuaDLL.luaopen_lpeg);
        luaState.OpenLibs(LuaDLL.luaopen_bit);

        luaState.LuaGetField(LuaIndexes.LUA_REGISTRYINDEX, "_LOADED");
        luaState.OpenLibs(LuaDLL.luaopen_cjson);
        luaState.LuaSetField(-2, "cjson");
        luaState.LuaSetTop(0);
        LuaBinder.Bind(luaState);
        luaState.Start();
        try
        {
            var content = File.ReadAllText(Application.dataPath + @"/../Tools/GarbageCode/garbage_code_generater.lua");
            object[] array = luaState.DoString(content);
            LuaTable luaTable = array[0] as LuaTable;
            LuaFunction func = luaTable["GenerateCode"] as LuaFunction;
            string args = string.Format("-w {0} -o {1} -c {2}", Application.dataPath + @"/../Tools/GarbageCode/Word", garbagePath, garbageCount);
            func.BeginPCall();
            func.Push(args);
            func.PCall();
            func.EndPCall();
            func.Dispose();
        }
        catch (Exception e)
        {
            Debug.Log(e.ToString());
        }
        //argList.Add(garbageCount);
        //var p = ProcessHelper.Start(ProcessHelper.CreateStartInfo());
        //ProcessHelper.WriteLine(p, ProcessHelper.CreateArguments(argList));
        //ProcessHelper.WaitForExit(p);
        luaState.Dispose();
        UnityEditor.AssetDatabase.Refresh();
    }

    private void CopyDll()
    {
        string dllsRoot = null;
        if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.StandaloneWindows)
        {
            dllsRoot = EditorUtility.SaveFolderPanel("Select the dll directory", "BuildWin", "");
        }
        else if (EditorUserBuildSettings.activeBuildTarget == BuildTarget.Android)
        {
            dllsRoot = EditorUtility.SaveFolderPanel("Select the dll directory", "BuildAPK", "");
        }

        if (string.IsNullOrEmpty(dllsRoot))
        {
            Debug.LogError(string.Format("The path: {0} There is no dll to be copied!", dllsRoot));

            return;
        }

        string cdnResDir = string.Format("{0}/{1}", AssetBundleBuilder.GetExportPlatformPath(), CLASSTAG_GameResPath.DLL_FILE_ROOT);
        IOHelper.FUNCTAG_CopyDirectory(dllsRoot, cdnResDir);
        Debug.Log("UploadDllsPatch Finished");
    }


    private void BuildIOS(string target_dir, bool cmd = false)
    {
        if (string.IsNullOrEmpty(target_dir))
        {
            return;
        }
        MoveMovieToStreamingAsset();
        string target_name = string.Format("{0}_{1}_{2}", PlayerSettings.bundleVersion, _gameSettingData.gameType, _gameSettingData.updateMode.ToString());

        if (!Directory.Exists(target_dir))
        {
            Directory.CreateDirectory(target_dir);
        }

        string fullPath = target_dir + "/" + target_name;

        Debug.Log(target_dir + "/" + target_name);

        if (cmd || EditorUtility.DisplayDialog("Export confirmation", string.Format("Are you sure to export {0}?", fullPath), "OK", "Cancel"))
        {
            //var ipaFlag = cmd ? true : EditorUtility.DisplayDialog("导出ipa", "是否导出ipa？", "确认", "取消");
            var buildoption = BuildOptions.ShowBuiltPlayer;
            if (_developmentBuild)
            {
                buildoption = buildoption | BuildOptions.AllowDebugging | BuildOptions.ConnectWithProfiler |
                              BuildOptions.Development;
            }
#if UNITY_2017
            string res = BuildPipeline.BuildPlayer(FindEnabledEditorScenes(), target_dir + "/" + target_name, BuildTarget.iOS, buildoption);
            if (res.Length > 0)
            {
                throw new Exception("BuildPlayer failure: " + res);
            }
#endif
#if UNITY_2020_1_OR_NEWER
            BuildReport error = BuildPipeline.BuildPlayer(FindEnabledEditorScenes(), target_dir + "/" + target_name, BuildTarget.iOS, buildoption);
            BuildSummary summary = error.summary;

            if (summary.result == BuildResult.Succeeded)
            {
                Debug.Log("Build succeeded: " + summary.totalSize + " bytes " + DateTime.UtcNow);
            }

            if (summary.result == BuildResult.Failed)
            {
                //Debug.Log("Build failed: " + error.summary.result + " " + error);
                throw new Exception("BuildPlayer failed: " + error.summary.result + " " + error);
            }
#endif
            //if (ipaFlag)
            //{
            //XCodeToIpaPostProcess.OnPostProcessBuild(BuildTarget.iOS, target_dir + "/" + target_name);
            //}
        }
        MoveMovieToResources();
    }

    public static void BuildPCDLL(bool cmd = false)
    {
        string outputPath = string.Format("BuildDLL/{0}/game.exe", PlayerSettings.bundleVersion);
        string outputDir = Path.GetDirectoryName(outputPath);

        CLASSTAG_FileHelper.FUNCTAG_DeleteDirectory(outputDir, true);
        CLASSTAG_FileHelper.CreateDirectory(outputDir);
        var buildoption = BuildOptions.ShowBuiltPlayer;
#if UNITY_2017
        string res = BuildPipeline.BuildPlayer(FindEnabledEditorScenes(), outputPath, BuildTarget.StandaloneWindows, buildoption);
        if (res.Length > 0)
        {
            throw new Exception("BuildPlayer failure: " + res);
        }
#endif
#if UNITY_2020_1_OR_NEWER
        BuildReport error = BuildPipeline.BuildPlayer(FindEnabledEditorScenes(), outputPath, BuildTarget.StandaloneWindows, buildoption);
        BuildSummary summary = error.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes " + DateTime.UtcNow);
        }

        if (summary.result == BuildResult.Failed)
        {
            //Debug.Log("Build failed: " + error.summary.result + " " + error);
            throw new Exception("BuildPlayer failed: " + error.summary.result + " " + error);
        }
#endif
        CommonPostProcessBuild.GenerateWinDll(outputPath);

        string cdnResDir = string.Format("{0}/{1}", AssetBundleBuilder.GetExportPlatformPath(), CLASSTAG_GameResPath.DLL_FILE_ROOT);
        IOHelper.FUNCTAG_CopyDirectory(outputDir + "_Dll", cdnResDir);
        Debug.Log("Copy the Dll patch file -> " + cdnResDir);

    }
	private void CheckReplaceCode()
	{
		if (_gameSettingData.oriClassTag != "" && _gameSettingData.replaceClassTag != "")
		{
			ReplaceCode(_gameSettingData.oriClassTag, _gameSettingData.replaceClassTag);
		}
		if (_gameSettingData.oriFuncTag != "" && _gameSettingData.replaceFuncTag != "")
		{
			ReplaceCode(_gameSettingData.oriFuncTag, _gameSettingData.replaceFuncTag);

		}
	}
	static public void ReplaceCode(string oriTag, string replaceTag)
	{
		string[] codePaths =
			{
				Application.dataPath + @"/Plugins",
				Application.dataPath + @"/Editor",
				Application.dataPath + @"/Scripts",
				Application.dataPath + @"/Standard Assets",
			};

		foreach (var path in codePaths)
		{
			var files = System.IO.Directory.GetFiles(path, "*.cs", SearchOption.AllDirectories);

			for (int i = 0; i < files.Length; i++)
			{
				var file = files[i];
				string content = File.ReadAllText(file);
				File.WriteAllText(file, content.Replace(oriTag, replaceTag));
				EditorUtility.DisplayProgressBar("Replacing" + path, string.Format(" {0} / {1} ", i,files.Length),
						i / (float)files.Length);
			}
		}
		EditorUtility.ClearProgressBar();
	}
    private void BuildPC(bool cmd = false)
    {
        var exportExe = cmd ? true : EditorUtility.DisplayDialog("Export Confirmation", "Export Export", "OK", "Cancel");
        if (!CheckIsCompiling())
        {
            SaveComeFromConfig();
        }

        string app_name = string.Format("{0}_{1}_{2}", PlayerSettings.bundleVersion, _gameSettingData.gameType, _gameSettingData.updateMode.ToString());
        string outputPath = string.Format("BuildWin/{0}/game.exe", app_name);
        string outputDir = Path.GetDirectoryName(outputPath);

        CLASSTAG_FileHelper.FUNCTAG_DeleteDirectory(outputDir, true);
        CLASSTAG_FileHelper.CreateDirectory(outputDir);
        var buildoption = BuildOptions.ShowBuiltPlayer;
        if (_developmentBuild)
        {
            buildoption = buildoption | BuildOptions.AllowDebugging | BuildOptions.ConnectWithProfiler |
                          BuildOptions.Development;
        }
#if UNITY_2017
        string res = BuildPipeline.BuildPlayer(FindEnabledEditorScenes(), outputPath, BuildTarget.StandaloneWindows, buildoption);
        if (res.Length > 0)
        {
            throw new Exception("BuildPlayer failure: " + res);
        }
#endif
#if UNITY_2020_1_OR_NEWER
        BuildReport error = BuildPipeline.BuildPlayer(FindEnabledEditorScenes(), outputPath, BuildTarget.StandaloneWindows, buildoption);
        BuildSummary summary = error.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes " + DateTime.UtcNow);
        }

        if (summary.result == BuildResult.Failed)
        {
            //Debug.Log("Build failed: " + error.summary.result + " " + error);
            throw new Exception("BuildPlayer failed: " + error.summary.result + " " + error);
        }
#endif
        CommonPostProcessBuild.GenerateWinDll(outputPath);

        if (exportExe)
        {
            CommonPostProcessBuild.GenerateWinExe(BuildTarget.StandaloneWindows, outputPath);
        }
    }
	


    private void BuildAndroid(bool cmd = false)
    {
        if (!CheckIsCompiling())
        {
            //var isExportDll = cmd ? true : EditorUtility.DisplayDialog("导出dll", "是否需要对dll处理？", "确认", "取消");
            var isExportDll = true;
            SaveComeFromConfig();
            MoveMovieToStreamingAsset();
            BulidTargetApk(_gameSettingData.channel, isExportDll);
            MoveMovieToResources();
        }
    }

    public static void BuildAndroidDLL(bool cmd = false)
    {
        /*
        PlayerSettings.Android.keystoreName = "PublishKey/nucleus.keystore";
        PlayerSettings.keystorePass = "nucleus123";
        PlayerSettings.Android.keyaliasName = "nucleus";
        PlayerSettings.Android.keyaliasPass = "nucleus123";


        string channel = "build";
        BuildTarget buildTarget = BuildTarget.Android;


        string target_name = string.Format("BuildDLL/{0}/build.apk", PlayerSettings.bundleVersion);
        string outputDir = Path.GetDirectoryName(target_name);


        CLASSTAG_FileHelper.FUNCTAG_DeleteDirectory(outputDir, true);
        CLASSTAG_FileHelper.CreateDirectory(outputDir);


        string[] scenes = FindEnabledEditorScenes();

        //开始Build场景，等待吧～
        BuildOptions buildOption = BuildOptions.ShowBuiltPlayer;

        EditorUserBuildSettings.SwitchActiveBuildTarget(buildTarget);
        //string error = BuildPipeline.BuildPlayer(scenes, target_name, buildTarget, buildOption);

        //if (error.Length > 0)
        //{
        //    throw new Exception("BuildPlayer failure: " + error);
        //}

        BuildReport error = BuildPipeline.BuildPlayer(scenes, target_name, buildTarget, buildOption);
        BuildSummary summary = error.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes " + DateTime.UtcNow);
        }

        if (summary.result == BuildResult.Failed)
        {
            //Debug.Log("Build failed: " + error.summary.result + " " + error);
            throw new Exception("BuildPlayer failed: " + error.summary.result + " " + error);
        }

        CommonPostProcessBuild.GenerateAndroidDll(target_name);

        string cdnResDir = string.Format("{0}/{1}", AssetBundleBuilder.GetExportPlatformPath(), CLASSTAG_GameResPath.DLL_FILE_ROOT);
        IOHelper.FUNCTAG_CopyDirectory(outputDir + "/build_Dll", cdnResDir);
        Debug.Log("Copy the Dll patch file -> " + cdnResDir);
        */

    }

    private bool CheckIsCompiling()
    {
        if (EditorApplication.isCompiling)
        {

            EditorUtility.DisplayDialog("Tip:",
                "please wait EditorApplication Compiling",
                "OK"
            );
            return true;

        }

        return false;
    }

    //保存渠道配置
    private void SaveComeFromConfig()
    {
        // 保存后做特定的回调
        ProjectCallback.TriggerAfterPlayerSettingToolSave();

        //修改游戏签名
        ChangeKeystorePass();

        //修改版本号
        ChangeBundleVersion();

        //修改游戏IconAndSplash
        //ChangeIconAndSplash(_gameSettingData.configSuffix);

        //修改游戏标签
        ChangeBundleIdentifier(_gameSettingData.channel);

        //更新打包文件名
        UpdateBuildToFileName();

        //改变需要的宏
        ChangDefineSymbols(_gameSettingData.channel);

        //修改产品名
        ChangeProductName(_gameSettingData.channel);

        //修改BuildeSettings
        ChangBuildSettings();

        //修改打包projmods
        ChangeProjmods(_gameSettingData.channel);

        // 修改打包SDK版本
        ChangeIOSTargetOSVersion();


		SaveGameSettingData();


    }

    private void ChangeKeystorePass()
    {
#if UNITY_EDITOR && UNITY_ANDROID
        PlayerSettings.Android.keystoreName = "PublishKey/nucleus.keystore";
        PlayerSettings.keystorePass = "nucleus123";

        PlayerSettings.Android.keyaliasName = "nucleus";
        PlayerSettings.Android.keyaliasPass = "nucleus123";
#endif
    }

    private void ChangeBundleVersion()
    {
        PlayerSettings.bundleVersion = string.Format("{0}.{1}.{2}", CLASSTAG_GameVersion.frameworkVersion, CLASSTAG_GameVersion.dllVersion, GetResVersion());
		if (!string.IsNullOrEmpty(_bundleVersionCode))
		{
#if UNITY_ANDROID
        PlayerSettings.Android.bundleVersionCode = int.Parse(_bundleVersionCode);
#elif UNITY_IPHONE
        PlayerSettings.iOS.buildNumber = _bundleVersionCode;
#endif
		}
		else
		{
#if UNITY_ANDROID
        PlayerSettings.Android.bundleVersionCode = GetSvnVersion();
#elif UNITY_IPHONE
        PlayerSettings.iOS.buildNumber = GetSvnVersion().ToString();
#endif
		}


        _gameSettingData.frameVersion = CLASSTAG_GameVersion.frameworkVersion;
        _gameSettingData.dllVersion = CLASSTAG_GameVersion.dllVersion;
        _gameSettingData.resVersion = GetResVersion();
        _gameSettingData.svnVersion = GetSvnVersion();
    }

    private bool IsMinResBuild()
    {
        string configPath = Path.Combine(Application.streamingAssetsPath, CLASSTAG_GameResPath.MINIRESCONFIG_FILE);
        if (CLASSTAG_FileHelper.IsExist(configPath))
        {
            return true;
        }
        return false;
    }


    private void ChangDefineSymbols(string channel)
    {
        List<string> defineSymbolsList = new List<string>();

        string defineSymbols = string.Empty;

        string spDefineSymbol = _channelConfig.GetChannelSymbol(channel);
        if (!string.IsNullOrEmpty(spDefineSymbol))
        {
            defineSymbolsList.Add(spDefineSymbol);
        }

        defineSymbols = string.Join(";", defineSymbolsList.ToArray());

#if UNITY_IPHONE
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.iOS, defineSymbols);
#elif UNITY_ANDROID
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Android, defineSymbols);
#else
        PlayerSettings.SetScriptingDefineSymbolsForGroup(BuildTargetGroup.Standalone, defineSymbols);
#endif
    }

    private void ChangeProjmods(string channel)
    {
        _buildProjmods = _channelConfig.GetChannelProjmods(channel);
        EditorPrefs.SetString("selectProjmods", _buildProjmods);
    }

    private void ChangeIOSTargetOSVersion()
    {
        //		// 仅针对ios渠道
        //		if (_gameSettingData.platformType != GameSetting.PlatformType.ROOTIOS &&
        //		    _gameSettingData.platformType != GameSetting.PlatformType.IOS)
        //		{
        //			return;
        //		}

        var modFolder = Application.dataPath.Replace("/Assets", "/Mods");
        var modName = _channelConfig.GetChannelProjmods(_gameSettingData.channel);
        var modPath = string.Format("{0}/{1}", modFolder, modName);
        if (string.IsNullOrEmpty(modName) || !File.Exists(modPath))
        {
            return;
        }

        //var mod = new XCMod(modPath);
        //PlayerSettings.iOS.targetOSVersion = mod.IOSTargetOSVersion >= iOSTargetOSVersion.iOS_6_0
        //    ? mod.IOSTargetOSVersion : iOSTargetOSVersion.iOS_6_0;
    }

    private void ChangeProductName(string channel)
    {
        PlayerSettings.companyName = "DUO";
        //string suffix = GetDomainAliasName(_gameSettingData.domainType);
        PlayerSettings.productName = _gameSettingData.gamename;
    }

    private void ChangeBundleIdentifier(string channel)
    {
        string bundleId = _channelConfig.GetChannelBundleId(channel);
        string bundleIdentifier = "";
        if (bundleId.Contains("{0}"))
        {
            bundleIdentifier = string.Format(bundleId, _gameSettingData.domainType.ToLower());
        }
        else
        {
            bundleIdentifier = bundleId;
        }

        bundleIdentifier = string.Format("{0}.{1}.{2}", bundleIdentifier, _gameSettingData.domainType.ToLower(), _gameSettingData.updateMode.ToString().ToLower());

        //如果是正式包， 则去掉后面的域定义
        bundleIdentifier = bundleIdentifier.Replace(".release", "");
        bundleIdentifier = bundleIdentifier.Replace(".update", "");

        PlayerSettings.applicationIdentifier = bundleIdentifier;
    }

    private void UpdateBuildToFileName()
    {
#if UNITY_ANDROID
        _buildToFileName = GetBuildTargetDir() + "/" + GetBuildTargetFileName();
#endif
    }

    //private static string IconAndSplashRoot = "Assets/IconAndSplash";

    private void ChangeIconAndSplash(string name)
    {
        string IconAndSplashRoot = "Assets/IconAndSplash/default";
        string path = string.Format("{0}/IconAndSplash/{1}", Application.dataPath, name);
        string respath = string.Format("{0}/StreamingAssets/Textures", Application.dataPath);
        if (Directory.Exists(path))
        {
            IconAndSplashRoot = "Assets/IconAndSplash/" + name;
        }

        //修改默认ICON
        Texture2D defaultIcon = AssetDatabase.LoadMainAssetAtPath(IconAndSplashRoot + "/icon.png") as Texture2D;
        if (defaultIcon != null)
        {
            PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, new Texture2D[1] { defaultIcon });
        }
        else
        {
            Debug.Log("Set Defaut ICON Error!!!");
        }

        string loadingPath = IconAndSplashRoot + "/loginBG.png";
        string dstloadingPath = respath + "/loginBG";
        if (File.Exists(loadingPath))
        {
            File.Copy(loadingPath, dstloadingPath, true);
        }
        else
        {
            Debug.LogError("Resource not found " + loadingPath);
        }

        string logoPath = IconAndSplashRoot + "/logo.png";
        string dstllogoPath = respath + "/logo";
        if (File.Exists(logoPath))
        {
            File.Copy(logoPath, dstllogoPath, true);
        }
        else
        {
            Debug.LogError("找不到资源 " + logoPath);
        }

        AssetDatabase.Refresh();

        //设置开始界面
        //        if (File.Exists(splashPath))
        //        {
        //            File.Delete(splashPath);
        //        }
        //
        //        File.Copy(platformIconPath + splash, splashPath);



        //#if UNITY_ANDROID
        //        String iconUrl = IconAndSplashRoot + "/Icon/Android/{0}x{0}.png";
        //        Texture2D icon_192 = AssetDatabase.LoadMainAssetAtPath(string.Format(iconUrl, 192)) as Texture2D;
        //        Texture2D icon_144 = AssetDatabase.LoadMainAssetAtPath(string.Format(iconUrl, 144)) as Texture2D;
        //        Texture2D icon_96 = AssetDatabase.LoadMainAssetAtPath(string.Format(iconUrl, 96)) as Texture2D;
        //        Texture2D icon_72 = AssetDatabase.LoadMainAssetAtPath(string.Format(iconUrl, 72)) as Texture2D;
        //        Texture2D icon_48 = AssetDatabase.LoadMainAssetAtPath(string.Format(iconUrl, 48)) as Texture2D;
        //        Texture2D icon_36 = AssetDatabase.LoadMainAssetAtPath(string.Format(iconUrl, 36)) as Texture2D;

        //        PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Android, new Texture2D[6] {
        //            icon_192,
        //            icon_144,
        //            icon_96,
        //            icon_72,
        //            icon_48,
        //            icon_36
        //        });
        //#elif UNITY_IPHONE
        //        //修改平台ICON
        //        String iconUrl = IconAndSplashRoot + "/Icon/iOS/{0}x{0}.png";
        //        Texture2D icon_180 = AssetDatabase.LoadMainAssetAtPath (string.Format (iconUrl, 180)) as Texture2D;
        //        Texture2D icon_152 = AssetDatabase.LoadMainAssetAtPath (string.Format (iconUrl, 152)) as Texture2D;
        //        Texture2D icon_144 = AssetDatabase.LoadMainAssetAtPath (string.Format (iconUrl, 144)) as Texture2D;
        //        Texture2D icon_120 = AssetDatabase.LoadMainAssetAtPath (string.Format (iconUrl, 120)) as Texture2D;
        //        Texture2D icon_114 = AssetDatabase.LoadMainAssetAtPath (string.Format (iconUrl, 114)) as Texture2D;
        //        Texture2D icon_76 = AssetDatabase.LoadMainAssetAtPath (string.Format (iconUrl, 76)) as Texture2D;
        //        Texture2D icon_72 = AssetDatabase.LoadMainAssetAtPath (string.Format (iconUrl, 72)) as Texture2D;
        //        Texture2D icon_57 = AssetDatabase.LoadMainAssetAtPath (string.Format (iconUrl, 57)) as Texture2D;

        //        PlayerSettings.SetIconsForTargetGroup (BuildTargetGroup.iOS, new Texture2D[8] {
        //            icon_180,
        //            icon_152,
        //            icon_144,
        //            icon_120,
        //            icon_114,
        //            icon_76,
        //            icon_72,
        //            icon_57
        //        });
        //#endif

        /*
		 * 
    320x480 pixels for 1–3rd gen devices
    1024x768 for iPad mini/iPad 1st/2nd gen
    2048x1536 for iPad 3th/4th gen
    640x960 for 4th gen iPhone / iPod devices
    640x1136 for 5th gen devices
		 */

        //设置开始界面
        //        if (File.Exists(splashPath))
        //        {
        //            File.Delete(splashPath);
        //        }
        //
        //        File.Copy(platformIconPath + splash, splashPath);
    }

    private void ChangBuildSettings()
    {
        EditorBuildSettingsScene[] original = EditorBuildSettings.scenes;
        EditorBuildSettingsScene[] newSettings = new EditorBuildSettingsScene[original.Length];
        System.Array.Copy(original, newSettings, original.Length);

        //if (_resLoadMode == AssetPipeline.AssetUpdate.LoadMode.EditorLocal)
        //{
        //    foreach (EditorBuildSettingsScene scene in newSettings)
        //    {
        //        scene.enabled = true;
        //    }
        //}
        //else
        //{
        //打包资源模式下，禁用掉所有游戏场景，只保留入口场景
        foreach (EditorBuildSettingsScene scene in newSettings)
        {
            if (scene.path == "Assets/Scenes/main.unity")
            {
                scene.enabled = true;
            }
            else
            {
                scene.enabled = false;
            }
        }
        //}
        EditorBuildSettings.scenes = newSettings;
    }

#region 批量打包渠道

    /// <summary>
    /// 导出Android工程
    /// </summary>
    public static void ExportAndroidProject(string path)
    {
        if (!Directory.Exists(path))
        {
            throw new Exception("No path：" + path);
        }

        var buildOption = BuildOptions.AcceptExternalModificationsToPlayer |
                          BuildOptions.ShowBuiltPlayer;
        BuildPipeline.BuildPlayer(FindEnabledEditorScenes(), path, BuildTarget.Android,
            buildOption);

    }

    private string GetBuildTargetDir()
    {
        string applicationPath = Application.dataPath.Replace("/Assets", "");
        string target_dir = string.Format(applicationPath + "/BuildAPK/{0}_{1}_{2}", PlayerSettings.bundleVersion,  GetSvnVersion(),  _gameSettingData.gameType.ToString());
        return target_dir;
    }

    private string GetBuildTargetFileName()
    {
        string app_name = string.Format("{0}_{1}_{2}_{3}_{4}", PlayerSettings.bundleVersion, GetSvnVersion(), _gameSettingData.gameType.ToString(), _gameSettingData.domainType.ToString(), _gameSettingData.updateMode.ToString().ToLower());

        app_name = app_name.Replace("_Release", "");

        //if (!_gameSettingData.release)
        //{
        //    app_name += "_debug";
        //}

        if (_developmentBuild)
        {
            app_name += "_dev";
        }

        //if (_enableJSB)
        //{
        //    app_name += "_JSB";
        //}

        if (_minResBuild)
        {
            app_name += "_min";
        }

        return app_name + ".apk";
    }

    //这里封装了一个简单的通用方法。
    private void BulidTargetApk(string channel, bool isExportDll)
    {
        BuildTarget buildTarget = BuildTarget.Android;

        string target_dir = GetBuildTargetDir();
        string target_name = GetBuildTargetFileName();

        //if (Directory.Exists(target_dir))
        //{
        //	Directory.Delete(target_dir, true);
        //}
        Debug.Log(target_name);

        ShowNotification(new GUIContent("Packing:" + target_name));

        //每次build删除之前的残留
        if (Directory.Exists(target_dir))
        {
            if (File.Exists(target_name))
            {
                File.Delete(target_name);
            }
        }
        else
        {
            Directory.CreateDirectory(target_dir);
        }

        string[] scenes = FindEnabledEditorScenes();

        //开始Build场景，等待吧～
        BuildOptions buildOption = BuildOptions.ShowBuiltPlayer;
        if (_developmentBuild)
        {
            buildOption = BuildOptions.ShowBuiltPlayer | BuildOptions.Development | BuildOptions.ConnectWithProfiler;
        }

        GenericBuild(scenes, target_dir + "/" + target_name, buildTarget, buildOption);
        if (isExportDll)
        {
            CommonPostProcessBuild.GenerateAndroidDll(target_dir + "/" + target_name);
        }
    }

    private string GetBuildDateTime()
    {
        DateTime dateTtime = DateTime.UtcNow.ToLocalTime();
        string str = string.Format("{0:D2}{1:D2}{2:D2}_{3:D2}{4:D2}", dateTtime.Year, dateTtime.Month, dateTtime.Day, dateTtime.Hour, dateTtime.Minute);
        return str;
    }

    public static string[] FindEnabledEditorScenes()
    {
        List<string> EditorScenes = new List<string>();
        foreach (EditorBuildSettingsScene scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled)
                continue;
            EditorScenes.Add(scene.path);
        }
        return EditorScenes.ToArray();
    }

    private void GenericBuild(string[] scenes, string targetPath, BuildTarget build_target, BuildOptions build_options)
    {
        EditorUserBuildSettings.SwitchActiveBuildTarget(build_target);
#if UNITY_2017
        string error = BuildPipeline.BuildPlayer(scenes, targetPath, build_target, build_options);

        if (error.Length > 0)
        {
            throw new Exception("BuildPlayer failure: " + error);
        }
#endif
#if UNITY_2020_1_OR_NEWER
        BuildReport error = BuildPipeline.BuildPlayer(scenes, targetPath, build_target, build_options);
        BuildSummary summary = error.summary;

        if (summary.result == BuildResult.Succeeded)
        {
            Debug.Log("Build succeeded: " + summary.totalSize + " bytes " + DateTime.UtcNow);
        }

        if (summary.result == BuildResult.Failed)
        {
            //Debug.Log("Build failed: " + error.summary.result + " " + error);
            throw new Exception("BuildPlayer failed: " + error.summary.result + " " + error);
        }
#endif
    }

    static public void MoveMovieToStreamingAsset()
    {
        var srcDir = Application.dataPath + "/Resources/Movies/";
        var dstDir = Application.streamingAssetsPath + "/Movies/";
        if (Directory.Exists(srcDir))
        {
            if (Directory.Exists(dstDir))
                Directory.Delete(dstDir, true);
            Directory.Move(srcDir, dstDir);
            AssetDatabase.Refresh();
        }

    }
    static public void MoveMovieToResources()
    {
        var srcDir = Application.streamingAssetsPath + "/Movies/";
        var dstDir = Application.dataPath + "/Resources/Movies/";
        if (Directory.Exists(srcDir))
        {
            if (Directory.Exists(dstDir))
                Directory.Delete(dstDir, true);
            Directory.Move(srcDir, dstDir);
            AssetDatabase.Refresh();
        }
    }
    

    public static int GetResVersion()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "resConfig.jz");
        byte[] bytes = CLASSTAG_FileHelper.ReadAllBytes(path);
        if (bytes == null)
        {
            Debug.LogError(string.Format("Load url:{0}\nerror", path));
        }
        CLASSTAG_ResConfig resConfig = CLASSTAG_ResConfig.FUNCTAG_ReadFile(bytes, true);
        return resConfig.Version;
    }

    public static int GetSvnVersion()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "resConfig.jz");
        byte[] bytes = CLASSTAG_FileHelper.ReadAllBytes(path);
        if (bytes == null)
        {
            Debug.LogError(string.Format("Load url:{0}\nerror", path));
        }
        CLASSTAG_ResConfig resConfig = CLASSTAG_ResConfig.FUNCTAG_ReadFile(bytes, true);

        return resConfig.svnVersion;
    }


    //private void BuildAllIpa()
    //{
    //    if (EditorUtility.DisplayDialog("打包全部Ipa", "先保存到任意一个渠道环境！\n你准备好卡死了么!", "确定", "取消"))
    //    {
    //        string applicationPath = Application.dataPath.Replace("/Assets", "/../..");
    //        string target_dir = EditorUtility.OpenFolderPanel("导出目录", applicationPath, "xcode");
    //        if (string.IsNullOrEmpty(target_dir))
    //        {
    //            return;
    //        }

    //        foreach (var channel in _channelValues)
    //        {
    //            _gameSettingData.channel = channel;
    //            SaveComeFromConfig();

    //            string target_name = string.Format("H1_{0}_{1}_{2}", GameVersion.BundleVersion, _gameSettingData.channel, _gameSettingData.domainType.ToString());

    //            if (!Directory.Exists(target_dir))
    //            {
    //                Directory.CreateDirectory(target_dir);
    //            }

    //            string fullPath = target_dir + "/" + target_name;

    //            Debug.Log(fullPath);

    //            string res = BuildPipeline.BuildPlayer(FindEnabledEditorScenes(), fullPath, BuildTarget.iOS, BuildOptions.ShowBuiltPlayer);
    //            if (res.Length > 0)
    //            {
    //                throw new Exception("BuildPlayer failure: " + res);
    //            }
    //            //				break;
    //        }
    //    }
    //}

#endregion
}

//打包域信息
public class GameInfo
{
    //游戏ID
    //public string gametype;
    //游戏名
    public string gamename;
    //配置标示识别
    public string configSuffix;
    //打包域信息
    public List<DomainInfo> domains;

    public List<SPChannel> channels;
}

//打包域信息
public class DomainInfo
{
    //域名字,打包放置目录也是它
    public string type;
    //正式域名地址
    public string httproot;
    //测试域名地址
    public string testhttproot;
    //资源加载啊目录，结合CDN路径使用
    //public string resdir;
    //打包识别
    //public string bundleId;

    //public string bundleName;
}

public class SPChannel
{
    //{"name":"kuaiyong","alias":"快用苹果助手","bundleId":"com.tiancity.xlsj.ky","projmods":"","symbol":""},
    public string name;
    public string alias;
    public string bundleId;
    public string projmods;
    public string symbol;
    //TESTIN  APPSTORE
    public string platform;
    //Android = 1,ROOTIOS = 2,IOS = 3
    public string domains;
    //LocalDev,内开发    LocalTest,内测试   Release,正式服   BetaTest,永测服    Business,商务服
}