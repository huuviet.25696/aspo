using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

[CustomEditor(typeof(AnimEffect))]
public class AnimEffectEditor : Editor
{
	public AnimEffect animEffect;
    public override void OnInspectorGUI()
    {
		animEffect = target as AnimEffect;

		animEffect.animName = EditorGUILayout.TextField("动作名", animEffect.animName);
		int len = EditorGUILayout.IntField("特效数量", animEffect.EffectLength);
		if (animEffect.EffectLength != len)
		{
			animEffect.EffectLength = len;
		}
		ShowEffectInfo();
    }
	private void ShowEffectInfo()
	{
		for (int i = 0; i < animEffect.EffectLength; i++)
		{
			AnimEffectInfo effectInfo = animEffect.infoArray[i];
			EditorGUILayout.BeginHorizontal();
			EditorGUIUtility.labelWidth = 48f;
			effectInfo.gameObject = (GameObject)EditorGUILayout.ObjectField("挂载点", effectInfo.gameObject, typeof(GameObject), true, GUILayout.Width(120f));
			EditorGUIUtility.labelWidth = 32f;
			var path = EditorGUILayout.TextField("路径", effectInfo.path, GUILayout.Width(140f));
			if (GUILayout.Button("选择", GUILayout.Width(32f)))
			{
				var tempPath = EditorUtility.OpenFilePanel("选择特效", Application.dataPath+"/GameRes/Effect/Anim/", "prefab");
				if (tempPath.Length != 0)
				{
					path = tempPath.Replace(Application.dataPath + "/GameRes/", "");
					effectInfo.path = path;
				}
			}
			effectInfo.path = path;
			EditorGUILayout.EndHorizontal();
			animEffect.infoArray[i] = effectInfo;
		}
	}
}
