// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// 雕像shader
// 支持边缘光、不支持Lightmap、支持阴影

Shader "<<Haymaker>>/MobileUnlit/Statue"
{
    Properties
    {
        _Color("Main Color", Color) = (1,1,1,1)
        _MainTex("Base (RGB)", 2D) = "white" {}
        _DetailTex("Detail (RGB)", 2D) = "white" {}
        _ContrastBegin("Contrast Begin", float) = 0
        _ContrastEnd("Contrast End", float) = 0
        _RimPower("Rim Power", float) = 2
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" "LightMode" = "ForwardBase" }
        LOD 100

        Pass
        {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma multi_compile_fwdbase

                #include "UnityCG.cginc"
                #include "AutoLight.cginc"

                struct appdata_t {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                    float3 normal : NORMAL;
                };

                struct v2f {
                    float4 pos : SV_POSITION;
                    half2 uv1 : TEXCOORD0;
                    half2 uv2 : TEXCOORD1;
                    float rim : TEXCOORD2;
                    LIGHTING_COORDS(3, 4)
                };

                sampler2D _MainTex;
                float4 _MainTex_ST;

                sampler2D _DetailTex;
                float4 _DetailTex_ST;

                float4 _Color;
                float _RimPower;

                float _ContrastBegin;
                float _ContrastEnd;

                v2f vert(appdata_t v)
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.uv1 = TRANSFORM_TEX(v.uv, _MainTex);
                    o.uv2 = TRANSFORM_TEX(v.uv, _DetailTex);

                    float3 viewDir = ObjSpaceViewDir(v.vertex);
                    float rim = saturate(dot(normalize(viewDir), normalize(v.normal)));
                    o.rim = pow(rim, _RimPower);

                    TRANSFER_VERTEX_TO_FRAGMENT(o)
                    return o;
                }

                fixed4 frag(v2f i) : SV_Target
                {
                    half4 col = tex2D(_MainTex, i.uv1) * _Color;
                    half4 detailCol = tex2D(_DetailTex, i.uv2);

                    half illum = col.r * 0.299 + col.g * 0.587 + col.b * 0.114;
                    half contrast = lerp(_ContrastBegin, _ContrastEnd, illum);
                    col.rgb = half3(contrast, contrast, contrast);
                    col.a = 1;

                    col.rgb += detailCol.rgb;
                    
                    fixed atten = LIGHT_ATTENUATION(i);
                    return col * atten * i.rim;
                }
            ENDCG
        }
    }
    Fallback "VertexLit"
}
