Shader "<<Haymaker>>/Character/Bumped Specular Gloss No Normalmap-Grey" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_SpecColor ("Specular Color", Color) = (0.5, 0.5, 0.5, 1)
	_Shininess ("Shininess", Range (0.03, 1)) = 0.078125
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_GlossTex ("Gloss (RGB)", 2D) = "black" {}
	
	HitColor ("Hit Color", Color) = (1.0,1.0,1.0,0.0)
	[HideInInspector]
    _RimPower ("Power", Range(0.5,8.0)) = 8.0
}
SubShader { 
	Tags { "RenderType"="Opaque" "IgnoreProjector"="True"}
	LOD 400
	
CGPROGRAM
#pragma surface surf BlinnPhong


sampler2D _MainTex;
sampler2D _GlossTex;
fixed4 _Color;
half _Shininess;

float4 HitColor;
float _RimPower;

struct Input {
	float2 uv_MainTex;
	float3 viewDir;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 tex = tex2D(_MainTex, IN.uv_MainTex);
	o.Albedo = dot(tex.rgb * _Color.rgb, half3(.222, .707, .071));
	o.Gloss = tex2D(_GlossTex, IN.uv_MainTex).r;
	o.Alpha = 1.0;
	o.Specular = _Shininess;
	o.Normal = fixed4(0.0,0.0,1.0,1.0);
	if(_RimPower < 7.0)
	{
		half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
		o.Emission = HitColor.rgb * pow (rim, _RimPower);
	}
}
ENDCG
}

FallBack "<<Haymaker>>/Character/Diffuse-Grey"
}
