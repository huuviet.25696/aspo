// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Self-Illumin/Character Special" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
}
SubShader
	{
        Tags { "Queue" = "Geometry+500"  "RenderType"="Opaque" "IgnoreProjector"="True"}
		LOD 200
		
		Pass
		{
			ZTest Greater
			ZWrite Off
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile LIGHTMAP_ON LIGHTMAP_OFF

			#include "UnityCG.cginc"

			struct v2f
			{
				half4 pos   : SV_POSITION;
			};

			half4 _MainTex_ST;
			sampler2D _MainTex;
			
			half4 _Color;

			v2f vert (appdata_full v) 
			{
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				return o;
			}

			half4 frag (v2f i) : COLOR0 
			{
                return half4(0.24, 0.26, 0.28, 1.0);
			}
			ENDCG
		}

		Pass
		{
			ZTest LEqual
			ZWrite ON
			
			Stencil {
                Ref 2
                Comp always
                Pass replace
            }
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#pragma multi_compile LIGHTMAP_ON LIGHTMAP_OFF

			#include "UnityCG.cginc"

			struct v2f
			{
				half4 pos   : SV_POSITION;
				half2 uv    : TEXCOORD0;
			};

			struct v2f_full
			{
				half4 pos   : SV_POSITION;
				half2 uv    : TEXCOORD0;
			};
			
			half4 _MainTex_ST;
			sampler2D _MainTex;
			
			half4 _Color;

			v2f vert (appdata_full v) 
			{
				v2f o;
				o.pos = UnityObjectToClipPos (v.vertex);
				o.uv = TRANSFORM_TEX (v.texcoord, _MainTex);
				return o;
			}

			half4 frag (v2f i) : COLOR0 
			{
				fixed4 tex = tex2D (_MainTex, i.uv);
                fixed4 _res = half4(tex * (_Color * _Color.a * 2.0));
				return _res;
			}
			ENDCG
		}
	}
	FallBack "Self-Illumin/VertexLit"
}

