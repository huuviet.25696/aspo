// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "<<Haymaker>>/MobileUnlit/HighRimAlpha"
{
    Properties
    {
        _Color("Main Color", Color) = (1,1,1,1)
        _MainTex("Base (RGB)", 2D) = "white" {}
        _ContrastBegin("Contrast Begin", float) = 0.07
        _ContrastEnd("Contrast End", float) = 0.8
        _RimPower("Rim Power", float) = 1
        _RimMulti("Rim Multi", float) = 1
        _AlphaToon("Alpha Toon", float) = 1
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" "LightMode" = "ForwardBase" }
        LOD 100
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
                #pragma multi_compile_fwdbase

                #include "UnityCG.cginc"
                #include "AutoLight.cginc"

                struct appdata_t {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                    float3 normal : NORMAL;
                };

                struct v2f {
                    float4 pos : SV_POSITION;
                    half2 uv1 : TEXCOORD0;
                    float rim : TEXCOORD1;
                    LIGHTING_COORDS(2, 3)
                };

                sampler2D _MainTex;
                float4 _MainTex_ST;

                float4 _Color;
                float _RimPower;
                float _RimMulti;
                float _AlphaToon;

                float _ContrastBegin;
                float _ContrastEnd;

                v2f vert(appdata_t v)
                {
                    v2f o;
                    o.pos = UnityObjectToClipPos(v.vertex);
                    o.uv1 = TRANSFORM_TEX(v.uv, _MainTex);

                    float3 viewDir = ObjSpaceViewDir(v.vertex);
                    float rim = 1 - saturate(dot(normalize(viewDir), normalize(v.normal)));
                    o.rim = pow(rim, _RimPower) * _RimMulti;

                    TRANSFER_VERTEX_TO_FRAGMENT(o)
                    return o;
                }

                fixed4 frag(v2f i) : SV_Target
                {
                    half4 col = tex2D(_MainTex, i.uv1) * _Color;

                    half illum = col.r * 0.299 + col.g * 0.587 + col.b * 0.114;
                    half contrast = lerp(_ContrastBegin, _ContrastEnd, illum);
                    col.a = i.rim * _AlphaToon;

                    fixed atten = LIGHT_ATTENUATION(i);
                    return col * atten + float4(i.rim, i.rim, i.rim, 0);
                }
            ENDCG
        }
    }
    Fallback "VertexLit"
}
