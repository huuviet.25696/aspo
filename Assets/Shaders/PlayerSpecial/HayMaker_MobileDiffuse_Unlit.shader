// Upgrade NOTE: commented out 'float4 unity_LightmapST', a built-in variable
// Upgrade NOTE: commented out 'sampler2D unity_Lightmap', a built-in variable
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
// Upgrade NOTE: replaced tex2D unity_Lightmap with UNITY_SAMPLE_TEX2D

// 不支持光照，支持Color Tint
// 支持应用Lightmap（不支持烘焙Lightmap，烘焙时需要替换回普通的Diffuse Shader）
// 支持阴影

Shader "<<Haymaker>>/MobileUnlit/Diffuse"
{
	Properties
	{
		_Color ("Main Color", Color) = (1,1,1,1)
		_MainTex ("Base (RGB)", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "LightMode"="ForwardBase" }
		LOD 100
	
		Pass
		{  
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile LIGHTMAP_ON LIGHTMAP_OFF
				#pragma multi_compile_fwdbase
			
				#include "UnityCG.cginc"
				#include "AutoLight.cginc"

				struct appdata_t {
					float4 vertex : POSITION;
					float2 uv1 : TEXCOORD0;
	#ifdef LIGHTMAP_ON
					float2 uv2 : TEXCOORD1;
	#endif
				};

				struct v2f {
					float4 pos : SV_POSITION;
					half2 uv1 : TEXCOORD0;
	#ifdef LIGHTMAP_ON
					half2 uv2 : TEXCOORD1;
	#endif
					LIGHTING_COORDS(2, 3)
				};

				sampler2D _MainTex;
				float4 _MainTex_ST;
				fixed4 _Color;

	#ifdef LIGHTMAP_ON
				// sampler2D unity_Lightmap;
				// float4 unity_LightmapST;
	#endif

				v2f vert (appdata_t v)
				{
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uv1 = TRANSFORM_TEX(v.uv1, _MainTex);
	#ifdef LIGHTMAP_ON
					o.uv2 = v.uv2 * unity_LightmapST.xy + unity_LightmapST.zw;
	#endif
					TRANSFER_VERTEX_TO_FRAGMENT(o)
					return o;
				}
			
				fixed4 frag (v2f i) : SV_Target
				{
					fixed4 col = tex2D(_MainTex, i.uv1) * _Color;
	#ifdef LIGHTMAP_ON
					fixed4 lmColor = UNITY_SAMPLE_TEX2D(unity_Lightmap, i.uv2);
					col.rgb *= DecodeLightmap(lmColor);
					col.a = lmColor.a;
	#endif
					fixed atten = LIGHT_ATTENUATION(i);
					return col * atten;
				}
			ENDCG
		}
	}
	Fallback "VertexLit"
}
