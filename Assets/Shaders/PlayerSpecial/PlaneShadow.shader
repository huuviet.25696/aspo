﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "<<Haymaker>>/PlaneShadow"
{
	Properties 
    {
        _DirectionAndGround ("Direction & Ground", Vector) = (0.655, 0.467, 0.325, 0.0)
        _ShadowColor ("Shadow Color" , Color) = (0.0 , 0.0 , 0.0 , 1.0)
        _Transparency ("Transparency" , Range(0.0 , 1.0)) = 1.0
    }
	
	SubShader 
    {
		Tags { "Queue" = "Geometry" }
		Lighting Off
		Blend SrcAlpha OneMinusSrcAlpha


        Pass 
        {
///*
            Stencil
            {
                Ref 1
                Comp greater
                Pass replace
            }
//*/

		    CGPROGRAM

		    #pragma vertex vert
		    #pragma fragment frag
		    #pragma fragmentoption ARB_precision_hint_fastest 

	        #include "UnityCG.cginc"

            uniform half4 _Params;
            uniform half4 _DirectionAndGround = half4(0.655, 0.467, 0.325, 0.0);
            uniform half4 _ShadowColor = half4(0.0, 0.0, 0.0, 1.0);
            half _Transparency = 1.0f;
  
            struct v2f 
            {
		        half4 pos : SV_POSITION;
		        half4 uv : TEXCOORD0;	
	        };

	        v2f vert(appdata_full v)
	        {
                half3 worldPos = mul(unity_ObjectToWorld , v.vertex).xyz;

                half t = (_DirectionAndGround.w - worldPos.y) / _DirectionAndGround.y;
                half4 worldProjPos = half4(worldPos.xyz + _DirectionAndGround.xyz * t + half3(0,0.03,0), v.vertex.w);
 
		        v2f o;
                o.pos = mul(UNITY_MATRIX_VP, worldProjPos);
                return o; 
	        }
		
	        half4 frag( v2f i ) : COLOR
	        {
                return half4(_ShadowColor.rgb , _ShadowColor.a * _Transparency);
                //return _ShadowColor;
	        }

		    ENDCG
		}		
	} 
	FallBack Off
}
