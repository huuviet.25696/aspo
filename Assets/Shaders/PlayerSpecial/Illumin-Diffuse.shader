// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "<<Haymaker>>/Character/Self-Illumin/Diffuse"
{
    Properties
    {
        _Color("Main Color", Color) = (1,1,1,1)
        _MainTex("Base (RGB) Gloss (A)", 2D) = "white" {}
        HitColor("Hit Color", Color) = (1.0,1.0,1.0,0.0)
        _RimPower("Power", Range(0.5,8.0)) = 8.0
        HayHit_Color("Color", Color) = (0.26,0.19,0.16,0.0)
        HayHit_Pow("Pow", Range(3.0,8.0)) = 3.0
    }

    SubShader
    {
        Tags { "RenderType" = "Opaque" "IgnoreProjector" = "True"}
        LOD 200

        Stencil
        {
            Ref 2
            Comp Always
            Pass Replace
            ZFail Keep
        }
        Pass
        {
            Name "PASS"
            Lighting On


            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            sampler2D _MainTex;
            float4 _MainTex_ST;

            fixed4 _Color;
            float4 HitColor;
            float _RimPower;
            float HayHit_Pow;
            fixed4 HayHit_Color;

            struct appdata_t
		    {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
		    };
            struct v2f
            {
                float4 pos : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 diff1 : TEXCOORD1;
                float3 diff2 : TEXCOORD2;
                float rim : TEXCOORD3;
            };

            float3 VertexLight(float4 vertex, float3 normal, int idx)
            {
                float3 viewpos = mul (UNITY_MATRIX_MV, vertex).xyz;
                float3 viewN = mul ((float3x3)UNITY_MATRIX_IT_MV, normal);
                float3 lightColor = 0;

                float3 toLight = unity_LightPosition[idx].xyz - viewpos.xyz * unity_LightPosition[idx].w;
                float lengthSq = dot(toLight, toLight);
                float atten = 1.0 / (1.0 + lengthSq * unity_LightAtten[idx].z);
                float diff = max (0, dot (viewN, normalize(toLight)));
                lightColor += unity_LightColor[idx].rgb * (diff * atten);
                return lightColor;
            }

            v2f vert(appdata_t v)
            {
                v2f output;
                output.pos = UnityObjectToClipPos(v.vertex);
                output.uv = TRANSFORM_TEX(v.uv, _MainTex);

                output.diff1 = VertexLight(v.vertex, v.normal, 0) + UNITY_LIGHTMODEL_AMBIENT.rgb;
                output.diff2 = VertexLight(v.vertex, v.normal, 1);

                float3 normalDirection = normalize(mul(unity_ObjectToWorld, float4(v.normal, 0)).xyz);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - mul(unity_ObjectToWorld, v.vertex).xyz);
                output.rim = pow(1.0 - saturate(dot(normalize(viewDirection), normalDirection)), _RimPower);

                return output;
            }

            fixed4 frag(v2f i) : COLOR
            {
                half4 texColor = tex2D(_MainTex, i.uv);
                half3 albedo = texColor.rgb * _Color.rgb;
                half3 emission = albedo;

                if (_RimPower < 7.0)
                    emission += HitColor.rgb * i.rim;

                if (HayHit_Pow > 3.0)
                {
                    albedo = texColor.rgb * HayHit_Color.rgb * HayHit_Pow;
                    emission = texColor.rgb;
                }

                return fixed4(albedo * i.diff1 * 2.0 + emission + i.diff2 * texColor, 1);
            }
            ENDCG
        }
    }
    FallBack "Self-Illumin/VertexLit"
}
