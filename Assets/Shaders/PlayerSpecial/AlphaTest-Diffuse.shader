Shader "<<Haymaker>>/Character/Cutout/Diffuse" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
	
	HitColor ("Hit Color", Color) = (1.0,1.0,1.0,0.0)
	[HideInInspector]
    _RimPower ("Power", Range(0.2,8.0)) = 8.0
}

SubShader {
	Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="TransparentCutout"}
	LOD 200
	
CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoff

sampler2D _MainTex;
fixed4 _Color;

float4 HitColor;
float _RimPower;

struct Input {
	float2 uv_MainTex;
	float3 viewDir;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	o.Albedo = c.rgb;
	o.Alpha = c.a;
	
	o.Normal = fixed4(0.0,0.0,1.0,1.0);
	
	if(_RimPower < 7.0)
	{
		half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
		o.Emission = HitColor.rgb * pow (rim, _RimPower);
	}
}
ENDCG
}

Fallback "Transparent/Cutout/VertexLit"
}
