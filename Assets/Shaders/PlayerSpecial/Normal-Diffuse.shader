Shader "<<Haymaker>>/Character/Diffuse" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB)", 2D) = "white" {}
	
	HitColor ("Hit Color", Color) = (1.0,1.0,1.0,0.0)
	[HideInInspector]
    _RimPower ("Power", Range(0.5,8.0)) = 8.0
}

SubShader {
	Tags { "RenderType"="Opaque" "IgnoreProjector"="True"}
	LOD 300

CGPROGRAM
#pragma surface surf Lambert

sampler2D _MainTex;
fixed4 _Color;

float4 HitColor;
float _RimPower;

struct Input {
	float2 uv_MainTex;
	float3 viewDir;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	o.Albedo = c.rgb;
	o.Alpha = 1.0;
	
	o.Normal = fixed4(0.0,0.0,1.0,1.0);
	if(_RimPower < 7.0)
	{
		half rim = 1.0 - saturate(dot (normalize(IN.viewDir), o.Normal));
		o.Emission = HitColor.rgb * pow (rim, _RimPower);
	}
}
ENDCG  
}

FallBack "Diffuse"
}
