// Upgrade NOTE: commented out 'float4 unity_LightmapST', a built-in variable
// Upgrade NOTE: commented out 'sampler2D unity_Lightmap', a built-in variable
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'
// Upgrade NOTE: replaced tex2D unity_Lightmap with UNITY_SAMPLE_TEX2D

// 不支持光照，支持应用Lightmap（不支持烘焙Lightmap，烘焙时需要替换回普通的T4M 4 Textures）
// 支持阴影

Shader "<<Haymaker>>/MobileUnlit/T4M/4Textures"
{
	Properties
	{
		_Splat0 ("Layer 1", 2D) = "white" {}
		_Splat1 ("Layer 2", 2D) = "white" {}
		_Splat2 ("Layer 3", 2D) = "white" {}
		_Splat3 ("Layer 4", 2D) = "white" {}
		_Control ("Control (RGBA)", 2D) = "white" {}
		_MainTex ("Never Used", 2D) = "white" {}
	}
	SubShader
	{
		Tags { "SplatCount" = "4" "RenderType" = "Opaque" "LightMode"="ForwardBase" }
		LOD 100
	
		Pass
		{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma multi_compile LIGHTMAP_ON LIGHTMAP_OFF
				#pragma multi_compile_fwdbase
			
				#include "UnityCG.cginc"
				#include "AutoLight.cginc"

				struct appdata_t {
					float4 vertex : POSITION;
					float2 texUV : TEXCOORD0;
	#ifdef LIGHTMAP_ON
					float2 lmUV : TEXCOORD1;
	#endif
				};

				struct v2f {
					float4 pos : SV_POSITION;
					half2 uvControl : TEXCOORD0;
					half2 uvSplat0 : TEXCOORD1;
					half2 uvSplat1 : TEXCOORD2;
					half2 uvSplat2 : TEXCOORD3;
					half2 uvSplat3 : TEXCOORD4;
	#ifdef LIGHTMAP_ON
					half2 lmUV : TEXCOORD5;
	#endif
					LIGHTING_COORDS(6, 7)
				};

				sampler2D _Control;
				float4 _Control_ST;

				sampler2D _Splat0;
				float4 _Splat0_ST;

				sampler2D _Splat1;
				float4 _Splat1_ST;

				sampler2D _Splat2;
				float4 _Splat2_ST;

				sampler2D _Splat3;
				float4 _Splat3_ST;

	#ifdef LIGHTMAP_ON
				// sampler2D unity_Lightmap;
				// float4 unity_LightmapST;
	#endif

				v2f vert(appdata_t v)
				{
					v2f o;
					o.pos = UnityObjectToClipPos(v.vertex);
					o.uvControl = TRANSFORM_TEX(v.texUV, _Control);
					o.uvSplat0 = TRANSFORM_TEX(v.texUV, _Splat0);
					o.uvSplat1 = TRANSFORM_TEX(v.texUV, _Splat1);
					o.uvSplat2 = TRANSFORM_TEX(v.texUV, _Splat2);
					o.uvSplat3 = TRANSFORM_TEX(v.texUV, _Splat3);
	#ifdef LIGHTMAP_ON
					o.lmUV = v.lmUV * unity_LightmapST.xy + unity_LightmapST.zw;
	#endif
					TRANSFER_VERTEX_TO_FRAGMENT(o)
					return o;
				}
			
				fixed4 frag(v2f i) : SV_Target
				{
					fixed4 splatControl = tex2D(_Control, i.uvControl).rgba;
					fixed3 rgbCol =
						(splatControl.r * tex2D(_Splat0, i.uvSplat0).rgb) +
						(splatControl.g * tex2D(_Splat1, i.uvSplat1).rgb) +
						(splatControl.b * tex2D(_Splat2, i.uvSplat2).rgb) +
						(splatControl.a * tex2D(_Splat3, i.uvSplat3).rgb);
					fixed4 col = fixed4(rgbCol, 0);

	#ifdef LIGHTMAP_ON
					fixed4 lmColor = UNITY_SAMPLE_TEX2D(unity_Lightmap, i.lmUV);
					col.rgb *= DecodeLightmap(lmColor);
					col.a = lmColor.a;
	#endif
					fixed atten = LIGHT_ATTENUATION(i);
					return col * atten;
				}
			ENDCG
		}
	}
	Fallback "Diffuse"
}
