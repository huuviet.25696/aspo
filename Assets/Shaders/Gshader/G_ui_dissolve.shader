// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:32914,y:32703,varname:node_4013,prsc:2|emission-528-OUT,alpha-4558-A;n:type:ShaderForge.SFN_VertexColor,id:8463,x:30905,y:32914,varname:node_8463,prsc:2;n:type:ShaderForge.SFN_Tex2d,id:4558,x:31001,y:32615,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_4558,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Lerp,id:528,x:32470,y:32861,varname:node_528,prsc:2|A-9112-OUT,B-4558-RGB,T-394-OUT;n:type:ShaderForge.SFN_TexCoord,id:8979,x:30458,y:33476,varname:node_8979,prsc:2,uv:0;n:type:ShaderForge.SFN_RemapRange,id:3867,x:30679,y:33476,varname:node_3867,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-8979-UVOUT;n:type:ShaderForge.SFN_Length,id:6070,x:30873,y:33476,varname:node_6070,prsc:2|IN-3867-OUT;n:type:ShaderForge.SFN_Clamp01,id:394,x:31846,y:33121,varname:node_394,prsc:2|IN-1920-OUT;n:type:ShaderForge.SFN_Clamp01,id:8762,x:31067,y:33477,varname:node_8762,prsc:2|IN-6070-OUT;n:type:ShaderForge.SFN_Tex2d,id:8038,x:31166,y:33203,ptovrint:False,ptlb:NoiseTex,ptin:_NoiseTex,varname:node_8038,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:51472ff479b48a14887c519c05b7208e,ntxv:0,isnm:False|UVIN-1748-OUT;n:type:ShaderForge.SFN_Step,id:1920,x:31664,y:33121,varname:node_1920,prsc:2|A-1658-OUT,B-6576-OUT;n:type:ShaderForge.SFN_TexCoord,id:4268,x:30681,y:33194,varname:node_4268,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:1748,x:30899,y:33204,varname:node_1748,prsc:2|A-4268-UVOUT,B-2798-OUT;n:type:ShaderForge.SFN_Vector1,id:2798,x:30681,y:33382,varname:node_2798,prsc:2,v1:2;n:type:ShaderForge.SFN_Multiply,id:1658,x:31448,y:33214,varname:node_1658,prsc:2|A-8038-R,B-8762-OUT;n:type:ShaderForge.SFN_RemapRange,id:6576,x:31195,y:33016,varname:node_6576,prsc:2,frmn:0.1,frmx:1,tomn:0,tomx:1|IN-8463-A;n:type:ShaderForge.SFN_Desaturate,id:2419,x:31357,y:32801,varname:node_2419,prsc:2|COL-4558-RGB;n:type:ShaderForge.SFN_Multiply,id:9112,x:31890,y:32934,varname:node_9112,prsc:2|A-2419-OUT,B-8463-RGB;proporder:4558-8038;pass:END;sub:END;*/

Shader "G/G_ui_dissolve" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _NoiseTex ("NoiseTex", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 2.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform sampler2D _NoiseTex; uniform float4 _NoiseTex_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float node_2419 = dot(_MainTex_var.rgb,float3(0.3,0.59,0.11));
                float2 node_1748 = (i.uv0*2.0);
                float4 _NoiseTex_var = tex2D(_NoiseTex,TRANSFORM_TEX(node_1748, _NoiseTex));
                float3 emissive = lerp((node_2419*i.vertexColor.rgb),_MainTex_var.rgb,saturate(step((_NoiseTex_var.r*saturate(length((i.uv0*2.0+-1.0)))),(i.vertexColor.a*1.111111+-0.1111111))));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,_MainTex_var.a);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
