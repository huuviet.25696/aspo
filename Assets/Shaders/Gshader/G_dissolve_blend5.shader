// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32982,y:32595,varname:node_3138,prsc:2|emission-9979-RGB,alpha-7540-OUT;n:type:ShaderForge.SFN_Step,id:9142,x:31881,y:32999,varname:node_9142,prsc:2|A-3392-OUT,B-3716-OUT;n:type:ShaderForge.SFN_OneMinus,id:8999,x:32053,y:32999,varname:node_8999,prsc:2|IN-9142-OUT;n:type:ShaderForge.SFN_Tex2d,id:3615,x:31255,y:32969,ptovrint:False,ptlb:Dissolve_Tex,ptin:_Dissolve_Tex,varname:node_3615,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Desaturate,id:3447,x:31466,y:32969,varname:node_3447,prsc:2|COL-3615-RGB;n:type:ShaderForge.SFN_Clamp01,id:7540,x:32624,y:33002,varname:node_7540,prsc:2|IN-8087-OUT;n:type:ShaderForge.SFN_Multiply,id:3392,x:31628,y:33084,varname:node_3392,prsc:2|A-3447-OUT,B-3615-A;n:type:ShaderForge.SFN_OneMinus,id:3716,x:31698,y:33240,varname:node_3716,prsc:2|IN-3408-OUT;n:type:ShaderForge.SFN_RemapRange,id:4914,x:31997,y:33257,varname:node_4914,prsc:2,frmn:0.75,frmx:1,tomn:1,tomx:0|IN-3408-OUT;n:type:ShaderForge.SFN_Multiply,id:8087,x:32422,y:33081,varname:node_8087,prsc:2|A-8999-OUT,B-4914-OUT;n:type:ShaderForge.SFN_Color,id:9979,x:32180,y:32649,ptovrint:False,ptlb:MainColor,ptin:_MainColor,varname:node_9979,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_ValueProperty,id:3408,x:31412,y:33394,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_3408,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;proporder:3615-9979-3408;pass:END;sub:END;*/

Shader "G/G_dissolve_blend5" {
    Properties {
        _Dissolve_Tex ("Dissolve_Tex", 2D) = "white" {}
        _MainColor ("MainColor", Color) = (0.5,0.5,0.5,1)
        _Alpha ("Alpha", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            //#pragma multi_compile_fwdbase
            //#pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Dissolve_Tex; uniform float4 _Dissolve_Tex_ST;
            uniform float4 _MainColor;
            uniform float _Alpha;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float3 emissive = _MainColor.rgb;
                float3 finalColor = emissive;
                float4 _Dissolve_Tex_var = tex2D(_Dissolve_Tex,TRANSFORM_TEX(i.uv0, _Dissolve_Tex));
                return fixed4(finalColor,saturate(((1.0 - step((dot(_Dissolve_Tex_var.rgb,float3(0.3,0.59,0.11))*_Dissolve_Tex_var.a),(1.0 - _Alpha)))*(_Alpha*-4.0+4.0))));
            }
            ENDCG
        }
    }
    //FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
