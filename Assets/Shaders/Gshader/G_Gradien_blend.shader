// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32790,y:32615,varname:node_3138,prsc:2|emission-7461-OUT,alpha-1928-A;n:type:ShaderForge.SFN_Color,id:7241,x:31725,y:32435,ptovrint:False,ptlb:Color1,ptin:_Color1,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:2768,x:31725,y:32631,ptovrint:False,ptlb:Color2,ptin:_Color2,varname:node_2768,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:8418,x:32197,y:32664,varname:node_8418,prsc:2|A-2768-RGB,B-7241-RGB,T-8119-OUT;n:type:ShaderForge.SFN_ComponentMask,id:1020,x:30768,y:32877,varname:node_1020,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-5884-OUT;n:type:ShaderForge.SFN_Clamp01,id:8119,x:31953,y:32854,varname:node_8119,prsc:2|IN-7616-OUT;n:type:ShaderForge.SFN_ValueProperty,id:138,x:30432,y:33100,ptovrint:False,ptlb:Gradient_Offset,ptin:_Gradient_Offset,varname:node_138,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Power,id:7616,x:31744,y:33011,varname:node_7616,prsc:2|VAL-5880-OUT,EXP-6712-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6712,x:31484,y:33142,ptovrint:False,ptlb:Gradient_Power,ptin:_Gradient_Power,varname:node_6712,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_TexCoord,id:4120,x:30432,y:32874,varname:node_4120,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:1394,x:31083,y:32591,varname:node_1394,prsc:2|A-1020-OUT,B-611-OUT;n:type:ShaderForge.SFN_Vector1,id:611,x:30907,y:32672,varname:node_611,prsc:2,v1:2;n:type:ShaderForge.SFN_Subtract,id:2730,x:31277,y:32591,varname:node_2730,prsc:2|A-1394-OUT,B-5134-OUT;n:type:ShaderForge.SFN_Vector1,id:5134,x:31084,y:32755,varname:node_5134,prsc:2,v1:1;n:type:ShaderForge.SFN_Abs,id:5880,x:31513,y:32591,varname:node_5880,prsc:2|IN-2730-OUT;n:type:ShaderForge.SFN_Add,id:5884,x:30605,y:32919,varname:node_5884,prsc:2|A-4120-UVOUT,B-138-OUT;n:type:ShaderForge.SFN_VertexColor,id:1928,x:32197,y:32901,varname:node_1928,prsc:2;n:type:ShaderForge.SFN_Multiply,id:7461,x:32460,y:32741,varname:node_7461,prsc:2|A-8418-OUT,B-1928-RGB;proporder:7241-2768-138-6712;pass:END;sub:END;*/

Shader "G/G_Gradien_blend" {
    Properties {
        _Color1 ("Color1", Color) = (1,1,1,1)
        _Color2 ("Color2", Color) = (0.5,0.5,0.5,1)
        _Gradient_Offset ("Gradient_Offset", Float ) = 0
        _Gradient_Power ("Gradient_Power", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            //#pragma multi_compile_fwdbase
            //#pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 2.0
            uniform float4 _Color1;
            uniform float4 _Color2;
            uniform float _Gradient_Offset;
            uniform float _Gradient_Power;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float3 emissive = (lerp(_Color2.rgb,_Color1.rgb,saturate(pow(abs((((i.uv0+_Gradient_Offset).g*2.0)-1.0)),_Gradient_Power)))*i.vertexColor.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,i.vertexColor.a);
            }
            ENDCG
        }
    }
    //FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
