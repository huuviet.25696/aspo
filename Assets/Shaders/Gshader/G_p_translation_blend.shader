// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:33160,y:32718,varname:node_3138,prsc:2|emission-9612-OUT,alpha-7540-OUT;n:type:ShaderForge.SFN_Tex2d,id:4077,x:31820,y:32730,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_4077,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-2070-OUT;n:type:ShaderForge.SFN_Color,id:876,x:31820,y:32415,ptovrint:False,ptlb:MainColor,ptin:_MainColor,varname:node_876,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:9612,x:32564,y:32569,varname:node_9612,prsc:2|A-876-RGB,B-2156-RGB,C-4077-RGB,D-1726-OUT;n:type:ShaderForge.SFN_VertexColor,id:2156,x:31820,y:32581,varname:node_2156,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:1726,x:32130,y:32773,ptovrint:False,ptlb:MainTex_Power,ptin:_MainTex_Power,varname:node_1726,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_Multiply,id:931,x:32813,y:32977,varname:node_931,prsc:2|A-7175-OUT,B-6722-OUT;n:type:ShaderForge.SFN_VertexColor,id:5452,x:30427,y:32779,varname:node_5452,prsc:2;n:type:ShaderForge.SFN_OneMinus,id:3623,x:30676,y:32779,varname:node_3623,prsc:2|IN-5452-A;n:type:ShaderForge.SFN_RemapRange,id:2223,x:30847,y:32779,varname:node_2223,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-3623-OUT;n:type:ShaderForge.SFN_Multiply,id:7175,x:32119,y:32869,varname:node_7175,prsc:2|A-4077-A,B-1726-OUT;n:type:ShaderForge.SFN_Clamp01,id:7540,x:32982,y:32977,varname:node_7540,prsc:2|IN-931-OUT;n:type:ShaderForge.SFN_Tex2d,id:6160,x:31819,y:33007,ptovrint:False,ptlb:MaskTex(U_trans),ptin:_MaskTexU_trans,varname:node_6160,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-3012-OUT;n:type:ShaderForge.SFN_Multiply,id:4183,x:32033,y:33042,varname:node_4183,prsc:2|A-6160-RGB,B-6160-A;n:type:ShaderForge.SFN_Desaturate,id:6722,x:32255,y:33042,varname:node_6722,prsc:2|COL-4183-OUT;n:type:ShaderForge.SFN_TexCoord,id:6311,x:30847,y:32531,varname:node_6311,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:8319,x:31119,y:32757,varname:node_8319,prsc:2|A-6311-V,B-2223-OUT;n:type:ShaderForge.SFN_Append,id:2070,x:31382,y:32635,varname:node_2070,prsc:2|A-6311-U,B-8319-OUT;n:type:ShaderForge.SFN_Add,id:574,x:31138,y:33120,varname:node_574,prsc:2|A-6311-U,B-5309-OUT;n:type:ShaderForge.SFN_Multiply,id:5309,x:30824,y:33218,varname:node_5309,prsc:2|A-5452-A,B-6184-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6184,x:30559,y:33276,ptovrint:False,ptlb:MaskTex_Speed_Amount,ptin:_MaskTex_Speed_Amount,varname:node_6184,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Append,id:3012,x:31380,y:33120,varname:node_3012,prsc:2|A-574-OUT,B-6311-V;proporder:876-4077-1726-6160-6184;pass:END;sub:END;*/

Shader "G/G_p_translation_blend" {
    Properties {
        _MainColor ("MainColor", Color) = (0.5,0.5,0.5,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _MainTex_Power ("MainTex_Power", Float ) = 3
        _MaskTexU_trans ("MaskTex(U_trans)", 2D) = "white" {}
        _MaskTex_Speed_Amount ("MaskTex_Speed_Amount", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            //#pragma multi_compile_fwdbase
            //#pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            //#pragma target 3.0
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _MainColor;
            uniform float _MainTex_Power;
            uniform sampler2D _MaskTexU_trans; uniform float4 _MaskTexU_trans_ST;
            uniform float _MaskTex_Speed_Amount;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float2 node_2070 = float2(i.uv0.r,(i.uv0.g+((1.0 - i.vertexColor.a)*2.0+-1.0)));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_2070, _MainTex));
                float3 emissive = (_MainColor.rgb*i.vertexColor.rgb*_MainTex_var.rgb*_MainTex_Power);
                float3 finalColor = emissive;
                float2 node_3012 = float2((i.uv0.r+(i.vertexColor.a*_MaskTex_Speed_Amount)),i.uv0.g);
                float4 _MaskTexU_trans_var = tex2D(_MaskTexU_trans,TRANSFORM_TEX(node_3012, _MaskTexU_trans));
                return fixed4(finalColor,saturate(((_MainTex_var.a*_MainTex_Power)*dot((_MaskTexU_trans_var.rgb*_MaskTexU_trans_var.a),float3(0.3,0.59,0.11)))));
            }
            ENDCG
        }
    }
    //Fallback "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
