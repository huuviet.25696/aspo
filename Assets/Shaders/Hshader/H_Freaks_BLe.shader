// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:False,qofs:0,qpre:0,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True;n:type:ShaderForge.SFN_Final,id:4795,x:32927,y:32526,varname:node_4795,prsc:2|emission-2393-OUT,alpha-798-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:31293,y:32506,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2393,x:32657,y:32578,varname:node_2393,prsc:2|A-9774-OUT,B-2053-RGB,C-4781-OUT;n:type:ShaderForge.SFN_VertexColor,id:2053,x:32054,y:32771,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Color,id:797,x:31693,y:32855,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:798,x:32432,y:32906,varname:node_798,prsc:2|A-797-B,B-2053-A,C-1680-OUT;n:type:ShaderForge.SFN_Fresnel,id:2488,x:31437,y:32059,varname:node_2488,prsc:2|EXP-1111-OUT;n:type:ShaderForge.SFN_Multiply,id:9737,x:31845,y:32240,varname:node_9737,prsc:2|A-2488-OUT,B-2534-OUT,C-2978-RGB;n:type:ShaderForge.SFN_ValueProperty,id:2534,x:31425,y:32202,ptovrint:False,ptlb:Fresnel_ZT,ptin:_Fresnel_ZT,varname:node_2534,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:1111,x:31253,y:32184,ptovrint:False,ptlb:Fresnel_Exp,ptin:_Fresnel_Exp,varname:node_1111,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Add,id:9774,x:32313,y:32479,varname:node_9774,prsc:2|A-9737-OUT,B-5316-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4781,x:32440,y:32756,ptovrint:False,ptlb:ZT,ptin:_ZT,varname:node_4781,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Color,id:2978,x:31471,y:32294,ptovrint:False,ptlb:Fresnel_Color,ptin:_Fresnel_Color,varname:node_2978,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:5316,x:32017,y:32568,varname:node_5316,prsc:2|A-9847-OUT,B-797-RGB;n:type:ShaderForge.SFN_Tex2d,id:1136,x:31151,y:32718,ptovrint:False,ptlb:liuguang,ptin:_liuguang,varname:node_1136,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False|UVIN-9499-OUT;n:type:ShaderForge.SFN_Add,id:9847,x:31638,y:32609,varname:node_9847,prsc:2|A-6074-RGB,B-3452-OUT;n:type:ShaderForge.SFN_Multiply,id:3452,x:31375,y:32757,varname:node_3452,prsc:2|A-1136-RGB,B-6234-RGB;n:type:ShaderForge.SFN_Color,id:6234,x:31151,y:32950,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_6234,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Panner,id:3593,x:30785,y:32663,varname:node_3593,prsc:2,spu:0,spv:0.1|UVIN-6792-UVOUT,DIST-1005-OUT;n:type:ShaderForge.SFN_TexCoord,id:6792,x:30559,y:32625,varname:node_6792,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:1005,x:30537,y:32812,varname:node_1005,prsc:2|A-3007-T,B-6561-OUT;n:type:ShaderForge.SFN_Time,id:3007,x:30300,y:32812,varname:node_3007,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:6561,x:30312,y:33019,ptovrint:False,ptlb:liuguang_sudu,ptin:_liuguang_sudu,varname:node_6561,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_SwitchProperty,id:9499,x:30954,y:32767,ptovrint:False,ptlb:UorV,ptin:_UorV,varname:node_9499,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-3593-UVOUT,B-8185-UVOUT;n:type:ShaderForge.SFN_Panner,id:8185,x:30785,y:32861,varname:node_8185,prsc:2,spu:0.1,spv:0|UVIN-6792-UVOUT,DIST-1005-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1680,x:32184,y:33181,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_1680,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;proporder:6074-797-2534-1111-4781-2978-1136-6234-6561-9499-1680;pass:END;sub:END;*/

Shader "H/H_Freaks_BLe" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _TintColor ("Color", Color) = (0.5,0.5,0.5,1)
        _Fresnel_ZT ("Fresnel_ZT", Float ) = 1
        _Fresnel_Exp ("Fresnel_Exp", Float ) = 1
        _ZT ("ZT", Float ) = 1
        _Fresnel_Color ("Fresnel_Color", Color) = (0.5,0.5,0.5,1)
        _liuguang ("liuguang", 2D) = "white" {}
        _Color ("Color", Color) = (0.5,0.5,0.5,1)
        _liuguang_sudu ("liuguang_sudu", Float ) = 2
        [MaterialToggle] _UorV ("UorV", Float ) = 0
        _Alpha ("Alpha", Float ) = 1
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="Background"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
           // #pragma multi_compile_fwdbase_fullshadows
           // #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            //#pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _TintColor;
            uniform float _Fresnel_ZT;
            uniform float _Fresnel_Exp;
            uniform float _ZT;
            uniform float4 _Fresnel_Color;
            uniform sampler2D _liuguang; uniform float4 _liuguang_ST;
            uniform float4 _Color;
            uniform float _liuguang_sudu;
            uniform fixed _UorV;
            uniform float _Alpha;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float4 node_3007 = _Time + _TimeEditor;
                float node_1005 = (node_3007.g*_liuguang_sudu);
                float2 _UorV_var = lerp( (i.uv0+node_1005*float2(0,0.1)), (i.uv0+node_1005*float2(0.1,0)), _UorV );
                float4 _liuguang_var = tex2D(_liuguang,TRANSFORM_TEX(_UorV_var, _liuguang));
                float3 emissive = (((pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel_Exp)*_Fresnel_ZT*_Fresnel_Color.rgb)+((_MainTex_var.rgb+(_liuguang_var.rgb*_Color.rgb))*_TintColor.rgb))*i.vertexColor.rgb*_ZT);
                float3 finalColor = emissive;
                return fixed4(finalColor,(_TintColor.b*i.vertexColor.a*_Alpha));
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
