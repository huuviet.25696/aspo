﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Game/Blur"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_BlurRadius ("BlurRadius", float) = 2
	}
	SubShader
	{
	
		Tags {"Queue"="Transparent" "RenderType" = "Transparent"}
		
		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float _Offset;
			float4 _MainTex_TexelSize;  
			//模糊半径  
			float _BlurRadius;  

			struct VertexOutput
			{
				float4 pos : SV_POSITION; //顶点位置  
				float2 uv  : TEXCOORD0;   //纹理坐标  
				float2 uv1 : TEXCOORD1;  //周围纹理1  
				float2 uv2 : TEXCOORD2;  //周围纹理2  
				float2 uv3 : TEXCOORD3;  //周围纹理3  
				float2 uv4 : TEXCOORD4;  //周围纹理4  
			};

			VertexOutput vert(appdata_base v)
			{
				VertexOutput o;  
				o.pos = UnityObjectToClipPos(v.vertex);  
				o.uv = v.texcoord.xy;  
				//计算uv上下左右四个点对于blur半径下的uv坐标  
				o.uv1 = v.texcoord.xy + _BlurRadius * _MainTex_TexelSize * float2( 1,  1);  
				o.uv2 = v.texcoord.xy + _BlurRadius * _MainTex_TexelSize * float2(-1,  1);  
				o.uv3 = v.texcoord.xy + _BlurRadius * _MainTex_TexelSize * float2(-1, -1);  
				o.uv4 = v.texcoord.xy + _BlurRadius * _MainTex_TexelSize * float2( 1, -1);  
  
        return o;  
			}

			fixed4 frag(VertexOutput IN):COLOR
			{
				fixed4 color = fixed4(0,0,0,0);  
  
				color += tex2D(_MainTex, IN.uv );  
				color += tex2D(_MainTex, IN.uv1);  
				color += tex2D(_MainTex, IN.uv2);  
				color += tex2D(_MainTex, IN.uv3);  
				color += tex2D(_MainTex, IN.uv4);  
          
				//相加取平均，据说shader中乘法比较快  
				return color * 0.2;  
			}

			ENDCG
		}
	} 

}