// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Game/TextureCover2"
{
	Properties
	{
		_MainTex ("Base (RGB), Alpha (A)", 2D) = "black" {}
		_SkipRange ("Clip Range",Vector) = (0,0,0,0)
	}
	
	SubShader
	{
		LOD 200

		Tags
		{
			"Queue" = "Transparent-10"
			"IgnoreProjector" = "True"
			"RenderType" = "Transparent"
		}
		
		Pass
		{
			Cull Off
			Lighting Off
			ZWrite On
			Fog { Mode Off }
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag			
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float4 _SkipRange; 
			struct appdata_t
			{
				float4 vertex : POSITION;
				float2 texcoord : TEXCOORD0;
				fixed4 color : COLOR;
			};
	
			struct v2f
			{
				float4 vertex : SV_POSITION;
				half2 texcoord : TEXCOORD0;
				fixed4 color : COLOR;
			};
	
			v2f o;

			v2f vert (appdata_t v)
			{
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.texcoord = v.texcoord;
				o.color = v.color;
				return o;
			}
				
			fixed4 frag (v2f IN) : SV_Target
			{
					
				if ( IN.texcoord.x < _SkipRange.x - _SkipRange.z )
				{
					fixed4 col = tex2D(_MainTex, IN.texcoord);
					col = col * IN.color;
					return col;	
				}
				else if(IN.texcoord.x > _SkipRange.x + _SkipRange.z )
				{
					fixed4 col = tex2D(_MainTex, IN.texcoord);
					col = col * IN.color;
					return col;	
				}
				else if( IN.texcoord.y < _SkipRange.y - _SkipRange.w )
				{
					fixed4 col = tex2D(_MainTex, IN.texcoord);
					col = col * IN.color;
					return col;		
				}
				else if (IN.texcoord.y > _SkipRange.y + _SkipRange.w )				
				{
					fixed4 col = tex2D(_MainTex, IN.texcoord);
					col = col * IN.color;
					return col;		
				}
				else
				{
					fixed4 col = fixed4(0,0,0,0);
					return col;
				}
			}
			ENDCG
		}
	}
}
