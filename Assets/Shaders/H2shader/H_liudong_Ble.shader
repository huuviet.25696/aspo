// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:14,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:True,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True;n:type:ShaderForge.SFN_Final,id:4795,x:32312,y:32460,varname:node_4795,prsc:2|emission-2393-OUT,alpha-5414-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:31359,y:33102,ptovrint:False,ptlb:Mask,ptin:_Mask,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:5dd2fdc55ddf3ad43894f9710d5a6b2e,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:2393,x:31847,y:32578,varname:node_2393,prsc:2|A-3191-RGB,B-2053-RGB,C-797-RGB,D-9248-OUT,E-6074-RGB;n:type:ShaderForge.SFN_VertexColor,id:2053,x:31369,y:32697,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Color,id:797,x:31349,y:32815,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.6,c2:2,c3:0.5,c4:1;n:type:ShaderForge.SFN_Vector1,id:9248,x:31330,y:32966,varname:node_9248,prsc:2,v1:2;n:type:ShaderForge.SFN_Append,id:2664,x:31172,y:32516,varname:node_2664,prsc:2|A-9290-OUT,B-37-OUT;n:type:ShaderForge.SFN_TexCoord,id:6184,x:30702,y:32556,varname:node_6184,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:9290,x:30962,y:32432,varname:node_9290,prsc:2|A-8940-OUT,B-6184-U;n:type:ShaderForge.SFN_Multiply,id:8940,x:30702,y:32407,varname:node_8940,prsc:2|A-2703-TSL,B-961-OUT;n:type:ShaderForge.SFN_Time,id:2703,x:30022,y:32588,varname:node_2703,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:961,x:30385,y:32544,ptovrint:False,ptlb:U_Speed01,ptin:_U_Speed01,varname:node_961,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Add,id:37,x:30953,y:32692,varname:node_37,prsc:2|A-6184-V,B-1653-OUT;n:type:ShaderForge.SFN_Tex2d,id:3191,x:31368,y:32516,ptovrint:False,ptlb:TEX,ptin:_TEX,varname:node_3191,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:e9b173c0d6ac89c44898e6fd7b99a15f,ntxv:0,isnm:False|UVIN-2664-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2826,x:30397,y:32757,ptovrint:False,ptlb:V_Speed01,ptin:_V_Speed01,varname:_node_961_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:1653,x:30702,y:32746,varname:node_1653,prsc:2|A-2703-TSL,B-2826-OUT;n:type:ShaderForge.SFN_Multiply,id:7470,x:31589,y:33907,varname:node_7470,prsc:2|A-4987-T,B-3230-OUT;n:type:ShaderForge.SFN_Time,id:4987,x:31193,y:33835,varname:node_4987,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:3230,x:31212,y:34017,ptovrint:False,ptlb:U_copy_copy_copy_copy,ptin:_U_copy_copy_copy_copy,varname:_U_copy_copy_copy_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:5414,x:31873,y:32931,varname:node_5414,prsc:2|A-6074-R,B-6074-A,C-3191-A,D-2053-A;proporder:3191-961-2826-797-6074;pass:END;sub:END;*/

Shader "H2/H_Liudong_Ble" {
    Properties {
        _TEX ("TEX", 2D) = "white" {}
        _U_Speed01 ("U_Speed01", Float ) = 1
        _V_Speed01 ("V_Speed01", Float ) = 1
        _TintColor ("Color", Color) = (0.6,2,0.5,1)
        _Mask ("Mask", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            ColorMask RGB
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            //#pragma multi_compile_fwdbase
           // #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
           // #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _Mask; uniform float4 _Mask_ST;
            uniform float4 _TintColor;
            uniform float _U_Speed01;
            uniform sampler2D _TEX; uniform float4 _TEX_ST;
            uniform float _V_Speed01;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_2703 = _Time + _TimeEditor;
                float2 node_2664 = float2(((node_2703.r*_U_Speed01)+i.uv0.r),(i.uv0.g+(node_2703.r*_V_Speed01)));
                float4 _TEX_var = tex2D(_TEX,TRANSFORM_TEX(node_2664, _TEX));
                float4 _Mask_var = tex2D(_Mask,TRANSFORM_TEX(i.uv0, _Mask));
                float3 emissive = (_TEX_var.rgb*i.vertexColor.rgb*_TintColor.rgb*2.0*_Mask_var.rgb);
                float3 finalColor = emissive;
                return fixed4(finalColor,(_Mask_var.r*_Mask_var.a*_TEX_var.a*i.vertexColor.a));
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
