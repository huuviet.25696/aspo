// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:0,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:1,fgcg:0.4527383,fgcb:0.4411765,fgca:1,fgde:0.01,fgrn:-43.8,fgrf:384.7,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True;n:type:ShaderForge.SFN_Final,id:4795,x:32697,y:32959,varname:node_4795,prsc:2|emission-2393-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:31564,y:33263,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:5dd2fdc55ddf3ad43894f9710d5a6b2e,ntxv:0,isnm:False|UVIN-1540-OUT;n:type:ShaderForge.SFN_Multiply,id:2393,x:32233,y:33039,varname:node_2393,prsc:2|A-797-A,B-2053-RGB,C-797-RGB,D-6199-OUT,E-461-OUT;n:type:ShaderForge.SFN_VertexColor,id:2053,x:31631,y:32886,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Color,id:797,x:31634,y:33087,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:9704,x:30272,y:33073,ptovrint:False,ptlb:Noise 01,ptin:_Noise01,varname:node_9704,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:9a3c093aa80a8c34ca15053ab7abda2c,ntxv:0,isnm:False|UVIN-2778-OUT;n:type:ShaderForge.SFN_Append,id:2778,x:30082,y:33042,varname:node_2778,prsc:2|A-8813-OUT,B-6750-OUT;n:type:ShaderForge.SFN_TexCoord,id:8230,x:29706,y:33081,varname:node_8230,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:8813,x:29902,y:32967,varname:node_8813,prsc:2|A-2506-OUT,B-8230-U;n:type:ShaderForge.SFN_Multiply,id:2506,x:29670,y:32907,varname:node_2506,prsc:2|A-9982-T,B-5869-OUT;n:type:ShaderForge.SFN_Time,id:9982,x:29437,y:32847,varname:node_9982,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:5869,x:29468,y:33070,ptovrint:False,ptlb:U_Speed,ptin:_U_Speed,varname:node_5869,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:6750,x:29874,y:33244,varname:node_6750,prsc:2|A-8230-V,B-9086-OUT;n:type:ShaderForge.SFN_Multiply,id:9086,x:29623,y:33243,varname:node_9086,prsc:2|A-9955-T,B-3622-OUT;n:type:ShaderForge.SFN_Time,id:9955,x:29413,y:33183,varname:node_9955,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:3622,x:29450,y:33410,ptovrint:False,ptlb:V_Speed,ptin:_V_Speed,varname:_node_5869_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:3853,x:31049,y:33285,varname:node_3853,prsc:2|A-8424-OUT,B-8125-OUT;n:type:ShaderForge.SFN_Add,id:1540,x:31276,y:33259,varname:node_1540,prsc:2|A-6037-UVOUT,B-3853-OUT;n:type:ShaderForge.SFN_Tex2d,id:5494,x:30269,y:33505,ptovrint:False,ptlb:Noise 02,ptin:_Noise02,varname:_node_9704_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7acc33f24a06fcd46baa112415b42195,ntxv:0,isnm:False|UVIN-9617-OUT;n:type:ShaderForge.SFN_Append,id:9617,x:30058,y:33525,varname:node_9617,prsc:2|A-3085-OUT,B-1401-OUT;n:type:ShaderForge.SFN_TexCoord,id:4987,x:29682,y:33564,varname:node_4987,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:3085,x:29878,y:33450,varname:node_3085,prsc:2|A-9194-OUT,B-4987-U;n:type:ShaderForge.SFN_Multiply,id:9194,x:29646,y:33390,varname:node_9194,prsc:2|A-1896-T,B-5069-OUT;n:type:ShaderForge.SFN_Time,id:1896,x:29314,y:33284,varname:node_1896,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:5069,x:29444,y:33553,ptovrint:False,ptlb:U_Speed_copy,ptin:_U_Speed_copy,varname:_U_Speed_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Add,id:1401,x:29850,y:33727,varname:node_1401,prsc:2|A-4987-V,B-8884-OUT;n:type:ShaderForge.SFN_Multiply,id:8884,x:29599,y:33726,varname:node_8884,prsc:2|A-8589-T,B-4099-OUT;n:type:ShaderForge.SFN_Time,id:8589,x:29389,y:33666,varname:node_8589,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:4099,x:29397,y:33889,ptovrint:False,ptlb:V_Speed_copy,ptin:_V_Speed_copy,varname:_V_Speed_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:1243,x:30533,y:33206,varname:node_1243,prsc:2|A-9704-RGB,B-5494-RGB;n:type:ShaderForge.SFN_ComponentMask,id:8424,x:30810,y:33306,varname:node_8424,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1243-OUT;n:type:ShaderForge.SFN_TexCoord,id:6037,x:31014,y:33083,varname:node_6037,prsc:2,uv:0;n:type:ShaderForge.SFN_Slider,id:461,x:31542,y:33603,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_461,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1.060438,max:2;n:type:ShaderForge.SFN_ValueProperty,id:7268,x:30708,y:33503,ptovrint:False,ptlb:Disturbance,ptin:_Disturbance,varname:node_7268,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Multiply,id:8125,x:30917,y:33462,varname:node_8125,prsc:2|A-7268-OUT,B-6256-OUT;n:type:ShaderForge.SFN_Vector1,id:6256,x:30718,y:33784,varname:node_6256,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:3460,x:31784,y:33248,varname:node_3460,prsc:2|A-6074-RGB,B-6074-A;n:type:ShaderForge.SFN_Power,id:6199,x:32105,y:33307,varname:node_6199,prsc:2|VAL-3460-OUT,EXP-5376-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5376,x:31971,y:33542,ptovrint:False,ptlb:Power,ptin:_Power,varname:node_5376,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;proporder:6074-797-5376-461-9704-5869-3622-5494-5069-4099-7268;pass:END;sub:END;*/

Shader "H2/H_Disturbance_ADD02" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _TintColor ("Color", Color) = (1,1,1,1)
        _Power ("Power", Float ) = 1
        _Alpha ("Alpha", Range(0, 2)) = 1.060438
        _Noise01 ("Noise 01", 2D) = "white" {}
        _U_Speed ("U_Speed", Float ) = 0
        _V_Speed ("V_Speed", Float ) = 0
        _Noise02 ("Noise 02", 2D) = "white" {}
        _U_Speed_copy ("U_Speed_copy", Float ) = 0
        _V_Speed_copy ("V_Speed_copy", Float ) = 0
        _Disturbance ("Disturbance", Float ) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
           // #pragma multi_compile_fwdbase
           // #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
           // #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _TintColor;
            uniform sampler2D _Noise01; uniform float4 _Noise01_ST;
            uniform float _U_Speed;
            uniform float _V_Speed;
            uniform sampler2D _Noise02; uniform float4 _Noise02_ST;
            uniform float _U_Speed_copy;
            uniform float _V_Speed_copy;
            uniform float _Alpha;
            uniform float _Disturbance;
            uniform float _Power;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 node_9982 = _Time + _TimeEditor;
                float4 node_9955 = _Time + _TimeEditor;
                float2 node_2778 = float2(((node_9982.g*_U_Speed)+i.uv0.r),(i.uv0.g+(node_9955.g*_V_Speed)));
                float4 _Noise01_var = tex2D(_Noise01,TRANSFORM_TEX(node_2778, _Noise01));
                float4 node_1896 = _Time + _TimeEditor;
                float4 node_8589 = _Time + _TimeEditor;
                float2 node_9617 = float2(((node_1896.g*_U_Speed_copy)+i.uv0.r),(i.uv0.g+(node_8589.g*_V_Speed_copy)));
                float4 _Noise02_var = tex2D(_Noise02,TRANSFORM_TEX(node_9617, _Noise02));
                float2 node_1540 = (i.uv0+((_Noise01_var.rgb*_Noise02_var.rgb).r*(_Disturbance*0.1)));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_1540, _MainTex));
                float3 node_2393 = (_TintColor.a*i.vertexColor.rgb*_TintColor.rgb*pow((_MainTex_var.rgb*_MainTex_var.a),_Power)*_Alpha);
                float3 emissive = node_2393;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
