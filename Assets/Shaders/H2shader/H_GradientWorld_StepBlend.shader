// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32823,y:32627,varname:node_3138,prsc:2|emission-8418-OUT,alpha-1567-OUT,clip-7230-OUT,olwid-2503-OUT,olcol-9827-RGB;n:type:ShaderForge.SFN_Color,id:7241,x:31519,y:32659,ptovrint:False,ptlb:Color1,ptin:_Color1,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Color,id:2768,x:31533,y:32469,ptovrint:False,ptlb:Color2,ptin:_Color2,varname:node_2768,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:8418,x:32091,y:32653,varname:node_8418,prsc:2|A-2768-RGB,B-7241-RGB,T-8119-OUT;n:type:ShaderForge.SFN_FragmentPosition,id:4995,x:30917,y:32759,varname:node_4995,prsc:2;n:type:ShaderForge.SFN_ComponentMask,id:1020,x:31113,y:32759,varname:node_1020,prsc:2,cc1:1,cc2:-1,cc3:-1,cc4:-1|IN-4995-XYZ;n:type:ShaderForge.SFN_Clamp01,id:8119,x:31754,y:32744,varname:node_8119,prsc:2|IN-7616-OUT;n:type:ShaderForge.SFN_Multiply,id:5979,x:31303,y:32823,varname:node_5979,prsc:2|A-1020-OUT,B-138-OUT;n:type:ShaderForge.SFN_ValueProperty,id:138,x:31051,y:32975,ptovrint:False,ptlb:Gradient_Offset,ptin:_Gradient_Offset,varname:node_138,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Power,id:7616,x:31545,y:32902,varname:node_7616,prsc:2|VAL-5979-OUT,EXP-6712-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6712,x:31285,y:33033,ptovrint:False,ptlb:Gradient_Power,ptin:_Gradient_Power,varname:node_6712,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Color,id:9827,x:32191,y:33263,ptovrint:False,ptlb:OutlineColor,ptin:_OutlineColor,varname:node_9827,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_VertexColor,id:2085,x:32042,y:32779,varname:node_2085,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1567,x:32475,y:32814,varname:node_1567,prsc:2|A-2085-A,B-9827-A;n:type:ShaderForge.SFN_ValueProperty,id:288,x:32071,y:32988,ptovrint:False,ptlb:Outline,ptin:_Outline,varname:node_288,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:2503,x:32339,y:33017,varname:node_2503,prsc:2|A-288-OUT,B-5904-OUT;n:type:ShaderForge.SFN_Vector1,id:5904,x:32035,y:33069,varname:node_5904,prsc:2,v1:0.001;n:type:ShaderForge.SFN_Tex2d,id:8337,x:31826,y:33101,ptovrint:False,ptlb:Diss_Tex,ptin:_Diss_Tex,varname:node_8337,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:fefbf7dfd2152db4f8baa6251c2c29da,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:7230,x:32117,y:33151,varname:node_7230,prsc:2|A-8337-R,B-5995-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5995,x:31833,y:33359,ptovrint:False,ptlb:Diss,ptin:_Diss,varname:node_5995,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;proporder:7241-2768-138-6712-9827-288-5995-8337;pass:END;sub:END;*/

Shader "H2/H_GradientWorld_blend" {
    Properties {
        _Color1 ("Color1", Color) = (1,1,1,1)
        _Color2 ("Color2", Color) = (0.5,0.5,0.5,1)
        _Gradient_Offset ("Gradient_Offset", Float ) = 1
        _Gradient_Power ("Gradient_Power", Float ) = 1
        _OutlineColor ("OutlineColor", Color) = (0.5,0.5,0.5,1)
        _Outline ("Outline", Float ) = 1
        _Diss ("Diss", Float ) = 5
        _Diss_Tex ("Diss_Tex", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            //#pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            //#pragma target 3.0
            uniform float4 _OutlineColor;
            uniform float _Outline;
            uniform sampler2D _Diss_Tex; uniform float4 _Diss_Tex_ST;
            uniform float _Diss;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(float4(v.vertex.xyz + v.normal*(_Outline*0.001),1) );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _Diss_Tex_var = tex2D(_Diss_Tex,TRANSFORM_TEX(i.uv0, _Diss_Tex));
                clip((_Diss_Tex_var.r*_Diss) - 0.5);
                return fixed4(_OutlineColor.rgb,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _Color1;
            uniform float4 _Color2;
            uniform float _Gradient_Offset;
            uniform float _Gradient_Power;
            uniform float4 _OutlineColor;
            uniform sampler2D _Diss_Tex; uniform float4 _Diss_Tex_ST;
            uniform float _Diss;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _Diss_Tex_var = tex2D(_Diss_Tex,TRANSFORM_TEX(i.uv0, _Diss_Tex));
                clip((_Diss_Tex_var.r*_Diss) - 0.5);
////// Lighting:
////// Emissive:
                float3 emissive = lerp(_Color2.rgb,_Color1.rgb,saturate(pow((i.posWorld.rgb.g*_Gradient_Offset),_Gradient_Power)));
                float3 finalColor = emissive;
                return fixed4(finalColor,(i.vertexColor.a*_OutlineColor.a));
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform sampler2D _Diss_Tex; uniform float4 _Diss_Tex_ST;
            uniform float _Diss;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float4 _Diss_Tex_var = tex2D(_Diss_Tex,TRANSFORM_TEX(i.uv0, _Diss_Tex));
                clip((_Diss_Tex_var.r*_Diss) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
