// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.28 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.28;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:False,igpj:False,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:True,fnfb:True;n:type:ShaderForge.SFN_Final,id:4795,x:33092,y:32562,varname:node_4795,prsc:2|emission-2393-OUT,alpha-798-OUT,clip-548-OUT;n:type:ShaderForge.SFN_Tex2d,id:6074,x:30905,y:32549,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:1a1992c76cf9d75479ecc734a7216892,ntxv:0,isnm:False|UVIN-5895-OUT;n:type:ShaderForge.SFN_Multiply,id:2393,x:32657,y:32578,varname:node_2393,prsc:2|A-799-OUT,B-2053-RGB,C-4781-OUT;n:type:ShaderForge.SFN_VertexColor,id:2053,x:31873,y:32778,varname:node_2053,prsc:2;n:type:ShaderForge.SFN_Color,id:797,x:31262,y:32709,ptovrint:True,ptlb:Color,ptin:_TintColor,varname:_TintColor,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Multiply,id:798,x:32427,y:32846,varname:node_798,prsc:2|A-1680-OUT,B-2053-A;n:type:ShaderForge.SFN_Fresnel,id:2488,x:29759,y:31722,varname:node_2488,prsc:2|EXP-5008-OUT;n:type:ShaderForge.SFN_Multiply,id:9737,x:30647,y:31953,varname:node_9737,prsc:2|A-9886-OUT,B-2534-OUT,C-2978-RGB;n:type:ShaderForge.SFN_ValueProperty,id:2534,x:30258,y:31949,ptovrint:False,ptlb:Fresnel_ZT,ptin:_Fresnel_ZT,varname:node_2534,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:1111,x:29751,y:31876,ptovrint:False,ptlb:Fresnel_Power,ptin:_Fresnel_Power,varname:node_1111,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Add,id:9774,x:31983,y:32424,varname:node_9774,prsc:2|A-44-OUT,B-5316-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4781,x:32451,y:32777,ptovrint:False,ptlb:ZT,ptin:_ZT,varname:node_4781,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Color,id:2978,x:30250,y:32043,ptovrint:False,ptlb:Fresnel_Color,ptin:_Fresnel_Color,varname:node_2978,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:5316,x:31700,y:32484,varname:node_5316,prsc:2|A-3359-OUT,B-797-RGB;n:type:ShaderForge.SFN_ValueProperty,id:1680,x:32214,y:32770,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_1680,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Power,id:9886,x:30045,y:31759,varname:node_9886,prsc:2|VAL-2488-OUT,EXP-1111-OUT;n:type:ShaderForge.SFN_Tex2d,id:9054,x:31876,y:33005,ptovrint:False,ptlb:DISS_TEX,ptin:_DISS_TEX,varname:node_9054,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:f32d8cbdfa3df5e48a885c48e037a62b,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Step,id:548,x:32430,y:33027,varname:node_548,prsc:2|A-7116-OUT,B-1095-OUT;n:type:ShaderForge.SFN_Slider,id:1095,x:31931,y:33253,ptovrint:False,ptlb:DISS,ptin:_DISS,varname:node_1095,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_SwitchProperty,id:44,x:30880,y:32023,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:node_44,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-9737-OUT,B-6858-OUT;n:type:ShaderForge.SFN_Vector1,id:6858,x:30626,y:32125,varname:node_6858,prsc:2,v1:0;n:type:ShaderForge.SFN_OneMinus,id:7262,x:31025,y:32306,varname:node_7262,prsc:2|IN-9886-OUT;n:type:ShaderForge.SFN_Multiply,id:3359,x:31348,y:32433,varname:node_3359,prsc:2|A-7262-OUT,B-6074-RGB,C-6074-A;n:type:ShaderForge.SFN_Append,id:5895,x:30698,y:32579,varname:node_5895,prsc:2|A-7389-OUT,B-5467-OUT;n:type:ShaderForge.SFN_TexCoord,id:9036,x:30247,y:32340,varname:node_9036,prsc:2,uv:0;n:type:ShaderForge.SFN_Add,id:7389,x:30481,y:32533,varname:node_7389,prsc:2|A-8649-OUT,B-9036-U;n:type:ShaderForge.SFN_Multiply,id:8649,x:30221,y:32508,varname:node_8649,prsc:2|A-4787-TSL,B-1364-OUT;n:type:ShaderForge.SFN_Time,id:4787,x:29904,y:32496,varname:node_4787,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:1364,x:29904,y:32645,ptovrint:False,ptlb:U_Speed01,ptin:_U_Speed01,varname:node_961,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:10;n:type:ShaderForge.SFN_Add,id:5467,x:30479,y:32755,varname:node_5467,prsc:2|A-9036-V,B-9840-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7806,x:29923,y:32820,ptovrint:False,ptlb:V_Speed01,ptin:_V_Speed01,varname:_node_961_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:8;n:type:ShaderForge.SFN_Multiply,id:9840,x:30228,y:32809,varname:node_9840,prsc:2|A-4787-TSL,B-7806-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5008,x:29490,y:31936,ptovrint:False,ptlb:Fresnel_exp,ptin:_Fresnel_exp,varname:node_5008,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Posterize,id:799,x:32289,y:32496,varname:node_799,prsc:2|IN-9774-OUT,STPS-4876-OUT;n:type:ShaderForge.SFN_SwitchProperty,id:7116,x:32158,y:32976,ptovrint:False,ptlb:Diss_par,ptin:_Diss_par,varname:node_7116,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-2053-A,B-9054-RGB;n:type:ShaderForge.SFN_Vector1,id:4876,x:32091,y:32624,varname:node_4876,prsc:2,v1:2;proporder:6074-797-1364-7806-4781-44-2978-5008-2534-1111-1680-9054-1095-7116;pass:END;sub:END;*/

Shader "H2/H_Freaks_BLe03" {
    Properties {
        _MainTex ("MainTex", 2D) = "white" {}
        _TintColor ("Color", Color) = (1,0,0,1)
        _U_Speed01 ("U_Speed01", Float ) = 10
        _V_Speed01 ("V_Speed01", Float ) = 8
        _ZT ("ZT", Float ) = 1
        [MaterialToggle] _Fresnel ("Fresnel", Float ) = 0
        _Fresnel_Color ("Fresnel_Color", Color) = (0,0,1,1)
        _Fresnel_exp ("Fresnel_exp", Float ) = 2
        _Fresnel_ZT ("Fresnel_ZT", Float ) = 1
        _Fresnel_Power ("Fresnel_Power", Float ) = 0.5
        _Alpha ("Alpha", Float ) = 1
        _DISS_TEX ("DISS_TEX", 2D) = "white" {}
        _DISS ("DISS", Range(0, 1)) = 1
        [MaterialToggle] _Diss_par ("Diss_par", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            //#pragma multi_compile_fwdbase_fullshadows
            //#pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float4 _TintColor;
            uniform float _Fresnel_ZT;
            uniform float _Fresnel_Power;
            uniform float _ZT;
            uniform float4 _Fresnel_Color;
            uniform float _Alpha;
            uniform sampler2D _DISS_TEX; uniform float4 _DISS_TEX_ST;
            uniform float _DISS;
            uniform fixed _Fresnel;
            uniform float _U_Speed01;
            uniform float _V_Speed01;
            uniform float _Fresnel_exp;
            uniform fixed _Diss_par;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _DISS_TEX_var = tex2D(_DISS_TEX,TRANSFORM_TEX(i.uv0, _DISS_TEX));
                clip(step(lerp( i.vertexColor.a, _DISS_TEX_var.rgb, _Diss_par ),_DISS) - 0.5);
////// Lighting:
////// Emissive:
                float node_9886 = pow(pow(1.0-max(0,dot(normalDirection, viewDirection)),_Fresnel_exp),_Fresnel_Power);
                float4 node_4787 = _Time + _TimeEditor;
                float2 node_5895 = float2(((node_4787.r*_U_Speed01)+i.uv0.r),(i.uv0.g+(node_4787.r*_V_Speed01)));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(node_5895, _MainTex));
                float node_4876 = 2.0;
                float3 emissive = (floor((lerp( (node_9886*_Fresnel_ZT*_Fresnel_Color.rgb), 0.0, _Fresnel )+(((1.0 - node_9886)*_MainTex_var.rgb*_MainTex_var.a)*_TintColor.rgb)) * node_4876) / (node_4876 - 1)*i.vertexColor.rgb*_ZT);
                float3 finalColor = emissive;
                return fixed4(finalColor,(_Alpha*i.vertexColor.a));
            }
            ENDCG
        }
    }
    CustomEditor "ShaderForgeMaterialInspector"
}
