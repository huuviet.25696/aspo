#ifndef IDSDK_APPLE
#define IDSDK_APPLE
#include <Availability.h>
#ifdef __MAC_OS_X_VERSION_MAX_ALLOWED
#import <Cocoa/Cocoa.h>
#else
#import <UIKit/UIKit.h>
#endif

#include "platform_detect.h"

#ifdef __MAC_OS_X_VERSION_MAX_ALLOWED
@interface NSImageView(Extend){
#else
    @interface UIImageView(Extend){
#endif
        
    }
    - (void) generateQRCode: (NSString*) info;
    @end
    
    @protocol IdSdkEvents
    @required
    - (void) onLoginSuccess:(NSString*) msg data:(NSString*) data;
    - (void) onLoginFail:(NSString*) msg data:(NSString*) data;
    - (void) onGetUrlContent:(NSString*) url content:(NSData*) data;
    - (void) onRequestQuickPlay:(NSString*) quick_username;
    - (void) onDownloadingFromUrl:(double) percent url:(NSString*) url file:(NSURL*) fileUrl;
    - (void) onDownloadedFromUrl:(int) result url:(NSString*) url file:(NSURL*) fileUrl error:(NSString*) error;
    - (void) onRequestNemoLogin;
    //- (void) onForgotPasswordCalled:(NSString*) msg data:(NSString*) data;
    //- (void) onRegisterCalled:(NSString*) msg data:(NSString*) data;
    //@optional
    //- (void) onLogoutCallback:(NSString*) msg data:(NSString*) data;
    //- (void) onRegisterSuccess:(NSString*) msg data:(NSString*) data;
    //- (void) onRegisterFail:(NSString*) msg data:(NSString*) data;
    //- (void) onPostSuccess:(NSString*) msg data:(NSString*) data;
    //- (void) onPostFail:(NSString*) msg data:(NSString*) data;
    //- (void) onGetSuccess:(NSString*) msg data:(NSString*) data;
    //- (void) onGetFail:(NSString*) msg data:(NSString*) data;
    @end
    
    @protocol IdSdkPaymentEvents
    @required
    - (void) onNemoPayment:(NSString*) msg data:(NSString*) data;
    - (void) onErrorPayment:(NSString*) msg;
    @end
    
    @interface IdSdk: NSObject
    {
        NSMutableSet *nemoProductIds;
    }
    @property (nonatomic, weak) id <IdSdkEvents> delegate;
    @property (nonatomic, weak) id <IdSdkPaymentEvents> paymentDelegate;
    +(IdSdk *) getInstance;
    + (void) init;
    + (void) initWithAppId: (NSString *) appId;
    + (NSString *) getAppId;
    + (NSString *) getUsername;
    + (NSString *) getGameAccounts;
    + (NSString *) getGameUsername;
    + (void) setGameUsername:(NSString *) username;
    + (void) setUsername:(NSString *) username;
    + (NSString *) getGameMessage;
    + (NSString *) getGameBindUrl;
    + (NSString *) getServerUrl;
    + (NSString *) getLoginData;
    + (NSString *) getDeviceTokenString;
    + (void) removeLoginData;
    
    + (void) setDelegate:(id <IdSdkEvents>)aDelegate;
    + (void) setPaymentDelegate:(id <IdSdkPaymentEvents>)aDelegate;
    + (id <IdSdkEvents>)getDelegate;
    + (id <IdSdkPaymentEvents>)getPaymentDelegate;
    + (void) setServer:(NSString *) url;
    + (void) setGameGatewayServer:(NSString *) url;
    + (void) track;
    //
    + (void) getUrlContent:(NSString *)url;
    + (void) downloadFromUrl:(NSString *)url saveTo: (NSURL *)fileUrl;
    + (void) cancelDownload;
    + (void) pauselDownload;
    + (void) resumeDownload;
    + (bool) isDownloading;
    + (char*) getCurrentDownloadUrl;
    + (char*) getCurrentDownloadFilePath;
    //Utils
    + (char*) md5: (const char *) str;
    + (char*) getBundleId;
    + (char*) getBundleVersion;
    + (char*) getBundleNumber;
    + (char*) getOsVersion;
    + (char*) getDeviceToken;
    + (char*) loadKeyChainData:(NSString*) key;
    + (void) saveKeychainData:(NSString *) key  value:(NSString *) value;
    //Socket
    + (void) setSocketServer:(NSString *) url port:(int) port useSSL:(BOOL) use_ssl;
    + (void) setAutoReconnect: (BOOL) auto_reconnect;
    + (void) connect;
    //IAP
    //#ifdef __MAC_OS_X_VERSION_MAX_ALLOWED
    //#else
    + (NSMutableSet *) getNemoProductIds;
    + (void) NEMO_IAP_initWithProductIdentifiers: (NSMutableArray<NSString*>*) productsIdentifier_;
    + (void) NEMO_IAP_addNemoProductIdentifier: (NSString*) nemoProductIdentifier;
    + (void) IAP_buyDynamicPackage:(const char *) jsonPackage;
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    + (void) IAP_initWithProductIdentifiers: (NSMutableArray<NSString*>*) productsIdentifier_;
    + (void) IAP_reload;
    + (void) IAP_restoreProducts;
    + (void) IAP_confirmRestorePurchased;
    + (void) IAP_buyPackage: (const char *)productIdentifier_;
    + (void) IAP_buyPackage_ex: (const char *)productIdentifier_
gameId:(const char*) gameId
username:(const char *) username
gameUsername: (const char*) gameUsername
sourceOrderId:(const char *) sourceOrderId
productCode:(const char*) productCode;
    + (float) IAP_getProductPrice:(const char *)productIdentifier_;
    + (const char*) IAP_getProductPriceCurrency:(const char *)productIdentifier_;
    + (const char*) IAP_getProductPriceCurrencySymbol:(const char *)productIdentifier_;
    + (bool) IAP_checkPurchased:(const char *)productIdentifier_;
#endif
    @end
    //
    @interface IdSdk(Extra){
        
    }
    + (void) requestQuickPlay;
    + (void) showLogin;
    + (void) showLoginNemo;
    + (void) hideLogin;
    + (void) openLink:(NSString *)url;
    + (void) showMessage:(NSString *) msg ;
    + (void) showMessage:(NSString *) msg buttonText:(NSString *) text linkUrl:(NSString *) url ;
    //#ifdef __MAC_OS_X_VERSION_MAX_ALLOWED
    //#else
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    + (void) showMessage:(NSString *) msg okButtonText:(NSString *) okText cancelButtonText:(NSString *) cancelText linkUrl:(NSString *) url ;
    + (void) showMessage:(NSString *) msg buttonText:(NSString *) text linkMetaMaskUrl:(NSString *) url ;
#endif
    //#ifdef __MAC_OS_X_VERSION_MAX_ALLOWED
    //#else
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    + (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
    + (void)applicationDidBecomeActive:(UIApplication *)application;
    + (BOOL)application:(UIApplication *)application
openURL:(NSURL *)url
options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options;
#endif
    @end
    
    
#endif /* IDSDK_APPLE */
    
    
    
