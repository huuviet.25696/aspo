//
//  idsdk_apple_ext.h
//  idsdk
//
//  Created by  Nhat Nguyen on 22/05/2022.
//

#ifndef idsdk_apple_ext_h
#define idsdk_apple_ext_h
#import <Foundation/Foundation.h>
#import "idsdk_apple.h"
@interface IdSdk (){
    
}
+ (void) requestFacebookPairing;
+ (void) requestGooglePairing;
+ (void) requestApplePairing;
+ (void) requestNemoPairing;
+ (void) requestPairing;
+ (void) pollingPairing:(const char*) sid;
+ (void) stopRequestPairing;
+ (void) doRegister:(NSString *) username email:(NSString *) email password:(NSString *) password;
+ (void) doForgotPassword:(NSString *) username;
+ (void) doNormalLogin:(NSString *) username password: (NSString*) password;
+ (void) doGoogleLogin:(NSString *) authToken;
+ (void) doFacebookLogin:(NSString *) authToken;
+ (void) doAppleLogin:(NSString *) authToken;
+ (void) doNemoLogin: (NSString *) accessToken;
+ (void) registerCallback:(NSString*) msg data:(NSString*) data;
+ (void) forgotPasswordCallback:(NSString*) msg data:(NSString*) data;
+ (void) loginSuccessCallback:(NSString*) msg data:(NSString*) data;
+ (void) loginFailCallback:(NSString*) msg data:(NSString*) data;
@end

#endif /* idsdk_apple_ext_h */
