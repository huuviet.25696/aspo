#import "CustomAppDelegate.h"
#import <AppTrackingTransparency/AppTrackingTransparency.h>
#include <iostream>
#include <vector>
// ------------------------------------------------// ------------------------------------------------
// ------------------------------------------------// ------------------------------------------------
#define DEBUG_MODE 1
UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;
UIKIT_EXTERN NSString *const IAPHelperProductRestoredNotification;
UIKIT_EXTERN NSString *const IAPHelperProductCanceledNotification;
UIKIT_EXTERN NSString *const IAPHelperAllProductRestoredNotification;
UIKIT_EXTERN NSString *const IAPHelperProductFailNotification;
UIKIT_EXTERN NSString *const IAPHelperProductReloadFromStoreNotification;
// ------------------------------------------------// ------------------------------------------------
// ------------------------------------------------// ------------------------------------------------
@implementation GameUtils

+(NSString*) createNSString:(const char*)string {
    if(string)
        return [NSString stringWithUTF8String:string];
    else
        return [NSString stringWithUTF8String:""];
}

+(char*) createCString:(const char *)string {
    if(string == NULL) return NULL;
    
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    
    return res;
}
+ (void) sendSdkMessage:(NSString *)UnityClass method: (NSString *) classMethod message: (NSString*) message{
    //+(void) sendSdkMessage:(NSString*)sdkMessage {
    UnitySendMessage([UnityClass UTF8String], [classMethod UTF8String], [message UTF8String]);
}

@end
// ------------------------------------------------// ------------------------------------------------
// ------------------------------------------------// ------------------------------------------------
@interface CustomAppDelegate ()<UNUserNotificationCenterDelegate,IdSdkEvents,IdSdkPaymentEvents>
{
    NSString    *userID;
    NSString    *access_token;
    
}
+ (CustomAppDelegate*) instance;
@end
// ------------------------------------------------// ------------------------------------------------
// ------------------------------------------------// ------------------------------------------------
@implementation CustomAppDelegate
+ (CustomAppDelegate*) instance {
    CustomAppDelegate *delegate = (CustomAppDelegate *)[[UIApplication sharedApplication] delegate];
    return delegate;
}

//- (void)startUnity:(UIApplication*)application{
//    [super startUnity:application];
//}
- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions
{
    NSLog(@"[CustomAppDelegate application:%@ didFinishLaunchingWithOptions:%@]", application, launchOptions);
    [super application:application didFinishLaunchingWithOptions:launchOptions];
    
    //--------- Custom UIViewController --------
    //    self->z77UIViewController = [[Z77UIViewController alloc]initWithNibName:@"Z77UIViewController" bundle:nil];
    //    _window.rootViewController = self->z77UIViewController;
    //    //------------------------------------------
    //    [_window makeKeyAndVisible];
    //    UIWindow *window = [self getTopApplicationWindow];
    //    window = _window;
    //init SDK
    /*
    [NftSdk init];
    [NftSdk changeDefaultServer:@"https://proxy.aspo.world"];
    [NftSdk setDelegate:self];
    [NftSdk track];
     */
//    std::vector<std::string> productsIdentifier_ = {
//        "com.aspoworld.ios.exp",
//        "com.aspoworld.ios.kimcuong",
//        "com.aspoworld.ios.vukhi",
//        "com.aspoworld.ios.trangbi",
//        "com.aspoworld.ios.nguyenlieu",
//        "com.aspoworld.ios.dakham",
//        "com.aspoworld.ios.ngulinh",
//        "com.aspoworld.ios.tanthu",
//        "com.aspoworld.ios.trungcap",
//        "com.aspoworld.ios.caocap"
//    };
//    NSMutableArray *productsIdentifier_ = [[NSMutableArray alloc]initWithArray:@[
//        @"com.aspoworld.ios.exp",
//        @"com.aspoworld.ios.kimcuong",
//        @"com.aspoworld.ios.vukhi",
//        @"com.aspoworld.ios.trangbi",
//        @"com.aspoworld.ios.nguyenlieu",
//        @"com.aspoworld.ios.dakham",
//        @"com.aspoworld.ios.ngulinh",
//        @"com.aspoworld.ios.tanthu",
//        @"com.aspoworld.ios.trungcap",
//        @"com.aspoworld.ios.caocap"
//    ]];
    [self turnOnIAPNotification];
    [IdSdk init];
    [IdSdk setDelegate:self];
//    [IdSdk setServer:@"https://id-mecha.oneteam.vn"];
//    [IdSdk setGameGatewayServer:@"https://proxy.aspo.world"];
//    [IdSdk IAP_initWithProductIdentifiers:productsIdentifier_];
//    [IdSdk IAP_reload];
//  for SDK, use when login with social
    [IdSdk application:application didFinishLaunchingWithOptions:launchOptions];
    
    [[NemoSDK sharedInstance] sdkInit];
    [NemoSDK sharedInstance].loginDelegate = self;
    [[NemoSDKTracking sharedInstance] applicationDelegate:self andApplication:application didFinishLaunchingWithOptions:launchOptions];
    return true;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    //for SDK, use when login with social
    [IdSdk application:application openURL:url options:options];
    return YES;
}

// ------------------------------------------------
//- (void) showLogin
//{
//    NSLog(@"showLogin");
//    [NftSdk showLogin];
//}

// ------------------------------------------------
#pragma mark Optional
//- (BOOL)application:(UIApplication *)application continueUserActivity:(NSUserActivity *)userActivity restorationHandler:(void (^)(NSArray<id<UIUserActivityRestoring>> * _Nullable))restorationHandler
//{
//    //[super application: application continueUserActivity: userActivity restorationHandler:restorationHandler];
//        //for SDK
//}
// ------------------------------------------------
#pragma mark Optional
//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
//{
//    [super application: application didRegisterForRemoteNotificationsWithDeviceToken: deviceToken];
//        //for SDK
//}
//
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
//{
//    [super application: application didReceiveRemoteNotification: userInfo fetchCompletionHandler:completionHandler];
//        //for SDK
//}
//
//- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
//        //NSLog(@"APNs Unable to register for remote notifications: %@", error);
//    [super application: application didFailToRegisterForRemoteNotificationsWithError: error];
//}
//
- (void)applicationWillEnterForeground:(UIApplication*)application{
    ::printf("-> applicationWillEnterForeground()\n");
    //for SDK
    [IdSdk track];
    [self turnOnIAPNotification];
    [super applicationWillEnterForeground: application];
}
- (void)applicationDidEnterBackground:(UIApplication *)application{
    [self turnOffIAPNotification];
    [super applicationDidEnterBackground: application];
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [super applicationDidBecomeActive: application];
//    if (@available(iOS 14, *)) {
//        ::printf("ATTrackingManager requestTrackingAuthorizationWithCompletionHandler");
//        [ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
//        }];
//    } else {
//        ::printf("no > ios 14");
//    }
    [IdSdk applicationDidBecomeActive:application];
    [[NemoSDKTracking sharedInstance] initTracking:application];
    
}
//
//- (void)applicationWillTerminate:(UIApplication *)application {
//    [super applicationWillTerminate: application];
//        //for SDK
//}

//========================
/*
#pragma mark NFTSDKCallback
- (void)onLoginSuccess:(NSString *)msg data:(NSString *)data {
    dispatch_block_t block = ^{
        NSLog(@"loginSuccess: %@ - %@", msg, data);
        NSString *sdkMessage = [NSString stringWithFormat:@"%@", data];
        UnitySendMessage("DuoSDK", "onLoginSuccess", [sdkMessage UTF8String]);
        
    };
    //
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
    //[NftSdk track];
}
 */

#pragma mark IDSDKCallback
- (void)onLoginSuccess:(NSString *)msg data:(NSString *)data {
    dispatch_block_t block = ^{
        NSLog(@"loginSuccess: %@ - %@", msg, data);
        if ([[IdSdk getGameUsername] isEqual:@""]){
            [IdSdk showMessage:[IdSdk getGameMessage] buttonText:@"Link now" linkUrl:[IdSdk getGameBindUrl]];
        }else{
            NSString *sdkMessage = [NSString stringWithFormat:@"{\"username\":\"%@\"}", [IdSdk getGameUsername]];
            NSLog(@"sdkMessage: %@", sdkMessage);
            UnitySendMessage("DuoSDK", "onLoginSuccess", [sdkMessage UTF8String]);
        }
        [IdSdk hideLogin];
        
    };
    //
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
    
    
}
- (void) onLoginFail:(NSString*) msg data:(NSString*) data{
    [IdSdk showMessage:msg];
    [NemoSDK sharedInstance].loginDelegate = self;
    [[NemoSDK sharedInstance] logout];
    //NSLog(@"onLoginFail: %@, %@",msg, data);
}
- (void)onGetUrlContent:(NSString *)url content:(NSData *)data {
    dispatch_block_t block = ^{
        NSString* aStr= [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
        //{"status":0,"message":"This game does not supprot quickplay, please login to play game.","data":""}
        NSLog(@"onGetUrlContent: %@",aStr);
        NSError *error;
        //NSData *jsonData = [jsonMsg dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        if(error !=nil){
            NSString *msg = [NSString stringWithFormat:@"%@", [error description]];
            [IdSdk showMessage:msg];
            return;
        }
        NSInteger status = [[jsonDict valueForKey:@"status"] integerValue];
        NSString* message = [NSString stringWithFormat:@"%@", [jsonDict valueForKey:@"message"]];
        NSString* device_token = [NSString stringWithFormat:@"%@", [jsonDict valueForKey:@"device_token"]];
        if(status == 0){
            [IdSdk showMessage:message];
        }else{
            NSString *sdkMessage = [NSString stringWithFormat:@"{\"username\":\"quick@%@\"}", device_token];
            NSLog(@"sdkMessage: %@", sdkMessage);
            UnitySendMessage("DuoSDK", "onLoginSuccess", [sdkMessage UTF8String]);
            [IdSdk hideLogin];
        }
    };
    //
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
    
}

- (void)onDownloadedFromUrl:(int)result url:(NSString *)url file:(NSURL *)fileUrl error:(NSString *)error {
   
}


- (void)onDownloadingFromUrl:(double)percent url:(NSString *)url file:(NSURL *)fileUrl {
    
}

- (void)onRequestQuickPlay:(NSString *)quick_username {
    [NemoSDK sharedInstance].loginDelegate = nil;
    [IdSdk hideLogin];
    
    NSString *sdkMessage = [NSString stringWithFormat:@"{\"username\":\"%@\"}", [IdSdk getGameUsername]];
    NSLog(@"sdkMessage: %@", sdkMessage);
    UnitySendMessage("DuoSDK", "onLoginSuccess", [sdkMessage UTF8String]);
}

- (void)onRequestNemoLogin {
    NSLog(@"onRequestNemoLogin");
    [NemoSDK sharedInstance].loginDelegate = self;
    [[NemoSDK sharedInstance] logout];
    [[NemoSDK sharedInstance] login];
}



- (void) onForgotPasswordCalled:(NSString*) msg data:(NSString*) data{
    [IdSdk showMessage:msg];
    //NSLog(@"onLoginFail: %@, %@",msg, data);
}

- (void) onRegisterCalled:(NSString*) msg data:(NSString*) data{
    [IdSdk showMessage:msg];
    //NSLog(@"onLoginFail: %@, %@",msg, data);
}
// ------------------------------------------------
#pragma mark UNUserNotificationCenterDelegate
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(nonnull UNNotification *)notification withCompletionHandler:(nonnull void (^)(UNNotificationPresentationOptions))completionHandler
API_AVAILABLE(ios(10.0))
{
    NSDictionary *userInfo = notification.request.content.userInfo;
    // Print full message.
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionBadge);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(nonnull UNNotificationResponse *)response withCompletionHandler:(nonnull void (^)(void))completionHandler
API_AVAILABLE(ios(10.0))
{
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    completionHandler();
}

#pragma Custom

- (UIWindow*) getTopApplicationWindow
{
    // grabs the top most window
    NSArray* windows = [[UIApplication sharedApplication] windows];
    return ([windows count] > 0) ? windows[0] : nil;
}


- (UIViewController*) getTopViewController
{
    // get the top most window
    UIWindow *window = self->_window;//[self getTopApplicationWindow];
    
    // get the root view controller for the top most window
    UIViewController *vc = window.rootViewController;
    
    // check if this view controller has any presented view controllers, if so grab the top most one.
    while (vc.presentedViewController != nil)
    {
        // drill to topmost view controller
        vc = vc.presentedViewController;
    }
    
    return vc;
}

- (void) showMessage: (NSString *) title message:(NSString *) message controller:(UIViewController*) ctrl {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [ctrl presentViewController:alert animated:YES completion:nil];
}

- (void) turnOffIAPNotification{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void) turnOnIAPNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productRestored:) name:IAPHelperProductRestoredNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productCanceled:) name:IAPHelperProductCanceledNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productRestoredAll:) name:IAPHelperAllProductRestoredNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productFail:) name:IAPHelperProductFailNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productReloadFromStore:) name:IAPHelperProductReloadFromStoreNotification object:nil];
}


#pragma --
#pragma In App Purchased Callback
- (void)productPurchased:(NSNotification *)notification {
    NSLog(@"productPurchased NSNotification!");
    
}
- (void)productCanceled:(NSNotification *)notification {
    NSLog(@"productCanceled NSNotification!");
    
}
- (void)productFail:(NSNotification *)notification {
    NSLog(@"productFail NSNotification!");
}
- (void)productRestored:(NSNotification *)notification {
    NSLog(@"productRestored NSNotification!");
    
}
- (void)productRestoredAll:(NSNotification *)notification {
    NSLog(@"productRestoredAll NSNotification!");
    
}
- (void) productReloadFromStore:(NSNotification *)notification {
    NSLog(@"productReloadFromStore NSNotification!");
    
}

//NEMO SDL
- (void)onLoginFailure:(NSString *)message {
    NSLog(@"onLoginFailure:%@",message);
}

- (void)onLoginSuccess:(NSString *)access_token_or_msg andIdToken:(NSString *) id_token_or_data {
    NSLog(@"onLoginSuccess:access_token:%@, id_token:%@",access_token_or_msg, id_token_or_data);
//    NSString *userInfo = [[NemoSDK sharedInstance] getUserInfo];
//    NSLog(@"userInfo:%@",userInfo);
//    [IdSdk doNemoLogin:access_token];
//    /*
//     onLoginSuccess:access_token:TVWQIeQgkXUU1FPy1ZS4PqyNMn7CYL6KqbN8iZtiegl, id_token:eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjRkNUdPY293M2lQa1R6SndBZG5kYlFRR2dHNTRucy1JQ1JlaXRYcGFPSDQifQ.eyJzdWIiOiI2MzgwNmI2NDgxN2Y3OThlNmRhYTkxYzUiLCJub25jZSI6InBnRk9rZHlsXy1WYldaLXByYlNpMmxMd1ZQMi13OFo4QV9JaUdtYUhONFkiLCJhdF9oYXNoIjoiWVVfdEhibER3akJmcnlJeXZxTnUtUSIsImF1ZCI6Im1hcnN3YXJfbmVtb3ZlcnNlIiwiZXhwIjoxNjY5MzY0MDkxLCJpYXQiOjE2NjkzNjA0OTEsImlzcyI6Imh0dHBzOi8vZ2lkLXVhdC5uZW1vdmVyc2UuaW8ifQ.f4u0OX2Z6amcnxeYLZ5-ZNgI6udY-BwOmh_QrxbXkYyiCQDffH_5sXhF05YSZEJMrTBbhL3uzSfIu7a0m1XwApSmPlD-2k9L8Fu6aKu6wGvJpdVsgpBn_M4_N2i6fMm0KXGoADx9AypJaZl-tBFFeZRHIi1OQPzj72x5u0pzXe2k-LskgZTcKMD6ayU4YGLOar4_xPwwLcaNfGSQXEMNEM6XO_AwneBG9Z-7FoB-dGpDFMpz07hp4yaTgRbnuUUpFybWdY6zGg8lwATb2CAwa7ZF2zaMVQCUsIAlHu_Kbq3RyzgtzvypFz6sVD1fPHeQtIgmxb-x0jLFxMTWLxuH8Q
//     userInfo:{
//     "gender" : "",
//     "phone_number_verified" : false,
//     "email_verified" : true,
//     "sub" : "63806b64817f798e6daa91c5",
//     "profile_picture" : "https:\/\/lh3.googleusercontent.com\/a\/ALm5wu0pzWjagCKma5Ho_GrurGzmtDkyK90m5sOuJa2e=s96-c",
//     "email" : "zozapxv@gmail.com",
//     "phone_number" : "",
//     "name" : "Technical Collection"
//     }
//     */
    dispatch_block_t block = ^{
//        if ([[IdSdk getGameUsername] isEqual:@""]){
//            [IdSdk showMessage:[IdSdk getGameMessage] buttonText:@"Link now" linkUrl:[IdSdk getGameBindUrl]];
//        }else{
//            NSString *sdkMessage = [NSString stringWithFormat:@"{\"username\":\"%@\"}", [IdSdk getGameUsername]];
//            NSLog(@"sdkMessage: %@", sdkMessage);
//            UnitySendMessage("DuoSDK", "onLoginSuccess", [sdkMessage UTF8String]);
//        }
        if ([NemoSDK sharedInstance].loginDelegate !=nil){
            [NemoSDK sharedInstance].loginDelegate = nil;
            [IdSdk doNemoLogin: access_token_or_msg];
        }else{
            [NemoSDK sharedInstance].loginDelegate = self;
            [IdSdk hideLogin];
            
            NSString *sdkMessage = [NSString stringWithFormat:@"{\"username\":\"%@\"}", [IdSdk getGameUsername]];
            NSLog(@"sdkMessage: %@", sdkMessage);
            UnitySendMessage("DuoSDK", "onLoginSuccess", [sdkMessage UTF8String]);
        }
        
    };
    //
    if ([NSThread isMainThread]) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

- (void)onLogoutFailure:(NSString *)message {
    NSLog(@"onLogoutFailure:%@",message);
}

- (void)onLogoutSuccess:(NSString *)message {
    NSLog(@"onLogoutSuccess:%@",message);
}

- (void)onErrorPayment:(NSString *)msg {
    NSLog(@"onErrorPayment:%@",msg);
}

- (void)onNemoPayment:(NSString *)msg data:(NSString *)data {
    NSLog(@"onNemoPayment:%@",msg);
//    [IdSdk showMessage:msg];
}

@end

//  - - - - - - - - - - - - - - - End - - - - - - - - - - - - - -


extern "C"
{
void showLoginIOS(){
//    [[NemoSDK sharedInstance] logout];
    [IdSdk showLoginNemo];
    }
void buyPackage(const char* jsonPackage){
//    const char *jsonPackage = "{\"source_order_id\":\"123456\" ,\"product_code\":\"5678\",\"product_id\":\"com.marswar.nemo.exp\"}";
    [IdSdk IAP_buyDynamicPackage:jsonPackage];
}
void callAFTracking(const char* jsonContent){

    NSData *data = [NSData dataWithBytes:jsonContent length:strlen(jsonContent)];
    NSError *error;
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if(error !=nil){
        NSString *msg = [NSString stringWithFormat:@"%@", [error description]];
        [IdSdk showMessage:msg];
        return;
    }
    NSString* event = [NSString stringWithFormat:@"%@", [jsonDict valueForKey:@"event"]];
    
    NSMutableDictionary* received_params = [jsonDict objectForKey:@"params"];
    
    
    NSMutableDictionary *params =  [[NSMutableDictionary alloc] init];
    if([SdkConfig sharedInstance].email !=nil) {
        [params setValue:[SdkConfig sharedInstance].email forKey:@"af_content"];
    }else{
        [params setValue:@"" forKey:@"af_content"];
    }
    if([SdkConfig sharedInstance].email !=nil){
        [params  setValue:[SdkConfig sharedInstance].email forKey:@"af_customer_user_email"];
    }else{
        [params setValue:@"" forKey:@"af_customer_user_email"];
    }
    if([SdkConfig sharedInstance].sub !=nil) {
        [params setValue:[SdkConfig sharedInstance].sub forKey:@"af_customer_user_id"];
    }else{
//        NSString *af_customer_user_id = [NSString stringWithFormat:@"%@", [IdSdk getGameUsername]];
        [params setValue: [IdSdk getGameUsername] forKey:@"af_customer_user_id"];
    }
    
    [params addEntriesFromDictionary: received_params];
    
    NSData *jsonParamsData = [NSJSONSerialization dataWithJSONObject:params
                                                       options: 0
                                                         error:&error];
    if(error !=nil){
        NSString *msg = [NSString stringWithFormat:@"%@", [error description]];
        [IdSdk showMessage:msg];
        return;
    }
      
    
    NSString *jsonParamsValue = [[NSString alloc] initWithData:jsonParamsData encoding:NSUTF8StringEncoding];

    NSLog(@"callAFTracking:event:%@, value:%@",event,jsonParamsValue);
    
    [[NemoSDKTracking AppsFlyer] trackingEventOnAF:event withValues:@{
        @"params": jsonParamsValue
    }];
}
void addIAPNemoProductIdentifier(const char* productId){
    [IdSdk NEMO_IAP_addNemoProductIdentifier:[GameUtils createNSString:productId]];
}

void initSDK(const char* appId,const char* server){
    [IdSdk initWithAppId:[GameUtils createNSString:appId]];
    [IdSdk setServer:[GameUtils createNSString:server]];
}
}
/*
 com.aspoworld.ios.exp
com.aspoworld.ios.kimcuong
com.aspoworld.ios.vukhi
com.aspoworld.ios.trangbi
com.aspoworld.ios.nguyenlieu
com.aspoworld.ios.dakham
com.aspoworld.ios.ngulinh
com.aspoworld.ios.tanthu
com.aspoworld.ios.trungcap
com.aspoworld.ios.caocap
 */
