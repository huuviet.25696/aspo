#import "UnityAppController.h"
#import <UserNotifications/UserNotifications.h>
#import "idsdk_apple.h"
#import "idsdk_apple_ext.h"
#import <NemoSDK/NemoSDK.h>
#import <NemoSDK/SdkConfig.h>
#import <NemoSDKTracking/NemoSDKTracking.h>

@interface CustomAppDelegate : UnityAppController<LoginDelegate>
@end
IMPL_APP_CONTROLLER_SUBCLASS(CustomAppDelegate)
// ------------------------------------------------
@interface GameUtils : NSObject

+(NSString*) createNSString:(const char*)string;
+(char*)     createCString:(const char*)string;
+(void)      sendSdkMessage:(NSString *)UnityClass method: (NSString *) classMethod message: (NSString*) message;
@end


extern "C"
{
    void showLoginIOS();
    void buyPackage(const char* jsonPackage);
    void callAFTracking(const char* jsonContent);
    void addIAPNemoProductIdentifier(const char* productId);
    void initSDK(const char* appId,const char* server);

}
