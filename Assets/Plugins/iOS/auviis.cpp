#include "auviis.h"
void Auviis_setUnityCallbacks(){}

int64_t Auviis_getUniqueId(){return 0;}

int64_t Auviis_createRandomChannel(){return 0;}

void Auviis_setActiveVoiceChannel(int64_t channel_id){}

int64_t Auviis_getActiveVoiceChannel(){return 0;}

void Auviis_sendTextChat(int64_t channel_id, const char *msg){}

void Auviis_recordVoice(){}

void Auviis_playRecord(){}

void Auviis_stopRecord(){}

void Auviis_sendVoiceChat(int64_t channel_id){}

const char* Auviis_getRecordId(){return "";}

void Auviis_playVoiceMessage(const char* msg_id){}

void Auviis_init(const char * id, const char * signature){}
void Auviis_default_connect(){}

void Auviis_connect(const char* server_address,uint32_t port, bool ssl){}

void Auviis_disconnect(){}

void Auviis_stop(){}

bool Auviis_ready(){return false;}

void Auviis_joinChannel(int64_t channel_id){}

void Auviis_leaveChannel(int64_t channel_id){}

void Auviis_startVoiceStream(){}

void Auviis_stopVoiceStream(){}

bool Auviis_isVoiceStreamEnable(){ return false;}

void Auviis_muteSend(){}

void Auviis_unmuteSend(){}

void Auviis_outputToDefault(){}

void Auviis_outputToSpeaker(){}

void Auviis_outputToBluetooth(){}

bool Auviis_audioReady(){return false;}

void Auviis_enableAudio(){}

void Auviis_disableAudio(){}

namespace Auviis {
    
    int64_t getUniqueId(){return 0;}
    
    int64_t createRandomChannel(){return 0;}
    
    void setActiveVoiceChannel(int64_t channel_id){}
    
    int64_t getActiveVoiceChannel(){return 0;}
    
    void sendTextChat(int64_t channel_id, std::string msg){}
    
    void recordVoice(){}
    
    void playRecord(){}
    
    void stopRecord(){}
    
    void sendVoiceChat(int64_t channel_id){}
    
    std::string getRecordId(){ return "";}
    
    void playVoiceMessage(std::string msg_id){}
    /*
     Setup application id and signature, it is provided when user register an application
     */
    void init(const char * id, const char * signature){}
    /*
     Start the connection session to community server
     */
    void connect(){}
    /*
     Start the connection session to dedicated server. Only use for enterprise users.
     */
    void connect(std::string server_address,uint32_t port, bool ssl){}
    /*
     Close the session
     */
    void disconnect(){}
    /*
     Stop send/receive any data to/from server
     */
    void stop(){}
    /*
     Check the session is ready
     */
    bool ready(){ return false;}
    /*
     Join to a channel before can send any voice/text stream
     */
    void joinChannel(int64_t channel_id){}
    /*
     Leave a channel
     */
    void leaveChannel(int64_t channel_id){}
    /**
     Channel members
     */
    std::vector<int64_t> getChannelMembers(int64_t channel_id){}
    /*
     Send text chat to active channel
     */
    void sendTextChat(std::string text){}
    /*
     Start to send and receive voice stream. The audio stream is only enable after use this function.
     */
    void startVoiceStream(){}
    /*
     Stop send/receive any voice stream.
     */
    void stopVoiceStream(){}
    /*
     Check if voice stream is enable
     */
    bool isVoiceStreamEnable(){return false;}
    /*
     Stop send voice stream, but can receive from others
     */
    void muteSend(){}
    /*
     Start to send voice stream if it is currently stopped.
     */
    void unmuteSend(){}
    /*
     Set output to  headset
     */
    void outputToDefault(){}
    /*
     Set output to speaker
     */
    void outputToSpeaker(){}
    /*
     Set output to bluetooth device, maybe it is not work at this release.
     */
    void outputToBluetooth(){}
        //Audio
    /*
     Check if the inout and output audio devices are already initialized.
     */
    bool audioReady(){return false;}
    /*
     This feature does not work in this release.
     */
    void setLocalLoop(bool loop){}
    /*
     Enable the input and output audio devices
     */
    void enableAudio(){}
    /*
     Disable the input and output audio devices
     */
    void disableAudio(){}
    
    void setOnErrorCallback(std::function<void(int, std::string)>  onErrorCallback){}
        //
    void setOnInitSuccessCallback(std::function<void(int64_t)>  onErrorCallback){}
        //
    void setOnJoinChannelCallback(std::function<void(int, int64_t, int64_t, std::string)>  onJoinChannelCallback){}
        //
    void setOnReceiveChannelMembersCallback(std::function<void(int64_t, std::vector<int64_t>)> onReceiveChannelMembersCallback){}
        //
    void setOnReleaseChannelCallback(std::function<void(int64_t, int64_t)>  onReleaseChannelCallback){}
        //
    void setOnTextChatReceiveCallback(std::function<void(int64_t, int64_t, std::string)>  onTextChatReceiveCallback){}
        //
    void setOnVoiceChatReceiveCallback(std::function<void(int64_t, int64_t, std::string)>  onVoiceChatReceiveCallback){}
        //
    void setOnDisconnectCallback(std::function<void(int, std::string)>  onDisconnectCallback){}
        //
    void setOnActivatedCallback(std::function<void(int64_t)>  onActivatedCallback){}
        //
    void setOnVoiceMessageReady(std::function<void(int, long)> onRecordVoiceSuccessCallback){}
        //
    void setOnRecordVoiceError(std::function<void(int,std::string )> onRecordVoiceErrorCallback){}
}

