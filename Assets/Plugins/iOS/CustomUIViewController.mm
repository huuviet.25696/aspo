//
//  Z77UIViewController.m
//  Unity-iPhone
//
//  Created by nhnhat on 22/09/2021.
//

#import "CustomUIViewController.h"

@interface CustomUIViewController ()<UIActionSheetDelegate>

@end
@implementation CustomUIViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.frame = [UIScreen mainScreen].bounds;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

@end
