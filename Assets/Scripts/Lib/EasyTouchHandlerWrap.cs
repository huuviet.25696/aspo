using System;
using LuaInterface;
using UnityEngine;

public class CLASSTAG_EasyTouchHandlerWrap
{
    public static void Register(LuaState L)
    {
        L.BeginClass(typeof(EasyTouchHandler), typeof(System.Object));
		L.RegFunction("GetTouchCount",FUNCTAG_GetTouchCount);
        L.RegFunction("SetCallback",FUNCTAG_SetCallback);
        L.RegFunction("AddCamera",FUNCTAG_AddCamera);
        L.RegFunction("DelCamera",FUNCTAG_DelCamera);
        L.RegFunction("Select",FUNCTAG_Select);
        L.RegFunction("SelectMultiple",FUNCTAG_SelectMultiple);
        L.EndClass();
    }

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_GetTouchCount(IntPtr L)
	{
		try
		{
			ToLua.Push(L, EasyTouchHandler.FUNCTAG_GetTouchCount());
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_SetCallback(IntPtr L)
    {
        try
        {
            LuaFunction luaFunc = ToLua.CheckLuaFunction(L, 1);
            EasyTouchHandler.FUNCTAG_SetCallback(luaFunc);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_AddCamera(IntPtr L)
    {
        try
        {
            UnityEngine.Camera arg0 = (UnityEngine.Camera)ToLua.CheckUnityObject(L, 1, typeof(UnityEngine.Camera));
            bool arg1 = LuaDLL.luaL_checkboolean(L, 2);
            EasyTouchHandler.FUNCTAG_AddCamera(arg0, arg1);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_DelCamera(IntPtr L)
    {
        try
        {
            UnityEngine.Camera arg0 = (UnityEngine.Camera)ToLua.CheckUnityObject(L, 1, typeof(UnityEngine.Camera));
            EasyTouchHandler.FUNCTAG_DelCamera(arg0);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    private static int FUNCTAG_Select(IntPtr L)
    {
        try
        {
            UnityEngine.Camera camera = (UnityEngine.Camera)ToLua.CheckUnityObject(L, 1, typeof(UnityEngine.Camera));
            float x = (float)LuaDLL.luaL_checknumber(L, 2);
            float y = (float)LuaDLL.luaL_checknumber(L, 3);
            int layerMask = LuaDLL.luaL_checkinteger(L, 4);
            Ray ray = camera.ScreenPointToRay(new Vector3(x, y, 0f));
            RaycastHit raycastHit;
            if (Physics.Raycast(ray, out raycastHit, float.PositiveInfinity, layerMask))
            {
                ToLua.Push(L, raycastHit.transform.gameObject);
                ToLua.Push(L, raycastHit.point);
                return 2;
            }
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    private static int FUNCTAG_SelectMultiple(IntPtr L)
    {
        try
        {
            UnityEngine.Camera camera = (UnityEngine.Camera)ToLua.CheckUnityObject(L, 1, typeof(UnityEngine.Camera));
            float x = (float)LuaDLL.luaL_checknumber(L, 2);
            float y = (float)LuaDLL.luaL_checknumber(L, 3);
            int layerMask = LuaDLL.luaL_checkinteger(L, 4);
            Ray ray = camera.ScreenPointToRay(new Vector3(x, y, 0f));
            RaycastHit[] array = Physics.RaycastAll(ray, float.PositiveInfinity, layerMask);
            if (array != null && array.Length > 0)
            {
                LuaDLL.lua_newtable(L);
                int top = LuaDLL.lua_gettop(L);
                for (int i = 0; i < array.Length; i++)
                {
                    ToLua.Push(L, i * 2 + 1);
                    ToLua.Push(L, array[i].transform.gameObject);
                    LuaDLL.lua_settable(L, -3);
                    ToLua.Push(L, i * 2 + 2);
                    ToLua.Push(L, array[i].point);
                    LuaDLL.lua_settable(L, -3);
                }
                LuaDLL.lua_settop(L, top);
                return 1;
            }
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }
}

