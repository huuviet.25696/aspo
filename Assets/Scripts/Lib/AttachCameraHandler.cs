using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttachCameraHandler : MonoBehaviour {
	public Camera mycamera;

	public Camera bgCamera;

	public List<Camera> attachCameras = new List<Camera>();
	// Use this for initialization
	void Start () {
	}

	public Camera FUNCTAG_NewAttachCamera(int layerMask, int depth, int skybox)
	{
		if (mycamera == null)
		{
			mycamera = this.transform.GetComponent<Camera>();
			bgCamera = mycamera;
		}
		GameObject go = new GameObject();
		
		go.transform.parent = this.transform;
		Camera attachCamra = go.AddComponent<Camera>();
		attachCamra.CopyFrom(mycamera);
		attachCamra.depth = depth;

		if (depth < bgCamera.depth)
		{
			bgCamera.clearFlags = CameraClearFlags.Depth;
			bgCamera = attachCamra;
		}
		else
		{
			attachCamra.clearFlags = CameraClearFlags.Depth;
		}
		if (skybox>0){
			bgCamera.clearFlags = CameraClearFlags.Skybox;
		}
		attachCamra.cullingMask = layerMask;
		attachCameras.Add(attachCamra);
		go.name = "attachCamera" + attachCameras.Count;
		return attachCamra;
	}

	public void FUNCTAG_SetRect(Rect rect)
	{
		foreach (var cam in attachCameras)
		{
			cam.rect = rect;
		}
	}
	public void FUNCTAG_SetEnabled(bool enabled)
	{
		foreach (var cam in attachCameras)
		{
			cam.enabled = enabled;
		}
	}

	public void FUNCTAG_SetFieldOfView(int fieldOfView)
	{
		foreach (var cam in attachCameras)
		{
			cam.fieldOfView = fieldOfView;
		}
	}

	public void FUNCTAG_SetFarClipPlane(int farClipPlane)
	{
		foreach (var cam in attachCameras)
		{
			cam.farClipPlane = farClipPlane;
		}
	}

	public void FUNCTAG_SetBackgroudColor(Color backgroundColor)
	{
		bgCamera.backgroundColor = backgroundColor;
	}

	public Color FUNCTAG_GetBackgroundColor()
	{
		return bgCamera.backgroundColor;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
