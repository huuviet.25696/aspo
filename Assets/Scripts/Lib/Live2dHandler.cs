using UnityEngine;
using System;
using System.Collections;
using live2d;
using AssetPipeline;
using live2d.framework;

public class Live2dHandler : MonoBehaviour
{
    public Live2DModelUnity live2DModel;
	private Matrix4x4 live2DCanvasPos;
	private Matrix4x4 matrix;
	public Camera mycamera;
	public float[] arr;

	public void FUNCTAG_SetLipsClip(AudioClip audioClip){
		if (audioClip == null ){
			return;
		}
		audioClip.LoadAudioData ();
		arr = new float[audioClip.samples];
		audioClip.GetData (arr, 0);
	}

	public float FUNCTAG_GetSampleValue(int idx){
		if (arr != null && idx > 0 && idx < arr.Length) {
			return arr [idx];
		}
		return 0;
	}

	public Live2DModelUnity FUNCTAG_LoadModel(string path)
	{
		UnityEngine.Object obj = AssetManager.Instance.FUNCTAG_LoadAsset(path);
		TextAsset testAsset = obj as TextAsset;
		var model = Live2DModelUnity.loadModel(testAsset.bytes);
		return model;
	}

	public L2DPhysics FUNCTAG_LoadPhysics(string path){
		UnityEngine.Object obj = AssetManager.Instance.FUNCTAG_LoadAsset(path);
		TextAsset testAsset = obj as TextAsset;
		var physics = L2DPhysics.load(testAsset.bytes);
		return physics;
	}

	public Live2DMotion FUNCTAG_LoadMotion(string path)
	{
		UnityEngine.Object obj = AssetManager.Instance.FUNCTAG_LoadAsset(path);
		TextAsset testAsset = obj as TextAsset;
		var motion = Live2DMotion.loadMotion(testAsset.bytes);
		return motion;
	}

	public void FUNCTAG_SetLive2dModel(Live2DModelUnity m)
	{
		if (live2DModel != null)
		{
			live2DModel.deleteTextures();
			live2DModel.releaseModel();
		}
		live2DModel = m;
		if (live2DModel != null)
		{
			float modelWidth = live2DModel.getCanvasWidth();
			live2DCanvasPos = Matrix4x4.Ortho(0, modelWidth, modelWidth*1.2f, 0, -50f, 50f);
			matrix = transform.localToWorldMatrix * live2DCanvasPos;
			live2DModel.setMatrix(matrix);
		}

	}

	void Start()
	{
		Live2D.init();
	}

	void OnDestory()
	{
		if (live2DModel != null)
		{
			live2DModel.deleteTextures();
			live2DModel.releaseModel();
		}
		Live2D.dispose();
	}

    void OnRenderObject()
    {
		if (live2DModel!=null)
		{
			live2DModel.update();
			live2DModel.draw();	
		}
    }

	public Vector2 FUNCTAG_TouchToModelPoint(float x, float y)
	{
		if (mycamera != null && live2DCanvasPos != null)
		{
			var screenPos = new Vector3(x, y, 0);
			var touchWorldPosition = mycamera.ScreenToWorldPoint(screenPos);
			var localPoint = transform.InverseTransformPoint(touchWorldPosition);
			// Inverse transform the position.
			var modelPointX = (localPoint.x - live2DCanvasPos.m03) / live2DCanvasPos.m00;
			var modelPointY = (localPoint.y - live2DCanvasPos.m13) / live2DCanvasPos.m11;

			return new Vector2(modelPointX, modelPointY);
		}
		else
		{
			return Vector2.zero;
		}

	}

	public Vector4 FUNCTAG_GetAABBSize(string drawDataIndex)
	{
		if (live2DModel != null)
		{
			var idx = live2DModel.getDrawDataIndex(drawDataIndex);
			var points = live2DModel.getTransformedPoints(idx);
			if (points == null) {
				Debug.Log(string.Format("<color=#ffeb04>{0}不存在</color>", drawDataIndex));
				return Vector4.zero;
			}
			float left = live2DModel.getCanvasWidth();
			float right = 0.0f;
			float top = live2DModel.getCanvasHeight();
			float bottom = 0.0f;


			for (var i = 0; i < points.Length; i = i + 2)
			{
				float x = points[i];
				float y = points[i + 1];


				if (x < left)
				{
					left = x;
				}
				if (x > right)
				{
					right = x;
				}

				if (y < top)
				{
					top = y;
				}
				if (y > bottom)
				{
					bottom = y;
				}
			}
			return new Vector4(left, right, top, bottom);
		}
		else
		{
			return Vector4.zero;
		}

	}

}