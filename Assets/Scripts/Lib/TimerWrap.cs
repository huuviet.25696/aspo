//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class CLASSTAG_TimerWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(Timer), typeof(System.Object));
        //L.RegFunction("AddTimer",FUNCTAG_AddTimer);
        //L.RegFunction("DelTimer",FUNCTAG_DelTimer);
		L.RegFunction("GetTickS",FUNCTAG_GetTickS);
		L.RegFunction("GetTickMS",FUNCTAG_GetTickMS);
		L.RegFunction("GetTicks",FUNCTAG_GetTicks);
		L.RegFunction("__tostring",ToLua.op_ToString);
		L.EndClass();
	}

    //[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    //static int FUNCTAG_AddTimer(IntPtr L)
    //{
    //    try
    //    {
    //        ToLua.CheckArgsCount(L, 3);
    //        LuaFunction arg1 = ToLua.CheckLuaFunction(L, 1);
    //        double interval = LuaDLL.luaL_checknumber(L, 2);
    //        double dealy = LuaDLL.luaL_checknumber(L, 3);
    //        int o = Timer.Instance.AddTimer(arg1, interval, dealy);
    //        LuaDLL.lua_pushinteger(L, o); 
    //        return 1;
    //    }
    //    catch(Exception e)
    //    {
    //        return LuaDLL.toluaL_exception(L, e);
    //    }
    //}

    //[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    //static int FUNCTAG_DelTimer(IntPtr L)
    //{
    //    try
    //    {
    //        ToLua.CheckArgsCount(L, 1);
    //        int arg0 = (int)LuaDLL.luaL_checknumber(L, 1);
    //        Timer.Instance.DelTimer(arg0);
    //        return 0;
    //    }
    //    catch(Exception e)
    //    {
    //        return LuaDLL.toluaL_exception(L, e);
    //    }
    //}

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_GetTickS(IntPtr L)
    {
        try
        {
            LuaDLL.lua_pushnumber(L, DateTime.Now.Ticks / 10000000);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_GetTickMS(IntPtr L)
    {
        try
        {
            LuaDLL.lua_pushnumber(L, DateTime.Now.Ticks / 10000);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }
	

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_GetTicks(IntPtr L)
	{
		try
		{
			LuaDLL.lua_pushnumber(L, DateTime.Now.Ticks);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

}

