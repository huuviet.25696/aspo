using UnityEngine;
using System.Collections;

public class GraySceneHandler : MonoBehaviour
{
    private Material _material;
    private Shader _shader;
    private float grayScaleAmount = 1.0f;

    public Material material
    {
        get
        {
            if (_material == null)
            {
                _material = new Material(_shader);
                _material.hideFlags = HideFlags.HideAndDontSave;
            }
            return _material;
        }
    }

    public Shader shader
    {
        get
        {
            if (_shader == null)
            {
				_shader = CLASSTAG_ShaderHelper.Find("Game/GrayScene");
            }
            return _shader;
        }
    }

    private void Start()
    {
        if (SystemInfo.supportsImageEffects == false)
        {
            enabled = false;
            return;
        }
        if (shader != null && shader.isSupported == false)
        {
            enabled = false;
            return;
        }
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        if (shader != null)
        {
            material.SetFloat("_LuminosityAmount", grayScaleAmount);
            Graphics.Blit(source, destination, material);
        }
        else
        {
            Graphics.Blit(source, destination);
        }
    }

    private void Update()
    {
        grayScaleAmount = Mathf.Clamp(grayScaleAmount, 0.0f, 1.0f);
    }

    private void OnDisable()
    {
        if (_material != null)
        {
            DestroyImmediate(_material);
        }
    }

    public void FUNCTAG_SetGrayScene(bool b)
    {
        if (b)
        {
            grayScaleAmount = 1.0f;
        }
        else
        {
            grayScaleAmount = 0f;
        }

    }
}
