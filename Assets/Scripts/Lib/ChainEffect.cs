using UnityEngine;
using System.Collections;

public class ChainEffect : MonoBehaviour {
	static int hideLayer = -1;
	public GameObject beginObj;
	public GameObject endObj;
	// Use this for initialization
	void Start () {
		if (hideLayer == -1)
		{
			hideLayer = LayerMask.NameToLayer("Hide");
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (beginObj!= null && endObj !=null)
		{
			if (beginObj.layer != hideLayer && beginObj.activeInHierarchy  && endObj.layer != hideLayer && endObj.activeInHierarchy)
			{
				var beginPos = beginObj.transform.position;
				var endPos = endObj.transform.position;
				transform.position = beginPos;
				float dis = Vector3.Distance(beginPos, endPos);
				transform.LookAt(endPos, endObj.transform.up);
				transform.localScale = new Vector3(1,1, dis);
				return;
			}

		}
		transform.localScale = Vector3.zero;
	}
}
