using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using UnityEngine;
using AssetPipeline;

public class Utils
{
	public static Material FUNCTAG_RenderAddMat(Renderer rend, Material mat)
	{
		var arr = rend.materials;
		var newArr = new Material[arr.Length + 1];
		System.Array.Copy(arr, 0, newArr, 0, arr.Length);
		newArr[arr.Length] = mat;
		rend.materials = newArr;
		return rend.materials[arr.Length];
	}
	public static Material[] FUNCTAG_RenderResizeMatCnt(Renderer rend, int cnt)
	{
		var arr = rend.materials;
		int delCnt = arr.Length - cnt;
		if (delCnt > 0)
		{
			Material[] delArr = new Material[delCnt];
			System.Array.Copy(arr, cnt, delArr, 0, delCnt);
			var newArr = new Material[cnt];
			System.Array.Copy(arr, 0, newArr, 0, cnt);
			rend.materials = newArr;
			return delArr;
		}
		return null;
		
	}

	public static List<Material> FUNCTAG_GetMaterials(object[] gameObjects)
	{
		var list = new List<Material>();

		foreach (GameObject go in gameObjects)
		{
			var rends = go.GetComponentsInChildren(typeof(Renderer), true);
			foreach (Renderer rend in rends)
			{
				foreach(Material mat in rend.materials)
				{
					if (!list.Contains(mat))
					{
						list.Add(mat);
					}
				}
				
			}
		}
		return list;
	}

    public static void FUNCTAG_RestoreGameRes(bool restart)
    {
        AssetUpdate.Instance.MarkCleanUpResFlag();
        AssetUpdate.Instance.CleanUpBundleResFolder();
        AssetUpdate.Instance.CleanUpDllFolder();
        UnityEngine.Application.Quit();
    }
}

