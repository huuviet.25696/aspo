//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class CLASSTAG_GameEventHandlerWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(GameEventHandler), typeof(UnityEngine.MonoBehaviour));
		L.RegFunction("SetClickCallback",FUNCTAG_SetClickCallback);
		L.RegFunction("SetApplicationPauseCallback",FUNCTAG_SetApplicationPauseCallback);
        L.RegFunction("SetApplicationFocusCallback",FUNCTAG_SetApplicationFocusCallback);
        L.RegVar("Instance",FUNCTAG_get_Instance, null);
		L.EndClass();
	}

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_get_Instance(IntPtr L)
    {
        try
        {
            ToLua.Push(L, GameEventHandler.Instance);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_SetClickCallback(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			GameEventHandler obj = (GameEventHandler)ToLua.CheckObject(L, 1, typeof(GameEventHandler));
			LuaFunction arg0 = ToLua.CheckLuaFunction(L, 2);
			obj.FUNCTAG_SetClickCallback(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}


	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_SetApplicationPauseCallback(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			GameEventHandler obj = (GameEventHandler)ToLua.CheckObject(L, 1, typeof(GameEventHandler));
			LuaFunction arg0 = ToLua.CheckLuaFunction(L, 2);
			obj.FUNCTAG_SetApplicationPauseCallback(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_SetApplicationFocusCallback(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 2);
            GameEventHandler obj = (GameEventHandler)ToLua.CheckObject(L, 1, typeof(GameEventHandler));
            LuaFunction arg0 = ToLua.CheckLuaFunction(L, 2);
            obj.FUNCTAG_SetApplicationFocusCallback(arg0);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }
}

