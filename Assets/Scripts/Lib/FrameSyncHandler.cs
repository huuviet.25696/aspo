using UnityEngine;
using System.Collections;

public class FrameSyncHandler : MonoBehaviour {
	public GameObject targetGo;
	public Camera targetCamera;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (targetGo == null) return;
		transform.position = targetGo.transform.position;
		transform.localScale = targetGo.transform.localScale;
		transform.rotation = targetGo.transform.rotation;
	}
	public void FUNCTAG_SetTarget(GameObject go)
	{
		targetGo = go;
		if (targetGo != null)
		{
			targetCamera = go.GetComponent<Camera>() as Camera;
		}
		else 
		{
			targetCamera = null;
		}
	}
}
