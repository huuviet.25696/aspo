using System;
using System.Collections;
using UnityEngine;

public class WalkerEventHandler : MonoBehaviour
{
    public bool isVisible = false;

	public float minX = 0f;
	public float maxX = 1f;
	public float minY = 0f;
	public float maxY = 1f;
	
	public Camera gameCamera;
	public int eventid = -1;
	
	void Update() 
	{
		if (eventid > 0)
		{
			bool newVisble =   FUNCTAG_CheckVisible();
			if (isVisible != newVisble)
			{
				isVisible = newVisble;
				GlobalEventHanlder.Call(eventid, isVisible);
			}
		}
	}

	public void FUNCTAG_StartCheckVisible(int id, float area, Camera camera)
	{
		minX = 1 - area;
		maxX = area;
		minY = 1 - area;
		maxY = area;
		eventid = id;
		gameCamera = camera;
		isVisible =  FUNCTAG_CheckVisible();
		GlobalEventHanlder.Call(eventid, isVisible);
	}

	public void FUNCTAG_StopCheckVisible()
	{
		minX = 0f;
		maxX = 1f;
		minY = 0f;
		maxY = 1f;
		gameCamera = null;
		eventid = -1;
	}

	private bool  FUNCTAG_CheckVisible()
	{
		bool visible = gameCamera.enabled;
		if (visible) 
		{
			var viewPos = gameCamera.WorldToViewportPoint(this.transform.position);

			if(viewPos.x > maxX || viewPos.x < minX || viewPos.y > maxY || viewPos.y < minY)
			{
				visible = false;
			}
		}
		return visible;
	}

}