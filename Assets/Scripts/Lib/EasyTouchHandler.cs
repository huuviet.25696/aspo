using System;
using System.Collections.Generic;
using HedgehogTeam.EasyTouch;
using UnityEngine;
using LuaInterface;


public class EasyTouchHandler
{
    public enum EasyTouchType
    {
        None,
        TouchDown,
        TouchUp,
		Swipe,
		LongTapStart,
		SwipeStart2Finger,
		Swipe2Finger,
		SwipeEnd2Finger,
		Pinch,
		TouchStart,
		Cancel,
		UIElementTouchUp,
		SwipeStart,
		SwipeEnd,
    }

    private static LuaFunction luaCallback;
    private static bool isTouch;
    private static Vector2 touchPos1;
    private static Vector2 touchPos2;


    public static void FUNCTAG_Init()
    {
        EasyTouch.On_TouchDown += FUNCTAG_OnTouchDown;
        EasyTouch.On_TouchUp += FUNCTAG_OnTouchUp;
		EasyTouch.On_Swipe += (gesture) => { FUNCTAG_OnCall(EasyTouchType.Swipe, gesture.swipeVector); };
		EasyTouch.On_LongTapStart += FUNCTAG_OnLongTabStart;
		EasyTouch.On_SwipeStart2Fingers += FUNCTAG_OnSwipeStart2Fingers;
		EasyTouch.On_Swipe2Fingers += FUNCTAG_OnSwipe2Fingers;
		EasyTouch.On_SwipeEnd2Fingers += FUNCTAG_OnSwipeEnd2Fingers;
		EasyTouch.On_Pinch += FUNCTAG_OnPinch;
		EasyTouch.On_TouchStart += (gesture) => { FUNCTAG_OnCall(EasyTouchType.TouchStart); };
		EasyTouch.On_Cancel += (gesture) => { FUNCTAG_OnCall(EasyTouchType.Cancel); };
		//EasyTouch.On_UIElementTouchUp += (gesture) => { OnCall(EasyTouchType.UIElementTouchUp); };

		EasyTouch.On_SwipeStart += (gesture) => { FUNCTAG_OnCall(EasyTouchType.SwipeStart); };
		EasyTouch.On_SwipeEnd += (gesture) => { FUNCTAG_OnCall(EasyTouchType.SwipeEnd); };
    }

	public static int FUNCTAG_GetTouchCount()
	{
		return EasyTouch.instance.input.TouchCount();
	}

    public static void FUNCTAG_Release()
    {
        EasyTouch.On_TouchDown -= FUNCTAG_OnTouchDown;
        EasyTouch.On_TouchUp -= FUNCTAG_OnTouchUp;
    }

    public static void FUNCTAG_SetCallback(LuaFunction func)
    {
        if (luaCallback != null)
        {
            luaCallback.Dispose();
            luaCallback = null;
        }
        luaCallback = func;
    }
    
    private static void FUNCTAG_OnTouchDown(Gesture gesture)
    {
        if (!isTouch)
        {
            isTouch = true;
            FUNCTAG_UpdateTouchPosition(gesture);
            FUNCTAG_OnCall(EasyTouchType.TouchDown, touchPos1);
        }
    }

    private static void FUNCTAG_OnTouchUp(Gesture gesture)
    {
        FUNCTAG_UpdateTouchPosition(gesture);
        FUNCTAG_OnCall(EasyTouchType.TouchUp, touchPos1);
        FUNCTAG_ClearTouchPos();
        isTouch = false;
    }

	private static void FUNCTAG_OnLongTabStart(Gesture gesture)
	{
		FUNCTAG_OnCall(EasyTouchType.LongTapStart, touchPos1);
	}

	private static void FUNCTAG_OnSwipe2Fingers(Gesture gesture)
	{
		FUNCTAG_OnCall(EasyTouchType.Swipe2Finger, gesture.swipeVector);
	}
	private static void FUNCTAG_OnSwipeStart2Fingers(Gesture gesture)
	{
		FUNCTAG_OnCall(EasyTouchType.SwipeStart2Finger, gesture.swipeVector);
	}

	private static void FUNCTAG_OnSwipeEnd2Fingers(Gesture gesture)
	{
		FUNCTAG_OnCall(EasyTouchType.SwipeEnd2Finger, gesture.swipeVector);
	}
	private static void FUNCTAG_OnPinch(Gesture gesture)
	{
		FUNCTAG_OnCall(EasyTouchType.Pinch, gesture.touchCount, gesture.deltaPinch);
	}


    public static void FUNCTAG_AddCamera(Camera camera, bool guiCam)
    {
        EasyTouch.AddCamera(camera, guiCam);
    }

    public static void FUNCTAG_DelCamera(Camera camera)
    {
        EasyTouch.RemoveCamera(camera);
    }


    private static void FUNCTAG_UpdateTouchPosition(Gesture gesture)
    {
        Vector2 pos1 = touchPos1;
        Vector2 pos2 = touchPos2;
        if (gesture.touchCount == 1)
        {
            touchPos1 = gesture.position;
            touchPos2 = Vector2.zero;
        }
        else if (gesture.touchCount == 2)
        {
            if (EasyTouch.instance.isRealTouch)
            {
                touchPos1 = EasyTouch.GetFingerPosition(Input.GetTouch(0).fingerId);
                touchPos2 = EasyTouch.GetFingerPosition(Input.GetTouch(1).fingerId);
            }
            else
            {
                touchPos1 = gesture.position;
                touchPos2 = Vector2.zero;
            }
        }
        else
        {
            touchPos1 = Vector2.zero;
            touchPos2 = Vector2.zero;
        }
    }

    private static void FUNCTAG_ClearTouchPos()
    {
        touchPos1 = Vector2.zero;
        touchPos2 = Vector2.zero;
    }

    private static void FUNCTAG_OnCall(EasyTouchHandler.EasyTouchType type, Vector2 pos)
    {
        try
        {
            if (luaCallback != null)
            {
                luaCallback.BeginPCall();
                luaCallback.Push(type);
                luaCallback.Push(pos);
                luaCallback.PCall();
                luaCallback.EndPCall();
            }
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }
    }

	private static void FUNCTAG_OnCall(EasyTouchHandler.EasyTouchType type, float f1, float f2)
	{
		try
		{
			if (luaCallback != null)
			{
				luaCallback.BeginPCall();
				luaCallback.Push(type);
				luaCallback.Push(f1);
				luaCallback.Push(f2);
				luaCallback.PCall();
				luaCallback.EndPCall();
			}
		}
		catch (Exception e)
		{
			Debug.LogError(e);
		}
	}
	private static void FUNCTAG_OnCall(EasyTouchHandler.EasyTouchType type)
	{
		try
		{
			if (luaCallback != null)
			{
				luaCallback.BeginPCall();
				luaCallback.Push(type);
				luaCallback.PCall();
				luaCallback.EndPCall();
			}
		}
		catch (Exception e)
		{
			Debug.LogError(e);
		}
	}
}
