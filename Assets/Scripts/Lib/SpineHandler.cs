using UnityEngine;
using System.Collections;
using Spine.Unity;
using System;
using Spine;
using System.Collections.Generic;
using LuaInterface;

public class SpineHandler : MonoBehaviour {
    public enum EventType
    {
        Sync = 0,
        Start = 1,
        Complete = 2,
    }
    private SkeletonAnimation mSkeletonAnimation;
    private static LuaFunction luaCallback;

    /// <summary>
    /// 每个通道的总时间，自己累加,因为spine下一个动画播发的start会先与上一个动画的complete回调
    /// </summary>
    private Dictionary<float, float> mTotalDurationDic = new Dictionary<float, float>();


    public delegate void StartDelegate(TrackEntry trackEntry);
    public delegate void CompleteDelegate(TrackEntry trackEntry);

    public int eventID = 0;

    private void Awake() {
        mSkeletonAnimation = gameObject.GetComponent<SkeletonAnimation>();
    }

    private void OnDestroy()
    {

    }

    public void FUNCTAG_SetEventID(int id)
    {
        eventID = id;
    }

    public void FUNCTAG_Call(EventType type, string key)
    {
        GlobalEventHanlder.Call(eventID, (int)type, key);
    }

    private void FUNCTAG_OnAnimationSync(TrackEntry trackEntry, string key)
    {
        try
        {
            FUNCTAG_Call(EventType.Sync, key);
        }
        catch (Exception e)
        {
            Debug.LogError(e);
        }

    }
    private void FUNCTAG_OnAnimationStart(TrackEntry trackEntry, string key)
    {
        trackEntry.Start += delegate
        {
            try
            {
                FUNCTAG_Call(EventType.Start, key);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        };
    }


    private void FUNCTAG_OnAnimationComplete(TrackEntry trackEntry, string key)
    {
        trackEntry.Complete += delegate
        {
            try
            {
                FUNCTAG_Call(EventType.Complete, key);
            }
            catch (Exception e)
            {
                Debug.LogError(e);
            }
        };
    }

    public void FUNCTAG_SetAnimation(int trackIndex, String animationName, bool loop, string startkey, string completekey)
    {
        //ClearLastAll();
        if (this.mSkeletonAnimation.state.TimeScale == 0) {
			this.mSkeletonAnimation.state.TimeScale = 1;
		}
        TrackEntry trackEntry = this.mSkeletonAnimation.state.SetAnimation(trackIndex, animationName, loop);
        this.mTotalDurationDic[trackIndex] = trackEntry.Animation.Duration;
        if (startkey != "")
        {
            FUNCTAG_OnAnimationSync(trackEntry, startkey);
        }
        if (completekey != "")
        {
            FUNCTAG_OnAnimationComplete(trackEntry, completekey);
        }
    }

    public void FUNCTAG_AddAnimation(int trackIndex, String animationName, bool loop, float delay, string startkey, string completekey)
    {
        if (this.mSkeletonAnimation.state.TimeScale == 0)
        {
            this.mSkeletonAnimation.state.TimeScale = 1;
        }
        /*先用回spine自身的队列播放
        if (delay == 0)
        {
            delay = this.mTotalDurationDic[trackIndex];
            this.mTotalDurationDic[trackIndex] += delay;
        }*/
        
        TrackEntry trackEntry = this.mSkeletonAnimation.state.AddAnimation(trackIndex, animationName, loop, delay);
        if (startkey != "")
        {
            FUNCTAG_OnAnimationStart(trackEntry, startkey);
        }
        if (completekey != "")
        {
            FUNCTAG_OnAnimationComplete(trackEntry, completekey);
        }
    }

    public string[] FUNCTAG_GetAnimationNames()
    {
        string[] allAnimation = new string[mSkeletonAnimation.skeleton.Data.Animations.Count + 1];
        int idx = 1;
        foreach (var item in mSkeletonAnimation.skeleton.Data.Animations)
        {
            allAnimation[idx] = item.Name;
            idx++;
        }
        return allAnimation;
    }

	public void FUNCTAG_StopAnimation()
	{
		this.mSkeletonAnimation.state.TimeScale = 0;
	}

	public void FUNCTAG_SetTimeScale(float timeScale)
	{
		this.mSkeletonAnimation.state.TimeScale = timeScale;
	}

    public void FUNCTAG_SetFlipX(bool bFlipX)
    {
        this.mSkeletonAnimation.skeleton.FlipX = bFlipX;
    }

    public void FUNCTAG_SetFlipY(bool bFlipY)
    {
        this.mSkeletonAnimation.skeleton.FlipY = bFlipY;
    }

    /// <summary>
    /// 处理过度平滑导致残影的问题
    /// </summary>
    public void FUNCTAG_ClearLastAll()
    {
        this.mSkeletonAnimation.skeleton.SetToSetupPose();
        this.mSkeletonAnimation.state.ClearTracks();
    }
}
