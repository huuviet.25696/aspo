using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LuaInterface;
using UnityEngine;
//using cn.sharesdk.unity3d;

public class GameEventHandler : MonoBehaviour, AuviisSDKDelegate
{
    public LuaFunction luaOnClick;
    public LuaFunction luaApplicationPause;
    public LuaFunction luaApplicationFocus;
    //AuviisSDK auviisSDK;
    private Int64 active_channel_id;

    public static GameEventHandler Instance
    {
        private set;
        get;
    }

    public static void FUNCTAG_CreateInstance()
    {
        if (Instance != null)
        {
            Debug.LogError("GameEventHandler.Instance already exist");
            Instance.StartSDK();
            return;
        }

        GameObject go = new GameObject("GameEventHandler");
        GameObject.DontDestroyOnLoad(go);
        Instance = go.AddComponent<GameEventHandler>();

        //if (GameObject.Find("GameRoot/AuviisSDK") != null)
        //{
        //    Instance.auviisSDK = GameObject.Find("GameRoot/AuviisSDK").GetComponent<AuviisSDK>();
        //Instance.StartSDK();
        //}
        Instance.StartSDK();
    }
    public void StartSDK()
    {
        Debug.Log("StartSDK");
        if (GameLauncher.Instance.enableVoiceChat)
        {
            AuviisSDK.Instance.setSDKDelegate(Instance);
            if (AuviisSDK.sdkDelegate == null)
            {
                Debug.Log("AuviisSDK StartSDK sdkDelegate NULL");
            }
        }
#if !UNITY_EDITOR && UNITY_IPHONE
        if (GameLauncher.Instance.enableVoiceChat) AuviisSDK.Auviis_setUnityCallbacks();
#endif
#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
        if (GameLauncher.Instance.enableVoiceChat) AuviisSDK.Auviis_init("6785JH889bhFGKU8904", "PnHDEHHEIhjjAvcgQWUbcv");
        if (GameLauncher.Instance.enableVoiceChat) AuviisSDK.Auviis_default_connect();
#endif
    }
    public void StopSDK()
    {
        Debug.Log("StopSDK");
#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
        if (GameLauncher.Instance.enableVoiceChat) AuviisSDK.Auviis_stop();
#endif
    }
    private void OnEnable()
    {
        UICamera.onClick += FUNCTAG_OnClickCallback;
    }

    private void OnDisable()
    {
        UICamera.onClick -= FUNCTAG_OnClickCallback;
    }

    private void OnDestroy()
    {
        if (luaOnClick != null)
        {
            luaOnClick.Dispose();
            luaOnClick = null;
        }
    }

    private void OnApplicationFocus(bool focus)
    {
        if (luaApplicationFocus != null)
        {
            luaApplicationFocus.BeginPCall();
            luaApplicationFocus.Push(focus);
            luaApplicationFocus.PCall();
            luaApplicationFocus.EndPCall();
        }
    }

    private void OnApplicationPause(bool paused)
    {
        if (luaApplicationPause != null)
        {
            luaApplicationPause.BeginPCall();
            luaApplicationPause.Push(paused);
            luaApplicationPause.PCall();
            luaApplicationPause.EndPCall();
        }
    }

    public void FUNCTAG_SetApplicationPauseCallback(LuaFunction func)
    {
        if (luaApplicationPause != null)
        {
            luaApplicationPause.Dispose();
        }
        luaApplicationPause = func;
    }

    public void FUNCTAG_SetApplicationFocusCallback(LuaFunction func)
    {
        if (luaApplicationFocus != null)
        {
            luaApplicationFocus.Dispose();
        }
        luaApplicationFocus = func;
    }

    private void FUNCTAG_OnClickCallback(GameObject go)
    {
        if (luaOnClick != null)
        {
            luaOnClick.BeginPCall();
            luaOnClick.Push(go);
            luaOnClick.PCall();
            luaOnClick.EndPCall();
        }
    }

    public void FUNCTAG_SetClickCallback(LuaFunction func)
    {
        if (luaOnClick != null)
        {
            luaOnClick.Dispose();
        }
        luaOnClick = func;
    }

    //AuviisSDK Delegate
    public void onAuviisSDKError(string reason)
    {
        Debug.Log("onAuviisSDKError\n");
    }

    public void onAuviisSDKDisconnect(string reason)
    {
        Debug.Log("onAuviisSDKDisconnect\n");
    }

    public void onAuviisSDKInitSuccess(long peer_id)
    {
        //        Debug.Log("onAuviisSDKInitSuccess");
        //#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
        //        AuviisSDK.Auviis_startVoiceStream();
        //#endif
    }

    public void onAuviisSDKActivated(long peer_id)
    {
        //Debug.Log("onAuviisSDKActivated\n");
        //AuviisSDK.Auviis_joinChannel(123);
    }

    public void onAuviisSDKJoinChannel(long channel_id)
    {

    }

    public void onReceiveChannelMembers(long channel_id, int members)
    {
        //        Debug.Log("onReceiveChannelMembers with channel id:" + channel_id.ToString() + "member:" + members.ToString());
        //        active_channel_id = channel_id;
        //#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
        //        AuviisSDK.Auviis_setActiveVoiceChannel(channel_id);
        //#endif
    }

    public void onAuviisSDKVoiceMessageReady()
    {
        //        Debug.Log("onAuviisSDKVoiceMessageReady\n");
        //#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
        //        AuviisSDK.Auviis_sendVoiceChat(AuviisSDK.Auviis_getActiveVoiceChannel());
        //        Debug.Log("Auviis_sendVoiceChat to channel " + AuviisSDK.Auviis_getActiveVoiceChannel().ToString());
        //#endif
    }

    public void onAuviisSDKVoiceMessageReceived(string msgId)
    {
        //        Debug.Log("onAuviisSDKVoiceMessageReceived: " + msgId);
        //#if !UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID)
        //        AuviisSDK.Auviis_playVoiceMessage(msgId);
        //        Debug.Log("onAuviisSDKVoiceMessageReceived  with id " + msgId);
        //#endif
    }
}

