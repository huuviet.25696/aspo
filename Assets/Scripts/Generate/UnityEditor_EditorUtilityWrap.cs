﻿//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
public class UnityEditor_EditorUtilityWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(UnityEditor.EditorUtility), typeof(System.Object));
		L.RegFunction("OpenFilePanel", OpenFilePanel);
		L.RegFunction("OpenFilePanelWithFilters", OpenFilePanelWithFilters);
		L.RegFunction("RevealInFinder", RevealInFinder);
		L.RegFunction("DisplayDialog", DisplayDialog);
		L.RegFunction("DisplayDialogComplex", DisplayDialogComplex);
		L.RegFunction("OpenFolderPanel", OpenFolderPanel);
		L.RegFunction("SaveFolderPanel", SaveFolderPanel);
		L.RegFunction("WarnPrefab", WarnPrefab);
		L.RegFunction("IsPersistent", IsPersistent);
		L.RegFunction("SaveFilePanel", SaveFilePanel);
		L.RegFunction("NaturalCompare", NaturalCompare);
		L.RegFunction("InstanceIDToObject", InstanceIDToObject);
		L.RegFunction("CompressTexture", CompressTexture);
		L.RegFunction("CompressCubemapTexture", CompressCubemapTexture);
		L.RegFunction("SetDirty", SetDirty);
		L.RegFunction("ClearDirty", ClearDirty);
		L.RegFunction("InvokeDiffTool", InvokeDiffTool);
		L.RegFunction("CopySerialized", CopySerialized);
		L.RegFunction("CopySerializedManagedFieldsOnly", CopySerializedManagedFieldsOnly);
		L.RegFunction("CollectDependencies", CollectDependencies);
		L.RegFunction("CollectDeepHierarchy", CollectDeepHierarchy);
		L.RegFunction("FormatBytes", FormatBytes);
		L.RegFunction("DisplayProgressBar", DisplayProgressBar);
		L.RegFunction("DisplayCancelableProgressBar", DisplayCancelableProgressBar);
		L.RegFunction("ClearProgressBar", ClearProgressBar);
		L.RegFunction("GetObjectEnabled", GetObjectEnabled);
		L.RegFunction("SetObjectEnabled", SetObjectEnabled);
		L.RegFunction("SetSelectedRenderState", SetSelectedRenderState);
		L.RegFunction("OpenWithDefaultApp", OpenWithDefaultApp);
		L.RegFunction("SetCameraAnimateMaterials", SetCameraAnimateMaterials);
		L.RegFunction("SetCameraAnimateMaterialsTime", SetCameraAnimateMaterialsTime);
		L.RegFunction("UpdateGlobalShaderProperties", UpdateGlobalShaderProperties);
		L.RegFunction("GetDirtyCount", GetDirtyCount);
		L.RegFunction("IsDirty", IsDirty);
		L.RegFunction("FocusProjectWindow", FocusProjectWindow);
		L.RegFunction("RequestScriptReload", RequestScriptReload);
		L.RegFunction("IsRunningUnderCPUEmulation", IsRunningUnderCPUEmulation);
		L.RegFunction("LoadWindowLayout", LoadWindowLayout);
		L.RegFunction("SaveFilePanelInProject", SaveFilePanelInProject);
		L.RegFunction("CopySerializedIfDifferent", CopySerializedIfDifferent);
		L.RegFunction("UnloadUnusedAssetsImmediate", UnloadUnusedAssetsImmediate);
		L.RegFunction("GetDialogOptOutDecision", GetDialogOptOutDecision);
		L.RegFunction("SetDialogOptOutDecision", SetDialogOptOutDecision);
		L.RegFunction("DisplayPopupMenu", DisplayPopupMenu);
		L.RegFunction("DisplayCustomMenu", DisplayCustomMenu);
		L.RegFunction("CreateGameObjectWithHideFlags", CreateGameObjectWithHideFlags);
		L.RegFunction("CompileCSharp", CompileCSharp);
		L.RegFunction("DisplayCustomMenuWithSeparators", DisplayCustomMenuWithSeparators);
		L.RegFunction("SetDefaultParentObject", SetDefaultParentObject);
		L.RegFunction("ClearDefaultParentObject", ClearDefaultParentObject);
		L.RegFunction("New", _CreateUnityEditor_EditorUtility);
		L.RegFunction("__tostring", ToLua.op_ToString);
		L.RegVar("audioMasterMute", get_audioMasterMute, set_audioMasterMute);
		L.RegVar("scriptCompilationFailed", get_scriptCompilationFailed, null);
		L.RegFunction("SelectMenuItemFunction", UnityEditor_EditorUtility_SelectMenuItemFunction);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int _CreateUnityEditor_EditorUtility(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 0)
			{
				UnityEditor.EditorUtility obj = new UnityEditor.EditorUtility();
				ToLua.PushObject(L, obj);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to ctor method: UnityEditor.EditorUtility.New");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OpenFilePanel(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			string arg0 = ToLua.CheckString(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			string arg2 = ToLua.CheckString(L, 3);
			string o = UnityEditor.EditorUtility.OpenFilePanel(arg0, arg1, arg2);
			LuaDLL.lua_pushstring(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OpenFilePanelWithFilters(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			string arg0 = ToLua.CheckString(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			string[] arg2 = ToLua.CheckStringArray(L, 3);
			string o = UnityEditor.EditorUtility.OpenFilePanelWithFilters(arg0, arg1, arg2);
			LuaDLL.lua_pushstring(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int RevealInFinder(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			string arg0 = ToLua.CheckString(L, 1);
			UnityEditor.EditorUtility.RevealInFinder(arg0);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DisplayDialog(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 3)
			{
				string arg0 = ToLua.CheckString(L, 1);
				string arg1 = ToLua.CheckString(L, 2);
				string arg2 = ToLua.CheckString(L, 3);
				bool o = UnityEditor.EditorUtility.DisplayDialog(arg0, arg1, arg2);
				LuaDLL.lua_pushboolean(L, o);
				return 1;
			}
			else if (count == 4)
			{
				string arg0 = ToLua.CheckString(L, 1);
				string arg1 = ToLua.CheckString(L, 2);
				string arg2 = ToLua.CheckString(L, 3);
				string arg3 = ToLua.CheckString(L, 4);
				bool o = UnityEditor.EditorUtility.DisplayDialog(arg0, arg1, arg2, arg3);
				LuaDLL.lua_pushboolean(L, o);
				return 1;
			}
			else if (count == 5)
			{
				string arg0 = ToLua.CheckString(L, 1);
				string arg1 = ToLua.CheckString(L, 2);
				string arg2 = ToLua.CheckString(L, 3);
				UnityEditor.DialogOptOutDecisionType arg3 = (UnityEditor.DialogOptOutDecisionType)ToLua.CheckObject(L, 4, typeof(UnityEditor.DialogOptOutDecisionType));
				string arg4 = ToLua.CheckString(L, 5);
				bool o = UnityEditor.EditorUtility.DisplayDialog(arg0, arg1, arg2, arg3, arg4);
				LuaDLL.lua_pushboolean(L, o);
				return 1;
			}
			else if (count == 6)
			{
				string arg0 = ToLua.CheckString(L, 1);
				string arg1 = ToLua.CheckString(L, 2);
				string arg2 = ToLua.CheckString(L, 3);
				string arg3 = ToLua.CheckString(L, 4);
				UnityEditor.DialogOptOutDecisionType arg4 = (UnityEditor.DialogOptOutDecisionType)ToLua.CheckObject(L, 5, typeof(UnityEditor.DialogOptOutDecisionType));
				string arg5 = ToLua.CheckString(L, 6);
				bool o = UnityEditor.EditorUtility.DisplayDialog(arg0, arg1, arg2, arg3, arg4, arg5);
				LuaDLL.lua_pushboolean(L, o);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: UnityEditor.EditorUtility.DisplayDialog");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DisplayDialogComplex(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 5);
			string arg0 = ToLua.CheckString(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			string arg2 = ToLua.CheckString(L, 3);
			string arg3 = ToLua.CheckString(L, 4);
			string arg4 = ToLua.CheckString(L, 5);
			int o = UnityEditor.EditorUtility.DisplayDialogComplex(arg0, arg1, arg2, arg3, arg4);
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OpenFolderPanel(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			string arg0 = ToLua.CheckString(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			string arg2 = ToLua.CheckString(L, 3);
			string o = UnityEditor.EditorUtility.OpenFolderPanel(arg0, arg1, arg2);
			LuaDLL.lua_pushstring(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SaveFolderPanel(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			string arg0 = ToLua.CheckString(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			string arg2 = ToLua.CheckString(L, 3);
			string o = UnityEditor.EditorUtility.SaveFolderPanel(arg0, arg1, arg2);
			LuaDLL.lua_pushstring(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int WarnPrefab(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 4);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.CheckObject<UnityEngine.Object>(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			string arg2 = ToLua.CheckString(L, 3);
			string arg3 = ToLua.CheckString(L, 4);
			bool o = UnityEditor.EditorUtility.WarnPrefab(arg0, arg1, arg2, arg3);
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int IsPersistent(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.CheckObject<UnityEngine.Object>(L, 1);
			bool o = UnityEditor.EditorUtility.IsPersistent(arg0);
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SaveFilePanel(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 4);
			string arg0 = ToLua.CheckString(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			string arg2 = ToLua.CheckString(L, 3);
			string arg3 = ToLua.CheckString(L, 4);
			string o = UnityEditor.EditorUtility.SaveFilePanel(arg0, arg1, arg2, arg3);
			LuaDLL.lua_pushstring(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int NaturalCompare(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			string arg0 = ToLua.CheckString(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			int o = UnityEditor.EditorUtility.NaturalCompare(arg0, arg1);
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int InstanceIDToObject(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 1);
			UnityEngine.Object o = UnityEditor.EditorUtility.InstanceIDToObject(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CompressTexture(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 3 && TypeChecker.CheckTypes<int>(L, 3))
			{
				UnityEngine.Texture2D arg0 = (UnityEngine.Texture2D)ToLua.CheckObject(L, 1, typeof(UnityEngine.Texture2D));
				UnityEngine.TextureFormat arg1 = (UnityEngine.TextureFormat)ToLua.CheckObject(L, 2, typeof(UnityEngine.TextureFormat));
				int arg2 = (int)LuaDLL.lua_tonumber(L, 3);
				UnityEditor.EditorUtility.CompressTexture(arg0, arg1, arg2);
				return 0;
			}
			else if (count == 3 && TypeChecker.CheckTypes<UnityEditor.TextureCompressionQuality>(L, 3))
			{
				UnityEngine.Texture2D arg0 = (UnityEngine.Texture2D)ToLua.CheckObject(L, 1, typeof(UnityEngine.Texture2D));
				UnityEngine.TextureFormat arg1 = (UnityEngine.TextureFormat)ToLua.CheckObject(L, 2, typeof(UnityEngine.TextureFormat));
				UnityEditor.TextureCompressionQuality arg2 = (UnityEditor.TextureCompressionQuality)ToLua.ToObject(L, 3);
				UnityEditor.EditorUtility.CompressTexture(arg0, arg1, arg2);
				return 0;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: UnityEditor.EditorUtility.CompressTexture");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CompressCubemapTexture(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 3 && TypeChecker.CheckTypes<int>(L, 3))
			{
				UnityEngine.Cubemap arg0 = (UnityEngine.Cubemap)ToLua.CheckObject(L, 1, typeof(UnityEngine.Cubemap));
				UnityEngine.TextureFormat arg1 = (UnityEngine.TextureFormat)ToLua.CheckObject(L, 2, typeof(UnityEngine.TextureFormat));
				int arg2 = (int)LuaDLL.lua_tonumber(L, 3);
				UnityEditor.EditorUtility.CompressCubemapTexture(arg0, arg1, arg2);
				return 0;
			}
			else if (count == 3 && TypeChecker.CheckTypes<UnityEditor.TextureCompressionQuality>(L, 3))
			{
				UnityEngine.Cubemap arg0 = (UnityEngine.Cubemap)ToLua.CheckObject(L, 1, typeof(UnityEngine.Cubemap));
				UnityEngine.TextureFormat arg1 = (UnityEngine.TextureFormat)ToLua.CheckObject(L, 2, typeof(UnityEngine.TextureFormat));
				UnityEditor.TextureCompressionQuality arg2 = (UnityEditor.TextureCompressionQuality)ToLua.ToObject(L, 3);
				UnityEditor.EditorUtility.CompressCubemapTexture(arg0, arg1, arg2);
				return 0;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: UnityEditor.EditorUtility.CompressCubemapTexture");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetDirty(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.CheckObject<UnityEngine.Object>(L, 1);
			UnityEditor.EditorUtility.SetDirty(arg0);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int ClearDirty(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.CheckObject<UnityEngine.Object>(L, 1);
			UnityEditor.EditorUtility.ClearDirty(arg0);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int InvokeDiffTool(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 6);
			string arg0 = ToLua.CheckString(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			string arg2 = ToLua.CheckString(L, 3);
			string arg3 = ToLua.CheckString(L, 4);
			string arg4 = ToLua.CheckString(L, 5);
			string arg5 = ToLua.CheckString(L, 6);
			string o = UnityEditor.EditorUtility.InvokeDiffTool(arg0, arg1, arg2, arg3, arg4, arg5);
			LuaDLL.lua_pushstring(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CopySerialized(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.CheckObject<UnityEngine.Object>(L, 1);
			UnityEngine.Object arg1 = (UnityEngine.Object)ToLua.CheckObject<UnityEngine.Object>(L, 2);
			UnityEditor.EditorUtility.CopySerialized(arg0, arg1);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CopySerializedManagedFieldsOnly(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			object arg0 = ToLua.ToVarObject(L, 1);
			object arg1 = ToLua.ToVarObject(L, 2);
			UnityEditor.EditorUtility.CopySerializedManagedFieldsOnly(arg0, arg1);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CollectDependencies(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			UnityEngine.Object[] arg0 = ToLua.CheckObjectArray<UnityEngine.Object>(L, 1);
			UnityEngine.Object[] o = UnityEditor.EditorUtility.CollectDependencies(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CollectDeepHierarchy(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			UnityEngine.Object[] arg0 = ToLua.CheckObjectArray<UnityEngine.Object>(L, 1);
			UnityEngine.Object[] o = UnityEditor.EditorUtility.CollectDeepHierarchy(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FormatBytes(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 1 && TypeChecker.CheckTypes<int>(L, 1))
			{
				int arg0 = (int)LuaDLL.lua_tonumber(L, 1);
				string o = UnityEditor.EditorUtility.FormatBytes(arg0);
				LuaDLL.lua_pushstring(L, o);
				return 1;
			}
			else if (count == 1 && TypeChecker.CheckTypes<long>(L, 1))
			{
				long arg0 = LuaDLL.tolua_toint64(L, 1);
				string o = UnityEditor.EditorUtility.FormatBytes(arg0);
				LuaDLL.lua_pushstring(L, o);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: UnityEditor.EditorUtility.FormatBytes");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DisplayProgressBar(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			string arg0 = ToLua.CheckString(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			float arg2 = (float)LuaDLL.luaL_checknumber(L, 3);
			UnityEditor.EditorUtility.DisplayProgressBar(arg0, arg1, arg2);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DisplayCancelableProgressBar(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			string arg0 = ToLua.CheckString(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			float arg2 = (float)LuaDLL.luaL_checknumber(L, 3);
			bool o = UnityEditor.EditorUtility.DisplayCancelableProgressBar(arg0, arg1, arg2);
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int ClearProgressBar(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 0);
			UnityEditor.EditorUtility.ClearProgressBar();
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int GetObjectEnabled(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.CheckObject<UnityEngine.Object>(L, 1);
			int o = UnityEditor.EditorUtility.GetObjectEnabled(arg0);
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetObjectEnabled(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.CheckObject<UnityEngine.Object>(L, 1);
			bool arg1 = LuaDLL.luaL_checkboolean(L, 2);
			UnityEditor.EditorUtility.SetObjectEnabled(arg0, arg1);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetSelectedRenderState(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEngine.Renderer arg0 = (UnityEngine.Renderer)ToLua.CheckObject<UnityEngine.Renderer>(L, 1);
			UnityEditor.EditorSelectedRenderState arg1 = (UnityEditor.EditorSelectedRenderState)ToLua.CheckObject(L, 2, typeof(UnityEditor.EditorSelectedRenderState));
			UnityEditor.EditorUtility.SetSelectedRenderState(arg0, arg1);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int OpenWithDefaultApp(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			string arg0 = ToLua.CheckString(L, 1);
			UnityEditor.EditorUtility.OpenWithDefaultApp(arg0);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetCameraAnimateMaterials(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEngine.Camera arg0 = (UnityEngine.Camera)ToLua.CheckObject(L, 1, typeof(UnityEngine.Camera));
			bool arg1 = LuaDLL.luaL_checkboolean(L, 2);
			UnityEditor.EditorUtility.SetCameraAnimateMaterials(arg0, arg1);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetCameraAnimateMaterialsTime(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEngine.Camera arg0 = (UnityEngine.Camera)ToLua.CheckObject(L, 1, typeof(UnityEngine.Camera));
			float arg1 = (float)LuaDLL.luaL_checknumber(L, 2);
			UnityEditor.EditorUtility.SetCameraAnimateMaterialsTime(arg0, arg1);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int UpdateGlobalShaderProperties(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			float arg0 = (float)LuaDLL.luaL_checknumber(L, 1);
			UnityEditor.EditorUtility.UpdateGlobalShaderProperties(arg0);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int GetDirtyCount(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 1 && TypeChecker.CheckTypes<int>(L, 1))
			{
				int arg0 = (int)LuaDLL.lua_tonumber(L, 1);
				int o = UnityEditor.EditorUtility.GetDirtyCount(arg0);
				LuaDLL.lua_pushinteger(L, o);
				return 1;
			}
			else if (count == 1 && TypeChecker.CheckTypes<UnityEngine.Object>(L, 1))
			{
				UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.ToObject(L, 1);
				int o = UnityEditor.EditorUtility.GetDirtyCount(arg0);
				LuaDLL.lua_pushinteger(L, o);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: UnityEditor.EditorUtility.GetDirtyCount");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int IsDirty(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 1 && TypeChecker.CheckTypes<int>(L, 1))
			{
				int arg0 = (int)LuaDLL.lua_tonumber(L, 1);
				bool o = UnityEditor.EditorUtility.IsDirty(arg0);
				LuaDLL.lua_pushboolean(L, o);
				return 1;
			}
			else if (count == 1 && TypeChecker.CheckTypes<UnityEngine.Object>(L, 1))
			{
				UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.ToObject(L, 1);
				bool o = UnityEditor.EditorUtility.IsDirty(arg0);
				LuaDLL.lua_pushboolean(L, o);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: UnityEditor.EditorUtility.IsDirty");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FocusProjectWindow(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 0);
			UnityEditor.EditorUtility.FocusProjectWindow();
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int RequestScriptReload(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 0);
			UnityEditor.EditorUtility.RequestScriptReload();
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int IsRunningUnderCPUEmulation(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 0);
			bool o = UnityEditor.EditorUtility.IsRunningUnderCPUEmulation();
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LoadWindowLayout(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			string arg0 = ToLua.CheckString(L, 1);
			bool o = UnityEditor.EditorUtility.LoadWindowLayout(arg0);
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SaveFilePanelInProject(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 4)
			{
				string arg0 = ToLua.CheckString(L, 1);
				string arg1 = ToLua.CheckString(L, 2);
				string arg2 = ToLua.CheckString(L, 3);
				string arg3 = ToLua.CheckString(L, 4);
				string o = UnityEditor.EditorUtility.SaveFilePanelInProject(arg0, arg1, arg2, arg3);
				LuaDLL.lua_pushstring(L, o);
				return 1;
			}
			else if (count == 5)
			{
				string arg0 = ToLua.CheckString(L, 1);
				string arg1 = ToLua.CheckString(L, 2);
				string arg2 = ToLua.CheckString(L, 3);
				string arg3 = ToLua.CheckString(L, 4);
				string arg4 = ToLua.CheckString(L, 5);
				string o = UnityEditor.EditorUtility.SaveFilePanelInProject(arg0, arg1, arg2, arg3, arg4);
				LuaDLL.lua_pushstring(L, o);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: UnityEditor.EditorUtility.SaveFilePanelInProject");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CopySerializedIfDifferent(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.CheckObject<UnityEngine.Object>(L, 1);
			UnityEngine.Object arg1 = (UnityEngine.Object)ToLua.CheckObject<UnityEngine.Object>(L, 2);
			UnityEditor.EditorUtility.CopySerializedIfDifferent(arg0, arg1);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int UnloadUnusedAssetsImmediate(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 0)
			{
				UnityEditor.EditorUtility.UnloadUnusedAssetsImmediate();
				return 0;
			}
			else if (count == 1)
			{
				bool arg0 = LuaDLL.luaL_checkboolean(L, 1);
				UnityEditor.EditorUtility.UnloadUnusedAssetsImmediate(arg0);
				return 0;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: UnityEditor.EditorUtility.UnloadUnusedAssetsImmediate");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int GetDialogOptOutDecision(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEditor.DialogOptOutDecisionType arg0 = (UnityEditor.DialogOptOutDecisionType)ToLua.CheckObject(L, 1, typeof(UnityEditor.DialogOptOutDecisionType));
			string arg1 = ToLua.CheckString(L, 2);
			bool o = UnityEditor.EditorUtility.GetDialogOptOutDecision(arg0, arg1);
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetDialogOptOutDecision(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			UnityEditor.DialogOptOutDecisionType arg0 = (UnityEditor.DialogOptOutDecisionType)ToLua.CheckObject(L, 1, typeof(UnityEditor.DialogOptOutDecisionType));
			string arg1 = ToLua.CheckString(L, 2);
			bool arg2 = LuaDLL.luaL_checkboolean(L, 3);
			UnityEditor.EditorUtility.SetDialogOptOutDecision(arg0, arg1, arg2);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DisplayPopupMenu(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			UnityEngine.Rect arg0 = StackTraits<UnityEngine.Rect>.Check(L, 1);
			string arg1 = ToLua.CheckString(L, 2);
			UnityEditor.MenuCommand arg2 = (UnityEditor.MenuCommand)ToLua.CheckObject(L, 3, typeof(UnityEditor.MenuCommand));
			UnityEditor.EditorUtility.DisplayPopupMenu(arg0, arg1, arg2);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DisplayCustomMenu(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 5)
			{
				UnityEngine.Rect arg0 = StackTraits<UnityEngine.Rect>.Check(L, 1);
				UnityEngine.GUIContent[] arg1 = ToLua.CheckObjectArray<UnityEngine.GUIContent>(L, 2);
				int arg2 = (int)LuaDLL.luaL_checknumber(L, 3);
				UnityEditor.EditorUtility.SelectMenuItemFunction arg3 = (UnityEditor.EditorUtility.SelectMenuItemFunction)ToLua.CheckDelegate<UnityEditor.EditorUtility.SelectMenuItemFunction>(L, 4);
				object arg4 = ToLua.ToVarObject(L, 5);
				UnityEditor.EditorUtility.DisplayCustomMenu(arg0, arg1, arg2, arg3, arg4);
				return 0;
			}
			else if (count == 6 && TypeChecker.CheckTypes<int, UnityEditor.EditorUtility.SelectMenuItemFunction, object, bool>(L, 3))
			{
				UnityEngine.Rect arg0 = StackTraits<UnityEngine.Rect>.Check(L, 1);
				UnityEngine.GUIContent[] arg1 = ToLua.CheckObjectArray<UnityEngine.GUIContent>(L, 2);
				int arg2 = (int)LuaDLL.lua_tonumber(L, 3);
				UnityEditor.EditorUtility.SelectMenuItemFunction arg3 = (UnityEditor.EditorUtility.SelectMenuItemFunction)ToLua.ToObject(L, 4);
				object arg4 = ToLua.ToVarObject(L, 5);
				bool arg5 = LuaDLL.lua_toboolean(L, 6);
				UnityEditor.EditorUtility.DisplayCustomMenu(arg0, arg1, arg2, arg3, arg4, arg5);
				return 0;
			}
			else if (count == 6 && TypeChecker.CheckTypes<System.Func<int,bool>, int, UnityEditor.EditorUtility.SelectMenuItemFunction, object>(L, 3))
			{
				UnityEngine.Rect arg0 = StackTraits<UnityEngine.Rect>.Check(L, 1);
				UnityEngine.GUIContent[] arg1 = ToLua.CheckObjectArray<UnityEngine.GUIContent>(L, 2);
				System.Func<int,bool> arg2 = (System.Func<int,bool>)ToLua.ToObject(L, 3);
				int arg3 = (int)LuaDLL.lua_tonumber(L, 4);
				UnityEditor.EditorUtility.SelectMenuItemFunction arg4 = (UnityEditor.EditorUtility.SelectMenuItemFunction)ToLua.ToObject(L, 5);
				object arg5 = ToLua.ToVarObject(L, 6);
				UnityEditor.EditorUtility.DisplayCustomMenu(arg0, arg1, arg2, arg3, arg4, arg5);
				return 0;
			}
			else if (count == 7)
			{
				UnityEngine.Rect arg0 = StackTraits<UnityEngine.Rect>.Check(L, 1);
				UnityEngine.GUIContent[] arg1 = ToLua.CheckObjectArray<UnityEngine.GUIContent>(L, 2);
				System.Func<int,bool> arg2 = (System.Func<int,bool>)ToLua.CheckDelegate<System.Func<int,bool>>(L, 3);
				int arg3 = (int)LuaDLL.luaL_checknumber(L, 4);
				UnityEditor.EditorUtility.SelectMenuItemFunction arg4 = (UnityEditor.EditorUtility.SelectMenuItemFunction)ToLua.CheckDelegate<UnityEditor.EditorUtility.SelectMenuItemFunction>(L, 5);
				object arg5 = ToLua.ToVarObject(L, 6);
				bool arg6 = LuaDLL.luaL_checkboolean(L, 7);
				UnityEditor.EditorUtility.DisplayCustomMenu(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
				return 0;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: UnityEditor.EditorUtility.DisplayCustomMenu");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CreateGameObjectWithHideFlags(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			string arg0 = ToLua.CheckString(L, 1);
			UnityEngine.HideFlags arg1 = (UnityEngine.HideFlags)ToLua.CheckObject(L, 2, typeof(UnityEngine.HideFlags));
			System.Type[] arg2 = ToLua.CheckParamsObject<System.Type>(L, 3, count - 2);
			UnityEngine.GameObject o = UnityEditor.EditorUtility.CreateGameObjectWithHideFlags(arg0, arg1, arg2);
			ToLua.PushSealed(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CompileCSharp(IntPtr L)
	{
		try
		{
			//ToLua.CheckArgsCount(L, 4);
			//string[] arg0 = ToLua.CheckStringArray(L, 1);
			//string[] arg1 = ToLua.CheckStringArray(L, 2);
			//string[] arg2 = ToLua.CheckStringArray(L, 3);
			//string arg3 = ToLua.CheckString(L, 4);
			//string[] o = UnityEditor.EditorUtility.CompileCSharp(arg0, arg1, arg2, arg3);
			//ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DisplayCustomMenuWithSeparators(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 7);
			UnityEngine.Rect arg0 = StackTraits<UnityEngine.Rect>.Check(L, 1);
			string[] arg1 = ToLua.CheckStringArray(L, 2);
			bool[] arg2 = ToLua.CheckBoolArray(L, 3);
			bool[] arg3 = ToLua.CheckBoolArray(L, 4);
			int[] arg4 = ToLua.CheckNumberArray<int>(L, 5);
			UnityEditor.EditorUtility.SelectMenuItemFunction arg5 = (UnityEditor.EditorUtility.SelectMenuItemFunction)ToLua.CheckDelegate<UnityEditor.EditorUtility.SelectMenuItemFunction>(L, 6);
			object arg6 = ToLua.ToVarObject(L, 7);
			UnityEditor.EditorUtility.DisplayCustomMenuWithSeparators(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int SetDefaultParentObject(IntPtr L)
	{
		try
		{
			//ToLua.CheckArgsCount(L, 1);
			//UnityEngine.GameObject arg0 = (UnityEngine.GameObject)ToLua.CheckObject(L, 1, typeof(UnityEngine.GameObject));
			//UnityEditor.EditorUtility.SetDefaultParentObject(arg0);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int ClearDefaultParentObject(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 0)
			{
				//UnityEditor.EditorUtility.ClearDefaultParentObject();
				return 0;
			}
			else if (count == 1)
			{
				//UnityEngine.SceneManagement.Scene arg0 = StackTraits<UnityEngine.SceneManagement.Scene>.Check(L, 1);
				//UnityEditor.EditorUtility.ClearDefaultParentObject(arg0);
				return 0;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: UnityEditor.EditorUtility.ClearDefaultParentObject");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_audioMasterMute(IntPtr L)
	{
		try
		{
			LuaDLL.lua_pushboolean(L, UnityEditor.EditorUtility.audioMasterMute);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_scriptCompilationFailed(IntPtr L)
	{
		try
		{
			LuaDLL.lua_pushboolean(L, UnityEditor.EditorUtility.scriptCompilationFailed);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_audioMasterMute(IntPtr L)
	{
		try
		{
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			UnityEditor.EditorUtility.audioMasterMute = arg0;
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int UnityEditor_EditorUtility_SelectMenuItemFunction(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateTraits<UnityEditor.EditorUtility.SelectMenuItemFunction>.Create(func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateTraits<UnityEditor.EditorUtility.SelectMenuItemFunction>.Create(func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}
}
#endif
