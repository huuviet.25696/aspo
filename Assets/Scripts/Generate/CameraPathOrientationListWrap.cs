﻿//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class CameraPathOrientationListWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(CameraPathOrientationList), typeof(CameraPathPointList));
		L.RegFunction("Init", Init);
		L.RegFunction("CleanUp", CleanUp);
		L.RegFunction(".geti", get_Item);
		L.RegFunction("get_Item", get_Item);
		L.RegFunction("AddOrientation", AddOrientation);
		L.RegFunction("RemovePoint", RemovePoint);
		L.RegFunction("GetOrientation", GetOrientation);
		L.RegFunction("FromXML", FromXML);
		L.RegVar("this", _this, null);
		L.RegFunction("__eq", op_Equality);
		L.RegFunction("__tostring", ToLua.op_ToString);
		L.RegVar("interpolation", get_interpolation, set_interpolation);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int _get_this(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			CameraPathOrientationList obj = (CameraPathOrientationList)ToLua.CheckObject<CameraPathOrientationList>(L, 1);
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			CameraPathOrientation o = obj[arg0];
			ToLua.Push(L, o);
			return 1;

		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int _this(IntPtr L)
	{
		try
		{
			LuaDLL.lua_pushvalue(L, 1);
			LuaDLL.tolua_bindthis(L, _get_this, null);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int Init(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			CameraPathOrientationList obj = (CameraPathOrientationList)ToLua.CheckObject<CameraPathOrientationList>(L, 1);
			CameraPath arg0 = (CameraPath)ToLua.CheckObject<CameraPath>(L, 2);
			obj.Init(arg0);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int CleanUp(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			CameraPathOrientationList obj = (CameraPathOrientationList)ToLua.CheckObject<CameraPathOrientationList>(L, 1);
			obj.CleanUp();
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_Item(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			CameraPathOrientationList obj = (CameraPathOrientationList)ToLua.CheckObject<CameraPathOrientationList>(L, 1);
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			CameraPathOrientation o = obj[arg0];
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int AddOrientation(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 2)
			{
				CameraPathOrientationList obj = (CameraPathOrientationList)ToLua.CheckObject<CameraPathOrientationList>(L, 1);
				CameraPathControlPoint arg0 = (CameraPathControlPoint)ToLua.CheckObject<CameraPathControlPoint>(L, 2);
				obj.AddOrientation(arg0);
				return 0;
			}
			else if (count == 5)
			{
				CameraPathOrientationList obj = (CameraPathOrientationList)ToLua.CheckObject<CameraPathOrientationList>(L, 1);
				CameraPathControlPoint arg0 = (CameraPathControlPoint)ToLua.CheckObject<CameraPathControlPoint>(L, 2);
				CameraPathControlPoint arg1 = (CameraPathControlPoint)ToLua.CheckObject<CameraPathControlPoint>(L, 3);
				float arg2 = (float)LuaDLL.luaL_checknumber(L, 4);
				UnityEngine.Quaternion arg3 = ToLua.ToQuaternion(L, 5);
				CameraPathOrientation o = obj.AddOrientation(arg0, arg1, arg2, arg3);
				ToLua.Push(L, o);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: CameraPathOrientationList.AddOrientation");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int RemovePoint(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 2 && TypeChecker.CheckTypes<CameraPathOrientation>(L, 2))
			{
				CameraPathOrientationList obj = (CameraPathOrientationList)ToLua.CheckObject<CameraPathOrientationList>(L, 1);
				CameraPathOrientation arg0 = (CameraPathOrientation)ToLua.ToObject(L, 2);
				obj.RemovePoint(arg0);
				return 0;
			}
			else if (count == 2 && TypeChecker.CheckTypes<CameraPathPoint>(L, 2))
			{
				CameraPathOrientationList obj = (CameraPathOrientationList)ToLua.CheckObject<CameraPathOrientationList>(L, 1);
				CameraPathPoint arg0 = (CameraPathPoint)ToLua.ToObject(L, 2);
				obj.RemovePoint(arg0);
				return 0;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: CameraPathOrientationList.RemovePoint");
			}
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int GetOrientation(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			CameraPathOrientationList obj = (CameraPathOrientationList)ToLua.CheckObject<CameraPathOrientationList>(L, 1);
			float arg0 = (float)LuaDLL.luaL_checknumber(L, 2);
			UnityEngine.Quaternion o = obj.GetOrientation(arg0);
			ToLua.Push(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FromXML(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			CameraPathOrientationList obj = (CameraPathOrientationList)ToLua.CheckObject<CameraPathOrientationList>(L, 1);
			System.Xml.XmlNodeList arg0 = (System.Xml.XmlNodeList)ToLua.CheckObject<System.Xml.XmlNodeList>(L, 2);
			obj.FromXML(arg0);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int op_Equality(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UnityEngine.Object arg0 = (UnityEngine.Object)ToLua.ToObject(L, 1);
			UnityEngine.Object arg1 = (UnityEngine.Object)ToLua.ToObject(L, 2);
			bool o = arg0 == arg1;
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int get_interpolation(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			CameraPathOrientationList obj = (CameraPathOrientationList)o;
			CameraPathOrientationList.Interpolation ret = obj.interpolation;
			ToLua.Push(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o, "attempt to index interpolation on a nil value");
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int set_interpolation(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			CameraPathOrientationList obj = (CameraPathOrientationList)o;
			CameraPathOrientationList.Interpolation arg0 = (CameraPathOrientationList.Interpolation)ToLua.CheckObject(L, 2, typeof(CameraPathOrientationList.Interpolation));
			obj.interpolation = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o, "attempt to index interpolation on a nil value");
		}
	}
}

