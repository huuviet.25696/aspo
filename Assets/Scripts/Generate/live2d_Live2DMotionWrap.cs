//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class CLASSTAG_live2d_Live2DMotionWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(live2d.Live2DMotion), typeof(live2d.AMotion));
		L.RegFunction("loadMotion",FUNCTAG_loadMotion);
		L.RegFunction("getDurationMSec",FUNCTAG_getDurationMSec);
		L.RegFunction("getLoopDurationMSec",FUNCTAG_getLoopDurationMSec);
		L.RegFunction("updateParamExe",FUNCTAG_updateParamExe);
		L.RegFunction("isLoop",FUNCTAG_isLoop);
		L.RegFunction("setLoop",FUNCTAG_setLoop);
		L.RegFunction("getFPS",FUNCTAG_getFPS);
		L.RegFunction("setFPS",FUNCTAG_setFPS);
		L.RegFunction("New",FUNCTAG__Createlive2d_Live2DMotion);
		L.RegFunction("__tostring",ToLua.op_ToString);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG__Createlive2d_Live2DMotion(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 0)
			{
				live2d.Live2DMotion obj = new live2d.Live2DMotion();
				ToLua.PushObject(L, obj);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to ctor method: live2d.Live2DMotion.New");
			}
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_loadMotion(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			if (count == 1 && TypeChecker.CheckTypes(L, 1, typeof(string)))
			{
				string arg0 = ToLua.ToString(L, 1);
				live2d.Live2DMotion o = live2d.Live2DMotion.loadMotion(arg0);
				ToLua.PushObject(L, o);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: live2d.Live2DMotion.loadMotion");
			}
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getDurationMSec(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DMotion obj = (live2d.Live2DMotion)ToLua.CheckObject(L, 1, typeof(live2d.Live2DMotion));
			int o = obj.getDurationMSec();
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getLoopDurationMSec(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DMotion obj = (live2d.Live2DMotion)ToLua.CheckObject(L, 1, typeof(live2d.Live2DMotion));
			int o = obj.getLoopDurationMSec();
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_updateParamExe(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 5);
			live2d.Live2DMotion obj = (live2d.Live2DMotion)ToLua.CheckObject(L, 1, typeof(live2d.Live2DMotion));
			live2d.ALive2DModel arg0 = (live2d.ALive2DModel)ToLua.CheckObject(L, 2, typeof(live2d.ALive2DModel));
			long arg1 = LuaDLL.tolua_checkint64(L, 3);
			float arg2 = (float)LuaDLL.luaL_checknumber(L, 4);
			live2d.MotionQueueEnt arg3 = (live2d.MotionQueueEnt)ToLua.CheckObject(L, 5, typeof(live2d.MotionQueueEnt));
			obj.updateParamExe(arg0, arg1, arg2, arg3);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_isLoop(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DMotion obj = (live2d.Live2DMotion)ToLua.CheckObject(L, 1, typeof(live2d.Live2DMotion));
			bool o = obj.isLoop();
			LuaDLL.lua_pushboolean(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setLoop(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			live2d.Live2DMotion obj = (live2d.Live2DMotion)ToLua.CheckObject(L, 1, typeof(live2d.Live2DMotion));
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			obj.setLoop(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getFPS(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DMotion obj = (live2d.Live2DMotion)ToLua.CheckObject(L, 1, typeof(live2d.Live2DMotion));
			float o = obj.getFPS();
			LuaDLL.lua_pushnumber(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setFPS(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			live2d.Live2DMotion obj = (live2d.Live2DMotion)ToLua.CheckObject(L, 1, typeof(live2d.Live2DMotion));
			float arg0 = (float)LuaDLL.luaL_checknumber(L, 2);
			obj.setFPS(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}
}

