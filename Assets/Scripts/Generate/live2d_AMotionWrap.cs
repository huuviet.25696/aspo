//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class CLASSTAG_live2d_AMotionWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(live2d.AMotion), typeof(System.Object));
		L.RegFunction("setFadeIn",FUNCTAG_setFadeIn);
		L.RegFunction("setFadeOut",FUNCTAG_setFadeOut);
		L.RegFunction("setWeight",FUNCTAG_setWeight);
		L.RegFunction("getFadeOut",FUNCTAG_getFadeOut);
		L.RegFunction("getFadeIn",FUNCTAG_getFadeIn);
		L.RegFunction("getWeight",FUNCTAG_getWeight);
		L.RegFunction("getDurationMSec",FUNCTAG_getDurationMSec);
		L.RegFunction("getLoopDurationMSec",FUNCTAG_getLoopDurationMSec);
		L.RegFunction("getEasing",FUNCTAG_getEasing);
		L.RegFunction("updateParam",FUNCTAG_updateParam);
		L.RegFunction("updateParamExe",FUNCTAG_updateParamExe);
		L.RegFunction("__tostring",ToLua.op_ToString);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setFadeIn(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			live2d.AMotion obj = (live2d.AMotion)ToLua.CheckObject(L, 1, typeof(live2d.AMotion));
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			obj.setFadeIn(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setFadeOut(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			live2d.AMotion obj = (live2d.AMotion)ToLua.CheckObject(L, 1, typeof(live2d.AMotion));
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			obj.setFadeOut(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setWeight(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			live2d.AMotion obj = (live2d.AMotion)ToLua.CheckObject(L, 1, typeof(live2d.AMotion));
			float arg0 = (float)LuaDLL.luaL_checknumber(L, 2);
			obj.setWeight(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getFadeOut(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.AMotion obj = (live2d.AMotion)ToLua.CheckObject(L, 1, typeof(live2d.AMotion));
			int o = obj.getFadeOut();
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getFadeIn(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.AMotion obj = (live2d.AMotion)ToLua.CheckObject(L, 1, typeof(live2d.AMotion));
			int o = obj.getFadeIn();
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getWeight(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.AMotion obj = (live2d.AMotion)ToLua.CheckObject(L, 1, typeof(live2d.AMotion));
			float o = obj.getWeight();
			LuaDLL.lua_pushnumber(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getDurationMSec(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.AMotion obj = (live2d.AMotion)ToLua.CheckObject(L, 1, typeof(live2d.AMotion));
			int o = obj.getDurationMSec();
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getLoopDurationMSec(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.AMotion obj = (live2d.AMotion)ToLua.CheckObject(L, 1, typeof(live2d.AMotion));
			int o = obj.getLoopDurationMSec();
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getEasing(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			float arg0 = (float)LuaDLL.luaL_checknumber(L, 1);
			float arg1 = (float)LuaDLL.luaL_checknumber(L, 2);
			float arg2 = (float)LuaDLL.luaL_checknumber(L, 3);
			float o = live2d.AMotion.getEasing(arg0, arg1, arg2);
			LuaDLL.lua_pushnumber(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_updateParam(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			live2d.AMotion obj = (live2d.AMotion)ToLua.CheckObject(L, 1, typeof(live2d.AMotion));
			live2d.ALive2DModel arg0 = (live2d.ALive2DModel)ToLua.CheckObject(L, 2, typeof(live2d.ALive2DModel));
			live2d.MotionQueueEnt arg1 = (live2d.MotionQueueEnt)ToLua.CheckObject(L, 3, typeof(live2d.MotionQueueEnt));
			obj.updateParam(arg0, arg1);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_updateParamExe(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 5);
			live2d.AMotion obj = (live2d.AMotion)ToLua.CheckObject(L, 1, typeof(live2d.AMotion));
			live2d.ALive2DModel arg0 = (live2d.ALive2DModel)ToLua.CheckObject(L, 2, typeof(live2d.ALive2DModel));
			long arg1 = LuaDLL.tolua_checkint64(L, 3);
			float arg2 = (float)LuaDLL.luaL_checknumber(L, 4);
			live2d.MotionQueueEnt arg3 = (live2d.MotionQueueEnt)ToLua.CheckObject(L, 5, typeof(live2d.MotionQueueEnt));
			obj.updateParamExe(arg0, arg1, arg2, arg3);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}
}

