//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class CLASSTAG_UIEffectRenderQueueWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(UIEffectRenderQueue), typeof(UnityEngine.MonoBehaviour));
		L.RegFunction("RecaluatePanelDepth",FUNCTAG_RecaluatePanelDepth);
		L.RegFunction("RecalculateEffectRegion",FUNCTAG_RecalculateEffectRegion);
		L.RegFunction("UpdateRenderQ",FUNCTAG_UpdateRenderQ);
		L.RegVar("renderQ",FUNCTAG_get_renderQ, FUNCTAG_set_renderQ);
		L.RegVar("needClip",FUNCTAG_get_needClip, FUNCTAG_set_needClip);
		L.RegVar("attachGameObject",FUNCTAG_get_attachGameObject, FUNCTAG_set_attachGameObject);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_RecaluatePanelDepth(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			UIEffectRenderQueue obj = (UIEffectRenderQueue)ToLua.CheckObject(L, 1, typeof(UIEffectRenderQueue));
			obj.RecaluatePanelDepth();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_RecalculateEffectRegion(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			UIEffectRenderQueue obj = (UIEffectRenderQueue)ToLua.CheckObject(L, 1, typeof(UIEffectRenderQueue));
			obj.RecalculateEffectRegion();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_UpdateRenderQ(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			UIEffectRenderQueue obj = (UIEffectRenderQueue)ToLua.CheckObject(L, 1, typeof(UIEffectRenderQueue));
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			obj.UpdateRenderQ(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_get_renderQ(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			UIEffectRenderQueue obj = (UIEffectRenderQueue)o;
			int ret = obj.renderQ;
			LuaDLL.lua_pushinteger(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index renderQ on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_get_needClip(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			UIEffectRenderQueue obj = (UIEffectRenderQueue)o;
			bool ret = obj.needClip;
			LuaDLL.lua_pushboolean(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index needClip on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_get_attachGameObject(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			UIEffectRenderQueue obj = (UIEffectRenderQueue)o;
			UnityEngine.GameObject ret = obj.attachGameObject;
			ToLua.Push(L, ret);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index attachGameObject on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_set_renderQ(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			UIEffectRenderQueue obj = (UIEffectRenderQueue)o;
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			obj.renderQ = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index renderQ on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_set_needClip(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			UIEffectRenderQueue obj = (UIEffectRenderQueue)o;
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			obj.needClip = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index needClip on a nil value" : e.Message);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_set_attachGameObject(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			UIEffectRenderQueue obj = (UIEffectRenderQueue)o;
			UnityEngine.GameObject arg0 = (UnityEngine.GameObject)ToLua.CheckUnityObject(L, 2, typeof(UnityEngine.GameObject));
			obj.attachGameObject = arg0;
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index attachGameObject on a nil value" : e.Message);
		}
	}
}

