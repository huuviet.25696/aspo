//this source code was auto-generated by tolua#, do not modify it
using System;
using LuaInterface;

public class CLASSTAG_live2d_Live2DModelUnityWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(live2d.Live2DModelUnity), typeof(live2d.ALive2DModel));
		L.RegFunction("releaseModel",FUNCTAG_releaseModel);
		L.RegFunction("setMatrix",FUNCTAG_setMatrix);
		L.RegFunction("update",FUNCTAG_update);
		L.RegFunction("draw",FUNCTAG_draw);
		L.RegFunction("setTexture",FUNCTAG_setTexture);
		L.RegFunction("loadModel",FUNCTAG_loadModel);
		L.RegFunction("getDrawParam",FUNCTAG_getDrawParam);
		L.RegFunction("enableCullingSetting",FUNCTAG_enableCullingSetting);
		L.RegFunction("setLayer",FUNCTAG_setLayer);
		L.RegFunction("getLayer",FUNCTAG_getLayer);
		L.RegFunction("setRenderMode",FUNCTAG_setRenderMode);
		L.RegFunction("getRenderMode",FUNCTAG_getRenderMode);
		L.RegFunction("setTextureColor",FUNCTAG_setTextureColor);
		L.RegFunction("setConvertTexture",FUNCTAG_setConvertTexture);
		L.RegFunction("toStringDrawCommand",FUNCTAG_toStringDrawCommand);
		L.RegFunction("New",FUNCTAG__Createlive2d_Live2DModelUnity);
		L.RegFunction("__tostring",ToLua.op_ToString);
		L.EndClass();
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG__Createlive2d_Live2DModelUnity(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 0)
			{
				live2d.Live2DModelUnity obj = new live2d.Live2DModelUnity();
				ToLua.PushObject(L, obj);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to ctor method: live2d.Live2DModelUnity.New");
			}
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_releaseModel(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			obj.releaseModel();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setMatrix(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 2 && TypeChecker.CheckTypes(L, 1, typeof(live2d.Live2DModelUnity), typeof(float[])))
			{
				live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.ToObject(L, 1);
				float[] arg0 = ToLua.CheckNumberArray<float>(L, 2);
				obj.setMatrix(arg0);
				return 0;
			}
			else if (count == 2 && TypeChecker.CheckTypes(L, 1, typeof(live2d.Live2DModelUnity), typeof(UnityEngine.Matrix4x4)))
			{
				live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.ToObject(L, 1);
				UnityEngine.Matrix4x4 arg0 = (UnityEngine.Matrix4x4)ToLua.ToObject(L, 2);
				obj.setMatrix(arg0);
				return 0;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: live2d.Live2DModelUnity.setMatrix");
			}
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_update(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			obj.update();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_draw(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			obj.draw();
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setTexture(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			UnityEngine.Texture2D arg1 = (UnityEngine.Texture2D)ToLua.CheckUnityObject(L, 3, typeof(UnityEngine.Texture2D));
			obj.setTexture(arg0, arg1);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_loadModel(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);

			if (count == 1 && TypeChecker.CheckTypes(L, 1, typeof(System.IO.Stream)))
			{
				System.IO.Stream arg0 = (System.IO.Stream)ToLua.ToObject(L, 1);
				live2d.Live2DModelUnity o = live2d.Live2DModelUnity.loadModel(arg0);
				ToLua.PushObject(L, o);
				return 1;
			}
			else if (count == 1 && TypeChecker.CheckTypes(L, 1, typeof(string)))
			{
				string arg0 = ToLua.ToString(L, 1);
				live2d.Live2DModelUnity o = live2d.Live2DModelUnity.loadModel(arg0);
				ToLua.PushObject(L, o);
				return 1;
			}
			else
			{
				return LuaDLL.luaL_throw(L, "invalid arguments to method: live2d.Live2DModelUnity.loadModel");
			}
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getDrawParam(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			live2d.DrawParam o = obj.getDrawParam();
			ToLua.PushObject(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_enableCullingSetting(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
			obj.enableCullingSetting(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setLayer(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			obj.setLayer(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getLayer(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			int o = obj.getLayer();
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setRenderMode(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			obj.setRenderMode(arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_getRenderMode(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			int o = obj.getRenderMode();
			LuaDLL.lua_pushinteger(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setTextureColor(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 5);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			float arg1 = (float)LuaDLL.luaL_checknumber(L, 3);
			float arg2 = (float)LuaDLL.luaL_checknumber(L, 4);
			float arg3 = (float)LuaDLL.luaL_checknumber(L, 5);
			obj.setTextureColor(arg0, arg1, arg2, arg3);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_setConvertTexture(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 3);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			int arg0 = (int)LuaDLL.luaL_checknumber(L, 2);
			UnityEngine.Texture2D arg1 = (UnityEngine.Texture2D)ToLua.CheckUnityObject(L, 3, typeof(UnityEngine.Texture2D));
			obj.setConvertTexture(arg0, arg1);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_toStringDrawCommand(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			live2d.Live2DModelUnity obj = (live2d.Live2DModelUnity)ToLua.CheckObject(L, 1, typeof(live2d.Live2DModelUnity));
			string o = obj.toStringDrawCommand();
			LuaDLL.lua_pushstring(L, o);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}
}

