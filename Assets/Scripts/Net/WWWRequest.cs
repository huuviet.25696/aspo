using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using LuaInterface;


public class WWWRequest : MonoBehaviour
{
    public static WWWRequest Instance
    {
        private set;
        get;
    }

    public static void CreateInstance()
    {
        if (Instance != null)
        {
            Debug.LogError("WWWRequest.Instance already exist");
            return;
        }

        GameObject go = new GameObject("WWWRequest");
        GameObject.DontDestroyOnLoad(go);
        Instance = go.AddComponent<WWWRequest>();
    }


    public WWW Get(string url, LuaFunction luaCallback)
    {
        WWW www = new WWW(url);
        StartCoroutine(GetTask(www, luaCallback));
        return www;
    }

    private IEnumerator GetTask(WWW www, LuaFunction luaCallback)
    {
        yield return www;

        if (luaCallback != null)
        {
            luaCallback.BeginPCall();
            luaCallback.Push(www);
            luaCallback.PCall();
            luaCallback.EndPCall();
            luaCallback.Dispose();
		}
    }

    public WWW Post(string url, Dictionary<string, string> headers, byte[] data, LuaFunction luaCallback)
    {
        WWW www = new WWW(url, data, headers);
        StartCoroutine(PostTask(www, luaCallback));
        return www;
    }

    private IEnumerator PostTask(WWW www, LuaFunction luaCallback)
    {
        yield return www;

        if (luaCallback != null)
        {
            luaCallback.BeginPCall();
            luaCallback.Push(www);
            luaCallback.PCall();
            luaCallback.EndPCall();
            luaCallback.Dispose();
        }
    }

}

