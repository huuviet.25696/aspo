using System;
using System.Collections.Generic;
using UnityEngine;

public class Hud : MonoBehaviour
{
    public Transform target;
    public Camera gameCamera;
    public Camera uiCamera;
    public bool isAutoUpdate = false;
	public WalkerEventHandler walkerEventHandler;
    private Transform cacheTrans;
	private static Dictionary<int, Hud> hudObjs = new Dictionary<int, Hud>();
	
	public bool isVisible = false;

    public static void CallUpdateAll()
    {
		foreach (var hud in hudObjs.Values)
		{
			hud.CheckShow();
		}
    }

	void Awake()
	{
		hudObjs.Add(GetInstanceID(), this);
	}

    public void Start()
    {
		cacheTrans = gameObject.transform;
        CheckShow();
    }

    public void OnDestroy()
    {
		if (hudObjs.ContainsKey(GetInstanceID()))
        {
			hudObjs.Remove(GetInstanceID());
		}
		ResetHud();
		uiCamera = null;
    }

    public void CheckShow()
    {
		if (isAutoUpdate == false || target == null || cacheTrans==null|| gameCamera == null || uiCamera == null)
        {
            return;
        }
		if ((walkerEventHandler != null) && (walkerEventHandler.eventid > 0))
		{
			CheckPos(walkerEventHandler.isVisible);
		}
		else
		{
			var pos = gameCamera.WorldToViewportPoint(target.position);
			pos.x = pos.x * gameCamera.rect.size.x + gameCamera.rect.position.x;
			pos.y = pos.y * gameCamera.rect.size.y + gameCamera.rect.position.y;
			bool isVisible = !(pos.x < 0f || pos.x > 1f || pos.y < 0f || pos.y > 1f || pos.z < 0 || !gameCamera.enabled || ((gameCamera.cullingMask & (1 << target.gameObject.layer)) == 0));
			CheckPos(isVisible, pos);
		}
    }

	public void CheckPos(bool visible)
	{
		isVisible = visible;
		if (visible)
		{
			var pos = gameCamera.WorldToViewportPoint(target.position);
			gameObject.SetActive(true);
			cacheTrans.position = uiCamera.ViewportToWorldPoint(pos);
			cacheTrans.localPosition = new Vector3(cacheTrans.localPosition.x, cacheTrans.localPosition.y, 0f);
		}
		else
		{
			gameObject.SetActive(false);
		}
	}
	public void CheckPos(bool visible, Vector3 viewPos)
	{
		isVisible = visible;
		if (visible)
		{
			gameObject.SetActive(true);
			cacheTrans.position = uiCamera.ViewportToWorldPoint(viewPos);
			cacheTrans.localPosition = new Vector3(cacheTrans.localPosition.x, cacheTrans.localPosition.y, 0f);
		}
		else
		{
			gameObject.SetActive(false);
		}
		
	}

	public void ResetHud()
	{
		isAutoUpdate = false;
		target = null;
		gameCamera = null;
		walkerEventHandler = null;
		isVisible = false;
		gameObject.SetActive(false);
	}


	public static Hud GetBatchObject(int id)
	{
		Hud hud;
		if (hudObjs.TryGetValue(id, out hud))
		{
			return hud;
		}
		return null;
	}
}
