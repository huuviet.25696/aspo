﻿//this source code was auto-generated by tolua#, do not modify it
using System;
using UnityEngine;
using LuaInterface;

public static class LuaBinder
{
	public static void Bind(LuaState L)
	{
		float t = Time.realtimeSinceStartup;
		L.BeginModule(null);
		//TweenFillWrap.Register(L);
		//UITweenerWrap.Register(L);
		//L.BeginModule("DG");
		//L.BeginModule("Tweening");
		//DG_Tweening_TweenerWrap.Register(L);
		//DG_Tweening_TweenWrap.Register(L);
		//L.BeginModule("Core");
		//DG_Tweening_Core_TweenerCore_UnityEngine_Quaternion_UnityEngine_Vector3_DG_Tweening_Plugins_Options_QuaternionOptionsWrap.Register(L);
		//DG_Tweening_Core_ABSSequentiableWrap.Register(L);
		//L.RegFunction("DOGetter_UnityEngine_Quaternion", DG_Tweening_Core_DOGetter_UnityEngine_Quaternion);
		//L.RegFunction("DOSetter_UnityEngine_Quaternion", DG_Tweening_Core_DOSetter_UnityEngine_Quaternion);
		//L.EndModule();
		//L.EndModule();
		//L.EndModule();
		L.BeginModule("More");
		GameUtilsWrap.Register(L);
		L.EndModule();
		//DuoSDK
		L.BeginModule("ASPO");
		CLASSTAG_DuoSDKWrap.Register(L);
		L.EndModule();
#if UNITY_EDITOR
		L.BeginModule("UnityEditor");
		UnityEditor_AssetDatabaseWrap.Register(L);
		UnityEditor_AnimationUtilityWrap.Register(L);
		UnityEditor_AnimationClipCurveDataWrap.Register(L);
		UnityEditor_EditorGUILayoutWrap.Register(L);
		UnityEditor_EditorWindowWrap.Register(L);
		UnityEditor_EditorUtilityWrap.Register(L);
		UnityEditor_PrefabUtilityWrap.Register(L);
		L.EndModule();
#endif
		L.BeginModule("C_api");
		CLASSTAG_UtilsWrap.Register(L);
		CLASSTAG_TimerWrap.Register(L);
		CLASSTAG_FileHandlerWrap.Register(L);
		CLASSTAG_IOHelperWrap.Register(L);
		CLASSTAG_UIEventHandlerWrap.Register(L);
		CLASSTAG_GameObjectContainerWrap.Register(L);
		TcpClientWrap.Register(L);
		WWWRequestWrap.Register(L);
		CLASSTAG_ResourceManagerWrap.Register(L);
		CLASSTAG_EasyTouchHandlerWrap.Register(L);
		CLASSTAG_HudWrap.Register(L);
		CLASSTAG_Map2DWrap.Register(L);
		CLASSTAG_Map3DWrap.Register(L);
		CLASSTAG_Map2DCameraWrap.Register(L);
		CLASSTAG_Map2DWalkerWrap.Register(L);
		CLASSTAG_Map3DWalkerWrap.Register(L);
		CLASSTAG_EmojiAnimationControllerWrap.Register(L);
		CLASSTAG_GameEventHandlerWrap.Register(L);
		CLASSTAG_UIEffectRenderQueueWrap.Register(L);
		CLASSTAG_HotkeyHandlerWrap.Register(L);
		CLASSTAG_AudioRecordWrap.Register(L);
		//Auviis
		CLASSTAG_AuviisSDKWrap.Register(L);
		//CLASSTAG_QiniuCloudWrap.Register(L);
		CLASSTAG_BindGuideObjectWrap.Register(L);
		CLASSTAG_UILabelHUDWrap.Register(L);
		CLASSTAG_AnimEffectInfoWrap.Register(L);
		CLASSTAG_AnimEffectWrap.Register(L);
		CLASSTAG_AttachCameraHandlerWrap.Register(L);
		CLASSTAG_FrameSyncHandlerWrap.Register(L);
		CLASSTAG_DataContainerWrap.Register(L);
		CLASSTAG_MD5HashingWrap.Register(L);
		//CLASSTAG_SPSDKWrap.Register(L);
		CLASSTAG_PlatformAPIWrap.Register(L);
		//CLASSTAG_TalkingDataWrap.Register(L);
		//CLASSTAG_PhotoReaderManagerWrap.Register(L);
		//CLASSTAG_AntaresQRCodeUtilWrap.Register(L);
		CLASSTAG_WebCamTextureHelperWrap.Register(L);
		//CLASSTAG_XinGeSdkWrap.Register(L);
		CLASSTAG_RenderObjectHandlerWrap.Register(L);
		CLASSTAG_WalkerEventHandlerWrap.Register(L);
		CLASSTAG_CGPlayerWrap.Register(L);
		CLASSTAG_BorderMoveWrap.Register(L);
		CLASSTAG_ChainEffectWrap.Register(L);
		L.EndModule();

		L.BeginModule("System");
		L.BeginModule("IO");
		System_IO_FileWrap.Register(L);
		System_IO_PathWrap.Register(L);
		System_IO_DirectoryWrap.Register(L);
		L.EndModule();
		L.BeginModule("Diagnostics");
		System_Diagnostics_StopwatchWrap.Register(L);
		L.EndModule();
		L.EndModule();

		L.BeginModule("DOTween");
        CLASSTAG_DG_Tweening_DOTweenWrap.Register(L);
        CLASSTAG_DG_Tweening_SequenceWrap.Register(L);
        DG_Tweening_TweenWrap.Register(L);
		CLASSTAG_DG_Tweening_ShortcutExtensionsWrap.Register(L);
		CLASSTAG_DG_Tweening_TweenSettingsExtensionsWrap.Register(L);
		L.EndModule();

		L.BeginModule("Live2d");
		CLASSTAG_Live2dHandlerWrap.Register(L);
		CLASSTAG_live2d_ALive2DModelWrap.Register(L);
		CLASSTAG_live2d_AMotionWrap.Register(L);
		CLASSTAG_live2d_Live2DModelUnityWrap.Register(L);
		CLASSTAG_live2d_Live2DMotionWrap.Register(L);
		CLASSTAG_live2d_MotionQueueManagerWrap.Register(L);
		CLASSTAG_live2d_framework_L2DPhysicsWrap.Register(L);
		L.EndModule();

		L.BeginModule("Spine");
		CLASSTAG_SpineHandlerWrap.Register(L);
		L.EndModule();

		L.BeginModule("GrayScene");
		CLASSTAG_GraySceneHandlerWrap.Register(L);
		L.EndModule();

		L.BeginModule("CameraPath3");
		CameraPathWrap.Register(L);
		CameraPathAnimatorWrap.Register(L);
		CameraPathControlPointWrap.Register(L);
		L.EndModule();

		//L.BeginModule("Share");
		//CLASSTAG_ShareSDKManagerWrap.Register(L);
		//CLASSTAG_cn_sharesdk_unity3d_ShareSDKWrap.Register(L);
		//CLASSTAG_cn_sharesdk_unity3d_ShareContentWrap.Register(L);
		//L.EndModule();


		L.BeginModule("NGUI");
		UI2DSpriteWrap.Register(L);
		UIAtlasWrap.Register(L);
		UIBasicSpriteWrap.Register(L);
		UIButtonColorWrap.Register(L);
		UIButtonScaleWrap.Register(L);
		UIButtonWrap.Register(L);
		UICameraWrap.Register(L);
		UICenterOnChildWrap.Register(L);
		UIFontWrap.Register(L);
		UIGridWrap.Register(L);
		UIInputWrap.Register(L);
		UILabelWrap.Register(L);
		UIPanelWrap.Register(L);
		UIPopupListWrap.Register(L);
		UIProgressBarWrap.Register(L);
		UIRectWrap.Register(L);
		UIAnchorWrap.Register(L);
		UIRootWrap.Register(L);
		UIAnchorWrap.Register(L);
		UIRectWrap.Register(L);
		UIScrollBarWrap.Register(L);
		UIScrollViewWrap.Register(L);
		UISliderWrap.Register(L);
		UISpriteAnimationWrap.Register(L);
		CLASSTAG_UISpriteDataWrap.Register(L);
		UITableWrap.Register(L);
		UITextureWrap.Register(L);
		UIToggleWrap.Register(L);
		UITweenerWrap.Register(L);
		TweenFillWrap.Register(L);
		UIWidgetWrap.Register(L);
		UIDragScrollViewWrap.Register(L);
		UISpriteWrap.Register(L);
		UITextTypeWriterWrap.Register(L);
		UITextListWrap.Register(L);
		TweenHeightWrap.Register(L);
		TweenPositionWrap.Register(L);
		TweenRotationWrap.Register(L);
		TweenAlphaWrap.Register(L);
        CLASSTAG_TweenScaleWrap.Register(L);
		CLASSTAG_SpringPanelWrap.Register(L);
		NGUIMathWrap.Register(L);
		NGUIToolsWrap.Register(L);
		UIRect_AnchorPointWrap.Register(L);
		UIWrapContentWrap.Register(L);
		CLASSTAG_UIDragObjectWrap.Register(L);
		TweenWidthWrap.Register(L);
		L.EndModule();

		L.BeginModule("UnityEngine");
		UnityEngine_Rigidbody2DWrap.Register(L);
		UnityEngine_HingeJoint2DWrap.Register(L);
		UnityEngine_ComponentWrap.Register(L);
		UnityEngine_TransformWrap.Register(L);
		UnityEngine_RectTransformWrap.Register(L);
		UnityEngine_MaterialWrap.Register(L);
		UnityEngine_LightWrap.Register(L);
		UnityEngine_CameraWrap.Register(L);
		UnityEngine_AudioSourceWrap.Register(L);
		UnityEngine_BehaviourWrap.Register(L);
		UnityEngine_MonoBehaviourWrap.Register(L);
		UnityEngine_GameObjectWrap.Register(L);
		UnityEngine_TrackedReferenceWrap.Register(L);
		UnityEngine_ApplicationWrap.Register(L);
		UnityEngine_PhysicsWrap.Register(L);
		UnityEngine_ColliderWrap.Register(L);
		UnityEngine_TimeWrap.Register(L);
		UnityEngine_TextureWrap.Register(L);
		UnityEngine_Texture2DWrap.Register(L);
		UnityEngine_ShaderWrap.Register(L);
		UnityEngine_RendererWrap.Register(L);
		UnityEngine_WWWWrap.Register(L);
		UnityEngine_ScreenWrap.Register(L);
		UnityEngine_AudioClipWrap.Register(L);
		UnityEngine_AssetBundleWrap.Register(L);
		UnityEngine_ParticleSystemWrap.Register(L);
		UnityEngine_AsyncOperationWrap.Register(L);
		UnityEngine_AnimatorWrap.Register(L);
		UnityEngine_InputWrap.Register(L);
		UnityEngine_SkinnedMeshRendererWrap.Register(L);
		UnityEngine_QualitySettingsWrap.Register(L);
		UnityEngine_RenderSettingsWrap.Register(L);
		UnityEngine_RandomWrap.Register(L);
		UnityEngine_RectWrap.Register(L);
		UnityEngine_SceneManagement_SceneManagerWrap.Register(L);
		UnityEngine_SceneManagement_SceneWrap.Register(L);
		UnityEngine_RuntimeAnimatorControllerWrap.Register(L);
		UnityEngine_AnimatorOverrideControllerWrap.Register(L);
		UnityEngine_AnimationClipWrap.Register(L);
		UnityEngine_MotionWrap.Register(L);
		UnityEngine_ScriptableObjectWrap.Register(L);
		UnityEngine_WWWFormWrap.Register(L);
		UnityEngine_Networking_UnityWebRequestWrap.Register(L);
		UnityEngine_Networking_DownloadHandlerWrap.Register(L);
		UnityEngine_Networking_UploadHandlerWrap.Register(L);
		//CLASSTAG_UnityEngine_ProfilerWrap.Register(L);
		UnityEngine_PlayerPrefsWrap.Register(L);
		UnityEngine_AnimationCurveWrap.Register(L);
		UnityEngine_KeyframeWrap.Register(L);
		UnityEngine_ShaderWrap.Register(L);
		UnityEngine_SystemInfoWrap.Register(L);
		UnityEngine_AnimationClipPairWrap.Register(L);
		//CLASSTAG_UnityEngine_NetworkWrap.Register(L);
		//UnityEngine_NetworkPlayerWrap.Register(L);
		UnityEngine_WebCamTextureWrap.Register(L);
		UnityEngine_AudioListenerWrap.Register(L);
#if UNITY_EDITOR
		UnityEngine_GUILayoutWrap.Register(L);
		UnityEngine_GUIStyleWrap.Register(L);
		UnityEngine_GUILayoutOptionWrap.Register(L);
		UnityEngine_GUIContentWrap.Register(L);
		UnityEngine_ScriptableObjectWrap.Register(L);
#endif
		//L.BeginModule("Experimental");
		//L.BeginModule("Director");
		//CLASSTAG_UnityEngine_Experimental_Director_DirectorPlayerWrap.Register(L);
		//L.EndModule();
		//L.EndModule();
		L.BeginModule("Events");
		L.RegFunction("UnityAction", UnityEngine_Events_UnityAction);
		L.EndModule();
		L.BeginModule("Camera");
		L.RegFunction("CameraCallback", UnityEngine_Camera_CameraCallback);
		L.EndModule();
		L.BeginModule("Application");
		L.RegFunction("LogCallback", UnityEngine_Application_LogCallback);
		L.RegFunction("AdvertisingIdentifierCallback", UnityEngine_Application_AdvertisingIdentifierCallback);
		L.EndModule();
		L.BeginModule("AudioClip");
		L.RegFunction("PCMReaderCallback", UnityEngine_AudioClip_PCMReaderCallback);
		L.RegFunction("PCMSetPositionCallback", UnityEngine_AudioClip_PCMSetPositionCallback);
		L.EndModule();
		L.EndModule();
		L.BeginModule("System");
		L.RegFunction("Action", System_Action);
		L.RegFunction("Predicate_int", System_Predicate_int);
		L.RegFunction("Action_int", System_Action_int);
		L.RegFunction("Comparison_int", System_Comparison_int);
		L.EndModule();
		L.EndModule();
		L.BeginPreLoad();
		L.AddPreLoad("UnityEngine.MeshRenderer", LuaOpen_UnityEngine_MeshRenderer, typeof(UnityEngine.MeshRenderer));
#if UNITY_5
		L.AddPreLoad("UnityEngine.ParticleEmitter", LuaOpen_UnityEngine_ParticleEmitter, typeof(UnityEngine.ParticleEmitter));
		L.AddPreLoad("UnityEngine.ParticleRenderer", LuaOpen_UnityEngine_ParticleRenderer, typeof(UnityEngine.ParticleRenderer));
		L.AddPreLoad("UnityEngine.ParticleAnimator", LuaOpen_UnityEngine_ParticleAnimator, typeof(UnityEngine.ParticleAnimator));
#endif
		L.AddPreLoad("UnityEngine.BoxCollider", LuaOpen_UnityEngine_BoxCollider, typeof(UnityEngine.BoxCollider));
		L.AddPreLoad("UnityEngine.MeshCollider", LuaOpen_UnityEngine_MeshCollider, typeof(UnityEngine.MeshCollider));
		L.AddPreLoad("UnityEngine.SphereCollider", LuaOpen_UnityEngine_SphereCollider, typeof(UnityEngine.SphereCollider));
		L.AddPreLoad("UnityEngine.CharacterController", LuaOpen_UnityEngine_CharacterController, typeof(UnityEngine.CharacterController));
		L.AddPreLoad("UnityEngine.CapsuleCollider", LuaOpen_UnityEngine_CapsuleCollider, typeof(UnityEngine.CapsuleCollider));
		L.AddPreLoad("UnityEngine.Animation", LuaOpen_UnityEngine_Animation, typeof(UnityEngine.Animation));
		L.AddPreLoad("UnityEngine.AnimationClip", LuaOpen_UnityEngine_AnimationClip, typeof(UnityEngine.AnimationClip));
		L.AddPreLoad("UnityEngine.AnimationState", LuaOpen_UnityEngine_AnimationState, typeof(UnityEngine.AnimationState));
		L.AddPreLoad("UnityEngine.BlendWeights", LuaOpen_UnityEngine_BlendWeights, typeof(SkinWeights));
		L.AddPreLoad("UnityEngine.RenderTexture", LuaOpen_UnityEngine_RenderTexture, typeof(UnityEngine.RenderTexture));
		L.AddPreLoad("UnityEngine.Rigidbody", LuaOpen_UnityEngine_Rigidbody, typeof(UnityEngine.Rigidbody));
		L.EndPreLoad();
		//Debugger.Log("Register lua type cost time: {0}", Time.realtimeSinceStartup - t);
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DG_Tweening_Core_DOGetter_UnityEngine_Quaternion(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(DG.Tweening.Core.DOGetter<UnityEngine.Quaternion>), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(DG.Tweening.Core.DOGetter<UnityEngine.Quaternion>), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int DG_Tweening_Core_DOSetter_UnityEngine_Quaternion(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(DG.Tweening.Core.DOSetter<UnityEngine.Quaternion>), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(DG.Tweening.Core.DOSetter<UnityEngine.Quaternion>), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}
	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int UnityEngine_Events_UnityAction(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.Events.UnityAction), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.Events.UnityAction), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int UnityEngine_Camera_CameraCallback(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.Camera.CameraCallback), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.Camera.CameraCallback), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int UnityEngine_Application_LogCallback(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.Application.LogCallback), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.Application.LogCallback), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int UnityEngine_Application_AdvertisingIdentifierCallback(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.Application.AdvertisingIdentifierCallback), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.Application.AdvertisingIdentifierCallback), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int UnityEngine_AudioClip_PCMReaderCallback(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.AudioClip.PCMReaderCallback), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.AudioClip.PCMReaderCallback), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int UnityEngine_AudioClip_PCMSetPositionCallback(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.AudioClip.PCMSetPositionCallback), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(UnityEngine.AudioClip.PCMSetPositionCallback), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int System_Action(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(System.Action), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(System.Action), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int System_Predicate_int(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(System.Predicate<int>), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(System.Predicate<int>), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int System_Action_int(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(System.Action<int>), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(System.Action<int>), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int System_Comparison_int(IntPtr L)
	{
		try
		{
			int count = LuaDLL.lua_gettop(L);
			LuaFunction func = ToLua.CheckLuaFunction(L, 1);

			if (count == 1)
			{
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(System.Comparison<int>), func);
				ToLua.Push(L, arg1);
			}
			else
			{
				LuaTable self = ToLua.CheckLuaTable(L, 2);
				Delegate arg1 = DelegateFactory.CreateDelegate(typeof(System.Comparison<int>), func, self);
				ToLua.Push(L, arg1);
			}
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_MeshRenderer(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_MeshRendererWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.MeshRenderer));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}
#if UNITY_5
	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_ParticleEmitter(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			CLASSTAG_UnityEngine_ParticleEmitterWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.ParticleEmitter));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_ParticleRenderer(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			CLASSTAG_UnityEngine_ParticleRendererWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.ParticleRenderer));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_ParticleAnimator(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			CLASSTAG_UnityEngine_ParticleAnimatorWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.ParticleAnimator));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}
#endif
	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_BoxCollider(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_BoxColliderWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.BoxCollider));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_MeshCollider(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_MeshColliderWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.MeshCollider));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_SphereCollider(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_SphereColliderWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.SphereCollider));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_CharacterController(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_CharacterControllerWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.CharacterController));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_CapsuleCollider(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_CapsuleColliderWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.CapsuleCollider));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_Animation(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_AnimationWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.Animation));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_AnimationClip(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_AnimationClipWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.AnimationClip));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_AnimationState(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_AnimationStateWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.AnimationState));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_BlendWeights(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_BlendWeightsWrap.Register(state);
			int reference = state.GetMetaReference(typeof(SkinWeights));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_RenderTexture(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_RenderTextureWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.RenderTexture));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int LuaOpen_UnityEngine_Rigidbody(IntPtr L)
	{
		try
		{
			LuaState state = LuaState.Get(L);
			state.BeginPreModule("UnityEngine");
			UnityEngine_RigidbodyWrap.Register(state);
			int reference = state.GetMetaReference(typeof(UnityEngine.Rigidbody));
			state.EndPreModule(L, reference);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}
}

