using System.Collections.Generic;
using UnityEngine;

public class CLASSTAG_GridMapConfig
{
    public string id;
    public int xTile;
    public int yTile;

    public List<CLASSTAG_GridMapEffectData> fgEffectList = new List<CLASSTAG_GridMapEffectData>();
    public List<CLASSTAG_GridMapEffectData> bgEffectList = new List<CLASSTAG_GridMapEffectData>();
    public List<CLASSTAG_GridMapEffectData> tfEffectList = new List<CLASSTAG_GridMapEffectData>();
    public List<CLASSTAG_GridMapEffectData> mcEffectList = new List<CLASSTAG_GridMapEffectData>();
    public List<CLASSTAG_GridMapTransferData> transferList = new List<CLASSTAG_GridMapTransferData>();
}

public class CLASSTAG_GridMapEffectData
{
	public string name;
	public Vector2 pos;
	public Vector3 rotation;
	public Vector3 scale;

	//Effect
	public float width;
	public float height;
	public float rotate;
	public Vector2 offset;
}
    
public class CLASSTAG_GridMapTransferData
{
    public int idx;
    public Vector2 pos;
    public Vector2 size;
}