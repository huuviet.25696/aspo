using System;
using System.Collections.Generic;
using LuaInterface;
using UnityEngine;

public class CLASSTAG_Map3DWalkerWrap
{
    public static void Register(LuaState L)
    {
        L.BeginClass(typeof(Map3DWalker), typeof(UnityEngine.MonoBehaviour));
        L.RegFunction("WalkTo",FUNCTAG_WalkTo);
        L.RegFunction("StopWalk",FUNCTAG_StopWalk);
        L.RegFunction("Follow",FUNCTAG_Follow);
        L.RegFunction("GetPath",FUNCTAG_GetPath);
        L.RegFunction("SetWalkEndCallback",FUNCTAG_SetWalkEndCallback);
        L.RegFunction("SetWalkStartCallback",FUNCTAG_SetWalkStartCallback);
        L.RegFunction("GetWayPoint",FUNCTAG_GetWayPoint);
        L.RegFunction("GetWayPointIndex",FUNCTAG_GetWayPointIndex);
        L.RegFunction("SetTraversableTags",FUNCTAG_SetTraversableTags);
        L.RegFunction("SetMapID",FUNCTAG_SetMapID);
        L.RegVar("moveTransform",FUNCTAG_get_moveTransform, FUNCTAG_set_moveTransform);
        L.RegVar("rotateTransform",FUNCTAG_get_rotateTransform, FUNCTAG_set_rotateTransform);
        L.RegVar("moveable",FUNCTAG_get_moveable, FUNCTAG_set_moveable);
        L.RegVar("moveSpeed",FUNCTAG_get_moveSpeed, FUNCTAG_set_moveSpeed);
		L.RegVar("climbSpeed",FUNCTAG_get_climbSpeed, FUNCTAG_set_climbSpeed);
        L.RegVar("rotateSpeed",FUNCTAG_get_rotateSpeed, FUNCTAG_set_rotateSpeed);
        L.EndClass();
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_WalkTo(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 4);
            Map3DWalker obj = (Map3DWalker)ToLua.CheckObject(L, 1, typeof(Map3DWalker));
            float arg0 = (float)LuaDLL.luaL_checknumber(L, 2);
            float arg1 = (float)LuaDLL.luaL_checknumber(L, 3);
            bool arg2 = LuaDLL.luaL_checkboolean(L, 4);
            obj.WalkTo(arg0, arg1, arg2);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_StopWalk(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 1);
            Map3DWalker obj = (Map3DWalker)ToLua.CheckObject(L, 1, typeof(Map3DWalker));
            obj.StopWalk();
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_Follow(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 3);
            Map3DWalker obj = (Map3DWalker)ToLua.CheckObject(L, 1, typeof(Map3DWalker));
            Map3DWalker arg0 = (Map3DWalker)ToLua.CheckUnityObject(L, 2, typeof(Map3DWalker));
            float arg1 = (float)LuaDLL.luaL_checknumber(L, 3);
            obj.Follow(arg0, arg1);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }


    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_GetPath(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 1);
            Map3DWalker obj = (Map3DWalker)ToLua.CheckObject(L, 1, typeof(Map3DWalker));
            List<Vector3> path = obj.FUNCTAG_GetPath();

            LuaDLL.lua_newtable(L);
            if (path != null)
            {
                for (int i = 0; i < path.Count; i++)
                {
                    ToLua.Push(L, i * 2 + 1);
                    ToLua.Push(L, path[i].x);
                    LuaDLL.lua_settable(L, -3);

                    ToLua.Push(L, i * 2 + 2);
                    ToLua.Push(L, path[i].y);
                    LuaDLL.lua_settable(L, -3);
                }
            }
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_SetWalkEndCallback(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 2);
            Map3DWalker obj = (Map3DWalker)ToLua.CheckObject(L, 1, typeof(Map3DWalker));
            LuaFunction arg0 = ToLua.CheckLuaFunction(L, 2);
            obj.SetWalkEndCallback(arg0);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_SetWalkStartCallback(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 2);
            Map3DWalker obj = (Map3DWalker)ToLua.CheckObject(L, 1, typeof(Map3DWalker));
            LuaFunction arg0 = ToLua.CheckLuaFunction(L, 2);
            obj.SetWalkStartCallback(arg0);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_GetWayPoint(IntPtr L)
    {
        try
        {
            Map3DWalker obj = (Map3DWalker)ToLua.CheckObject(L, 1, typeof(Map3DWalker));
            Vector2 pos = obj.FUNCTAG_GetWayPoint();
            LuaDLL.lua_pushnumber(L, pos.x);
            LuaDLL.lua_pushnumber(L, pos.y);
            return 2;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_GetWayPointIndex(IntPtr L)
    {
        try
        {
            Map3DWalker obj = (Map3DWalker)ToLua.CheckObject(L, 1, typeof(Map3DWalker));
            int index = obj.GetWayPointIndex();
            LuaDLL.lua_pushnumber(L, index);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_SetTraversableTags(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 2);
            Map3DWalker obj = (Map3DWalker)ToLua.CheckObject(L, 1, typeof(Map3DWalker));
            int arg0 = LuaDLL.lua_tointeger(L, 2);
            obj.SetTraversableTags(arg0);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_SetMapID(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 2);
            Map3DWalker obj = (Map3DWalker)ToLua.CheckObject(L, 1, typeof(Map3DWalker));
            int arg0 = LuaDLL.lua_tointeger(L, 2);
            obj.SetMapID(arg0);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }


    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_get_moveTransform(IntPtr L)
    {
        object o = null;

        try
        {
            o = ToLua.ToObject(L, 1);
            Map3DWalker obj = (Map3DWalker)o;
            UnityEngine.Transform ret = obj.moveTransform;
            ToLua.Push(L, ret);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index moveTransform on a nil value" : e.Message);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_get_rotateTransform(IntPtr L)
    {
        object o = null;

        try
        {
            o = ToLua.ToObject(L, 1);
            Map3DWalker obj = (Map3DWalker)o;
            UnityEngine.Transform ret = obj.rotateTransform;
            ToLua.Push(L, ret);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index rotateTransform on a nil value" : e.Message);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_get_moveable(IntPtr L)
    {
        object o = null;

        try
        {
            o = ToLua.ToObject(L, 1);
            Map3DWalker obj = (Map3DWalker)o;
            bool ret = obj.moveable;
            LuaDLL.lua_pushboolean(L, ret);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index moveable on a nil value" : e.Message);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_get_moveSpeed(IntPtr L)
    {
        object o = null;

        try
        {
            o = ToLua.ToObject(L, 1);
            Map3DWalker obj = (Map3DWalker)o;
            float ret = obj.moveSpeed;
            LuaDLL.lua_pushnumber(L, ret);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index moveSpeed on a nil value" : e.Message);
        }
    }

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_get_climbSpeed(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			Map3DWalker obj = (Map3DWalker)o;
			float ret = obj.climbSpeed;
			LuaDLL.lua_pushnumber(L, ret);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index climbSpeed on a nil value" : e.Message);
		}
	}



    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_get_rotateSpeed(IntPtr L)
    {
        object o = null;

        try
        {
            o = ToLua.ToObject(L, 1);
            Map3DWalker obj = (Map3DWalker)o;
            float ret = obj.rotateSpeed;
            LuaDLL.lua_pushnumber(L, ret);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index rotateSpeed on a nil value" : e.Message);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_set_moveTransform(IntPtr L)
    {
        object o = null;

        try
        {
            o = ToLua.ToObject(L, 1);
            Map3DWalker obj = (Map3DWalker)o;
            UnityEngine.Transform arg0 = (UnityEngine.Transform)ToLua.CheckUnityObject(L, 2, typeof(UnityEngine.Transform));
            obj.moveTransform = arg0;
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index moveTransform on a nil value" : e.Message);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_set_rotateTransform(IntPtr L)
    {
        object o = null;

        try
        {
            o = ToLua.ToObject(L, 1);
            Map3DWalker obj = (Map3DWalker)o;
            UnityEngine.Transform arg0 = (UnityEngine.Transform)ToLua.CheckUnityObject(L, 2, typeof(UnityEngine.Transform));
            obj.rotateTransform = arg0;
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index rotateTransform on a nil value" : e.Message);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_set_moveable(IntPtr L)
    {
        object o = null;

        try
        {
            o = ToLua.ToObject(L, 1);
            Map3DWalker obj = (Map3DWalker)o;
            bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
            obj.moveable = arg0;
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index moveable on a nil value" : e.Message);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_set_moveSpeed(IntPtr L)
    {
        object o = null;

        try
        {
            o = ToLua.ToObject(L, 1);
            Map3DWalker obj = (Map3DWalker)o;
            float arg0 = (float)LuaDLL.luaL_checknumber(L, 2);
            obj.moveSpeed = arg0;
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index moveSpeed on a nil value" : e.Message);
        }
    }

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_set_climbSpeed(IntPtr L)
	{
		object o = null;

		try
		{
			o = ToLua.ToObject(L, 1);
			Map3DWalker obj = (Map3DWalker)o;
			float arg0 = (float)LuaDLL.luaL_checknumber(L, 2);
			obj.climbSpeed = arg0;
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index climbSpeed on a nil value" : e.Message);
		}
	}

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_set_rotateSpeed(IntPtr L)
    {
        object o = null;

        try
        {
            o = ToLua.ToObject(L, 1);
            Map3DWalker obj = (Map3DWalker)o;
            float arg0 = (float)LuaDLL.luaL_checknumber(L, 2);
            obj.rotateSpeed = arg0;
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e, o == null ? "attempt to index rotateSpeed on a nil value" : e.Message);
        }
    }

}

