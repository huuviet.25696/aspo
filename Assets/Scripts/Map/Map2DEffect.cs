using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using AssetPipeline;

public class Map2DEffect
{
	static int[] triangles = new[] { 0, 1, 3, 1, 2, 3 };

    private CLASSTAG_GridMapEffectData effectData;
	public bool isLoaded = false;
	public GameObject effectGo;
	public GameObject effectShowGo;
	public GameObject effectRoot = null;
	public Mesh mesh;
	
    public Map2DEffect(GameObject root, CLASSTAG_GridMapEffectData data)
    {
        this.effectRoot = root;
		this.effectData = data;

		var go = new GameObject();
		go.name = data.name;
		Transform trans = go.transform;
		trans.parent = root.transform;
		trans.rotation = Quaternion.Euler(effectData.rotation);
		trans.localScale = effectData.scale;
		trans.localPosition = new Vector3(effectData.pos.x, effectData.pos.y, 0);
		this.effectGo = go;
		this.TryCreateMesh();
    }
	private void TryCreateMesh()
	{
		if (effectData.height != 0f && effectData.width != 0f)
		{
			mesh = new Mesh();
			Vector3[] vertices = new Vector3[4];
			for (int i = 0; i < vertices.Length; i++)
			{
				vertices[i] = Vector3.zero;
			}
			float tWidth = effectData.width / 2;
			float tHeight = effectData.height / 2;
			vertices[0].x = vertices[0].x - tWidth;
			vertices[0].y = vertices[0].y + tHeight;

			vertices[1].x = vertices[1].x + tWidth;
			vertices[1].y = vertices[1].y + tHeight;

			vertices[2].x = vertices[2].x + tWidth;
			vertices[2].y = vertices[2].y - tHeight;

			vertices[3].x = vertices[3].x - tWidth;
			vertices[3].y = vertices[3].y - tHeight;


			Matrix4x4 matrixRotate = Matrix4x4.TRS(Vector3.zero, Quaternion.Euler(0f, 0f, effectData.rotate), Vector3.one);

			for (int i = 0; i < vertices.Length; i++)
			{
				vertices[i] = matrixRotate.MultiplyPoint3x4(vertices[i]);
				vertices[i] = vertices[i] + new Vector3(effectData.offset.x, effectData.offset.y, 0f);
			}
			mesh.vertices = vertices;
			mesh.triangles = triangles;
			mesh.RecalculateBounds();
			MeshFilter meshFilter = effectGo.GetMissingComponent<MeshFilter>();
			meshFilter.mesh = mesh;

			MeshRenderer meshRenderer = effectGo.GetMissingComponent<MeshRenderer>();
			meshRenderer.materials = new Material[0];

			var eventHandler = effectGo.GetMissingComponent<ModelEventHandler>();
			eventHandler.visibleAction = () =>
			{
				this.SetShow(true);
			};
			eventHandler.invisibleAction = () =>
			{
				this.SetShow(false);
			};
		}
		else
		{
			this.SetShow(true);
		}
	}
    private void OnLoadEffect(object asset, LoadErrorCode error)
    {
		if (this.effectRoot == null) //父节点已经销毁
		{
			return;
		}
        if (asset != null)
        {
			this.effectShowGo = GameObject.Instantiate<GameObject>((GameObject)asset);
			this.effectGo.name = this.effectShowGo.name;
			this.effectShowGo.name = "show";

			Transform trans = this.effectShowGo.transform;
			trans.parent = this.effectRoot.transform;
			trans.rotation = Quaternion.Euler(effectData.rotation);
			trans.localScale = effectData.scale;
			trans.localPosition = new Vector3(effectData.pos.x, effectData.pos.y, 0);
			trans.SetParent(this.effectGo.transform, true);
			ResourceManager.AddAssetBundleRef(this.effectData.name);
        }
        else
        {
#if UNITY_EDITOR
            Debug.LogError("OnLoadEffect Error! " + effectData.name);
#endif
        }
    }

	public void SetShow(bool isShow)
	{
		if (effectShowGo != null)
		{
			effectShowGo.SetActive(isShow);
		}
		else if (isShow == true && !isLoaded)
		{
			isLoaded = true;
			ResourceManager.LoadAsync(effectData.name, OnLoadEffect);
		}
	}
    public void Release()
    {
		GameObject.Destroy(effectGo);
		this.effectGo = null;
		this.effectRoot = null;
		if (this.effectShowGo != null)
		{
			ResourceManager.DelAssetBundleRef(this.effectData.name);
			this.effectShowGo = null;
		}
    }
}
