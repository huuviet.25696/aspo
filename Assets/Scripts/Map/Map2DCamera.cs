using System.Collections;
using LuaInterface;
using UnityEngine;


public class Map2DCamera : MonoBehaviour
{
    public static Map2DCamera Instance
    {
        get;
        protected set;
    }


    public Transform target;
    public bool followTarget = false;

    public bool isSmooth = true;
    public float damping = 30f;

    public float xMin;
    public float xMax = 999f;
    public float yMin;
    public float yMax = 999f;
	public Vector3 offset = Vector3.zero;

    private Transform mTrans;

    private LuaFunction luaOnLateUpdateCallback;

    public Transform cacheTrans
    {
        get { return mTrans; }
    }

    public Map2D curMap;
    private Camera mycamera;
    public Vector3 lastCachePos;

    void Awake()
    {
        Instance = this;
        mTrans = this.transform;
        lastCachePos = mTrans.localPosition;
        mycamera = GetComponent<Camera>();
    }

    public void Follow(Transform t)
    {
        target = t;
        followTarget = true;
        SyncTargetPos();
    }

    public void SyncTargetPos()

    {
        if (target != null && mTrans != null)
        {
            lastCachePos = mTrans.localPosition;

            var end = target.position + offset;
            end.x = Mathf.Clamp(end.x, xMin, xMax);
			end.y = Mathf.Clamp(end.y, yMin, yMax);
            end.z = mTrans.position.z;
            mTrans.position = end;

            if (curMap != null && !curMap.isReleased)
            {
                curMap.CallLateUpdate(end, true);
            }
        }
    }

    //public void UpdateViewRange(float xMin, float xMax, float yMin, float yMax)
    //{
    //    this.xMin = xMin;
    //    this.xMax = xMax;
    //    this.yMin = yMin;
    //    this.yMax = yMax;
    //    SyncTargetPos();
    //}

    private float passTime = 0f;
    private bool isMoving = false;
    public float speedUpTime = 0.1f;
    public bool firstUpdate = false;

    public void CallLateUpdate()
    {
        if (target == null || !followTarget)
            return;
        
        float followRate = 1f;
        if (!isMoving)
        {
            isMoving = true;
        }
        else
        {
            passTime += Time.deltaTime;
            followRate = Mathf.Min(passTime / speedUpTime, 1f);
            if(Vector2.SqrMagnitude(mTrans.localPosition - target.position) < 0.01)
            {
                isMoving = false;
                followRate = 0;
                passTime = 0;
            }
        }

        if (!isMoving && !firstUpdate)
        {
            return;
        }
        //if (firstUpdate) firstUpdate = false;

        lastCachePos = mTrans.localPosition;
		Vector2 start = new Vector2(mTrans.position.x, mTrans.position.y);
        Vector2 end = new Vector2(target.position.x + offset.x, target.position.y + offset.y);
		Vector2 movePos = isSmooth ? Vector2.MoveTowards(start, end, Time.deltaTime * damping * followRate) : end;
		movePos.x = Mathf.Clamp(movePos.x, xMin, xMax);
		movePos.y = Mathf.Clamp(movePos.y, yMin, yMax);
		float dis = Vector2.Distance(start, movePos);
		if (dis > 0.001f || firstUpdate)
		{
			var pos = mTrans.position;
			pos.x = movePos.x;
			pos.y = movePos.y;
			mTrans.position = pos;
			if (curMap != null && !curMap.isReleased)
			{
				curMap.CallLateUpdate(mTrans.position, true);
			}
            if (luaOnLateUpdateCallback != null)
            {
                luaOnLateUpdateCallback.BeginPCall();
                luaOnLateUpdateCallback.Push(target);
                luaOnLateUpdateCallback.Push(mTrans);
                luaOnLateUpdateCallback.PCall();
                luaOnLateUpdateCallback.EndPCall();
            }
        }
    }

    public void SetCurMap(Map2D map2D)
    {
        curMap = map2D;

        //if (GameObject.Find("GameRoot/UICamera") != null)
        //{
        //    Camera mcamera = GameObject.Find("GameRoot/UICamera").GetComponent<Camera>();

        //    Transform target = mcamera.transform;
        //    map2D.transform.parent = target;
        //}
        //Vector3 p = map2D.transform.position;
        //p.z = 99.99f;
        //map2D.transform.position = p;

        UpdateCameraSize();
    }

	public void UpdateCameraSize()
	{
		float halfHeight = mycamera.orthographicSize;
		float halfWidth = mycamera.orthographicSize * mycamera.aspect;
		this.xMin = halfWidth;
		this.xMax = curMap.width - halfWidth;
		this.yMin = halfHeight;
		this.yMax = curMap.height - halfHeight;
		Map2D.cameraHalfHeight = halfHeight;
		Map2D.cameraHalfWidth = halfWidth;
		SyncTargetPos();
	}

    public void Reset()
    {
        if (mTrans != null)
        {
            SyncTargetPos();
            /*
            Vector3 finlEndPoint = new Vector3(0, 0, mTrans.localPosition.z);
            finlEndPoint.x = Mathf.Clamp(finlEndPoint.x, xMin, xMax);
            finlEndPoint.y = Mathf.Clamp(finlEndPoint.y, yMin, yMax);
            mTrans.localPosition = finlEndPoint;
            */
        }

        target = null;
        followTarget = false;
    }

    public void SetOnLateUpdateCallback(LuaFunction func)
    {
        if (luaOnLateUpdateCallback != null)
        {
            luaOnLateUpdateCallback.Dispose();
        }
        luaOnLateUpdateCallback = func;
    }
}


