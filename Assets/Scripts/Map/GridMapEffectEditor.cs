using UnityEngine;
public class GridMapEffectEditor : MonoBehaviour
{
    public float width = 3f;
    public float height = 3f;
    public float rotate;
    public Vector2 offset;
}
