using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using AssetPipeline;

public class CLASSTAG_Map2DEffectManager
{
    public static readonly int FG_Z = 0;
    public static readonly int BG_Z = 90;
    public static readonly int TF_Z = 100;

    private int mapId;
    private GameObject mapRootGo;
    private GameObject mapEffectGo;
    private GameObject bgEffectGo;
    private GameObject fgEffectGo;
    private GameObject tfEffectGo;
    private GameObject mcEffectGo;

    private List<Map2DEffect> bgList;
    private List<Map2DEffect> fgList;
    private List<Map2DEffect> tfList;
    private List<Map2DEffect> mcList;

    public CLASSTAG_Map2DEffectManager(int mapId, GameObject mapRootGo)
    {
        this.mapId = mapId;
        this.mapRootGo = mapRootGo;
        InitMapEffectRoot();
        bgList = new List<Map2DEffect>();
        fgList = new List<Map2DEffect>();
        tfList = new List<Map2DEffect>();
        mcList = new List<Map2DEffect>();
    }

    private void InitMapEffectRoot()
    {
        mapEffectGo = new GameObject("MapEffect" + mapId);
        bgEffectGo = new GameObject("bg");
        fgEffectGo = new GameObject("fg");
        tfEffectGo = new GameObject("tf");
        bgEffectGo.transform.parent = mapEffectGo.transform;
        bgEffectGo.transform.position = new Vector3(0, 0, BG_Z);
        bgEffectGo.transform.localScale = Vector3.one;
        fgEffectGo.transform.parent = mapEffectGo.transform;
        fgEffectGo.transform.position = new Vector3(0, 0, FG_Z);
        fgEffectGo.transform.localScale = Vector3.one;
        tfEffectGo.transform.parent = mapEffectGo.transform;
        tfEffectGo.transform.position = new Vector3(0, 0, TF_Z);
        tfEffectGo.transform.localScale = Vector3.one;
        mcEffectGo = GameObject.Find("MainCamera");
    }

    public void LoadBgEffect(List<CLASSTAG_GridMapEffectData> list)
    {
        for(int i = 0; i < list.Count; i++)
        {
            bgList.Add(new Map2DEffect(bgEffectGo, list[i]));
        }
    }

    public void LoadFgEffect(List<CLASSTAG_GridMapEffectData> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            fgList.Add(new Map2DEffect(fgEffectGo, list[i]));
        }
    }

    public void LoadTfEffect(List<CLASSTAG_GridMapEffectData> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            tfList.Add(new Map2DEffect(tfEffectGo, list[i]));
        }
    }

    public void LoadMcEffect(List<CLASSTAG_GridMapEffectData> list)
    {
        for (int i = 0; i < list.Count; i++)
        {
            tfList.Add(new Map2DEffect(mcEffectGo, list[i]));
        }
    }

    public GameObject GetTransferEffectParent()
    {
        return this.tfEffectGo;
    }

    public void Release()
    {
        for (int i = 0; i < bgList.Count; i++)
        {
            bgList[i].Release();
        }
        for (int i = 0; i < fgList.Count; i++)
        {
            fgList[i].Release();
        }
        for (int i = 0; i < tfList.Count; i++)
        {
            tfList[i].Release();
        }
        for (int i = 0; i < mcList.Count; i++)
        {
            mcList[i].Release();
        }

        if (mapEffectGo != null)
        {
            GameObject.Destroy(mapEffectGo);
        }
        mapRootGo = null;
        bgEffectGo = null;
        fgEffectGo = null;
        mcEffectGo = null;
    }
}