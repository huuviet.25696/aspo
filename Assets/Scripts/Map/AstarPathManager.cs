using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CLASSTAG_AstarPathManager
{
    public static readonly int LayerGround = 0;
    public static readonly int LayerSky = 1;

    public static CLASSTAG_AstarPathManager Instance
    {
        get;
        private set;
    }

    public AstarPath astarPath
    {
        get;
        private set;
    }
    
    public static void FUNCTAG_CreateInstance()
    {
        if (Instance != null)
        {
            Debug.LogError("AstarPathManager.Instance already exist");
            return;
        }

        Instance = new CLASSTAG_AstarPathManager();
    }

    public CLASSTAG_AstarPathManager()
    {
        astarPath = GameObject.FindObjectOfType<AstarPath>();
        if (astarPath == null)
        {
            GameObject go = new GameObject("AstarPath");
            astarPath = go.AddComponent<AstarPath>();
        }
 
        astarPath.heuristic = Heuristic.Manhattan;
        astarPath.logPathResults = PathLog.None;
        astarPath.maxFrameTime = 10;
        astarPath.showGraphs = false;
        astarPath.showNavGraphs = false;
        astarPath.maxFrameTime = 10;
        astarPath.debugMode = GraphDebugMode.Tags;
    }

}
