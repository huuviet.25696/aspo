using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using AssetPipeline;
using Object = UnityEngine.Object;

#if UNITY_EDITOR
using UnityEditor;
#endif

public enum LoadErrorCode
{
    None,
    BundleLoadFail,
    AssetLoadFail,
    Cancel,
}

public delegate void OnLoadAssetCallback(object asset, LoadErrorCode error);

public class ResourceManager
{
    public static bool autoUnloadAtlas = true;

    public static void CallUpdate()
    {
        UnityEngine.Profiling.Profiler.BeginSample("UnloadOneUnusedAtlas");
        if (autoUnloadAtlas)
        {
            CLASSTAG_AssetBundleInfo.UnloadOneUnusedAtlas();
        }
        UnityEngine.Profiling.Profiler.EndSample();
    }

	public static bool IsExist(string path)
	{
		if (AssetManager.UseAssetBundle)
		{
			CLASSTAG_AssetNameInfo assetNameInfo = CLASSTAG_AssetNameInfo.ParseAssetPath(path);
			var resInfo = AssetManager.Instance.curResConfig.GetResInfo(assetNameInfo.bundleName);
			return (resInfo != null);
		}
		else
		{
#if UNITY_EDITOR
			path = Application.dataPath + "/GameRes/" + path;
			return  CLASSTAG_FileHelper.IsExist(path);
#endif
            return false;
		}
	}

    public static void UnloadAtlas(bool unloadall = false)
    {
        CLASSTAG_AssetBundleInfo.UnloadUnusedAtlas(unloadall);
    }

    public static void UnloadUnusedAssetBundle()
    {
        AssetManager.Instance.UnloadUnusedAssetBundle();
    }

    public static Object Load(string path)
    {
        Object obj = AssetManager.Instance.FUNCTAG_LoadAsset(path);
        return obj;
    }

    public static void LoadAsync(int eventid, string path)
    {
        AssetManager.Instance.LoadAssetAsync(path, asset =>
        {
            GlobalEventHanlder.luaGlobalCallback.BeginPCall();
            GlobalEventHanlder.luaGlobalCallback.Push(eventid);
            GlobalEventHanlder.luaGlobalCallback.Push(path);
            GlobalEventHanlder.luaGlobalCallback.Push(asset);
            GlobalEventHanlder.luaGlobalCallback.PCall();
            GlobalEventHanlder.luaGlobalCallback.EndPCall();
        }, 
        () =>
        {
            object o = null;
            GlobalEventHanlder.luaGlobalCallback.BeginPCall();
            GlobalEventHanlder.luaGlobalCallback.Push(eventid);
            GlobalEventHanlder.luaGlobalCallback.Push(path);
            GlobalEventHanlder.luaGlobalCallback.Push(o);
            GlobalEventHanlder.luaGlobalCallback.PCall();
            GlobalEventHanlder.luaGlobalCallback.EndPCall();
            GameDebug.LogError("LoadAssetAsync Error! " + path);
        });
    }


    public static void LoadAsync(string path, OnLoadAssetCallback callback)
    {
        AssetManager.Instance.LoadAssetAsync(path, asset =>
        {
            if (callback != null)
            {
                callback(asset, LoadErrorCode.None);
            }
        },
        () =>
        {
            if (callback != null)
            {
                callback(null, LoadErrorCode.AssetLoadFail);
            }
            GameDebug.LogError("LoadAssetAsync Error! " + path);
        });
    }

    public static void AddAssetBundleRef(string assetPath)
    {
        AssetManager.Instance.AddAssetBundleRef(assetPath);
    }

    public static void DelAssetBundleRef(string assetPath)
    {
        AssetManager.Instance.DelAssetBundleRef(assetPath);
    }

	public static void UnloadAssetBundle(string assetPath, bool unloadAll = false)
	{
		AssetManager.Instance.UnloadAssetBundle(assetPath, unloadAll);
	}
}

