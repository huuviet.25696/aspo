using System;
using LuaInterface;
using System.Collections;
using System.Collections.Generic;

public class CLASSTAG_ResourceManagerWrap
{
	public static void Register(LuaState L)
	{
		L.BeginClass(typeof(ResourceManager), typeof(System.Object));
        L.RegFunction("GetPersistentDataPath",FUNCTAG_GetPersistentDataPath);
		L.RegFunction("IsExist",FUNCTAG_IsExist);
		L.RegFunction("Load",FUNCTAG_Load);
        L.RegFunction("LoadAsync",FUNCTAG_LoadAsync);
        L.RegFunction("UnloadUnusedAssetBundle",FUNCTAG_UnloadUnusedAssetBundle);
        L.RegFunction("UnloadAtlas",FUNCTAG_UnloadAtlas);
        L.RegFunction("UnloadAsset",FUNCTAG_UnloadAsset);
        L.RegFunction("UnloadUnusedAssets",FUNCTAG_UnloadUnusedAssets);
        L.RegFunction("AddAssetBundleRef",FUNCTAG_AddAssetBundleRef);
        L.RegFunction("DelAssetBundleRef",FUNCTAG_DelAssetBundleRef);
		L.RegFunction("UnloadAssetBundle",FUNCTAG_UnloadAssetBundle);
		L.RegFunction("ResourcesLoad",FUNCTAG_ResourcesLoad);
        L.RegFunction("LoadStreamingAssetsTexture",FUNCTAG_LoadStreamingAssetsTexture);
        L.RegVar("autoUnloadAtlas",FUNCTAG_get_autoUnloadAtlas, FUNCTAG_set_autoUnloadAtlas);
		L.EndClass();
	}

    
    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_GetPersistentDataPath(IntPtr L)
    {
        try
        {
            LuaDLL.lua_pushstring(L, CLASSTAG_GameResPath.FUNCTAG_persistentDataPath);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_IsExist(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			string arg0 = ToLua.CheckString(L, 1);
			bool b = ResourceManager.IsExist(arg0);
			ToLua.Push(L, b);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_Load(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 1);
            string arg0 = ToLua.CheckString(L, 1);
            object obj = ResourceManager.Load(arg0);
            ToLua.Push(L, obj);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_LoadAsync(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 2);
            int eventid = LuaDLL.lua_tointeger(L, 1);
            string arg0 = ToLua.CheckString(L, 2);
            ResourceManager.LoadAsync(eventid, arg0);
			return 0;
		}
		catch(Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_UnloadAtlas(IntPtr L)
    {
        try
        {
            bool unloadAll = LuaDLL.lua_toboolean(L, 1);
            ResourceManager.UnloadAtlas(unloadAll);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_UnloadUnusedAssetBundle(IntPtr L)
    {
        try
        {
            ResourceManager.UnloadUnusedAssetBundle();
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_UnloadUnusedAssets(IntPtr L)
    {
        try
        {
            UnityEngine.Resources.UnloadUnusedAssets();
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_UnloadAsset(IntPtr L)
    {
        try
        {
            UnityEngine.Object obj = ToLua.ToObject(L, 1) as UnityEngine.Object;
			if (obj != null)
			{ 
				UnityEngine.Resources.UnloadAsset(obj);
			}
            
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_AddAssetBundleRef(IntPtr L)
    {
        try
        {
            string arg0 = ToLua.CheckString(L, 1);
            ResourceManager.AddAssetBundleRef(arg0);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_DelAssetBundleRef(IntPtr L)
    {
        try
        {
            string arg0 = ToLua.CheckString(L, 1);
			ResourceManager.DelAssetBundleRef(arg0);
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

	[MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_UnloadAssetBundle(IntPtr L)
	{
		try
		{
			string arg0 = ToLua.CheckString(L, 1);
			bool arg1 = LuaDLL.lua_toboolean(L, 2);
			ResourceManager.UnloadAssetBundle(arg0, arg1);
			return 0;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
	static int FUNCTAG_ResourcesLoad(IntPtr L)
	{
		try
		{
			ToLua.CheckArgsCount(L, 1);
			string path = ToLua.CheckString(L, 1);
			var obj = UnityEngine.Resources.Load(path);
			ToLua.Push(L, obj);
			return 1;
		}
		catch (Exception e)
		{
			return LuaDLL.toluaL_exception(L, e);
		}
	}

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_LoadStreamingAssetsTexture(IntPtr L)
    {
        try
        {
            ToLua.CheckArgsCount(L, 1);
            string path = ToLua.CheckString(L, 1);
            var obj = AssetPipeline.AssetManager.LoadStreamingAssetsTexture(path);
            ToLua.Push(L, obj);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_get_autoUnloadAtlas(IntPtr L)
    {
        try
        {
            ToLua.Push(L, ResourceManager.autoUnloadAtlas);
            return 1;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

    [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
    static int FUNCTAG_set_autoUnloadAtlas(IntPtr L)
    {
        try
        {
            bool arg0 = LuaDLL.luaL_checkboolean(L, 2);
            ResourceManager.autoUnloadAtlas = arg0;
            return 0;
        }
        catch (Exception e)
        {
            return LuaDLL.toluaL_exception(L, e);
        }
    }

}

