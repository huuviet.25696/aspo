using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using LuaInterface;
using DG.Tweening;
using AssetPipeline;

public class LuaMain: MonoBehaviour 
{
    private LuaFunction luaStart;
    private LuaFunction luaUpdate;
    private LuaFunction luaLateUpdate;

    private bool initDone = false;

    public static LuaMain Instance
    {
        get;
        protected set;
    }

    public LuaState luaState
    {
        get;
        protected set;
    }

    //public void OnGUI()
    //{
    //    if (GUI.Button(new Rect(300, 0, 200, 100), "Test 1"))
    //    {
    //        GameLauncher.Test();
    //    }

    //}

    private void Awake()
    {
		Instance = this;
        //Crasheye.SetAppVersion(CLASSTAG_GameVersion.FullVersion);
        EasyTouchHandler.FUNCTAG_Init();
        DOTween.Init(false, true, LogBehaviour.ErrorsOnly);
		TcpParser.InitKeyMap();
        NetworkManager.CreateInstance();
        WWWRequest.CreateInstance();
        CLASSTAG_AstarPathManager.FUNCTAG_CreateInstance();
        AudioRecord.FUNCTAG_CreateInsatnce();
        HotkeyHandler.FUNCTAG_CreateInsatnce();
        //Auviis
        if (GameLauncher.Instance.enableVoiceChat) AuviisSDK.CreateInstance();
        //
        GameEventHandler.FUNCTAG_CreateInstance();
        //
        PlatformAPI.Init();
        //SPSDK.FUNCTAG_Setup();
        //SPSDK.UnityUpdateFinish();


        //must be call this first to enable arm64
#if !UNITY_EDITOR && UNITY_ANDROID

        using (AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                obj_Activity.Call("ValidatePackage");
            }
        }
#endif
        //启动Luastsate，绑定c和c#的库
        luaState = new LuaState();
        //
        luaState.OpenLibs(LuaDLL.luaopen_protobuf_c);
        //
        //luaState.LogGC = true;
        luaState.OpenLibs(LuaDLL.luaopen_bit);
        //luaState.OpenLibs(LuaDLL.luaopen_struct);
        //luaState.OpenLibs(LuaDLL.luaopen_lpeg);

        luaState.LuaGetField(LuaIndexes.LUA_REGISTRYINDEX, "_LOADED");
        luaState.OpenLibs(LuaDLL.luaopen_cjson);
        luaState.LuaSetField(-2, "cjson");
        //luaState.OpenLibs(LuaDLL.luaopen_cjson_safe);
        //luaState.LuaSetField(-2, "cjson.safe");

        luaState.LuaSetTop(0);
        LuaBinder.Bind(luaState);
        luaState.Start();

        object[] array = luaState.DoFile("main");
        LuaTable luaTable = array[0] as LuaTable;
        luaStart = luaTable["start"] as LuaFunction;
        luaUpdate = luaTable["update"] as LuaFunction;
        luaLateUpdate = luaTable["lateupdate"] as LuaFunction;

		CodeHandler.Run();
    }

    private void Start()
    {
        GameDebug.SetLogLevel(GameDebug.LEVEL_LOG);
        if (luaStart != null)
        {
            luaStart.Call();
            luaStart.Dispose();
            luaStart = null;
        }
        initDone = true;

        //GameLauncherViewController.ShowTips("");
    }

    private void Update()
    {
        if (!initDone)
            return;
		HotkeyHandler.Instance.FUNCTAG_CallUpdate(); ;
        NetworkManager.Instance.CallUpdate();
        GameDebug.CallUpdate();
        //QiniuCloud.CallUpdate();
        ResourceManager.CallUpdate();
        if (luaUpdate != null)
        {
            luaUpdate.BeginPCall();
            luaUpdate.Push(Time.deltaTime);
            luaUpdate.PCall();
            luaUpdate.EndPCall();
        }

        luaState.Collect();
    }

    private void LateUpdate()
    {
		if (!initDone)
			return;
		if (Map2DCamera.Instance != null)
		{
			Map2DCamera.Instance.CallLateUpdate();
		}
		Hud.CallUpdateAll();
		CLASSTAG_UpdateManager.CallLateUpdate();
		if (luaLateUpdate != null)
		{
			luaLateUpdate.BeginPCall();
            luaLateUpdate.Push(Time.deltaTime);
			luaLateUpdate.PCall();
			luaLateUpdate.EndPCall();
		}
    }

    private void OnApplicationPause(bool paused)
    {
        if (!paused)
        {
            //TalkingData.Setup();
        }
        else
        {
            //TalkingData.Dispose();
        }
    }

    private void OnDestroy()
    {
        luaState.Dispose();
        EasyTouchHandler.FUNCTAG_Release();
        PlatformAPI.Release();
       // AudioRecord.Release();
        HotkeyHandler.Instance.FUNCTAG_Release();
        //TalkingData.Dispose();
        GameDebug.Release();
    }

    GameObject go;

    //public void OnGUI()
    //{
    //    if (GUI.Button(new Rect(300, 0, 200, 100), "Test 1"))
    //    {
    //        GameObject prefab = ResourceManager.Load("UI/GameObject.prefab") as GameObject;
    //        go = GameObject.Instantiate<GameObject>(prefab);

    //        GameObject g1 = go.transform.FindChild("Sprite2").gameObject;
    //        g1.SetActive(true);
    //        UISprite sprite = g1.GetComponent<UISprite>();

    //        GameObject atlas = ResourceManager.Load("Atlas/RefAttrAtlas.prefab") as GameObject;
    //        UIAtlas uiatlas = atlas.GetComponent<UIAtlas>();

    //        sprite.atlas = uiatlas;

    //        //weakSet.Add(new WeakReference(g1));

    //        //sprite = g1.GetComponent<UISprite>();
    //        //weakSet.Add(new WeakReference(sprite));

    //        //Debug.Log(atlas.name);
    //        //g1.SetActive(true);
    //        //g1.SetActive(false);

    //        //go = new GameObject("aaaaaa");
    //        //Test t = go.AddComponent<Test>();
    //        //go.SetActive(false);
    //        //t.enabled = false;
    //    }

    //    else if (GUI.Button(new Rect(300, 100, 200, 100), "Test 2"))
    //    {
    //        GameObject.Destroy(go);

    //        //foreach (WeakReference one in weakSet)
    //        //{
    //        //    Debug.Log(one.IsAlive + "  " + one.Target == null + " "  + (one.Target == null || one.Target.Equals(null)));
    //        //}

    //    }
    //    else if (GUI.Button(new Rect(300, 200, 200, 100), "Test 3"))
    //    {
    //        AssetManager.Instance.UnloadAtlas();
    //        ResourceManager.UnloadUnusedAssetBundle();
    //        Resources.UnloadUnusedAssets();

    //    } 
    //}

}

