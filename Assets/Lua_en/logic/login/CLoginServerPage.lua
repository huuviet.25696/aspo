local CLoginServerPage = class("CLoginServerPage", CPageBase)
local CSetBox = class("CSetBox", CBox)
CLoginServerPage.CSetBox = CSetBox

function CSetBox.ctor(self, obj)
	CBox.ctor(self, obj)
	self.m_CheckSprite = self:NewUI(1, CSprite)   
	self.m_CnormalSprite = self:NewUI(2, CSprite)  
	
	self.m_Active = true


	
end
function CSetBox.SetOnOff(self,value)
	self.m_Active = value
	self.m_CheckSprite:SetActive(value)
	self.m_CnormalSprite:SetActive(not value)

end
function CSetBox.GetOnOff(self)
	return self.m_Active
	

end

function CLoginServerPage.ctor(self, obj)
	CPageBase.ctor(self, obj)
end

function CLoginServerPage.OnInitPage(self)
	self.m_ServerBtn = self:NewUI(1, CButton)
	self.m_ConnectBtn = self:NewUI(2, CButton)
	self.m_ChangeAccountBtn = self:NewUI(3, CButton)
	self.m_NoticeBtn = self:NewUI(4, CButton)
	self.m_SoundSetBox = self:NewUI(6, CSetBox)

	
	-- self.m_SoundSetBox.m_CheckSprite = self.m_SoundSetBox:NewUI(1,CSprite)
	-- self.m_SoundSetBox.m_CnormalSprite = self.m_SoundSetBox:NewUI(2,CSprite)
	self.m_ScanQRCodeBtn = self:NewUI(5, CButton)

	self.m_ServerBtn:AddUIEvent("click", callback(self, "SelectServerView"))
	self.m_ConnectBtn:AddUIEvent("click", callback(self, "ConnectServer"))
	self.m_ChangeAccountBtn:AddUIEvent("click", callback(self, "ChangeAccount"))
	self.m_ScanQRCodeBtn:AddUIEvent("click", callback(self, "ScanQRCode"))
	self.m_NoticeBtn:SetActive(true)
	self.m_ScanQRCodeBtn:SetActive(g_LoginCtrl:IsSdkLogin() and (not Utils.IsPC()) and Utils.IsDevUser())
	self.m_NoticeBtn:AddUIEvent("click", callback(self, "ShowNotice"))
	self.m_ChangeAccountBtn:SetActive(not g_LoginCtrl:IsSdkLogin())
	self.m_SoundSetBox:AddUIEvent("click",callback(self, "SetCheckSpriteFunc"))
	g_ServerCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnShowPage"))
end
function CLoginServerPage.OnMusicCheckSprite(self, oSprite)
	-- local isChecked = oSprite:GetSelected()
		--printDebug("CSetBox.SetCheckSpriteFunc", 11)
	--printc(string.format("系统设置音乐：isChecked = %s", tostring(isChecked)))
	-- g_SysSettingCtrl:SaveLocalSettings("music_enabled", isChecked)
	-- g_AudioCtrl:CheckAuidoAll()
end
function CLoginServerPage.SetCheckSpriteFunc(self)
	
	--printDebug("CSetBox.SetCheckSpriteFunc", self.m_SoundSetBox.m_CheckSprite)
	self.m_SoundSetBox:SetOnOff( not self.m_SoundSetBox:GetOnOff())
	g_SysSettingCtrl:SaveLocalSettings("music_enabled", self.m_SoundSetBox:GetOnOff())
	g_AudioCtrl:CheckAuidoAll()
	-- self:AddUIEvent("click", function ()
		
	-- 	local isEnabled = self.m_CnormalSprite:GetActive()
	-- 	self.m_CnormalSprite:SetActive( not isEnabled )
	-- 	--self.m_CheckSprite:SetActive(notisEnabled )
		
	-- end)
end
function CLoginServerPage.OnShowPage(self)
	local tAccount = g_SysSettingCtrl:GetSysSettings()
	g_SysSettingCtrl:ReadLocalSettings()
	--printDebug("tAccount",tAccount)
	self.m_SoundSetBox:SetOnOff(tAccount["music_enabled"])
	local bShowServer = g_ServerCtrl:IsInit()
	if g_LoginCtrl:IsSdkLogin() then
		bShowServer = bShowServer and g_SdkCtrl:IsLogin()
	end
	if bShowServer then
		local dServer = IOTools.GetClientData("login_server")
		--printDebug("CLoginServerPage.OnShowPage",dServer)
		if dServer and dServer.server_id then
			local dNew = g_ServerCtrl:GetServerByID(dServer.server_id)
			if dNew then
				table.update(dServer, dNew)
			else
				dServer = g_ServerCtrl:GetNewestServer()
			end
		else
			dServer = g_ServerCtrl:GetNewestServer()
		end
		self:SetServer(dServer)
	end
	self.m_ServerBtn:SetActive(bShowServer)
	if g_LoginCtrl:IsForceShowNotice() then
		CLoginNoticeView:ShowView()
	end
end

function CLoginServerPage.ScanQRCode(self)
	CQRCodeScanView:ShowView()
end

function CLoginServerPage.ShowNotice(self)
	if g_LoginCtrl:GetNoticeMd5() then
		CLoginNoticeView:ShowView()
	else
		g_NotifyCtrl:FloatMsg("No notifications available temporarily")
	end
end

function CLoginServerPage.SetServer(self, dServer)
	self.m_Server = dServer
	if self.m_Server  then
		IOTools.SetClientData("login_server", self.m_Server)
		--printDebug("CLoginServerPage.SetServer",self.m_Server)
		self.m_ServerBtn:SetText(self.m_Server.name)
	end
end

function CLoginServerPage.SelectServerView(self)
	
	CSelectServerView:ShowView(
				function (oView)
					oView:SetConfirmCb(callback(self, "Login"))
					oView:SetServer(self.m_Server)
				end
	)
	
end

function CLoginServerPage.Login(self, dServer)
	self:SetServer(dServer)
	self:ConnectServer()
end

function CLoginServerPage.IsCanConnect(self)
	if g_LoginCtrl:IsSdkLogin() then
		if not g_SdkCtrl:IsInit() then
			-- print("Restore failed, please try again")
			g_SdkCtrl:Init()
			return false
		end
		if not g_SdkCtrl:IsLogin() then
			-- print("Login failed, please try again")
			g_SdkCtrl:Login()
			return false
		end
	end
	if not self.m_Server then
		g_NotifyCtrl:FloatMsg("Please select a server")
		return false
	end
	if g_AttrCtrl.pid ~= 0 then
		-- print("Loading maps…")
		return false
	end
	return true
end

function CLoginServerPage.ConnectServer(self)
	if self:IsCanConnect() then
		g_LoginCtrl:ShowLoginTips("Connecting to the server")
		g_LoginCtrl:ConnectServer(self.m_Server)
	end
end

function CLoginServerPage.ChangeAccount(self)
	g_LoginCtrl:InitValue()
	ASPO.DuoSDK.Instance:showLogin()
end

return CLoginServerPage