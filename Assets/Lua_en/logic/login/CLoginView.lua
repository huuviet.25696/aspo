CLoginView = class("CLoginView", CViewBase)

function CLoginView.ctor(self, cb)
	CViewBase.ctor(self, "UI/Login/LoginView.prefab", cb)
	self.m_DepthType = "Login"
end

function CLoginView.OnCreateView(self)
	self.m_Container = self:NewUI(1, CWidget)
	self.m_AccountPage = self:NewPage(2, CLoginAccountPage)
	self.m_ServerPage = self:NewPage(3, CLoginServerPage)
	self.m_TipsPage = self:NewPage(4, CLoginTipsPage)
	self.m_VersionLabel = self:NewUI(5, CLabel)
	self.m_InvitationCodePage = self:NewPage(6, CInvitationCodePage)
	self.m_Bg = self:NewUI(7, CWidget)
	self.m_MsgLabel = self:NewUI(8, CLabel)
	-- self.m_LoginEffect = self:NewUI(9, CObject)
	self.m_SdkPage = self:NewPage(10, CSdkPage)
	self.m_QRCodePage = self:NewPage(11, CQRCodePage)
	self.m_FixedLabel = self:NewUI(12, CLabel)
	self.m_Logo = self:NewUI(13, CWidget)
	self.m_TipLabel = self:NewUI(14, CLabel)
	self.m_LastPag = nil
	self:InitContent()
end

function CLoginView.IsOtherBgRes(self)
	if Utils.IsEditor() then
		return false
	end
	if main.g_DllVer <= 3 then
		return false
	end
	local sType = Utils.GetGameType()
	if sType == "ylq" then
		return false 
	end

	return true
end

function CLoginView.SetEffectShow(self, b)
	-- if self:IsNeedShowEffect() then
	-- 	self.m_LoginEffect:SetActive(b)
	-- end
end

function CLoginView.IsNeedShowEffect(self)
	local sType = Utils.GetGameType()
	if sType == "ylq" or sType == "ylwy" then
		return true
	end
	return false
end

function CLoginView.InitContent(self)
	self.m_FixedLabel:SetActive(false)
	self.m_FixedLabel:AddUIEvent("click", callback(self, "OnFixed"))
	g_AudioCtrl:PlayMusic("bgm_login.ogg")
	Utils.AddTimer(function() C_api.Utils.HideGameLoading() end, 0, 0)
	UITools.ResizeToRootSize(self.m_Container)
	UITools.FitToRootScale(self.m_Bg, 1334, 750)
	local framever, gamever, resver = C_api.Utils.GetAppVersion()
	local framever2, gamever2, resver2, svnver2 = C_api.Utils.GetPackageResVersion()
	local framever3, gamever3, resver3, svnver3 = C_api.Utils.GetResVersion()
	local s = string.format("App:%s.%s.%s Base:%s.%s.%s.%s Res:%s.%s.%s.%s P:%s", framever, gamever, resver, framever2, gamever2, resver2, svnver2, framever3, gamever3, resver3, svnver3, main.g_ProtoVer)
	self.m_VersionLabel:SetText(s)
	self.m_MsgLabel:SetText("")
	
	-- self.m_Bg.m_UIWidget.mainTexture = C_api.ResourceManager.LoadStreamingAssetsTexture("Textures/loginBG")
	-- -- if self:IsOtherBgRes() then
	-- self.m_Logo.m_UIWidget.mainTexture = C_api.ResourceManager.LoadStreamingAssetsTexture("Textures/logo")
	-- end
	local framever, dllver, resver = C_api.Utils.GetResVersion()
	if resver >= 76 then
		self.m_Logo:MakePixelPerfect()
	end
	-- self.m_LoginEffect:SetActive(self:IsNeedShowEffect())
	local dGameSettingData = g_ApplicationCtrl:GetGameSettingData()
	if dGameSettingData.loginTipText then
		-- self.m_TipLabel:SetActive(true)
		self.m_TipLabel:SetActive(false)
		self.m_TipLabel:SetText(dGameSettingData.loginTipText)
	else
		self.m_TipLabel:SetActive(false)
	end

	self:CheckShowPage()
	g_ViewCtrl:HideBottomView()
	
end

function CLoginView.OnFixed(self)
	local args ={
		title = "Sửa client",
		msg = "Update bản vá thất bại, không thể vào game, hãy thử lại",
		okCallback = function() C_api.Utils.RestoreGameRes(true) end,
		okStr = "Sửa chữa",
		cancelCallback = function()  end,
		forceConfirm = true,
	}
	g_WindowTipCtrl:SetWindowConfirm(args)
	
end

function CLoginView.CheckShowPage(self)
	-- self:ShowServerPage()
	if Utils.IsEditor()  or main.bypassSDK  then
		self:ShowAccountPage()
	else
		self:ShowServerPage()
	end
end

function CLoginView.SetLoginMsg(self, sText)
	self.m_MsgLabel:SetText(sText)
end

function CLoginView.ShowAccountPage(self)
	-- printDebug("============> show login page")
	if g_LoginCtrl:IsSdkLogin() then
		return
	end
	--comment show login view
	self:ShowSubPage(self.m_AccountPage)
	if Utils.IsEditor()  then
		self:ShowSubPage(self.m_AccountPage)
	end
	
	self:SetEffectShow(true)
end
function CLoginView.GetLoginVerify(self)
	local loginVerify = IOTools.GetClientData("login_verify")
	-- printDebug("Login:",loginVerify)
	if loginVerify and loginVerify[1] then
		return loginVerify
	end
	return {}
end
function CLoginView.ShowServerPage(self)
	local one = self:GetLoginVerify()
	-- printDebug("ShowServerPage",one)

	if (one ~= nil and #one > 0 ) then
		g_ServerCtrl:GetServerList(one[1].account)
	else
		local function checkServerList()
			local loginVerifies = self:GetLoginVerify()
			if (loginVerifies ~= nil and #loginVerifies > 0 ) then
				g_ServerCtrl:GetServerList(loginVerifies[1].account)
				--printDebug("Account is ok, exit loop")
				return false
			else
				--printDebug("Account does not exist, continue loop")
				return true
			end
		end
		Utils.AddTimer(checkServerList, 1, 1)
	end	
	self:ShowSubPage(self.m_ServerPage)
	
	self:SetEffectShow(true)
end

function CLoginView.ShowSdkPage(self)
	self:ShowSubPage(self.m_SdkPage)
	self:SetEffectShow(true)
end

function CLoginView.ShowQRCodePage(self)
	self:ShowSubPage(self.m_QRCodePage)
	self:SetEffectShow(true)
end

function CLoginView.ShowTipsPage(self, sTips)
	if self.m_CurPage ~= self.m_TipsPage then
		self.m_LastPage = self.m_CurPage
	end
	self:ShowSubPage(self.m_TipsPage)
	self.m_TipsPage:SetTips(sTips)
	self:SetEffectShow(true)
end


function CLoginView.HideTipsPage(self)
	if self.m_LastPage then
		self:ShowSubPage(self.m_LastPage)
	else
		self:HideAllPage()
	end
	self:SetEffectShow(true)
end

function CLoginView.ShowInvitationCodePage(self)
	self:ShowSubPage(self.m_InvitationCodePage)
	self:SetEffectShow(false)
end

function CLoginView.CloseView(self)
	CViewBase.CloseView(self)
	g_ViewCtrl:ShowBottomView()
end

function CLoginView.showBntChange (self)
	-- printDebug("ShowServerPage ---------->")
	-- if(self.m_ServerPage ~=nil) then self.m_ServerPage:ShowChangeAccount() end
end

return CLoginView