local CWarriorLockHud = class("CWarriorLockHud", CAsyncHud)

function CWarriorLockHud.ctor(self, cb)
	CAsyncHud.ctor(self, "UI/Hud/FightLockHud.prefab", cb, true)
end

function CWarriorLockHud.OnCreateHud(self)
	self.m_Label = self:NewUI(2, CLabel)
end

function CWarriorLockHud.SetLevel(self, iLevel)
	iLevel = iLevel or 0
	--printDebug("CWarriorLockHud.SetLevel",iLevel)
	self.m_Label:SetText(string.format("After level %d will unlock", iLevel))
end

return CWarriorLockHud