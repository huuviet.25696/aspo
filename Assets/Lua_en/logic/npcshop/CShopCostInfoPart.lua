local CShopCostInfoPart = class("CShopCostInfoPart", CBox)

function CShopCostInfoPart.ctor(self, cb)
	CBox.ctor(self, cb)
	self.m_MinNum = 0
	self.m_MaxNum = 1000
	self.m_DefaultBuyNum = 1
	self.m_UnitPrice = 0
	self.m_SumPrice = 0
	self.m_OwnCurrency = 0
	self.m_BuyNum = 1
	self.m_CurrentLimit = self.m_MaxNum
	self.m_SelectedItem = nil
	self.m_Currency = nil
	self.m_TextColor = Color.New(0.54, 0.38, 0.13, 1)
	self:InitContent()
end

function CShopCostInfoPart.InitContent(self)
	self.m_DecCountBtn = self:NewUI(1, CAddorDecButton)
	self.m_AddCountBtn = self:NewUI(2, CAddorDecButton)
	self.m_CountBtn = self:NewUI(3, CButton)
	self.m_NumberLabel = self:NewUI(4, CLabel)
	self.m_SumLabel = self:NewUI(5, CLabel)
	self.m_OwnLabel = self:NewUI(6, CLabel)
	self.m_BuyBtn = self:NewUI(7, CButton)
	self.m_SumCurrencySprite = self:NewUI(8, CSprite)
	self.m_OwnCurrencySprite = self:NewUI(9, CSprite)
	self.m_ItemInfoLabel = self:NewUI(10, CLabel)
	self.m_ItemInfoScrollView = self:NewUI(11, CScrollView)
	self.m_ItemNameLabel = self:NewUI(12, CLabel)
	self.m_ItemBox = self:NewUI(13, CItemTipsBox)
	self.m_CloseBtn = self:NewUI(14, CBox)
	self.m_CloseMask = self:NewUI(15, CBox)
	self.m_ItemSlot = self:NewUI(16, CBox)
	self.m_AmountLabel = self:NewUI(17, CLabel)
	self.m_MaxBtn = self:NewUI(18, CButton)

	self.m_ItemInfoBox = self.m_ItemBox:Clone()
	self.m_ItemInfoBox:SetShowTips(false)
	self.m_ItemInfoBox:SetParent(self.m_ItemSlot.m_Transform)
	self.m_ItemInfoBox:SetLocalPos(Vector3.zero)
	self.m_ItemInfoBox:SetActive(true)
	self.m_CloseMask:AddUIEvent("click", callback(self, "OnClickClose"))
	self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClickClose"))
	self.m_AddCountBtn:SetData({Label = self.m_NumberLabel, LimitNum = self.m_MaxNum, ChangeTable = {{0, 1}}, Callback = callback(self, "OnChangeCount")})
	self.m_DecCountBtn:SetData({Label = self.m_NumberLabel, LimitNum = self.m_MinNum, ChangeTable = {{0, -1}}, Callback = callback(self, "OnChangeCount")})
	self.m_CountBtn:AddUIEvent("click", callback(self, "OnShowKeyboard"))
	self.m_BuyBtn:AddUIEvent("click", callback(self, "OnBuy"))
	self.m_MaxBtn:AddUIEvent("click", callback(self, "OnClickMax"))

	g_NetCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlBtnBuyEvent"))
	
	self:SetActive(false)
end

function CShopCostInfoPart.OnCtrlBtnBuyEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Item.Event.RefreshBuy then
		self:RefreshBtnBuy()
	end
end

function CShopCostInfoPart.OnClickMax(self)
	if self.m_SelectedItem == nil then
		g_NotifyCtrl:FloatMsg("Please select the item!")
		return
	end
	local iCount = math.modf(self.m_OwnCurrency / self.m_UnitPrice)
	if self.m_SelectedItem.m_GoodsData.currency.currency_type == define.Currency.Type.ColorCoin then
		iCount = math.modf(self.m_OwnCurrency*10000 / self.m_UnitPrice)
	end
	-- table.print(iCount)
	if iCount <= 0 then
		iCount = 1
	end
	if iCount > self.m_CurrentLimit then
		iCount = self.m_CurrentLimit
	end
	self:OnChangeCount(iCount)
end

function CShopCostInfoPart.OnChangeCount(self, value)
	if self.m_SelectedItem == nil then
		g_NotifyCtrl:FloatMsg("Please select the item!")
		return
	end
	self.m_NumberLabel:SetText(value)
	self.m_SumPrice = value * self.m_UnitPrice
	if self.m_SelectedItem.m_GoodsData.currency.currency_type == define.Currency.Type.ColorCoin then
		self.m_SumPrice = self.m_SumPrice/10000
	elseif self.m_SelectedItem.m_GoodsData.currency.currency_type == define.Currency.Type.NEMO then
		self.m_SumPrice = self.m_SumPrice/10000
	end
	if self.m_SelectedItem.m_GoodsData.currency.currency_type == define.Currency.Type.RMB then
		self.m_SumLabel:SetText(self.m_SumPrice/100 .. "$")
	else
		if self.m_SumPrice > self.m_OwnCurrency then
			self.m_SumLabel:SetColor(Color.red)
		else
			self.m_SumLabel:SetColor(Color.white)
		end
		self.m_SumLabel:SetNumberString(self.m_SumPrice)
	end
end

function CShopCostInfoPart.OnShowKeyboard(self)
	if self.m_SelectedItem == nil then
		g_NotifyCtrl:FloatMsg("Please select the item!")
		return
	end
	local function syncCallback(self, count)
		self:OnChangeCount(count)
	end
	g_WindowTipCtrl:SetWindowNumberKeyBorad(
		{num = self.m_BuyNum, min = self.m_MinNum, max = self.m_CurrentLimit, syncfunc = syncCallback, obj = self},
		{widget = self.m_CountBtn, side = enum.UIAnchor.Side.Top, offset = Vector2.New(0,0), extendClose = true}
	)
end

function CShopCostInfoPart.SetInfo(self, oItemCell)
	self:SetActive(true)
	local sInfo = ""
	if oItemCell.m_Amount ~= nil then
		sInfo = string.format("Quantity:%s", oItemCell.m_Amount)
		if oItemCell.m_Amount < self.m_MaxNum then
		-- self.m_AmountLabel:SetText(string.format("数量：%s", oItemCell.m_Amount))
			self.m_CurrentLimit = oItemCell.m_Amount
			self.m_AddCountBtn:SetLimitNum(oItemCell.m_Amount)
		end
		if oItemCell.m_GoodsData.cycle_type == "day" then
			sInfo = string.format("%s\nNote:%s", sInfo, "Today's limit")
			elseif oItemCell.m_GoodsData.cycle_type == "week" then
			sInfo = string.format("%s\nNote:%s", sInfo, "This week limit")
			elseif oItemCell.m_GoodsData.cycle_type == "month" then
			sInfo = string.format("%s\nNote:%s", sInfo, "This month limit")
		else
			sInfo = string.format("%s\nNote:%s", sInfo, "This time limit")
		end
	else
		-- self.m_AmountLabel:SetText("")
		self.m_CurrentLimit = self.m_MaxNum
		self.m_AddCountBtn:SetLimitNum(self.m_MaxNum)
	end
	
	self.m_AmountLabel:SetText(sInfo)

	self.m_UnitPrice = g_NpcShopCtrl:GetGoodsPrice(oItemCell.m_GoodsInfo.pos)
	self.m_SelectedItem = oItemCell
	self.m_OwnCurrencySprite:SetSpriteName(oItemCell.m_GoodsData.currency.icon)
if oItemCell.m_GoodsData.currency.currency_type == define.Currency.Type.NEMO then
	self.m_SumCurrencySprite:SetSpriteName(1001)
else
	self.m_SumCurrencySprite:SetSpriteName(oItemCell.m_GoodsData.currency.icon)
end
	
	self.m_OwnCurrency = g_NpcShopCtrl:GetCurrencyValue(oItemCell.m_GoodsData.currency.currency_type)
	self.m_OwnLabel:SetNumberString(self.m_OwnCurrency)
	self.m_ItemInfoLabel:SetText(oItemCell.m_GoodsData.description)
	self.m_ItemInfoScrollView:ResetPosition()
	self.m_ItemNameLabel:SetText(oItemCell.m_GoodsData.name)
	if oItemCell.m_GoodsData.gType == define.Store.GoodsType.Partner then
		self.m_ItemInfoBox:SetItemData(oItemCell.m_GoodsData.item_id, nil, oItemCell.m_GoodsData.exData.partner_type, {isLocal = true, uiType = 2})
	else
		self.m_ItemInfoBox:SetItemData(oItemCell.m_GoodsData.item_id, nil, nil, {isLocal = true, uiType = 2})
	end

	local iCount = math.modf(self.m_OwnCurrency / self.m_UnitPrice)
	if self.m_SelectedItem.m_GoodsData.currency.currency_type == define.Currency.Type.ColorCoin then
		iCount = math.modf(self.m_OwnCurrency*10000 / self.m_UnitPrice)
	end
	if iCount <= 0 then
		iCount = 1
	end
	if self.m_CurrentLimit > iCount then
		self.m_CurrentLimit = iCount
		self.m_AddCountBtn:SetLimitNum(iCount)
	end
	self:OnChangeCount(self.m_DefaultBuyNum)
end
function CShopCostInfoPart.RefreshBtnBuy(self)
	self.m_BuyBtn:SetEnabled(true);
end

function CShopCostInfoPart.OnBuy(self)
	self.m_BuyBtn:SetEnabled(g_NetCtrl:IsValidSession(9999999999,5) == false);
	local num = tonumber(self.m_NumberLabel:GetText())
	if self.m_SelectedItem == nil then
		g_NotifyCtrl:FloatMsg("Please select the item!")
		return
	elseif self.m_SelectedItem.m_GoodsData.vip == 1 and not (g_WelfareCtrl:HasYueKa() or g_WelfareCtrl:HasZhongShengKa()) then
		g_NotifyCtrl:FloatMsg("Buy Monthly Card, Lifetime Card")
	elseif self.m_SelectedItem.m_GoodsData.grade_limit ~= nil 
			and (
					(self.m_SelectedItem.m_GoodsData.grade_limit.max ~= nil and self.m_SelectedItem.m_GoodsData.grade_limit.max < g_AttrCtrl.grade)
					or(self.m_SelectedItem.m_GoodsData.grade_limit.min ~= nil and self.m_SelectedItem.m_GoodsData.grade_limit.min > g_AttrCtrl.grade)
				) then
			g_NotifyCtrl:FloatMsg("Current level can not buy")
			return
	elseif self.m_SelectedItem.m_Amount == 0 then
		g_NotifyCtrl:FloatMsg("Not enough quantity")
	elseif self.m_SelectedItem.m_GoodsData.currency.currency_type == define.Currency.Type.RMB then
		-- if Utils.IsEditor() then
		-- 	g_NotifyCtrl:FloatMsg("Can not process on Editor mode")
		-- else
			g_NpcShopCtrl.m_Recharge = self.m_SelectedItem.m_GoodsData
			netother.C2GSRequestPay(self.m_SelectedItem.m_GoodsData.payid, self.m_SelectedItem.m_GoodsData.id)
		-- end
	elseif self.m_SelectedItem.m_GoodsData.currency.currency_type == define.Currency.Type.NEMO then	
		local args = 
			{
				msg = "Buy this package with NEMO?",
				okCallback = function ()
					-- if Utils.IsEditor() then
					-- 	g_NotifyCtrl:FloatMsg("Can not process on Editor mode")
					-- else
						g_NpcShopCtrl.m_Recharge = self.m_SelectedItem.m_GoodsData
						if Utils.IsDevUser() and Utils.IsEditor() then
							netother.C2GSGMCmd(string.format("huodong charge 1003 %s",self.m_SelectedItem.m_GoodsData.id))
							g_NotifyCtrl:FloatMsg("Directly call the GM commands, the operation is prone to high risk!!! For testing only")
						else
							netother.C2GSRequestPay(self.m_SelectedItem.m_GoodsData.payid, self.m_SelectedItem.m_GoodsData.id)	
							g_NotifyCtrl:FloatMsg("The request is under processing and will be updated when the transaction is finished.")
						end
						self:OnClickClose()
					-- end
				end,
				cancelCallback = function ()
					-- self:OnClickClose()
				end,
				okStr = "Yes",
				cancelStr = "No",
			}
			g_WindowTipCtrl:SetWindowConfirm(args)

	elseif self.m_SumPrice > self.m_OwnCurrency then
		if self.m_SelectedItem.m_GoodsData.currency.currency_type == define.Currency.Type.GoldCoin then
			g_SdkCtrl:ShowPayView()
		elseif self.m_SelectedItem.m_GoodsData.currency.currency_type == define.Currency.Type.ColorCoin then
			g_SdkCtrl:ShowPayView()
		elseif self.m_SelectedItem.m_GoodsData.currency.currency_type == define.Currency.Type.Gold then
			g_NpcShopCtrl:ShowGold2CoinView()
		end
		g_NotifyCtrl:FloatMsg(string.format("You are not enough %s", self.m_SelectedItem.m_GoodsData.currency.name))
	elseif num <= 0 then
		g_NotifyCtrl:FloatMsg("Minimum purchaseed quantity is 1")
	else
		self.m_BuyNum = num
		-- printDebug(self.m_SelectedItem.m_GoodsData)
		if g_NetCtrl:IsValidSession(netdefines.C2GS_BY_NAME["C2GSNpcStoreBuy"]) then
			netstore.C2GSNpcStoreBuy(self.m_SelectedItem.m_GoodsData.id, num, self.m_SumPrice, self.m_SelectedItem.m_GoodsInfo.pos)
		else
			g_NotifyCtrl:FloatMsg("Your hand speed has surpassed internet speed")
		end
		self:OnClickClose()
	end
end

function CShopCostInfoPart.OnClickClose(self)
	self:SetActive(false)
end

return CShopCostInfoPart