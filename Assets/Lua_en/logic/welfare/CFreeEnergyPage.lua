local CFreeEnergyPage = class("CFreeEnergyPage", CPageBase)

function CFreeEnergyPage.ctor(self, ob)
	CPageBase.ctor(self, ob)
end

function CFreeEnergyPage.OnInitPage(self)
	self.m_GetEnergySpr = self:NewUI(1, CSprite)
	self.m_FreeEnergyLabel = self:NewUI(2, CLabel)
	self.m_TextDecreptiont = self:NewUI(3, CLabel)

	self.m_TweenScale = self.m_GetEnergySpr:GetComponent(classtype.TweenScale)
	self.m_TweenScale.enabled = false
	self:InitContent()
end

function CFreeEnergyPage.InitContent(self)
	self.m_GetEnergySpr:AddUIEvent("click", callback(self, "OnGetEnergy"))
	self.m_FreeEnergyLabel:SetText(data.globaldata.GLOBAL.freeenergy_value.value)
	g_WelfareCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnWelfareEvnet"))
	self:build_day();
	self:Refresh()
end

function CFreeEnergyPage.build_day( self )

	local timem= string.split(data.globaldata.GLOBAL.time_energy_value.value, ";")
	local time_1= string.split(timem[1],',')
	local time_2= string.split(timem[2],',')
	--printDebug("timem -",time_1)
	-- local iCur = os.date("!*t",g_TimeCtrl:GetTimeS())
	-- --printDebug("CFreeEnergyPage.build_day",iCur)
	-- local zhongwu = {
	-- 	iopen = os.time({
	-- 		year=iCur.year, 
	-- 		month=iCur.month, 
	-- 		day=iCur.day, 
	-- 		hour=12, 
	-- 		min=0, 
	-- 		sec=0,
	-- 	}),
	-- 	iend = os.time({
	-- 		year=iCur.year, 
	-- 		month=iCur.month, 
	-- 		day=iCur.day, 
	-- 		hour=16, 
	-- 		min=30, 
	-- 		sec=0,
	-- 	}),
	-- } --12:00~17:30
	-- local wanshang = {
	-- 	iopen = os.time({
	-- 		year=iCur.year, 
	-- 		month=iCur.month, 
	-- 		day=iCur.day, 
	-- 		hour=17, 
	-- 		min=0, 
	-- 		sec=0,
	-- 	}),
	-- 	iend = os.time({
	-- 		year=iCur.year, 
	-- 		month=iCur.month, 
	-- 		day=iCur.day, 
	-- 		hour=22, 
	-- 		min=30, 
	-- 		sec=0,
	-- 	}),
	-- } --

	-- local start_1=os.date("!*t",Utils.convertUTCToLocalTime(zhongwu.iopen))
	-- local start_2=os.date("!*t",Utils.convertUTCToLocalTime(wanshang.iopen))
	-- local end_1=os.date("!*t",Utils.convertUTCToLocalTime(zhongwu.iend))
	-- local end_2=os.date("!*t",Utils.convertUTCToLocalTime(wanshang.iend))
	-- -- local start_1 = os.date("!*t", zhongwu.iopen)
	-- -- local end_1 = os.date("!*t", zhongwu.iend)
	-- -- --printDebug("time start",zhongwu.iopen)
	 local text = "Replenishment Time\n %02d:%02d - %02d:%02d\n%02d:%02d -  %02d:%02d"
	 self.m_TextDecreptiont:SetText(string.format(text,time_1[1],time_1[2],time_1[3],time_1[4],time_2[1],time_2[2],time_2[3],time_2[4]))
	
	-- body
end

function CFreeEnergyPage.OnWelfareEvnet(self, oCtrl)
	if oCtrl.m_EventID == define.Welfare.Event.OnFreeEnergyZhongwu or
		oCtrl.m_EventID == define.Welfare.Event.OnFreeEnergyWanshang or
		oCtrl.m_EventID == define.Welfare.Event.OnFreeEnergyClose then
		self:Refresh()
	end
end

function CFreeEnergyPage.OnGetEnergy(self, obj)
	local idx
	if g_WelfareCtrl:IsZhongWuCanGet() then
		idx = 1
	elseif g_WelfareCtrl:IsWanshangCanGet() then
		idx = 2
	end
	if idx then
		nethuodong.C2GSReceiveEnergy(idx)
	end
end

function CFreeEnergyPage.Refresh(self)
	local bCanGet = g_WelfareCtrl:IsZhongWuCanGet() or g_WelfareCtrl:IsWanshangCanGet()
	self.m_TweenScale.enabled = bCanGet
	self.m_GetEnergySpr:SetGrey(not bCanGet)
	if not bCanGet then
		self.m_GetEnergySpr:SetLocalScale(Vector3.one)
	end
end

return CFreeEnergyPage