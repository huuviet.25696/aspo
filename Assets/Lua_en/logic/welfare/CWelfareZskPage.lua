local CWelfareZskPage = class("CWelfareZskPage", CPageBase)

function CWelfareZskPage.ctor(self, obj)
	CPageBase.ctor(self, obj)
end

function CWelfareZskPage.OnInitPage(self)
	self.m_ZskBuyBtn = self:NewUI(1, CButton)
	self.m_IAPPrice = self:NewUI(2, CLabel)
	self.m_ZskNemoBuyBtn = self:NewUI(3, CButton)
	self.m_NemoPrice = self:NewUI(5, CLabel)

	self.m_BuyMark = self:NewUI(4, CLabel)
	self.m_ZskDescLabel = self:NewUI(6, CLabel)
	self.m_ZskDetailBtn = self:NewUI(8, CLabel)
	self.m_ZskItemGrid = self:NewUI(10, CGrid)
	self.m_ItemTipsBox = self:NewUI(11, CItemTipsBox)
    self.m_InstantlyLabel = self:NewUI(12, CLabel)
	self.DailyLabel = self:NewUI(13, CLabel)
	self.m_ZskGetBtn = self:NewUI(14, CButton)
	self.m_ZskGotBtn = self:NewUI(15, CSprite)
	self.m_YkDayLabel = self:NewUI(16, CLabel)
	self.m_ZskGetBtn:AddUIEvent("click", callback(self, "OnGetZsk"))
	self.m_ZskBuyBtn:AddUIEvent("click", callback(self, "OnBuyZskIAP"))
	self.m_ZskNemoBuyBtn:AddUIEvent("click", callback(self, "OnBuyZskNemo"))
	-- self.m_ZskDetailBtn:AddUIEvent("click", callback(self, "OnZskDetial"))
    
	g_WelfareCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlEvent"))
	self.m_ZskDescLabel:SetText(data.welfaredata.WelfareControl[define.Welfare.ID.Zsk].short_desc)
	self.m_InstantlyLabel:SetText(data.welfaredata.WelfareControl[define.Welfare.ID.Zsk].item_list[1].amount)
	self.DailyLabel:SetText(data.welfaredata.WelfareControl[define.Welfare.ID.Zsk].item_list_after[1].amount)
	self:RefreshZskState()

	self:RefreshGradeGiftPrice(define.Store.Page.LiBaoShop)
	self:RefreshGradeGiftPrice(define.Store.Page.LimitSkin)

	self:InitItemGrid()
end

function CWelfareZskPage.InitItemGrid(self)
	for i,v in ipairs(data.welfaredata.WelfareControl[define.Welfare.ID.Zsk].item_list) do
		local oItemTipsBox = self.m_ItemTipsBox:Clone()
		self.m_ZskItemGrid:AddChild(oItemTipsBox)
		oItemTipsBox:SetSid(v.sid, v.amount, {isLocal = true, uiType = 2})
		if i == 3 then
			oItemTipsBox.m_IconSprite:AddEffect("circle")
		end
		oItemTipsBox:SetActive(true)
	end
end

function CWelfareZskPage.OnCtrlEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Welfare.Event.OnYueKa then
		local dData = oCtrl.m_EventData
		local oInfo = nil
		if dData.key == "zsk" then
			oInfo = data.npcstoredata.RechargeStore[1010]
			self:RefreshZskState()
		end
		if oInfo and #oInfo.random_talk > 0 then
			-- CGuideView:ShowView(function (oView)
			-- 	oView:ShowShopTalk(oInfo)
			-- end)
			Utils.AddTimer(function ()
				CThanksView:ShowView()
			end, 0.03, 0.03)
		end
	end
end

function CWelfareZskPage.OnZskDetial(self)
	CHelpView:ShowView(function (oView)
		oView:ShowHelp("zhongshenka")
	end)
end
function CWelfareZskPage.RefreshZskState(self)
	--self.m_ZskGotBtn:SetActive(true)
	--self.m_ZskGetBtn:SetActive(false)
	if g_WelfareCtrl:HasZhongShengKa() then
		local bCanGet = (g_WelfareCtrl:GetYueKaData("zsk", "val") == 1)
		--printDebug("gggg",bCanGet)
        local iLeft = g_WelfareCtrl:GetYueKaData("zsk", "left_count")
		--local  t =g_TimeCtrl:GetLeftTime(iLeft)
		--printDebug("tttt",iLeft)
		self.m_YkDayLabel:SetText(string.format("Remains %d expire days", iLeft))
		--self.m_ZskBuyBtn:SetActive(false)
		--self.m_BuyMark:SetActive(true)
		self.m_ZskGetBtn:SetActive(bCanGet)
		self.m_ZskGotBtn:SetActive( not bCanGet)
	else
		--self.m_ZskBuyBtn:SetActive(true)
		--self.m_BuyMark:SetActive(false)
		self.m_ZskGotBtn:SetActive(true)
	end
end
function CWelfareZskPage.OnGetZsk(self)
	nethuodong.C2GSChargeCardReward("zsk")
end


function CWelfareZskPage.RefreshGradeGiftPrice(self,shopId)
	local goodsPosListFull = data.npcstoredata.GoodsDataSort[shopId] -- g_NpcShopCtrl:GetGoodsPosList(shopId)
	local goodsPosList ={}
	local goodYk = nil
	if shopId ~= nil and goodsPosListFull ~= nil then
		for k,v in ipairs(goodsPosListFull) do
			local storeData = data.npcstoredata.DATA[v]
			if (storeData.event_type == "zsk" ) then
				table.insert(goodsPosList,v)
				goodYk = storeData
			end
		end
	end
	if(goodYk ~=nil) then 
		if shopId == define.Store.Page.LimitSkin then
			-- netother.C2GSRequestPay(self.nemo_payid, self.nemo_id)
			self.nemo_id = goodYk.id
			self.nemo_payid = goodYk.payid
			-- if Utils.IsIOS() then
			-- 	self.nemo_payid = goodYk.iospayid
			-- end
			self.m_NemoPrice:SetText(goodYk.coin_count/10000)
		end
		if shopId == define.Store.Page.LiBaoShop then
			-- netother.C2GSRequestPay(self.iap_payid, self.iap_id)
			self.iap_id = goodYk.id
			self.iap_payid = goodYk.payid
			if Utils.IsIOS() then
				self.iap_payid = goodYk.iospayid
			end
			self.m_IAPPrice:SetText(goodYk.coin_count/100 .. "$")
		end
	end
end

function CWelfareZskPage.OnBuyZskIAP(self)
	printDebug(self.iap_payid,self.iap_id)
	if Utils.IsDevUser() and Utils.IsEditor() then
		netother.C2GSGMCmd(string.format("huodong charge 202"))
		g_NotifyCtrl:FloatMsg("Directly call the GM commands, the operation is prone to high risk!!! For testing only")
	else
		netother.C2GSRequestPay(self.iap_payid, self.iap_id)
	end
end

function CWelfareZskPage.OnBuyZskNemo(self)
	printDebug(self.nemo_payid,self.nemo_id)
	if Utils.IsDevUser() and Utils.IsEditor() then
		netother.C2GSGMCmd(string.format("huodong charge 202"))
		g_NotifyCtrl:FloatMsg("Directly call the GM commands, the operation is prone to high risk!!! For testing only")
	else
		local args = 
				{
					msg = "Buy this package with NEMO?",
					okCallback = function ()
							netother.C2GSRequestPay(self.nemo_payid, self.nemo_id)
							g_NotifyCtrl:FloatMsg("The request is under processing and will be updated when the transaction is finished.")
					end,
					cancelCallback = function ()
					end,
					okStr = "Yes",
					cancelStr = "No",
				}
		g_WindowTipCtrl:SetWindowConfirm(args)
		end
end

function CWelfareZskPage.OnBuyZsk(self)
	local key
	if g_LoginCtrl:IsSdkLogin() then
		if Utils.IsAndroid() then
			key = "com.kaopu.ylq.zsk"
		elseif Utils.IsIOS() then
			key = "com.kaopu.ylq.appstore.zsk"
		end
	end
	if key then
		g_SdkCtrl:Pay(key, 1)
	else
		if Utils.IsDevUser() and Utils.IsEditor() then
			netother.C2GSGMCmd(string.format("huodong charge 202"))
			g_NotifyCtrl:FloatMsg("Directly call the GM commands, the operation is prone to high risk!!! For testing only")
		else
			g_NotifyCtrl:FloatMsg("The current environment does not support purchasing")
		end
	end
end

return CWelfareZskPage