local CCzjjPage = class("CCzjjPage", CPageBase)

--成长基金
function CCzjjPage.ctor(self, obj)
	CPageBase.ctor(self, obj)
end

function CCzjjPage.OnInitPage(self)
	self.m_BuyNemoBtn = self:NewUI(1, CSprite)
	self.m_Grid = self:NewUI(2, CGrid)
	self.m_BoxClone = self:NewUI(3, CCzjjBox)
	self.m_NumYuanBox = self:NewUI(4, CSprNumberBox)
	self.m_NumBeiBox = self:NewUI(5, CSprNumberBox)
	self.m_BuyBtn  = self:NewUI(6, CSprite)
	self.m_NemoPrice = self:NewUI(7, CLabel)
	self.m_IAPPrice  = self:NewUI(8, CLabel)
	-- self.m_YkGetTipLabel = self:NewUI(4, CLabel)

	self.m_NumYuanBox:SetPrefix("pic_numyuan_")
	self.m_NumBeiBox:SetPrefix("pic_numbei_")
	self.m_BoxClone:SetActive(false)
	self.m_BuyBtn:AddUIEvent("click", callback(self, "OnBuyGradeGiftIAP"))
	self.m_BuyNemoBtn:AddUIEvent("click", callback(self, "OnBuyGradeGiftNemo"))

	g_WelfareCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlEvent"))
	self:RefreshBuyState()
	self:RefreshGrid()
	self:RefreshNumber()
	self:RefreshGradeGiftPrice(define.Store.Page.LiBaoShop)
	self:RefreshGradeGiftPrice(define.Store.Page.LimitSkin)
end

function CCzjjPage.RefreshNumber(self)
	self.m_NumYuanBox:SetNumber(98)
	self.m_NumBeiBox:SetNumber(7)
end

function CCzjjPage.OnCtrlEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Welfare.Event.OnCzjj then
		local dData = oCtrl.m_EventData
		if dData.key == "buy_flag" then
			self:RefreshBuyState()
			local oInfo = data.npcstoredata.RechargeStore[1011]
			if oInfo and #oInfo.random_talk > 0 then
				-- CGuideView:ShowView(function (oView)
				-- 	oView:ShowShopTalk(oInfo)
				-- end)
				Utils.AddTimer(function ()
					CThanksView:ShowView()
				end, 0.03, 0.03)
			end
		else
			self:RefreshGrid()
		end
	end
end

function CCzjjPage.RefreshBuyState(self)
	-- local bGrey = g_WelfareCtrl:IsBuyCzjj()
	-- -- self.m_BuyBtn:SetGrey(bGrey)
	-- -- self.m_BuyBtn:SetEnabled(not bGrey)
	-- if bGrey then
	-- 	self.m_BuyBtn:UITweenStop()
	-- end
	-- self.m_BuyBtn:SetSpriteName(bGrey and "btn_erji_anhua" or "btn_putong_anniu")
	if g_WelfareCtrl:IsBuyCzjj() then
		self.m_BuyBtn:SetActive(false)
		self.m_BuyNemoBtn:SetActive(false)
	else
		self.m_BuyBtn:SetActive(true)
		self.m_BuyNemoBtn:SetActive(true)
	end
end

function CCzjjPage.RefreshGradeGiftPrice(self,shopId)
	local goodsPosListFull = data.npcstoredata.GoodsDataSort[shopId] -- g_NpcShopCtrl:GetGoodsPosList(shopId)
	local goodsPosList ={}
	local goodYk = nil
	if shopId ~= nil and goodsPosListFull ~= nil then
		for k,v in ipairs(goodsPosListFull) do
			local storeData = data.npcstoredata.DATA[v]
			if (storeData.event_type == "grade_gift1" ) then
				table.insert(goodsPosList,v)
				goodYk = storeData
			end
		end
	end
	if(goodYk ~=nil) then 
		if shopId == define.Store.Page.LimitSkin then
			-- netother.C2GSRequestPay(self.nemo_payid, self.nemo_id)
			self.nemo_id = goodYk.id
			self.nemo_payid = goodYk.payid
			-- if Utils.IsIOS() then
			-- 	self.nemo_payid = goodYk.iospayid
			-- end
			self.m_NemoPrice:SetText(goodYk.coin_count/10000)
		end
		if shopId == define.Store.Page.LiBaoShop then
			-- netother.C2GSRequestPay(self.iap_payid, self.iap_id)
			self.iap_id = goodYk.id
			self.iap_payid = goodYk.payid
			if Utils.IsIOS() then
				self.iap_payid = goodYk.iospayid
			end
			self.m_IAPPrice:SetText(goodYk.coin_count/100 .. "$")
		end
	end
end


function CCzjjPage.OnBuyGradeGiftIAP(self)
	printDebug(self.iap_payid,self.iap_id)
	if Utils.IsDevUser() and Utils.IsEditor() then
		netother.C2GSGMCmd(string.format("huodong charge 301"))
		g_NotifyCtrl:FloatMsg("Directly call the GM commands, the operation is prone to high risk!!! For testing only")
	else
		netother.C2GSRequestPay(self.iap_payid, self.iap_id)
	end
end

function CCzjjPage.OnBuyGradeGiftNemo(self)
	printDebug(self.nemo_payid,self.nemo_id)
	if Utils.IsDevUser() and Utils.IsEditor() then
		netother.C2GSGMCmd(string.format("huodong charge 301"))
		g_NotifyCtrl:FloatMsg("Directly call the GM commands, the operation is prone to high risk!!! For testing only")
	else
		local args = 
				{
					msg = "Buy this package with NEMO?",
					okCallback = function ()
							netother.C2GSRequestPay(self.nemo_payid, self.nemo_id)
							g_NotifyCtrl:FloatMsg("The request is under processing and will be updated when the transaction is finished.")
					end,
					cancelCallback = function ()
					end,
					okStr = "Yes",
					cancelStr = "No",
				}
		g_WindowTipCtrl:SetWindowConfirm(args)
	end
end

function CCzjjPage.OnBuy(self, oBtn)
	if g_WelfareCtrl:IsBuyCzjj() then
		g_NotifyCtrl:FloatMsg("You've bought it")
		return
	end
	local key
	if g_LoginCtrl:IsSdkLogin() then
		if Utils.IsAndroid() then
			key = "com.kaopu.ylq.czjj"
		elseif Utils.IsIOS() then
			key = "com.kaopu.ylq.appstore.czjj"
		end
	end
	if key then
		g_SdkCtrl:Pay(key, 1)
	else
		if Utils.IsDevUser() and Utils.IsEditor() then
			netother.C2GSGMCmd(string.format("huodong charge 301"))
			g_NotifyCtrl:FloatMsg("Directly call the GM commands, the operation is prone to high risk!!! For testing only")
		else
			g_NotifyCtrl:FloatMsg("The current environment does not support purchasing")
		end
	end
end

function CCzjjPage.RefreshGrid(self)
	local lDatas = table.values(data.chargedata.CZJJ_DATA)
	local function sortfunc(d1, d2)
		local v1 = g_WelfareCtrl:IsGetCzjjReward(d1.key) and 1 or 0
		local v2 = g_WelfareCtrl:IsGetCzjjReward(d2.key) and 1 or 0
		if v1 == v2 then
			return d1.grade < d2.grade
		else
			return v1 < v2
		end
	end
	table.sort(lDatas, sortfunc)
	self.m_Grid:Clear()
	for i, dData in ipairs(lDatas) do
		local oBox = self.m_BoxClone:Clone()
		oBox:SetActive(true)
		oBox:SetData(dData)
		self.m_Grid:AddChild(oBox)
	end
	self.m_Grid:Reposition()
end

return CCzjjPage