local CTyThiSelectAspoView = class("CTyThiSelectAspoView", CViewBase)

function CTyThiSelectAspoView.ctor(self, cb)
	CViewBase.ctor(self, "UI/TyThi/TyThiSelectAspoView.prefab", cb)
end

function CTyThiSelectAspoView.OnCreateView(self)
	self.m_CloseBtn = self:NewUI(1, CButton)
	self.m_Grid = self:NewUI(2, CGrid)
	self.m_OKBtn = self:NewUI(3, CButton)
	self:InitContent()
end
function CTyThiSelectAspoView.PK(self,Pid)
	self.m_Pid = Pid
end

function CTyThiSelectAspoView.InitContent(self)
	self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))
	self.m_OKBtn:AddUIEvent("click", callback(self, "OnOk"))
	self:InitGrid()
end

function CTyThiSelectAspoView.InitGrid(self)
	self.m_Id = nil
	self.m_Grid:InitChild(function (obj, idx)
		local oBox = CBox.New(obj)
		oBox.m_Idx = idx
		oBox.m_TimeLabel = oBox:NewUI(1, CLabel)
		oBox.m_OnSelectSprite = oBox:NewUI(2, CSprite)
		oBox:SetGroup(self.m_Grid:GetInstanceID())
		return oBox
	end)
	local oDefault
	local lData = data.pkdata.PK_BET
	for i,oBox in ipairs(self.m_Grid:GetChildList()) do
		local dData = lData[i]
		oBox.m_Id = dData.id
		oBox.m_TimeLabel:SetText(dData.cost_color_coin/10000)
		oBox:AddUIEvent("click", callback(self, "OnBox"))
		self.m_Grid:AddChild(oBox)
		if not oDefault then
			oDefault = oBox
		end
	end
	self.m_Grid:Reposition()
	if oDefault then
		self:OnBox(oDefault)
	end
end

function CTyThiSelectAspoView.OnBox(self, oBox)
	oBox:SetSelected(true)
	self.m_Id = oBox.m_Id
end

function CTyThiSelectAspoView.OnOk(self)
	netplayer.C2GSPlayerPK(self.m_Pid,self.m_Id)
	self:CloseView()
end

return CTyThiSelectAspoView