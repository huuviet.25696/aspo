local CDrawWlPage = class("CDrawWlPage", CPageBase)

function CDrawWlPage.ctor(self, obj)
	CPageBase.ctor(self, obj)
end

function CDrawWlPage.OnInitPage(self)
	self.m_CloseBtn = self:NewUI(1, CButton)
	self.m_CartGrid = self:NewUI(2, CGrid)
	self.m_CardBox = self:NewUI(3, CBox)

	self.m_AgainBtn = self:NewUI(4, CButton)
	self.m_SmallCardBox = self:NewUI(5, CBox)
	self.m_JoinUpBtn = self:NewUI(6, CButton)
	self.m_CloseBtn2 = self:NewUI(7, CButton)
	self.m_BtnContainer = self:NewUI(8, CObject)
	self.m_NextBtn = self:NewUI(9, CSprite)
	self.m_LastBtn = self:NewUI(10, CSprite)
    self.m_ScrollView = self:NewUI(11, CScrollView)
	self.m_WLDrawOnceIcon = self:NewUI(12, CSprite)
	self.m_WLDrawOnceLabel = self:NewUI(13, CLabel)

	self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))
	self.m_CloseBtn2:AddUIEvent("click", callback(self, "OnClose"))
	self.m_AgainBtn:AddUIEvent("click", callback(self, "OnAgain"))
	self:InitCardBox(self.m_CardBox)
	self.m_SmallCardBox:SetActive(false)
	g_GuideCtrl:AddGuideUI("close_wl_result_rt", self.m_CloseBtn)
	g_GuideCtrl:AddGuideUI("close_wl_result_lb", self.m_CloseBtn2)
	 self.m_ScrollView:AddMoveCheck("left", self.m_ScrollView, callback(self, "OnMoveEnd"))
	 self.m_ScrollView:AddMoveCheck("right", self.m_ScrollView, callback(self, "OnMoveEnd"))
	--self:OnMoveEnd()
	self:InitValues()
	self:RefreshValues()
end

function CDrawWlPage:InitValues()  
	self.item_upcard_vals = string.safesplit(data.globaldata.GLOBAL.item_upcard.value, ",")
	self.item_upcard_nums = string.safesplit(data.globaldata.GLOBAL.item_upcard_num.value, ",")
	self.item_upcard_multis = string.safesplit(data.globaldata.GLOBAL.item_upcard_multi.value, ",")
	
end

function CDrawWlPage:RefreshValues()  

	local index =tonumber(self.item_upcard_vals[g_ChoukaCtrl.drawType])
	local one =  tonumber(self.item_upcard_nums[g_ChoukaCtrl.drawType])
	local multi = one * tonumber(self.item_upcard_multis[g_ChoukaCtrl.drawType])/10
	if(g_ChoukaCtrl.drawMulti) then
		self.m_WLDrawOnceLabel:SetText(multi)
		self.m_AgainBtn:SetText("Recruit more 10")
	else
		self.m_AgainBtn:SetText("Recruit one more")
		self.m_WLDrawOnceLabel:SetText(one)
	end
	self.m_WLDrawOnceIcon:SetSpriteName(string.format("%s", data.itemdata.OTHER[index].icon))
	
end

function CDrawWlPage.OnMoveEnd(self)
	local pos = self.m_ScrollView:GetLocalPos()
	self.m_LastBtn:SetActive(pos.x <= 463)
	self.m_NextBtn:SetActive(pos.x >= -252)
end
function CDrawWlPage.SyncCardPos(self, oCard, iEffectIdx)
	local oEffect = g_ChoukaCtrl.m_WLEffectList[iEffectIdx]
	local oPos = oEffect:GetPos()
	local oChoukaCam = g_CameraCtrl:GetChoukaCamera()
	local oUICam = g_CameraCtrl:GetUICamera()
	local viewPos = oWarCam:WorldToViewportPoint(warpos)
	local oUIPos = oUICam:ViewportToWorldPoint(viewPos)
	oUIPos.z = 0
	oCard:SetPos(oUIPos)
end

function CDrawWlPage:SetResult(parlist)
	g_GuideCtrl:DelGuideUIEffect("close_wl_result_lb", "round")
	if #parlist == 1 then
		self.m_CartGrid:SetActive(false)
		self.m_CardBox:SetActive(true)
		self.m_CardBox:SetLocalPos(Vector3.New(0,0,0))
		self:SetCardPartner(self.m_CardBox, parlist[1])
		g_ChoukaCtrl:SyncCardPos(self.m_CardBox, 1)
	else
		self.m_CartGrid:Clear()
		self.m_CardBox:SetActive(false)
		self.m_CartGrid:SetActive(true)
		local amount = #parlist
		for i, parid in ipairs(parlist) do
			local oPartner = g_PartnerCtrl:GetPartner(parid)
			if oPartner then
				local smallbox = self.m_SmallCardBox:Clone()
				local box = smallbox:NewUI(1, CBox)
				smallbox.m_CardBox = box
				self:InitCardBox(box)
				self:SetCardPartner(box, parid)
				-- local v = box:GetLocalPos()
				-- local x = math.floor(math.abs(i - (1 + amount)/2))
				-- v.y = v.y + x*5
				-- box:SetLocalPos(v)
				-- box:SetDepth(box:GetDepth()+i)
				smallbox:SetActive(true)
				self.m_CartGrid:AddChild(smallbox)
				g_ChoukaCtrl:SyncCardPos(smallbox, i)
			end
		end
		-- self.m_CartGrid:Reposition()
	end

	-- if g_GuideCtrl:IsCustomGuideFinishByKey("Open_ZhaoMu") and not g_GuideCtrl:IsCustomGuideFinishByKey("DrawCard") then
	-- 	self.m_IsInDrawCardGuide = true
	-- end
end

function CDrawWlPage:SetResultWithPartersAndItems(parlist, itemlist)
	self:RefreshValues()
	printDebug("CDrawWlPage:SetResultWithPartersAndItems ".. #parlist .. "." .. #itemlist)
	g_GuideCtrl:DelGuideUIEffect("close_wl_result_lb", "round")
	if (#parlist + #itemlist) == 1 then
		self.m_CartGrid:SetActive(false)
		self.m_CardBox:SetActive(true)
		self.m_CardBox:SetLocalPos(Vector3.New(0,0,0))
		if(#parlist == 1) then
			self:SetCardPartner(self.m_CardBox, parlist[1])
		end
		if(#itemlist == 1) then
			self:SetCardItem(self.m_CardBox, itemlist[1])
		end
		g_ChoukaCtrl:SyncCardPos(self.m_CardBox, 1)
	else
		self.m_CartGrid:Clear()
		self.m_CardBox:SetActive(false)
		self.m_CartGrid:SetActive(true)
		local amount = #parlist + #itemlist
		for i, parid in ipairs(parlist) do
			local oPartner = g_PartnerCtrl:GetPartner(parid)
			if oPartner then
				local smallbox = self.m_SmallCardBox:Clone()
				local box = smallbox:NewUI(1, CBox)
				smallbox.m_CardBox = box
				self:InitCardBox(box)
				self:SetCardPartner(box, parid)
				-- local v = box:GetLocalPos()
				-- local x = math.floor(math.abs(i - (1 + amount)/2))
				-- v.y = v.y + x*5
				-- box:SetLocalPos(v)
				-- box:SetDepth(box:GetDepth()+i)
				smallbox:SetActive(true)
				self.m_CartGrid:AddChild(smallbox)
				g_ChoukaCtrl:SyncCardPos(smallbox, i)
			end
		end
		for i, item in ipairs(itemlist) do
			local itemId = item["sid"]
			local amount = item["amount"]
			
			local oItem = CItem.NewBySid(tonumber(itemId))
			if oItem then
				local smallbox = self.m_SmallCardBox:Clone()
				local box = smallbox:NewUI(1, CBox)
				smallbox.m_CardBox = box
				self:InitCardBox(box)
				self:SetCardItem(box, item)
				-- local v = box:GetLocalPos()
				-- local x = math.floor(math.abs(i - (1 + amount)/2))
				-- v.y = v.y + x*5
				-- box:SetLocalPos(v)
				-- box:SetDepth(box:GetDepth()+i)
				smallbox:SetActive(true)
				self.m_CartGrid:AddChild(smallbox)
				g_ChoukaCtrl:SyncCardPos(smallbox, i)
			end
		end
		-- self.m_CartGrid:Reposition()
	end

	-- if g_GuideCtrl:IsCustomGuideFinishByKey("Open_ZhaoMu") and not g_GuideCtrl:IsCustomGuideFinishByKey("DrawCard") then
	-- 	self.m_IsInDrawCardGuide = true
	-- end
end

function CDrawWlPage.SetBtnShow(self, bShow)
	self.m_BtnContainer:SetActive(bShow)
end

function CDrawWlPage:SetCardPartner(box, parid)
	local oPartner = g_PartnerCtrl:GetPartner(parid)
	local oPartnerRare = g_PartnerCtrl:GetAttr(oPartner:GetValue("parid"))
	local iRare = oPartnerRare["total_rank"]
	box.m_NameLabel:SetText(oPartner:GetValue("name"))
	box:SetActive(false)
	box.m_StarGrid:Clear()
	box.m_PartnerTexture:SetActive(true)
	box.m_ItemTexture:SetActive(false)
	for i = 1, 5 do
		local starbox = box.m_StarBox:Clone()
		starbox.m_StarSpr = starbox:NewUI(1, CSprite)
		starbox.m_GreySpr = starbox:NewUI(2, CSprite)
		starbox.m_StarSpr:SetActive(oPartner:GetValue("star") >= i)
		starbox.m_GreySpr:SetActive(oPartner:GetValue("star") < i)
		starbox:SetActive(true)
		box.m_StarGrid:AddChild(starbox)
	end
	box.m_RareSpr:SetSpriteName("text_xiaodengji_"..tostring(iRare))
	box.m_PartnerTexture:ChangeShape(oPartner:GetValue("shape"), {}, function () box:SetActive(true) end)
end

function CDrawWlPage:SetCardItem(box, item)
	local itemId = item["sid"]
	local amount = item["amount"]
	local oItem = CItem.NewBySid(tonumber(itemId))
	
	box.m_NameLabel:SetText(oItem:GetValue("name"))
	box.m_StarGrid:Clear()
	box.m_PartnerTexture:SetActive(false)
	box.m_ItemTexture:SetActive(true)
	box.m_ItemTexture:SetSpriteName(oItem:GetValue("icon"))
	box.m_ItemNum:SetText(amount)
	box:SetActive(true)
end

function CDrawWlPage:InitCardBox(box)
	box.m_NameLabel = box:NewUI(1, CLabel)
	box.m_RareSpr = box:NewUI(2, CSprite)
	box.m_PartnerTexture = box:NewUI(3, CActorTexture)
	box.m_StarGrid = box:NewUI(4, CGrid)
	box.m_StarBox = box:NewUI(5, CBox)
	box.m_ItemTexture = box:NewUI(6, CSprite)
	box.m_ItemNum = box:NewUI(7, CLabel)
	box.m_StarBox:SetActive(false)
end

function CDrawWlPage.OnAgain(self)
	-- if not g_NetCtrl:IsValidSession(netdefines.C2GS_BY_NAME["C2GSDrawWuLingCard"], 5) then
	-- 	return
	-- end
	local bUp = self.m_JoinUpBtn:GetSelected()
	-- local istate = IOTools.GetRoleData("chouka_bullet") or 1
	-- netpartner.C2GSDrawWuLingCard(1, istate == 0)
	local amount = g_ItemCtrl:GetBagItemAmountBySid(tonumber(self.item_upcard_vals[g_ChoukaCtrl.drawType]))
	local one =  tonumber(self.item_upcard_nums[g_ChoukaCtrl.drawType])
	local multi = one * tonumber(self.item_upcard_multis[g_ChoukaCtrl.drawType])/10

	-- printDebug(istate,amount .. ":" .. one)

	if(g_ChoukaCtrl.drawMulti) then
		if( ((amount < multi) == true)) then 
			g_NotifyCtrl:FloatMsg("Not enough Valarian item to Recruit")
			return
		end
		netpartner.C2GSDrawWuHunCard(g_ChoukaCtrl.drawType, false, 1, 10)
	else
		if( ((amount < one) == true)) then 
			g_NotifyCtrl:FloatMsg("Not enough Valarian item to Recruit")
			return
		end
		netpartner.C2GSDrawWuHunCard(g_ChoukaCtrl.drawType, false, 1, 1)
	end
	
end

-- function CDrawWlPage.OnAgainMore(self)
-- 	local bUp = self.m_JoinUpBtn:GetSelected()
-- 	-- netpartner.C2GSDrawWuLingCard(5)
-- 	netpartner.C2GSDrawWuHunCard(g_ChoukaCtrl.drawType, false, 1, 10)
-- end

function CDrawWlPage.OnClose(self)
	self.m_ParentView:ShowMain()
	g_ChoukaCtrl:ShowMainPage(1)
	if self.m_IsInDrawCardGuide == true then
		g_GuideCtrl:DelayShowDrawCloseLBEffect()
	end
end

return CDrawWlPage