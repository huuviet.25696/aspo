local CDrawMainPage = class("CDrawMainPage", CPageBase)

function CDrawMainPage.ctor(self, obj)
	CPageBase.ctor(self, obj)
end

function CDrawMainPage:OnInitPage()
	self.m_BackBtn = self:NewUI(2, CButton)
	self.m_HelpBtn = self:NewUI(3, CButton)
	self.m_JoinUpBtn = self:NewUI(4, CButton)
	self.m_WuLingCard = self:NewUI(5, CButton)
	self.m_WuHunCard = self:NewUI(6, CButton)
	self.m_ZhongshenTexture = self:NewUI(8, CTexture)
	self.m_ZhongshenBtn = self:NewUI(9, CButton)
	self.m_WuHunPart = self:NewUI(10, CBox)
    self.m_CardNangCao = self:NewUI(11, CButton)
	self.m_NormalPart = self:NewUI(12, CBox)
	self.m_LegendaryPart = self:NewUI(13, CBox)
	self.m_WuLingCard:AddUIEvent("click", callback(self, "OnShowNormalPart"))
	self.m_WuHunCard:AddUIEvent("click", callback(self, "OnShowWuhunPart"))
	self.m_CardNangCao:AddUIEvent("click", callback(self, "OnShowLegendaryPart"))
	self.m_HelpBtn:AddUIEvent("click", callback(self, "OpenWuHunChance"))
	-- self.m_ZhongshenBtn:AddUIEvent("click", callback(g_OpenUICtrl, "OpenYueKa"))
	self.m_ZhongshenBtn:SetActive(false)
	-- self.m_HelpBtn:SetActive(false)

	g_StateCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnStateEvent"))
	g_PartnerCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnPartnerEvent"))
	g_WelfareCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnWelfareCtrl"))
	g_GuideCtrl:AddGuideUI("draw_wl_card", self.m_WuLingCard)
	g_GuideCtrl:AddGuideUI("draw_wh_card", self.m_WuHunCard)
	local guide_ui = {"draw_wl_card", "draw_wh_card"}
	g_GuideCtrl:LoadTipsGuideEffect(guide_ui)

	self:initValues()

	self:InitEffect()
	self:UpdateZhongshenkai()
	self:InitNormalPart()
	self:InitWuHunPart()
	self:InitLegendaryPart()
end

-- function CDrawMainPage.OnShowPage(self)
-- 	g_ChoukaCtrl:ShowMainPage()
-- end
function CDrawMainPage:initValues()
	self.drawType = 1
	self.oneTimeFree = false
	self.item_upcard_vals = string.safesplit(data.globaldata.GLOBAL.item_upcard.value, ",")
	self.item_upcard_nums = string.safesplit(data.globaldata.GLOBAL.item_upcard_num.value, ",")
	self.item_upcard_multis = string.safesplit(data.globaldata.GLOBAL.item_upcard_multi.value, ",")

end
function CDrawMainPage.OnStateEvent(self, oCtrl)
	self:RefreshDrawState()
end

function CDrawMainPage.OnPartnerEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Partner.Event.UpdateChoukaConfig then
		self:RefreshDrawState()
	end
end

function CDrawMainPage.OnWelfareCtrl(self, oCtrl)
	if oCtrl.m_EventID == define.Welfare.Event.OnYueKa then
		self:UpdateZhongshenkai()
		netpartner.C2GSOpenDrawCardUI()
	end
end

function CDrawMainPage:InitEffect()
	------- NORMAL ---------
	local function cb1()
		local comp = self.m_NormalEff.m_Eff:GetComponent(classtype.DataContainer)
		local replaceAnimObj = comp.gameObjectValue
		local m_EffectBox = CBox.New(replaceAnimObj.gameObject)
		self.m_FreeLabelNormal = m_EffectBox:NewUI(1, CLabel)
		self:RefreshNormalState()
	end

	local path = "Effect/UI/ui_eff_6300/Prefabs/ui_eff_6300_chouka_yueliang_lan.prefab"
	self.m_NormalEff =  CEffect.New(path, self:GetLayer(), false, cb1)
	self.m_NormalEff:SetParent(self.m_WuLingCard.m_Transform)

	------- SPECIAL ---------
	local function cb2()
		local comp = self.m_WHEffect.m_Eff:GetComponent(classtype.DataContainer)
		local replaceAnimObj = comp.gameObjectValue
	
		local m_EffectBox = CBox.New(replaceAnimObj.gameObject)
		-- self.m_OQLabel = self.m_EffectBox:NewUI(1, CLabel)
		-- self.m_BaodiLabel = self.m_EffectBox:NewUI(2, CLabel)
		-- self.m_FreeLabel = self.m_EffectBox:NewUI(3, CLabel)
		-- self.m_BaodiLabel2 = self.m_EffectBox:NewUI(4, CLabel)
		-- self.m_AchieveSlider = self.m_EffectBox:NewUI(5, CSlider)
		-- self.m_EffectProcess = self.m_EffectBox:NewUI(6, CLabel)
		self.m_ProgressSpecialLabel = m_EffectBox:NewUI(7, CLabel)

		self:RefreshDrawState()
	end
	path = "Effect/UI/ui_eff_6300/Prefabs/ui_eff_6300_chouka_yueliang_huang.prefab"
	self.m_WHEffect =  CEffect.New(path, self:GetLayer(), false, cb2)
	self.m_WHEffect:SetParent(self.m_WuHunCard.m_Transform)
	
	-- EFFECT NANG CAO
	local function cb3()
		local comp = self.m_LegendaryEff.m_Eff:GetComponent(classtype.DataContainer)
		local replaceAnimObj = comp.gameObjectValue
	
		local m_EffectBox = CBox.New(replaceAnimObj.gameObject)
	
		self.m_ProgressLegendaryLabel = m_EffectBox:NewUI(5, CLabel)

		self:RefreshDrawState()
	end
	local path = "Effect/UI/ui_eff_6300/Prefabs/CAM.prefab"
	self.m_LegendaryEff =  CEffect.New(path, self:GetLayer(), false, cb3)
	self.m_LegendaryEff:SetParent(self.m_CardNangCao.m_Transform)

end

--Normalpart
function CDrawMainPage:InitNormalPart()
	-- self.m_WHDrawOnceBtn = self.m_NormalPart:NewUI(1, CButton)
	-- self.m_WHDrawOnceLabel = self.m_NormalPart:NewUI(2, CLabel)
	-- self.m_WHDrawTenBtn = self.m_NormalPart:NewUI(3, CButton)
	-- self.m_WHDrawFiveLabel = self.m_NormalPart:NewUI(4, CLabel)
	-- self.m_WHDrawFreeLabel = self.m_NormalPart:NewUI(5, CLabel)-- khong dung free
	--self.m_IconSpriteOnce=self.m_NormalPart:NewUI(6, CSprite)
	--self.m_IconSpriteTen=self.m_NormalPart:NewUI(7, CSprite)
	-- self.m_WHDrawOnceBtn:AddUIEvent("click", callback(self, "OpenWuLing"))
	-- self.m_WHDrawTenBtn:AddUIEvent("click", callback(self, "OpenWuLingMore"))
	-- self.m_WHDrawFreeLabel:SetActive(false)

	self.m_NormalPart:SetActive(false)
	self.m_BackBtn:AddUIEvent("click", callback(self, "OnBackMain"))
	self.m_BackBtn:SetActive(false)

	------------------------------
	-- local one =  tonumber(self.item_upcard_nums[1])
	-- local multi = one * tonumber(self.item_upcard_multis[1])/10
	-- self.m_WHDrawOnceLabel:SetText(one)
	-- self.m_WHDrawFiveLabel:SetText(multi)
end
--end


-- SpecialPart 
function CDrawMainPage:InitWuHunPart()
	-- self.m_WHDrawOnceBtn = self.m_WuHunPart:NewUI(1, CButton)
	-- self.m_WHDrawOnceLabel = self.m_WuHunPart:NewUI(2, CLabel)
	-- self.m_WHDrawFiveBtn = self.m_WuHunPart:NewUI(3, CButton)
	-- self.m_WHDrawFiveLabel = self.m_WuHunPart:NewUI(4, CLabel)
	-- --self.m_WHDrawFreeLabel = self.m_WuHunPart:NewUI(5, CLabel)-- khong dung free
	-- self.m_WHDrawOnceBtn:AddUIEvent("click", callback(self, "ShowWuHunTip"))
	-- self.m_WHDrawFiveBtn:AddUIEvent("click", callback(self, "OnDrawFiveWuHun"))
	-- self.m_BackBtn:AddUIEvent("click", callback(self, "OnBackMain"))
	-- self.m_BackBtn:SetActive(false)
	self.m_WuHunPart:SetActive(false)
	--self.m_WHDrawFreeLabel:SetActive(false)
	-- g_GuideCtrl:AddGuideUI("draw_wh_card_again", self.m_WHDrawOnceBtn)
end
--end

--LegendaryPart
function CDrawMainPage:InitLegendaryPart()
	-- self.m_WHDrawOnceBtn = self.m_LegendaryPart:NewUI(1, CButton)
	-- self.m_WHDrawOnceLabel3 = self.m_LegendaryPart:NewUI(2, CLabel)
	-- self.m_WHDrawTenBtn = self.m_LegendaryPart:NewUI(3, CButton)
	-- self.m_WHDrawFiveLabel3 = self.m_LegendaryPart:NewUI(4, CLabel)
	-- --self.m_WHDrawFreeLabel = self.m_LegendaryPart:NewUI(5, CLabel) --khong dung free
	-- self.m_WHDrawOnceBtn:AddUIEvent("click", callback(self, "OpenOneLegendary"))
	-- self.m_WHDrawTenBtn:AddUIEvent("click", callback(self, "OpenTenLegendary"))
	-- self.m_BackBtn:AddUIEvent("click", callback(self, "OnBackMain"))
	-- self.m_BackBtn:SetActive(false)
	self.m_LegendaryPart:SetActive(false)
	--self.m_WHDrawFreeLabel:SetActive(false)
end
function CDrawMainPage.UpdateZhongshenkai(self)
	local d = data.welfaredata.WelfareControl[define.Welfare.ID.Zsk]
	local bShow = true
	if main.g_AppType  == "shenhe" or 
		data.globalcontroldata.GLOBAL_CONTROL.welfare.is_open ~= "y" or 
		g_AttrCtrl.grade < data.globalcontroldata.GLOBAL_CONTROL.welfare.open_grade then
		bShow = false
	end
	if d.open ~= 1 or g_AttrCtrl.grade < d.grade then
		bShow = false
	end
	if not g_WelfareCtrl:HasZhongShengKa() and bShow then
		self.m_ZhongshenTexture:SetActive(true)
	else
		self.m_ZhongshenTexture:SetActive(false)
	end
end

function CDrawMainPage.DoShowEffect(self)
	g_ChoukaCtrl:ForceShowMain()
end

function CDrawMainPage:RefreshNormalState()
	-- local index =tonumber(self.item_upcard_vals[1])
	-- local one =  tonumber(self.item_upcard_nums[1])
	-- local multi = one * tonumber(self.item_upcard_multis[1])/10
	
	-- local iTime = g_PartnerCtrl:GetChoukaFreeCD() - g_TimeCtrl:GetTimeS()
	-- if iTime > 0 then
	-- 	local str = self:ConverTimeString(iTime)
	-- 	self.m_FreeLabelNormal:SetText(str)
	-- else
	-- 	self.oneTimeFree = true
	-- 	self.m_FreeLabelNormal:SetText("")
	-- end
	self:CreateFreeNormalTimer()
end

function CDrawMainPage:RefreshDrawState()
	-- if self.m_OQLabel then
	-- 	local statedata = g_StateCtrl:GetState(1003)
	-- 	if statedata then
	-- 		self.m_LeftTime = statedata["time"] - g_TimeCtrl:GetTimeS()
	-- 		if self.m_LeftTime > 0 then
	-- 			self:CreateOQTimer()
	-- 		else
	-- 			self.m_OQLabel:SetActive(false)
	-- 		end
	-- 	else
	-- 		self.m_OQLabel:SetActive(false)
	-- 	end

	-- 	local cost = g_PartnerCtrl:GetChoukaCost()
	-- 	self:RefreshFresTime()
	-- 	self.m_WHDrawOnceLabel:SetText(tostring(cost))
	-- 	self.m_WHDrawFiveLabel:SetText(tostring(g_PartnerCtrl:GetChoukaMulCost()))
		
	-- end
	if(self.m_ProgressSpecialLabel) then 
		local baodi = tonumber(g_PartnerCtrl:GetBaodiTimes())
		self.m_ProgressSpecialLabel:SetText(string.format("%d/%d", baodi, 20))
	end
	if(self.m_ProgressLegendaryLabel) then 
		local smax = tonumber(g_PartnerCtrl:GetBaodiMaxTimes())
		self.m_ProgressLegendaryLabel:SetText(string.format("%d/%d", smax, 20))
	end
end

function CDrawMainPage:RefreshFresTime()
	-- local t = g_PartnerCtrl:GetChoukaFreeCD()
	-- if not self.m_FreeLabel then
	-- 	return
	-- end
	-- --code b77
	-- --local baodiMax = g_PartnerCtrl:GetBaodiMaxTimes()
	-- --end 
	-- local baodi = g_PartnerCtrl:GetBaodiTimes() or 9
	-- if baodi == 1 then
	-- 	self.m_BaodiLabel:SetActive(false)
	-- 	self.m_BaodiLabel2:SetActive(true)
	-- else
	-- 	self.m_BaodiLabel:SetText(tostring(baodi))
	-- 	self.m_BaodiLabel:SetActive(true)
	-- 	self.m_BaodiLabel2:SetActive(false)
	-- end
    --b77
	-- local value = baodi / baodiMax
	-- if value >= 1 then
	-- 	self.m_EffectProcess:SetActive(true)
	-- else
	-- 	self.m_EffectProcess:SetActive(false)
	-- end
	-- self.m_AchieveSlider:SetValue(value)
	-- self.m_AchieveSlider:SetSliderText(string.format("%d/%d", baodi, baodiMax))
	--end

	-- if g_PartnerCtrl:IsChoukaFree() then
	-- 	self.m_FreeLabel:SetText("Royal recruits for free this time")
	-- 	self.m_WHDrawFreeLabel:SetActive(true)
	-- 	self.m_WHDrawOnceLabel:SetActive(false)
	-- 	return
	-- else
	-- 	self.m_WHDrawFreeLabel:SetActive(false)
	-- 	self.m_WHDrawOnceLabel:SetActive(true)
	-- 	self.m_FreeLabel:SetText("\n\n\n"..g_TimeCtrl:GetLeftTime(t-g_TimeCtrl:GetTimeS()).."later can be free")
	-- end

	-- local function update()
	-- 	if Utils.IsNil(self) then
	-- 		return
	-- 	end
	-- 	local leftTime = g_PartnerCtrl:GetChoukaFreeCD() - g_TimeCtrl:GetTimeS()
	-- 	if leftTime > 0 then
	-- 		self.m_FreeLabel:SetText(g_TimeCtrl:GetLeftTime(leftTime).."later can be free")
	-- 		return true
	-- 	else
	-- 		self.m_FreeLabel:SetText("Royal recruits for free this time")
	-- 	end
	-- end
	
	-- if self.m_FreeTimer then
	-- 	Utils.DelTimer(self.m_FreeTimer)
	-- end
	-- self.m_FreeTimer = Utils.AddTimer(update, 1, 0)
end

-- function CDrawMainPage:CreateOQTimer()
	-- local function update()
	-- 	if Utils.IsNil(self) then
	-- 		return
	-- 	end
	-- 	self.m_LeftTime = self.m_LeftTime - 1
	-- 	if self.m_LeftTime >= 0 then
	-- 		local timestr = g_TimeCtrl:GetLeftTime(self.m_LeftTime)
	-- 		self.m_OQLabel:SetText("God Blessing:"..timestr)
	-- 		return true
	-- 	else
	-- 		self.m_OQLabel:SetActive(false)
	-- 	end
	-- end
	-- self.m_OQLabel:SetActive(true)
	-- self.m_OQLabel:SetText("God Blessing:"..g_TimeCtrl:GetLeftTime(self.m_LeftTime))
	
	-- if self.m_OQTimer then
	-- 	Utils.DelTimer(self.m_OQTimer)
	-- end
	-- self.m_OQTimer = Utils.AddTimer(update, 1, 0)
-- end

function CDrawMainPage:CreateFreeNormalTimer()
	local index =tonumber(self.item_upcard_vals[1])
	local one =  tonumber(self.item_upcard_nums[1])
	local multi = one * tonumber(self.item_upcard_multis[1])/10
	
	self.m_NextTime = g_PartnerCtrl:GetChoukaFreeCD() - g_TimeCtrl:GetTimeS()
	if self.m_NextTime > 0 then
		self.oneTimeFree = false
		local str = self:ConverTimeString(self.m_NextTime)
		self.m_FreeLabelNormal:SetText(str)
	else
		self.oneTimeFree = true
		self.m_FreeLabelNormal:SetText("")
		if(self.m_WHDrawOnceLabel) then self.m_WHDrawOnceLabel:SetText("FREE") end
		-- return
	end

	g_ChoukaCtrl.oneTimeFree = self.oneTimeFree
	-- printDebug("g_ChoukaCtrl oneTime", self.oneTimeFree)
	-- if(g_ChoukaCtrl.oneTimeFree) then return end

	local function update()
		if Utils.IsNil(self) then
			return
		end
		self.m_NextTime = self.m_NextTime - 1
		if self.m_NextTime > 0 then
			local str = self:ConverTimeString(self.m_NextTime)
			self.m_FreeLabelNormal:SetText(str)
			-- printDebug(str)
			return true
		else
			self.oneTimeFree = true
			g_ChoukaCtrl.oneTimeFree = self.oneTimeFree
			self.m_FreeLabelNormal:SetText("")
			if(self.m_WHDrawOnceLabel) then self.m_WHDrawOnceLabel:SetText("FREE") end
		end
	end
	-- self.m_FreeLabelNormal:SetText(self:ConverTimeString(self.m_NextTime))
	
	if self.m_FreeNormalTimer then
		Utils.DelTimer(self.m_FreeNormalTimer)
	end
	self.m_FreeNormalTimer = Utils.AddTimer(update, 1, 0)
end

function CDrawMainPage:OpenWuLing()
	--printDebug("1222222")
	-- if not g_NetCtrl:IsValidSession(netdefines.C2GS_BY_NAME["C2GSDrawWuLingCard"], 5) then
	-- 	return
	-- end
	-- local bUp = self.m_JoinUpBtn:GetSelected()
	-- local istate = IOTools.GetRoleData("chouka_bullet") or 1
	-- netpartner.C2GSDrawWuLingCard(1, istate == 0, 0)
	-- local bUp = self.m_JoinUpBtn:GetSelected()
	-- local iSend = bUp and 1 or 0
	local istate = self.oneTimeFree --IOTools.GetRoleData("chouka_bullet") or 1
	-- if (self.drawType >1 ) then istate = false end
	g_ChoukaCtrl.drawType = self.drawType
	g_ChoukaCtrl.drawMulti = false

	local amount = g_ItemCtrl:GetBagItemAmountBySid(tonumber(self.item_upcard_vals[self.drawType]))
	local one =  tonumber(self.item_upcard_nums[self.drawType])
	local multi = one * tonumber(self.item_upcard_multis[self.drawType])/10

	-- printDebug(istate,amount .. ":" .. one)

	if( ((amount < one) == true) and (istate == false)) then 
		g_NotifyCtrl:FloatMsg("Not enough Valarian item to Recruit")
		return
	end
	printDebug("CDrawMainPage:OpenWuLing "..self.drawType)
	netpartner.C2GSDrawWuHunCard(self.drawType, istate, 1, 1)
end

function CDrawMainPage:OpenWuLingMore()
	-- local bUp = self.m_JoinUpBtn:GetSelected()
	-- local istate = IOTools.GetRoleData("chouka_bullet") or 1
	-- netpartner.C2GSDrawWuLingCard(10, istate == 0, 0)
	-- local bUp = self.m_JoinUpBtn:GetSelected()
	-- local iSend = bUp and 1 or 0
	g_ChoukaCtrl.drawType = self.drawType
	g_ChoukaCtrl.drawMulti = true

	local amount = g_ItemCtrl:GetBagItemAmountBySid(tonumber(self.item_upcard_vals[self.drawType]))
	local one =  tonumber(self.item_upcard_nums[self.drawType])
	local multi = one * tonumber(self.item_upcard_multis[self.drawType])/10
	if(amount < multi) then 
		g_NotifyCtrl:FloatMsg("Not enough Valarian item to Recruit")
		return
	else

	end

	printDebug("CDrawMainPage:OpenWuLingMore "..self.drawType)
	local istate = false -- IOTools.GetRoleData("chouka_bullet") or 1
	netpartner.C2GSDrawWuHunCard(self.drawType, istate, 1, 10)
end

-- function CDrawMainPage:OpenWuHun(itype)
-- 	local bUp = self.m_JoinUpBtn:GetSelected()
-- 	local iSend = bUp and 1 or 0
-- 	local istate = IOTools.GetRoleData("chouka_bullet") or 1
-- 	itype = itype or 0
-- 	netpartner.C2GSDrawWuHunCard(2, 0, 1, 1)
-- end
--Legendary-- code moi
function CDrawMainPage:OpenOneLegendary()
	local bUp = self.m_JoinUpBtn:GetSelected()
	local iSend = bUp and 1 or 0
	local istate = IOTools.GetRoleData("chouka_bullet") or 1
	--itype = itype or 0
	netpartner.C2GSDrawWuHunCard(3, 0, 1, 1)
end

function CDrawMainPage.OpenTenLegendary(self)
	local bUp = self.m_JoinUpBtn:GetSelected()
	local iSend = bUp and 1 or 0
	local istate = IOTools.GetRoleData("chouka_bullet") or 1
	--itype = itype or 0
	netpartner.C2GSDrawWuHunCard(3, 0, 1, 10)
end

function CDrawMainPage:OnBackMain()
	self.m_WuLingCard:SetActive(true)
	self.m_CardNangCao:SetActive(true)
	self.m_WuHunCard:SetActive(true)
	self.m_WuLingCard:SetLocalPos(Vector3.New(7, 42.96321, 0))
	self.m_WuLingCard:SetLocalPos(Vector3.New(-413.7, 4, 0))
	self.m_CardNangCao:SetLocalPos(Vector3.New(414.9, 42, 0))
	self.m_WuHunPart:SetActive(false)
	self.m_NormalPart:SetActive(false)
	self.m_LegendaryPart:SetActive(false)
	self.m_HelpBtn:SetActive(true)
	self.m_BackBtn:SetActive(false)
end
---Normal
function CDrawMainPage:OnShowNormalPart()
	self.drawType = 1
	self.oneTimeFree = false
	self.m_WHDrawOnceBtn = self.m_NormalPart:NewUI(1, CButton)
	self.m_WHDrawOnceLabel = self.m_NormalPart:NewUI(2, CLabel)
	self.m_WHDrawTenBtn = self.m_NormalPart:NewUI(3, CButton)
	self.m_WHDrawFiveLabel = self.m_NormalPart:NewUI(4, CLabel)
	self.m_WHDrawFreeLabel = self.m_NormalPart:NewUI(5, CLabel)-- khong dung free

	self.m_WHDrawOnceIcon = self.m_NormalPart:NewUI(6, CSprite)
	self.m_WHDrawFiveIcon = self.m_NormalPart:NewUI(7, CSprite)
	
	self.m_WHDrawFreeLabel:SetActive(false)
	self.m_WHDrawOnceBtn:AddUIEvent("click", callback(self, "OpenWuLing"))
	self.m_WHDrawTenBtn:AddUIEvent("click", callback(self, "OpenWuLingMore"))

	self.m_WuHunCard:SetActive(false)
	self.m_CardNangCao:SetActive(false)
	self.m_WuLingCard:SetLocalPos(Vector3.New(7, 42.96321, 0))
	self.m_NormalPart:SetActive(true)
	self.m_HelpBtn:SetActive(false)
	self.m_BackBtn:SetActive(true)

	local index =tonumber(self.item_upcard_vals[1])
	local one =  tonumber(self.item_upcard_nums[1])
	local multi = one * tonumber(self.item_upcard_multis[1])/10
	self.m_WHDrawOnceLabel:SetText(one)
	self.m_WHDrawFiveLabel:SetText(multi)
	self.m_WHDrawOnceIcon:SetSpriteName(string.format("%s", data.itemdata.OTHER[index].icon))
	self.m_WHDrawFiveIcon:SetSpriteName(string.format("%s", data.itemdata.OTHER[index].icon))

	self:CreateFreeNormalTimer()
end

-- Special
function CDrawMainPage:OnShowWuhunPart()
	self.drawType = 2
	self.oneTimeFree = false 

	self.m_WHDrawOnceBtn = self.m_WuHunPart:NewUI(1, CButton)
	self.m_WHDrawOnceLabel = self.m_WuHunPart:NewUI(2, CLabel)
	self.m_WHDrawFiveBtn = self.m_WuHunPart:NewUI(3, CButton)
	self.m_WHDrawFiveLabel = self.m_WuHunPart:NewUI(4, CLabel)
	--self.m_WHDrawFreeLabel = self.m_WuHunPart:NewUI(5, CLabel)-- khong dung free

	self.m_WHDrawOnceIcon = self.m_NormalPart:NewUI(6, CSprite)
	self.m_WHDrawFiveIcon = self.m_NormalPart:NewUI(7, CSprite)

	self.m_WHDrawOnceBtn:AddUIEvent("click", callback(self, "OpenWuLing"))
	self.m_WHDrawFiveBtn:AddUIEvent("click", callback(self, "OpenWuLingMore"))
	self.m_BackBtn:AddUIEvent("click", callback(self, "OnBackMain"))
	-- self.m_BackBtn:SetActive(false)
	-- self.m_WuHunPart:SetActive(false)
	--self.m_WHDrawFreeLabel:SetActive(false)
	-- g_GuideCtrl:AddGuideUI("draw_wh_card_again", self.m_WHDrawOnceBtn)

	self.m_WuLingCard:SetActive(false)
	self.m_CardNangCao:SetActive(false)
	self.m_WuHunCard:SetLocalPos(Vector3.New(7, 42, 0))
	self.m_WuHunPart:SetActive(true)
	self.m_HelpBtn:SetActive(false)
	self.m_BackBtn:SetActive(true)

	local index =tonumber(self.item_upcard_vals[2])
	local one =  tonumber(self.item_upcard_nums[2])
	local multi = one * tonumber(self.item_upcard_multis[2])/10
	self.m_WHDrawOnceLabel:SetText(one)
	self.m_WHDrawFiveLabel:SetText(multi)
	self.m_WHDrawOnceIcon:SetSpriteName(string.format("%s", data.itemdata.OTHER[index].icon))
	self.m_WHDrawFiveIcon:SetSpriteName(string.format("%s", data.itemdata.OTHER[index].icon))

end
-- function CDrawMainPage.OnNormalBackMain(self)
-- 	self.m_WuHunCard:SetActive(true)
-- 	self.m_CardNangCao:SetActive(true)
-- 	self.m_WuLingCard:SetLocalPos(Vector3.New(-413.7, 4, 0))
-- 	self.m_NormalPart:SetActive(false)
-- 	self.m_HelpBtn:SetActive(true)
-- 	self.m_BackBtn:SetActive(false)
-- end

--Legendary  OnShowLegendaryPart
function CDrawMainPage:OnShowLegendaryPart()
	self.drawType = 3
	self.oneTimeFree = false 

	self.m_WHDrawOnceBtn = self.m_LegendaryPart:NewUI(1, CButton)
	self.m_WHDrawOnceLabel = self.m_LegendaryPart:NewUI(2, CLabel)
	self.m_WHDrawTenBtn = self.m_LegendaryPart:NewUI(3, CButton)
	self.m_WHDrawFiveLabel = self.m_LegendaryPart:NewUI(4, CLabel)
	self.m_WHDrawOnceBtn:AddUIEvent("click", callback(self, "OpenWuLing"))
	self.m_WHDrawTenBtn:AddUIEvent("click", callback(self, "OpenWuLingMore"))
	--self.m_WHDrawFreeLabel = self.m_LegendaryPart:NewUI(5, CLabel) --khong dung free
	-- self.m_WHDrawOnceBtn:AddUIEvent("click", callback(self, "OpenOneLegendary"))
	-- self.m_WHDrawTenBtn:AddUIEvent("click", callback(self, "OpenTenLegendary"))
	self.m_BackBtn:AddUIEvent("click", callback(self, "OnBackMain"))
	-- self.m_BackBtn:SetActive(false)

	self.m_WHDrawOnceIcon = self.m_NormalPart:NewUI(6, CSprite)
	self.m_WHDrawFiveIcon = self.m_NormalPart:NewUI(7, CSprite)

	self.m_WuHunCard:SetActive(false)
	self.m_WuLingCard:SetActive(false)
	self.m_CardNangCao:SetLocalPos(Vector3.New(7, 42.96321, 0))
	self.m_LegendaryPart:SetActive(true)
	self.m_HelpBtn:SetActive(false)
	self.m_BackBtn:SetActive(true)

	local index =tonumber(self.item_upcard_vals[3])
	local one =  tonumber(self.item_upcard_nums[3])
	local multi = one * tonumber(self.item_upcard_multis[3])/10
	self.m_WHDrawOnceLabel:SetText(one)
	self.m_WHDrawFiveLabel:SetText(multi)
	self.m_WHDrawOnceIcon:SetSpriteName(string.format("%s", data.itemdata.OTHER[index].icon))
	self.m_WHDrawFiveIcon:SetSpriteName(string.format("%s", data.itemdata.OTHER[index].icon))
end

-- function CDrawMainPage.OnLegendaryBackMain(self)
-- 	self.m_WuHunCard:SetActive(true)
-- 	self.m_WuLingCard:SetActive(true)
-- 	self.m_CardNangCao:SetLocalPos(Vector3.New(414.9, 42, 0))
-- 	self.m_LegendaryPart:SetActive(false)
-- 	self.m_HelpBtn:SetActive(true)
-- 	self.m_BackBtn:SetActive(false)
-- end
-- function CDrawMainPage.ShowWuHunTip(self)
-- 	g_GuideCtrl:ReqTipsGuideFinish("draw_wh_card")
-- 	if not g_NetCtrl:IsValidSession(netdefines.C2GS_BY_NAME["C2GSDrawWuHunCard"], 5) then
-- 		return
-- 	end	
	
-- 	self.m_UseSSRItem = 0
-- 	if g_ItemCtrl:GetBagItemAmountBySid(10019) + g_ItemCtrl:GetBagItemAmountBySid(10018) <= 0 or CGuideView:GetView() ~= nil then
-- 		self:ShowWuHunTip2()
-- 	else
-- 		CDrawSelectView:ShowView(function (oView)
-- 			oView:SetCallBack(callback(self, "OpenWuHun"))
-- 		end)
-- 	end
-- end

-- function CDrawMainPage.OnDrawFiveWuHun(self)
	-- if g_WindowTipCtrl:IsShowTips("draw_five_tip") then
	-- 	local windowConfirmInfo = {
	-- 		msg				= string.format("Your Royal Covenant is not enough, does it cost #w2%d to recruit?", g_PartnerCtrl:GetChoukaMulCost()),
	-- 		okCallback		= callback(self, "OnDrawFiveAction"),
	-- 		selectdata		={
	-- 			text = "No more reminders today",
	-- 			CallBack = callback(g_WindowTipCtrl, "SetTodayTip", "draw_five_tip")
	-- 		},
	-- 	}
	-- 	g_WindowTipCtrl:SetWindowConfirm(windowConfirmInfo)
	-- else
		-- self:OnDrawFiveAction()
	--end
-- end
-- function CDrawMainPage.OnDrawFiveAction(self)
-- 	local bUp = self.m_JoinUpBtn:GetSelected()
-- 	local iSend = bUp and 1 or 0
-- 	local istate = IOTools.GetRoleData("chouka_bullet") or 1
-- 	netpartner.C2GSDrawWuHunCard(2, 0, 1, 10)
-- end

-- function CDrawMainPage.ShowWuHunTip2(self)
-- 	local isfree = g_PartnerCtrl:IsChoukaFree()
-- 	if g_ItemCtrl:GetBagItemAmountBySid(10021) < 1 and not isfree and g_WindowTipCtrl:IsShowTips("draw_whcard_tip") then
-- 		local windowConfirmInfo = {
-- 			msg				= string.format("Your Royal Covenant is not enough, does it cost #w2%d to recruit?", g_PartnerCtrl:GetChoukaCost()),
-- 			okCallback		= callback(self, "OpenWuHun"),
-- 			selectdata		={
-- 				text = "No more reminders today",
-- 				CallBack = callback(g_WindowTipCtrl, "SetTodayTip", "draw_whcard_tip")
-- 			},
-- 		}
-- 		g_WindowTipCtrl:SetWindowConfirm(windowConfirmInfo)
-- 	else
-- 		self:OpenWuHun()
-- 	end
-- end

function CDrawMainPage.OpenWuLingChance(self)
	CLuckyChanceView:ShowView()
end

function CDrawMainPage.OpenWuHunChance(self)
	CLuckyChanceView:ShowView()
end


function CDrawMainPage:GetLeftTimeStrByEndTime(leftTime)
	local str = ""
	-- local time = g_TimeCtrl:GetTimeS()		
	-- local leftTime = endTime - time - 60 * 25
	if leftTime > 0 then
		-- printDebug(leftTime) --1803711
		str = self:ConverTimeString(leftTime)
	end
	return str	
end

function CDrawMainPage:ConverTimeString(time)
	local str = ""
	time = tonumber(time)
	if time	>= 0 then
		local days = math.floor(time / (24*3600))
		time = time % (24*3600)
		local h = math.floor(time / 3600)
		time = time % (3600)
		local min = math.floor(time / 60)
		local sec = time % 60
		-- local h = math.floor(time / 3600)
		-- local min = math.floor(time % 3600 / 60)
		-- local sec = time % 60
		if (days<1) then 
			str = string.format("Free in %02d:%02d:%02d", h,min,sec)
		else
			str = string.format("Free in %02d days\n%02d:%02d:%02d", days, h,min,sec)
		end
	end
	return str
end

return CDrawMainPage