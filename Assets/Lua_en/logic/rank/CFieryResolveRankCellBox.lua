local CFieryResolveRankCellBox = class("CFieryResolveRankCellBox", CBox)

function CFieryResolveRankCellBox.ctor(self, ob)
	CBox.ctor(self, ob)
	self:InitContent()
end

function CFieryResolveRankCellBox.InitContent(self)
	self.m_infobar_name = self:NewUI(1, CLabel)
	self.m_infobar_level = self:NewUI(2, CLabel)
	self.m_infobar_cp = self:NewUI(3, CLabel)
	self.m_infobar_rank = self:NewUI(4, CSprite)
	self.m_infobar_rank_label = self:NewUI(5, CLabel)
	self.m_infobar_avatar = self:NewUI(6, CSprite)
	self.m_ItemBox = self:NewUI(7, CItemRewardBox)
	self.m_ItemGrid = self:NewUI(8, CGrid)


	self.m_ItemBox:SetActive(false)
	self.m_ItemList = {}
	self.m_ItemGrid:Clear()

end

function CFieryResolveRankCellBox.UpdateTargetItem(self)
	local dData = self.rewardData
	for i,v in ipairs(self.m_ItemList) do
		if v then
			v:SetActive(false)
		end
	end
	local lRewardItem = dData.weeky_award
	for i,v in ipairs(lRewardItem) do
		local box = self.m_ItemList[i]
		if box then
			box:SetActive(true)
		else
			box = self.m_ItemBox:Clone()
			box:SetActive(true)
			table.insert(self.m_ItemList, box)
			self.m_ItemGrid:AddChild(box)
		end
		box:SetItemBySid(v.id, v.num)
		self.m_ItemGrid:AddChild(box)
	end
	self.m_ItemGrid:Reposition()
end

function CFieryResolveRankCellBox.SetData(self, oData, index, iSub)
	self.m_infobar_level:SetText("Recharged: " .. oData.rechargediamond)
	self.m_infobar_cp:SetText("")
	self.m_infobar_avatar:SetSpriteName(tostring(oData.shape))
	if (oData.rank < 6) then
		self.m_infobar_rank:SetSpriteName(string.format("pic_topevent_%d", oData.rank))
		self.m_infobar_rank_label:SetText("")
		
	else
		self.m_infobar_rank:SetSpriteName("pic_topevent_defaut")
		self.m_infobar_rank_label:SetText(oData.rank)
	end
	self.m_infobar_name:SetText(oData.name)
	self.rewardIndex = nil
	if (oData.rank == 1) then 
		self.rewardIndex = 1
	elseif (oData.rank == 2) or (oData.rank == 3) then 
		self.rewardIndex = 3
	elseif (oData.rank >= 4) and (oData.rank <= 10) then 
		self.rewardIndex = 10
	elseif (oData.rank >= 11) and (oData.rank <= 20) then 
		self.rewardIndex = 20
	elseif (oData.rank >= 21) and (oData.rank <= 50) then 
		self.rewardIndex = 50
	end

	-- {
	-- 	desc = "Top 4-10",
	-- 	id = 10,
	-- 	reward = { 1003 },
	-- 	weeky_award = { {
	-- 		id = 1009,
	-- 		num = 200
	-- 	  } }
	-- }

	if self.rewardIndex ~=nil then 
		self.rewardData = data.rechargediamonddata.Reward[self.rewardIndex]
		self:UpdateTargetItem()
	end
end

return CFieryResolveRankCellBox
