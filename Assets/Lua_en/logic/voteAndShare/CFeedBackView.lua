local CFeedBackView = class("CFeedBackView",CViewBase)
    function CFeedBackView.ctor(self, cb)
        CViewBase.ctor(self, "UI/VoteAndShare/FeedbackView.prefab", cb)
        
        self.m_GroupName = "main"
        self.m_ExtendClose = "Black"
        self.m_OpenEffect = "Scale"
        self.m_IsAlwaysShow = true
    end
    function CFeedBackView.OnCreateView(self)
        --printDebug("FeedbackView click")
         self.m_btnClose = self:NewUI(1,CSprite)
         self.m_NameVote = self:NewUI(2,CLabel)
         self.m_DateVote = self:NewUI(3,CLabel)
         self.m_TieuDe = self:NewUI(4, CInput)
         self.m_Content = self:NewUI(5, CInput)
         self.m_ButtonSend = self:NewUI(6, CButton)
        -- self.m_VoteCellBox :SetActive(false)
        -- self.m_ScrollWidget = self:NewUI(3, CWidget)
        -- self.m_ScrollView = self:NewUI(4,CRecyclingScrollView)
         self.m_btnClose:AddUIEvent("click", callback(self, "OnCloseView"))
         self.m_ButtonSend:AddUIEvent("click", callback(self, "OnSendMsg"))
        --self:SetDataScroll()
        self:InitContent()
    end
    function CFeedBackView.InitContent(self)
        self.m_NameVote:SetText(g_AttrCtrl.name)
        
        self.m_DateVote:SetText(os.date("%d/%m/%Y %H:%M", g_TimeCtrl:GetTimeS()))
       
    end
    function  CFeedBackView:OnCloseView()
        -- body
        self:CloseView()
    end

    function  CFeedBackView:OnSendMsg()
        --printDebug(self.m_Content:GetText())
        self:OnCloseView();
        g_CVoteAndShareCtrl:ShowFeedBackDone()
        -- body
    end
return CFeedBackView