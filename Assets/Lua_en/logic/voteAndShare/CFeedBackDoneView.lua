local CFeedBackDoneView = class("CFeedBackDoneView",CViewBase)

    function CFeedBackDoneView.ctor(self, cb)
        CViewBase.ctor(self, "UI/VoteAndShare/FeedbackDoneView.prefab", cb)
        
        self.m_GroupName = "main"
        self.m_ExtendClose = "Black"
        self.m_OpenEffect = "Scale"
        self.m_IsAlwaysShow = true
    end
    function CFeedBackDoneView.OnCreateView(self)
        --printDebug("FeedbackView click")
        self.m_btnClose = self:NewUI(1,CSprite)
        
        -- self.m_ScrollWidget = self:NewUI(3, CWidget)
        -- self.m_ScrollView = self:NewUI(4,CRecyclingScrollView)
        self.m_btnClose:AddUIEvent("click", callback(self, "OnCloseView"))
        
    end
    
    function  CFeedBackDoneView:OnCloseView()
        -- body
        self:CloseView()
    end

    
return CFeedBackDoneView