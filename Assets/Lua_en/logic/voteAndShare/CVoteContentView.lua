local CVoteContentView = class("CVoteContentView", CViewBase)

function CVoteContentView.ctor(self, cb)
	CViewBase.ctor(self, "UI/VoteAndShare/VoteContentView.prefab", cb)
	self.m_ExtendClose = "Black"
	self.m_DepthType = "Dialog"
end
function CVoteContentView.OnCreateView(self)
	 self.m_CloseBtn = self:NewUI(1, CButton)
     self.m_ContentLabel= self:NewUI(2,CLabel)
     self.m_SendName= self:NewUI(5,CLabel)
     --printDebug(self.m_ContentLabel)
	-- self.m_NormalPart = self:NewUI(2, CBox)
	-- self.m_BG = self:NewUI(3, CSprite)
	-- self.m_PicTipsWidget = self:NewUI(4, CBox)
	-- self.m_PicTipsButton = self:NewUI(5, CBox)
	self:InitContent()
end

function CVoteContentView.InitContent(self)
    
	-- self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))
	--self:InitNormal()
end
function CVoteContentView.ShowContent(self, use_send,content)
    --printDebug(content)
    self.m_ContentLabel:SetText(content)
    self.m_SendName:SetText(use_send)
end

return CVoteContentView