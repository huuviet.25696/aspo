local CVoteView = class("CVoteView",CViewBase)

function CVoteView.ctor(self, cb)
	CViewBase.ctor(self, "UI/VoteAndShare/VoteView.prefab", cb)
	
	self.m_GroupName = "main"
	self.m_ExtendClose = "Black"
	self.m_OpenEffect = "Scale"
	self.m_IsAlwaysShow = true
end
function CVoteView.OnCreateView(self)
    --printDebug("Vote and share view click")
    self.m_btnClose = self:NewUI(1,CSprite)
    self.m_VoteCellBox = self:NewUI(2,CVoteCellBox)
    self.m_VoteCellBox :SetActive(false)
    self.m_ScrollWidget = self:NewUI(3, CWidget)
    self.m_ScrollView = self:NewUI(4,CRecyclingScrollView)
    self.m_btnClose:AddUIEvent("click", callback(self, "OnCloseView"))
    self:SetDataScroll()
end

function CVoteView.InitContent(self)
    CVoteView:ShowView(function ( oView )
        -- body
        --printDebug("Show Vote View")
        
    end)
end

function CVoteView.SetDataScroll( self )
    self.m_ScrollView:SetData(self.m_ScrollWidget, 1, self.m_VoteCellBox, callback(self, "CellInit"), callback(self, "CellSetData"))
    -- body
end
function CVoteView.CellSetData(self, oBox, index)
	return oBox:SetData(nil, index, nil)
end
function CVoteView.CellInit(self, oBox)
	oBox.m_ParentView = self
    oBox:SetActive(true)
end
function  CVoteView:OnCloseView()
    -- body
    self:CloseView()
end
return CVoteView