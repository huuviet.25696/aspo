local CVoteAndShareCtr = class("CVoteAndShareCtr",CCtrlBase)

function CVoteAndShareCtr.ctor (self)
    CCtrlBase.ctor(self)
    self.m_IsVote= false
    self:ResetCtrl()
end

function CVoteAndShareCtr.ResetCtrl(self)
	
	self:Clear()
end

function CVoteAndShareCtr.Clear(self)
	-- self.m_NeedRefresh = false
	-- self.m_RankCount = {}
	-- self.m_NextRefreshTime = {}
	-- self.m_PlayerRankInfo = {}
	-- self.m_NetDataDic = {}
	-- self.m_NetExtraDataDic = {}
	-- self.m_NetExtraDataRecord = {}
	-- self.m_DefaultRankPartner = nil
end

function CVoteAndShareCtr.IsVote(self)
	return self.m_IsVote
end

function CVoteAndShareCtr:ShowContentVote( self,use_send,content)
    -- body
    CVoteContentView:ShowView(function ( oView )
        oView:ShowContent(self,use_send,content)
        -- body
    end)
    
end

function CVoteAndShareCtr.ShowListVote( self)
    -- body
    CVoteView:ShowView()
end

function CVoteAndShareCtr.ShowFeedBack( self)
    -- body
    CFeedBackView:ShowView()
end
function CVoteAndShareCtr.ShowFeedBackDone( self)
    -- body
    CFeedBackDoneViewView:ShowView()
end

function CVoteAndShareCtr.SetStatusVote( self,isVote)
    -- body
    self.m_IsVote = isVote
    self:OnEvent(define.VoteAndShare.Event.SwapVoteOrFeedBack)
end

return CVoteAndShareCtr