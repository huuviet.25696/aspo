local CVipMainView = class("CVipMainView", CViewBase)

function CVipMainView.ctor(self, cb)
	CViewBase.ctor(self, "UI/MainMenu/VipMainView.prefab", cb)
	self.m_DepthType = "Dialog"
	self.m_GroupName = "main"
	self.m_ExtendClose = "Black"
	self.m_OpenEffect = "Scale"
end

function CVipMainView.OnCreateView(self)
	self.m_CloseBtn = self:NewUI(1, CButton)
	self.m_SideBtnGrid = self:NewUI(2, CGrid)
	self.m_VipPage = self:NewPage(3, CVipPage)
	self.m_YueKaPage = self:NewPage(4, CVipDayByDayPage)
	self.m_ZskPage = self:NewPage(5, CVipTettraBlessingPage)
	self:InitContent()
end

function CVipMainView.InitContent(self)
	self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))
	self.m_BtnList = {}
	self.m_SideBtnGrid:InitChild(function (obj, index)
		local oBtn = CButton.New(obj)
		oBtn.m_Idx = index
		oBtn:SetGroup(self.m_SideBtnGrid:GetInstanceID())
		oBtn:AddUIEvent("click", callback(self, "OnSwitchPage", index))
		self.m_BtnList[index] = oBtn
		return oBtn
	end)
	local oBtn = self.m_SideBtnGrid:GetChild(1)
	self:OnSwitchPage(oBtn.m_Idx)
	self:UpdateShow()
end
function CVipMainView.OnSwitchPage(self, index)
	if index == 1 then
		self:ShowVipPage()
	elseif index == 2 then
		self:ShowYueKaPagePage()
	elseif index == 3 then
		self:ShowZskPage()
	end
	if self.m_BtnList[index] then
		self.m_BtnList[index]:SetSelected(true)
	end
end
function CVipMainView.ShowVipPage(self)
	self:ShowSubPage(self.m_VipPage)
end
---day by day
function CVipMainView.ShowYueKaPagePage(self)
	self:ShowSubPage(self.m_YueKaPage)
end
---- Tettra Blessing
function CVipMainView.ShowZskPage(self)
	self:ShowSubPage(self.m_ZskPage)
end
function CVipMainView.UpdateShow(self)
	self:RefreshRedDot()
	-- 	self.m_BtnList[1]:SetActive(true)
	-- self.m_SideBtnGrid:Reposition()
end

function CVipMainView.RefreshRedDot(self)
	if g_WelfareCtrl:IsYueKaRedDot() then
		self.m_BtnList[2]:AddEffect("RedDot")
	else
		self.m_BtnList[2]:DelEffect("RedDot")
	end
	if g_WelfareCtrl:IsZskPageRedDot() then
		self.m_BtnList[3]:AddEffect("RedDot")
	else
		self.m_BtnList[3]:DelEffect("RedDot")
	end
	
end


return CVipMainView