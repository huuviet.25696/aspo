local CVipPage = class("CVipPage", CPageBase)

function CVipPage.ctor(self, ob)
	CPageBase.ctor(self, ob)
end

function CVipPage.OnInitPage(self)
	self.m_ATKLabel = self:NewUI(1, CLabel)
	self.m_DEFLabel = self:NewUI(2, CLabel)
	self.m_HPLabel = self:NewUI(3, CLabel)
	self.m_Vip7days = self:NewUI(4, CButton)
	self.m_Vip30days = self:NewUI(5, CButton)
	self.m_Price7days = self:NewUI(6, CLabel)
	self.m_Price30days = self:NewUI(7, CLabel)

	self.m_Vip7days:AddUIEvent("click", callback(self, "OnBuyVIP7"))
	self.m_Vip30days:AddUIEvent("click", callback(self, "OnBuyVIP30"))
	self:InitContent()
end

function CVipPage.InitContent(self)
	self:RefreshVipPrice(define.Store.Page.LimitSkin)
end

function CVipPage.RefreshVipPrice(self,shopId)
	local goodsPosListFull = data.npcstoredata.GoodsDataSort[shopId] -- g_NpcShopCtrl:GetGoodsPosList(shopId)
	local goodsPosList ={}
	if shopId ~= nil and goodsPosListFull ~= nil then
		for k,v in ipairs(goodsPosListFull) do
			-- local itemData = g_NpcShopCtrl:GetCommonGoodsData(g_NpcShopCtrl:GetGoodsInfo(v).item_id)
			-- local storeData = data.npcstoredata.DATA[itemData.id]
			local storeData = data.npcstoredata.DATA[v]
			if (storeData.event_type == "7day" or storeData.event_type == "30day" ) then
				table.insert(goodsPosList,v)
				-- goodVip = storeData
			end
		end
	end
	if shopId ~= nil and goodsPosList ~= nil then
		for k,v in ipairs(goodsPosList) do
			local itemData = g_NpcShopCtrl:GetCommonGoodsData(v)
			local storeData = data.npcstoredata.DATA[itemData.id]
			local goodVip = storeData
			if(goodVip ~=nil) then 
				if shopId == define.Store.Page.LimitSkin then
					self.nemo_id = goodVip.id
					self.nemo_payid = goodVip.payid
					if(goodVip.event_type == "7day") then
						self.nemo_id_7days = goodVip.id
						self.nemo_payid_7days = goodVip.payid
						self.m_Price7days:SetText(goodVip.coin_count/10000)
					end
					if(goodVip.event_type == "30day") then
						self.nemo_id_30days = goodVip.id
						self.nemo_payid_30days = goodVip.payid
						self.m_Price30days:SetText(goodVip.coin_count/10000)
						-- printDebug("itemData",itemData)
						local Description = itemData.exData.description -- "{attack=150,defense=150,maxhp=1000}" -- 
						local eval = function(s)
							return assert(loadstring("return " .. s))()
						end
						local info = eval(Description)
						self.m_ATKLabel:SetText(info["attack"] or "-")
						self.m_DEFLabel:SetText(info["defense"] or "-")
						self.m_HPLabel:SetText(info["maxhp"] or "-")
					end

					
				end
				-- if shopId == define.Store.Page.LiBaoShop then
				-- 	-- netother.C2GSRequestPay(self.iap_payid, self.iap_id)
				-- 	self.iap_id = goodVip.id
				-- 	self.iap_payid = goodVip.payid
				-- 	if Utils.IsIOS() then
				-- 		self.iap_payid = goodVip.iospayid
				-- 	end
				-- 	self.m_IAPPrice:SetText(goodVip.coin_count/100 .. "$")
				-- end
			end
		end
	end



	-- local info = g_ItemCtrl:GetBuffMedalData().apply_info
	-- if (not next(info)) then 
	-- 	local Des = itemData.description
	-- end
	-- printDebug(info)
	-- for k,v in ipairs(info) do
	-- 	printDebug(v)
	-- end

end

function CVipPage.OnBuyVIP7(self)
	printDebug(self.nemo_payid_7days,self.nemo_id_7days)
	if Utils.IsDevUser() and Utils.IsEditor() then
		netother.C2GSGMCmd(string.format("huodong charge 1003 212026"))
		g_NotifyCtrl:FloatMsg("Directly call the GM commands, the operation is prone to high risk!!! For testing only")
	else
			local args = 
					{
						msg = "Buy this package with NEMO?",
						okCallback = function ()
								netother.C2GSRequestPay(self.nemo_payid_7days, self.nemo_id_7days)
								g_NotifyCtrl:FloatMsg("The request is under processing and will be updated when the transaction is finished.")
						end,
						cancelCallback = function ()
						end,
						okStr = "Yes",
						cancelStr = "No",
					}
			g_WindowTipCtrl:SetWindowConfirm(args)
	end
end
function CVipPage:OnBuyVIP30()
	printDebug(self.nemo_payid_30days,self.nemo_id_30days)
	if Utils.IsDevUser() and Utils.IsEditor() then
		netother.C2GSGMCmd(string.format("huodong charge 1003 212027"))
		g_NotifyCtrl:FloatMsg("Directly call the GM commands, the operation is prone to high risk!!! For testing only")
	else
			local args = 
					{
						msg = "Buy this package with NEMO?",
						okCallback = function ()
								netother.C2GSRequestPay(self.nemo_payid_30days, self.nemo_id_30days)
								g_NotifyCtrl:FloatMsg("The request is under processing and will be updated when the transaction is finished.")
						end,
						cancelCallback = function ()
						end,
						okStr = "Yes",
						cancelStr = "No",
					}
			g_WindowTipCtrl:SetWindowConfirm(args)
	end
end

return CVipPage