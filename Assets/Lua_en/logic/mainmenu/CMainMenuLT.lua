local CMainMenuLT = class("CMainMenuLT", CBox)

function CMainMenuLT.ctor(self, obj)
	CBox.ctor(self, obj)
	self.m_HeroBox = self:NewUI(1, CBox)
	-- HeroBox
	self.m_HeroBox.m_Avatar = self.m_HeroBox:NewUI(1, CSprite)
	self.m_HeroBox.m_GradeLabel = self.m_HeroBox:NewUI(2, CLabel)
	self.m_HeroBox.m_MainMenuBuffBox = self.m_HeroBox:NewUI(3, CMainMenuBuffBox)
	self.m_HeroBox.m_HeroPowLabel = self.m_HeroBox:NewUI(4, CLabel)
	self.m_HeroBox.m_HeroVipSprite = self.m_HeroBox:NewUI(5, CSprite)
	self.m_HeroBox.m_HeroVipLevelLabel = self.m_HeroBox:NewUI(6, CLabel)
	self.m_HeroBox.m_HeroNameLabel = self.m_HeroBox:NewUI(7, CLabel)

	self.m_ExpandBox = self:NewUI(2, CMainMenuExpandBox)
	self.m_TeamBox = self:NewUI(3, CMainMenuTeamBox)
	self.m_FriendBtn = self:NewUI(4, CSprite)
	self.m_MsgLabel = self:NewUI(5, CLabel)
	self.m_ExpSlider = self:NewUI(6, CSlider)
	self.m_ExpCurLabel = self:NewUI(7, CLabel)
	self.m_ExpNextLabel = self:NewUI(8, CLabel)
	self.m_ExpDivLabel = self:NewUI(9, CLabel)
	self.m_ExpGroup = self:NewUI(10, CBox)
	self.m_ExpHideLabel = self:NewUI(11, CLabel)
	self.m_VipBtn = self:NewUI(12, CButton)

	self.m_HeroBox.m_IgnoreCheckEffect = true
	self:InitContent()
end

function CMainMenuLT.InitContent(self)
	g_AttrCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnAttrEvent"))
	g_TaskCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnTaskEvent"))
	g_TalkCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnTalkEvent"))
	g_MailCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnMailEvent"))
	g_FriendCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnFriendEvent"))
	g_ActivityCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlActivityEvent"))
	g_EquipFubenCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlEquipFbEvent"))
	g_MapCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlMapEvent"))
	g_AnLeiCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlAnLeilEvent"))		
	g_FieldBossCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnFieldBossEvent"))
	g_ConvoyCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnConvoyEvent"))
	self.m_FriendBtn:AddUIEvent("click", callback(self, "OpenFriendInfoView"))
	self.m_HeroBox:AddUIEvent("click", callback(self, "OnShowAttr"))
	self.m_ExpHideLabel:AddUIEvent("click", callback(self, "OnExpAll"))
	self.m_ExpGroup:AddUIEvent("click", callback(self, "OnExpPercent"))
	self.m_VipBtn:AddUIEvent("click", callback(self, "OnOpenVipMainView"))
	self:UpdateMsgAmount()
	self:CheckOpenGrade()
	self:RefreshHero()
	self:RefreshButton()
	self:CheckHeroBoxRedDot()
	self:RefrehExp()
end

function CMainMenuLT.Destroy(self)
	CViewBase.Destroy(self)
end

function CMainMenuLT.CheckOpenGrade(self)
	self.m_FriendBtn:SetActive(g_FriendCtrl:IsOpen())	
end

function CMainMenuLT.OnAttrEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Attr.Event.Change then
		self:DelayCall(0, "CheckOpenGrade")
		self:DelayCall(0, "RefreshHero")
		self:DelayCall(0, "RefreshButton")
		local data = oCtrl.m_EventData
		self:RefrehExp(data.dPreAttr, data.dAttr)
	elseif oCtrl.m_EventID == define.Attr.Event.UpdateSkin then
		self:CheckHeroBoxRedDot()
	end
	
end
function CMainMenuLT.RefrehExp(self, preData, curData)
	-- print("当前角色经验："..g_AttrCtrl:GetCurGradeExp().." 升级经验："..g_AttrCtrl:GetUpgradeExp())
	-- TODO:寫得好長，回頭拆分
	if not curData or not curData.exp then
		self.m_ExpSlider:SetValue(g_AttrCtrl:GetCurGradeExp()/g_AttrCtrl:GetUpgradeExp())		
		self.m_ExpCurLabel:SetText(tostring( math.floor(g_AttrCtrl:GetCurGradeExp())))
		self.m_ExpNextLabel:SetText(tostring( math.floor(g_AttrCtrl:GetUpgradeExp())))
		self.m_ExpHideLabel:SetText(string.format("%d",math.floor((g_AttrCtrl:GetCurGradeExp()))/(g_AttrCtrl:GetUpgradeExp())*100))
		if(math.floor((g_AttrCtrl:GetCurGradeExp()))/(g_AttrCtrl:GetUpgradeExp())>=1) then 
			self.m_ExpHideLabel:SetText("100")
		end 
	else
		if not curData.grade then
			curData.grade = g_AttrCtrl.grade
			preData.grade = g_AttrCtrl.grade
		end
		local preExpinfo = data.upgradedata.DATA[preData.grade]
		local nextExpinfo = data.upgradedata.DATA[preData.grade + 1]
		if not preExpinfo then		--等级为0的特殊处理
			preExpinfo = nextExpinfo
			preExpinfo.sum_player_exp = 0
		end
		if not nextExpinfo then		--满级的特殊处理
			self.m_ExpSlider:SetValue(g_AttrCtrl:GetCurGradeExp()/g_AttrCtrl:GetUpgradeExp())			
			self.m_ExpCurLabel:SetText(tostring( math.floor(g_AttrCtrl:GetCurGradeExp())))
			self.m_ExpNextLabel:SetText(tostring( math.floor(g_AttrCtrl:GetUpgradeExp())))
			self.m_ExpHideLabel:SetText(string.format("%d",math.floor((g_AttrCtrl:GetCurGradeExp()))/(g_AttrCtrl:GetUpgradeExp())*100))
			if(math.floor((g_AttrCtrl:GetCurGradeExp()))/(g_AttrCtrl:GetUpgradeExp())>=1) then 
				self.m_ExpHideLabel:SetText("100")
			end 
			return
		end
		local curGrade = -1
		local remainExp = curData.exp - preData.exp
		local addExp = 0
		local curExp = 0

		local function updateExp(delta)
			if addExp >= 1 then				--更新规则：按每个等级的经验更新量折半添加
				curExp = addExp/4 + curExp
				self.m_ExpSlider:SetValue(curExp/nextExpinfo.player_exp)
				self.m_ExpCurLabel:SetText(tostring( math.floor(g_AttrCtrl:GetCurGradeExp())))
				self.m_ExpNextLabel:SetText(tostring( math.floor(g_AttrCtrl:GetUpgradeExp())))
				self.m_ExpHideLabel:SetText(string.format("%d",(math.floor(curExp/nextExpinfo.player_exp))*100))
				if((math.floor(curExp/nextExpinfo.player_exp))>=1) then 
					self.m_ExpHideLabel:SetText("100")
				end 
				addExp = addExp - addExp/4
			else
				if remainExp <= 0 then
					self.m_ExpSlider:SetValue(g_AttrCtrl:GetCurGradeExp()/g_AttrCtrl:GetUpgradeExp())
					self.m_ExpHideLabel:SetText(string.format("%d",math.floor((g_AttrCtrl:GetCurGradeExp()))/(g_AttrCtrl:GetUpgradeExp())*100))	
					if(math.floor((g_AttrCtrl:GetCurGradeExp()))/(g_AttrCtrl:GetUpgradeExp())>=1) then 
						self.m_ExpHideLabel:SetText("100")
					end 
					return false
				end
				if curGrade > 0 then
					curExp = 0
				else
					curGrade = preData.grade
					curExp = preData.exp - preExpinfo.sum_player_exp
				end
				preExpinfo = data.upgradedata.DATA[curGrade]
				nextExpinfo = data.upgradedata.DATA[curGrade + 1]
				curGrade = curGrade + 1
				if curExp + remainExp <= nextExpinfo.player_exp then
					addExp = remainExp
					remainExp = 0
				else
					addExp = nextExpinfo.player_exp - curExp
					remainExp = curExp + remainExp - nextExpinfo.player_exp
				end
			end
			return true
		end
		Utils.AddTimer(updateExp, 0.05, 0)
	end
end
function CMainMenuLT.OnExpAll(self)
	self.m_ExpGroup:SetActive(true)
	self.m_ExpHideLabel:SetActive(false)
end
function CMainMenuLT.OnExpPercent(self)
	self.m_ExpGroup:SetActive(false)
	self.m_ExpHideLabel:SetActive(true)
end
function CMainMenuLT.OnTalkEvent(self, oCtrl)
	self:DelayCall(0, "UpdateMsgAmount", "talk")
end

function CMainMenuLT.OnFriendEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Friend.Event.UpdateApply then
		self:DelayCall(0, "UpdateMsgAmount", "apply")
	end 
end

function CMainMenuLT.OnTaskEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Task.Event.RefreshAllTaskBox then
		self:DelayCall(0, "CheckOpenGrade")
		self:DelayCall(0, "RefreshButton")
	end
end

function CMainMenuLT.OnMailEvent(self, oCtrl)
	self:DelayCall(0, "UpdateMsgAmount", "mail")
end

function CMainMenuLT.UpdateMsgAmount(self, sType)
	local function getStr(str)
		if str == "talk" then
			return "You have a new message"
		elseif str == "mail" then
			return "You have new mail"
		elseif str == "apply" then
			return "You have a new request"
		end
		return ""
	end
	local dAmount = {}
	dAmount["talk"] = g_TalkCtrl:GetTotalNotify()
	dAmount["apply"] = g_FriendCtrl:GetApplyAmount()
	dAmount["mail"] = g_MailCtrl:GetUnOpenMailAmount()
	if dAmount["talk"] + dAmount["apply"] + dAmount["mail"] > 0 then
		self.m_MsgLabel:SetActive(true)
		if sType and dAmount[sType] > 0 then
			self.m_MsgLabel:SetText(getStr(sType))
		else
			for _, key in ipairs({"talk", "mail", "apply"}) do
				if dAmount[key] and dAmount[key] > 0 then
					self.m_MsgLabel:SetText(getStr(key))
					break
				end
			end
		end
	else
		self.m_MsgLabel:SetActive(false)
	end
end

function CMainMenuLT.OpenFriendInfoView(self)
	CFriendMainView:ShowView()
end
function CMainMenuLT.OnOpenVipMainView(self)
	CVipMainView:ShowView()
end

function CMainMenuLT.RefreshHero(self)
	self.m_HeroBox.m_Avatar:SpriteMainMenuAvatarBig(g_AttrCtrl.model_info.shape)
	self.m_HeroBox.m_GradeLabel:SetText(g_AttrCtrl.grade)
	self.m_HeroBox.m_HeroNameLabel:SetText(g_AttrCtrl.name)
	self:RefreshHeroPower()
	if g_AttrCtrl.vip_level ~= 0 then
		self.m_HeroBox.m_HeroVipSprite:SetActive(true)
		self.m_HeroBox.m_HeroVipLevelLabel:SetText("V"..tostring(g_AttrCtrl.vip_level))
	else
		self.m_HeroBox.m_HeroVipSprite:SetActive(false)
	end

	-- self.m_VipBtn:SetText("VIP ".. g_AttrCtrl.vip_level)
end

function CMainMenuLT.RefreshHeroPower(self)
	self.m_HeroBox.m_HeroPowLabel:SetText(g_AttrCtrl:GetTotalPower())
end

function CMainMenuLT.OnShowAttr(self)
	CAttrMainView:ShowView()
end

function CMainMenuLT.RefreshButton(self)
	self.m_TeamBox:SetActive(g_ActivityCtrl:IsActivityVisibleBlock("team"))
end

function CMainMenuLT.OnCtrlActivityEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Activity.Event.DCAddTeam then
		self:DelayCall(0, "RefreshButton")
	elseif oCtrl.m_EventID == define.Activity.Event.DCLeaveTeam then
		self:DelayCall(0, "RefreshButton")

	elseif oCtrl.m_EventID == define.Activity.Event.DCUpdateTeam then
		self:DelayCall(0, "RefreshButton")

	elseif oCtrl.m_EventID == define.Activity.Event.DCRefreshTask then
		self:DelayCall(0, "RefreshButton")
	end
end

function CMainMenuLT.OnCtrlEquipFbEvent(self, oCtrl)
	if oCtrl.m_EventID == define.EquipFb.Event.BeginFb then
		self:DelayCall(0, "RefreshButton")

	elseif oCtrl.m_EventID == define.EquipFb.Event.EndFb then
		self:DelayCall(0, "RefreshButton")

	elseif oCtrl.m_EventID == define.EquipFb.Event.CompleteFB then
		self:DelayCall(0, "RefreshButton")
	end
end

function CMainMenuLT.OnCtrlMapEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Map.Event.ShowScene then
		self:DelayCall(0, "RefreshButton")		
	elseif oCtrl.m_EventID == define.Map.Event.MapLoadDone then
		self:DelayCall(0, "RefreshButton")
	elseif oCtrl.m_EventID == define.Map.Event.EnterScene then
	 	self:DelayCall(0, "RefreshButton")
	end	
end

function CMainMenuLT.OnCtrlAnLeilEvent( self, oCtrl)
	if oCtrl.m_EventID == define.AnLei.Event.BeginPatrol then
		self:DelayCall(0, "RefreshButton")
	elseif oCtrl.m_EventID == define.AnLei.Event.EndPatrol then
		self:DelayCall(0, "RefreshButton")	
	end
end

function CMainMenuLT.OnFieldBossEvent(self, oCtrl)
	if oCtrl.m_EventID == define.FieldBoss.Event.UpadteBossList then
		self:DelayCall(0, "RefreshButton")
	end
end

function CMainMenuLT.OnConvoyEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Convoy.Event.UpdateConvoyInfo then
		self:DelayCall(0, "RefreshButton")
	end
end

function CMainMenuLT.CheckHeroBoxRedDot(self)
	local redDot = g_AttrCtrl:GetSkinRedDot()
	if redDot and #redDot > 0 then
		self.m_HeroBox:AddEffect("RedDot")
	else
		self.m_HeroBox:DelEffect("RedDot")
	end
end

return CMainMenuLT