local CSpeechCtrl = class("CSpeechCtrl", CCtrlBase)

--语音
function CSpeechCtrl.ctor(self)
	CCtrlBase.ctor(self)
	-- self.m_AudioRecord = C_api.AudioRecord.Instance
	self.m_Sepeechs = {}
	self.m_PlayList = {}
	self.m_TranslateDone = {}
	self.m_WaitTranslate = {}
	self.m_PathKey = {}
	self.m_TranslateToken ="test_token"
	self.m_TokenGetting = false
	self.m_MaxTime = 10 -- 10 second of recording
	self.m_Audioviis = C_api.AuviisSDK.Instance
end

function CSpeechCtrl.GetRecordVolume(self)
	return 1
end

function CSpeechCtrl.InitCtrl(self)
end

function CSpeechCtrl.GetMaxTime(self)
	return self.m_MaxTime
end

function CSpeechCtrl.GetTokenFromLocal(self)
end

function CSpeechCtrl.GetTokenFormServer(self)

end

function CSpeechCtrl.GetTokenUrl(self)

end

function CSpeechCtrl.OnTokenGet(self, success, tResult)

end

function CSpeechCtrl.StartRecord(self, iMax)
	--printDebug("CSpeechCtrl.StartRecord")
	self.m_StartTime = g_TimeCtrl:GetTimeS()
	g_AudioCtrl:SetSlience()
	if self.m_Audioviis ~= nil then self.m_Audioviis:RecordVoice() end
end

function CSpeechCtrl.EndRecord(self)
	--printDebug("CSpeechCtrl.EndRecord")
	if self.m_Audioviis ~= nil then self.m_Audioviis:StopRecord(10) end
	g_AudioCtrl:ExitSlience()
	-- local sKey = nil
	-- local iTime = 0
	-- if self.m_StartTime then
	-- 	iTime = g_TimeCtrl:GetTimeS() - self.m_StartTime
	-- 	self.m_StartTime = nil
	-- else
	-- 	iTime = 10
	-- end
	-- sKey = Utils.NewGuid()
	-- if sKey then
	-- 	local dSpeech = self:GetSpeech(sKey)
	-- 	dSpeech["time"] = iTime
	-- end
end

function CSpeechCtrl.GetTestAmrPath(self)
	
end

function CSpeechCtrl.SaveToAmr(self, key)

end

function CSpeechCtrl.SaveToWav(self, key)
	
end

function CSpeechCtrl.UploadToServer(self, key, path, dUploadArgs)
	
end

function CSpeechCtrl.OnUploadResult(self, path, dUploadArgs, key, sucess)
	
end

function CSpeechCtrl.TranslateFromServer(self, key, filepath)
	
end

function CSpeechCtrl.OnTranslateResult(self, key, success, tResult)
	
end

function CSpeechCtrl.ProcessTranslateResult(self, sTranslate)
	sTranslate = string.gsub(sTranslate, ", ", "")
	return sTranslate
end

function CSpeechCtrl.DownloadFromServer(self, key, type, cb)
	
end

function CSpeechCtrl.OnDownloadResult(self, type, cb, key, www)
	
end

function CSpeechCtrl.CheckSendSpeech(self, key)
	
end

function CSpeechCtrl.GetSpeech(self, key)
	
end

function CSpeechCtrl.PlayWithKey(self, key)
	
end

function CSpeechCtrl.PlayWithPath(self, path)
	
end


function CSpeechCtrl.AddPlayWithPath(self, path, bFirst)
	
end

function CSpeechCtrl.IsPlay(self, key)
	
end

function CSpeechCtrl.AddPlayWithKey(self, key)
	
end

function CSpeechCtrl.ClearPlayList(self)
	self.m_PlayList = {}
end

function CSpeechCtrl.OnPlayEnd(self)
	
end

function CSpeechCtrl.PlayNext(self)
	
end

function CSpeechCtrl.SendTochannel(self, channel)
	local sMsg = self.m_Audioviis:getVoiceLink()
	g_ChatCtrl:SendMsg(sMsg, dSpeech.channel)
end
return CSpeechCtrl