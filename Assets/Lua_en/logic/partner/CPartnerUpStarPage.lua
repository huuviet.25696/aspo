local CPartnerUpStarPage = class("CPartnerUpStarPage", CPageBase)
local CSetBox = class("CSetBox", CBox)
CSysSettingView.CSetBox = CSetBox
function CPartnerUpStarPage.ctor(self, obj)
	CPageBase.ctor(self, obj)
end

function CSetBox.ctor(self, obj)
	CBox.ctor(self, obj)
	self.m_CheckSprite = self:NewUI(1, CSprite)   
	--self.m_Slider = self:NewUI(2, CSlider, false)     
	self.m_TipsBtn = self:NewUI(3, CButton, false)
	self.m_ShowRewardTimer = nil
end
function CSetBox.SetSelected(self, bSelect)
	self.m_CheckSprite:SetSelected(bSelect)
	--self:SetSliderEnabled(bSelect)
end
function CSetBox.GetSelected(self)
	local isEnabled = self.m_CheckSprite:GetSelected()
	--self:SetSliderEnabled(bSelect)
	return isEnabled
end

function CSetBox.SetCheckSpriteFunc(self, func)
	self.m_CheckSprite:AddUIEvent("click", function ()
		local isEnabled = self.m_CheckSprite:GetSelected()
		self:SetSliderEnabled(isEnabled)
		func(self.m_CheckSprite)
	end)
end

function CPartnerUpStarPage.OnInitPage(self)
	
	self.m_LStarGrid = self:NewUI(1, CGrid)
	self.m_RStarGrid = self:NewUI(2, CGrid)
	self.m_StarBox = self:NewUI(3, CBox)
	self.m_AttrPart = self:NewUI(4, CBox)
	self.m_ConfirmBtn = self:NewUI(5, CButton)
	self.m_NeedItemPart = self:NewUI(6, CBox)
	self.m_GoldLabel = self.m_NeedItemPart:NewUI(4, CLabel)
	self.m_Rate = self.m_NeedItemPart:NewUI(5, CLabel)
	self.m_TipLabel = self:NewUI(7, CLabel)
	self.m_TipBtn = self:NewUI(8, CButton)
	self.m_UpStarPart = self:NewUI(9, CObject)
	self.m_FullPart = self:NewUI(10, CObject)
	self.m_ItemStarHope = self:NewUI(14,CBox)
	self.m_ItemStarHope:AddUIEvent("click", function ()
		-- CHelpView:ShowView(function (oView)
		-- 	oView:ShowHelp("star_hope_upstar")
		-- end)
		CItemTipsSimpleInfoView:ShowView(function (oView)
			oView:SetInitBox("14999", nil, {})
			oView:ForceShowFindWayBox(true)
		end)
	end)
	self.m_Label2=	self.m_UpStarPart:Find("CostPart/Label (2)").transform
	local m_lbl2= self.m_Label2:GetComponent(classtype.UILabel)
	m_lbl2.text="Use Lucky Charm, success upgrade increases 2 stars, fail increases 1 star.\nGAS fee for each additional burnt Token is 5 Diamond."
	self.m_localItm = self:NewUI(11, CBox)
	self.m_ItemRequire = {}
	self.m_ItemRequire[1] = self.m_NeedItemPart:NewUI(1,CItemUpgradeStar)
	self.m_ItemRequire[2] = self.m_NeedItemPart:NewUI(2, CItemUpgradeStar)
	self.m_ItemRequire[3] = self.m_NeedItemPart:NewUI(3, CItemUpgradeStar)
	for i =1,3,1 do
		self.m_ItemRequire[i]:AddUIEvent("click", function ()
			local cost_partner = data.partnerdata.UPSTAR[self.m_Star].cost_partner
			if i == 3 and cost_partner < 3 then
				-- self.m_ItemRequire[i].m_AddIcon:SetSpriteName("pic_zb_suo")
				return
			end
			CPartnerUpgradeStarChooseView:ShowView(function (oView)
				local args ={
					parnerId= self.m_CurParID,
					indexSellect =i,
					starCurrent= self.m_Star
				}
				oView:SetDatafilter(args)
			end)
		end)
	end
	self.m_StarBox:SetActive(false)
	self.m_TipLabel:SetActive(false)
	self:InitAttrPart()
	self:InitNeedItem()
	self.m_TipBtn:AddHelpTipClick("partner_upstar")
	self.m_ConfirmBtn:AddUIEvent("click", callback(self, "OnUpStar"))
	-- self.m_ItemStarHope:AddUIEvent("click", callback(self, "OnSellectStar"))
	
	-- g_PartnerCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnUpdateCtrlEvent"))
	g_PartnerCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "UpdateItem"))
	
end
function CPartnerUpStarPage.OnCreateView(self)
	
end
function CPartnerUpStarPage.Destroy( self )
	--printDebug("dkm vcl")
	self:StopShowTimer()
	g_PartnerCtrl:ResetItemUpgrade()
	-- body
end
function CPartnerUpStarPage.OnSellectStar( self )
	-- body
	CItemTipsSimpleInfoView:ShowView(function (oView)
		oView:SetInitBox("14999", nil, {})
		oView:ForceShowFindWayBox(true)
	end)
end

function CPartnerUpStarPage.InitStarHope( self )
	-- body
	local itemHope = self.m_ItemStarHope
	itemHope.m_Spr= itemHope:NewUI(1,CSprite)
	itemHope.m_CounLable= itemHope:NewUI(2,CLabel)
	itemHope.m_SellectBox = itemHope:NewUI(3,CSetBox)
	itemHope.m_SellectBox:SetSelected(false)
	itemHope.m_Background = itemHope:NewUI(4,CSprite)
	itemHope.m_Help = itemHope:NewUI(5,CButton)
	--itemHope:AddHelpTipClick("star_hope_upstar")
	-- itemHope.m_Background:AddUIEvent("click", function ()
	-- 	CItemTipsSimpleInfoView:ShowView(function (oView)
	-- 		oView:SetInitBox("14999", nil, {})
	-- 		oView:ForceShowFindWayBox(true)
	-- 	end)
	-- end)
	local item = data.partnerdata.UPSTAR[self.m_Star]
	local needCount = item.cost_amount
	local ownCount = g_ItemCtrl:GetBagItemAmountBySid(item.item_bonus)
	local str =""
	if needCount > ownCount then 
		str = string.format("#R%d[ffffff]/%d", ownCount, needCount)
		
	else
		str = string.format("[1cee00]%d/%d", ownCount, needCount)
	end
	itemHope.m_CounLable:SetText(str)

	if self.m_Star>=4 or  (needCount > ownCount) then
		itemHope.m_Background:SetActive(true)
	else
		itemHope.m_Background:SetActive(false)
	end
	
end
function CPartnerUpStarPage.InitAttrPart(self)
	self.m_AttrList = {}
	local list = {"maxhp", "defense", "attack"}
	for i, name in ipairs(list) do
		self.m_AttrList[name] = self.m_AttrPart:NewUI(i*2-1, CLabel)
		self.m_AttrList["next_"..name] = self.m_AttrPart:NewUI(i*2, CLabel)
	end
	for i = 1, 5 do
		local spr = self.m_StarBox:Clone()
		spr.m_Effect = spr:NewUI(1, CUIEffect)
		spr.m_Spr = spr:NewUI(2, CSprite)
		spr.m_Effect:Above(self.m_StarBox)
		spr.m_Effect:SetActive(false)
		spr:SetActive(true)

		self.m_LStarGrid:AddChild(spr)
		local spr2 = self.m_StarBox:Clone()
		spr2.m_Effect = spr2:NewUI(1, CUIEffect)
		spr2.m_Spr = spr2:NewUI(2, CSprite)
		spr2.m_Effect:Above(self.m_StarBox)
		spr2.m_Effect:SetActive(false)
		spr2:SetActive(true)

		self.m_RStarGrid:AddChild(spr2)
	end
	self.m_LStarGrid:Reposition()
	self.m_RStarGrid:Reposition()
end
function CPartnerUpStarPage.UpdateItem( self,oCtrl )
	if oCtrl.m_EventID == define.Partner.Event.UPdateItemSellect then
		
		local lst = g_PartnerCtrl:getItemUpgrade()
		for i=1,3,1 do
			local itm = lst[i]
			self.m_ItemRequire[i]:SetData(itm)
		end

		
	end
	-- body
end


	

function CPartnerUpStarPage.InitNeedItem(self)
	-- self.m_ItemBox = self.m_NeedItemPart:NewUI(1, CItemTipsBox)
	-- self.m_AmountLabel = self.m_NeedItemPart:NewUI(2, CLabel)
	-- self.m_GoldLabel = self.m_NeedItemPart:NewUI(3, CLabel)
	-- self.m_Slider = self.m_NeedItemPart:NewUI(4, CSlider)
	local lst = g_PartnerCtrl:getItemUpgrade()
	
	for i=1,3,1
	do
		local itm = lst[i]
		
		self.m_ItemRequire[i]:SetData(itm)
	end
end

function CPartnerUpStarPage.OnUpdateCtrlEvent(self)
	--printDebug("OnUpdateCtrlEvent:", "----------------------")
	
	self:UpdatePartner()

end

function CPartnerUpStarPage.OnAttrEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Attr.Event.Change then
		if oCtrl.m_EventData["dAttr"]["coin"] then
			local oPartner = g_PartnerCtrl:GetPartner(self.m_CurParID)
			if oPartner then
				self:UpdateCost(oPartner)
			end
		end
	end
end

function CPartnerUpStarPage.SetPartnerID(self, parid)
	
	self.m_CurParID = g_PartnerCtrl.m_CurParID
	local oPartner = g_PartnerCtrl:GetPartner(self.m_CurParID)
	-- local iStar = oPartner:GetValue("star")
	-- --printDebug("CPartnerUpStarPage.SetPartnerID ",data.partnerdata.UPSTAR[iStar].cost_coin)
	self.m_Star =  oPartner:GetValue("star")
	self.m_GoldLabel:SetRichText(tostring( "#w2"..data.partnerdata.UPSTAR[self.m_Star].cost_coin))
	self:InitStarHope()
	self:UpdatePartner()
	g_PartnerCtrl:ResetItemUpgrade()
	local oPartner = g_PartnerCtrl:GetPartner(self.m_CurParID)
	if oPartner then
		oPartner:SetUpStarRedPoint()
	end
	local cost_partner = data.partnerdata.UPSTAR[self.m_Star].cost_partner
	if cost_partner < 3 then
		self.m_ItemRequire[3].m_AddIcon:SetSpriteName("pic_zb_suo")
	else
		self.m_ItemRequire[3].m_AddIcon:SetSpriteName("btn_jiahao_02")
	end
end

function CPartnerUpStarPage.UpdatePartner(self)
	local oPartner = g_PartnerCtrl:GetPartner(self.m_CurParID)
	if not oPartner then
		return
	end

	if oPartner:GetValue("partner_type") == tonumber(IOTools.GetRoleData("task_partner_2")) then
		g_GuideCtrl:AddGuideUI("partner_up_star_confirm_302_btn", self.m_ConfirmBtn)
		local guide_ui = {"partner_up_star_confirm_302_btn"}
		g_GuideCtrl:LoadTipsGuideEffect(guide_ui)		
	end

	if oPartner:GetValue("star") == 5 then
		self.m_UpStarPart:SetActive(false)
		self.m_FullPart:SetActive(true)
		return
	else
		self.m_UpStarPart:SetActive(true)
		self.m_FullPart:SetActive(false)
	end
	self:UpdateStar(oPartner)
	self:UpdateCost(oPartner)
	self:UpdateAttr(oPartner)
	self:InitStarHope()
	g_GuideCtrl:TriggerAll()
end

function CPartnerUpStarPage.UpdateStar(self, oPartner)
	
	local iStar = oPartner:GetValue("star")
	--printDebug("cc",iStar);
	for i, spr in ipairs(self.m_LStarGrid:GetChildList()) do
		if i <= iStar then
			spr.m_Spr:SetSpriteName("pic_chouka_dianliang")
		else
			spr.m_Spr:SetSpriteName("pic_chouka_weidianliang")
		end
	end
	local iRStar = math.min(iStar+1, 5)
	for i, spr in ipairs(self.m_RStarGrid:GetChildList()) do
		if i <= iRStar then
			spr.m_Spr:SetSpriteName("pic_chouka_dianliang")
		else
			spr.m_Spr:SetSpriteName("pic_chouka_weidianliang")
		end
	end
	self.m_GoldLabel:SetRichText(tostring( "#w2"..data.partnerdata.UPSTAR[self.m_Star].cost_coin))
	g_PartnerCtrl:ResetItemUpgrade()
end

function CPartnerUpStarPage.DoUpEffect(self)
	if self.m_CloseEffctTimer then
		Utils.DelTimer(self.m_CloseEffctTimer)
	end
	self:CloseUpEffect()
	local oPartner = g_PartnerCtrl:GetPartner(self.m_CurParID)
	if not oPartner then
		return
	end

	local iStar = oPartner:GetValue("star")
	--printDebug("start up",iStar)
	self.m_Star =  oPartner:GetValue("star")
	for i, spr in ipairs(self.m_LStarGrid:GetChildList()) do
		if i == iStar + 1 then
			spr.m_Effect:SetActive(true)
		end
	end
	local iRStar = math.min(iStar+1, 5)
	for i, spr in ipairs(self.m_RStarGrid:GetChildList()) do
		if i == iRStar + 1 then
			spr.m_Effect:SetActive(true)
		end
	end
	self.m_CloseEffctTimer = Utils.AddTimer(callback(self, "CloseUpEffect"), 0, 2)
	--printDebug("dkm vcl")
	g_PartnerCtrl:ResetItemUpgrade()
end

function CPartnerUpStarPage.CloseUpEffect(self)
	for i, spr in ipairs(self.m_LStarGrid:GetChildList()) do
		spr.m_Effect:SetActive(false)
	end
	for i, spr in ipairs(self.m_RStarGrid:GetChildList()) do
		spr.m_Effect:SetActive(false)
	end
	self.m_CloseEffctTimer = nil
end

function CPartnerUpStarPage.UpdateAttr(self, oPartner)
	
	self.m_CacheData = self.m_CacheData or {}
	local dict = self.m_CacheData[oPartner.m_ID] or {}
	local list = {"maxhp", "attack", "defense"}
	for i, name in ipairs(list) do
		self.m_AttrList[name]:SetText(tostring(oPartner:GetValue(name)))
		self.m_AttrList["next_"..name]:SetText(dict[name] or "")
	end
	--printDebug("CPartnerUpStarPage.UpdateAttr",dict["maxhp"])
	netpartner.C2GSOpenPartnerUI(oPartner.m_ID, 2)
	-- if not dict["maxhp"] then
		
	-- 	netpartner.C2GSOpenPartnerUI(oPartner.m_ID, 2)
	-- end
end

function CPartnerUpStarPage.UpdateAttrResult(self, iParID, dApplyList,max_weight_star,weight_star)
	--printDebug("CPartnerUpStarPage.UpdateAttrResult",dApplyList)
	self.max_weight_star = max_weight_star
	self.weight_star = weight_star
	local dict = {}
	for _, dAttr in ipairs(dApplyList) do
		dict[dAttr.key] = dAttr.value
	end
	self.m_CacheData = self.m_CacheData or {}
	self.m_CacheData[iParID] = dict
	local val = (weight_star/max_weight_star)*100
	if val < data.partnerdata.UPSTAR[self.m_Star].min_rate_percent then
		val = data.partnerdata.UPSTAR[self.m_Star].min_rate_percent
	end
	self.m_Rate:SetText("Rate:[30DB00FF]"..val.."%")
	
	if self.m_CurParID == iParID then
		local list = {"maxhp", "attack", "defense"}
		for i, name in ipairs(list) do
			self.m_AttrList["next_"..name]:SetText(tostring(dict[name]))
		end
	end
end

function CPartnerUpStarPage.UpdateCost(self, oPartner)
	local list = g_PartnerCtrl:GetPartnerByRare(0, true)
	--printDebug("cao list",#list)
	for _, t in ipairs(list) do
		local start = t:GetValue("star")
		--printDebug(start)
	end

	local level_require = data.partnerdata.UPSTAR[self.m_Star].limit_level
	if g_AttrCtrl.grade < level_require then
			self.m_ConfirmBtn:SetActive(false)
			self.m_TipLabel:SetActive(true)
			self.m_GoldLabel:SetActive(false)
			self.m_TipLabel:SetText(string.format("Level %d can star up", level_require))
	else
			self.m_GoldLabel:SetActive(true)
			self.m_ConfirmBtn:SetActive(true)
			self.m_TipLabel:SetActive(false)
	end
	-- local iStar = oPartner:GetValue("star")
	-- if iStar >= 5 then
	-- 	self.m_NeedItemPart:SetActive(false)
	-- 	self.m_TipLabel:SetActive(true)
	-- 	self.m_TipLabel:SetText("Hồn sư này đã đủ sao")
	-- 	return
	-- else
	-- 	self.m_NeedItemPart:SetActive(true)
	-- end
	-- local upstardata = data.partnerdata.UPSTAR
	-- local iNeedAmount = upstardata[iStar]["cost_amount"]
	-- local iGold = upstardata[iStar]["cost_coin"]
	-- local iLimitGrade = upstardata[iStar]["limit_level"]
	-- if oPartner:GetValue("grade") < iLimitGrade then
	-- 	self.m_ConfirmBtn:SetActive(false)
	-- 	self.m_TipLabel:SetActive(true)
	-- 	self.m_GoldLabel:SetActive(false)
	-- 	self.m_TipLabel:SetText(string.format("Cần Hồn sư cấp %d mới được tăng sao", iLimitGrade))
	-- else
	-- 	self.m_GoldLabel:SetActive(true)
	-- 	self.m_ConfirmBtn:SetActive(true)
	-- 	self.m_TipLabel:SetActive(false)
	-- end

	-- local iChipType = g_PartnerCtrl:GetChipByPartner(oPartner:GetValue("partner_type"))
	-- local oItem = g_PartnerCtrl:GetSingleChipInfo(iChipType)
	-- self.m_ItemBox:SetItemData(iChipType, 1, nil, {isLocal = true, uiType = 1, openView = self.m_ParentView})
	-- local iAmount = oItem:GetValue("amount")
	-- self.m_AmountLabel:SetText(string.format("%d/%d", iAmount, iNeedAmount))
	-- self.m_Slider:SetValue(iAmount / iNeedAmount)
	-- --self.m_GoldLabel:SetActive(iAmount >= iNeedAmount)
	-- if g_AttrCtrl.coin >= iGold then
	-- 	self.m_GoldLabel:SetRichText(string.format("#w1%s", string.numberConvert(iGold)))
	-- else
	-- 	self.m_GoldLabel:SetRichText(string.format("#R #w1%s", string.numberConvert(iGold)))
	-- end
end

function CPartnerUpStarPage.OnUpStar(self)
	local oPartner = g_PartnerCtrl:GetPartner(self.m_CurParID)
	local tMaterial = {}
	local lst = g_PartnerCtrl:getItemUpgrade()
	local newlist = g_PartnerCtrl:GetPartnerByRare(0)

	if oPartner:IsTravel() then
		g_NotifyCtrl:FloatMsg("Valarion is on an Expedition!")
		return
	end

	local level_require = data.partnerdata.UPSTAR[self.m_Star].limit_level
	local cost_partner = data.partnerdata.UPSTAR[self.m_Star].cost_partner
	if g_AttrCtrl.grade < level_require then
			self.m_ConfirmBtn:SetActive(false)
			self.m_TipLabel:SetActive(true)
			self.m_GoldLabel:SetActive(false)
			self.m_TipLabel:SetText(string.format("Level %d can star up", level_require))
	else
			self.m_GoldLabel:SetActive(true)
			self.m_ConfirmBtn:SetActive(true)
			self.m_TipLabel:SetActive(false)

	
		if #newlist <6 then
			g_NotifyCtrl:FloatMsg("You must have more than 6 Valarion to use this feature!")
		else
			for i =1,cost_partner,1 do
				if lst[i] ~= nil then
					local mat = {
						partnerid = lst[i]:GetValue("parid"),
						amount=1,

					}
					table.insert( tMaterial,mat)
				end
			end

			if oPartner then
				local isUseItem = self.m_ItemStarHope.m_SellectBox:GetSelected()
				local nn = 0
				if isUseItem then
					nn=1
				end
				self.m_ConfirmBtn:SetEnabled(true)
				local function delay()
					if self.m_ConfirmBtn then
						self.m_ConfirmBtn:SetEnabled(true)
					end
				end
				self:StopShowTimer()
				self.m_ShowRewardTimer = Utils.AddTimer(delay, 0, 15)
				netpartner.C2GSUpgradePartnerStar(oPartner.m_ID,tMaterial,nn)
			end
			g_GuideCtrl:ReqTipsGuideFinish("partner_up_star_confirm_302_btn")
		end
	end
	
end

function CPartnerUpStarPage.StopShowTimer(self)
	if self.m_ShowRewardTimer then
		Utils.DelTimer(self.m_ShowRewardTimer)
		self.m_ShowRewardTimer = nil
	end
end

return CPartnerUpStarPage