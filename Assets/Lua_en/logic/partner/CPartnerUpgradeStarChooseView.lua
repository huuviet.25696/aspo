local CPartnerUpgradeStarChooseView = class("CPartnerUpgradeStarChooseView", CViewBase)

function CPartnerUpgradeStarChooseView.ctor(self, cb)
	CViewBase.ctor(self, "UI/partner/PartnerUpgradeStarChooseView.prefab", cb)
	self.m_DepthType = "Dialog"
	self.m_ExtendClose = "Black"
	self.m_GroupName = "main"

   
    
end

function CPartnerUpgradeStarChooseView.SetDatafilter(self, dData)
    self:InitContent()
    --printDebug("star ",g_PartnerCtrl:GetFightList())
    local list = g_PartnerCtrl:getListItemParner(dData["parnerId"],dData["starCurrent"])
    -- for _,n in pairs(list) do
    --     --printDebug(#list, n:GetValue("parid"))
    --    -- self.m_ItemBox:SetItemData(iChipType, 1, nil, {isLocal = true, uiType = 1, openView = self.m_ParentView})
    -- end
--    self:SetData(list)
   self:RefreshContent(list)
end

-- function CPartnerUpgradeStarChooseView.SetData( self,list )
--     -- body
--     self.m_WrapContent:Clear()
--      for _,n in pairs(list) do
--         --printDebug(#list, n:GetValue("icon"))
--         local oBelong = self.m_Box:Clone()
--         oBelong.m_Sprite  = oBelong:NewUI(1,CSprite)
--         oBelong.m_Border = oBelong:NewUI(8,CSprite)
--         oBelong.m_StarGird = oBelong:NewUI(6,CGrid)
--         oBelong.m_Star = oBelong:NewUI(7,CSprite)
--         oBelong.m_Sellect = oBelong:NewUI(5,CSprite)
--         self:UpgradeData(oBelong,n)
--         oBelong:SetActive(true)
--         self.m_WrapContent:AddChild(oBelong)
--         oBelong:AddUIEvent("click", callback(self, "OnSellect", n:GetValue("parid"), oBelong))
--        -- self.m_ItemBox:SetItemData(iChipType, 1, nil, {isLocal = true, uiType = 1, openView = self.m_ParentView})
--     end

-- end
function  CPartnerUpgradeStarChooseView.OnSellect( self,oPartner,oBox)
	if g_TravelCtrl:IsMainTraveling() then
		if oPartner:IsTravel() then
			g_NotifyCtrl:FloatMsg("Valarion is on an Expedition!")
			return
		end
	end
	local parid = oPartner:GetValue("parid")
	local iStar = oPartner:GetValue("star")
    local isSellect = g_PartnerCtrl:CheckItemIsSellect(parid)
    g_PartnerCtrl:addItemUpgrade(parid,iStar,function(isSellect )
        -- body
        --printDebug("add Success",isSellect)
        oBox.m_Sellect:SetActive(isSellect)

    end)
end
-- function  CPartnerUpgradeStarChooseView.UpgradeData( self,oBox,dData )
--     --printDebug(dData:GetValue("icon"))
--     local oPartnerRanking = g_PartnerCtrl:GetAttr( dData:GetValue("parid"))
--     local iRare = oPartnerRanking["total_rank"]
--     --printDebug("co lo","bg_color_card_"..tostring(iRare))
--     oBox.m_Sprite:SpriteAvatar(dData:GetValue("icon"))
--     oBox.m_Border:SetSpriteName("bg_color_card_"..tostring(iRare))
--     oBox.m_Star:SetActive(false)
--     local iStar = dData:GetValue("star")
--     for  i =1,iStar,1 do
--         -- body
--         local clStar = oBox.m_Star:Clone()
--         oBox.m_StarGird:AddChild(clStar)
--         oBox.m_Star:SetActive(true)
--     end
--     oBox.m_StarGird:SetActive(true)
--     oBox.m_Sellect:SetActive(g_PartnerCtrl:CheckItemIsSellect(dData:GetValue("parid")))
--     -- body
-- end
function CPartnerUpgradeStarChooseView.InitContent(self)
    self.m_BtnClose = self:NewUI(1, CButton)
    -- self.m_ScrollView = self:NewUI(2, CScrollView)
    -- self.m_WrapContent = self:NewUI(6, CGrid)   
    self.m_Box = self:NewUI(3, CItemTipsBox)

    self.m_ScrollView2 = self:NewUI(8, CScrollView)
	self.m_WrapContent2 = self:NewUI(9, CWrapContent)
	self.m_CardList = self:NewUI(10, CBox)
	self.m_Confirm = self:NewUI(11, CButton)
    self.m_CardList:SetActive(false)
    self.m_RowAmount = 5
    self:InitWrapContent()
    
	self.m_Box:SetActive(false)
    self.m_Confirm:AddUIEvent("click", callback(self, "CloseView"))
	self.m_BtnClose:AddUIEvent("click", callback(self, "CloseView"))
	-- self.m_WrapContent:SetCloneChild(self.m_Box, 
	-- 	function(oBox)
			
	-- 		return oBox
	-- 	end)
	-- self.m_WrapContent:SetRefreshFunc(function(oBox, dData)
	-- 	if dData then
			
    --         local iChipType = g_PartnerCtrl:GetChipByPartner(dData:GetValue("partner_type"))
    --         oBox:SetItemData(iChipType, 1, nil, {isLocal = true, uiType = 1})
	-- 		oBox:SetActive(true)
	-- 	else	
	-- 		oBox:SetActive(false)
	-- 	end
	-- end)

    
	
end

function CPartnerUpgradeStarChooseView.InitWrapContent(self)
	self.m_WrapContent2:SetCloneChild(self.m_CardList, callback(self, "CloneCardList"))
	self.m_WrapContent2:SetRefreshFunc(callback(self, "RefreshCardList"))
end

function CPartnerUpgradeStarChooseView.GetDivideList(self, list)
	local newlist = {}
	local data = {}
	for i, oPartner in ipairs(list) do
		table.insert(data, oPartner)
		if #data >= self.m_RowAmount then
			table.insert(newlist, data)
			data = {}
		end
	end
	if #data > 0 then
		table.insert(newlist, data)
	end
	
	return newlist
end

function CPartnerUpgradeStarChooseView.RefreshContent(self,list)
	local partnerList = self:GetDivideList(list)
	self.m_WrapContent2:SetData(partnerList, true)
	self.m_ScrollView2:ResetPosition()
end

function CPartnerUpgradeStarChooseView.RefreshCardList(self, obj, dData)
	
	if dData then
		obj:SetActive(true)
		for i = 1, self.m_RowAmount do
			self:UpdateCard(obj.m_List[i], dData[i])
		end
	else
		obj:SetActive(false)
	end
end

function CPartnerUpgradeStarChooseView.UpdateCard(self, obj, oPartner)
	if oPartner then
		obj:SetActive(true)
		obj.m_Texture:SetActive(false)
		obj.m_Texture:LoadCardPhoto(oPartner:GetIcon(), function () obj.m_Texture:SetActive(true) end)
		if oPartner:GetValue("awake") == 1 then
			obj.m_AwakeSpr:SetActive(true)
			obj.m_RareLabel:SetLocalPos(Vector3.New(-23, 75, 0))
		else
			obj.m_AwakeSpr:SetActive(false)
			obj.m_RareLabel:SetLocalPos(Vector3.New(-38, 75, 0))
		end
		obj.m_LockSpr:SetActive(oPartner:IsLock())
		obj.m_GradeLabel:SetText(tostring(oPartner:GetValue("grade")))
		
		
		local oPartnerRanking = g_PartnerCtrl:GetAttr( oPartner:GetValue("parid"))
        local iRare = oPartnerRanking["total_rank"]
		obj.m_RareLabel:SetText("")
		
		obj.m_OutBorderSpr:SetSpriteName("pic_card_out"..tostring(iRare))
		obj.m_InBorderSpr:SetSpriteName("pic_card_in"..tostring(iRare))
		obj.m_AwakeSpr:SetSpriteName("pic_card_awake"..tostring(iRare))
		obj.m_RareSpr:SetActive(false)
		obj.m_Chucvi:SetSpriteName("chucvi_"..tostring(oPartner:GetValue("chucvi")))
		
		obj.m_NameLabel:SetText(oPartner:GetValue("name"))
		local iStar = oPartner:GetValue("star")
		for i = 1, 5 do
			if iStar >= i then
				obj.m_StarList[i]:SetSpriteName("pic_chouka_dianliang")
			else
				obj.m_StarList[i]:SetSpriteName("pic_chouka_weidianliang")
			end
		end
		obj.m_ID = oPartner.m_ID
		-- obj:AddUIEvent("click", callback(self, "OnClickPartner", obj.m_ID))		
        --printDebug(oPartner:GetValue("name"),oPartner:GetValue("status"))
		
		obj.m_Sellect:SetActive(g_PartnerCtrl:CheckItemIsSellect(oPartner:GetValue("parid")))
			-- thien
		self:SetGrey(obj,false)
		obj:AddUIEvent("click", callback(self, "OnSellect",oPartner, obj))
		if g_TravelCtrl:IsMainTraveling() then
			if oPartner:IsTravel() then
				self:SetGrey(obj,true)
			end
		end
        -- thien
	else
		obj.m_ID = nil
		obj:SetActive(false)
	end
end

function CPartnerUpgradeStarChooseView.SetGrey(self,obj,status)
	obj.m_Texture:SetGrey(status)
	obj.m_OutBorderSpr:SetGrey(status)
	obj.m_InBorderSpr:SetGrey(status)
	obj.m_AwakeSpr:SetGrey(status)
end

function CPartnerUpgradeStarChooseView.CloneCardList(self, obj)
	obj.m_List = {}
	for i = 1, self.m_RowAmount do
		local oCard = obj:NewUI(i, CBox)
		oCard.m_InBorderSpr = oCard:NewUI(1, CSprite)
		oCard.m_OutBorderSpr = oCard:NewUI(2, CSprite)
		oCard.m_RareLabel = oCard:NewUI(3, CLabel)
		oCard.m_AwakeSpr = oCard:NewUI(4, CSprite)
		oCard.m_Texture = oCard:NewUI(5, CTexture)
		oCard.m_GradeLabel = oCard:NewUI(6, CLabel)
		oCard.m_NameLabel = oCard:NewUI(7, CLabel)
		oCard.m_StarGrid = oCard:NewUI(8, CGrid)
		oCard.m_RareSpr = oCard:NewUI(9, CSprite)
		oCard.m_LockSpr = oCard:NewUI(10, CSprite)
		oCard.m_Chucvi = oCard:NewUI(11, CSprite)
		oCard.m_Blank = oCard:NewUI(12, CSprite)
        oCard.m_Sellect = oCard:NewUI(13,CSprite)
		oCard.m_Blank:SetActive(false)
		oCard.m_StarList = {}
		oCard.m_StarGrid:InitChild(function(obj, idx)
			local spr = CSprite.New(obj)
			oCard.m_StarList[idx] = spr
			return spr
		end)
		oCard:SetActive(false)
		obj.m_List[i] = oCard
	end
	return obj
end


return CPartnerUpgradeStarChooseView