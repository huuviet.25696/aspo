local CConvertMainView = class("CConvertMainView", CViewBase)

function CConvertMainView.ctor(self, cb)
	CViewBase.ctor(self, "UI/partner/ConvertMainView.prefab", cb)
	self.m_DepthType = "Dialog"
	self.m_ExtendClose = "Black"
	self.m_GroupName = "main"
end

function CConvertMainView.SetData(self)
    self:InitContent()
    local list = g_PartnerCtrl:GetAllPartnerList()
   self:RefreshContent(list)
end
function  CConvertMainView.OnSellect( self,oPartner,oBox)
	-- if g_TravelCtrl:IsMainTraveling() then
	-- 	if oPartner:IsTravel() then
	-- 		g_NotifyCtrl:FloatMsg("Valarion is on an Expedition!")
	-- 		return
	-- 	end
	-- end
	local parid = oPartner:GetValue("parid")
    local isSellect = self:CheckItemIsSellect(parid)
    self:addItemUpgrade(parid,function(isSellect )
        -- body
        --printDebug("add Success",isSellect)
        oBox.m_Sellect:SetActive(isSellect)

    end)
end
function CConvertMainView.InitContent(self)
    self.m_BtnClose = self:NewUI(1, CButton)
    self.m_ScrollView2 = self:NewUI(2, CScrollView)
	self.m_WrapContent2 = self:NewUI(3, CWrapContent)
	self.m_CardList = self:NewUI(4, CBox)
	self.m_Confirm = self:NewUI(5, CButton)
	self.m_TipBtn = self:NewUI(6, CButton)
    self.m_CardList:SetActive(false)
    self.m_RowAmount = 6
	self.m_ItemUpgrade = {}
    self:InitWrapContent()
    self.m_Confirm:AddUIEvent("click", callback(self, "OnConvert"))
	self.m_BtnClose:AddUIEvent("click", callback(self, "CloseView"))
	self.m_TipBtn:AddHelpTipClick("convert")
	g_MainMenuCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnRevertSuccesEvent"))
end

function CConvertMainView.OnRevertSuccesEvent(self, oCtrl)
	if oCtrl.m_EventID == define.MainMenu.RevertSuccess then
		self:CloseView()
	end
end

function CConvertMainView.InitWrapContent(self)
	self.m_WrapContent2:SetCloneChild(self.m_CardList, callback(self, "CloneCardList"))
	self.m_WrapContent2:SetRefreshFunc(callback(self, "RefreshCardList"))
end

function CConvertMainView.GetDivideList(self, list)
	local newlist = {}
	local data = {}
	for i, oPartner in ipairs(list) do
		table.insert(data, oPartner)
		if #data >= self.m_RowAmount then
			table.insert(newlist, data)
			data = {}
		end
	end
	if #data > 0 then
		table.insert(newlist, data)
	end
	
	return newlist
end

function CConvertMainView.RefreshContent(self,list)
	local partnerList = self:GetDivideList(list)
	self.m_WrapContent2:SetData(partnerList, true)
	self.m_ScrollView2:ResetPosition()
end

function CConvertMainView.RefreshCardList(self, obj, dData)
	
	if dData then
		obj:SetActive(true)
		for i = 1, self.m_RowAmount do
			self:UpdateCard(obj.m_List[i], dData[i])
		end
	else
		obj:SetActive(false)
	end
end

function CConvertMainView.UpdateCard(self, obj, oPartner)
	if oPartner then
		obj:SetActive(true)
		obj.m_Texture:SetActive(false)
		obj.m_Texture:LoadCardPhoto(oPartner:GetIcon(), function () obj.m_Texture:SetActive(true) end)
		if oPartner:GetValue("awake") == 1 then
			obj.m_AwakeSpr:SetActive(true)
			obj.m_RareLabel:SetLocalPos(Vector3.New(-23, 75, 0))
		else
			obj.m_AwakeSpr:SetActive(false)
			obj.m_RareLabel:SetLocalPos(Vector3.New(-38, 75, 0))
		end
		obj.m_LockSpr:SetActive(oPartner:IsLock())
		obj.m_GradeLabel:SetText(tostring(oPartner:GetValue("grade")))
		
		
		local oPartnerRanking = g_PartnerCtrl:GetAttr( oPartner:GetValue("parid"))
        local iRare = oPartnerRanking["total_rank"]
		obj.m_RareLabel:SetText("")
		
		obj.m_OutBorderSpr:SetSpriteName("pic_card_out"..tostring(iRare))
		obj.m_InBorderSpr:SetSpriteName("pic_card_in"..tostring(iRare))
		obj.m_AwakeSpr:SetSpriteName("pic_card_awake"..tostring(iRare))
		obj.m_RareSpr:SetActive(false)
		obj.m_Chucvi:SetSpriteName("chucvi_"..tostring(oPartner:GetValue("chucvi")))
		obj.m_IconAspo:SetActive(false)
		if oPartner:GetValue("nfttoken") then
			obj.m_IconAspo:SetActive(true)
		end
		obj.m_NameLabel:SetText(oPartner:GetValue("name"))
		local iStar = oPartner:GetValue("star")
		for i = 1, 5 do
			if iStar >= i then
				obj.m_StarList[i]:SetSpriteName("pic_chouka_dianliang")
			else
				obj.m_StarList[i]:SetSpriteName("pic_chouka_weidianliang")
			end
		end
		obj.m_ID = oPartner.m_ID
		obj.m_Sellect:SetActive(self:CheckItemIsSellect(oPartner:GetValue("parid")))
		self:SetGrey(obj,false)
		obj:AddUIEvent("click", callback(self, "OnSellect",oPartner, obj))
	else
		obj.m_ID = nil
		obj:SetActive(false)
	end
end

function  CConvertMainView.addItemUpgrade( self, parId, callback )
	local isSellect = self:CheckItemIsSellect(parId)
	local result= false
	-- if self:GetPostParnerFight(parId) ~=-1 then
	-- 	g_NotifyCtrl:FloatMsg("Unit on the battle, remove then try again.")
	-- 	return false
	-- end
	if isSellect == true then
		for i =1,4,1 do
				if self.m_ItemUpgrade[i] ~= nil and self.m_ItemUpgrade[i] == parId then
					self.m_ItemUpgrade[i]= nil
					result = false
					break
				end
		end
	else
		if self.m_ItemUpgrade[1] ~= nil and self.m_ItemUpgrade[2] ~= nil and self.m_ItemUpgrade[3] ~= nil and self.m_ItemUpgrade[4] ~= nil then

			result= false
			g_NotifyCtrl:FloatMsg("Materials maxed out!")
		
		else
			for i =1,4,1 do
				if self.m_ItemUpgrade[i] == nil then
					self.m_ItemUpgrade[i]= parId
					result = true
					break
					
				else
						
					result = false
				end

			end
		end
	end
	

	-- if callback~=nil then
	-- 	self:OnEvent(define.Partner.Event.UPdateItemSellect)
		callback(result)
	-- end
	-- body
end

function  CConvertMainView.CheckItemIsSellect( self,parid )
	-- body
	local result = false
	for i =1,4,1 do
		if self.m_ItemUpgrade[i] ~= nil and self.m_ItemUpgrade[i] == parid then
			result= true
		end
	end
	return result
end

function CConvertMainView.SetGrey(self,obj,status)
	obj.m_Texture:SetGrey(status)
	obj.m_OutBorderSpr:SetGrey(status)
	obj.m_InBorderSpr:SetGrey(status)
	obj.m_AwakeSpr:SetGrey(status)
end

function CConvertMainView.CloneCardList(self, obj)
	obj.m_List = {}
	for i = 1, self.m_RowAmount do
		local oCard = obj:NewUI(i, CBox)
		oCard.m_InBorderSpr = oCard:NewUI(1, CSprite)
		oCard.m_OutBorderSpr = oCard:NewUI(2, CSprite)
		oCard.m_RareLabel = oCard:NewUI(3, CLabel)
		oCard.m_AwakeSpr = oCard:NewUI(4, CSprite)
		oCard.m_Texture = oCard:NewUI(5, CTexture)
		oCard.m_GradeLabel = oCard:NewUI(6, CLabel)
		oCard.m_NameLabel = oCard:NewUI(7, CLabel)
		oCard.m_StarGrid = oCard:NewUI(8, CGrid)
		oCard.m_RareSpr = oCard:NewUI(9, CSprite)
		oCard.m_LockSpr = oCard:NewUI(10, CSprite)
		oCard.m_Chucvi = oCard:NewUI(11, CSprite)
		oCard.m_Blank = oCard:NewUI(12, CSprite)
        oCard.m_Sellect = oCard:NewUI(13,CSprite)
		oCard.m_IconAspo = oCard:NewUI(14,CSprite)
		oCard.m_Blank:SetActive(false)
		oCard.m_StarList = {}
		oCard.m_StarGrid:InitChild(function(obj, idx)
			local spr = CSprite.New(obj)
			oCard.m_StarList[idx] = spr
			return spr
		end)
		oCard:SetActive(false)
		obj.m_List[i] = oCard
	end
	return obj
end



function CConvertMainView.OnConvert(self)
	local curPosInfo = {}
	local pos_info = self.m_ItemUpgrade
	if not pos_info then
		return
	end
	local list = g_PartnerCtrl:GetAllPartnerList()
	if #list < 8 then
		g_NotifyCtrl:FloatMsg("Need at least 8 Valarion to convert")
		return
	end
	if self.m_ItemUpgrade[1] == nil or self.m_ItemUpgrade[2] == nil or self.m_ItemUpgrade[3] == nil or self.m_ItemUpgrade[4] == nil then
		g_NotifyCtrl:FloatMsg("Please choose 4 Valarion.")
		return
	end
	for i=1,4 do
		if pos_info[i] then
			table.insert(curPosInfo, {pos=i, parid=pos_info[i]})
		end
	end
	if 1000000 > g_AttrCtrl.goldcoin then
		g_NotifyCtrl:FloatMsg("Not enough Diamond")
		return
	end
	local windowConfirmInfo = {
		msg = string.format("Are you sure to convert F2P account to P2E? All chosen Valarion will be removed permament and cannot be restored"),
		okStr = "Confirm",
		cancelStr = "Cancel",
		okCallback = function()
			netpartner.C2GSActiveP2pPlayer(curPosInfo)
		end
	}
	g_WindowTipCtrl:SetWindowConfirm(windowConfirmInfo)
end


return CConvertMainView