local CPartnerExpeditionPage = class("CPartnerExpeditionPage", CPageBase)
function CPartnerExpeditionPage.ctor(self, obj)
	CPageBase.ctor(self, obj)
end

function CPartnerExpeditionPage.OnInitPage(self)
	self.m_ExpLabel = self:NewUI(1, CLabel)
	self.m_RealSlider = self:NewUI(2, CSlider)
	self.m_ConfirmBtn = self:NewUI(3, CButton)
	self.m_GaspoLabel = self:NewUI(4, CLabel)
	self.m_SoulLabel = self:NewUI(5, CLabel)
	self.m_AstroLabel = self:NewUI(6, CLabel)
	self.m_AspoPriceLabel = self:NewUI(7, CLabel)
	self.m_NameLabel = self:NewUI(8, CLabel)
	self.m_1TimeBtn = self:NewUI(9, CButton)
	self.m_5TimeBtn = self:NewUI(10, CButton)
	self.m_AspoPrice1Label = self:NewUI(11, CLabel)
	self.m_AspoPrice5Label = self:NewUI(12, CLabel)
	self.m_GaspoNextLabel = self:NewUI(13, CLabel)
	self.m_SoulNextLabel = self:NewUI(14, CLabel)
	self.m_AstroNextLabel = self:NewUI(15, CLabel)
	self.m_AttrRight = self:NewUI(16, CGrid)
	self:InitContent()
end
function CPartnerExpeditionPage.InitContent(self)
	g_WelfareCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnWelfareEvent"))
	-- self.m_ConfirmBtn:AddUIEvent("click", callback(self, "OnRecover"))
	self.m_1TimeBtn:AddUIEvent("click", callback(self, "OnRecover1"))
	self.m_5TimeBtn:AddUIEvent("click", callback(self, "OnRecover5"))
end

function CPartnerExpeditionPage.OnWelfareEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Welfare.Event.UpdateExpedition then
		local oPartner = g_PartnerCtrl:GetPartner(self.m_CurParID)
		local function f()
			self:UpdateExpedition(oPartner)
		end
		Utils.AddTimer(f, 0, 0.3)
	end
end


function CPartnerExpeditionPage.SetPartnerID(self, parid)
	self.m_CurParID = parid
	self:UpdatePartner()
end

function CPartnerExpeditionPage.UpdatePartner(self)
	local oPartner = g_PartnerCtrl:GetPartner(self.m_CurParID)
	if not oPartner then
		return
	end
	self:UpdateExpedition(oPartner)
	-- self:UpdateSkill()
end

function CPartnerExpeditionPage.UpdateExpedition(self, oPartner)
	-- self.m_ConfirmBtn:SetEnabled(false)
	if self.m_Sessions then
		self.m_1TimeBtn:SetEnabled(false)
		self.m_5TimeBtn:SetEnabled(false)
	else
		self.m_1TimeBtn:SetEnabled(true)
		self.m_5TimeBtn:SetEnabled(true)
	end
	
	self.m_AttrRight:SetActive(true)
	local star = oPartner:GetValue("star")
	local name = oPartner:GetValue("name")
	local talent = oPartner:GetValue("talent") or 0
	local oPartnerRanking = g_PartnerCtrl:GetAttr( oPartner:GetValue("parid"))
	local iRare = oPartnerRanking["total_rank"]
	local idx = iRare*100+star
	if talent > 0 then
		idx = 1000+iRare*100+star
	end
	local data2 = data.rewarddata.TRAVEL.reward[(1000+iRare*100+star)]
	local data = data.rewarddata.TRAVEL.reward[idx]
	
	self.m_GaspoNextLabel:SetText("#w2 "..(data2.aspo))
	self.m_SoulNextLabel:SetText("#13212 "..(data2.soulbreaking))
	self.m_AstroNextLabel:SetText("#16008 "..(data2.astrological))
	self.oPartner = oPartner
	self.m_NameLabel:SetText(name.." Rare expedition "..talent.." time")
	self.m_GaspoLabel:SetText("#w2 "..(data.aspo))
	self.m_SoulLabel:SetText("#13212 "..(data.soulbreaking))
	self.m_AstroLabel:SetText("#16008 "..(data.astrological))
	-- self.m_AspoPriceLabel:SetText((data.recover/10000).."#wa")
	self.m_AspoPrice1Label:SetText((data.recover/30).."#w2")
	self.m_AspoPrice5Label:SetText((data.recover*5/30).."#w2")
	self.m_ExpLabel:SetText(talent.."/"..data.limit)
	self.gaspo =  data.recover/30
	self.m_RealSlider:SetValue(talent / data.limit)
	-- self.m_AchieveSlider:SetSliderText(string.format("%d/%d", talent, data.limit))
	-- if data.limit ~= 0 and talent == 0 then
	-- 	self.m_ConfirmBtn:SetEnabled(true)
	-- end
	if data.limit == 0 or (data.limit - talent < 5) then
		self.m_5TimeBtn:SetEnabled(false)
	end
	if data.limit == 0 or data.limit == talent  then
		self.m_1TimeBtn:SetEnabled(false)
	end
	if data.limit == 0 or talent > 0  then
		self.m_AttrRight:SetActive(false)
	end
end

function CPartnerExpeditionPage.OnRecover(self)
	-- netpartner.C2GSAddPartnerTalent(self.m_CurParID)
end

function CPartnerExpeditionPage.SessionResponse(self, iSession)
	local timer = self.m_Sessions
	if timer then
		Utils.DelTimer(timer) 
	end
	self.m_1TimeBtn:SetEnabled(true);
	self.m_5TimeBtn:SetEnabled(true);
	self.m_Sessions = nil
end

function CPartnerExpeditionPage.OnRecover1(self)
	self.m_Sessions = Utils.AddTimer(callback(self, "SessionResponse"), 0, 5)
	self.m_1TimeBtn:SetEnabled(false);
	self.m_5TimeBtn:SetEnabled(false);
	if self.oPartner:IsTravel() then
		g_NotifyCtrl:FloatMsg("Valarion is on an Expedition!")
		return
	end
	if self.gaspo > g_AttrCtrl.goldcoin then
		g_NotifyCtrl:FloatMsg("Not enough Diamond")
		return
	end
	local windowConfirmInfo = {
		msg = string.format("Confirm use #w2%s restore rare expedition 1 time", tostring(self.gaspo)),
		okStr = "Confirm",
		cancelStr = "Cancel",
		okCallback = function()
			netpartner.C2GSAddPartnerTalent(self.m_CurParID,1)
		end
	}
	g_WindowTipCtrl:SetWindowConfirm(windowConfirmInfo)
end

function CPartnerExpeditionPage.OnRecover5(self)
	self.m_Sessions = Utils.AddTimer(callback(self, "SessionResponse"), 0, 5)
	self.m_1TimeBtn:SetEnabled(false);
	self.m_5TimeBtn:SetEnabled(false);
	if self.oPartner:IsTravel() then
		g_NotifyCtrl:FloatMsg("Valarion is on an Expedition!")
		return
	end
	if self.gaspo*5 > g_AttrCtrl.goldcoin then
		g_NotifyCtrl:FloatMsg("Not enough Diamond")
		return
	end
	local windowConfirmInfo = {
		msg = string.format("Confirm use #w2%s restore rare expedition 5 time",  tostring(self.gaspo*5)),
		okStr = "Confirm",
		cancelStr = "Cancel",
		okCallback = function()
			netpartner.C2GSAddPartnerTalent(self.m_CurParID,2)
		end
	}
	g_WindowTipCtrl:SetWindowConfirm(windowConfirmInfo)
	
end

return CPartnerExpeditionPage