local CPartnerChooseArtifactView = class("CPartnerChooseArtifactView", CViewBase)

function CPartnerChooseArtifactView.ctor(self, cb)
	CViewBase.ctor(self, "UI/Partner/PartnerChooseArtifactView.prefab", cb)
	self.m_ExtendClose = "Black"
	self.m_RowAmount = 4
end

CPartnerChooseArtifactView.g_SortData = {
	{"soul_quality", "Class"}, {"res_critical_ratio", "Crit-Resistence Rate"},
	{"level", "Level"}, {"critical_damage", "Crit damage"}, 
	{"create_time", "Order"}, {"cure_critical_ratio", "Heal"},
	{"maxhp", "HP"}, {"abnormal_attr_ratio", "ACC"},
	{"attack", "ATK"}, {"res_abnormal_ratio", "EVA"}, 
	{"defense", "DEF"}, {"maxhp_ratio", "Increase HP"},
	{"speed", "Speed"}, {"attack_ratio", "Increase ATK"},
	{"critical_ratio", "Crit Rate"}, {"defense_ratio", "Increase DEF"},  
}
CPartnerChooseArtifactView.WearSel = true
function CPartnerChooseArtifactView.OnCreateView(self)
	self.m_SortPopupBox = self:NewUI(2, CPopupBox, true, CPopupBox.EnumMode.SelectedMode, nil, true)
	self.m_BackTypeBtn = self:NewUI(3, CButton)
	self.m_WearBtn = self:NewUI(4, CButton)
	self.m_ItemScrollView = self:NewUI(5, CScrollView)
	self.m_ItemWrapContent = self:NewUI(6, CWrapContent)
	self.m_GridBox = self:NewUI(7, CBox)
	self.m_GridBox:SetActive(false)
	self:InitContent()
end
function CPartnerChooseArtifactView.InitContent(self)
	self.m_ItemWrapContent:SetCloneChild(self.m_GridBox, callback(self, "SetItemWrapCloneChild"))
	self.m_ItemWrapContent:SetRefreshFunc(callback(self, "SetItemWrapRefreshFunc"))
	self.m_BackTypeBtn:AddUIEvent("click", callback(self, "OnClose"))
	g_ItemCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnItemCtrlEvent"))
	self.m_SortPopupBox:Clear()
	self.m_SortPopupBox:SetCallback(callback(self, "OnSortChange"))
	self.m_SortPopupBox:SetOffsetHeight(15)
	for k, v in pairs(CPartnerChooseArtifactView.g_SortData) do
		self.m_SortPopupBox:AddSubMenu(v[2])
	end
	self.m_SortPopupBox:SetSelectedIndex(1)
	self.m_WearBtn:AddUIEvent("click", callback(self, "OnChangeWear"))
	self.m_WearBtn:SetSelected(CPartnerChooseArtifactView.WearSel)
end

function CPartnerChooseArtifactView.SetItemWrapCloneChild(self, oChild)
	oChild.m_IconList = {}
	for i = 1, 4 do
		local box = oChild:NewUI(i, CBox)
		box.m_BorderSpr = box:NewUI(1, CSprite)
		box.m_SoulItem = box:NewUI(2, CParSoulItem)
		box.m_AddBtn = box:NewUI(3, CButton)
		-- box.m_AddBtn:AddUIEvent("click", callback(self, "OnShowGetHelp"))
		box.m_AddBtn:SetActive(false)
		table.insert(oChild.m_IconList, box)
	end
	oChild:SetActive(true)
	return oChild
end

function CPartnerChooseArtifactView.SetItemWrapRefreshFunc(self, oChild, dData)
	if dData then
		oChild:SetActive(true)
		for i = 1, 4 do
			local box = oChild.m_IconList[i]
			if dData[i] then
				box:SetActive(true)
				if dData[i].m_ID == nil then
					box.m_ID = nil
					box.m_SoulItem:SetActive(false)
					box.m_AddBtn:SetActive(true)
				else
					local oItem = dData[i]
					box.m_ID = dData[i].m_ID
					box.m_SoulItem:SetActive(true)
					box.m_AddBtn:SetActive(false)
					box.m_SoulItem:SetItem(oItem.m_ID)
					box:AddUIEvent("click", callback(self, "OnClickItem", oItem))
				end
			else
				box.m_ID = nil
				box:SetActive(false)
			end
		end
	else
		oChild:SetActive(false)
	end
end

function CPartnerChooseArtifactView.SetTypeWrapCloneChild(self, oBox)
	oBox:SetActive(true)
	oBox.m_Name = oBox:NewUI(1, CLabel)
	oBox.m_Desc = oBox:NewUI(2, CLabel)
	oBox.m_AmountLable = oBox:NewUI(3, CLabel)
	oBox.m_Icon = oBox:NewUI(4, CSprite)
	oBox.m_EquipBtn = oBox:NewUI(5, CButton)
	oBox.m_RecommandObj = oBox:NewUI(6, CObject)
	oBox.m_GuideBtn = oBox:NewUI(7, CButton)
	return oBox
end


function CPartnerChooseArtifactView.OnItemCtrlEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Item.Event.RefreshBagItem or 
		oCtrl.m_EventID == define.Item.Event.RefreshSpecificItem or 
		oCtrl.m_EventID == define.Item.Event.RefreshPartnerSoul then
		self:OnDealyUpdate()
	end
end

function CPartnerChooseArtifactView.SetPartnerID(self, iParID)
	if self.m_CurParID ~= iParID then
		self.m_CurParID = iParID
	end
end

function CPartnerChooseArtifactView.GetTypeEquipList(self)
	local oPartner = g_PartnerCtrl:GetPartner(self.m_CurParID)

	local recommandList = {}
	local iSoulType = nil
	if oPartner then
		local oRecommendData = data.partnerrecommenddata.PartnerGongLue[oPartner:GetValue("partner_type")]
		if oRecommendData then
			recommandList = oRecommendData.equip_list
		end
		iSoulType = oPartner:GetValue("soul_type")
	end
	local list = table.values(data.partnerequipdata.ParSoulType)
	local equiplist = g_ItemCtrl:GetParSoulListMain()
	local amountdict = {}
	local iswearequip = not self.m_WearBtn:GetSelected()
	for _, oItem in ipairs(equiplist) do
		local itype = oItem:GetValue("soul_type")
		if iswearequip and oItem:GetValue("parid") ~= 0 then
			
		else
			if not amountdict[itype] then
				amountdict[itype] = {}
				amountdict[itype]["amount"] = 1

			else
				amountdict[itype]["amount"] = amountdict[itype]["amount"] + 1
			end
		end
	end
	local index = nil
	for i, dTypeObj in ipairs(list) do
		local j = table.index(recommandList, dTypeObj.id)
		if j then
			dTypeObj.m_IsRecommand = 999 - j
		else
			dTypeObj.m_IsRecommand = 0
		end
	end
	list = self:SortTypeList(list, iSoulType, amountdict)
	for i, dTypeObj in ipairs(list) do
		dTypeObj.m_Idx = i
	end
	self.m_TypeDataList = list
	self.m_TypeIdxList = table.range(1, #list)
	self.m_AmountDict = amountdict
	return list
end

function CPartnerChooseArtifactView.SortTypeList(self, list, iSoulType, amountdict)
	local function cmp(a, b)
		local typeA = a["id"]
		local typeB = b["id"]
		if a.m_IsRecommand ~= b.m_IsRecommand then
			return a.m_IsRecommand > b.m_IsRecommand
		end
		if a["id"] == iSoulType then
			return true
		end
		if b["id"] == iSoulType then
			return false
		end
		if amountdict[typeA] and not amountdict[typeB] then
			return true
		elseif not amountdict[typeA] and amountdict[typeB] then
			return false
		end
		if a["id"] < b["id"] then
			return true
		end
		return false
	end
	table.sort(list, cmp)
	return list
end

function CPartnerChooseArtifactView.UpdateTypeBox(self, box, typedata)
	box.m_Name:SetText(typedata["name"])
	box.m_Icon:SpriteItemShape(typedata["icon"])
	box.m_Desc:SetText(typedata["skill_desc"])
	box.m_RecommandObj:SetActive(typedata.m_IsRecommand > 990)
	box:AddUIEvent("click", callback(self, "OnClickType", typedata["id"]))
	box:AddUIEvent("longpress", callback(self, "OnPress", typedata["id"]))
	box:SetGroup(self.m_TypeWrapContent:GetInstanceID())
	local amount = 0
	local typeList = {}
	local amountdict = self.m_AmountDict
	if amountdict[typedata["id"]] then
		amount = amountdict[typedata["id"]]["amount"]

	end
	box.m_AmountLable:SetText(amount)
	box.m_EquipBtn:SetActive(true)
	box.m_EquipBtn:AddUIEvent("click", callback(self, "OnFastEquip", typedata["id"]))
	return box
end

function CPartnerChooseArtifactView.OnDealyUpdate(self)
	if self.m_DealyTimer then
		return
	end
	local function delay(obj)
		obj:RefreshItemPart()
		obj.m_DealyTimer = nil
	end
	self.m_DealyTimer = Utils.AddTimer(objcall(self, delay), 0, 0.2)
end

function CPartnerChooseArtifactView.RefreshItemPart(self)
	local itemList = self:GetParSoulList()
	itemList = self:GetDivideList(itemList)
	self.m_ItemWrapContent:SetData(itemList, true)
	self.m_ItemScrollView:ResetPosition()
end

function CPartnerChooseArtifactView.GetParSoulList(self)
	local itemList = g_ItemCtrl:GetParSoulListMain()
	local resultList = {}
	local iswearequip = not self.m_WearBtn:GetSelected()
	for _, oItem in ipairs(itemList) do
		-- if oItem:GetValue("soul_type") == self.m_CoreType then
			if not iswearequip or oItem:GetValue("parid") == 0 then
				table.insert(resultList, oItem)
			end
		-- end
	end
	resultList = self:SortItem(resultList)
	table.insert(resultList, CItem.NewBySid(0))
	return resultList
end

function CPartnerChooseArtifactView.SortItem(self, list)
	local sortList = {}
	for _, oItem in ipairs(list) do
		local key = nil
		if table.index({"level", "soul_quality","quality"}, self.m_SortKey) then
			key = oItem:GetValue(self.m_SortKey)
		else
			local attr = oItem:GetParSoulAttr()
			key = attr[self.m_SortKey] or 0
		end
		local t = {
			oItem,
			key,
			oItem:GetValue("quality"),
			oItem:GetValue("soul_quality"), 
			oItem:GetValue("level"),
			oItem.m_ID, 
		}
		table.insert(sortList, t)
	end
	local function cmp(listA, listB)
		local keyA = listA[2]
		local keyB = listB[2]
		if keyA ~= keyB then
			return keyA > keyB
		end
		for i = 3, 5 do
			if listA[i] ~= listB[i] then
				return listA[i] > listB[i]
			end
		end
		return false
	end
	table.sort(sortList, cmp)
	list = {}
	for _, t in ipairs(sortList) do
		table.insert(list, t[1])
	end
	return list
end

function CPartnerChooseArtifactView.GetDivideList(self, itemList)
	local newlist = {}
	local data = {}
	for i, oItem in ipairs(itemList) do
		table.insert(data, oItem)
		if #data >= self.m_RowAmount then
			table.insert(newlist, data)
			data = {}
		end
	end
	if #data > 0 then
		table.insert(newlist, data)
	end
	return newlist
end

function CPartnerChooseArtifactView.OnSortChange(self, oBox)
	local idx = self.m_SortPopupBox:GetSelectedIndex()
	self.m_SortKey = CPartnerChooseArtifactView.g_SortData[idx][1]
	self:RefreshItemPart()
end

function CPartnerChooseArtifactView.OnChangeWear(self)
	CPartnerChooseArtifactView.WearSel = not self.m_WearBtn:GetSelected()
	self:RefreshItemPart()
end

function CPartnerChooseArtifactView.OnClickType(self)
	-- self.m_CoreType = typeid
	-- self:ShowItemPart()
	self:RefreshItemPart()
end

-- function CPartnerChooseArtifactView.OnShowGetHelp(self)
-- 	if self.m_CoreType then
-- 		CPEGetWayView:ShowView(function(oView)
-- 			oView:SetData(self.m_CoreType)
-- 		end)
-- 	end
-- end

function CPartnerChooseArtifactView.OnPress(self, typeid, box, press)
	local oView = CGuideView:GetView()
	if oView then
		return
	end
	if press then
		CPEGetWayView:ShowView(function(oView)
			oView:SetData(typeid)
		end)
	end
end

function CPartnerChooseArtifactView.OnClickItem(self, oItem)
	if oItem then
		local oPartner = g_PartnerCtrl:GetPartner(self.m_CurParID)
		g_WindowTipCtrl:SetWindowItemTipsPartnerSoulInfo(oItem, {partner = oPartner})
	end
end

return CPartnerChooseArtifactView