---------------------------------------------------------------
--物品基本信息展示窗口


---------------------------------------------------------------

local CNFTTipsBaseInfoView = class("CNFTTipsBaseInfoView", CViewBase)

CNFTTipsBaseInfoView.enum =
{
	BaseInfo = 1,	--基本信息
	SellInfo = 2	--出售信息
}

CNFTTipsBaseInfoView.EnumPopup = 
{
	Use  = { Enum = 1, String = "Use", Key = "use"},
	Get  = { Enum = 2, String = "Receive", Key = "get"},	
	Sell = { Enum = 3, String = "Sell", Key = "sell"},	
	Buy  = { Enum = 4, String = "Mua", key = "buy"},
	Composite  = { Enum = 5, String = "Form", key = "composite"},
}

CNFTTipsBaseInfoView.BatUseMinCount = 2

function CNFTTipsBaseInfoView.ctor(self, cb)
	CViewBase.ctor(self, "UI/Item/NFTTipsBaseInfoView.prefab", cb)
	self.m_DepthType = "Dialog"
	--self.m_ExtendClose = "ClickOut"

	self.m_OwnerView = nil
	self.m_ItemInfo = nil
	self.m_Type = nil
	self.m_PopupList = {}
	self.m_SourceBoxList = {}
end

function CNFTTipsBaseInfoView.OnCreateView(self)
	self.m_Container = self:NewUI(1, CWidget)
	self.m_ItemIconSprite = self:NewUI(2, CSprite)
	self.m_ItemQualitySprite = self:NewUI(3, CSprite)
	self.m_ItemNameLabel = self:NewUI(4, CLabel)
	self.m_ItemTypeLabel = self:NewUI(5, CLabel)
	self.m_ItemUseforLabel = self:NewUI(6, CLabel)
	self.m_ItemBingSprite = self:NewUI(7, CSprite)
	self.m_ItemDestroyBtn = self:NewUI(8, CButton)
	self.m_ItemTimeLimitLabel = self:NewUI(9, CLabel)	
	self.m_ItemPriceLabel = self:NewUI(10, CLabel)
	self.m_ItemDesLabel = self:NewUI(11, CLabel)
	self.m_ItemCountLabel = self:NewUI(12, CLabel)
	self.m_ItemUseBtn = self:NewUI(13, CButton)
	self.m_ItemUnConfirmleBtn = self:NewUI(14, CButton)	--可变功能按钮
	self.m_ItemUnConfirmleLabel = self:NewUI(15, CLabel)
	self.m_ItemMorePopupBox = self:NewUI(16, CPopupBox, true, CPopupBox.EnumMode.NoneSelectedMode,nil, true)
	self.m_ItemMorePopupBox.m_BgSprite = self.m_ItemMorePopupBox:NewUI(8, CSprite)
	self.m_ItemUseBtnLabel = self:NewUI(17, CLabel)
	self.m_FindWayGroup = self:NewUI(18, CBox)
	self.m_FindWayGrid = self:NewUI(19, CGrid)
	self.m_FindWayCloneBox = self:NewUI(20, CBox)
	self.m_BottomGroup = self:NewUI(21, CWidget)
	self.m_BgSprite = self:NewUI(22, CSprite)
	self.m_MaskGrid = self:NewUI(23, CGrid)
	self.m_DesBgSpr = self:NewUI(24, CSprite)
	self.m_SourceWayLabel = self:NewUI(25, CLabel)
	self.m_SourceWayGrid = self:NewUI(26, CGrid)
	self.m_SourceWayBox = self:NewUI(27, CBox)
	self.m_FindWayGroupBgSrp = self:NewUI(28, CSprite)
	self.m_TitleQualitySpr = self:NewUI(29, CSprite)
	self.m_SubTitleQualitySpr = self:NewUI(30, CSprite)
	self.m_MaskWidget = self:NewUI(31, CBox)
	self.m_ItemBgSpr = self:NewUI(32, CSprite)
	self.m_PartnerBgSpr = self:NewUI(33, CSprite)
	self.m_PartnerShapSpr = self:NewUI(34, CSprite)
	self.m_PartnerQualitySpr = self:NewUI(35, CSprite)	
	self.m_nftContainer = self:NewUI(36, CBox)	
	self.m_AspoIcon = self:NewUI(37, CBox)	
	self.m_nftStatus = self:NewUI(38, CBox)
	self.m_starGrid = self:NewUI(39, CGrid)
	self.m_star = self:NewUI(40, CSprite)

	self:InitContent()
end

function CNFTTipsBaseInfoView.InitContent(self, type)
	UITools.ResizeToRootSize(self.m_MaskGrid)
	self.m_MaskGrid:InitChild(function (obj, idx)
		local oBox = CBox.New(obj)
		oBox:AddUIEvent("click", callback(self, "OnMaskClose"))
		return oBox
	end)
	self.m_MaskWidget:AddUIEvent("click", callback(self, "OnMaskClose"))

	self.m_ItemUseBtn:AddUIEvent("click", callback(self, "OnBtnClick", "use"))
	self.m_ItemUnConfirmleBtn:AddUIEvent("click", callback(self, "OnBackItem", "use"))
	self.m_ItemDestroyBtn:AddUIEvent("click", callback(self, "OnBtnClick", "destory"))
	self.m_ItemUnConfirmleBtn:SetActive(false)
	self.m_FindWayGroup:SetActive(false)
	self.m_FindWayCloneBox:SetActive(false)
	self.m_SourceWayBox:SetActive(false)
	self.m_SourceWayLabel:SetActive(false)
	-- g_ItemCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlItemlEvent"))	
end

function CNFTTipsBaseInfoView.SetContent(self, tItem)
	self.m_Item = tItem
	--printDebug(self.m_Item:GetValue("parid"))
	self.m_ItemUseBtn:SetActive(self.m_Item:GetValue("nftstatus")==1)
	self.m_ItemUnConfirmleBtn:SetActive(self.m_Item:GetValue("nftstatus")==2)
	local showItem = self.m_Item ~= nil
	-- local isTouch = false
	-- self.m_CurSellSelectSpr:SetActive(false)
	-- self.m_ScoreCompareBox:SetActive(false)
	-- self.m_IconSprite:SetActive(showItem)
	
	self.m_nftStatus:SetActive(false)
	self.m_AspoIcon:SetActive( self.m_Item:GetValue("nfttoken") ~=nil )

	 if showItem and self.m_Item:GetType() ==1 then
		self.m_nftContainer:SetActive(self.m_Item:GetValue("isNFT")==1)
	-- 	self.m_StarGird:SetActive(false)
	-- 	--printDebug("items: ",self.m_Item:GetValue("nftstatus") == 3)
	-- 	self.m_nft:SetActive(self.m_Item:GetValue("isNFT")==1)
	-- 	self.m_nftlock:SetActive(self.m_Item:GetValue("nftstatus") == 3)
	-- 	self.m_aspo:SetActive( self.m_Item:GetValue("nfttoken") ~=nil )
	-- 	self.m_InfoWidget:SetActive(true)
	 	local shape = self.m_Item:GetValue("icon") or 0
	-- 	local count = self.m_Item:GetValue("amount") or 0
	 	local quality = self.m_Item:GetValue("itemlevel") or 0
		local name = self.m_Item:GetValue("name") or ""
	-- 	local isLock = self.m_Item:IsEuqipLock()
	-- 	local minGrade = self.m_Item:GetValue("min_grade") or 0
		local des = self.m_Item:GetValue("description") or "Doesn't work, write whatever you want"
		local itemType = self.m_Item:GetValue("type")
		local iType = self.m_Item:GetValue("type")
		local count = self.m_Item:GetValue("amount")	
		self.m_ItemTypeLabel:SetText(string.format("Type: %s",define.Item.ItemTypeString[iType]))
		self.m_ItemCountLabel:SetText(string.format("Quantity: %d", count))
		local usefor = self.m_Item:GetValue("introduction") or "No effect"
		self.m_ItemUseforLabel:SetText(string.format("[Effect] %s", usefor))
		self.m_starGrid:SetActive(false)
	 	if itemType == define.Item.ItemType.PartnerChip then
	-- 		self.m_IconSprite:SetActive(false)
	 		self.m_PartnerBgSpr:SetActive(true)
	-- 		self.m_PartnerShapSpr:SetActive(true)
	 		local rare = self.m_Item:GetValue("rare")			
	-- 		self.m_PartnerShapSpr:SpriteAvatarBig(shape)			
	 		self.m_PartnerBgSpr:SetSpriteName(g_PartnerCtrl:GetRareBorderSpriteName(rare))
	-- 		self.m_PartnerQualitySpr:SetSpriteName(g_PartnerCtrl:GetChipMarkSpriteName(rare))	
	-- 		self:SetItemQuality(rare+2)
	 	else
	 		if itemType == define.Item.ItemType.EquipStone then
	 			--self.m_PartnerBgSpr
	 			self.m_PartnerBgSpr:SetSpriteName(g_PartnerCtrl:GetRareBorderSpriteName(quality))
	 		else
	 			self.m_PartnerBgSpr:SetSpriteName(g_PartnerCtrl:GetRareBorderSpriteName(0))
	 		end
	 		self.m_PartnerShapSpr:SetActive(true)
	 		self.m_PartnerShapSpr:SpriteItemShape(shape)
	 		-- if quality then
	 		-- 	self:SetItemQuality(quality)
	 		-- end			
		end

	-- 	self:SetCounttext(count)
		self.m_ItemNameLabel:SetText(name)
		self.m_ItemDesLabel:SetText(des)
	-- 	self:SetSellCounttext(0)
	-- 	self:UpdateItemState()
	-- 	self:UpdateItemScoreCompare()
	-- 	isTouch = true

	-- 	self.m_ItemIdLabel:SetActive(false)
	-- 	if CItemBagBox.TestToggle == 1 then
	-- 		self.m_ItemIdLabel:SetActive(true)
	-- 		self.m_ItemIdLabel:SetText(self.m_Item:GetValue("id"))
	-- 	end
	-- 	self.m_LockBox:SetActive(isLock)
	-- 	self.m_LevelLimitSpr:SetActive(minGrade > g_AttrCtrl.grade and (itemType == define.Item.ItemType.EquipStone))
		elseif  showItem and self.m_Item:GetType() ==0 then
	-- 	--printDebug("heros: ",self.m_Item:GetValue("parid"))
	-- 	self.m_nft:SetActive(true)
	-- 	self.m_nftlock:SetActive(self.m_Item:GetValue("nftstatus")==3)
	-- 	self.m_aspo:SetActive( self.m_Item:GetValue("nfttoken") ~=nil )
	-- 	self.m_InfoWidget:SetActive(true)
		local shape = self.m_Item:GetValue("icon") or 0
		self.m_PartnerShapSpr:SpriteAvatarBig(shape)	
	 	self.m_PartnerBgSpr:SetActive(true)
	-- 	self.m_PartnerShapSpr:SetActive(true)
	 	local oPartnerRanking = g_PartnerCtrl:GetAttr( self.m_Item:GetValue("parid"))
     	local iRare = oPartnerRanking["total_rank"]		
	-- 	self.m_PartnerShapSpr:SpriteAvatarBig(shape)	
	-- 	self.m_LevelLimitSpr:SetActive(false)		
	-- 	-- self.m_PartnerBgSpr:SetSpriteName(g_PartnerCtrl:GetRareBorderSpriteName(rare))
	 	self.m_PartnerBgSpr:SetSpriteName("bg_color_card_"..tostring(iRare))	
	-- 	-- self:SetItemQuality(rare+2)
	 	self.m_ItemNameLabel:SetText(self.m_Item:GetValue("name") or "")
		self.m_ItemDesLabel:SetText(self.m_Item:GetValue("description") or "Doesn't work, write whatever you want")
		self.m_ItemTypeLabel:SetText(string.format("Type: %s","General"))
		self.m_ItemCountLabel:SetText(string.format("Quantity: %d", 1))
		self.m_ItemUseforLabel:SetText(string.format("[Effect] %s", "Join the battle!"))
		self.m_starGrid:SetActive(true)
		self.m_starGrid:Clear()
		self.m_star:SetActive(false)
		local iStar = self.m_Item:GetValue("star")
		for  i =1,iStar,1 do
			-- body
			local clStar = self.m_star:Clone()
			self.m_starGrid:AddChild(clStar)
			self.m_star:SetActive(true)
		end
	-- 	self.m_StarGird:SetActive(true)
	-- 	self.m_StarGird:Clear()
	-- 	self.m_Star:SetActive(false)
	-- 	local iStar = self.m_Item:GetValue("star")
	-- 	for  i =1,iStar,1 do
	-- 		-- body
	-- 		local clStar = self.m_Star:Clone()
	-- 		self.m_StarGird:AddChild(clStar)
	-- 		self.m_Star:SetActive(true)
	-- 	end
	-- 	isTouch = true
	-- 	-- if iRare then
	-- 	-- 	self:SetItemQuality(iRare)
	-- 	-- end	
	-- else
	-- 	-- body
	-- 	self.m_InfoWidget:SetActive(false)
	 end
end

function CNFTTipsBaseInfoView.SetInitBox( self, tItem)
	if not tItem then
		return		
	end
	self.m_ItemInfo = tItem
	self:RefreshBaeInfo()
	if self.m_Type == CNFTTipsBaseInfoView.enum.BaseInfo then
		self.m_ItemUseBtn:SetActive(true)
		self:RefreshBtnState()
	elseif self.m_Type == CNFTTipsBaseInfoView.enum.SellInfo then
		self.m_ItemUseBtn:SetActive(false)
		self.m_ItemUnConfirmleBtn:SetActive(false)
		self.m_ItemMorePopupBox:SetActive(false)
	end

	self:RefreshBg()
end

function CNFTTipsBaseInfoView.RefreshBtnState( self )
	if self.m_ItemInfo:GetLimitTime() ~= "Invalid" then
		self.m_ItemUseBtn:SetActive(true)
		self.m_ItemDestroyBtn:SetActive(false)
		local count = self.m_ItemInfo:GetValue("amount")	
		local useType = self.m_ItemInfo:GetValue("use_type")
		local compose_amount = self.m_ItemInfo:GetValue("compose_amount") or 0
		local compose_item = self.m_ItemInfo:GetValue("compose_item") or {}

		self.m_PopupList = {}
		local isInbagView = CItemBagMainView:GetView() ~= nil
			
		if useType ~= nil and useType ~= "" then
			if useType == "partner_awake" then
				if isInbagView or self.m_ItemInfo:GetValue("composable") == 1 then
					self.m_ItemUseBtnLabel:SetText("Use")
				else
					self.m_ItemUseBtnLabel:SetText("Confirm")
				end
			elseif useType == "composite_in_bag" then
				self.m_ItemUseBtnLabel:SetText("Form")	
			else
				self.m_ItemUseBtnLabel:SetText("Use")
			end

			--如果，右侧按钮有使用功能，则左侧更多不出现使用
			--table.insert(self.m_PopupList, CNFTTipsBaseInfoView.EnumPopup.Use)	
		else
			self.m_ItemUseBtnLabel:SetText("Confirm")
		end

		if isInbagView and self.m_ItemInfo:GetValue("sale_price") ~= 0 then
			table.insert(self.m_PopupList, CNFTTipsBaseInfoView.EnumPopup.Sell)	
		end

		if self.m_ItemInfo:GetValue("composable") == 1 then
			if isInbagView then
				table.insert(self.m_PopupList, CNFTTipsBaseInfoView.EnumPopup.Composite)	
			else
				self.m_ItemUseBtnLabel:SetText("Form")
			end
		else
			if compose_amount ~= 0 and next(compose_item) and useType ~= "composite_in_bag" then
				table.insert(self.m_PopupList, CNFTTipsBaseInfoView.EnumPopup.Composite)	
			end
		end

		self.m_ItemUnConfirmleBtn:SetActive(false)
		self.m_ItemMorePopupBox:SetActive(false)
		if #self.m_PopupList == 1 then
			self.m_ItemUnConfirmleBtn:SetActive(true)
			self.m_ItemUnConfirmleLabel:SetText(self.m_PopupList[1].String)
			self.m_ItemUnConfirmleBtn:AddUIEvent("click", "OnBackItem")

			---如果左侧功能按钮是1个，则点击获取途径以外的按钮会隐藏获取途径
			--g_UITouchCtrl:TouchOutDetect(self.m_FindWayGroup, callback(self, "HideFindWayGroup"))

			self.m_ItemUseBtn:SetLocalPos(Vector3.New(106, self.m_ItemUseBtn:GetLocalPos().y, 0))

		elseif #self.m_PopupList > 1 then
			self.m_ItemMorePopupBox:ShowAniConfig()
			self.m_ItemMorePopupBox:Clear()
			local FindWarBtnIndex = 0
			self.m_ItemMorePopupBox:SetActive(true)
			self.m_ItemMorePopupBox:SetCallback(callback(self, "OnMoreClick"))
			for i = 1, #self.m_PopupList  do				
				if self.m_PopupList[i].String == "Receive" then
					FindWarBtnIndex = (#self.m_PopupList - i) + 1
					self.m_ItemMorePopupBox:AddSubMenu(self.m_PopupList[i].String, nil, nil, false)
				else			
					self.m_ItemMorePopupBox:AddSubMenu(self.m_PopupList[i].String)	
				end				
			end	
			self.m_ItemMorePopupBox:SetPopupShowAni(false)

			--如果左侧功能按钮是2个以上（包括2个）则，把获取途径挂到popupbox上，方便同时隐藏
			-- self.m_FindWayGroup.m_Transform:SetParent(self.m_ItemMorePopupBox.m_BgSprite.m_Transform)
			-- if FindWarBtnIndex ~= 0 then
			-- 	local pos = self.m_FindWayGroup:GetLocalPos()
			-- 	local h = self.m_FindWayCloneBox:GetHeight()
			-- 	self.m_FindWayGroup:SetLocalPos(Vector3.New(pos.x, pos.y + FindWarBtnIndex * h, pos.z))
			-- end

			self.m_ItemUseBtn:SetLocalPos(Vector3.New(106, self.m_ItemUseBtn:GetLocalPos().y, 0))

		else
			self.m_ItemUseBtn:SetLocalPos(Vector3.New(0, self.m_ItemUseBtn:GetLocalPos().y, 0))
		end	
	else
		self.m_ItemDestroyBtn:SetActive(true)
		self.m_ItemUseBtn:SetActive(false)
		self.m_ItemUnConfirmleBtn:SetActive(false)
		self.m_ItemMorePopupBox:SetActive(false)
	end
end

function CNFTTipsBaseInfoView.HideFindWayGroup(self)
	self.m_FindWayGroup:SetActive(false)
end

function CNFTTipsBaseInfoView.OnMoreClick(self, oBox)
	local subMenu = oBox:GetSelectedSubMenu()
	local clickType = self.m_ItemMorePopupBox:GetSelectedIndex()
	if self.m_PopupList[clickType].String == "Sell" then
		self:OnBtnClick("sell")	
	elseif self.m_PopupList[clickType].String == "Receive" then
		self:OnBtnClick("get")	
	elseif self.m_PopupList[clickType].String == "Use" then		
		self:OnBtnClick("use")	
	elseif self.m_PopupList[clickType].String == "Form" then		
		self:OnBtnClick("composite")
		self:CloseView()			
	end
end

function CNFTTipsBaseInfoView.RefreshBaeInfo(self)
	local oItem = self.m_ItemInfo
	local shape = oItem:GetValue("icon") or 0
	local quality = oItem:GetValue("itemlevel") or 0
	local name = oItem:GetValue("name") or ""
	local iType = oItem:GetValue("type")
	local usefor = oItem:GetValue("introduction") or "No effect"
	local key = oItem:GetValue("key")
	local bing =  oItem:IsBingdingItem()
	local limit = oItem:IsLimitItem()
	local des = oItem:GetValue("description") or "Doesn't work, write whatever you want"
	local count = oItem:GetValue("amount")	

	if iType == define.Item.ItemType.PartnerChip then
		self.m_ItemBgSpr:SetActive(false)
		self.m_PartnerBgSpr:SetActive(true)
		local rare = oItem:GetValue("rare")			
		self.m_PartnerShapSpr:SpriteAvatarBig(shape)			
		self.m_PartnerBgSpr:SetSpriteName(g_PartnerCtrl:GetRareBorderSpriteName(rare))
		self.m_PartnerQualitySpr:SetSpriteName(g_PartnerCtrl:GetChipMarkSpriteName(rare))			
	else
		self.m_ItemBgSpr:SetActive(true)
		self.m_PartnerBgSpr:SetActive(false)
		self.m_ItemIconSprite:SpriteItemShape(shape)
	end

	
	self.m_ItemQualitySprite:SetItemQuality(quality)
	self.m_ItemNameLabel:SetQualityColorText(quality, name)
	
	self.m_ItemTypeLabel:SetText(string.format("Type: %s",define.Item.ItemTypeString[iType]))
	
	self.m_ItemUseforLabel:SetText(string.format("[Effect] %s", usefor))
	self.m_ItemBingSprite:SetActive(bing)
	self.m_ItemDesLabel:SetText(des)
	
	self.m_ItemCountLabel:SetText(string.format("Quantity: %d", count))

	self.m_TitleQualitySpr:SetTitleQuality(quality, 1)
	self.m_SubTitleQualitySpr:SetTitleQuality(quality, 2)

	local price = 0
	if self.m_Type == CNFTTipsBaseInfoView.enum.BaseInfo then
		price = oItem:GetValue("buy_price") or 0		
		-- if price ~= 0 then
		-- 	self.m_ItemPriceLabel:SetActive(true)
		-- 	self.m_ItemPriceLabel:SetText(string.format("价格 :%d", price))
		-- else
		-- 	self.m_ItemPriceLabel:SetActive(false)
		-- end
		--隐藏购买价格
		self.m_ItemPriceLabel:SetActive(false)

	--不显示出售价格		
	elseif self.m_Type == CNFTTipsBaseInfoView.enum.SellInfo then
		-- price = oItem:GetValue("sale_price") or 0		
		-- if price ~= 0 then
		-- 	self.m_ItemPriceLabel:SetActive(true)
		-- 	self.m_ItemPriceLabel:SetText(string.format("价格 :%d", price))				
		-- else
		-- 	self.m_ItemPriceLabel:SetActive(false)	
		-- end
	end
	if limit then
		self.m_ItemTimeLimitLabel:SetActive(true)
		self.m_ItemTimeLimitLabel:SetText(oItem:GetLimitTime())
	else 
		self.m_ItemTimeLimitLabel:SetActive(false)
	end
end

function CNFTTipsBaseInfoView.OnBtnClick(self, bKey)
	if (self.m_ItemUseBtnLabel:GetText() == "Close") then 
		self:CloseView()
		return 
	end
	if self.m_Item:GetType()==1 then
		g_ItemCtrl:C2GSItemNftStatus(self.m_Item:GetValue("id"),2)
	elseif self.m_Item:GetType()==0 then
		
		local parid = self.m_Item:GetValue("parid")
		--printDebug("parid",parid)
		g_PartnerCtrl:C2GSPartnerStatus(parid,2)
	end
end

function CNFTTipsBaseInfoView.OnBackItem(self, bKey)
	if self.m_Item:GetType()==1 then
		g_ItemCtrl:C2GSItemNftStatus(self.m_Item:GetValue("id"),1)
	elseif self.m_Item:GetType()==0 then
		g_PartnerCtrl:C2GSPartnerStatus(self.m_Item:GetValue("parid"),1)
	end
end

function CNFTTipsBaseInfoView.OnCtrlItemlEvent(  self, oCtrl )
	if oCtrl.m_EventID == define.Item.Event.RefreshBagItem then
		local id = self.m_ItemInfo:GetValue("id")
		local count = g_ItemCtrl:GetTargetItemCountById(id)
		if count == 0 then	   		
			--如果当前没有打开批量使用或者在出售时 并且是在售或者使用,才会关闭此画面
		 	if CItemTipsMoreView:GetView() == nil and 
		 		(g_ItemCtrl.m_CurSellItemId == id or g_ItemCtrl.m_CurUseItemId == id ) then
		 		g_ItemCtrl.m_CurSellItemId = 0
		 		g_ItemCtrl.m_CurUseItemId = 0
				self:CloseView()
		 	end
		 	self.m_ItemInfo:SetValue("amount", count)	   				   		
		 end		  
		 self.m_ItemCountLabel:SetText(string.format("Quantity: %d", count))

	elseif oCtrl.m_EventID == define.Item.Event.RefreshSpecificItem then
		if oCtrl.m_EventData ~= nil then
			if oCtrl.m_EventData:GetValue("id") == self.m_ItemInfo:GetValue("id") then
				self.m_ItemInfo = oCtrl.m_EventData
	   			self.m_ItemCountLabel:SetText(string.format("Quantity: %d", self.m_ItemInfo:GetValue("amount")))
			end
		end	   
	end
end

function CNFTTipsBaseInfoView.UseWhenItemReduce(self)
	--printtrace()
	if CItemBagMainView:GetView() then
		g_NotifyCtrl:FloatMsg(string.format("The number of %s has changed, please confirm again", self.m_ItemInfo:GetValue("name")))
	end
	self:CloseView()	
end

function CNFTTipsBaseInfoView.CheckOpenCondition(self, sid)
	return g_ItemCtrl:CheckOpenCondition(sid)
end

function CNFTTipsBaseInfoView.RefreshBg(self)
	local h1 = self.m_ItemUseforLabel:GetLocalPos().y - self.m_ItemUseforLabel:GetHeight()
	self.m_ItemDesLabel:SetLocalPos(Vector3.New(self.m_ItemDesLabel:GetLocalPos().x, h1, 0))

	local h2 = self.m_ItemUseforLabel:GetHeight()  + self.m_ItemDesLabel:GetHeight()  + 20
	h2 = h2 > 100 and h2 or 100

	self.m_BgSprite:SetHeight(h2 + 200)

	local h3 = self.m_BgSprite:GetLocalPos().y - self.m_BgSprite:GetHeight() + 55
	self.m_BottomGroup:SetLocalPos(Vector3.New(self.m_BottomGroup:GetLocalPos().x, h3, 0))
end

function CNFTTipsBaseInfoView.SetOwnerView(self, owner)
	self.m_OwnerView = owner
end

function CNFTTipsBaseInfoView.OnMaskClose(self)
	self:CloseView()
end


function CNFTTipsBaseInfoView.SetMaskWidget(self, b)
	self.m_MaskWidget:SetActive(b)
end

return CNFTTipsBaseInfoView