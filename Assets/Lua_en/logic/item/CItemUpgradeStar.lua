local CItemUpgradeStar = class("CItemUpgradeStar", CBox)

function CItemUpgradeStar.ctor(self, obj)
	CBox.ctor(self, obj)
	self.m_IconSprite= self:NewUI(1,CSprite)
	self.m_AddIcon = self:NewUI(10,CSprite)
	self.m_Border = self:NewUI(8,CSprite)
	self.m_StartGid = self:NewUI(6,CGrid)
	self.m_Star = self:NewUI(7,CSprite)
end

function CItemUpgradeStar.SetData(self, dData)
	if dData == nil then
        self.m_IconSprite:SetSpriteName(244)
        self.m_Border:SetSpriteName("bg_haoyoukuang_huise")
        self.m_StartGid:SetActive(false)
        self.m_AddIcon:SetActive(true)
        
    else
       
        local oPartnerRanking = g_PartnerCtrl:GetAttr( dData:GetValue("parid"))
        local iRare = oPartnerRanking["total_rank"]
        self.m_IconSprite:SpriteAvatar(dData:GetValue("icon"))
        self.m_Border:SetSpriteName("bg_color_card_"..tostring(iRare))
        --printDebug("color","bg_color_card_"..tostring(iRare))
        self.m_Star:SetActive(false)
        self.m_StartGid:SetActive(true)
        self.m_StartGid:Clear()
        self.m_AddIcon:SetActive(false)
        local iStar = dData:GetValue("star")
        for  i =1,iStar,1 do
            -- body
            local clStar = self.m_Star:Clone()
            self.m_StartGid:AddChild(clStar)
            self.m_Star:SetActive(true)
        end
        self.m_StartGid:SetActive(true)
        --oBox.m_Sellect:SetActive(g_PartnerCtrl:CheckItemIsSellect(dData:GetValue("parid")))

    end
end

return CItemUpgradeStar