local CNFTConvertValarionView = class("CNFTConvertValarionView", CViewBase)

function CNFTConvertValarionView.ctor(self, cb)
	CViewBase.ctor(self, "UI/Item/NFTConvertValarionView.prefab", cb)
	self.m_ExtendClose = "Black"
end

function CNFTConvertValarionView.OnCreateView(self)
    self.m_ExchangeContentLabel = self:NewUI(1, CLabel)
	self.m_FeeLabel = self:NewUI(2, CLabel)
	self.m_ConvertBtn = self:NewUI(3, CButton)
	self.m_CloseBtn = self:NewUI(4, CButton)

	self:InitContent()
end

function CNFTConvertValarionView.InitContent(self)
	self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))
	self.m_ConvertBtn:AddUIEvent("click", callback(self, "OnClickMintNFT"))
	-- mint_nemo_cost
	self.mint_nemo_costs = string.safesplit(data.globaldata.GLOBAL.mint_nemo_cost.value, ",")
	
end

function CNFTConvertValarionView.SetNFTType(self, type)
	self.type = type
end

function CNFTConvertValarionView.SetParId(self, Id)
	self.parId = Id
	if (self.type ==0) then
		local oPartner = g_PartnerCtrl:GetPartner(self.parId)
		local star = tonumber(oPartner:GetValue("star"))
		self.m_FeeLabel:SetText(string.format("%s",self.mint_nemo_costs[star]))
	else
		self.m_FeeLabel:SetText(string.format("%s","0.00"))
	end
end

function CNFTConvertValarionView.OnClickMintNFT(self)
	self:DoMintConvert()
end

function CNFTConvertValarionView.DoMintConvert(self)
	local curPosInfo = {}
	if not self.parId then
		return
	end
	local list = g_PartnerCtrl:GetAllPartnerList()
	if #list < 1 then
		g_NotifyCtrl:FloatMsg("Can mint NFT if having more than 1 item")
		return
	end

	table.insert(curPosInfo, {pos=1, parid=self.parId})
	
	printDebug("CNFTConvertValarionView.OnMintNFT", self.parId)
	if self.type == 0 then --general
		netpartner.C2GSActiveMintPartner(curPosInfo)
		self:OnClose()
		return
	end
	if self.type == 1 then --(weapon)
		g_NotifyCtrl:FloatMsg("Mint weapon to NFT does not activate yet.")
		return
	end
	if self.type == 2 then --item (not weapon)
		netpartner.C2GSActiveMintItem(self.parId)
		self:OnClose()
		return
	end
	-- local windowConfirmInfo = {
	-- 	msg = string.format("Your mint request is under processing, please check your NFT on marketplace."),
	-- 	okStr = "OK",
	-- 	okCallback = function()
	-- 		-- netpartner.C2GSActiveMintPartner(curPosInfo)
	-- 	end
	-- }
	-- g_WindowTipCtrl:SetWindowAlert(windowConfirmInfo)
	self:OnClose()
end

return CNFTConvertValarionView