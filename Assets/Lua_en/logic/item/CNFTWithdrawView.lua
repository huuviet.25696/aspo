local CNFTWithdrawView = class("CNFTWithdrawView", CViewBase)

function CNFTWithdrawView.ctor(self, cb)
	CViewBase.ctor(self, "UI/Item/NFTWithdrawView.prefab", cb)
	self.m_ExtendClose = "Black"
end

function CNFTWithdrawView.OnCreateView(self)
	self.m_CloseBtn = self:NewUI(10, CButton)
	self.m_ColorCoinLabel = self:NewUI(3, CLabel)
	self.m_IncBtn = self:NewUI(4, CButton)
	self.m_DecBtn = self:NewUI(5, CButton)
	self.m_AmountLabel = self:NewUI(6, CLabel)
	self.m_GainLabel = self:NewUI(8, CLabel)
	self.m_exchangeBtn = self:NewUI(9, CButton)
	self.m_maxBtn = self:NewUI(15, CButton)


	self.m_FindWayGroup = self:NewUI(11, CBox)
	self.m_FindWayGrid = self:NewUI(12, CGrid)
	self.m_FindWayCloneBox = self:NewUI(13, CBox)
	self.m_FindWayGroupBgSrp = self:NewUI(14, CSprite)


	self:InitContent()

	self:InitFindWay()
	
end

function CNFTWithdrawView.InitContent(self)
	self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))
	self.m_exchangeBtn:AddUIEvent("click", callback(self, "OnClickExchange"))
	self.m_maxBtn:AddUIEvent("click", callback(self, "OnClickMax"))
	self.m_IncBtn:AddUIEvent("click", callback(self, "OnClickInc"))
	self.m_DecBtn:AddUIEvent("click", callback(self, "OnClickDec"))
	self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))

	g_AttrCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlAttrEvent"))

	self:RefreshView()
end

function CNFTWithdrawView.InitFindWay(self)
	local colorCoinWay = data.npcstoredata.GetWay.color_coin
	if (colorCoinWay == nil) then 
		self.m_FindWayGroup:SetActive(false)
		return 
	end
	local gainWay = colorCoinWay.get_way
	for i,v in ipairs(gainWay) do
		local oBox = self:CreateFindWayBox()
		self.m_FindWayGrid:AddChild(oBox)
		oBox:SetData(data.itemdata.MODULE_SRC[v])
	end
	local w, h = self.m_FindWayGrid:GetCellSize()
	self.m_FindWayGroupBgSrp:SetHeight(91 + #gainWay * h)
	self.m_FindWayCloneBox:SetActive(false)
	self.m_FindWayGrid:Reposition()
end

function CNFTWithdrawView.CreateFindWayBox(self)
	local oBox = self.m_FindWayCloneBox:Clone()
	oBox.m_Label = oBox:NewUI(1, CLabel)
	oBox:AddUIEvent("click", callback(self, "OnClickGo", oBox))

	function oBox.SetData(self, oData)
		oBox.m_Data = oData
		oBox.m_Label:SetText(oData.name)
	end
	return oBox
end

function CNFTWithdrawView.OnClickMax(self)
	local colorCoin = g_AttrCtrl.color_coin/10000
	
	self.m_AmountLabel:SetNumberString(math.floor(colorCoin))
	self.m_GainLabel:SetNumberString(math.floor(colorCoin))
end

function CNFTWithdrawView.OnClickInc(self)
	local colorCoin = g_AttrCtrl.color_coin/10000
	local currentValue = tonumber(self.m_AmountLabel:GetText())
	if(currentValue < math.floor(colorCoin)) then 
		currentValue = currentValue + 1
		self.m_AmountLabel:SetNumberString(currentValue)
		self.m_GainLabel:SetNumberString(currentValue)
	end

end

function CNFTWithdrawView.OnClickDec(self)
	local currentValue = tonumber(self.m_AmountLabel:GetText())
	if(currentValue > 0) then 
		currentValue = currentValue - 1
		self.m_AmountLabel:SetNumberString(currentValue)
		self.m_GainLabel:SetNumberString(currentValue)
	end
end


function CNFTWithdrawView.OnClickExchange(self)
	local colorCoin = g_AttrCtrl.color_coin/10000
	local currentValue = tonumber(self.m_AmountLabel:GetText())
	if(currentValue == 0) then 
		g_NotifyCtrl:FloatMsg("The amount to withdraw must more than 0!")
		return
	end
	if(currentValue >colorCoin ) then 
		g_NotifyCtrl:FloatMsg("The amount to withdraw must less than 0!")
		return
	end
	self:DoNEMOConvert()
end

function CNFTWithdrawView.DoNEMOConvert(self)
	local colorCoin = g_AttrCtrl.color_coin
	local currentValue = tonumber(self.m_AmountLabel:GetText())
	colorCoin = colorCoin - currentValue*10000
	self.m_ColorCoinLabel:SetNumberString(colorCoin)

	printDebug("CNFTWithdrawView.DoNEMOConvert", currentValue)
	netpartner.C2GSActiveConvertToken(math.floor(currentValue))
	self:RefreshView(false)
	-- self:OnClose()
end

function CNFTWithdrawView.RefreshView(self, refresh_color_coin)
	refresh_color_coin = refresh_color_coin or true
	if (refresh_color_coin == true) then
		local colorCoin = g_AttrCtrl.color_coin/10000
		self.m_ColorCoinLabel:SetNumberString(colorCoin)
	end
	self.m_AmountLabel:SetNumberString(0)
	self.m_GainLabel:SetNumberString(0)
end

function CNFTWithdrawView.OnCtrlAttrEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Attr.Event.Change then
		local colorCoin = g_AttrCtrl.color_coin/10000
		self.m_ColorCoinLabel:SetNumberString(colorCoin)
	end
end

return CNFTWithdrawView