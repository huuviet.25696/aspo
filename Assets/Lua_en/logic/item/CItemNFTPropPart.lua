local CItemNFTPropPart = class("CItemNFTPropPart", CBox)

local PerColNumber = 8

CItemNFTPropPart.Config = {
	GridItemMax = 500,
	GridRowMax =  500 / PerColNumber,
	GridRowMin =  20 / PerColNumber,
}
function CItemNFTPropPart.ctor(self, obj, parentView)
	CBox.ctor(self, obj)
	self.m_SortType = CItemBagPropPart.EnumSort.Time
	self.m_TabIndex = -1
	self.m_ItemBtnList = {}
	self.m_ParentView = parentView
	self.m_SellSelectIndex = 0
	self.m_SellSelectItem = nil
	self.m_SellSelectBagBox = nil

	self.m_TabGird = self:NewUI(1, CTabGrid)
	self.m_ScrollViewBg = self:NewUI(2, CBox)
	self.m_NonePropWidget = self:NewUI(3, CBox)
	self.m_ItemScrollView = self:NewUI(5, CScrollView)
	self.m_BatSelectBtn = self:NewUI(8, CButton)
	self.m_WrapContent = self:NewUI(9, CWrapContent)
	self.m_ItemCloneHorGrid = self:NewUI(10, CGrid)
	self.m_LocationPopupBox = self:NewUI(11, CPopupBox, true, CPopupBox.EnumMode.SelectedMode, 1, true)
	self.m_ConvertBtn = self:NewUI(13, CButton)
	self.m_WithdrawBtn = self:NewUI(14, CButton)
	self:InitContent()
end
function CItemNFTPropPart.InitContent(self)
	self.m_ItemCloneHorGrid:SetActive(false)
	self.m_LocationPopupBox:SetRepositionWhenOpen(true)
	self:InitDropList()
	--默认标签
	local defaultTab = define.Item.ItemType.Genernal
	--local defaultTab = g_ItemCtrl:GetRedDotTab()

	self.m_TabGird.m_TabText = {}
	self.m_TabGird.m_TabList = {}
	self.m_TabGird:InitChild(function ( obj, idx )
		local tTab = CBox.New(obj)
		tTab:SetGroup(self.m_TabGird:GetInstanceID())
		tTab:AddUIEvent("click", callback(self, "OnSwitchTab", idx))
		self.m_TabGird.m_TabText[idx] = tTab:NewUI(1, CLabel)
		self.m_TabGird.m_TabList[idx] = tTab		
		if defaultTab == idx then
			tTab:SetSelected(true)
		end	
		return tTab	
		end)

	self:InitWarpContent()

	--self.m_ParentView:SetValueChangeCallback(self:GetInstanceID(), callback(self, "ValueChangeCallback"))	
	-- self.m_ScrollViewBg:AddUIEvent("press", callback(self, "OnScrollViewBg"))

	 self.m_BatSelectBtn:AddUIEvent("click", callback(self, "OnBatSelect"))
	 self.m_ConvertBtn:AddUIEvent("click", callback(self, "OnConvert"))
	 self.m_WithdrawBtn:AddUIEvent("click", callback(self, "OnWithdraw"))
	-- g_ItemCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlItemlEvent"))
	-- g_AttrCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlAttrlEvent"))

	-- self:OnSwitchTab(defaultTab)
	-- self:RefreshTabRedDot()
	g_PartnerCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnPartnerCtrlEvent"))
	g_ItemCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnCtrlItemlEvent"))

    self:OnSwitchTab(defaultTab)
end
function CItemNFTPropPart.OnClickTeamTarget(self)
	g_WindowTipCtrl:SetWindowFilterBag(
		{
		  valueCallback = callback(self, "OnTeamTargetChange"),
		  closeCallback = callback(self, "OnTeanTargetChangeEnd"),
		  --taskId = auto_target,
		},
		{ widget = self.m_TeamTargetBtn, side = enum.UIAnchor.Side.Bottom, offset = Vector2.New(5, -5)}
	)
	self.m_TeamTargetBtn.m_FlagSpr.TweenRotation:Toggle()
end
function CItemNFTPropPart.OnTeamTargetChange(self, taskId)
	-- if g_TeamCtrl:CanManualCreareTarget(taskId) then
	-- 	self.m_LocalTaskId = taskId 
	-- end
end

function CItemNFTPropPart.OnTeanTargetChangeEnd(self)
	self.m_TeamTargetBtn.m_FlagSpr.TweenRotation:Toggle()
	-- if self.m_LocalTaskId ~= nil then
	-- 	local tTargetInfo = g_TeamCtrl:GetTeamTargetInfo()
	-- 	if tTargetInfo.auto_target ~= self.m_LocalTaskId then
	-- 		local min, max = g_TeamCtrl:GetTeamTargetDefaultLevel(self.m_LocalTaskId)
	-- 		self:OnSetTeamTarget(self.m_LocalTaskId, min, max)
	-- 	end
	-- end
end
function CItemNFTPropPart.OnBatSelect(self )
	CNFTTipsBaseInfoView:ShowView(function (oView)
		 oView.m_ItemUseBtnLabel:SetText("Close")
		 oView:SetContent(self.m_SellSelectBagBox)
		--  oView.m_ItemUseBtn:SetActive(false)
		-- args = args or {}
		-- args.widget = args.widget or oView
		-- args.side = args.side or enum.UIAnchor.Side.Top
		-- args.offset = args.offset or Vector2.New(0, 10)
		-- args.openView = args.openView or nil
		-- args.hideMaskWidget = args.hideMaskWidget or false
		-- if args.hideMaskWidget then
		-- 	oView:SetMaskWidget(false)
		-- end
		-- oView:SetOwnerView(args.openView)
		--UITools.NearTarget(args.widget, part.m_TipWidget, args.side, args.offset)		
	end)
end
function CItemNFTPropPart.OnConvert(self)
	-- if 1000000 > g_AttrCtrl.color_coin then
	-- 	g_NotifyCtrl:FloatMsg("Not enough NEMO")
	-- 	return
	-- end
	-- local windowConfirmInfo = {
	-- 	msg = string.format("Do you want convert Valarion to NFT\nFree: %d#wa",200),
	-- 	title = "Convert",
	-- 	okStr = "Convert",
	-- 	cancelStr = "Cancel",
	-- 	okCallback = function()
	-- 		--netpartner.C2GSActiveP2pPlayer(curPosInfo)
	-- 	end
	-- }
	-- g_WindowTipCtrl:SetWindowConfirm(windowConfirmInfo)
	if (self.m_TabIndex == 1 and self.m_SellSelectBagBox ~=nil) then
		CNFTConvertValarionView:ShowView(function (oView)
			oView:SetNFTType(0)
			oView:SetParId(self.m_SellSelectBagBox:GetValue("parid"))
			self.m_SellSelectBagBox = nil
		end)
	elseif (self.m_TabIndex == 3 and self.m_SellSelectBagBox ~=nil) then
		CNFTConvertValarionView:ShowView(function (oView)
			oView:SetNFTType(2)
			oView:SetParId(self.m_SellSelectBagBox:GetValue("id"))
			self.m_SellSelectBagBox = nil
		end)
	else
		g_NotifyCtrl:FloatMsg("Please select an item to mint")
	end
end
function CItemNFTPropPart.OnWithdraw(self)
	CNFTWithdrawView:ShowView()
end
function CItemNFTPropPart.OnPartnerCtrlEvent(self, oCtrl)
	if oCtrl.m_EventID == define.Partner.Event.PartnerAdd and (self.m_TabIndex == 1  or self.m_TabIndex == 4)  then
		g_NFTCtrl.m_BagTabItemsCache = g_NFTCtrl:GetAllBagItemsByShowTypeAndSort(self.m_TabIndex, 1, false)
		self:RefreshAll()
	end
	if oCtrl.m_EventID == define.Partner.Event.UpdatePartner and self.m_TabIndex == 1 or (self.m_TabIndex == 4)  then
		g_NFTCtrl.m_BagTabItemsCache = g_NFTCtrl:GetAllBagItemsByShowTypeAndSort(self.m_TabIndex, 1, false)
		self:RefreshAll()
	end
	if oCtrl.m_EventID == define.Partner.Event.DelPartner and self.m_TabIndex == 1 or (self.m_TabIndex == 4)  then
		g_NFTCtrl.m_BagTabItemsCache = g_NFTCtrl:GetAllBagItemsByShowTypeAndSort(self.m_TabIndex, 1, false)
		self:RefreshAll()
	end
end

function CItemNFTPropPart.OnCtrlItemlEvent( self, oCtrl)
	
	if oCtrl.m_EventID == define.Item.Event.RefreshBagItem  and self.m_TabIndex ~= 1  then
		g_NFTCtrl.m_BagTabItemsCache = g_NFTCtrl:GetAllBagItemsByShowTypeAndSort(self.m_TabIndex, 1, false)
		self:RefreshAll()
	end
end

function CItemNFTPropPart.OnSwitchTab(self, tabIdx , isInit)
	if  self.m_TabIndex == tabIdx then
		return
	end
	self.m_BatSelectBtn:SetActive(false)
	-- self.m_LocationPopupBox:Clear()
	self.m_LocationPopupBox:SetActive(tabIdx==4)
   	self.m_SellSelectItem = nil
	self.m_TabIndex = tabIdx
    if self.m_TabIndex ==1 then
        --printDebug("12",g_PartnerCtrl:GetPartnerNoFight())
    end
	self.m_ConvertBtn:SetActive(true)
	self.m_WithdrawBtn:SetLocalPos(Vector3.New(63.6,-206))
	if self.m_TabIndex ==4 then
		self.m_ConvertBtn:SetActive(false)
		self.m_WithdrawBtn:SetLocalPos(Vector3.New(223.6,-206))
    end
    g_NFTCtrl.m_RecordItemPageTab = self.m_TabIndex
	-- local sortIndex = g_ItemCtrl.m_RecordItembBagSortTypeCache[tabIdx]
	-- local sort = CItemBagSellInfoPart.EnumSort[tabIdx][sortIndex]	
	-- local isSell = (g_ItemCtrl.m_RecordItembBagViewState == 2)
    g_NFTCtrl.m_BagTabItemsCache = g_NFTCtrl:GetAllBagItemsByShowTypeAndSort(tabIdx, 1, false)
    --printDebug("get items",g_NFTCtrl.m_BagTabItemsCache)
	--self.m_ParentView:OnValueChange("SwitchTab", self.m_TabIndex)	
	-- --清除当前选中的出售道具
	self.m_SellSelectIndex = 0
	self.m_SellSelectItem = nil
	self.m_SellSelectBagBox=nil
	self:SetCurSellSelectActive(false)
	self.m_SellSelectBagBox = nil
	self:UpdataTabText()
	if isInit ~= true then
		self:RefreshAll()
	end
	-- self:SetBatBtnVisible()
	-- g_ItemCtrl:RefeshItemGetTipsByType(tabIdx)
end

function CItemNFTPropPart.InitDropList( self )
	-- body
	self.m_LocationPopupBox:AddSubMenu("All", 1, "bg_dijidi_mengban")
	self.m_LocationPopupBox:AddSubMenu("Valarion", 2, "bg_dijidi_mengban")
	self.m_LocationPopupBox:AddSubMenu("Gear", 3, "bg_dijidi_mengban")
	self.m_LocationPopupBox:AddSubMenu("Miscellaneous", 4, "bg_dijidi_mengban")

	self.m_LocationPopupBox:ChangeSelectedIndex(1)
	self.m_LocationPopupBox:SetOffsetHeight(-35)
	self.m_LocationPopupBox:SetCallback(callback(self, "SetLocation"))
	--self.m_RefreshInfoLabel:SetText(self.m_RankInfo.refresh_tips)
end

function CItemNFTPropPart.SetCurSellSelectActive(self, b)
    --printDebug("is sellect:",b)
	if self.m_SellSelectBagBox then
		self.m_SellSelectBagBox:SetSellSelectActive(b)
	end
end

function CItemNFTPropPart.SetLocation(self, oBox)
	--printDebug("is sellect:",oBox:GetSelectedSubMenu().m_ExtraData )
	self:FillterItemNFT(oBox:GetSelectedSubMenu().m_ExtraData)
	-- self.m_Location = oBox:GetSelectedSubMenu().m_ExtraData
	-- local page = math.ceil(oBox:GetSelectedSubMenu().m_ExtraData / self.m_RankInfo.per_page)
	-- if g_RankCtrl:GetDataFromServer(self.m_CurrentRankId, page, self.m_PartnerType) then
	-- 	self:ShowGettingData(page)
	-- end
end

function CItemNFTPropPart.UpdataTabText(self)
	-- for i = 1, #self.m_TabGird.m_TabText do		
	-- 	if self.m_TabIndex == i then
	-- 		self.m_TabGird.m_TabText[i]:SetActive(false)
	-- 	else
	-- 		self.m_TabGird.m_TabText[i]:SetActive(true)
	-- 	end
	-- end
end
function CItemNFTPropPart.RefreshAll(self)
	local oView = CItemTipsBaseInfoView:GetView()
	if oView then
		oView:Close()
	end
	self:RefreshData(true)
end

function CItemNFTPropPart.RefreshData(self , isReposition)
	local itemCount = #g_NFTCtrl.m_BagTabItemsCache
	--local itemRow = math.ceil( itemCount / PerColNumber) + 1
	--暂时显示的行数，和道具当前的行数一样
	local itemRow = math.ceil( itemCount / PerColNumber)
	itemRow = math.max(itemRow, CItemNFTPropPart.Config.GridRowMin) 
	itemRow = math.min(itemRow, CItemNFTPropPart.Config.GridRowMax) 
	
	local d = {}
	for i = 1, itemRow do
		local t = {row = i}
		table.insert(d, t)
	end

	local list = self.m_WrapContent:GetChildList()
	for i = 1, #list do
		local oChild = list[i]
		if oChild then
			oChild.m_isInit = false
		end
	end

    --printDebug("set items: ",d)
	self.m_WrapContent:SetData(d, true)

	if isReposition ~= false then
		--重置ScrollView位置		
		self.m_ItemScrollView:ResetPosition()
		-- UITools.MoveToTarget(self.m_ItemScrollView, self.m_BagItemGrid:GetChild(1))
	end	
	self.m_NonePropWidget:SetActive(itemCount == 0)
end


function CItemNFTPropPart.InitWarpContent(self)
	self.m_WrapContent:SetCloneChild(self.m_ItemCloneHorGrid, 
		function(oChild)
			oChild.m_ItemBoxTable = {}
			oChild:InitChild(function ( obj, idx )
				local oBox = CItemBagBox.New(obj)
				oBox:SetGroup(self.m_WrapContent:GetInstanceID())
				oChild.m_ItemBoxTable[idx] = oBox		
				oChild.m_isInit = false		
				return oBox
			end)	
			return oChild
		end)

	self.m_WrapContent:SetRefreshFunc(function(oChild, dData)
		if dData and dData.row then		
			oChild:SetActive(true)	
			if oChild.m_Row == dData.row then
				if oChild.m_isInit == true then
					return
				end
			end
	
			oChild.m_isInit = true
			oChild.m_Row = dData.row
			local bagData = g_NFTCtrl.m_BagTabItemsCache
			--出售缓存列表
			local sellCacheList = g_NFTCtrl.m_RecordSellItemCache[self.m_TabIndex]			
			for i = 1, PerColNumber do
				if oChild.m_ItemBoxTable[i] then
					local index = (dData.row - 1) * PerColNumber + i
					oChild.m_ItemBoxTable[i]:SetIndexAndParentview(index, self.m_ParentView)

					local oItem  = bagData[index]
					oChild.m_ItemBoxTable[i]:SetBagItem(nil)
					if oItem then
						oChild.m_ItemBoxTable[i]:SetBagItem(oItem)

						--背包状态在出售状态
						if g_NFTCtrl.m_RecordItembBagViewState == 2 then
							--价钱为0，显示不可出售
							local price = oItem:GetValue("sale_price") or 0
							oChild.m_ItemBoxTable[i]:SetItemStateCanSell(price ~= 0)				
							--当前处于出售状态下时，当出售缓存有内容，则显示出售的数量	
							oChild.m_ItemBoxTable[i]:SetSellWidgetActive(true)
							oChild.m_ItemBoxTable[i]:SetSellCounttext(0)			
							if sellCacheList ~= nil then
								for k, v in pairs(sellCacheList) do			
									if v[3] == oItem:GetValue("id") then
										local sellcount = v[2]
										oChild.m_ItemBoxTable[i]:SetSellCounttext(sellcount)			
										break
									end
								end
							end

						--背包状态在正常状态
						else
							--隐藏无法出售的状态				
							oChild.m_ItemBoxTable[i]:SetItemStateCanSell(true)
							oChild.m_ItemBoxTable[i]:SetSellWidgetActive(false)
						end
					end

					if self.m_SellSelectIndex == index then
						oChild.m_ItemBoxTable[i]:SetSelected(true)
					else
						oChild.m_ItemBoxTable[i]:SetSelected(false)
					end

				end				
			end	
		else
			oChild:SetActive(false)
		end
	end)
end

function CItemNFTPropPart.FillterItemNFT( self,iFilter )
	-- if iFilter ==1 then
	-- 	g_NFTCtrl.m_BagTabItemsCache = g_NFTCtrl:GetAllBagItemsByShowTypeAndSort(1, 1, false)
	-- elseif iFilter== 2 then
	-- 	local itms = g_NFTCtrl:GetAllBagItemsByShowTypeAndSort(2, 1, false)
	-- 	local tab = {}
	-- 	for _m,n in pairs(itms) do
	-- 		-- body
	-- 		if n:GetType()== 0 then
	-- 			table.insert( tab,n)
	-- 		end
	-- 	end
	-- 	g_NFTCtrl.m_BagTabItemsCache = tab
	-- elseif iFilter== 3 then
	-- 	local itms = g_NFTCtrl:GetAllBagItemsByShowTypeAndSort(3, 1, false)
	-- 	local tab = {}
	-- 	for _m,n in pairs(itms) do
	-- 		-- body
	-- 		if n:GetType()== 1 and  n:GetValue("bag_show_type")==3 then
	-- 			table.insert( tab,n)
				
	-- 		end
	-- 	end
	-- 	g_NFTCtrl.m_BagTabItemsCache = tab
	
	-- elseif iFilter== 4 then
	-- 	local itms = g_NFTCtrl:GetAllBagItemsByShowTypeAndSort(4, 1, false)
	-- 	local tab = {}
	-- 	for _m,n in pairs(itms) do
	-- 		-- body
	-- 		if n:GetType()== 1 and  n:GetValue("bag_show_type")~=3 then
	-- 			table.insert( tab,n)
				
	-- 		end
	-- 	end
	-- 	g_NFTCtrl.m_BagTabItemsCache = tab
	-- end
	-- self:RefreshAll()
	-- body
end

function CItemNFTPropPart.ValueChangeCallback(self, obj, tType, ...)
	-- if obj:GetInstanceID() ~= self.m_ParentView:GetInstanceID() then
	-- 	return
	-- end
	-- local arg1 = select(1, ...)
	-- local arg2 = select(2, ...)
	-- local arg3 = select(3, ...)	
	-- local arg4 = select(4, ...)	
	-- local arg5 = select(5, ...)	
	-- if  tType == "ShowSellInfo" then
	-- 	local sortIndex = g_ItemCtrl.m_RecordItembBagSortTypeCache[self.m_TabIndex]
	-- 	local sort = CItemBagSellInfoPart.EnumSort[self.m_TabIndex][sortIndex]
	-- 	local isSell = (g_ItemCtrl.m_RecordItembBagViewState == 2)
	-- 	g_ItemCtrl.m_BagTabItemsCache = g_ItemCtrl:GetAllBagItemsByShowTypeAndSort(self.m_TabIndex, sort, isSell)		
	-- 	--清除当前选中的出售道具
	-- 	self.m_SellSelectIndex = 0
	-- 	self.m_SellSelectItem = nil
	-- 	self:SetCurSellSelectActive(false)
	-- 	self.m_SellSelectBagBox = nil

	-- 	--出售时，默认选中某个道具，为0或者为空，不处理
	-- 	if isSell == true and arg2 and arg2 ~= 0 then
	-- 		local bagData = g_ItemCtrl.m_BagTabItemsCache			
	-- 		for i,v in ipairs(bagData) do
	-- 			if v:GetValue("id") == arg2 and v:IsEuqipLock() == false then
	-- 				g_ItemCtrl.m_RecordSellItemCache[self.m_TabIndex][i] = {v, 1, arg2}
	-- 				self.m_SellSelectIndex = i
	-- 				self.m_SellSelectItem = v
	-- 				self.m_SellSelectBagBox = arg5
	-- 				self:SetCurSellSelectActive(true)
	-- 				self.m_ParentView:OnValueChange("SelectSellItem", true, i, 1, v)
	-- 				break
	-- 			end
	-- 		end			
	-- 	end

	-- 	self:RefreshAll()			
	-- 	self:SetBatBtnVisible()		

	-- elseif tType == "SelectSellItem" then
	-- 	-- arg1 表示选中还是取消选中
	-- 	-- arg2 表示当前背包类型下，该道具的序号
	-- 	-- arg3 表示当前物品出售的个数(如果当前为取消状态下时，忽略)
	-- 	-- arg4 物品信息
	-- 	if arg1 == true then
	-- 		--缓存出售道具的数量

	-- 		--TODO 获取该标签下，该位置的道具信息
	-- 		g_ItemCtrl.m_RecordSellItemCache[self.m_TabIndex][arg2] = {arg4, arg3, arg4:GetValue("id")}
	-- 		-- printc("打印批量列表.....................................开始")
	-- 		-- for t, list in pairs (g_ItemCtrl.m_RecordSellItemCache) do
	-- 		-- 	for k , v in pairs (list) do
	-- 		-- 		local tData = v
	-- 		-- 		local tItem = tData[1]
	-- 		-- 		local count = tData[2]	
	-- 		-- 		printc("tItem ", type(tItem))		
	-- 		-- 		printc(string.format(" 标签 = %d,  序号 = %d, 数量 = %d", t, k, count))
	-- 		-- 	end 
	-- 		-- end
	-- 		-- printc("打印批量列表.....................................结束")

	-- 		--记录当前选中的出售道具
	-- 		self.m_SellSelectIndex = arg2
	-- 		self.m_SellSelectItem = arg4
	-- 		self:SetCurSellSelectActive(false)
	-- 		self.m_SellSelectBagBox = arg5
	-- 		self:SetCurSellSelectActive(true)
	-- 	else
	-- 		--从缓存中取出取消选中的道具的数量
	-- 		g_ItemCtrl.m_RecordSellItemCache[self.m_TabIndex][arg2] = nil

	-- 		--清除当前选中的出售道具
	-- 		self.m_SellSelectIndex = 0
	-- 		self.m_SellSelectItem = nil
	-- 		self:SetCurSellSelectActive(false)
	-- 		self.m_SellSelectBagBox = nil
	-- 	end	

	-- elseif tType == "ChangeSellCount" then
	-- 	local tData = g_ItemCtrl.m_RecordSellItemCache[self.m_TabIndex][arg1]
	-- 	tData[2] = arg2
	-- 	g_ItemCtrl.m_RecordSellItemCache[self.m_TabIndex][arg1] = tData
	-- 	--self.m_ItemBtnList[arg1]:SetSellCounttext(arg2)
	-- 	local oItemBox = self:GetTargetItemBox(arg1)
	-- 	if oItemBox then
	-- 		oItemBox:SetSellCounttext(arg2)
	-- 	end

	-- elseif tType == "OnSort" then
	-- 	local tab = g_ItemCtrl.m_RecordItemPageTab
	-- 	local sortIndex = g_ItemCtrl.m_RecordItembBagSortTypeCache[tab]
	-- 	local sort = CItemBagSellInfoPart.EnumSort[tab][sortIndex]
	-- 	local isSell = (g_ItemCtrl.m_RecordItembBagViewState == 2)
	-- 	g_ItemCtrl.m_BagTabItemsCache = g_ItemCtrl:GetAllBagItemsByShowTypeAndSort(self.m_TabIndex, sort, isSell)
	-- 	self:RefreshAllItem()
	-- end
end

function CItemNFTPropPart.OnSellectItem( self,items,index_tab )
    -- body
    self.m_BatSelectBtn:SetActive(false)
	-- self.m_ConvertBtn:SetActive(false)
	self.m_SellSelectBagBox = items
	if (index_tab ==1) then
		self:OnBatSelect()
		printDebug("CItemNFTPropPart.OnSellectItem",items:GetValue("parid"))
		-- printDebug("CItemNFTPropPart.OnSellectItem",items)
		self.m_BatSelectBtn:SetActive(false)
		return
	end
	if (index_tab ==3) then
		printDebug("CItemNFTPropPart.OnSellectItem",items:GetValue("id"))
		self:OnBatSelect()
		-- printDebug("CItemNFTPropPart.OnSellectItem",items)
		self.m_BatSelectBtn:SetActive(false)
		return
	end
    if index_tab ==4 then
		if items ~=nil and items:GetValue("nftstatus") == 3 then
			self.m_BatSelectBtn:SetActive(false)
        else
            self.m_BatSelectBtn:SetText("Withdrawal")
			self.m_BatSelectBtn:SetActive(false)
        end
    else
        
        if items ~=nil and items:GetValue("nfttoken") == nil then
            self.m_BatSelectBtn:SetText("Casting NFT")
			self.m_BatSelectBtn:SetActive(false)
        else
            self.m_BatSelectBtn:SetText("Storage")
			self.m_BatSelectBtn:SetActive(false)
        end
    end
end

return CItemNFTPropPart