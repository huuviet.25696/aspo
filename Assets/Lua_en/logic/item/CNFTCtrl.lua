local CNFTCtrl = class("CNFTCtrl", CCtrlBase)

function CNFTCtrl.ctor(self)
	CCtrlBase.ctor(self)

    self.m_RecordItemPageTab = 0	
    self.m_BagTabItemsCache = {}
    self.m_RecordItembBagViewState = 0

    self.m_RecordSellItemCache = {
		[1] = {},
		[2] = {},
		[3] = {},
		[4] = {},
	}
	-- self.m_RefreshTimer = nil
	-- self.m_RefreshPriceTimer = nil
	-- self.m_RefreshFuwenTimer = nil

	-- -- 背包
	-- -- 背包分页记录
	-- -- 0表示不在背包界面
	-- -- 1表示在背包普通标签，2表示背包材料标签，3表示背包宝石标签，4表示背包装备标签
	-- self.m_RecordItemPageTab = 0	

	-- -- 背包画面当前属于哪个状态
	-- -- 0表示当前不在背包界面，1表示背包在正常预览状态，2表示背包在出售状态
	-- self.m_RecordItembBagViewState = 0

	-- --背包道具的分类
	-- self.m_BagType = 
	-- {
	-- 	[1] = define.Item.ItemBagShowType.Genernal, 	--普通
	-- 	[2] = define.Item.ItemBagShowType.Material, 	--材料
	-- 	[3] = define.Item.ItemBagShowType.Equip, 		--装备灵石(客户端显示装备)
	-- 	[4] = define.Item.ItemBagShowType.Partner, 		--伙伴
	-- 	[5] = define.Item.ItemBagShowType.Chip, 		--碎片
	-- }

	-- -- 背包出售道具队列缓存
	-- --[[格式类型： 
	-- 	{	标签类型	 序号	 道具信息 出售数量
	-- 		[type] = {  [indx] = {[1] = titem, [2] = count} },
	-- 	}
	-- ]]	
	-- self.m_RecordSellItemCache = {
	-- 	[1] = {},
	-- 	[2] = {},
	-- 	[3] = {},
	-- 	[4] = {},
	-- }

	-- --背包排序(每个标签单独控制)
	-- --(key = 1 普通标签，2 材料标签，3 装备标签, 4 伙伴标签 5 碎片标签
	-- self.m_RecordItembBagSortTypeCache = 
	-- {
	-- 	[1] = 1,
	-- 	[2] = 1,
	-- 	[3] = 1,
	-- 	[4] = 1,
	-- 	[5] = 1,
	-- }
	-- self.m_IsInitBagSortType = false	--加载背包标签序号(每次重登第一次打开背包时加载)

	-- --背包总列表
	-- self.m_BagItems = {}

	-- --背包分类列表(标签变化，排序变化，道具发生变动, 出售状态切换 才会更新)
	-- self.m_BagTabItemsCache = {}

	-- --材料的价格缓存，每次得到的价格都缓存在这里。格式 sid = price
	-- self.m_MaterailPriceCache = {}

	-- --装备
	-- self.m_EquipedItems = {}

	-- --装备强化画面，元宝自动填充材料
	-- self.m_ForgeStrengthAutoFill = false 

	-- --装备符文，元宝自动填充材料
	-- self.m_ForgeFuwenAutoFill = false 

	-- --当前出售的道具的系统id
	-- self.m_CurSellItemId = 0
	-- --当前使用的道具的系统id
	-- self.m_CurUseItemId = 0 

	-- --背包整理计时
	-- self.m_ArrangeItemTimer = 0

	-- --背包红点提示id(系统id)缓存
	-- self.m_RedDotIdTable = {}

	-- --快捷使用道具(系统id)缓存
	-- self.m_QuickUseIdCache = {}
	-- self.m_QuickUseTimer = nil

	-- self.m_ForgeFuwenName = {}

	-- --显示属性变化飘字相关
	-- if self.m_ShowAttrTimer ~= nil then
	-- 	Utils.DelTimer(self.m_ShowAttrTimer)
	-- 	self.m_ShowAttrTimer = nil
	-- end
	-- self.m_ShowAttrCacheKey = {}
	-- self.m_ShowAttrCacheAttr = {}
	-- self.m_ShowAttrChangeFlag = false
end

function CNFTCtrl.GetAllBagItemsByShowTypeAndSort(self, iShowType, sort, isSell)
	local t = {}
    if iShowType ==1 then
        local lstParner= g_PartnerCtrl:GetPartnerNoFight()
        for _,v in pairs(lstParner) do
			if v:GetValue("nftstatus") ==1 then
				table.insert( t, v )
			end
        end
    elseif iShowType ==2 then
        local allEquip = g_ItemCtrl:GetAllBagItemsByShowTypeAndSort(define.Item.ItemBagShowType.Equip, define.Item.SortType.Level)
        --printDebug("soluong item: ",#allEquip)
        for _,v in pairs(allEquip) do
			--printDebug("soluong item: ",v)
			--printDebug("item is nft:",v:GetValue("isNFT"))
            if v:GetValue("isNFT") ==1 then
                if v:GetValue("nftstatus") ==1 then
                    table.insert( t, v )
                end
            end
        end
        --printDebug("soluong item sort: ",#t)
    elseif iShowType ==3 then
        local allItems = g_ItemCtrl:GetItemNonEquipNFT()
        t = allItems

    elseif iShowType ==4 then
        local Process = function (itmProgress)
            -- body
            if  itmProgress:GetValue("nftstatus") ~= nil and itmProgress:GetValue("nftstatus") ==3  then
                table.insert( t, itmProgress )
            end
        end

        local lstParner= g_PartnerCtrl:GetPartnerNoFight()
        for _,v in pairs(lstParner) do
            Process(v)
        end

        local items = g_ItemCtrl:GetAllItems()
        for _,i in pairs(items) do
            Process(i)
        end

    end
	-- for _, v in pairs(self.m_BagItems) do
	-- 	local Process = function ( )		
	-- 		if v:GetValue("bag_show_type") == iShowType then
	-- 			table.insert(t, v)
	-- 		end
	-- 	end
	-- 	if isSell == true then
	-- 		if v:GetValue("sale_price") ~= 0 and not v:IsEuqipLock() then
	-- 			Process()
	-- 		end
	-- 	else
	-- 		Process()
	-- 	end
	-- end
	
	-- --如果是数量排序为主排序，则需要重置物品的组合数量
 	-- if sort == define.Item.SortType.Amount then
 	-- 	self:RefreshBagItemsGroupAmount(t)
 	-- end

	-- --普通类型物品排序
	-- if iShowType == define.Item.ItemBagShowType.Genernal then
	-- 	if sort == define.Item.SortType.Sid then			
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.sid,
	-- 			[2] = CItemCtrl.tSortPool.groupAmount,
	-- 			[3] = CItemCtrl.tSortPool.state,
	-- 			[4] = CItemCtrl.tSortPool.time,
	-- 		}))		
	-- 	elseif sort == define.Item.SortType.Itemlevel then			
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.itemLevel,
	-- 			[2] = CItemCtrl.tSortPool.sort,
	-- 			[3] = CItemCtrl.tSortPool.sid,
	-- 			[4] = CItemCtrl.tSortPool.state,
	-- 			[5] = CItemCtrl.tSortPool.amount,
	-- 			[6] = CItemCtrl.tSortPool.time,
	-- 		}))
	-- 	elseif sort == define.Item.SortType.Type then			
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.sort,
	-- 			[2] = CItemCtrl.tSortPool.sid,
	-- 			[3] = CItemCtrl.tSortPool.state,
	-- 			[4] = CItemCtrl.tSortPool.amount,
	-- 			[5] = CItemCtrl.tSortPool.time,				
	-- 		}))
	-- 	elseif sort == define.Item.SortType.Amount then			
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			--数量排序，组合排序之后，先比较sid，在比较单个物品的数量
	-- 			[1] = CItemCtrl.tSortPool.groupAmount,
	-- 			[2] = CItemCtrl.tSortPool.sid,
	-- 			[3] = CItemCtrl.tSortPool.stateGroupAmount,   --同一状态的道具，组合排序
	-- 			[4] = CItemCtrl.tSortPool.state,		
	-- 			[5] = CItemCtrl.tSortPool.amount,
	-- 			[6] = CItemCtrl.tSortPool.time,	
	-- 		}))
	-- 	else 
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.time,	
	-- 			[2] = CItemCtrl.tSortPool.amount,					
	-- 		}))
	-- 	end		

	-- --材料类型物品排序
	-- elseif iShowType == define.Item.ItemBagShowType.Material then
	-- 	if sort == define.Item.SortType.Sid then			
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.sid,
	-- 			[2] = CItemCtrl.tSortPool.groupAmount,
	-- 			[3] = CItemCtrl.tSortPool.state,
	-- 			[4] = CItemCtrl.tSortPool.time,
	-- 		}))			
	-- 	elseif sort == define.Item.SortType.Level then	
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.level,
	-- 			[2] = CItemCtrl.tSortPool.sort,
	-- 			[3] = CItemCtrl.tSortPool.sid,
	-- 			[4] = CItemCtrl.tSortPool.state,
	-- 			[5] = CItemCtrl.tSortPool.amount,
	-- 			[6] = CItemCtrl.tSortPool.time,				
	-- 		}))
	-- 	elseif sort == define.Item.SortType.Type then			
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.sort,
	-- 			[2] = CItemCtrl.tSortPool.sid,
	-- 			[3] = CItemCtrl.tSortPool.state,				
	-- 			[4] = CItemCtrl.tSortPool.amount,
	-- 			[5] = CItemCtrl.tSortPool.time,					
	-- 		}))
	-- 	elseif sort == define.Item.SortType.Amount then	
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			--数量排序，组合排序之后，先比较sid，在比较单个物品的数量
	-- 			[1] = CItemCtrl.tSortPool.groupAmount,	
	-- 			[2] = CItemCtrl.tSortPool.sid,
	-- 			[3] = CItemCtrl.tSortPool.stateGroupAmount,    --同一状态的道具，组合排序
	-- 			[4] = CItemCtrl.tSortPool.state,		
	-- 			[5] = CItemCtrl.tSortPool.amount,
	-- 			[6] = CItemCtrl.tSortPool.time,				
	-- 		}))
	-- 	else 
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.time,
	-- 			[2] = CItemCtrl.tSortPool.amount,					
	-- 		}))
	-- 	end		
	-- elseif iShowType == define.Item.ItemBagShowType.Equip then
	-- 	if sort == define.Item.SortType.Equip then			
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.canEquipLevel,
	-- 			[2] = CItemCtrl.tSortPool.greateSelfScore,
	-- 			[3] = CItemCtrl.tSortPool.baseScore,
	-- 			[4] = CItemCtrl.tSortPool.time,
	-- 		}))			
	-- 	elseif sort == define.Item.SortType.Level then	
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.level,
	-- 			[2] = CItemCtrl.tSortPool.itemLevel,
	-- 			[3] = CItemCtrl.tSortPool.pos,
	-- 			[4] = CItemCtrl.tSortPool.sid,	
	-- 			[5] = CItemCtrl.tSortPool.state,	
	-- 			[6] = CItemCtrl.tSortPool.time,
	-- 		}))
	-- 	elseif sort == define.Item.SortType.Pos then	
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.pos,
	-- 			[2] = CItemCtrl.tSortPool.level,
	-- 			[3] = CItemCtrl.tSortPool.itemLevel,
	-- 			[4] = CItemCtrl.tSortPool.sid,	
	-- 			[5] = CItemCtrl.tSortPool.state,				
	-- 			[6] = CItemCtrl.tSortPool.time,				
	-- 		}))
	-- 	elseif sort == define.Item.SortType.Itemlevel then
	-- 		table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.itemLevel,
	-- 			[2] = CItemCtrl.tSortPool.level,
	-- 			[3] = CItemCtrl.tSortPool.pos,
	-- 			[4] = CItemCtrl.tSortPool.sid,
	-- 			[5] = CItemCtrl.tSortPool.state,	
	-- 			[6] = CItemCtrl.tSortPool.time,					
	-- 		}))
	-- 	else
	-- 		table.sort(t, self:ItemSortTypeFunc({			
	-- 			[1] = CItemCtrl.tSortPool.time,								
	-- 		}))
	-- 	end
	-- -- 伙伴型物品排序
	-- elseif iShowType == define.Item.ItemBagShowType.Partner then
	-- 	table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.pos,
	-- 			[2] = CItemCtrl.tSortPool.sid,
	-- 			[3] = CItemCtrl.tSortPool.time,					
	-- 		}))

	-- -- 碎片类型物品排序
	-- elseif iShowType == define.Item.ItemBagShowType.Chip then
	-- 	table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.rare,
	-- 			[2] = CItemCtrl.tSortPool.sid,
	-- 			[3] = CItemCtrl.tSortPool.time,					
	-- 		}))
	-- -- 其他类型物品排序
	-- else 
	-- 	table.sort(t, self:ItemSortTypeFunc({
	-- 			[1] = CItemCtrl.tSortPool.sid,
	-- 			[2] = CItemCtrl.tSortPool.time,					
	-- 		}))
	-- end

	return t		
end



return CNFTCtrl