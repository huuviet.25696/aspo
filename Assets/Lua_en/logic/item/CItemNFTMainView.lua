local CItemNFTMainView = class("CItemNFTMainView", CViewBase)
    function CItemNFTMainView.ctor(self, cb)
        CViewBase.ctor(self, "UI/Item/NFTItemBagMainView.prefab", cb)
        --界面设置
        self.m_DepthType = "Dialog"
        --self.m_GroupName = "main"
        self.m_ExtendClose = "Black"
        self.m_OpenEffect = "Scale"
        self.m_RegDict = {}
        self.m_RegList = {}
    end
    function CItemNFTMainView.OnCreateView(self)

        -- g_ItemCtrl.m_RecordItembBagViewState = 1
         self.m_CloseBtn = self:NewUI(1, CButton)
         self.m_MainItemPart = self:NewUI(2, CItemNFTPropPart, true, self)	
         self.m_BagWealthPart = self:NewUI(3, CItemBagWealthInfoPart, true, self)
        -- self.m_SellInfoPart = self:NewUI(4, CItemBagSellInfoPart, true, self)
        
        self:InitContent()
    end
    function CItemNFTMainView.InitContent(self)
        self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))
    end
    function CItemNFTMainView.SetValueChangeCallback( self ,regid, callback)

        -- self.m_RegDict[regid] = callback
        -- table.insert(self.m_RegList, regid)
    
    end
    function CItemNFTMainView.OnValueChange(self,items)

      
        if items ~= nil  and self.m_MainItemPart ~=nil then
            -- printDebug("CItemNFTMainView.OnValueChange",self.m_MainItemPart.m_TabIndex)
            self.m_MainItemPart:OnSellectItem(items,self.m_MainItemPart.m_TabIndex)
        end
       
        
    end

return CItemNFTMainView		