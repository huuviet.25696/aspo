local config = {}

config.arg = {}

config.arg.template = 
{
	member_cnt = {
		name = "Number of teams",
		key = "member_cnt",
		format = "number_type",
		default = 0,
	},
	partner_cnt = {
		name = "Số Hồn sư đội trưởng",
		key = "partner_cnt",
		format = "number_type",
		default = 1,
	},
	lineup_type = {
		name = "Type of location",
		key = "lineup_type",
		select_update = function()
			return table.values(data.lineupdata.LINEUP_TYPE)
		end,
		force_input = true,
		default = "single",
	},
}

return config