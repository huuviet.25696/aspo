module(..., package.seeall)

select =
{
	bool_type = {
		{true, "Yes"},
		{false, "No"},
		{nil, "No"}
	},
	effect_type = {
		{"dlg", "Chat box"},
		{"texture", "Image"},
		{"focus_ui", "Outstanding button"},
		{"focus_pos", "Outstanding location"},
		{"click_ui", "Notification button"},
		{"teach_guide", "Teaching Interface"},
		{"focus_common", "Common bright spot"},
		{"open", "Unlock"},
		{"spine", "Simpe type"},
		{"none", "Processing empty"},
		{"hide_click_event", "Click to hide the location"},
		{"bigdlg", "Type of chat box"},
		{"textdlg", "Type of chat text"},

	},
	ui_key = {
		{"", "Not installed"},
		{"mainmenu_operate_btn", "Operating Main interface button"},
		{"mainmenu_house_btn", "Family Main interface button"},
		{"operate_drawcard_btn", "Recruit button"},
		{"operate_arena_btn", "Arena button"},		
		{"operate_map_book_btn", "Appraise button"},	
		{"operate_pata_btn", "Prison button"},		
		{"operate_org_btn", "Guild button"},	
		{"operate_welfare_btn", "Benefit button"},							
		{"operate_skill_btn", "Soul button"},	
		{"operate_arnea_btn", "Combat button"},									
		{"draw_wl_card", "Martial Artifact Card"},
		{"draw_wh_card", "Martial Soul Card"},
		{"draw_wh_card_again", "Martial Soul Card button?"},
		{"drawcard_close_rt", "The upper right button is the button to close draw interface"},
		{"drawcard_close_lb", "The lower right button is the button to close draw interface"},
		{"close_wh_result_rt", "The upper right button is the button to close Royal draw result interface"},
		{"close_wh_result_lb", "The lower right button is the button to close Royal draw result interface"},
		{"war_order_all",  "Complete command button"},
		{"close_wl_result_rt", "The upper right button is draw result of hero"},
		{"close_wl_result_lb", "The lower right button is draw result of hero"},
		{"skill_point_label", "Text Soul Skill"},
		{"skill_switch_btn", "Change character"},
		{"skill_learn_btn", "Study the soul well"},
		{"skill_des_other_label", "Other soulful description content"},	
		{"skill_skillbtn_1", "Soul skill interface button 1"},
		{"skill_skillbtn_2", "Soulful interface button 2"},
		{"skill_skillbtn_3", "3 . soulful interface button"},
		{"skill_skillbtn_4", "4 . soulful interface button"},
		{"skill_skillbtn_5", "Soulful interface button"},
		{"skill_skillbtn_6", "6 . spirit interface button"},
		{"yuejian_help_btn", "Yuan Tian help button"},
		{"pata_monster_texture", "Prison Monster  model"},
		{"arena_help_btn", "Arena help button"},
		{"arena_fight_btn", "Arena combat button"},		
		{"confirm_ok_btn", "Confirm button."},
		{"confirm_cancel_btn", "Cancel button"},
		{"mainmenu_schedule_btn", "Main interface schedule button"},
		{"mainmenu_powerguide_btn", "Main interface Elf button game"},
		{"mainmenu_anlei_btn", "Main interface thunder button"},
		{"mainmenu_dailycultivate_btn", "Main interface Daily cultivating button"},
		{"mainmenu_loginreward_btn", "Login 7 days"},		
		{"mainmenu_partner_btn", "Soul Master button main interface"},	
		{"mainmenu_item_btn", "Main interface pocket opening button"},	
		{"mainmenu_xmqq_task_nv_btn", "Tutorial button of the cute Xiao Wu task main interface"},
		{"mainmenu_minimap_btn", "Main interface map button"},
		{"mainmenu_drawcard_btn", "Main interface draw card button"},		
		{"mainmenu_forge_btn", "Main interface equipment button"},
		{"mainmenu_hunt_btn", "Main interface Soul Hunting  button"},
		{"partner_upgrade_list_1_1_partner_btn", "Box 1-1 is a list of Soul Master training in the Soul Master interface"},			
		{"partner_upgrade_add_partner_btn", "Add button to add Soul Master of Soul Master Cultivation interface"},	
		{"partner_upgrade_ok_btn", "Soul Master interface refresher confirmation button"},	
		{"partner_upgrade_close_btn", "The button to close the Soul Master training interface"},	
		{"partner_main_breed_btn", "Soul Master interface refresher button"},	
		{"partner_equip_tab_btn", "Soul Stone Tab Soul Master interface"},	
		{"partner_lineup_tab_btn", "Tab to battle the Soul Master interface"},
		{"partner_yuling_tab_btn", "Tab of the Soul Master interface"},
		{"partner_lineup_pos_1_btn", "1st position button, Soul Master goes to battle"},
		{"partner_lineup_pos_2_btn", "Button position 2, Soul Master goes to battle"},	
		{"partner_lineup_pos_3_btn", "Button position 3, Soul Master goes to battle"},	
		{"partner_lineup_pos_4_btn", "Position button 4 Soul Masters go to battle"},	
		{"partner_equip_box_add_1", "Sorcerer's Soul Stone interface Button to add the number 1 Soul Stone box"},	
		{"partner_equip_box_item_1", "Soul Stone interface No. 1 Soul Stone button"},	
		{"partner_equip_change_type_btn", "Change the type of Soul Stone"},
		{"partner_equip_list_1_1_fuwen_btn", "The 1st Soul Stone in the list of Sorcerer Soul Stones"},
		{"partner_equip_strong_1_1_fuwen_btn", "Soul Stone 1st list of strengthening Soul Stone Master"},
		{"partner_equip_strong_ok_btn", "Soul Master Stone enhancement confirmation button."},
		{"partner_equip_rightpage_btn", "The page turn button on the right of the Soul Master Stone interface."},
		{"partner_equip_type_label", "The Sorcerer Soul Stone interface describes the Soul Stone equipment set"},
		{"partner_equip_replace_btn", "Button to replace the interface tips Stone Soul Master"},
		{"partner_equip_strong_btn", "Button strengthens the interface tips of the Sorcerer's Stone"},
		{"partner_cost_buy_fuwen_btn", "Buy button Soul Stone buy interface"},
		{"partner_equip_buy_fuwen_btn", "Button to buy Soul Stone"},
		{"partner_equip_1_1_fuwen_btn", "Soul Stone button position 1_1 in the list of Soul Stones"},
		{"partner_equip_left_pos_1_add_btn", "Sorcerer's Stone 1, add button on the left"},
		{"partner_equip_left_pos_1_fuwen_btn", "Soul Stone 1, Left Soul Stone button"},
		{"partner_equip_left_pos_1_add_btn", "Sorcerer's Soul Stone 2, add button on the left"},
		{"partner_equip_left_pos_1_fuwen_btn", "Soul Stone 2, Left Soul Stone button"},
		{"partner_equip_strong_page_upgrade", "Soul Master Stone enhancement interface, Enhance button"},
		{"partner_draw_partner_1_1_btn", "Recruitment interface 1-1"},
		{"partner_draw_partner_confirm_btn", "Recruiting interface Recruit button"},
		{"partner_draw_partner_close_btn", "Close recruiting interface button"},
		{"partner_left_list_501_partner", "Main interface Soul Master Button Soul Master A Phuong"},
		{"partner_soul_type_1_fast_bg", "Dance Soul Master Interface Type 1 Scenario"},
		{"partner_soul_type_1_box_btn", "Dance Soul Master interface 1st type button"},
		{"partner_soul_type_1_fast_equip_btn", "Dance Soul Master Interface Quick Equip Button"},
		{"partner_chip_compose_show_btn", "The entrance to the Soul Master"},
		{"partner_chip_compose_tips_btn", "Announcement of Soul Master"},
		{"partner_choose_partner_403", "Interface to choose Soul Master Snake Soul Master"},		
		{"partner_choose_partner_301", "Interface for selecting Soul Master Spirit Master Ky Lien"},		
		{"partner_choose_partner_501", "Interface to choose Soul Master Soul Master A Phuong"},	
		{"partner_choose_partner_502", "Interface for choosing Soul Master Ma Dien face"},	
		{"partner_gain_close_btn", "The button to close the interface to receive the Soul Master"},	
		{"schedule_award_box_1_btn", "Dynamic Adventure Reward 1"},	
		{"schedule_allday_go_btn", "All day event button"},		
		{"war_first_speed_box", "First action figure"},
		{"war_two_speed_box", "Second action figure"},
		{"yuejian_monster_2", "Full Moon model 2"},
		{"task_nv_btn", "Task introduction"},
		{"hunt_partner_soul_1_1_btn", "Button 1_Soul Hunting list"},
		{"hunt_partner_soul_list_1_btn", "Hunting Soul select Soul Hunting buttion"},		
		{"house_touch_btn", "Family decoration button"},
		{"house_back_btn", "Back to Family button"},			
		{"house_main_door_btn", "Family kitchen table"},	
		{"house_cooker_idx_1_btn", "Family kitchen table button"},	
		{"house_cooker_work_1_btn", "Family Kitchen Chores Button"},	
		{"house_cooker_back_btn", "Back to Family Kitchen Table button"},	
		{"house_main_buff_btn", "Family Buff button"},	
		{"house_main_buff_sprite", "Announcement background of family buff"},	
		{"schedule_everydya_reward_label", "Reward text to-do-list"},	
		{"war_auto_skill_box1", "Soul skill button automatically hits 1"},
		{"war_auto_skill_box2", "Soul skill button automatically hits 2"},
		{"war_auto_skill_box3", "Soul skill button automatically hits 3"},
		{"war_auto_skill_box4", "Soul skill button automatically hits 4"},
		{"war_auto_skill_box5", "Soul skill button automatically hits 5"},
		{"war_select_auto_skill_box1", "Soul skill selection button automatically hits 1"},
		{"war_select_auto_skill_box2", "Self-hit spirit skill selector button 2"},
		{"war_replace_btn", "Combat speed up button"},
		{"war_speed_btn", "Button to change Soul Master when fighting"},
		{"war_speed_tips_bg", "Attack Speed Background Task"},
		{"war_fore_bg_sprite", "Combating sence"},
		{"map_world_map_btn", "Change the world map button"},
		{"map_world_map_city_2_btn", "World map urban button 2"},
		{"mapbook_partner_box", "The entrance to the Soul Master"},
		{"mapbook_main_close_lb", "The bottom left of the Appraising interface"},
		{"mapbook_world_box", "The world source of appraising entrance"},
		{"mapbook_world_city_1_btn", "Appraising the world urban button 2"},
		{"mapbook_world_city_award_btn", "Appraising the world urban reward button"},	
		{"mapbook_world_main_close", "Back to The World Appraising Map button"},	
		{"mapbook_world_main_city_close", "Back to tap of The World Appraising Map button"},		
		{"mapbook_partner_Photo_tab", "the entrance to the tab of the storybook of the Soul Master"},
		{"mapbook_partner_photo_1", "The entrance to the Master's Soul Master Trong Hoa"},		
		{"mapbook_person_1007_reward_btn", "Reward button of TieDan collection"},		
		{"mapbook_reward_view_1007_go_btn     ", "Entrance to appraising rewards button"},		
		{"pefunben_start_btn", "Start Time Travelling button"},
		{"linlianview_go_btn", "Quick active Practicing interface button"},
		{"mainmenu_team_btn", "Main interface button"},
		{"teammain_handybuild_btn", "Quick team button"},
		{"teamhandybuild_target_btn", "Quick team purpose button"},
		{"teamtarget_minglei_btn", "Ming Lei button of the team target"},	
		{"chapter_fuben_btn_1", "Dungeon  Plot Chapter 1"},	
		{"chapter_fuben_btn_2", "Dungeon  Plot Chapter 2"},	
		{"chapter_fuben_btn_3", "Dungeon  Plot Chapter 3"},	
		{"chapter_fuben_reward_btn_3", "Reward of Dungeon Plot 3 button"},	
		{"chapter_fuben_fight_btn", "Challenging Dungeon Plot 3 button"},	
		{"forge_strength_fast_strength_btn", "Quick Enhanceing equipment button"},
		{"forge_gem_fast_mix_btn", "Quick merging Gem button"},	
		{"forge_equip_pos_1", "Equipment Feature 1 button"},	
		{"convoy_refresh_btn", "Refreshing Shipping button"},	
		{"convoy_start_btn", "Starting Shipping button"},	
		{"equipfuben_auto_btn", "Auto Equipment Dungeon Button"},			
		{"quickusew_use_btn", "Quck use interface button"},
		{"dialogue_right_btn_1", "The first button on the right of the dialogue task interface"},
		{"mainmenu_nv_task_10003_btn", "Task button 10003 task board"},
		{"bag_1_1_btn", "1-1 pocket interface"},
		{"use_item_btn", "Using items"},
	},
	condition = {
		{"", "Not installed"},
		{"drawcard_main_show", "Main drawing interface"},
		{"drawcard_result_show", "Drawing result interface"},
		{"teach_view_show", "Unlock Teaching interface"},
		{"teach_view_hide", "Lock Teaching interface"},
		{"house_view_show", "Disdeliver Family interface"},
		{"operate_view_show", "Disdeliver operation button interface"},
	},
	cmd_name ={
		{"Skill", "Magic ( id how to make- Attacked table-Magic ID-Magic code)"},
		{"Damage", "DMG (id-Crit (1 yes 0 no)-DMG"},
		{"Chat", "Chat (id-Content)"},
		{"Wait", "Wait (Waiting time)"},
		{"GoBack", "Location about (id)"},
		{"WarriorStatus", "Soldier Status (id-Survive {1 yes 2 no})"},
		{"BoutStart", "Start turn (Number of turns)"},
		{"BoutEnd", "End of turn (Number of turns)"},
	}
}

select_func = 
{
	texture_name = function() 
		local list = IOTools.GetFiles(IOTools.GetGameResPath("/Texture/Guide"), "*.png", true)
		local newList = {}
		for i, sPath in ipairs(list) do
			table.insert(newList, IOTools.GetFileName(sPath, false))
		end
		table.sort(newList)
		return newList
	end,
	ui_effect = function()
		return {"", "Finger","Finger1","Finger2", "Rect"}
	end,
	dlg_sprite = function()
		return {"pic_zhiying_ditu_1", "pic_zhiying_ditu_2"}
	end,
	dlg_tips_sprite = function ()
		return {"guide_3",}
	end,
	guide_voice_list_1 = function ()
	return {
			"guide_mxm_001_0",
			"guide_mxm_003_3",
			"house_mxm_001_1",
			"house_mxm_001_2",
			"house_mxm_001_3",
			"house_mxm_002_1",
			"house_mxm_002_2",
			"house_mxm_002_3",
			"house_mxm_003_1",
			"house_mxm_003_2",
			"house_mxm_004_1",
			"guide_mxm_001_1",
			"house_mxm_004_2",
			"house_mxm_005_1",
			"house_mxm_005_2",
			"house_mxm_006_1",
			"house_mxm_006_2",
			"house_mxm_007_1",
			"house_mxm_007_2",
			"house_mxm_008_1",
			"house_mxm_008_2",
			"house_mxm_009_1",
			"guide_mxm_001_2",
			"house_mxm_009_2",
			"house_mxm_010_1",
			"house_mxm_010_2",
			"guide_mxm_001_3",
			"guide_mxm_001_4",
			"guide_mxm_002_0",
			"guide_mxm_002_1",
			"guide_mxm_003_1",
			"guide_mxm_003_2",
			"guide_mxm_002_2",
			"guide_mxm_002_3",
			"guide_mxm_003_4",			
		}
	end,
	guide_voice_list_2 = function ()
	return {
			"guide_mxm_001_0",
			"guide_mxm_003_3",
			"house_mxm_001_1",
			"house_mxm_001_2",
			"house_mxm_001_3",
			"house_mxm_002_1",
			"house_mxm_002_2",
			"house_mxm_002_3",
			"house_mxm_003_1",
			"house_mxm_003_2",
			"house_mxm_004_1",
			"guide_mxm_001_1",
			"house_mxm_004_2",
			"house_mxm_005_1",
			"house_mxm_005_2",
			"house_mxm_006_1",
			"house_mxm_006_2",
			"house_mxm_007_1",
			"house_mxm_007_2",
			"house_mxm_008_1",
			"house_mxm_008_2",
			"house_mxm_009_1",
			"guide_mxm_001_2",
			"house_mxm_009_2",
			"house_mxm_010_1",
			"house_mxm_010_2",
			"guide_mxm_001_3",
			"guide_mxm_001_4",
			"guide_mxm_002_0",
			"guide_mxm_002_1",
			"guide_mxm_003_1",
			"guide_mxm_003_2",
			"guide_mxm_002_2",
			"guide_mxm_002_3",
			"guide_mxm_003_4",
		}
	end,	
	spine_left_shape = function ()
		return {" ", "1752"}
	end	,
	spine_right_shape = function ()
		return {" ", "1752"}
	end	,	
	spine_left_motion = function ()
		return {"chashou", "dazhaohu", "jushou", "idle"}
	end	,	
	spine_right_motion = function ()
		return {"chashou", "dazhaohu", "jushou", "idle"}
	end	,	
}

arg = {}
arg.template = {
	sel_type = {
		key = "sel_type",
		name = "Main Type",
		select_update = function ()
			local list = {}
			for k, v in pairs(data_config) do
				table.insert(list, k)
			end
			table.sort(list)
			return list
		end,
		wrap  = function (s)
			if data_config[s] then
				return data_config[s].name
			else
				return s
			end
		end,
		default = "magic",
		change_refresh = true,
	},
	sel_key = {
		key = "sel_key",
		name = "Extra Type",
		select_update = function ()
			local oView = CEditorTableView:GetView()
			local list = {}
			for k, v in pairs(data_config[oView.m_UserCache.sel_type].modify_table) do
				table.insert(list, k)
			end
			table.sort(list)
			return list
		end,
		wrap = function(sOri)
			local oView = CEditorTableView:GetView()
			local sNew = data_config[oView.m_UserCache.sel_type].modify_table[sOri].name
			if sNew then
				return sNew
			else
				return sOri
			end
		end,
		change_refresh = true,
	},
}

dict_open = function(sKey, sName, sFunc, sTrigger)
	local d = {
			key = sKey,
			name = sName,
			preview_func = function(dNew)
				data.guidedata[sKey] = dNew
				data.guidedata.FuncMap[sFunc] = function()
					return true
				end
				g_GuideCtrl:LoginInit({})
				g_GuideCtrl:TriggerCheck(sTrigger)
			end,
	}
	return d
end

dict_view = function (sKey, sName, cls)
	local d = {
				key = sKey,
				name = sName,
				preview_func = function(dNew)
					data.guidedata[sKey] = dNew
					cls:CloseView()
					g_GuideCtrl:LoginInit({})
					cls:ShowView()
				end,
			}
	return d
end


data_config = {
	guide ={
		name="Newbie instruction",
		path="/Lua/logic/data/guidedata.lua",
		modify_table = {
			War1 = {
				key = "War1",
				name = "Combat instruction 1",
				preview_func = function(dNew)
					g_GuideCtrl:LoginInit({})
					war_id = warsimulate.war_id
					data.guidedata.War1 = dNew
					warsimulate.Start(1, 302, 2100, 10001)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 4, name="test", parid=9001, pos = 2, owner=1, status={auto_skill=50702, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 5, name="test", parid=9002, pos = 5, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					netwar.GS2CWarBoutStart({war_id = war_id, bout_id = 1, left_time=30})
				end,
			},
			War2 = {
				key = "War2",
				name = "Combat instruction 2",
				preview_func = function(dNew)
					g_GuideCtrl:LoginInit({})
					war_id = warsimulate.war_id
					data.guidedata.War2 = dNew
					warsimulate.Start(1, 302, 2100, 10004)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {40402, 40402}, wid = 4, name="test", parid=9001, pos = 2, owner=1, status={auto_skill=50702, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 5, name="test", parid=9002, pos = 5, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 6, name="test", parid=9002, pos = 3, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					netwar.GS2CWarBoutStart({war_id = war_id, bout_id = 1, left_time=30})
				end,
			},
			-- War3 = {
			-- 	key = "War3",
			-- 	name = "竞技场手动教学",
			-- 	preview_func = function(dNew)
			-- 		g_GuideCtrl:LoginInit({})
			-- 		war_id = warsimulate.war_id
			-- 		data.guidedata.War2 = dNew
			-- 		warsimulate.Start(1, 302, 2100, 10004)
			-- 		local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {40402, 40402}, wid = 4, name="test", parid=9001, pos = 2, owner=1, status={auto_skill=50702, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
			-- 		netwar.GS2CWarAddWarrior(t)
			-- 		local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 5, name="test", parid=9002, pos = 5, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
			-- 		netwar.GS2CWarAddWarrior(t)
			-- 		local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 6, name="test", parid=9002, pos = 3, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
			-- 		netwar.GS2CWarAddWarrior(t)
			-- 		netwar.GS2CWarBoutStart({war_id = war_id, bout_id = 1, left_time=30})
			-- 	end,
			-- },
			WarAutoWar = {
				key = "WarAutoWar",
				name = "Auto combat",
				preview_func = function(dNew)
					g_GuideCtrl:LoginInit({})
					war_id = warsimulate.war_id
					data.guidedata.War2 = dNew
					warsimulate.Start(1, 302, 2100, 10004)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {40402, 40402}, wid = 4, name="test", parid=9001, pos = 2, owner=1, status={auto_skill=50702, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 5, name="test", parid=9002, pos = 5, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 6, name="test", parid=9002, pos = 3, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					netwar.GS2CWarBoutStart({war_id = war_id, bout_id = 1, left_time=30})
				end,
			},	
			War4 = {
				key = "War4",
				name = "Soul Skill Fury",
				preview_func = function(dNew)
					g_GuideCtrl:LoginInit({})
					war_id = warsimulate.war_id
					data.guidedata.War2 = dNew
					warsimulate.Start(1, 302, 2100, 10004)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {40402, 40402}, wid = 4, name="test", parid=9001, pos = 2, owner=1, status={auto_skill=50702, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 5, name="test", parid=9002, pos = 5, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 6, name="test", parid=9002, pos = 3, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
					netwar.GS2CWarAddWarrior(t)
					netwar.GS2CWarBoutStart({war_id = war_id, bout_id = 1, left_time=30})
				end,
			},			
			-- WarReplace = {
			-- 	key = "WarReplace",
			-- 	name = "战斗替换伙伴",
			-- 	preview_func = function(dNew)
			-- 		g_GuideCtrl:LoginInit({})
			-- 		war_id = warsimulate.war_id
			-- 		data.guidedata.War2 = dNew
			-- 		warsimulate.Start(1, 302, 2100, 10004)
			-- 		local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {40402, 40402}, wid = 4, name="test", parid=9001, pos = 2, owner=1, status={auto_skill=50702, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
			-- 		netwar.GS2CWarAddWarrior(t)
			-- 		local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 5, name="test", parid=9002, pos = 5, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
			-- 		netwar.GS2CWarAddWarrior(t)
			-- 		local t = {war_id = war_id, camp_id=1, type=4,partnerwarrior={ pflist = {3201, 3202}, wid = 6, name="test", parid=9002, pos = 3, owner=1, status={auto_skill=50701, status=1, mp=30, max_mp=30, hp=6000, max_hp=7000, model_info={shape=401}}, }}
			-- 		netwar.GS2CWarAddWarrior(t)
			-- 		netwar.GS2CWarBoutStart({war_id = war_id, bout_id = 1, left_time=30})
			-- 	end,
			-- },
			--StoryDlg = {key = "StoryDlg",name = "剧情对话框",},
			--Skill = {key = "Skill",name = "技能",},
			--Pata = {key = "Pata",name = "爬塔",},

			Open_ZhaoMu = dict_open("Open_ZhaoMu", "Recruit 1 (1/4)", "luckdraw_open", "grade"),
			DrawCard = dict_view("DrawCard", "Recruit 1 (2/4)", CPartnerHireView),
			DrawCardLineUp_MainMenu = dict_view("DrawCardLineUp_MainMenu", "Recruit 1 to go to battle (3/4)", CMainMenuView),
			DrawCardLineUp_PartnerMain = dict_view("DrawCardLineUp_PartnerMain", "Recruit 1 to go to battle (4/4)", CPartnerMainView),

			Open_ZhaoMu_Two = dict_open("Open_ZhaoMu_Two", "Recruit 2 (1/4)", "luckdraw_open", "grade"),
			DrawCard_Two = dict_view("DrawCard_Two", "Recruit 1 (2/4)", CPartnerHireView),
			DrawCardLineUp_Two_MainMenu = dict_view("DrawCardLineUp_Two_MainMenu", "Recruit 2 to go to battle (3/4)", CMainMenuView),
			DrawCardLineUp_Two_PartnerMain = dict_view("DrawCardLineUp_Two_PartnerMain", "Recruit 2 to go to battle (4/4)", CPartnerMainView),			

			Open_ZhaoMu_Three = dict_open("Open_ZhaoMu_Three", "Recruit 3 (1/5)", "luckdraw_open", "grade"),
			DrawCard_Three = dict_view("DrawCard_Three", "Recruit 3 (2/5)", CPartnerHireView),
			Partner_HBPY_MainMenu = dict_view("Partner_HBPY_MainMenu", "Recruit 3 to foster (3/5)", CMainMenuView),
			Partner_HPPY_PartnerMain = dict_view("Partner_HPPY_PartnerMain", "Recruit 3 to foster (4/5)", CPartnerMainView),		
			DrawCardLineUp_Three_PartnerMain = dict_view("DrawCardLineUp_Three_PartnerMain", "Recruit 3 to battle (5/5)", CPartnerMainView),		

			Partner_FWCD_One_MainMenu = dict_view("Partner_FWCD_One_MainMenu", "Bringing Soul Stone 1 (1/2)", CMainMenuView),
			Partner_FWCD_One_PartnerMain = dict_view("Partner_FWCD_One_PartnerMain", "Bringing Soul Stone 1 (2/2)", CPartnerMainView),

			Partner_FWCD_Two_MainMenu = dict_view("Partner_FWCD_Two_MainMenu", "Bringing Soul Stone 1 (1/2)", CMainMenuView),
			Partner_FWCD_Two_PartnerMain = dict_view("Partner_FWCD_Two_PartnerMain", "Bringing Soul Stone 2 (2/2)", CPartnerMainView),

			Partner_FWQH_MainMenu = dict_view("Partner_FWQH_MainMenu", "Strengthening Soul Stone (1/2)", CMainMenuView),
			Partner_FWQH_PartnerMain = dict_view("Partner_FWQH_PartnerMain", "Strengthening Soul Stone (2/2)", CPartnerMainView),

			Skill = dict_view("Skill", "Change character (1/1)", CSkillMainView),

			Open_Skill_Three = dict_open("Open_Skill_Three", "Character's 3rd spirit skill (1/2)", "luckdraw_open", "grade"),
			Skill_Three = dict_view("Skill_Three", "Character's 3rd spirit skill (2/2)", CSkillMainView),		

			Open_Skill_Four = dict_open("Open_Skill_Four", "Character 4th spirit skill (1/2)", "luckdraw_open", "grade"),
			Skill_Four = dict_view("Skill_Four", "Character 4th spirit skill (2/2)", CSkillMainView),										

			MapSwitchMainmenu = dict_view("MapSwitchMainmenu", "Switch map (1/2)", CMainMenuView),
			MapSwitchMapView = dict_view("MapSwitchMapView", "Switch map (2/2)", CMapMainView),			
					
			Open_House = dict_open("Open_House", "Unlock Family (1/6)", "house_open", "grade"),
			HouseView = dict_view("HouseView", "Unlock Family (2/6)", CHouseMainView),
			HouseTwoView = dict_view("HouseView", "Unlock Family (3/6)", CHouseMainView),			
			HouseTeaartView = dict_view("HouseTeaartView", "Unlock Family (5/6)", CTeaartView),

			TeamMainView_HandyBuild = dict_view("TeamMainView_HandyBuild", "Quick guiding team up (1/1)", CTeamTargetSetView),

			HuntPartnerSoulView = dict_view("HuntPartnerSoulView", "Soul Hunting (1/3)", HuntPartnerSoulView),
			Open_Yuling = dict_view("Open_Yuling", "Soul Hunting (2/3)", CMainMenuView),
			Yuling_PartnerMain = dict_view("Yuling_PartnerMain", "Soul Hunting (3/3)", CPartnerMainView),

			Open_Achieve = dict_open("Open_Achieve", "Achievements (1/2)", "achieve_open", "grade"),
			Open_Pefuben = dict_open("Open_Pefuben", "Extra Soul Chapter(1/1)", "yikong_open", "grade"),
			Open_Shimen = dict_open("Open_Pvp", "Sect Task (1/1)", "equipfuben_open", "grade"),							
			Open_Convoy = dict_open("Open_Convoy", "Rush to Dudu (1/1)", "equipfuben_open", "grade"),							
			Open_Pata = dict_open("Open_Pata", "Prison (1/1)", "pata_open", "grade"),
			Open_Travel = dict_open("Open_Pata", "Excursion (1/1)", "travel_open", "grade"),
			Open_Org = dict_open("Open_Org", "Unlock Guild (1/1)", "org_open", "grade"),	
			Open_Travel = dict_open("Open_Travel", "Excursion (1/1)", "schedule_open", "grade"),
			Open_FieldBoss = dict_open("Open_FieldBoss", "Attack Effigy (1/1)", "schedule_open", "grade"),
			Open_YJFuben = dict_open("Open_YJFuben", "Nightmare Hunting (1/1)", "schedule_open", "grade"),
			Open_Schedule = dict_open("Open_Org", "Unlock Calendar (1/1)", "schedule_open", "grade"),
			Open_Arena = dict_open("Open_Arena", "Arena (1/1)", "arena_open", "grade"),
			Open_EqualArena = dict_open("Open_EqualArena", "Balance Arena (1/1)", "arena_open", "grade"),
			Open_Trapmine = dict_open("Open_Trapmine", "Unlock Searching (1/1)", "trapmine_open", "grade"),
			Open_MingLei = dict_open("Open_MingLei", "Unlock Tea party (1/1)", "minglei_open", "grade"),
			Open_MapBook = dict_open("Open_MapBook", "Unlock Appraising (1/1)", "mapbook_open", "grade"),			
			Open_Lilian = dict_open("Open_Lilian", "Unlock Daily Cultivating (1/1)", "lilian_open", "grade"),						
			Open_Equipfuben = dict_open("Open_Equipfuben", "Unlock Equipmentt Dungeon (1/1)", "equip_fb", "grade"),
			Open_Forge = dict_open("Open_Forge", "Unlock Making Equipment (1/1)", "forge_open", "grade"),
			Open_Forge_composite = dict_open("Open_Forge_composite", "Unlock Forming Equipment (1/1)", "forge_open", "grade"),
		
		},
		modify_key = {
			start_condition = {name="Active conditions", select_type="condition"},
			continue_condition = {name="Continue condition", select_type="condition"},
			ui_key = {name="UI name"},
			effect_tips_enum = {name="Reminder type"},
			texture_name ={name="Picture name"},
			guide_list = 
			{
				name="Guidance List",
				list_type = true, 
				default_value={
					leave_team="",
					click_continue=false,
					click_continue_time=0,
					continue_condition="",
					effect_list={},
					start_condition="",
					need_guide_view = true,
					force_hide_continue_label=false,
					is_cache_step=false
				},
			},
			click_continue = {name="Click to continue"},
			force_hide_continue_label = {name="Hide click text to continue"},
			effect_list = {
				name="Instruction effect",
				list_type = true,
				type_arginfo = {
					key = "value_type",
					name = "Instruction type",
					select_type = "effect_type",
					default = "???",
				},
			},
			near_pos = {name="Far from instruction location"},
			x = {},
			y = {},
			w = {name="Width"},
			h = {name="Height"},
			text_list = {
						name="Text list", 
						list_type = true,
						default_value=" "},
			fixed_pos = {name="Fixed location"},
			ui_effect = {name="Instruction effect"},
			play_tween = {name="Disdeliver animations"},
			dlg_is_left = {name="Align Left ?"},
			dlg_is_flip = {name="Backward ?"},
			spine_left_shape = {name="The left hand side character"},			
			spine_right_shape = {name="The left hand side character"},
			spine_left_motion = {name="The left hand side action"},
			spine_right_motion = {name="The left hand side action"},
			guide_voice_list_1 = {name="Voice 1"},
			guide_voice_list_2 = {name="Voice 2"},			
			aplha= {name="Transparency"},
			side_list = {
						name="Left and right chat target", 
						list_type = true,
						default_value="0"},			
			dlg_sprite = {name="Chat box"},
			flip_y = {name="y Switch"},
			-- guide_key = {},
			effect_type = {name="Type", wrap="effect_type"},
			teach_id = {name="Teaching id"},
			sprite_name = {name="spritename",},
			open_text = {name="Text"},
			focus_ui_size = {name="Brightness size ui"},
			need_guide_view = {name="Need instruction interface"},
			next_tip = {name="Tab trang sau", select_type="bool_type", default=nil,}
		},
		value_default = {
			["texture"] = {
					ui_key=[[]],
					near_pos={x=0, y=0},
					fixed_pos={x=0,y=0,},
					flip_y=false,
					play_tween=true,
					texture_name=[[guide_1.png]],
					effect_type=[[texture]],
				},
			["dlg"]={
					ui_key=[[]],
					near_pos={x=0, y=0},
					dlg_sprite=[[pic_zhiying_ditu_1]],
					dlg_tips_sprite=[[guide_3]],
					fixed_pos={x=0,y=0,},
					dlg_is_left=true,
					play_tween=true,
					text_list={
					},
					effect_type=[[dlg]],
					aplha=255,
				},
			["textdlg"]={
					ui_key=[[]],
					near_pos={x=0, y=0},
					fixed_pos={x=0,y=0,},
					dlg_is_left=true,
					play_tween=true,
					text_list={
					},
					effect_type=[[textdlg]],
					aplha=255,
				},				
			["bigdlg"]={
					ui_key=[[]],
					near_pos={x=0, y=0},
					fixed_pos={x=0,y=0,},
					dlg_is_left=true,
					dlg_is_flip=false,
					play_tween=true,
					text_list={
					},
					effect_type=[[bigdlg]],
					aplha=255,
				},				
			["spine"]={
					ui_key=[[]],
					spine_left_shape=[[1001]],
					spine_right_shape=[[1002]],						
					text_list={
					},
					side_list={
					},
					guide_voice_list_1=[[0]],
					guide_voice_list_2=[[0]],			
					effect_type=[[spine]],
					spine_left_motion=[[idle]],
					spine_right_motion=[[idle]],	
					aplha=255,
				},
			["none"]={			
					effect_type=[[none]],
				},		
			["focus_ui"]={
						aplha=255,
						focus_ui_size=1.2, 
						effect_type="focus_ui", 
						ui_key="draw_wl_card", 
						ui_effect="Finger",
						effect_tips_enum=0,
						effect_offset_pos={x=0,y=0},
						mode=1,
						},
			["focus_pos"]={
						aplha=255,
						h=0.12,
						pos_func=[[war1_pos2]],
						effect_type=[[focus_pos]],
						ui_effect=[[Finger]],
						w=0.07,
						effect_tips_enum=1,
						},
			["click_ui"]={
						near_pos={x=0,y=0}, 
						offset_pos={x=0,y=0},
						effect_type=[[click_ui]],
						ui_effect=[[Finger]],
						ui_key=[[请选择]],
						aplha=255,
						effect_tips_enum=1,
						view_name=[[]]
						},
			["hide_click_event"]={},
			["hide_focus_box"]={},
			["teach_guide"]={effect_type=[[teach_guide]], teach_id = 30007},
			["focus_common"]={effect_type=[[focus_common]],x=0.5,y=0.5,w=1,h=1},		
			["open"] = {
					effect_type="open", 
					ui_key="mainmenu_operate_btn", 
					sprite_name = "btn_bwcrk2017",
					open_text='Arena',
				},
		},
		before_dump = function(t)
			for _, dGuide in ipairs(t.guide_list) do
				local list = {}
				for _, dEffect in ipairs(dGuide.effect_list) do
					for k, v in pairs(dEffect) do
						if k == "ui_key" then
							if not table.index(list, v) then
								table.insert(list, v)
							end
						end
					end
					dEffect.guide_key = nil
				end
				dGuide.necessary_ui_list = list
			end
		end
	},
	notice={
		name="Notification",
		path="/Lua/logic/data/noticedata.lua",
		modify_table = {
			Content = {
				key = "Content",
				name = "Content",
			},
		},
		modify_key = {
			contents = {default_value={title="Enter the title", text = "Enterthe content"}, list_type = true,},
			pic = {name="pic"},
			text = {name="text"},
			title = {name="title"},
		},
		before_dump = function(t)
			printc("How json is generated:\n\r", cjson.encode(t))
			return t
		end,
	},
	showwar={
		name="Perform",
		path="/Lua/logic/data/showwardata.lua",
		modify_table = {
			Boss = {
				key = "Boss",
				name = "Boss battle",
				preview_func = function(dNew)
					g_ShowWarCtrl:LoadShowWar("Boss")
				end,
			},
		},
		modify_key = {
			wid = {name="Soldier ID"},
			name = {name="Name",},
			pos = {name="Standing point",},
			camp = {name="Phe",},
			shape = {name="Figure",},
			weapon = {name="Weapon",},
			partner = {name="Hero",},
			cmd_name = {name="Command name", select_type="cmd_name"},
			warrior_list = {
				name="List of Soldiers",
				list_type = true,
				default_value = {
					wid = 1,
					name = "Name",
					pos = 1,
					camp = 1,
					shape = 130,
					weapon = 0,
					partner = 0,
				},
			},
			cmd_list = {
				name="Command List", 
				list_type = true,
				default_value = {
					cmd_name = "",
					arg_list = {},
				}
			},
			arg_list = {
				name="Parameter list", 
				list_type = true,
				default_value = "",
			},
		},
		value_default = {
		},
		before_dump = function(t)
			return t
		end,
	}
}