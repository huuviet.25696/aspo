config = {}
config.run_env = nil
config.func = {
	ease = function()
		local t = table.keys(enum.DOTween.Ease)
		table.sort(t, function(k1, k2) return enum.DOTween.Ease[k1] < enum.DOTween.Ease[k2] end)
		return t
	end
}

config.select =
{
	get_type = {
		{"random", "Random"},
		{"list", "Order"},
	},
	magic_type = {
		{1, "Normal Magic"},
	},
	pos_type = {
		{"nil", "No"},
		{"atk", "Attacker location"},
		{"vic", "Attacked location"},
		{"atk_lineup", "Attacker strategy location"},
		{"vic_lineup", "Attacked strategy location"},
		{"center", "Battle Center"},
		{"atk_team_center", "Attack Team Center"},
		{"vic_team_center", "Attacked Team Center"},
		{"cam", "Camera location"},
	},
	bool_type = {
		{true, "Yes"},
		{false, "No"}
	},
	excutor_type = {
		{"atkobj", "The attacker"},
		{"vicobj", "Target"},
		{"vicobjs", "Attacked (All)"},
		{"camobj", "Camera"},
		{"allys", "Teammate(All)"},
		{"ally_na", "Teammate(Not attackers)"},
		{"ally_alive", "Teammate(Still alive)"},
		{"enemys", "Enemy(All)"},
		{"enemy_nv", "Enemy(not target)"},
	},
	body_type = {
		{"head", "Head"},
		{"waist", "Waist"},
		{"foot", "Leg"},
	},
	res_type = {
	},
	move_type = {
		{"line", "Straight line"},
		{"circle", "Arc"},
		{"jump", "Jump"}
	},
	transform_name_type = {
		{"Mount_Hit", "Mount_Hit"},
		{"Mount_Head", "Mount_Head"},
		{"Mount_Shadow", "Mount_Shadow"},
		{"Bip001", "Bip001"}
	},
	effect_cnt_type = {
		{"one", "Single"},
		{"allvic", "All Attacked"},
	},
	move_dir = {
		{"local_up", "Up local coordinates"},
		{"local_right", "Current coordinates right"},
		{"local_forward", "forward current coordinates"},
		{"world_up", "Up the world coordinates"},
		{"world_right", "The world coordinates right"},
		{"world_forward", "The world coordinates forward"},
	},
	excutor_dir = {
		{"empty", "No"},
		{"forward", "Fixed person-Before"},
		{"backward", "Fixed person-Behind"},
		{"left", "Fixed person-Left"},
		{"right", "Fixed person-Right"},
		{"up", "Fixed person-Above"},
		{"down", "Fixed person-Under"},
		{"relative", "Self-defining"}
	}
}

config.normal_arg = {


}

config.arg = {
	--复合参数
	complex_pos = {
		sublist = {"base_pos", "relative_dis", "relative_angle", "depth"},
		need_run_env = true,
	},
	complex_effect ={
		sublist = {"path", "cached", "magic_layer", "preload"},
	},
	complex_color = {
		sublist = {"r", "g", "b", "a"},
	}
}

config.arg.template ={
	r = {
		name = "r",
		key = "r",
		format = "number_type",
		default = 255,
	},
	g = {
		name = "g",
		key = "g",
		format = "number_type",
		default = 255,
	},
	b = {
		name = "b",
		key = "b",
		format = "number_type",
		default = 255,
	},
	a = {
		name = "a",
		key = "a",
		format = "number_type",
		default = 255,
	},
	hit = {
		name = "Attaked time",
		key = "hit",
		format = "number_type",
	},
	hurt = {
		name = "HP loosing time",
		key = "hurt",
		format = "number_type",
	},
	endhit = {
		name = "End of being attacked",
		key = "endhit",
		format = "number_type",
	},
	start_time = {
		name = "Start time",
		key = "start_time",
		format = "number_type",
	},

	path = {
		name = "Natural resource path",
		key = "path",
		select = function() 
				local dirs = {"/Effect/Magic"}
				local newList = {}
				if Utils.IsEditor() then
					for _, dir in pairs(dirs) do
						local list = IOTools.GetFiles(IOTools.GetGameResPath(dir), "*.prefab", true)
						for i, sPath in ipairs(list) do
							local idx = string.find(sPath, dir)
							if idx then
								table.insert(newList, string.sub(sPath, idx+1, string.len(sPath)))
							end
						end
					end
				end
				return newList
			end,
		wrap = function(s) return IOTools.GetFileName(s, true) end,
		input_width = 200,
	},
	flip = {
		name = "Roll",
		key = "flip",
		select_type = "bool_type",
		default = false,
	},
	cached = {
		name = "can cache",
		key = "is_cached",
		select_type = "bool_type",
		default = true,
	},
	base_pos = {
		name = "Basic location",
		key = "base_pos",
		select_type = "pos_type",
		default = "atk",
		isnil = true,
	},
	relative_dis = {
		name = "Tilt distance",
		key = "relative_dis",
		format = "number_type",
		default = 0,
	},
	relative_angle = {
		name = "Tilt distance",
		key = "relative_angle",
		format = "number_type",
		default = 0,
	},

	depth = {
		name = "Height",
		key = "depth",
		format = "number_type",
		default = 0,
	},

	--编辑主界面用的
	warrior_cnt = {
		name = "Number of people",
		key = "warrior_cnt",
		format = "number_type",
		default = 1,
		change_refresh = 2, -- 1保存数据，2刷新界面，3运行演示
	},
	atk_id = {
		name = "Attacker ID",
		key = "atk_id",
		format = "number_type",
		change_refresh = 1,
	},
	vic_ids = {
		name = "Attacked ID",
		key = "vic_ids",
		format = "list_type",
		change_refresh = 1,
	},
	vic_array = {
		name = "Attacked group",
		key = "vic_array",
		format = "number_type", 
		change_refresh = 1,
	},
	damage_cnt = {
		name = "Attacked",
		key = "damage_cnt",
		select = {"one", "all"},
		wrap = {"Attacked division", "Being attacked at the same time"},
		change_refresh = 1,
	},
	sub_type = {
		name = "Attacked location",
		key = "sub_type",
		select = {"one","all", "chain", "sequence"},
		wrap = {"Alone","Together", "Connection type", "Order"},
		change_refresh = 1,
	},
	shape = {
		name = "Figure",
		key = "shape",
		select = ModelTools:GetAllModelShape(),
		default = 101,
		change_refresh = 2,
	},
	weapon = {
		name = "Weapon",
		key = "weapon",
		select = ModelTools:GetAllWeaponShape(),
		change_refresh = 2,
	},
	special_type = {
		name = "Type",
		key = "special_type",
		select = {0, define.War.Type.GuideBoss},
		wrap ={"Normal", "Boss battle"},
		change_refresh = 2,
	},
	run_env = {
		key = "run_env",
		name = "Operating environment",
		select = {"war", "createrole", "dialogueani"},
		wrap = {"Combat", "Character", "Animation"},
		default = "war",
		change_refresh = 1,
	},
	magic_file = {
		key = "magic_file",
		name = "Save type",
		select = {"magic", "goback", "createrole", "dialogueani"},
		wrap = {"Magic", "Reset", "Character", "Animation"},
		default = "magic",
	},
	magic_layer = {
		name = "Disdeliver preset",
		key = "magic_layer",
		select = {"bottom", "center", "top"},
		wrap = {"Shield", "Normal", "Top"},
		default = "center",
	},
	preload = {
		name = "Loading first",
		key = "preload",
		select_type = "bool_type",
		default = true,
	},
}



local cmd = {}
cmd.MagcAnimStart = {
	wrap_name = "Start performing magic",
	sort = 0,
	args ={
	},
}

cmd.MagcAnimEnd = {
	wrap_name = "Finish performing magic",
	sort = 0,
	args ={
	},
}

cmd.PlaySound = {
	wrap_name = "Sound effects",
	sort = 0,
	args = {
		{
			name = "Natural resource path",
			key = "sound_path",
			select = function() 
					local dirs = {"/Audio/Sound/Magic"}
					local newList = {}
					if Utils.IsEditor() then
						for _, dir in pairs(dirs) do
							local list = IOTools.GetFilterFiles(IOTools.GetGameResPath(dir), function(s) return string.find(s, ".meta") == nil end, true)
							for i, sPath in ipairs(list) do
								local sPattrn = "Magic"
								local idx = string.find(sPath, sPattrn)
								if idx then
									table.insert(newList, string.sub(sPath, idx, string.len(sPath)))
								end
							end
						end
					end
					return newList
				end,
			wrap = function(s) return IOTools.GetFileName(s, false) end,
			input_width = 200,
		},
		
		{
			name = "Volume (0-1)",
			key = "sound_rate",
			format = "number_type",
			default = 1,
		}
	},
}


cmd.VicHitInfo = {
	wrap_name = "Attacked information",
	sort = 1,
	args ={
		{
			key = "hurt_delta",
			name = "HP dropping time",
			format = "number_type",
			default = 0,
		},

		{
			key = "face_atk",
			name = "Encounter attacker",
			select_type = "bool_type",
			default = true,
		},
		{
			key = "play_anim",
			name = "Attacked moves",
			select_type = "bool_type",
			default = true,
		},
		{
			key = "damage_follow",
			name = "Attached Damage",
			select_type = "bool_type",
			default = true,
		},
		{
			key = "consider_hight",
			name = "Consider height",
			select_type = "bool_type",
			default = false,
		},
	},
}

cmd.GroupCmd = {
	wrap_name = "Command group",
	sort = 1001,
	args ={
		{
			key = "group_type",
			name = "Type of Command group",
			select = {"condition", "repeat"},
			wrap = {"Evaluation conditions","Edit"},
			refresh_args = true,
		},
		["condition"] = {
			{
				key = "condition_name",
				name = "Condition",
				select = {"ally", "endidx", "firstidx", "atkmale"},
				wrap = {"Teammate", "The last one in the series", "The first one in the series", "The attacker is male"},
				default = "ally",
			},
			{
				key = "true_group",
				name = "true command group",
				format = "string_type"
			},
			{
				key = "false_group",
				name = "False command group",
				format = "string_type"
			},
		},
		["repeat"] = {
			{
				key = "group_names",
				name = "Select list",
				format = "list_type",
			},
			{
				key = "get_type",
				name = "Select type",
				select_type = "get_type",
			},
			{
				key = "cnt",
				name = "Select turn",
				format = "string_type",
				default = 0,
			},
		},
		{
			key = "add_type",
			name = "How to add",
			select = {"insert", "merge"},
			wrap = {"Insert","Pair"},
			default = "insert",
		},
	},
}

cmd.GroupTime = {
	wrap_name = "Command group time",
	sort = 1002,
	args ={
		{
			key = "duration",
			name = "Team time",
			format = "float",
			default = 0,
		},
	},
}
cmd.Name = {
	wrap_name = "Calling",
	sort = 1003,
	args ={
		{
			key = "alive_time",
			name = "Existence time",
			format = "number_type",
			default = 0.5,
		},
	},
}

cmd.PlayerBigMagic = {
	wrap_name = "Character great moves",
	sort = 1004,
	args ={
		{
			key = "alive_time",
			name = "Existence time",
			format = "number_type",
			default = 0.5,
		},
	},
}

cmd.PlayAction = {
	wrap_name = "deliver action",
	sort = 10,
	args ={
		{	
			key = "action_name",
			name = "Action",
			format = "string_type",
			select = function ()
				local iShape
				local oView = CEditorMagicView:GetView()
				if oView then
					iShape = oView:GetShape()
				end
				return ModelTools.GetAllState(iShape)
			end,
		},
		{
			key = "excutor",
			name = "Operator",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "atkobj",
		},
		{
			key = "start_frame",
			name = "Frame start (?)",
			format = "number_type",
		},
		{
			key = "end_frame",
			name = "Frame finish (?)",
			format = "number_type",
		},
		{
			key = "action_time",
			name = "Movement time (?)",
			format = "number_type",
		},
		{
			key = "bak_action_name",
			name = "The reserve movement (?)",
			format = "string_type",
			select = function ()
				return ModelTools.GetAllState()
			end,
			force_input = true,
		},
	},
	short_desc = {"action_name"}
}

cmd.FaceTo = {
	wrap_name = "Adjust the main direction",
	sort = 11,
	args ={
		{
			key = "excutor",
			name = "Operator",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "atkobj",
		},
		{
			key = "face_to",
			name = "Type of direction",
			format = "string_type",
			select = {"default", "fixed_pos", "lerp_pos", "look_at", "random", "prepare"},
			wrap = {"Default direction", "Fixed location", "Rotate evenly", "LookAt", "Random direction", "Preset"},
			default = "default",
			refresh_args = true, 
		},
		fixed_pos = {
			{
				key = "pos",
				name = "Location",
				complex_type = "complex_pos",
				col = 3,
			},
		},
		look_at = {
			{
				key = "pos",
				name = "Location",
				complex_type = "complex_pos",
				col = 3,
			},
		},
		lerp_pos = {
			{
				key = "h_dis",
				name = "Average moving",
				format = "number_type",
				default = 0,
			},
			{
				key = "v_dis",
				name = "Move straight",
				format = "number_type",
				default = 0,
			},
		},
		random = {
			{
				key = "x_min",
				name = "x Min value",
				format = "number_type",
				default = 0,
			},
			{
				key = "x_max",
				name = "x Max value",
				format = "number_type",
				default = 360,
			},
			{
				key = "y_min",
				name = "y Min value",
				format = "number_type",
				default = 0,
			},
			{
				key = "y_max",
				name = "y Max value",
				format = "number_type",
				default = 360,
			},
			{
				key = "z_min",
				name = "z Min value",
				format = "number_type",
				default = 0,
			},
			{
				key = "z_max",
				name = "z Max value",
				format = "number_type",
				default = 360,
			},
		},
		prepare = {
			{
				key = "prepare_pos",
				name = "Preset location",
				select_update = function() 
					local t = table.extend(table.keys(data.cameradata.INFOS.war),
					table.keys(data.cameradata.INFOS.warrior))
					table.sort(t)
					return t
				end,
			},
		},
		{
			key = "time",
			name = "Roatation time",
			format = "number_type",
			default = 0,
		},
	}
}

cmd.Move = {
	wrap_name = "Moving location",
	sort = 12,
	args ={
		{
			key = "excutor",
			name = "Operator",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "atkobj",
		},
		{
			key = "move_type",
			name = "Moving type",
			select_type = "move_type",
			default = "line",
			refresh_args = true,
		},
		{
			key = "move_time",
			name = "Moving time",
			format = "number_type",
			default = 0,
		},
		line = {
			{
				key = "ease_type",
				name = "Steep curve",
				select = config.func.ease, 
				format = "string_type",
				default = "Linear",
			},
			{
				key = "begin_type",
				name = "Start type",
				select = {"current", "begin_prepare", "begin_relative"},
				wrap = {"Current location", "Preset", "Self-defining"},
				default = "current",
				refresh_args = true,
			},
			{
				key = "end_type",
				name = "End type",
				select = {"empty", "end_prepare", "end_relative"},
				wrap = {"No", "Preset", "Self-defining"},
				default = "end_relative",
				refresh_args = true,
			},
			{
				key = "calc_face",
				name = "Calculate direction",
				select_type = "bool_type",
				default = true,
			},
			{
				key = "look_at_pos",
				name = "End point encounter",
				select_type = "bool_type",
				default = true,
			},
			begin_prepare = {
				{
					key = "begin_prepare",
					name = "Start point preset",
					select_update = function() 
						local t = table.extend(table.keys(data.cameradata.INFOS.war),
						table.keys(data.cameradata.INFOS.warrior))
						table.sort(t)
						return t
					end,
				},
			},
			begin_relative = {
				{
					key = "begin_relative",
					name = "Adjust end point",
					complex_type = "complex_pos",
					col = 3,
				},
			},
			end_prepare = {
				{
					key = "end_prepare",
					name = "Preset end point",
					select_update = function()
						local t = table.extend(table.keys(data.cameradata.INFOS.war),
						table.keys(data.cameradata.INFOS.warrior))
						table.sort(t)
						return t
					end,
				},
			},
			end_relative = {
				{
					key = "end_relative",
					name = "Adjust end point",
					complex_type = "complex_pos",
					col = 3,
				},
			},
		},
		circle = {
			{
				key = "lerp_cnt",
				name = "Value inserts", 
				format = "number_type",
				default = 5,
			},
			{
				key = "begin_relative",
				name = "Adjust end point",
				complex_type = "complex_pos",
				col = 3,
			},
			{
				key = "end_relative",
				name = "Adjust end point",
				complex_type = "complex_pos",
				col = 3,
			},
		},
		jump = {
			{
				key = "min_jump_power",
				name = "Min jumping force",
				format = "number_type",
				default = 1,
			},
			{
				key = "max_jump_power",
				name = "Max jumping force",
				format = "number_type",
				default = 1,
			},
			{
				key = "jump_num",
				name = "Jumping time",
				format = "number_type",
				default = 1,
			},
			{
				key = "ease_type",
				name = "Steep curve",
				select = config.func.ease, 
				format = "string_type",
				default = "Linear",
			},
			{
				key = "begin_type",
				name = "Start type",
				select = {"current", "begin_prepare", "begin_relative"},
				wrap = {"Current location", "Preset", "Self-defining"},
				default = "current",
				refresh_args = true,
			},
			{
				key = "end_type",
				name = "End type",
				select = {"empty", "end_prepare", "end_relative"},
				wrap = {"No", "Preset", "Self-defining"},
				default = "end_relative",
				refresh_args = true,
			},
			{
				key = "calc_face",
				name = "Calculate direction",
				select_type = "bool_type",
				default = true,
			},
			{
				key = "look_at_pos",
				name = "End point encounter",
				select_type = "bool_type",
				default = true,
			},
			begin_prepare = {
				{
					key = "begin_prepare",
					name = "Start point preset",
					select_update = function() 
						local t = table.extend(table.keys(data.cameradata.INFOS.war),
						table.keys(data.cameradata.INFOS.warrior))
						table.sort(t)
						return t
					end,
				},
			},
			begin_relative = {
				{
					key = "begin_relative",
					name = "Adjust end point",
					complex_type = "complex_pos",
					col = 3,
				},
			},
			end_prepare = {
				{
					key = "end_prepare",
					name = "Preset end point",
					select_update = function()
						local t = table.extend(table.keys(data.cameradata.INFOS.war),
						table.keys(data.cameradata.INFOS.warrior))
						table.sort(t)
						return t
					end,
				},
			},
			end_relative = {
				{
					key = "end_relative",
					name = "Adjust end point",
					complex_type = "complex_pos",
					col = 3,
				},
			},
		},
	},
	short_desc = {"excutor", "begin_prepare", "end_prepare"}
}


cmd.MoveDir = {
	wrap_name = "Teleport Position (Direction+Speed)",
	sort = 13,
	args ={
		{
			key = "excutor",
			name = "Operator",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "camobj",
		},
		{
			key = "dir",
			name = "Direction of birth",
			select_type = "move_dir",
		},
		{
			key = "speed",
			name = "Speed",
			format = "number_type",
		},
		{
			key = "move_time",
			name = "Time",
			format = "number_type",
		},
	},
	short_desc = {"excutor"}
}

cmd.BodyEffect = {
	wrap_name = "Fixed body",
	sort = 60,
	args ={
		{
			key = "excutor",
			name = "Fixed people",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			defalut = "vicobjs",
		},
		{
			key = "effect",
			name = "Effect",
			complex_type = "complex_effect", col = 3,
		},
		{
			key = "height",
			name = "Height",
			format = "number_type",
			default = 0,
		},
		{
			key = "alive_time",
			name = "Existence time",
			format = "number_type",
		},
		{
			key = "bind_type",
			name = "Fixed type",
			select = {'empty','node','pos', "model"},
			wrap = {'No','Knot','Feature', "Paradigm"},
			default = "empty",
			refresh_args = true
		},
		pos =
		{
			{
				key = "body_pos",
				name = "Fixed location",
				select_type = "body_type",
				default = "head",
			},
		},
		node = 
		{
			{
			key = "bind_idx",
			name = "Auto train (?)",
			format = "number_type",
			default = "",
			},
		},
		model =
		{
			{
				key = "find_path",
				name = "transform Name",
				select_type = "transform_name_type",
				default = "Mount_Shadow",
				force_input = true,
			},
		},
	},
	short_desc = {"path"}
}

cmd.StandEffect = {
	wrap_name = "Stop effect",
	sort = 70,
	args ={
		{
			key = "excutor",
			name = "Fixed people",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "vicobjs",
		},
		{
			key = "effect",
			name = "Effect",
			complex_type = "complex_effect", col = 3,
		},
		{
			key = "effect_pos",
			name = "Effect location",
			complex_type = "complex_pos",
			col = 3,
		},
		{
			key = "effect_dir_type",
			name = "Direction(?)",
			select_type = "excutor_dir",
			default = "empty",
			refresh_args = true,
		},
		relative = {
			{
				key = "relative_dir",
				name = "Location",
				complex_type = "complex_pos",
				col = 3,
			},
		},
		{
			key = "alive_time",
			name = "Existence time",
			format = "number_type",
		},
	},
	short_desc = {"path"}
}

cmd.ShootEffect = {
	wrap_name = "Shot effect",
	sort = 80,
	args ={
		{
			key = "excutor",
			name = "Fixed people",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "vicobjs",
		},
		{
			key = "effect",
			name = "Effect",
			complex_type = "complex_effect", col = 3,
		},
		{
			key = "begin_pos",
			name = "Star location",
			complex_type = "complex_pos",
			col = 3,
		},
		{
			key = "end_pos",
			name = "End location",
			complex_type = "complex_pos",
			col = 3,
		},
		{
			key = "move_time",
			name = "Moving time",
			format = "number_type",
		},
		{
			key = "delay_time",
			name = "Late move",
			format = "number_type",
			default = 0,
		},
		{
			key = "ease_type",
			name = "Steep curve",
			select = config.func.ease, 
			format = "string_type",
			default = "Linear",
		},
		{
			key = "alive_time",
			name = "Existence time",
			format = "number_type",
			default = 1,
		},
	},
	short_desc = {"path"}
}

cmd.ChainEffect = {
	wrap_name = "Connection effect",
	sort = 81,
	args ={
		{
			key = "excutor",
			name = "Fixed people",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "vicobjs",
		},
		{
			key = "effect",
			name = "Effect",
			complex_type = "complex_effect", col = 3,
		},
		{
			key = "begin_pos",
			name = "Star location",
			complex_type = "complex_pos",
			col = 3,
		},
		{
			key = "end_pos",
			name = "End location",
			complex_type = "complex_pos",
			col = 3,
		},
		{
			key = "scale_time",
			name = "Time",
			format = "number_type",
			default = 1,
		},
		{
			key = "ease_type",
			name = "Steep curve",
			select = config.func.ease, 
			format = "string_type",
			default = "Linear",
		},
		{
			key = "alive_time",
			name = "Existence time",
			format = "number_type",
			default = 1,
		},
		{
			key = "repeat_texture",
			name = "Duplicated pattern",
			select_type = "bool_type",
			default = true,
		},
	},
	short_desc = {"path"}
}

cmd.AnimatorEffect = {
	wrap_name = "Animator effect",
	sort = 82,
	args ={
		{
			key = "excutor",
			name = "Fixed people",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "vicobjs",
		},
		{
			key = "effect",
			name = "Effect",
			complex_type = "complex_effect", col = 3,
		},
		{
			key = "alive_time",
			name = "Existence time",
			format = "number_type",
			default = 1,
		},
	},
	short_desc = {"path"}
}


cmd.ShakeScreen = {
	wrap_name = "Rung",
	sort = 81,
	args ={
		{
			key = "shake_time",
			name = "Time",
			format = "number_type",
		},
		{
			key = "shake_dis",
			name = "Amplitude",
			format = "number_type",
			defalut = 0.1,
		},
		{
			key = "shake_rate",
			name = "Frequency",
			format = "number_type",
			defalut = 1,
		},
		-- {
		-- 	key = "shake_randomness",
		-- 	name = "随机性",
		-- 	format = "number_type",
		-- 	defalut = 90,
		-- },
	},
}

cam_config = require "logic.editor.editor_camera.editor_camera_config"

cmd.CameraTarget = {
	wrap_name = "Character discription",
	sort = 84,
	args ={
		{
			key = "excutor",
			name = "Character discription",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
		},
		{
			key = "move_type",
			name = "Moving type",
			select = {"cam", "actor"},
			wrap = {"Camera->Character", "Characters->Camera"},
			default = "cam",
			refresh_args = true,
		},
		cam = {
			{
				key = "camera_pos",
				name = "Camera location",
				complex_type = "complex_pos",
				pos_cam = function() return g_CameraCtrl:GetWarCamera() end,
				col = 3,
			},
			{
				key = "move_time",
				name = "Moving time",
				format = "number_type",
				default = 0,
			},
		},
		actor = {
			{
				key = "actor_pos",
				name = "Character location",
				complex_type = "complex_pos",
				col = 3,
			},
		}
	},
}



cmd.CameraColor = {
	wrap_name = "Projector's color",
	sort = 86,
	args ={
		{
			key = "color",
			name = "Color",
			complex_type = "complex_color",
			col = 3,
		},
		{
			key = "fade_time",
			name = "Time to change gradually (?)",
			format = "number_type",
		},
		{
			key = "restore_time",
			name = "Return time (?)",
			format = "number_type",
		}
	},
}

cmd.CameraFieldOfView = {
	wrap_name = "Projector FieldOfView",
	sort = 87,
	args ={
		{
			key = "start_val",
			name = "Start point",
			format = "number_type",
			default = 26,
		},
		{
			key = "end_val",
			name = "End point",
			format = "number_type",
			default = 26,
		},
		{
			key = "fade_time",
			name = "Changing time",
			format = "number_type",
			default = 0,
		},

	},
}


cmd.CameraLock = {
	wrap_name = "Lock projector",
	sort = 88,
	args ={
		{
			key = "player_swipe",
			name = "The deliverer moves the projector",
			select_type = "bool_type",
		},
	},
}

cmd.CameraPathPercent = {
	wrap_name = "Projector path",
	sort = 89,
	args ={
		{
			key = "path_percent",
			name = "Percentage of paths",
			format = "number_type",
			default = 0,
		},
	},
}

cmd.HideUI = {
	wrap_name = "Hide UI",
	sort = 90,
	args ={
		{
			key = "time",
			name = "Maintainance time (0 always hidden)",
			format = "number_type",
			default = 0,
		}
	},
}

cmd.ShowUI = {
	wrap_name = "Disdeliver UI",
	sort = 91,
	args ={
	},
}

cmd.LoadUI = {
	wrap_name = "Dowload UI",
	sort = 92,
	args ={
		{
			key = "path",
			name = "Path",
			select = function() 
					local dirs = {"/UI/Magic"}
					local newList = {}
					if Utils.IsEditor() then
						for _, dir in pairs(dirs) do
							local list = IOTools.GetFiles(IOTools.GetGameResPath(dir), "*.prefab", true)
							for i, sPath in ipairs(list) do
								local idx = string.find(sPath, dir)
								if idx then
									table.insert(newList, string.sub(sPath, idx+1, string.len(sPath)))
								end
							end
						end
					end
					return newList
				end,
			wrap = function(s) return IOTools.GetFileName(s, true) end,
			format = "string_type",
		},
		{
			key = "time",
			name = "Time",
			format = "number_type",
			default = 1,
		}
	},
}

cmd.ActorColor = {
	wrap_name = "Model's color",
	sort = 91,
	args ={
		{
			key = "excutor",
			name = "Operator",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "vicobjs",
		},
		{
			key = "color",
			name = "Color",
			complex_type = "complex_color",
			col = 3,
		},
		{
			key = "alive_time",
			name = "Maintainance time (?)",
			format = "number_type",
		},
		{
			key = "fade_time",
			name = "Time to change gradually (?)",
			format = "number_type",
			default = 0,
		},
	},
}

cmd.ActorMaterial = {
	wrap_name = "Balloon Material",
	sort = 93,
	args ={
		{
			key = "excutor",
			name = "Operator",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "vicobjs",
		},
		{
			key = "alive_time",
			name = "Maintainance time (?)",
			format = "number_type",
		},
		{
			key = "ease_show_time",
			name = "The disdeliver time changes gradually",
			format = "number_type",
			default = 0,
		},
		{
			key = "ease_hide_time",
			name = "The disappeared time changes gradually",
			format = "number_type",
			default = 0,
		},
		{
			name = "Balloon Material",
			key = "mat_path",
			select = function() 
					local newList = {}
					if Utils.IsEditor() then
						local list = IOTools.GetFiles(IOTools.GetGameResPath("/Material"), "*.mat", true)
						for i, sPath in ipairs(list) do
							local idx = string.find(sPath, "Material")
							if idx then
								table.insert(newList, string.sub(sPath, idx, string.len(sPath)))
							end
							
						end
					end
					return newList
				end,
			wrap = function(s) return IOTools.GetFileName(s, true) end,
			format = "string_type",
			input_width = 150,
		},
	},
}


cmd.KillTargetTween = {
	wrap_name = "Stop target action",
	sort = 98,
	args = {
		{
			key = "excutor",
			name = "Operator",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "vicobjs",
		},
	},
}



cmd.ControlObject = {
	wrap_name = "Set variaions",
	sort = 98,
	args ={
		{
			key = "name",
			name = "Variation",
			format = "string_type",
			default = "obj1",
		},
	},
}

cmd.SlowMotion = {
	wrap_name = "deliver slowly",
	sort = 98,
	args = {
		{
			key = "scale",
			name = "deliver SlowlySpeed",
			format = "number_type",
			default = 1,
		},
		{
			key = "time",
			name = "Maintainance time",
			format = "number_type",
			default = 1,
		},
	}
}

cmd.FloatHit = {
	wrap_name = "Suspended",
	sort = 99,
	args = {
		{
			key = "excutor",
			name = "Operator",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "vicobjs",
		},
		{
			key = "up_speed",
			name = "Speed ​​upFloat",
			format = "number_type",
			default = 10,
		},
		{
			key = "up_time",
			name = "Time ​​upFloat",
			format = "number_type",
			default = 0.2,
		},
		{
			key = "hit_speed",
			name = "Speed hitFloat",
			format = "number_type",
			default = 8,
		},
		{
			key = "hit_time",
			name = "Time hitFloat",
			format = "number_type",
			default = 0.2,
		},
		{
			key = "down_time",
			name = "Landing time",
			format = "number_type",
			default = 1,
		},
		{
			key = "lie_time",
			name = "Grounding time",
			format = "number_type",
			default = 0.5,
		},
	}
}

cmd.WarResultAnim = {
	wrap_name = "Ending animation",
	sort = 101,
	args ={},
}



cmd.End = {
	wrap_name = "End",
	sort = 100,
	args ={},
}



cmd.FloatTest= {
	wrap_name = "Floating Test",
	sort = 199,
	args ={
		{
			key = "up_speed",
			name = "Speed ​​upFloat",
			format = "number_type",
			default = 10,
		},
		{
			key = "up_time",
			name = "Time ​​upFloat",
			format = "number_type",
			default = 0.2,
		},
		{
			key = "hit_speed",
			name = "Speed hitFloat",
			format = "number_type",
			default = 8,
		},
		{
			key = "hit_time",
			name = "Time hitFloat",
			format = "number_type",
			default = 0.2,
		},
		{
			key = "down_time",
			name = "Landing time",
			format = "number_type",
			default = 1,
		},
		{
			key = "sk1",
			name = "Soul test 1",
			format = "number_type",
			default = 1,
		},
		{
			key = "sk2",
			name = "Soul test 2",
			format = "number_type",
			default = 1,
		},
	},
}

cmd.Shadow = {
	wrap_name = "Shadow",
	sort = 100,
	args ={		
		{
			key = "excutor",
			name = "Operator",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "atkobj",
		},
		{
			key = "is_show",
			name = "Disdeliver",
			select_type = "bool_type",
			default = true,
		},
	},
}

cmd.LockHide = {
	wrap_name = "Hidden Lock",
	sort = 101,
	args ={		
		{
			key = "excutor",
			name = "Operator",
			format = "string_type",
			select_type = "excutor_type",force_input = true, 
			default = "atkobj",
		},
	},
}

config.cmd = cmd

return config