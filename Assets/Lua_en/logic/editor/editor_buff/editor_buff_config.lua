config = {}

config.select =
{
	pos_type = {
		{"head", "Head"},
		{"waist", "Waist"},
		{"foot", "Leg"},
		{"chest", "In front of the chest"},
		{"node", "Knot"},
		{"model", "Paradigm"},
	},
	buff_type = {
		{"normal", "Normal"},
		{"add", "Pile up"},
		{"multi", "Multi-storey"},
		{"chain", "Connection"},
	},
}
config.arg = {}
config.arg.buff = {
	{
		name = "ID",
		key = "buff_id",
		format = "number_type",
		default = 0,
	},
	{
		name = "Type",
		key = "buff_type",
		select_type = "buff_type",
		default = "normal",
		refresh_args = true,
	},
	["add"] = {
		{
			name = "Pile up Quantity",
			key = "add_cnt",
			format = "number_type",
			default = 1,
		},
	}
}

config.arg.effect =
{
	{
		name = "Routing effect",
		key = "path",
		select = function() 
				local dirs = {"/Effect/Buff","/Effect/Magic"}
				local newList = {""}
				if Utils.IsEditor() then
					for _, dir in pairs(dirs) do
						local list = IOTools.GetFiles(IOTools.GetGameResPath(dir), "*.prefab", true)
						for i, sPath in ipairs(list) do
							local idx = string.find(sPath, dir)
							if idx then
								table.insert(newList, string.sub(sPath, idx+1, string.len(sPath)))
							end
						end
					end
				end
				return newList
			end,
		wrap = function(s) return IOTools.GetFileName(s, true) end,
		format = "string_type",
		input_width = 150,
	},
	{
		name = "Wooden ball (?)",
		key = "mat_path",
		select = function() 
				local list = IOTools.GetFiles(IOTools.GetGameResPath("/Material"), "*.mat", true)
				local newList = {""}
				for i, sPath in ipairs(list) do
					local idx = string.find(sPath, "Material")
					if idx then
						table.insert(newList, string.sub(sPath, idx, string.len(sPath)))
					end
				end
				return newList
			end,
		wrap = function(s) return IOTools.GetFileName(s, true) end,
		format = "string_type",
		input_width = 150,
	},
	{
		name = "Height",
		key = "height",
		format = "number_type",
		default = 0,
	},
	{
		name = "Type of location",
		key = "pos_type",
		select_type = "pos_type",
		default = "waist",
		input_width = 150,
		refresh_args = true,
	},
	["node"] = {
		{
			name = "ID contact",
			key = "node_idx",
			format = "number_type",
			default = 0,
		}
	},
	["model"] = {
		{
			name = "Paradigm",
			key = "find_path",
			select = {"Mount_Hit", "Mount_Head", "Mount_Shadow", "Bip001"},
			format = "string_type",
			default = "Bip001",
		}
	},
}
config.arg.template ={


	layer_paths = {
		name = "Buff layer",
		key = "chest_layer",
		select = function() 
				local list = IOTools.GetFiles(IOTools.GetGameResPath("/Effect/Buff"), "*.prefab", true)
				local newList = {""}
				for i, sPath in ipairs(list) do
					local idx = string.find(sPath, "Effect/Buff")
					if idx then
						table.insert(newList, string.sub(sPath, idx, string.len(sPath)))
					end
					
				end
				return newList
			end,
		wrap = function(s) return IOTools.GetFileName(s, true) end,
		format = "string_type",
		input_width = 150,
	},
}
return config