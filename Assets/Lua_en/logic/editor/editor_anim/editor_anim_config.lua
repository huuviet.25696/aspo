config = {}
config.select =
{	
}
config.arg = {}

config.arg.template = {
	action = {
		name = "Action",
		key = "action",
		select = ModelTools:GetAllState(),
		default = "attack1",
	},
	start_frame = {
		name = "Start",
		key = "start_frame",
		format = "number_type",
		default = 0,
		frame_to_time = true,
	},
	end_frame = {
		name = "Finish frame",
		key = "end_frame",
		format = "number_type",
		default = 0,
		frame_to_time = true,
		change_refresh = 1,
	},
	shape = {
		name = "Figure",
		key = "shape",
		select = ModelTools:GetAllModelShape(),
		default = 101,
		change_refresh = 2,
	},
	hit_frame = {
		name = "hit frame",
		key = "hit_frame",
		format = "number_type",
		default = 0,
		frame_to_time = true,
	},
	speed = {
		name = "Speed",
		key = "speed",
		format = "number_type",
		default = 1,
		change_refresh = 1,
	},
	hit = {
		name = "hit frame",
		key = "hit",
		format = "number_type",
		change_refresh = 1,
	},
	hurt = {
		name = "HP loosing time",
		key = "hurt",
		format = "number_type",
	},
	endhit = {
		name = "End of being attacked",
		key = "endhit",
		format = "number_type",
	},

	name = {
		name = "Name",
		key = "name",
		select_update = function ()
			local oView = CEditorAnimView:GetView()
			if oView then
				return oView:GetAnimSequenceName()
			end
		end,
		force_input = true,
		change_refresh = 1,
	}
}

return config