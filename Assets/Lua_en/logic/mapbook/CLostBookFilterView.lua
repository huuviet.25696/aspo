local CLostBookFilterView = class("CLostBookFilterView", CViewBase)

CLostBookFilterView.LostData = {
	{
		name = "Lost task can be found",
		list = {"Not specified", "High to low", "Low to high"},
	},
	{
		name = "",
		list = {"Not specified", "Edited drawing", "Written name", "Unedited statue", "Unwritten name"},
	},
	{
		name = "Lost state can be found",
		list = {"Not specified", "Not unlocked yet", "Can unlock", "Unlocked", "Unlocked unread", "Read"},
	},
	{
		name = "Character gender",
		list = {"Not specified", "She", "He", "It"},
	},
}

CLostBookFilterView.PartnerData = {
	{
		name = "Task Chapter",
		list = {"Not specified", "High to low", "Low to high"},
	},
	{
		name = "Hero Quality",
		list = {"Not specified", "Legend", "Tinh Anh"},
	},
	{
		name = "Meeting state",
		list = {"Not specified", "Have not met", "Met", "Meet unread", "Read"},
	},
	{
		name = "Hero gender",
		list = {"Not specified", "She", "He"},
	},
}

function CLostBookFilterView.ctor(self, cb)
	CViewBase.ctor(self, "UI/MapBook/LostBookFilterView.prefab", cb)
	self.m_ExtendClose = "Black"
	self.m_DepthType = "Dialog"
end

function CLostBookFilterView.OnCreateView(self)
	self.m_CloseBtn = self:NewUI(1, CButton)
	self.m_Table = self:NewUI(2, CTable)
	self.m_ItemBox = self:NewUI(3, CBox)
	self.m_ResetBtn = self:NewUI(4, CButton)
	self.m_ConfirmBtn = self:NewUI(5, CButton)
	self:InitContent()
end

function CLostBookFilterView.InitContent(self)
	self.m_ItemBox:SetActive(false)
	self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))
	self.m_ResetBtn:AddUIEvent("click", callback(self, "OnReset"))
	self.m_ConfirmBtn:AddUIEvent("click", callback(self, "OnConfirm"))
	--self:CreateTable()
end

function CLostBookFilterView.SetType(self, sType)
	self.m_Type = sType
	self:CreateTable()
end

function CLostBookFilterView.CreateTable(self)
	self.m_Table:Clear()
	local filterdata = nil
	if self.m_Type == "PartnerBook" then
		filterdata = CLostBookFilterView.PartnerData
	
	elseif self.m_Type == "LostBook" then
		filterdata = CLostBookFilterView.LostData
	end
	
	for _, tdata in ipairs(filterdata) do
		local box = self.m_ItemBox:Clone()
		box:SetActive(true)
		box.m_Label = box:NewUI(1, CLabel)
		box.m_Grid = box:NewUI(2, CGrid)
		box.m_Btn = box:NewUI(3, CButton)
		box.m_Btn:SetActive(false)
		box.m_Label:SetText(tdata["name"])
		box.m_Grid:Clear()
		for _, name in ipairs(tdata["list"]) do
			local btn = box.m_Btn:Clone()
			btn:SetGroup(box:GetInstanceID())
			btn:SetActive(true)
			btn:SetText(name)
			box.m_Grid:AddChild(btn)
			btn.m_Name = name
			if name == "Not specified" then
				btn:SetSelected(true)
			end
		end
		box.m_Grid:Reposition()
		self.m_Table:AddChild(box)
	end
	self.m_Table:Reposition()
end

function CLostBookFilterView.SetLastList(self, keyList, callBack)
	keyList = keyList or {}
	for i, box in ipairs(self.m_Table:GetChildList()) do
		local name = keyList[i] or "Not specified"
		for _, btn in ipairs(box.m_Grid:GetChildList()) do
			if btn.m_Name == name then
				btn:SetSelected(true)
				break
			end
		end
	end
	self.m_CallBack = callBack
end

function CLostBookFilterView.OnReset(self)
	for _, box in ipairs(self.m_Table:GetChildList()) do
		for _, btn in ipairs(box.m_Grid:GetChildList()) do
			if btn.m_Name == "Not specified" then
				btn:SetSelected(true)
				break
			end
		end
	end
end

function CLostBookFilterView.OnConfirm(self)
	local list = {}
	for _, box in ipairs(self.m_Table:GetChildList()) do
		for _, btn in ipairs(box.m_Grid:GetChildList()) do
			if btn:GetSelected() then
				table.insert(list, btn.m_Name)
				break
			end
		end
	end
	if self.m_CallBack then
		self.m_CallBack(list)
	end
	self:OnClose()
end

return CLostBookFilterView