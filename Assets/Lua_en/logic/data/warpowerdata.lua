module(...)
--auto generate data
Reward={
	[1]={
		desc=[[Top 1]],
		id=1,
		reward={[1]=1001,[2]=1003,[3]=1006,},
		weeky_award={
			[1]={id=10027,num=1,},
			[2]={id=13103,num=1,},
			[3]={id=10019,num=3,},
		},
	},
	[3]={
		desc=[[Top 2-3]],
		id=3,
		reward={[1]=1001,[2]=1004,[3]=1007,},
		weeky_award={
			[1]={id=10027,num=1,},
			[2]={id=13102,num=1,},
			[3]={id=10019,num=2,},
		},
	},
	[10]={
		desc=[[Top 4-10]],
		id=10,
		reward={[1]=1002,[2]=1005,[3]=1007,},
		weeky_award={
			[1]={id=10026,num=1,},
			[2]={id=13101,num=1,},
			[3]={id=10019,num=2,},
		},
	},
	[20]={
		desc=[[Top 11-20]],
		id=20,
		reward={[1]=1002,[2]=1007,},
		weeky_award={[1]={id=10026,num=1,},[2]={id=10019,num=2,},},
	},
	[50]={
		desc=[[Top 21-50]],
		id=50,
		reward={[1]=1008,},
		weeky_award={[1]={id=10019,num=1,},},
	},
}
