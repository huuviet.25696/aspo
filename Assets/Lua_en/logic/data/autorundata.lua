module(...)
--auto generate data
SCENEDATA={
	[401010]={
		desc=[[5-10 cấp độ]],
		event=1001,
		icon=[[half_5101]],
		id=401010,
		jiaobiao=[[]],
		monster={[1]=10001,[2]=10002,},
		name=[[大雁塔一]],
		sort=1,
	},
	[401020]={
		desc=[[11-20 cấp độ]],
		event=1001,
		icon=[[half_5109]],
		id=401020,
		jiaobiao=[[]],
		monster={[1]=10002,[2]=10003,},
		name=[[大雁塔二]],
		sort=2,
	},
	[401030]={
		desc=[[21-30 cấp độ]],
		event=1001,
		icon=[[half_5111]],
		id=401030,
		jiaobiao=[[]],
		monster={[1]=10003,[2]=10004,},
		name=[[大雁塔三]],
		sort=3,
	},
	[401040]={
		desc=[[31-40 cấp độ]],
		event=1001,
		icon=[[half_5114]],
		id=401040,
		jiaobiao=[[]],
		monster={[1]=10004,[2]=10005,},
		name=[[大雁塔四]],
		sort=4,
	},
}
