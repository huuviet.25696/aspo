module(...)
--auto generate data
FARDESC=[[Warning! The deviation is more than 50m!]]
NEARDESC=[[Detected there is a Treasure's energy reaction ahead!]]
HEREDESC=[[Hunt for treasure!]]

CHANGGUI={[1]=[[转吧，转完这周不用吃土了。]],}

BAODI={[1]=[[上!我感受到了王之宝藏的气息！]],}

MID={
	[1]=[[Ah ah… puff puff]],
	[2]=[[Billow, go and go ~]],
	[3]=[[^^ I'm a little fairy]],
	[4]=[[Stay away, I'll turn here]],
	[5]=[[Attack, spin!]],
}

STOP={[1]=[[Well done, get a flower]],[2]=[[Great!]],}

EVENTTIPS={
	[1]={
		desc=[[Can activate Meeting-Flying Leaves]],
		id=1,
		name=[[LiuLi is sleeping]],
		spirte=404,
	},
	[2]={
		desc=[[Can activate Meeting-Decisive Blow]],
		id=2,
		name=[[FeiLong loves chess]],
		spirte=1150,
	},
}
