module(...)
--auto generate data
DATA={
	PartnerEquipStrongView={
		content=[["[d9ceba]◆Leveling up the Rune can effectively increase 2 main attributes, the higher the level of the Rune star is, the more the effect increases each time, up to level 15
[d9ceba]◆Every time the enhancement level reaches [4edaad]3, 6, 9, 12, 15[d9ceba], will randomly increase 1 line of sub-attributes or increase the existing sub-attributes
[d9ceba]◆When using Rune star and higher level Rune as an enhancement material, the more EXP it gives and the more Gold it costs.
[d9ceba]◆The higher the Rune star level, the more EXP is needed to level up, when using Rune EXP as a material, providing a large amount of Rune EXP
[d9ceba]◆Quick add can only add [4edaad]Experience Rune[d9ceba] and [4edaad]Normal Runner 1~2 stars[d9ceba]
[d9ceba]◆Rune EXP is only used as a material to provide Rune EXP, cannot be enhanced"]],
		key=[[PartnerEquipStrongView]],
		pichelp={},
		title=[[Enhance the Rune]],
	},
	anlei_main={
		content=[[[8c8783]Searching Rules:
[d9ceba]◆In the map, click on the area with directions to search
◆The searching area requires 1 searching point to conduct a search.
◆00:00 every day, the system provides [4edaad]50 searching points[d9ceba].
◆If searching points are not enough, spend Diamonds to buy searching points.
◆Can buy the maximum of 150 searching points every day.
◆Successful searched can be rewarded.
◆Depending on different map names, [4edaad]Elite Monster[d9ceba] and [4edaad]Secret Treasure Hunter[d9ceba] activation will be different.
◆On the way to search, there is a chance to meet [4edaad]Secret Treasure Hunter[d9ceba] and [4edaad]Elite Monster[d9ceba].
◆The Secret Treasure Hunter and the Elite Monster can be teamed up.
◆Secret Treasure Hunter received [4edaad]large amount of Gold[d9ceba].
◆The Elite Monster received [4edaad]rare reward[d9ceba].]],
		key=[[anlei_main]],
		pichelp={
			[1]=[[pic_rukou_yuanhua_1; The area where the new pointer is searched for the drop is displayed in the map]],
			[2]=[[pic_rukou_yuanhua_2; 0h per day restores 50 searching points each searching time consumes 1 point]],
			[3]=[[pic_rukou_yuanhua_3; Offline Search can support 30m quickly start searching!]],
		},
		title=[[Research Notification]],
	},
	anlei_offline_tips={
		content=[[[8c8783] Searching commissioned Notification:
[d9ceba]◆During the searching, when offline or changing backstage, you can continue to search.
◆ Offline search supports 30 minutes. (Monthly Card, Lifetime Card can be enjoyed for 1 hour.)
◆The system starts to calculate the time after offline, after reaching the corresponding time can receive the reward.
◆Rewards are received directly online.]],
		key=[[anlei_offline_tips]],
		pichelp={},
		title=[[Offline Research]],
	},
	arena={
		content=[["◆21:00-22:00 Mon-Friday opens World Match, pairing players with the same Rank to fight
◆The winner will get the loser's Match Score, reaching a certain level of Match Score can increase the Rank
◆Reward will get depend on player Rank
◆0:00 every Monday, distribute Match rewards according to the current Rank, then reset the Match Score equal to the base score of the current Rank"]],
		key=[[arena]],
		pichelp={},
		title=[[World Match]],
	},
	attr_main={
		content=[[[8c8783]◆Pilot Combat Power Resources: Enhance Gear level, Upgrade Gems, Enchant attributes, Forge Orange Gears, Gear set effects...
[8c8783]◆Valarion Combat Power Resources: Valarion level, Star level, Valarion awakening, Rune and Jewel...]],
		key=[[attr_main]],
		pichelp={},
		title=[[Pilot attributes]],
	},
	chouka1={
		content=[[[e3c280]The Ratio of getting [c8c8c8]Normal Summon[e3c280]:
   [e3c280]- [c8c8c8]4%[e3c280] getting Common Unit: [6AD709]Europa, Isaiah Lass, Elesis, Kibera, Dio, Nyx, Oddy[e3c280]
   [e3c280]- [c8c8c8]1%[e3c280] getting Rare Unit: [34C8F4]Ezekiel, Naveed, Natthan, Zero, Lire, Grandiel, Clause, Rephy[e3c280]
   [e3c280]- [c8c8c8]45%[e3c280] getting Item: [c8c8c8]Maiden Stone x50, Golden Stone x5, Valarion Core x10, Lucky Shard x5, Energy Bag x10[e3c280]
   [e3c280]- [c8c8c8]50%[e3c280] getting Item: [c8c8c8]Lunar Stone x1, Velator Potion x100, Emerald Stone-3 x1, Onyx Stone-3 x1, Topaz Stone-3 x1[e3c280] ]],
		key=[[chouka1]],
		pichelp={},
		title=[[Normal Summon]],
	},
	chouka2={
		content=[[[e3c280]The Ratio of getting [c8c8c8]Rare Summon[e3c280]:
   [e3c280]- [c8c8c8]23%[e3c280] getting Rare Unit: [34C8F4]Ezekiel, Naveed, Natthan, Zero, Lire, Grandiel, Clause, Rephy[e3c280]
   [e3c280]- [c8c8c8]3%[e3c280] getting Elite Unit: [e110fa]Lucikiel, Ronan, Ryan, Liana, Alice, Lei, Callisto, Loman[e3c280]
   [e3c280]- [c8c8c8]2%[e3c280] getting Ascension Unit: [FAB310]Mitron, Marth, Erze[e3c280]
   [e3c280]- [c8c8c8]30%[e3c280] getting Item: [c8c8c8]Material Chest x5, (LV50) Orange Gear x1, Rising Stone x5[e3c280]
   [e3c280]- [c8c8c8]42%[e3c280] getting Item: [c8c8c8]Amethyst Stone-3 x5, Pearl Stone-3 x5, Lapis Stone-3 x5, Lunar Stone x10[e3c280] ]],
		key=[[chouka2]],
		pichelp={},
		title=[[Rare Summon]],
	},
	chouka3={
		content=[[[e3c280]The Ratio of getting [c8c8c8]Special Summon[e3c280]:
   [e3c280]- [c8c8c8]36%[e3c280]  getting Elite Unit: [e110fa]Alice, Lei, Callisto, Loman[e3c280]
   [e3c280]- [c8c8c8]40%[e3c280] getting Ascension Unit: [FAB310]Mitron, Marth, Erze, Shin, Ranzal, Joe, Lowen, Mediana[e3c280]
   [e3c280]- [c8c8c8]24%[e3c280] getting Legendary Unit: [EC2213]Rehan, Yotsuyu, Nerdick, Shuu, Pietro, Michael, Theria, Laudia[e3c280]
]],
		key=[[chouka3]],
		pichelp={},
		title=[[Special Summon]],
	},
	clubarena={
		content=[["[8c8783]Master Challenge
[d9ceba]◆Players can conduct manual battle
[d9ceba]◆Win the battle, if the Master of defense team is higher than the attacking team, the 2 faction's Master swaps places
[d9ceba]◆Each player has 5 free battles per day, 00:00 reset the number of free battles per day
[d9ceba]◆After the battle is over, the attacking team receives the battle reward
[d9ceba]◆Failure of the battle will have a countdown, after the countdown is over, you can continue to challenge
[d9ceba]◆Fighting enemies cannot be selected as opponents
[d9ceba]◆00:00 Every day the ranking is summed up, and through the mail, the ranks are distributed to the Master Player (except for the Starter Rank).
[8c8783]Match Challenge
[d9ceba]◆Win a member with the same Master Challenge 5 times, you can challenge this rank Owner
[d9ceba]◆Challenging the rank Owner doesn't need to take the number of times to challenge, it only takes Gold
[d9ceba]◆Win the battle, swap positions
[d9ceba]◆The group Owner will accumulate the rank owner bonus over time
[d9ceba]◆Starter rank does not have a Rank Owner.
[8c8783] The number of people that Master challenges can contain
[d9ceba]◆Master Rank (10 people)
[d9ceba]◆Diamond Rank (30 people)
[d9ceba]◆Golden Rank (60 people)
[d9ceba]◆Silver Rank (150 people)
[d9ceba]◆Bronze Rank (250 people)."]],
		key=[[clubarena]],
		pichelp={},
		title=[[Match]],
	},
	convert={
		content=[[[d9ceba]◆ Rules:
[d9ceba]◆1. GNEMO, Valarions, gears, lucky charms obtain in the game cannot mint NFT
[d9ceba]◆2. Cannot join Universal Battle
[d9ceba]◆3. Cannot use auto burn features on marketplace
[d9ceba]◆4. Cannot duel with other player
[d9ceba]◆ Note:
[d9ceba]◆When your success convert F2P to P2E can unlock all features in the rules
[d9ceba]◆Convert F2P to P2E must purchase 4 Valarions then lock in your wallet on the marketplace]],
		key=[[convert]],
		pichelp={},
		title=[[Free to Play]],
	},
	convoy={
		content=[["[d9ceba]◆Every day can conduct [4edaad]3[d9ceba] times of escorting, after starting the mission costs 1 time, every day 0h reset the number of times
[d9ceba]◆The higher the escort target milestone, the higher the reward. Through [4edaad]refresh target[d9ceba] can refresh the escort milestone this time, there will be a higher Ratio of claiming the [4edaad]milestone[d9ceba]
[d9ceba]◆There is a certain number of [4edaad]free[d9ceba] refreshes every day, after using up the free times, it can cost [4edaad]Diamonds[d9ceba] to refresh
[d9ceba]◆During escorting, there will be a Ratio of encountering obstacles, after winning the battle, continue to escort, be defeated, the quest failed, need to get the mission back
[d9ceba]◆If you stop escorting midway, this quest failed, you need to get the quest again"]],
		key=[[convoy]],
		pichelp={},
		title=[[Guardian]],
	},
	editexpress={
		content=[[Confession edit]],
		key=[[editexpress]],
		pichelp={},
		title=[[Confession edit]],
	},
	endlesspve={
		content=[["[d9ceba]◆Using [4edaad]Full Moon Ticket[d9ceba] can open Fate Calibration, [4edaad]Full Moon Ticket[d9ceba] is received through [4edaad]Daily Excitement[d9ceba].
[d9ceba]◆When Illusion is opened, in [4edaad]10p[d9ceba] knockback the Illusion it attacks, according to the number of knockbacks, can drop a generous Valarion Gold/Gem bonus.
[d9ceba]◆Illusion has 3 difficulty levels, open at Lv46/60/75 in turn.
[d9ceba]◆Can choose solo or team to enter the Illusion, maximum team [4edaad]2 people[d9ceba]. Joining another team does not cost Full Moon Ticket, but the number of times per day to join the team is limited to [4edaad]3 times[d9ceba].
[d9ceba]◆Solo and team fight back the same number of waves, the value of items received in each wave is the same,[4edaad]recommends the team to enter[d9ceba], actively helps other players."]],
		key=[[endlesspve]],
		pichelp={},
		title=[[Fate Calibration]],
	},
	equalarena={
		content=[["[d9ceba]◆Universal Battle opens at 00:00-23:00 every day, for players Lv.35 or higher
[d9ceba]◆After successfully paired, the players take turn to select Valarions and the Jewel Talent from the system, respectively. After choosing 4 Valarions and 4 sets of Jewel Talent. Valarions to install the Jewel Talent and go into battle
[d9ceba]◆Universal Battle battle attributes are given by the system, Valarion value is the same and skill level is exactly the same
[d9ceba]◆Winner get Universal Battle points from loser, 0:00 on the 1st of every month rewards are distributed based on Point Ranking, then reset to 2000 points."]],
		key=[[equalarena]],
		pichelp={},
		title=[[Universal Battle]],
	},
	equalarenaworld={
		content=[["[d9ceba]◆World Universal Battle opens at 10:00 - 19:00 from Mon to Fri, for players Lv.50 or higher
[d9ceba]◆After successfully paired, the players take turn to select Valarions and the Jewel Talent from the system, respectively. After choosing 4 Valarions and 4 sets of Jewel Talent. Valarions to install the Jewel Talent and go into battle
[d9ceba]◆World Universal Battle battle attributes are given by the system, Valarion value is the same and skill level is exactly the same
[d9ceba]◆Winner get Honor points from loser, 0:00 every Monday rewards are distributed based on Point Ranking, then reset to 2000 points."]],
		key=[[equalarenaworld]],
		pichelp={},
		title=[[World Universal Battle]],
	},
	equipfuben_1={
		content=[["[8c8783] Ozma Temple
[d9ceba]◆Ozma Temple Dungeon drops Gear Enhance Materials and Attack/Resistance Gems, enough stars to claim the corresponding level Purple Weapon/Shoe
[d9ceba]◆Each time passing the dungeon chap takes [4edaad]12 Energy[d9ceba]; Not having passed the Dungeon only takes [4edaad]3 Energy[d9ceba]."]],
		key=[[equipfuben_1]],
		pichelp={},
		title=[[Ozma Temple]],
	},
	equipfuben_2={
		content=[["[8c8783] Ezora Temple
[d9ceba]◆Ezora Temple Dungeon drops Equip Enhance Materials and Attack/Defense Gems, enough stars to claim the corresponding level PurpleChakram/Helmet
[d9ceba]◆Each time passing the Dungeon takes [4edaad]12 Energy[d9ceba]; Not having passed the Dungeon only takes [4edaad]3 Energy[d9ceba]."]],
		key=[[equipfuben_2]],
		pichelp={},
		title=[[Ezora Temple]],
	},
	equipfuben_3={
		content=[["[8c8783]Acient Temple
[d9ceba]◆Acient Temple Dungeon drops Gear Enhance Materials and Speed/Energy Gems, enough stars to claim the corresponding Purple Armors/Guards
[d9ceba]◆Each time passing the Dungeon takes [4edaad]12 Energy[d9ceba]; Not having passed the Dungeon only costs [4edaad]3 Energy[d9ceba]."]],
		key=[[equipfuben_3]],
		pichelp={},
		title=[[Acient Temple]],
	},
	equipfuben_main={
		content=[["[d9ceba]◆Joint Strike creates a lot of good Gear and enhancing materials.
[d9ceba]◆Joint Strike is divided into [4edaad]3 Dungeons [d9ceba], each dungeon has [4edaad]8 floors[d9ceba], after passing 1 floor, unlock the next floor. Passing the floor for the first time to receive the corresponding grade blue Gear
[d9ceba]◆After passing 1 dungeon, will be based on the performance to evaluate the star level. The higher the star level passed, the more the number of drops are
[d9ceba]◆Dungeon Passing Star level will be accumulated, when in a certain dungeon receiving a specified number of stars, can choose 1 [4edaad]Purple[d9ceba] Gear or higher of the corresponding level
[d9ceba]◆After passing the 3-star dungeon, you can also open this [4edaad]sweeping[d9ceba] feature of this Dungeon, costing 2 Skip Tickets to receive a 3-star reward immediately
[d9ceba]◆It costs [4edaad]12 Energy[d9ceba] every time you pass the dungeon; The un-passed dungeon only costs [4edaad]3 Energy[d9ceba]."]],
		key=[[equipfuben_main]],
		pichelp={},
		title=[[Joint Strike]],
	},
	field_boss={
		content=[["[8c8783]Master Challenge
[d9ceba]◆Players can conduct manual battle.
[d9ceba]◆Win the battle, if the Master team defense is higher than the attacking team, the 2 faction's Master swaps places.
[d9ceba]◆Each player has 5 free battle times per day, 00:00 reset the number of times per day.
[d9ceba]◆After the battle is over, the attacking team receives the battle reward.
[d9ceba]◆Failure of the battle will have a countdown, after the countdown is over, you can continue to challenge.
[d9ceba]◆Fighting enemies cannot be selected as opponents.
[d9ceba]◆00:00 Every day the ranking is summed up, and through the mail, the ranks are distributed to the Master Player (except for the Starter Group).
[8c8783]Challenge the Shop owner
[d9ceba]◆Win a member with the same Master Challenge 5 times, you can challenge this group owner
[d9ceba]◆Challenging the group owner doesn't need to take the number of times to challenge, it only takes Gold.
[d9ceba]◆Win the battle, swap positions.
[d9ceba]◆The group owner will accumulate the group owner bonus over time.
[d9ceba]◆Starter group does not have a Group Owner.
[8c8783] The number of people that master challenge can contain
[d9ceba]◆Master Group (10 people)
[d9ceba]◆Diamond Group (30 people)
[d9ceba]◆Golden Group (60 people)
[d9ceba]◆Silver Group(150 people)
[d9ceba]◆Bronze Group (250 people)"]],
		key=[[field_boss]],
		pichelp={},
		title=[[Dimensional Chasm]],
	},
	forge_composite={
		content=[[[d9ceba]◆Can use Gear Ether, Ghost Ether, and Fusion Sand to forge an Orange Gear, which has special effects and higher attribute values than regular Purple Gear
[d9ceba]◆Weapon will have a strong effect; equip Clothes+Helmet+Boots of the same type to activate Costume set effect; equipChakrams+Guards of the same type to activate Jewelry set effect
[d9ceba]◆The higher the level of the combining Gear is, the more materials it costs, the greater the skill is
[d9ceba]◆Premium Gear can use low-grade Gear as supplement materials to combine without loss.]],
		key=[[forge_composite]],
		pichelp={},
		title=[[Forge Gear]],
	},
	forge_composite_random_1={
		content=[["[d9ceba]◆You can use Weapon Ether, Ghost Ether, and Fusion Sand to use as materials, to create Orange Weapons level 50-100. The advanced Gear effectiveness number is higher
[d9ceba]◆After successfully creating, based on the suffix of Orange Weapon, you will receive the following strong effect:
[d9ceba][Fire] Turns used single damage skill, have 40% chance to get battle buff, next turn's single skill deals 15-45% bonus damage, in 1 turn
[d9ceba][Water] Before starting each turn, have a 40% chance to increase ATK equal to 50-160% of DEF, maintains in 1 action
[d9ceba][Lightning] Have 40% chance to cause each hit skill to deal 3-8% extra damage*number of targets
[d9ceba][Earth] For every 1 enemy member who falls into a control type strange status, have 40% chance to deal pure damage to the enemy equal to 25-75% of ATK
[d9ceba][Wind] Have 40% chance of reducing the target's CRIT RES rate by 3-8% before executing the skill, lasts until the end of that turn
[d9ceba][Ice] When there is unit recovering HP on the battle , have 40% chance to deal HP equal to 20-60% ATK to allied unit with the lowest HP rate
[d9ceba]◆The Ratio of producing 6 types of effect are the same."]],
		key=[[forge_composite_random_1]],
		pichelp={},
		title=[[Gear creating]],
	},
	forge_composite_random_2={
		content=[["[d9ceba]◆You can use Guard Ether, Ghost Ether, and Fusion Sand as materials to create a grade Orange Guard of Level 50~100. The advanced Gear effectiveness number is higher
[d9ceba]◆When equipping Guards andChakrams with the same suffix, get the following powerful effects (Same suffix can be activated, skill level is based on low level Gear):
[d9ceba][Fire] When the enemy performs group skills, 5-20% rate add to 20% of the total DMG taken to your own ATK power, in 1 turn
[d9ceba][Water] When the enemy using recover skill, 5-20% stealing rate to HP of 1 unit revive the unit, reviving for the unit of the lowest HP on our side
[d9ceba][Lightning] When the enemy causes CRIT DMG, 5-20% chance to take away 12% of his CRIT Rate, in 1 turn
[d9ceba][Earth] When unit in ridiculous status of our side conducting normal attack, 5-20% chance to increase 350% DMG
[d9ceba][Wind] When enemy uses single skill, 5-20% chance to add 300% Fury increasing CRIT DMG, in 1 turn
[d9ceba][Ice] When our unit is attached to strange status, 5-20% rate reducing 1 turn for 1 unit
[d9ceba]◆The Ratio of producing 6 types of effect are the same."]],
		key=[[forge_composite_random_2]],
		pichelp={},
		title=[[Gear creating]],
	},
	forge_composite_random_3={
		content=[["[d9ceba]◆It is possible to use Armor Ether, Ghost Ether, and Fusion Sand as materials, to combine a Level 50-100 Orange Armor. The advanced Gear effectiveness number is higher
[d9ceba]◆When equipping Armor, Helmet, Boots with the same suffix, get the following powerful effect (Same suffix can be activated, skill level is based on low level Gear):
[d9ceba][Fire] When your team is attacked, having 8% chance to make received DMG source increase 4-14% DMG, in 1 turn
[d9ceba][Water] When your team is attacked, having 8% chance to reduce DMG, and reducing 6-18% DMG
[d9ceba][Lightning] When your team is attacked, having 10% chance to increase yourself 4-12% ATK and Speed of team
[d9ceba][Earth] When your team is attacked, having 4-9% chance to silence attacker
[d9ceba][Wind] When your team is attacked, having 8% chance to increase your team and yourself 5-20% CRIT Rate, in 1 turn
[d9ceba][Ice] When your team is attacked, having 8% chance to heal the team the amount of HP equals to 10-25% of their HP
[d9ceba]◆The Ratio of producing 6 types of effect are the same."]],
		key=[[forge_composite_random_3]],
		pichelp={},
		title=[[Gear creating]],
	},
	forge_composite_random_4={
		content=[["[d9ceba]◆You can useChakram Ether, Ghost Ether, and Fusion Sand as materials to create a grade OrangeChakram of Level 50~100. The advanced Gear effectiveness number is higher
[d9ceba]◆When equipping Guards andChakrams with the same suffix, get the following powerful effects (Same suffix can be activated, skill level is based on low level Gear):
[d9ceba][Fire] When the enemy performs group skills, 5-20% rate add to 20% of the total DMG taken to your own ATK power, in 1 turn
[d9ceba][Water] When the enemy using recover skill, 5-20% stealing rate to HP of 1 unit revive the unit, reviving for the unit of the lowest HP on our side
[d9ceba][Lightning] When the enemy causes CRIT DMG, 5-20% chance to take away 12% of his CRIT Rate, in 1 turn
[d9ceba][Earth] When unit in ridiculous status of our side conducting normal attack, 5-20% chance to increase 350% DMG
[d9ceba][Wind] When enemy uses single skill, 5-20% chance to add 300% Fury increasing CRIT DMG, in 1 turn
[d9ceba][Ice] When our unit is attached to strange status, 5-20% rate reducing 1 turn for 1 unit
[d9ceba]◆The Ratio of producing 6 types of effect are the same."]],
		key=[[forge_composite_random_4]],
		pichelp={},
		title=[[Gear creating]],
	},
	forge_composite_random_5={
		content=[["[d9ceba]◆It is possible to use Helmet Ether, Ghost Ether, and Fusion Sand as materials, to combine a Level 50-100 Orange Helmet. The advanced Gear effectiveness number is higher
[d9ceba]◆When equipping Armor, Helmet, Boots with the same suffix, get the following powerful effect (Same suffix can be activated, skill level is based on low level Gear):
[d9ceba][Fire] When your team is attacked, having 8% chance to make received DMG source increase 4-14% DMG, in 1 turn
[d9ceba][Water] When your team is attacked, having 8% chance to reduce DMG, and reducing 6-18% DMG
[d9ceba][Lightning] When your team is attacked, having 10% chance to increase yourself 4-12% ATK and Speed of team
[d9ceba][Earth] When your team is attacked, having 4-9% chance to silence attacker
[d9ceba][Wind] When your team is attacked, having 8% chance to increase your team and yourself 5-20% CRIT Rate, in 1 turn
[d9ceba][Ice] When your team is attacked, having 8% chance to heal the team the amount of HP equals to 10-25% of their HP
[d9ceba]◆The Ratio of producing 6 types of effect are the same."]],
		key=[[forge_composite_random_5]],
		pichelp={},
		title=[[Gear creating]],
	},
	forge_composite_random_6={
		content=[["[d9ceba]◆It is possible to use Boots Ether, Ghost Ether, and Fusion Sand as materials, to combine a Level 50-100 Orange Boots. The advanced Gear effectiveness number is higher
[d9ceba]◆When equipping Armor, Helmet, Boots with the same suffix, get the following powerful effect (Same suffix can be activated, skill level is based on low level Gear):
[d9ceba][Fire] When your team is attacked, having 8% chance to make received DMG source increase 4-14% DMG, in 1 turn
[d9ceba][Water] When your team is attacked, having 8% chance to reduce DMG, and reducing 6-18% DMG
[d9ceba][Lightning] When your team is attacked, having 10% chance to increase yourself 4-12% ATK and Speed of team
[d9ceba][Earth] When your team is attacked, having 4-9% chance to silence attacker
[d9ceba][Wind] When your team is attacked, having 8% chance to increase your team and yourself 5-20% CRIT Rate, in 1 turn
[d9ceba][Ice] When your team is attacked, having 8% chance to heal the team the amount of HP equals to 10-25% of their HP
[d9ceba]◆The Ratio of producing 6 types of effect are the same."]],
		key=[[forge_composite_random_6]],
		pichelp={},
		title=[[Gear creating]],
	},
	forge_exchange={
		content=[[[d9ceba]◆Can spend Ghost Ether to convert Orange weapon/Gear into another random suffix
[d9ceba]◆Recommend needed when you need to collect the full set of effect.]],
		key=[[forge_exchange]],
		pichelp={},
		title=[[Convert Gear]],
	},
	forge_fuwen={
		content=[[[d9ceba]◆ Level 50 will unlock Enchant feature with Normal and Advance mode. Must be using material for enchant.
[d9ceba]- Normal Enchant: Use Memory Crystal
[d9ceba]- Advance Enchant: Use Soul Speeling Cloud and Lucky Charm
[d9ceba]◆ Advance enchant can be unlocked to 5 attributes
[d9ceba]◆ The basic grade of advance enchant are Elite.
[d9ceba]◆The reseting Enchant attribute needs to click “Save” to take effect
[d9ceba]◆Enchant attribute limit related to current Gear level
[d9ceba]◆Gear changing automatically inherits the Enchant attribute of the previously Gear
]],
		key=[[forge_fuwen]],
		pichelp={},
		title=[[Enchant]],
	},
	forge_gem={
		content=[["[d9ceba]◆Each fixed item encrusts 1 type of Gem, every 10 levels can unlock 1 Gem box.
[d9ceba]◆After changing Gear, Gems automatically inherit without loss on the new Gear
[d9ceba]◆3 Gems of the same level combine to form a Gem that is higher than 1 level
[d9ceba]◆The Gems in the Gear need to be removed before they can be combined
[d9ceba]◆[Quick Upgrade] will encrust Gems according to the preferred scheme"]],
		key=[[forge_gem]],
		pichelp={},
		title=[[Gear Gem]],
	},
	forge_resolve={
		content=[[[d9ceba]◆Can desynthesis Orange Gear into materials to reuse
[d9ceba]◆Desynthesis process cannot be reverted, please be careful.]],
		key=[[forge_resolve]],
		pichelp={},
		title=[[Desynthesis Gear]],
	},
	forge_strength={
		content=[["[d9ceba]◆Enhance level limit is related to Pilot level
[d9ceba]◆Enhance increases the base attributes of Gear
[d9ceba]◆When changing Gear automatically inherits the Enhance level and Enhance attributes of the previous Gear
[d9ceba]◆“Quick Enhance” automatically distributes existing Enhance Materials, and prioritizes current low level Gear.”]],
		key=[[forge_strength]],
		pichelp={},
		title=[[Gear Enhance]],
	},
	haoyou={
		content=[[[d9ceba]◆Online reminders only remind the two sides of the players are [4edaad]wife and husband, ally, master and student[d9ceba] and friends with the relation of [4edaad]good friends[d9ceba]
◆The player set to reject messages from strangers and the rejection level of friends' applications cannot be greater than the player's own level
◆When auto-reply to friends' messages is opened, when you receive a message, it will automatically reply to the setting content]],
		key=[[haoyou]],
		pichelp={},
		title=[[Friend settings]],
	},
	hideplayer={
		content=[[[d9ceba]◆Hide other players except yourself and teammates.]],
		key=[[hideplayer]],
		pichelp={},
		title=[[Hide players]],
	},
	house_buff={
		content=[["[d9ceba]◆The total intimacy level of House is the total intimacy level of all House Valarions with .
[d9ceba]◆When the House total intimacy level reaches the required House attribute stage, the corresponding House attribute will be opened.
[d9ceba]◆The level of Valarion Intimacy can be increased through decoration, entertainment, gift giving..."]],
		key=[[house_buff]],
		pichelp={},
		title=[[Total Intimacy]],
	},
	huntpartnersoul={
		content=[[[d9ceba]◆It can cost Diamond to get an Jewel Atributes
[d9ceba]◆Jewel has a total of [4edaad]5[d9ceba] milestones, each time is completed, there is a certain ratio of activation the next milestone
[d9ceba]◆The higher the Jewel milestone is, the higher the Ratio of receiving a high quality Jewel Atributes is
[d9ceba]◆It can cost [4edaad]Diamond[d9ceba] to activate the 4th milestone directly, for every [4edaad]24[d9ceba]h it's free to activate]],
		key=[[huntpartnersoul]],
		pichelp={},
		title=[[Jewel ]],
	},
	mapbook={
		content=[["[8c8783]Classification of Archives:
[8c8783](I) Valarion Profile
[614c32]◆[d9ceba]Book Unlocking: Through ([4edaad]summoning[d9ceba], [4edaad]combination[d9ceba]...) after receiving a certain Valarion, this Valarion is automatically included in the profile. , Account books will also open.
[614c32]◆[d9ceba]Read account: [4edaad]Completing unlocking conditions[d9ceba] can read more Valarion account stories

[8c8783](II) Pilot Profile
[8c8783] The census of the Fantasia Land has been carried out.
[614c32]◆[d9ceba]Unlock the book: When [4edaad]Pilot to investigate initiates a challenge[d9ceba], the old file document will be updated.
[614c32]◆[d9ceba]Reading Account: [4edaad]The number of times challenging of the Pilot reach specified amount[d9ceba], can [4edaad]read the account story of the Pilot[d9ceba]
[614c32]◆[d9ceba]Notification: Due to the data need to be sorted, the Pilot profile's account store is temporarily closed.

[8c8783](III) Jewel Forbidden Area
[8c8783]Here stores the most comprehensive Jewel information.
[614c32]◆[d9ceba]Notification: Due to the data need to be sorted, the library of the Jewel Forbidden Are archives is temporarily closed.

[8c8783](IV) Origin of the World
[614c32]◆[d9ceba][4edaad]Achieve [d9ceba]collection Valarions can unlock the corresponding map event, and obtain [4edaad]reward[d9ceba]."]],
		key=[[mapbook]],
		pichelp={},
		title=[[Library Manage]],
	},
	monsteratkcity={
		content=[["[d9ceba]◆[4edaad]Wednesday [d9ceba], [4edaad]Saturday at 20:00~21:00 every week[d9ceba] open Fortress Attacking Monsters.
[d9ceba]◆After opening the event, monsters will appear at [4edaad]Soul Village[d9ceba] ([4edaad]4,6[d9ceba]), [4edaad]Bamboo Forest[d9ceba] ([4edaad]5, 5 [d9ceba]), [4edaad]Eight Gates Village[d9ceba] ([4edaad]26.9[d9ceba]).
[d9ceba]◆In [4edaad]settings[d9ceba] can choose [4edaad]block other players[d9ceba].
[d9ceba]◆After opening the event, on the map based on the number of waves, showing different types of monsters.
[d9ceba]◆Event has a total of [4edaad]10 waves[d9ceba] of monsters, the end of the time to receive the number of waves completed is the end of the event.
[d9ceba]◆The ultimate target of all monsters is [4edaad]Capital Teleport Circle[d9ceba], if the monsters come to the Capital Teleport Circle, DEF points will decrease 
[d9ceba]◆Pioneer Monsters and Elite Monsters prioritize clickable players who can challenge.
[d9ceba]◆Every time attacking the monsters will receive [4edaad]reward[d9ceba], when the event ends, based on Capital Defense points, [4edaad]reward summaries[d9ceba] mailed to all participating players join the event."]],
		key=[[monsteratkcity]],
		pichelp={},
		title=[[Fortress Attacking Monster]],
	},
	org_build={
		content=[["[d9ceba]◆Each day you can choose 1 of 2 types to build
◆Building claims [4edaad]Alliance EXP, Alliance Capital, Alliance Contribution[d9ceba] and [4edaad]signing progress[d9ceba]
◆Different building, different amount of money and consumption, please choose the corresponding building method according to your needs
◆0h every day reset the signing progress, claim the building reward before the reset
◆When the signing progress reaches a certain number, every Alliance member clicks to claim the reward"]],
		key=[[org_build]],
		pichelp={},
		title=[[Donation]],
	},
	org_info={
		content=[["[d9ceba]◆The Alliance EXP can be leveled up, after leveling up the number of Alliance people and the number of managers increase
◆Alliance EXP can be received through [4edaad]building every day, killing Alliance Boss, Alliance War[d9ceba]…
◆Alliance Capital can be received through [4edaad]building every day, killing Alliance Boss,, Alliance War[d9ceba]…
◆Alliance Fame can be received through [4edaad]killing Alliance Boss,, Alliance Battle, Alliance War[d9ceba]…
◆Alliance Rank is based on [4edaad]Reputation[d9ceba] of the Alliance to rank
◆Alliance Excitement is calculated from [4edaad]Daily Excitement[d9ceba] of the members, if this week's Excitement does not meet the requirements, Alliance Level will decrease 1 level next Monday
◆If the Alliance drops to level 1, the Alliance will disband
◆During the Alliance creation cycle, Excitement does not meet the requirements will not level down
◆After leaving the Alliance, the individual Alliance Contribution will not change, the accumulated Alliance Contribution will be removeed.
◆When the Alliance Captain is offline for 7 consecutive days, he will automatically resign
◆Just joined the Alliance is the position of [4edaad]newbie[d9ceba], when Contribution reaches [4edaad]360[d9ceba] points, increase to regular members, in [4edaad]2[d9ceba] days of joining the Alliance, Contribution does not meet the membership requirements, you will be automatically invited out of the Alliance
◆Alliance Captain and Vice Captain Captain are invited to a certain number of members each day, members who invite out for 2 consecutive days without being online will not record the number of times. Members invited out every day will increase with Alliance level
◆The Alliance can only send 3 mail notifications every day, reset the number of times at 0h each day
◆After leaving the Alliance, in [4edaad]12h[d9ceba] cannot enter the Alliance (First time leaving will have no cooldown)"]],
		key=[[org_info]],
		pichelp={},
		title=[[Alliance Information]],
	},
	org_tuitu={
		content=[["[d9ceba]◆The daily Alliance Boss can be challenged [4edaad]2[d9ceba] times, reset the number of times every day at 0:00
◆After the challenge is over, receive the Alliance Contribution reward
◆Killing the current Alliance Boss will unlock the next Alliance Boss
◆You get a reward for every kill 1 Alliance Boss
◆Every Monday [4edaad]free reset[d9ceba] Alliance Boss once, Alliance Captain and Vice Captain spend Alliance Capital to reset progress
◆Valarion of the day that entered the Reward battle can no longer go to war
◆Members who have not participated in this Alliance Boss battle will not receive the reward after killing the Alliance Boss.]],
		key=[[org_tuitu]],
		pichelp={},
		title=[[Alliance Boss]],
	},
	org_war={
		content=[[[d9ceba]◆During Alliance Wars, actively launching battle, Diamond ATK/DEF is all require a team of 2 or more.
[d9ceba]◆The Defense Glass Color of each battling Alliance is randomly divided.
[d9ceba]◆Players in the battlefield and the preparation hall can only invite players from the same battlefield and the same preparation hall, cannot invite players from other maps to join the team.
[d9ceba]◆After starting the Alliance War, arrange the members properly, 1 team to attack enemy Glass, 1 Glass defence team of our side
[d9ceba]◆When Glass is attacked, 1 Energy will be deducted every second. Multiple teams attacking at the same time will not increase speed of decreasing Energy
[d9ceba]◆When defending Glass, will automatically fight against the enemy team that attacks Glass. During the battle time, Glass will not lose Energy.
[d9ceba]◆After a failed battle, you will be kicked out of the battlefield. After 3 minutes, you can re-enter the battlefield.
[d9ceba]◆Fleeing in battle will be considered a failure and will be kicked out of the battlefield, and kicked out of the team.
[d9ceba]◆Every winning battle will get rich rewards, losing will get a small amount of rewards.
[d9ceba]◆The last winning Alliance gets rich rewards, and players who join the battle will get extra rewards.]],
		key=[[org_war]],
		pichelp={},
		title=[[Alliance War]],
	},
	org_wish={
		content=[["[d9ceba]◆Every day, each type of Blessing can only bless [4edaad]1[d9ceba] times, reset every day at 0h
◆Depending on the different players' Valarion grades, the number of wished Shards are also different
◆Level 50 player opens the Wish for Gear materials, according to the type of Gear materials the player has wished for, the number of materials that have been wished is also different
◆When the wish request is fulfilled by another player, need to re-enter the interface or at the Wish interface to receive the reward
◆Players who give other players a Summon Shard or Gear material can receive the [4edaad]Alliance Contribution[d9ceba]"]],
		key=[[org_wish]],
		pichelp={},
		title=[[Alliance Wish]],
	},
	orgmemberbuff={
		content=[["[8c8783]Increase attributes
[d9ceba]◆Depending on other positions of players in the Alliance, the player's attribute increasing is also different.
[d9ceba]◆Vice Captain: [4edaad]Attack +5 % HP +5%
[d9ceba]◆Elite: [4edaad]Attack +2 % HP +2%
[8c8783]Number of positions
[d9ceba]◆Along with the growth of the Alliance level, the number of members that can be contained and the number of positions also varies.
[d9ceba]◆Level 1 Alliance: [4edaad]Total Alliance members [4edaad]30[d9ceba] people, Alliance Captain [4edaad]1[d9ceba] people, Vice Captain [4edaad]1[d9ceba] people, Elite [4edaad]6[d9ceba] people
[d9ceba]◆Level 2 Alliance: [4edaad]Total Alliance members [4edaad]40[d9ceba] people, Alliance Captain [4edaad]1[d9ceba] people, Vice Captain [4edaad]1[d9ceba] people, Elite[4edaad]8[d9ceba] people
[d9ceba]◆Level 3 Alliance: [4edaad]Total Alliance members [4edaad]50[d9ceba] people, Alliance Captain [4edaad]1[d9ceba] people, Vice Captain [4edaad]1[d9ceba] people, Elite[4edaad]10[d9ceba] people
[d9ceba]◆Alliance level 4: [4edaad]Total Alliance members [4edaad]60[d9ceba] people, Alliance Captain [4edaad]1[d9ceba] people, Vice Captain [4edaad]2[d9ceba] people, Elite[4edaad]12[d9ceba] people
[d9ceba]◆Level 5 Alliance: [4edaad]Total Alliance members [4edaad]70[d9ceba] people, Alliance Captain [4edaad]1[d9ceba] people, Vice Captain [4edaad]2[d9ceba] people, Elite[4edaad]14[d9ceba] people"]],
		key=[[orgmemberbuff]],
		pichelp={},
		title=[[Member Information]],
	},
	parequip_stone={
		content=[[[d9ceba]◆Consume corresponding [4edaad]Gem[d9ceba] can increase rune's Gem attribute, after enhancement, Gem will disappear
[d9ceba]◆Rune with different parts can only use Gem of the corresponding type to enhance
[d9ceba]◆Gem with different levels, depending on increasing [4edaad]Rune Level[d9ceba] to [4edaad]unlock[d9ceba]
[d9ceba]◆Gem of different levels have different numbers of enhancements, the higher the level is, the more the numbers are
[d9ceba]◆[4edaad]The color[d9ceba] of Rune will change according to [4edaad]progress[d9ceba] of enhancing Gem]],
		key=[[parequip_stone]],
		pichelp={},
		title=[[Enhance the Gem]],
	},
	parequip_upgrade={
		content=[[[d9ceba]◆Cost [4edaad]Varian Totem[d9ceba] can increase the level of Rune, after leveling up can increase the attribute of Rune
[d9ceba]◆The maximum level of the Rune is [4edaad]10[d9ceba], after reaching the maximum level, you must increase [4edaad]stars[d9ceba] to continue to level up the enhancement
[d9ceba]◆The higher the star level and rune level is, the more Varian Totems needed for each level up are]],
		key=[[parequip_upgrade]],
		pichelp={},
		title=[[Level up Rune]],
	},
	parequip_upstar={
		content=[[[d9ceba]◆Cost corresponding[4edaad]Cloud Stone[d9ceba] can increase Rune star level
[d9ceba]◆After increasing the rune star can [4edaad]increase the rune attribute[d9ceba],[4edaad]reset the rune enhancement level[d9ceba] and [4edaad]unlock the Gem Enhancement[d9ceba ]
[d9ceba]◆Rune needs to reach level [4edaad]10[d9ceba] to increase stars
[d9ceba]◆The maximum star level of Rune is [4edaad]6[d9ceba] stars, reaching the maximum star level will not be able to continue to increase stars]],
		key=[[parequip_upstar]],
		pichelp={},
		title=[[Star up Rune]],
	},
	parsoul_upgrade={
		content=[["[d9ceba]◆ Level up [4edaad]Main Jewel[d9ceba] by using more 2 Main Jewels of the same rank and quality to level up
[d9ceba]◆ Level up [4edaad]Jewel Atributes[d9ceba] by absorbing other Jewel Atributes to get exp level
[d9ceba]◆ The maximum level of the Main Jewel is level [4edaad]5[d9ceba], the maximum level of the Jewel Atributes is level [4edaad]15[d9ceba], it cannot be enhanced after reaching full level
[d9ceba]◆ Cost Diamond to level up Jewel"]],
		key=[[parsoul_upgrade]],
		pichelp={},
		title=[[Level up Jewel ]],
	},
	parstone_compose={
		content=[[[d9ceba]◆Gem of the same grade can level up Gem through combination
[d9ceba]◆Gem combination rate is [4edaad]3 with 1[d9ceba], combination rate is[4edaad]100%[d9ceba]
[d9ceba]◆Gem's maximum level is [4edaad]7[d9ceba], Gem that have reached the maximum level cannot continue to be combined]],
		key=[[parstone_compose]],
		pichelp={},
		title=[[Combine Gem]],
	},
	partner_awake={
		content=[["[d9ceba]◆After awakening the Valarion, can greatly increase the attributes of the Valarion and unlock powerful Skills
[d9ceba]◆Each Valarion only conducts [4edaad]1[d9ceba] time of awakening
[d9ceba]◆Different Valarions cost different Awakening Materials and Gold."]],
		key=[[partner_awake]],
		pichelp={},
		title=[[Awaken]],
	},
	partner_equip={
		content=[["[8c8783]Basic Rune
[d9ceba]◆Each Valarion can carry [4edaad]4[d9ceba] Runes, Rune with different parts has different providing attributes
[d9ceba]◆Rune through leveling up, increasing the star to increase the main attribute, can also absorb the corresponding Gem to improve the Gem attribute
[d9ceba]◆Rune through [4edaad]Purchasing gold [d9ceba], Purchasing Rune by default is 1 star level 1
[d9ceba]◆If you sell the Rune [4edaad] that has already been enhanced[d9ceba], after selling the expensive enhancement materials will not be returned
[8c8783]Level up Rune
[d9ceba]◆Cost [4edaad]Varian Totem[d9ceba] can increase the level of Rune, after leveling up can increase the attribute of Rune
[d9ceba]◆The maximum level of the Rune is [4edaad]10[d9ceba], after reaching the maximum level, you must conduct [4edaad]star increasing[d9ceba] before you can continue to level up the enhancement
[d9ceba]◆The higher the rune and star level is, the more Varian Totems is needed each time you level up are.
[8c8783] Star up Rune
[d9ceba]◆Cost corresponding [4edaad]Cloud Stone[d9ceba] can increase the level of Rune star
[d9ceba]◆After increasing the Rune star [4edaad], increase the Rune attribute[d9ceba],[4edaad]reset the enchantment level of the Rune[d9ceba] and [4edaad]unlock the advanced enchantment of the Gem[d9ceba ]
[d9ceba] The rune must reach level [4edaad]10[d9ceba] to proceed to star up
[d9ceba]◆Maximum level of Rune [4edaad]6[d9ceba] stars, after reaching the maximum star level cannot continue to star up
[8c8783]Enhance Gem
[d9ceba]◆Cost corresponding[4edaad]Gem[d9ceba] can increase the Gem attribute of Rune, after enhancing, Gem disappears
[d9ceba]◆Different Runes can only be enhanced using Gem of the corresponding type
[d9ceba]◆Variety levels of Gem along with [4edaad]Rune Star level[d9ceba] will gradually increase [4edaad]unlocking[d9ceba]
[d9ceba]◆Gem in different levels, numbers of Gem can be enhanced are different, the higher the level is, the more numbers of Gem can be enhanced are
[d9ceba]◆[4edaad]The color[d9ceba] of Rune will change according to [4edaad]progress[d9ceba] of enhancement of Gem"]],
		key=[[partner_equip]],
		pichelp={},
		title=[[Valarion Rune]],
	},
	partner_hire={
		content=[[[d9ceba]◆Cost 1 Spirit Contract to summon a Valarion.]],
		key=[[partner_hire]],
		pichelp={},
		title=[[Training]],
	},
	partner_rank={
		content=[[[d9ceba]◆Valarion Ranking refreshing cycle is 1h, after refreshing will rank according to Valarion combat power.]],
		key=[[partner_rank]],
		pichelp={},
		title=[[Valarion Ranking]],
	},
	partner_soul={
		content=[["[d9ceba]◆Jewel is divided into 3 parts, respectively [4edaad]Main Jewel[d9ceba], [4edaad]Jewel Shard[d9ceba] and [4edaad]Jewel Atributes[d9ceba]
[d9ceba]◆Jewel Main provide Main effect, each Valarion can use 1 random type of Main Jewel 
[d9ceba]◆Collect 100 Jewel Shards that can unlock Main Jewel
[d9ceba]◆Jewel Atributes provides an attribute effect, only carrying the corresponding Main Jewel can carry the Jewel Atributes
[d9ceba]◆The number of Jewel Atributes will open gradually as the Pilot's level increases"]],
		key=[[partner_soul]],
		pichelp={
			[1]=[[pic_rukou_yuanhua_7; Click more Main Jewel]],
			[2]=[[pic_rukou_yuanhua_8; Select the corresponding Main Jewel and then click confirm]],
			[3]=[[pic_rukou_yuanhua_9; Quickly select the Gear to wear!]],
			[4]=[[pic_rukou_yuanhua_10; If there is no Jewel Atributes click here to receive!]],
		},
		title=[[Valarion Jewel]],
	},
	partner_upgrade={
		content=[["[d9ceba]◆Taking [4edaad]Velator Card[d9ceba] can increase Valarion level, after leveling up can increase Valarion attributes
[d9ceba]◆Can click [4edaad]1 Level up[d9ceba] or [4edaad]5 Levels up[d9ceba] to quickly level up Valarion."]],
		key=[[partner_upgrade]],
		pichelp={},
		title=[[Level up]],
	},
	partner_upskill={
		content=[["[d9ceba]◆Cost [4edaad]Unit Core[d9ceba] to level up Valarion skill
[d9ceba]◆Randomly select 1 unlocked and not-yet full-leveled skill each time to increase [4edaad]1[d9ceba] level
[d9ceba]◆The number of Unit Cores that increase the skill level varies with different Valarions."]],
		key=[[partner_upskill]],
		pichelp={},
		title=[[Skill up]],
	},
	partner_upstar={
		content=[["[d9ceba]◆Cost [4edaad]any 2 Valarions with the same star level as upgrading Valarion[d9ceba] to upgrade the Valarion star, increasing the Valarion attributes tremendously
[d9ceba]◆Maximum star level of Valarion is [4edaad]5[d9ceba] stars."]],
		key=[[partner_upstar]],
		pichelp={},
		title=[[Star up]],
	},
	pata_main={
		content=[["[8c8783]Battle Rules
[614c32]◆[d9ceba]Training Tower has a total of 100 floors, successfully defeating monsters to be able to go to the next floor
[614c32]◆[d9ceba]Escape or being defeated is failure
[614c32]◆[d9ceba]Valarion HP bar does not reset, Pilot HP will reset
[614c32]◆[d9ceba]Previous floor Valarion die, next floor cannot be used
[8c8783]Other Rules
[614c32]◆[d9ceba]Every day can [4edaad]reset once[d9ceba]. After reset, HP bar of all Valarions reset, dead Valarions can also revive
[614c32]◆[d9ceba]After reseting can use sweep to directly receive the reward.
[614c32]◆[d9ceba]Every day can [4edaad]invite friend Valarion 3 times[d9ceba] to join the battle, at [4edaad]0h the next day will reset[d9ceba].
[8c8783]Reward Rules
[614c32]◆[d9ceba]For every successful floor passing, you can receive a reward for finish the floor
[614c32]◆[d9ceba]First clear for each level can get [4edaad]First clear reward[d9ceba], click the chest in the upper left corner to view.
[614c32]◆[d9ceba]Ranking is based on the number of floor of the Training Tower."]],
		key=[[pata_main]],
		pichelp={},
		title=[[Training Tower]],
	},
	pefuben_droplist={
		content=[["[d9ceba]◆From Monday to Friday, you can see the corresponding Jewel Attributes that Jewel Raid will create today.
[d9ceba]◆Saturday, Sunday chooses 1 falling way of Jewel Atributes from Monday to Friday. When not selected, the system will randomly give 1 falling way.
[d9ceba]◆Click on the icon of Jewel Atributes to see the effect."]],
		key=[[pefuben_droplist]],
		pichelp={},
		title=[[Jewel Raid]],
	},
	pefuben_info={
		content=[["[d9ceba]◆Jewel Raid creates a lot of [4edaad]Jewel Valarions[d9ceba], which is an important trick to personalize the fighting method of Valarions.
[d9ceba]◆Jewel Raid has [4edaad]10 classes[d9ceba], after passing 1 class, unlock the next floor. The higher the dungeon level is, the higher Jewel Atributes star level creates.
[d9ceba]◆It takes [4edaad]12 Energy[d9ceba] every time you pass Jewel Raid, cost [4edaad]3 Energy[d9ceba] when can't pass. Before entering the dungeon, spin the wheel, after fighting through the next dungeon, you will receive 1 Gear set and parts that the clock hands indicate.
[d9ceba]◆Before each time attacking the dungeon, free spins, continuing to spins can cost Gold, Gold needed and the number of classes to pass are related.
[d9ceba]◆Jewel Raid, after passing the chap, based on the number of units alive and the number of times used to evaluate the star level, after 3 stars open this class sweep feature, each sweep costs 2 Skip Tickets, Free Sweeping Lifetime Card privileges.
[d9ceba]◆From Monday to Friday, the fixed dungeon creates 6 sets of Jewel Atributes, can also use Diamond to choose another option, after choosing the valid option for the day, Diamond needed and the number of classes to pass are currently related; Saturday, Sunday is free to choose 1 set of falling way of Monday to Friday. Click the "Change droping team" button to find out more information."]],
		key=[[pefuben_info]],
		pichelp={},
		title=[[Jewel Raid]],
	},
	qqvip={
		content=[[[d9ceba]◆Newbie Gift: [4edaad]388 #w2[d9ceba], [4edaad]60 #tl[d9ceba], [4edaad] 5 hundred thousands #w1[d9ceba].
[d9ceba]◆Member Gift: [4edaad]888 #w2[d9ceba], [4edaad]120 #tl[d9ceba], [4edaad]15 hundred thousands #w1[d9ceba].
[d9ceba]◆Opening/ Member Renewal Gift: 


      [4edaad]Soul Keys X5[d9ceba], [4edaad]240#tl[d9ceba], [4edaad] 30 hundred thousands#w1[d9ceba].
[d9ceba]◆QQ 1 Member Gift: 

      [4edaad]Title: Outstanding[d9ceba], [4edaad]688 #w2[d9ceba], [4edaad]5 hundred thousands #w1[d9ceba].
[d9ceba]◆QQ 2 Member Gift: 

      [4edaad]Instant Soul Entering Shard X5[d9ceba], [4edaad]Versatile Shard X2[d9ceba], [4edaad]10 hundred thousands #w1[d9ceba].]],
		key=[[qqvip]],
		pichelp={},
		title=[[Tencent Welfare Gift]],
	},
	question_answer={
		content=[["[8c8783]Participation Conditions
[614c32]◆[d9ceba]Player Pilot reaches [4edaad]Lv36[d9ceba]
[614c32]◆[d9ceba]Every Monday, Wednesday, Friday, Sunday at [4edaad]12:00-12:10[d9ceba] is the time to prepare [4edaad]resignation[d9ceba][4edaad][ d9ceba];
[614c32]◆[d9ceba]Only during the preparation time, [4edaad]click[d9ceba]Cute Cat chat box under the screen and successful registered players can officially join the event
[614c32]◆[d9ceba]12:11 The event officially starts
[614c32]◆[d9ceba]Total 20 questions
[8c8783]Bonus gold
[614c32]◆[4edaad]Correct answer[d9ceba] get Basic Gold,
[614c32]◆[4edaad]First Answer[d9ceba] gets 200% more Gold
[614c32]◆[d9ceba]For every [4edaad]continuously correct answer [d9ceba]1 question, Gold bonus increases by 2.5%, is stacked
[614c32]◆[d9ceba]Wrong answers can be answered again, but the number of consecutive correct answers is erased
[8c8783]Bonus Points
[614c32]◆[d9ceba]For every correct answer, get 100 points
[614c32]◆[d9ceba]If you answer in the first 5 seconds and answer correctly, you will get an extra 50 points
[614c32]◆[d9ceba]Top 10 people who answered correctly, in order, get more 100-10 points
[614c32]◆[d9ceba]Finally, based on the high and low Point Ranking, receive different rewards"]],
		key=[[question_answer]],
		pichelp={},
		title=[[Quiz]],
	},
	rewardback={
		content=[[[d9ceba]◆The rewards of some unfinished events from yesterday can be recovered.
[d9ceba]◆Refind for free, get half the reward; Use Diamond to find it again, get the full reward.]],
		key=[[rewardback]],
		pichelp={},
		title=[[Reward recovery]],
	},
	richenghuoyue={
		content=[["[8c8783]Dungeon
[d9ceba]◆Can go alone or in a team, this is an important way to develop combat power.
[8c8783]Challenge
[d9ceba]◆Break through yourself, challenge for higher achievements, a group of powerful warriors are watching you.
[8c8783]Idle
[d9ceba]◆Easy to get EXP, happy to receive items.
[8c8783]Time Limit Event
[d9ceba]◆The even is only opened for a limited time, seize the opportunity to win the world.
[8c8783]Excitement Quest
[d9ceba]◆Randomly refresh 13 types of quests every day, complete quests to get [4edaad]EXP[d9ceba] and [4edaad]beautiful rewards[d9ceba].
[8c8783]Change Excitement
[d9ceba]◆The Excitement you receive every day will accumulate in the Excitement Shop tab, after accumulating, exchange [4edaad]rare props[d9ceba]."]],
		key=[[richenghuoyue]],
		pichelp={},
		title=[[Excitement]],
	},
	richengxitong={
		content=[["[8c8783]Dungeon
[d9ceba]◆Can go alone or in a team, this is an important way to develop combat power.
[8c8783]Challenge
[d9ceba]◆Break through yourself, challenge for higher achievements, a group of powerful warriors are watching you.
[8c8783]Idle
[d9ceba]◆Easy to get EXP, happy to receive items.
[8c8783]Time Limit Event
[d9ceba]◆The even is only opened for a limited time, seize the opportunity to win the world.
[8c8783]Excitement Quest
[d9ceba]◆Randomly refresh 13 types of quests every day, complete quests to get [4edaad]EXP[d9ceba] and [4edaad]beautiful rewards[d9ceba].
[8c8783]Change Excitement
[d9ceba]◆The Excitement you receive every day will accumulate in the Excitement Shop tab, after accumulating, exchange [4edaad]rare props[d9ceba]."]],
		key=[[richengxitong]],
		pichelp={},
		title=[[Adventure]],
	},
	scene_exam={
		content=[["[8c8783]Participation Conditions
[614c32]◆[d9ceba]Player Pilot reaches [4edaad]Lv36[d9ceba]
[614c32]◆[d9ceba]Every Tuesday, Thursday, Saturday at [4edaad]12:00-12:10[d9ceba] is the time to prepare [4edaad]registration[d9ceba][4edaad][d9ceba] ;
[614c32]◆[d9ceba]Only during the preparation time, [4edaad]click[d9ceba]Cute Cat chat box under the screen and successful registered players can officially join the event
[614c32]◆[d9ceba]After registering, [4edaad] quickly entered the battle[d9ceba], the Event is proceeded in the designated stage
[614c32]◆[d9ceba]12:11 The event officially starts
[8c8783]Introduction
[614c32]◆[d9ceba]Total 20 questions
[614c32]◆[d9ceba]Event starts, [4edaad]click question select[d9ceba] go to fixed judgment area
[614c32]◆[d9ceba]Live[4edaad] click on the ground[d9ceba] go to the selected location
[614c32]◆[4edaad]Count down each question to the end, based on the area where you stood to judge right or wrong
[614c32]◆[4edaad]Neutral zone players will be judged as “Wrong Answer”
[8c8783]Bonus Gold
[614c32]◆[4edaad]Correct answer[d9ceba] get Basic Gold
[614c32]◆[d9ceba]For every consecutive[4edaad]answer [d9ceba] 1 question, Gold Bonus increases by 2.5%, is stacked
[8c8783]Bonus Points
[614c32]◆[d9ceba]For every correct answer, get 100 points
[614c32]◆[d9ceba]For every [4edaad]continuously correct answer[d9ceba] 1 question, points increase by 5%, no stack limit
[614c32]◆[d9ceba]Finally, based on the high and low Point Ranking, receive different rewards"]],
		key=[[scene_exam]],
		pichelp={},
		title=[[Special Quiz]],
	},
	shijieboss={
		content=[["[d9ceba]◆[4edaad]20:00[d9ceba] everyday, BOSS will appear in the Labyrinth
[d9ceba]◆All server enjoy the BOSS serum
[d9ceba]◆BOSS will leave the best reward to the person who causes the biggest Damage with it, also the Valarion who deal the final blow to BOSS will have a lucky bonus
[d9ceba]◆If before [4edaad]21:00[d9ceba] defeating the BOSS, all players participating in the defeating get an extra defeating bonus
[d9ceba]◆After the BOSS is defeated a certain number of times, the next time you come back, you will become stronger, be prepared!"]],
		key=[[shijieboss]],
		pichelp={},
		title=[[Labyrinth]],
	},
	teaart={
		content=[["[8c8783]Chef Attribute
[614c32]◆[d9ceba]The player who cooks will get cooking EXP for each time.
[614c32]◆[d9ceba]After the cooking EXP is full, it is possible to level up the chef.
[8c8783]Open the Kitchen Table
[614c32]◆[d9ceba]Opening a new Kitchen Table based on the number of House Valarions.
[614c32]◆[d9ceba]Initially opens 1 Kitchen Table by default.
[614c32]◆[d9ceba]The number of House Valarions is 4, then open the 2nd one, the total House intimacy  reaches level 80, open the 3rd one.
[8c8783]Opening gifts
[614c32]◆[d9ceba]Based on the player's chelf level can unlock new cooking.
[614c32]◆[d9ceba]Each time cooking, the player randomly receives cooking in the unlocked cooking.
[614c32]◆[d9ceba]After cooking, the gift is directly put into the Valarion gifting interface.
[614c32]◆[d9ceba]Depending on different gifts, the intimacy received will also be different.
[8c8783] Cooking with friends
[614c32]◆[d9ceba]Friends can cook at your Kitchen Table.
[614c32]◆[d9ceba]Can help your friends speed up cooking.
[614c32]◆[d9ceba]After friends have just cooked, friends receive cooking, players can receive rewards.
[614c32]◆[d9ceba]Rewards will be sent to the mail."]],
		key=[[teaart]],
		pichelp={},
		title=[[[fefbc8]Kitchen Table Notification]],
	},
	team_arena={
		content=[["[d9ceba]◆22:00~23:00 every Saturday, Sunday open United Battle, [4edaad]10 minutes before [d9ceba] the event is opened can [4edaad] get into the map early[d9ceba]
[d9ceba]◆It is possible for 2 people in team to enter the map early, also to find a suitable teammate in the map
[d9ceba]◆If the two team members combine successfully, the two of them will fight together as comrades. If [4edaad]single combination[d9ceba], randomly combine 1 teammate
[d9ceba]◆In the battle of the United Battle, each person in the same battle can go to battle at most [4edaad]2[d9ceba] Valarions, please arrange it properly
[d9ceba]◆After each battle ends, certain rewards can be obtained, the winning side can also claim the [4edaad]United Battle Points[d9ceba], the losing side cannot get the Points bonus
[d9ceba]◆When the cumulative number of defeats reaches [4edaad]10[d9ceba] matches, it will automatically be invited to leave the map and cannot participate in the United Battle today
[d9ceba]◆During the event time, players in the United Battle completed [4edaad]total wins[d9ceba] reaching [4edaad]1/3/5/10[d9ceba] and [4edaad]number of winning streaks[d9ceba] reach [4edaad]3/5/10[d9ceba]..., all can get [4edaad]extra bonus[d9ceba]
[d9ceba]◆After the event ends, can receive rewards corresponding to the rank of this event, when [4edaad]Points are the same[d9ceba], get the highest [4edaad]Point Rank[4edaad]."]],
		key=[[team_arena]],
		pichelp={},
		title=[[United Battle]],
	},
	teampvp_rank={
		content=[["[d9ceba]◆22:00~23:00 every Saturday, Sunday open United Battle, [4edaad]10 minutes before [d9ceba] the event is opened can [4edaad] get into the map early[d9ceba]
[d9ceba]◆It is possible for 2 people in team to enter the map early, also to find a suitable teammate in the map
[d9ceba]◆If the two team members combine successfully, the two of them will fight together as comrades. If [4edaad]single combination[d9ceba], randomly combine 1 teammate
[d9ceba]◆In the battle of the United Battle, each person in the same battle can go to battle at most [4edaad]2[d9ceba] Valarions, please arrange it properly
[d9ceba]◆After each battle ends, certain rewards can be obtained, the winning side can also claim the [4edaad]United Battle Points[d9ceba], the losing side cannot get the Points bonus
[d9ceba]◆When the cumulative number of defeats reaches [4edaad]10[d9ceba] matches, it will automatically be invited to leave the map and cannot participate in the United Battle today
[d9ceba]◆During the event time, players in the United Battle completed [4edaad]total wins[d9ceba] reaching [4edaad]1/3/5/10[d9ceba] and [4edaad]number of winning streaks[d9ceba] reach [4edaad]3/5/10[d9ceba]..., all can get [4edaad]extra bonus[d9ceba]
[d9ceba]◆After the event ends, can receive rewards corresponding to the rank of this event, when [4edaad]Points are the same[d9ceba], get the highest [4edaad]Point Rank[4edaad]."]],
		key=[[teampvp_rank]],
		pichelp={},
		title=[[United Battle Ranking]],
	},
	tequanka={
		content=[[[8c8783] Privilege details:
◆The display of privileges in many places, showing the noble status
◆Each day provides 25 more searching points
◆Buy items at Shop 15% off
◆Each month get 3 additional signatures
◆Increase the amount of God Blessing
◆Get 1 Dig Map Every Week
◆Get 100 Diamonds Every Day
◆PB Dungeon gets 1 chance to withdraw every day]],
		key=[[tequanka]],
		pichelp={},
		title=[[Lifetime VIP Card]],
	},
	terrawar_map={
		content=[["""[8c8783]Opening time:
[614c32]◆[4edaad]Open the server on the 3rd day [d9ceba]opening, the event lasts 3 days. After the event ends, 4 days later open the next round.
[8c8783]Battle conditions:
[614c32]◆[d9ceba]After the gameplay is open, the map shows the base, the initial base is in a lordless status.
[614c32]◆[d9ceba]Owns Medal - Player level [4edaad]>=level 15[d9ceba], and the player has a Alliance.
[614c32]◆[d9ceba]Players with [4edaad]3 or more Valarions >=level 20[d9ceba] can proceed to attack the base.
[614c32]◆[d9ceba]The same Valarion cannot hold two bases at the same time.
[614c32]◆[d9ceba]Each time to capture, attack or support takes [4edaad]1 Spiritual Energy[d9ceba].
[614c32]◆[d9ceba]Spirit limit is [4edaad]10 Spirit[d9ceba], starting gameplay the default system provides 10 Spirit, restores 1 Spirit every hour, [4edaad]can be purchased Spirit[d9ceba]. 
[8c8783]Attack base & defense:
[614c32]◆[d9ceba]Moving and clicking a non-flank base on the map can complete the attack.
[614c32]◆[d9ceba]The offensive side defeats the defenders' Valarions in the base then can be able to capture the base.
[614c32]◆[d9ceba]Before the battle has [4edaad]30s[d9ceba] to prepare, during the preparation time the defender can choose to enter this battle.
[8c8783]IBattle Introduction:
[614c32]◆[d9ceba]The battle team of the two sides ranked according to the time it took to attack or support the base achieved.
[614c32]◆[d9ceba]Battle in solo or team as ranked order.
[614c32]◆[d9ceba]In the battle of newly joined squads, order into battle team by attack or reinforcement time.
[614c32]◆[d9ceba]Line up the maximum reinforcement team with 6 teams, the maximum attack side with 5 teams.
[614c32]◆[d9ceba]Pilot that can support himself, for example, the Pilot does not support, the reinforcement side and the attack side are the same, 5 teams.
[614c32]◆[d9ceba]After 1 team is defeated, on the 30s countdown, the winning side continues to fight, the losing side goes to the battle with the next team.
[614c32]◆[d9ceba]Winner saves remaining Energy in the next battle, the dead Valarions cannot proceed to battle.
[614c32]◆[d9ceba]The right to own the base is decided when all the fighting teams of one faction fail, the winning side has the right to capture the base.
[614c32]◆[d9ceba]After the base has a battle, clicking the base will display the "watch battle" button, you can enter to watch the battle.
[614c32]◆[d9ceba]After capturing the base, there will be a protection period of [4edaad]15 minutes[d9ceba].
[614c32]◆[d9ceba]After the battle ends, the occupier will save the remaining Energy, performing recovery based on the division of unnit.
[8c8783] Base benefits:
[614c32]◆[d9ceba]Different large and small bases, different benefits received.
[614c32]◆[d9ceba]Base produces [4edaad]Alliance Points[d9ceba] every minute, [4edaad]Personal Points[d9ceba] and [4edaad]Contribution[d9ceba].
[614c32]◆[d9ceba]The longer the capture time is, the lower the base benefit is.
[614c32]◆[d9ceba]Each time you capture, attack, or support, you will receive Alliance Points, Personal Points, and Contribution.
[8c8783] Increased area CRIT:
[614c32]◆[d9ceba]Capturing the base of 3 areas connected in a triangle, can activate [4edaad]effectively increase the area's CRIT [d9ceba]..
[614c32]◆[d9ceba]After Activation the effect of increasing area CRIT, [4edaad]base benefits around the area will increase[d9ceba].
[8c8783] Rankings:
[614c32]◆[d9ceba]After the event ends, based on the total Alliance Point rank to give [4edaad]Alliance Reward[d9ceba].
[614c32]◆[d9ceba]After the event ends, based on the rank of Personal Points to give [4edaad]Internal Alliance rewards[d9ceba]."""]],
		key=[[terrawar_map]],
		pichelp={
			[1]=[[pic_rukou_yuanhua_13; The original base is empty]],
			[2]=[[pic_rukou_yuanhua_14; Click on an empty base for details and directions]],
			[3]=[[pic_rukou_yuanhua_17; Click on an empty base to capture it]],
			[4]=[[pic_rukou_yuanhua_18; Only 3 Valarions of level 20 or higher can be captured]],
			[5]=[[pic_rukou_yuanhua_15; Click to attack captured Base]],
			[6]=[[pic_rukou_yuanhua_16; Let's fight with the strongest Valarion!]],
			[7]=[[pic_rukou_yuanhua_19; Occupying the triangle area will increase Contribution and Accumulation!]],
		},
		title=[[[fefbc8]Alliance Battle Rules]],
	},
	terrawar_mine={
		content=[["""[8c8783]Opening time:
[614c32]◆[4edaad]Open the server on the 3rd day [d9ceba]opening, the event lasts 3 days. After the event ends, 4 days later open the next round.
[8c8783]Battle conditions:
[614c32]◆[d9ceba]After the gameplay is open, the map shows the base, the initial base is in a lordless status.
[614c32]◆[d9ceba]Owns Medal - Player level [4edaad]>=level 15[d9ceba], and the player has a Alliance.
[614c32]◆[d9ceba]Players with [4edaad]3 or more Valarions >=level 20[d9ceba] can proceed to attack the base.
[614c32]◆[d9ceba]The same Valarion cannot hold two bases at the same time.
[614c32]◆[d9ceba]Each time to capture, attack or support takes [4edaad]1 Spiritual Energy[d9ceba].
[614c32]◆[d9ceba]Spirit limit is [4edaad]10 Spirit[d9ceba], starting gameplay the default system provides 10 Spirit, restores 1 Spirit every hour, [4edaad]can be purchased Spirit[d9ceba]. 
[8c8783]Attack base & defense:
[614c32]◆[d9ceba]Moving and clicking a non-flank base on the map can complete the attack.
[614c32]◆[d9ceba]The offensive side defeats the defenders' Valarions in the base then can be able to capture the base.
[614c32]◆[d9ceba]Before the battle has [4edaad]30s[d9ceba] to prepare, during the preparation time the defender can choose to enter this battle.
[8c8783]IBattle Introduction:
[614c32]◆[d9ceba]The battle team of the two sides ranked according to the time it took to attack or support the base achieved.
[614c32]◆[d9ceba]Battle in solo or team as ranked order.
[614c32]◆[d9ceba]In the battle of newly joined squads, order into battle team by attack or reinforcement time.
[614c32]◆[d9ceba]Line up the maximum reinforcement team with 6 teams, the maximum attack side with 5 teams.
[614c32]◆[d9ceba]Pilot that can support himself, for example, the Pilot does not support, the reinforcement side and the attack side are the same, 5 teams.
[614c32]◆[d9ceba]After 1 team is defeated, on the 30s countdown, the winning side continues to fight, the losing side goes to the battle with the next team.
[614c32]◆[d9ceba]Winner saves remaining Energy in the next battle, the dead Valarions cannot proceed to battle.
[614c32]◆[d9ceba]The right to own the base is decided when all the fighting teams of one faction fail, the winning side has the right to capture the base.
[614c32]◆[d9ceba]After the base has a battle, clicking the base will display the "watch battle" button, you can enter to watch the battle.
[614c32]◆[d9ceba]After capturing the base, there will be a protection period of [4edaad]15 minutes[d9ceba].
[614c32]◆[d9ceba]After the battle ends, the occupier will save the remaining Energy, performing recovery based on the division of unnit.
[8c8783] Base benefits:
[614c32]◆[d9ceba]Different large and small bases, different benefits received.
[614c32]◆[d9ceba]Base produces [4edaad]Alliance Points[d9ceba] every minute, [4edaad]Personal Points[d9ceba] and [4edaad]Contribution[d9ceba].
[614c32]◆[d9ceba]The longer the capture time is, the lower the base benefit is.
[614c32]◆[d9ceba]Each time you capture, attack, or support, you will receive Alliance Points, Personal Points, and Contribution.
[8c8783] Increased area CRIT:
[614c32]◆[d9ceba]Capturing the base of 3 areas connected in a triangle, can activate [4edaad]effectively increase the area's CRIT [d9ceba]..
[614c32]◆[d9ceba]After Activation the effect of increasing area CRIT, [4edaad]base benefits around the area will increase[d9ceba].
[8c8783] Rankings:
[614c32]◆[d9ceba]After the event ends, based on the total Alliance Point rank to give [4edaad]Alliance Reward[d9ceba].
[614c32]◆[d9ceba]After the event ends, based on the rank of Personal Points to give [4edaad]Internal Alliance rewards[d9ceba]."""]],
		key=[[terrawar_mine]],
		pichelp={},
		title=[[[fefbc8]Alliance Battle Rules]],
	},
	terrawar_orgrank={
		content=[["""[8c8783]Opening time:
[614c32]◆[4edaad]Open the server on the 3rd day [d9ceba]opening, the event lasts 3 days. After the event ends, 4 days later open the next round.
[8c8783]Battle conditions:
[614c32]◆[d9ceba]After the gameplay is open, the map shows the base, the initial base is in a lordless status.
[614c32]◆[d9ceba]Owns Medal - Player level [4edaad]>=level 15[d9ceba], and the player has a Alliance.
[614c32]◆[d9ceba]Players with [4edaad]3 or more Valarions >=level 20[d9ceba] can proceed to attack the base.
[614c32]◆[d9ceba]The same Valarion cannot hold two bases at the same time.
[614c32]◆[d9ceba]Each time to capture, attack or support takes [4edaad]1 Spiritual Energy[d9ceba].
[614c32]◆[d9ceba]Spirit limit is [4edaad]10 Spirit[d9ceba], starting gameplay the default system provides 10 Spirit, restores 1 Spirit every hour, [4edaad]can be purchased Spirit[d9ceba]. 
[8c8783]Attack base & defense:
[614c32]◆[d9ceba]Moving and clicking a non-flank base on the map can complete the attack.
[614c32]◆[d9ceba]The offensive side defeats the defenders' Valarions in the base then can be able to capture the base.
[614c32]◆[d9ceba]Before the battle has [4edaad]30s[d9ceba] to prepare, during the preparation time the defender can choose to enter this battle.
[8c8783]IBattle Introduction:
[614c32]◆[d9ceba]The battle team of the two sides ranked according to the time it took to attack or support the base achieved.
[614c32]◆[d9ceba]Battle in solo or team as ranked order.
[614c32]◆[d9ceba]In the battle of newly joined squads, order into battle team by attack or reinforcement time.
[614c32]◆[d9ceba]Line up the maximum reinforcement team with 6 teams, the maximum attack side with 5 teams.
[614c32]◆[d9ceba]Pilot that can support himself, for example, the Pilot does not support, the reinforcement side and the attack side are the same, 5 teams.
[614c32]◆[d9ceba]After 1 team is defeated, on the 30s countdown, the winning side continues to fight, the losing side goes to the battle with the next team.
[614c32]◆[d9ceba]Winner saves remaining Energy in the next battle, the dead Valarions cannot proceed to battle.
[614c32]◆[d9ceba]The right to own the base is decided when all the fighting teams of one faction fail, the winning side has the right to capture the base.
[614c32]◆[d9ceba]After the base has a battle, clicking the base will display the "watch battle" button, you can enter to watch the battle.
[614c32]◆[d9ceba]After capturing the base, there will be a protection period of [4edaad]15 minutes[d9ceba].
[614c32]◆[d9ceba]After the battle ends, the occupier will save the remaining Energy, performing recovery based on the division of unnit.
[8c8783] Base benefits:
[614c32]◆[d9ceba]Different large and small bases, different benefits received.
[614c32]◆[d9ceba]Base produces [4edaad]Alliance Points[d9ceba] every minute, [4edaad]Personal Points[d9ceba] and [4edaad]Contribution[d9ceba].
[614c32]◆[d9ceba]The longer the capture time is, the lower the base benefit is.
[614c32]◆[d9ceba]Each time you capture, attack, or support, you will receive Alliance Points, Personal Points, and Contribution.
[8c8783] Increased area CRIT:
[614c32]◆[d9ceba]Capturing the base of 3 areas connected in a triangle, can activate [4edaad]effectively increase the area's CRIT [d9ceba]..
[614c32]◆[d9ceba]After Activation the effect of increasing area CRIT, [4edaad]base benefits around the area will increase[d9ceba].
[8c8783] Rankings:
[614c32]◆[d9ceba]After the event ends, based on the total Alliance Point rank to give [4edaad]Alliance Reward[d9ceba].
[614c32]◆[d9ceba]After the event ends, based on the rank of Personal Points to give [4edaad]Internal Alliance rewards[d9ceba]."""]],
		key=[[terrawar_orgrank]],
		pichelp={},
		title=[[[fefbc8]Alliance Battle Rules]],
	},
	terrawar_serverrank={
		content=[["""[8c8783]Opening time:
[614c32]◆[4edaad]Open the server on the 3rd day [d9ceba]opening, the event lasts 3 days. After the event ends, 4 days later open the next round.
[8c8783]Battle conditions:
[614c32]◆[d9ceba]After the gameplay is open, the map shows the base, the initial base is in a lordless status.
[614c32]◆[d9ceba]Owns Medal - Player level [4edaad]>=level 15[d9ceba], and the player has a Alliance.
[614c32]◆[d9ceba]Players with [4edaad]3 or more Valarions >=level 20[d9ceba] can proceed to attack the base.
[614c32]◆[d9ceba]The same Valarion cannot hold two bases at the same time.
[614c32]◆[d9ceba]Each time to capture, attack or support takes [4edaad]1 Spiritual Energy[d9ceba].
[614c32]◆[d9ceba]Spirit limit is [4edaad]10 Spirit[d9ceba], starting gameplay the default system provides 10 Spirit, restores 1 Spirit every hour, [4edaad]can be purchased Spirit[d9ceba]. 
[8c8783]Attack base & defense:
[614c32]◆[d9ceba]Moving and clicking a non-flank base on the map can complete the attack.
[614c32]◆[d9ceba]The offensive side defeats the defenders' Valarions in the base then can be able to capture the base.
[614c32]◆[d9ceba]Before the battle has [4edaad]30s[d9ceba] to prepare, during the preparation time the defender can choose to enter this battle.
[8c8783]IBattle Introduction:
[614c32]◆[d9ceba]The battle team of the two sides ranked according to the time it took to attack or support the base achieved.
[614c32]◆[d9ceba]Battle in solo or team as ranked order.
[614c32]◆[d9ceba]In the battle of newly joined squads, order into battle team by attack or reinforcement time.
[614c32]◆[d9ceba]Line up the maximum reinforcement team with 6 teams, the maximum attack side with 5 teams.
[614c32]◆[d9ceba]Pilot that can support himself, for example, the Pilot does not support, the reinforcement side and the attack side are the same, 5 teams.
[614c32]◆[d9ceba]After 1 team is defeated, on the 30s countdown, the winning side continues to fight, the losing side goes to the battle with the next team.
[614c32]◆[d9ceba]Winner saves remaining Energy in the next battle, the dead Valarions cannot proceed to battle.
[614c32]◆[d9ceba]The right to own the base is decided when all the fighting teams of one faction fail, the winning side has the right to capture the base.
[614c32]◆[d9ceba]After the base has a battle, clicking the base will display the "watch battle" button, you can enter to watch the battle.
[614c32]◆[d9ceba]After capturing the base, there will be a protection period of [4edaad]15 minutes[d9ceba].
[614c32]◆[d9ceba]After the battle ends, the occupier will save the remaining Energy, performing recovery based on the division of unnit.
[8c8783] Base benefits:
[614c32]◆[d9ceba]Different large and small bases, different benefits received.
[614c32]◆[d9ceba]Base produces [4edaad]Alliance Points[d9ceba] every minute, [4edaad]Personal Points[d9ceba] and [4edaad]Contribution[d9ceba].
[614c32]◆[d9ceba]The longer the capture time is, the lower the base benefit is.
[614c32]◆[d9ceba]Each time you capture, attack, or support, you will receive Alliance Points, Personal Points, and Contribution.
[8c8783] Increased area CRIT:
[614c32]◆[d9ceba]Capturing the base of 3 areas connected in a triangle, can activate [4edaad]effectively increase the area's CRIT [d9ceba]..
[614c32]◆[d9ceba]After Activation the effect of increasing area CRIT, [4edaad]base benefits around the area will increase[d9ceba].
[8c8783] Rankings:
[614c32]◆[d9ceba]After the event ends, based on the total Alliance Point rank to give [4edaad]Alliance Reward[d9ceba].
[614c32]◆[d9ceba]After the event ends, based on the rank of Personal Points to give [4edaad]Internal Alliance rewards[d9ceba]."""]],
		key=[[terrawar_serverrank]],
		pichelp={},
		title=[[[fefbc8]Alliance Battle Rules]],
	},
	travel_adventure={
		content=[[[d9ceba]◆Each time when Activation the Trader Meeting, up to a maximum of [4edaad]2[d9ceba] times, after playing 2 times, the Trader Meeting leaves
[d9ceba]◆For every 1 meeting, it costs a certain amount of Gold, Gold costs the same for each time
[d9ceba]◆Points obtained in Meeting can be exchanged for Valarion awakening materials, Expedition items and Dig Map...
[d9ceba] ◆Each time of Meeting will have 8 seconds of observation time, after 30 seconds of guessing, choosing the same Valarion will be awarded Points.]],
		key=[[travel_adventure]],
		pichelp={},
		title=[[Meeting Trader]],
	},
	travel_exchange={
		content=[[[d9ceba]◆It can take you points to change the necessary items]],
		key=[[travel_exchange]],
		pichelp={},
		title=[[Expedition Changing Notification]],
	},
	travel_item={
		content=[[[d9ceba]◆After using the Expedition prop, for a limited time increase the benefits of getting Gold or EXP during the Expedition]],
		key=[[travel_item]],
		pichelp={},
		title=[[Expedition Item Notification]],
	},
	travel_main={
		content=[[[8c8783] Expedition Rules:
[d9ceba]◆You can send 4 Valarions to your Expedition Team
[d9ceba]◆During Expedition time, the Valarion can't be used in Valarion star upgrading
[d9ceba]◆During Expedition time, if you sell your Valarion on Marketplace, corresponding reward belong to the selling Valarion will be forfeited
[d9ceba]◆Expedition time is limited by 3 hours
[8c8783]Reward rules:
[d9ceba]◆After the Expedition begins, reward will be sent every 60 minutes
[d9ceba]◆Depend on your Valarion quality, you will get corresponding Diamond and Astrological Shards.]],
		key=[[travel_main]],
		pichelp={},
		title=[[Expedition]],
	},
	tujian={
		content=[[[d9ceba]◆Can be dragged into the maximum of [4edaad]6[d9ceba] Valarions proceed to display
◆Dragged-in Valarion can be selected to the right of the list and adjusted [4edaad]class level[d9ceba] of the original image
◆Click the "X" on the top right corner of the Valarion in the list to cancel Valarions that have been dragged in
◆The list on the right can adjust the left and right orientation of the original image dragged in]],
		key=[[tujian]],
		pichelp={},
		title=[[Fellow feeling]],
	},
	yjfuben={
		content=[[[d9ceba]◆Project X mainly produces grade Orange materials
[d9ceba]◆Dungeon has 3 types of difficulty to choose, the time to pass is 60 minutes
[d9ceba]◆Free challenge 2 times a day, cost Diamond to challenge 1 more time
[d9ceba]◆During defeating all bosses, 1 card flip bonus can be obtained
[d9ceba]◆Every Wednesday, Sunday after the daily ranking refresh, the top 20 players can get additional rewards.]],
		key=[[yjfuben]],
		pichelp={},
		title=[[Project X]],
	},
	youhaodu={
		content=[[[d9ceba]◆The two sides are friends to increase their friendliness, the starting point is [4edaad]1[d9ceba]
◆Anyone who deletes friends will reduce the friendliness [4edaad]to 0[d9ceba]
◆Team with friends and win the battle, friendly between the two sides [4edaad]+1[d9ceba] (Except for non-Arena)
◆Maximum friendliness is 50000]],
		key=[[youhaodu]],
		pichelp={},
		title=[[Friendly reminder]],
	},
	yueka={
		content=[[[d9ceba]◆[4edaad]30 RMB[d9ceba] buy super value Monthly Card, claim [4edaad]300#w2
[d9ceba]◆[4edaad]For 30 days[d9ceba] enjoy the following privileges:
[d9ceba]◆Each day claim [4edaad]100#w2
[d9ceba]◆Claim [4edaad]5 Skip Tickets Every Day
[d9ceba]◆Energy Limit [4edaad]+20 points
[d9ceba]◆Search offline trust prompt [4edaad]1h
[d9ceba]◆Every day the number of free refreshes [4edaad]Guardian[d9ceba] [4edaad] +1
[d9ceba]◆Intimacy fosters House Goddess [4edaad]+5%
Gift Shop bonuses [4edaad]Limited Day Offers[d9ceba]
After offline searching time increased [4edaad]1h[d9ceba] ]],
		key=[[yueka]],
		pichelp={},
		title=[[Valuable Monthly Card]],
	},
	zhongshenka={
		content=[[[d9ceba]◆[4edaad]98 RMB[d9ceba] buy the strongest Lifetime Card, claim [4edaad]980 #w2
[d9ceba]◆And [4edaad] permanently enjoy the following privileges:
[d9ceba]◆Every day claim [4edaad]110 #w2
[d9ceba]◆Claim [4edaad]10 Skip Tickets Every Day
[d9ceba]◆Gear /Jewel Raid can sweep [4edaad]free
[d9ceba]◆Energy Limit [4edaad]+30 points
[d9ceba]◆Free admission to [4edaad]Shadow Dream Hunter[d9ceba][4edaad]+1 every day
[d9ceba]◆Every day the number of free refreshes [4edaad]Guardian[d9ceba] [4edaad]+2
[d9ceba]◆Intimacy fosters House Goddess [4edaad]+10%
Gift Shop bonuses [4edaad]Limited Day Offers[d9ceba]
After offline search time increased [4edaad]1h[d9ceba] ]],
		key=[[zhongshenka]],
		pichelp={},
		title=[[Lifetime card]],
	},
	zoom_lens={
		content=[[[d9ceba]◆In non-battle maps, hold the screen with two fingers
[d9ceba]◆Slide inward to pull away from the lens, slide out to pull closer to the lens.]],
		key=[[zoom_lens]],
		pichelp={},
		title=[[Zoom lens]],
	},
}
