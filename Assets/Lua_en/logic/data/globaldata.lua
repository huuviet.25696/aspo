module(...)
--auto generate data
GLOBAL={
	attr_charm_itemid={
		desc=[[Vật phẩm tẩy luyện đặc biệt]],
		id=[[attr_charm_itemid]],
		value=[[14999]],
	},
	attr_coin_itemid={
		desc=[[Player's Gold item id]],
		id=[[attr_coin_itemid]],
		value=[[1002]],
	},
	attr_exp_itemid={
		desc=[[Player's EXP item id]],
		id=[[attr_exp_itemid]],
		value=[[1005]],
	},
	attr_fuwen_itemid={
		desc=[[Reset the item id of Rune]],
		id=[[attr_fuwen_itemid]],
		value=[[11101]],
	},
	attr_goldcoin_itemid={
		desc=[[Player's Diamond item id]],
		id=[[attr_goldcoin_itemid]],
		value=[[1003]],
	},
	buyenergy_cost={
		desc=[[Purchase HP once, consumes Diamond]],
		id=[[buyenergy_cost]],
		value=[[10]],
	},
	buyenergy_maxtime={
		desc=[[Number of maximum HP purchasing times every day]],
		id=[[buyenergy_maxtime]],
		value=[[5]],
	},
	buyenergy_rate={
		desc=[[Purchase HP, consuming numbers are increasing]],
		id=[[buyenergy_rate]],
		value=[[10,15,20,25,30]],
	},
	buyenergy_value={
		desc=[[Get HP with only single purchase]],
		id=[[buyenergy_value]],
		value=[[60]],
	},
	canbreak_gradelimit={
		desc=[[It can break through the level guide table control, if the configuration is 0, it means that it cannot be broken, and the value between 0~8 can be configured. The first version can break through to level 0]],
		id=[[canbreak_gradelimit]],
		value=[[5]],
	},
	chapter_dialogue_delay={
		desc=[[Open Plot Dungeon Chap, after x seconds, start the dialogue]],
		id=[[chapter_dialogue_delay]],
		value=[[2]],
	},
	chapter_dialogue_intervel={
		desc=[[Start the dialogue after x seconds]],
		id=[[chapter_dialogue_intervel]],
		value=[[10]],
	},
	charge2score={
		desc=[[Card Purchasing receives double points]],
		id=[[charge2score]],
		value=[[1]],
	},
	charge_card_notify={
		desc=[[Announce the remaining days of Monthly Card]],
		id=[[charge_card_notify]],
		value=[[3]],
	},
	colorcoin_one_row_enchant_charm={
		desc=[[aspo tiêu tốn khi khóa 1 dòng enchant]],
		id=[[colorcoin_one_row_enchant_charm]],
		value=[[20]],
	},
	convoy_fightinterval={
		desc=[[Time to activate Escort quest]],
		id=[[convoy_fightinterval]],
		value=[[20-30]],
	},
	convoy_freerefresh={
		desc=[[Free fefreshing turn every day]],
		id=[[convoy_freerefresh]],
		value=[[1]],
	},
	convoy_maxtime={
		desc=[[Maximum number of escorting turns every day]],
		id=[[convoy_maxtime]],
		value=[[3]],
	},
	convoy_refreshcost={
		desc=[[Fee to refresh Escort Quest]],
		id=[[convoy_refreshcost]],
		value=[[5]],
	},
	convoy_scenegroup={
		desc=[[Escort Quest map group]],
		id=[[convoy_scenegroup]],
		value=[[201]],
	},
	convoy_targetlist={
		desc=[[Escort quest target npc]],
		id=[[convoy_targetlist]],
		value=[[5014,5015,5016,5017,5018,5020,5021,5022,5023,5039,5024,5025,5026,5027,5028,5029,5030,5031,5032,5033,5034,5035,5036,5037,5038,5059,5060]],
	},
	convoy_time={
		desc=[[Time Limit Escort Quest, the unit is minute]],
		id=[[convoy_time]],
		value=[[30]],
	},
	czjj_grade={
		desc=[[Growth Fund Purchasing Level]],
		id=[[czjj_grade]],
		value=[[1]],
	},
	daily_buy_trapmine_point={
		desc=[[Limit of daily purchase searching points (Shadow Lightning Gameplay)]],
		id=[[daily_buy_trapmine_point]],
		value=[[150]],
	},
	daily_fuwen_count={
		desc=[[Daily Soul Spell turns]],
		id=[[daily_fuwen_count]],
		value=[[10]],
	},
	dailytask_checkinterval={
		desc=[[Time period to check the Daily Quest]],
		id=[[dailytask_checkinterval]],
		value=[[2]],
	},
	dailytask_checktime={
		desc=[[Time to check the Daily Quest, enter 30 with 30 minutes, enter 0 with even hour]],
		id=[[dailytask_checktime]],
		value=[[30]],
	},
	dailytask_triggertime={
		desc=[[Can activate Daily quest every day]],
		id=[[dailytask_triggertime]],
		value=[[5]],
	},
	dailytrain_scene_group={
		desc=[[Practising Schedule Monster Attacking Map Group]],
		id=[[dailytrain_scene_group]],
		value=[[103]],
	},
	day_schedule_task={
		desc=[[Random schedule]],
		id=[[day_schedule_task]],
		value=[[13]],
	},
	endless_pve_cost_item={
		desc=[[Endless Dungeon consuming item id]],
		id=[[endless_pve_cost_item]],
		value=[[10022]],
	},
	endlesspve_time={
		desc=[[Battling time of Full Moon Summoning, in seconds]],
		id=[[endlesspve_time]],
		value=[[600]],
	},
	energy_recovertime={
		desc=[[HP recovering time]],
		id=[[energy_recovertime]],
		value=[[5]],
	},
	equalarena_aspo_cost={
		desc=[[GASPO win to Balance Arena]],
		id=[[equalarena_aspo_cost]],
		value=[[10000]],
	},
	equalarena_aspo_rate_fee={
		desc=[[Fee to receive GASPO ]],
		id=[[equalarena_aspo_rate_fee]],
		value=[[5]],
	},
	equalarenaworld_aspo_cost={
		desc=[[GASPO win to Balance Arena World]],
		id=[[equalarenaworld_aspo_cost]],
		value=[[0]],
	},
	equalarenaworld_aspo_rate_fee={
		desc=[[Fee to receive GASPO ]],
		id=[[equalarenaworld_aspo_rate_fee]],
		value=[[0]],
	},
	equip_gem_max_level={
		desc=[[Limit the gemstone equipment level of the character]],
		id=[[equip_gem_max_level]],
		value=[[10]],
	},
	equip_strength_max_level={
		desc=[[Limit the gemstone equipment level of the character]],
		id=[[equip_strength_max_level]],
		value=[[100]],
	},
	fieldboss_reward_limit={
		desc=[[Number of Picnic Boss rewarding times]],
		id=[[fieldboss_reward_limit]],
		value=[[5]],
	},
	fieldboss_reward_ratio={
		desc=[[Picnic Boss rewarding number]],
		id=[[fieldboss_reward_ratio]],
		value=[[0.1]],
	},
	first_charge_chip={
		desc=[[Exchange the First Purchasing Hero Piece]],
		id=[[first_charge_chip]],
		value=[[30]],
	},
	first_task={
		desc=[[New character birth quest]],
		id=[[first_task]],
		value=[[10001]],
	},
	freeenergy_value={
		desc=[[Claim HP free once]],
		id=[[freeenergy_value]],
		value=[[40]],
	},
	handbook_key={
		desc=[[Collection Key item id]],
		id=[[handbook_key]],
		value=[[10022]],
	},
	herobox_scene_group={
		desc=[[Soul Chest Falling map group]],
		id=[[herobox_scene_group]],
		value=[[300]],
	},
	house_gift_count_cost={
		desc=[[Formula of consuming number of times giving gift of the House]],
		id=[[house_gift_count_cost]],
		value=[[2*2^n]],
	},
	house_gift_count_max_cost={
		desc=[[Consume the maximum number of gift giving times of the House]],
		id=[[house_gift_count_max_cost]],
		value=[[2000]],
	},
	house_recover_cnt_time={
		desc=[[CD Time of House Loving turn, unit is second]],
		id=[[house_recover_cnt_time]],
		value=[[3600]],
	},
	house_teaart_speed_cost={
		desc=[[Speeding up fee of the House Tea Ceremony]],
		id=[[house_teaart_speed_cost]],
		value=[[10*2^n]],
	},
	house_worddesk_max_cost={
		desc=[[Consume House Work speeding up]],
		id=[[house_worddesk_max_cost]],
		value=[[2000]],
	},
	hunt_saleprice={
		desc=[[Price for Hunting Spiritual Piece]],
		id=[[hunt_saleprice]],
		value=[[2000]],
	},
	ignore_expadd_grade={
		desc=[[Excess EXP increases the player level]],
		id=[[ignore_expadd_grade]],
		value=[[24]],
	},
	item_nft_targetlist={
		desc=[[item nft]],
		id=[[item_nft_targetlist]],
		value=[[11004,11005,11006,11007,11008,11009,12004,13212,10031,10019,10026,10027,10028,10029,14999,12853,11016,11010]],
	},
	item_upcard={
		desc=[[item upcard]],
		id=[[item_upcard]],
		value=[[10046,10047,10048]],
	},
	item_upcard_multi={
		desc=[[tỷ lệ upcard]],
		id=[[item_upcard_multi]],
		value=[[80,90,90]],
	},
	item_upcard_num={
		desc=[[số lượng item upcard ]],
		id=[[item_upcard_num]],
		value=[[1,1,1]],
	},
	lilian_scene_group={
		desc=[[Practising Schedule Monster Attacking Map Group]],
		id=[[lilian_scene_group]],
		value=[[103]],
	},
	lingli_cost={
		desc=[[Diamonds needed to buy spiritual power]],
		id=[[lingli_cost]],
		value=[[20]],
	},
	lingli_cost_interval={
		desc=[[The gradual increasing of Diamonds used to buy Spiritual Power]],
		id=[[lingli_cost_interval]],
		value=[[2]],
	},
	lingli_cost_max={
		desc=[[Giới hạn tối đa ASPO mua thể lực War base]],
		id=[[lingli_cost_max]],
		value=[[40]],
	},
	lingli_interval={
		desc=[[Time period to recover the Spiritual Power at the Battle Base, the unit is minute]],
		id=[[lingli_interval]],
		value=[[60]],
	},
	lingli_pergive={
		desc=[[Battle Base with each Spiritual Power Recovering time]],
		id=[[lingli_pergive]],
		value=[[1]],
	},
	lockscreen_interval={
		desc=[[Automatic screen lock time period, in seconds]],
		id=[[lockscreen_interval]],
		value=[[600]],
	},
	lovelyhelp_maxtimes={
		desc=[[Number of times receiving maximum points every day]],
		id=[[lovelyhelp_maxtimes]],
		value=[[200]],
	},
	lovelyhelp_maxtimes_sameplayer={
		desc=[[Maximum number of supporting times of the same player]],
		id=[[lovelyhelp_maxtimes_sameplayer]],
		value=[[120]],
	},
	lovelyhelp_rewardsliver_maxtimes={
		desc=[[Number of maximum Coin Pocket rewarding times every day]],
		id=[[lovelyhelp_rewardsliver_maxtimes]],
		value=[[60]],
	},
	lovelyhelp_score={
		desc=[[Number of plus points with each love pairing up time]],
		id=[[lovelyhelp_score]],
		value=[[2]],
	},
	lovelyhelp_sliver={
		desc=[[Rewards Coin pocket with each love pairing up time]],
		id=[[lovelyhelp_sliver]],
		value=[[1]],
	},
	max_difficult_chapter={
		desc=[[The highest chap of the hard Campaign]],
		id=[[max_difficult_chapter]],
		value=[[15]],
	},
	max_endless_pve={
		desc=[[Number of times that player was invited to Endless Dungeon]],
		id=[[max_endless_pve]],
		value=[[3]],
	},
	max_energy={
		desc=[[HP recovering limitation]],
		id=[[max_energy]],
		value=[[120]],
	},
	max_simple_chapter={
		desc=[[The maximum chap opens in the common campaign]],
		id=[[max_simple_chapter]],
		value=[[25]],
	},
	minglei_scene_group={
		desc=[[Map Group of defeating monsters at MinhLei Dungeon]],
		id=[[minglei_scene_group]],
		value=[[200]],
	},
	mint_nemo_cost={
		desc=[[Phí mint Nemo]],
		id=[[mint_nemo_cost]],
		value=[[0.50,0.75,1.00,1.25,1.50]],
	},
	mint_nft_cost={
		desc=[[KC tốn phí khi mint]],
		id=[[mint_nft_cost]],
		value=[[1]],
	},
	mint_nft_level={
		desc=[[Giới hạn cấp nhân vật khi mint]],
		id=[[mint_nft_level]],
		value=[[50]],
	},
	newrole_item={
		desc=[[New account Rewards items]],
		id=[[newrole_item]],
		value=[[0]],
	},
	open_expadd_grade={
		desc=[[Open server level, increase EXP]],
		id=[[open_expadd_grade]],
		value=[[45]],
	},
	open_time_consumediamond={
		desc=[[0h-0p d-m-y bắt đầu,23h59p59s  d-m-y kết thúc total kc]],
		id=[[open_time_consumediamond]],
		value=[[18-2-2023,24-2-2023]],
	},
	open_time_grade={
		desc=[[0h-0p d-m-y bắt đầu,23h59p59s  d-m-y kết thúc cấp]],
		id=[[open_time_grade]],
		value=[[18-2-2023,24-2-2023]],
	},
	open_time_rechargediamond={
		desc=[[0h-0p d-m-y bắt đầu,23h59p59s  d-m-y kết thúc tiêu kc]],
		id=[[open_time_rechargediamond]],
		value=[[18-2-2023,3-3-2023]],
	},
	open_time_warpower={
		desc=[[0h-0p d-m-y bắt đầu,23h59p59s  d-m-y kết thúc CP]],
		id=[[open_time_warpower]],
		value=[[18-2-2023,24-2-2023]],
	},
	org_auto_replace_leader_day={
		desc=[[Number of days joining the Guild to automatically take the position of Guild Leader]],
		id=[[org_auto_replace_leader_day]],
		value=[[1]],
	},
	org_auto_replace_leader_offer={
		desc=[[Exciting to automatically take the position of Guild Leader]],
		id=[[org_auto_replace_leader_offer]],
		value=[[1]],
	},
	parsoul_upgrade_coin={
		desc=[[Gold consuming formula to upgrade Artifact]],
		id=[[parsoul_upgrade_coin]],
		value=[[exp]],
	},
	partner_grade_limit={
		desc=[[giới hạn chênh lệch cấp giữa player vs partner]],
		id=[[partner_grade_limit]],
		value=[[9999]],
	},
	partner_grade_max_limit={
		desc=[[giới hạn cấp partner]],
		id=[[partner_grade_max_limit]],
		value=[[150]],
	},
	partner_item_exp={
		desc=[[Hero EXP Items provide EXP]],
		id=[[partner_item_exp]],
		value=[[2000]],
	},
	partner_quality_recover_talent={
		desc=[[aspo tiêu tốn khi hồi recover talent cho partner theo phẩm]],
		id=[[partner_quality_recover_talent]],
		value=[[3240,1880,560,66,0]],
	},
	partner_rename_goldcoin_cost={
		desc=[[KC tốn đổi tên Hero]],
		id=[[partner_rename_goldcoin_cost]],
		value=[[20]],
	},
	partner_reward_itemid={
		desc=[[Common id of hero reward]],
		id=[[partner_reward_itemid]],
		value=[[1010]],
	},
	partner_talent_max={
		desc=[[điểm latent tối đa của partner]],
		id=[[partner_talent_max]],
		value=[[30,30,30,30,0]],
	},
	pata_grade={
		desc=[[Climbing Tower level]],
		id=[[pata_grade]],
		value=[[45]],
	},
	pata_sweep_cost={
		desc=[[Diamond Tower Climbing consumption]],
		id=[[pata_sweep_cost]],
		value=[[1]],
	},
	playboy_animtime={
		desc=[[Time of giving the Loving Pleasure Kid Chest, in seconds]],
		id=[[playboy_animtime]],
		value=[[3]],
	},
	playboy_boxspeed={
		desc=[[The smaller the Loving Pleasure Kid chest, the faster it moves]],
		id=[[playboy_boxspeed]],
		value=[[0.3]],
	},
	playboy_flydeskinterval={
		desc=[[Interval seconds between Loving Pleasure Kid items and flying table]],
		id=[[playboy_flydeskinterval]],
		value=[[0.2]],
	},
	playboy_flydeskspeed={
		desc=[[The smaller the child's toy item, the faster the flight speed]],
		id=[[playboy_flydeskspeed]],
		value=[[0.3]],
	},
	player_gradelimit={
		desc=[[Player's level limit]],
		id=[[player_gradelimit]],
		value=[[98]],
	},
	player_nemo_limit={
		desc=[[Giới hạn dặc quền mới có tỷ lệ rơi NEMO]],
		id=[[player_nemo_limit]],
		value=[[1]],
	},
	random_row_enchant_charm={
		desc=[[tỷ lệ random số dòng khi  enchant có sử dụng charm]],
		id=[[random_row_enchant_charm]],
		value=[[0,0,10,80,10]],
	},
	recharge_score={
		desc=[[ID of the purchasing period currently opened]],
		id=[[recharge_score]],
		value=[[1]],
	},
	redraw_partner={
		desc=[[Pick the Instant Soul Entering again]],
		id=[[redraw_partner]],
		value=[[2000]],
	},
	rename_role_item={
		desc=[[Consumed item id to rename the character]],
		id=[[rename_role_item]],
		value=[[10023]],
	},
	resume_restore_ratio={
		desc=[[Consuming deposit Rate]],
		id=[[resume_restore_ratio]],
		value=[[0.6,0.8]],
	},
	reward_list_view_close_time={
		desc=[[Automatic closing time of common window of the reward]],
		id=[[reward_list_view_close_time]],
		value=[[10]],
	},
	sevenday_close={
		desc=[[Number of 7-day Target event days]],
		id=[[sevenday_close]],
		value=[[9999]],
	},
	shimen_maxtime={
		desc=[[The largest number of Master quest rounds per day]],
		id=[[shimen_maxtime]],
		value=[[1]],
	},
	terrawars_firstprepare_time={
		desc=[[Time of  the first predicting Battle Base after the server is opened, the first parameter is the number of days, that is, the number of days after the server is opened, the second parameter is the hour, and the third parameter is the minute, that is what time in a day, 3-0 -0 means 0 hour 0 minute the 3rd day starts to predict after opening the server]],
		id=[[terrawars_firstprepare_time]],
		value=[[3-0-0]],
	},
	terrawars_max_lingli={
		desc=[[The maximum Spiritual Power of the Battle Base]],
		id=[[terrawars_max_lingli]],
		value=[[10]],
	},
	terrawars_open_interval={
		desc=[[The next hot predicting time after closing the Battle Base, the unit is minute]],
		id=[[terrawars_open_interval]],
		value=[[4320]],
	},
	terrawars_open_time={
		desc=[[Time to maintain the Battle Base, the unit is minute]],
		id=[[terrawars_open_time]],
		value=[[4320]],
	},
	terrawars_preparetime={
		desc=[[Time to maintain hot predicting of the Battle Base, the unit is minute]],
		id=[[terrawars_preparetime]],
		value=[[1440]],
	},
	time_energy_value={
		desc=[[khung giờ nhận thể lực]],
		id=[[time_energy_value]],
		value=[[12,0,17,30;18,0,22,30]],
	},
	train_maxtimes={
		desc=[[Maximum training reward turns]],
		id=[[train_maxtimes]],
		value=[[420]],
	},
	train_rewardtimes={
		desc=[[Number of rewards for each of the training]],
		id=[[train_rewardtimes]],
		value=[[60]],
	},
	trapmine_box_monster_cd={
		desc=[[Recovering time of Shadow Lightning Chest Monster]],
		id=[[trapmine_box_monster_cd]],
		value=[[21600]],
	},
	trapmine_box_monster_personal={
		desc=[[Daily challenging turn of Shadow Lightning Chest Monster-Personal]],
		id=[[trapmine_box_monster_personal]],
		value=[[10]],
	},
	trapmine_box_monster_server={
		desc=[[Daily challenging turn of Shadow Lightning Chest Monster-Full server]],
		id=[[trapmine_box_monster_server]],
		value=[[500]],
	},
	trapmine_point_recover={
		desc=[[Number of points searching everyday recovering]],
		id=[[trapmine_point_recover]],
		value=[[50]],
	},
	trapmine_rare_monster_personal={
		desc=[[Daily challenging turn of Shadow Lightning Rare Monster-Personal]],
		id=[[trapmine_rare_monster_personal]],
		value=[[10]],
	},
	trapmine_rare_monster_server={
		desc=[[Daily challenging turn of Shadow Lightning Rare Monster-Full server]],
		id=[[trapmine_rare_monster_server]],
		value=[[500]],
	},
	travel_game_card_speed={
		desc=[[Card Flipping Speed]],
		id=[[travel_game_card_speed]],
		value=[[0.3]],
	},
	treasure_scene_group={
		desc=[[Rune map group]],
		id=[[treasure_scene_group]],
		value=[[102]],
	},
	upgrade_energy={
		desc=[[Upgrade HP to receive]],
		id=[[upgrade_energy]],
		value=[[3]],
	},
	version_task={
		desc=[[Quest continuity between versions]],
		id=[[version_task]],
		value=[[0]],
	},
	warchatspeed_left={
		desc=[[Move the battle chat to the left: Number of words/150; The smaller the slower]],
		id=[[warchatspeed_left]],
		value=[[100]],
	},
	warchatspeed_top={
		desc=[[Move the battle chat to the top: 0.5s; The smaller the slower]],
		id=[[warchatspeed_top]],
		value=[[0.5]],
	},
	wash_schoolskill_itemid={
		desc=[[Reset the item id of career skill]],
		id=[[wash_schoolskill_itemid]],
		value=[[10001]],
	},
	wash_schoolskill_openlevel={
		desc=[[Level of unlocking point reseting]],
		id=[[wash_schoolskill_openlevel]],
		value=[[10]],
	},
	yjfuben_buy={
		desc=[[Mua lượt Nightmare]],
		id=[[yjfuben_buy]],
		value=[[200]],
	},
	yjfuben_drawcard={
		desc=[[Gaspo tốn khi mở rương Nightmare (tăng dần)]],
		id=[[yjfuben_drawcard]],
		value=[[100]],
	},
	yk_grade={
		desc=[[Monthly Card Purchasing Level]],
		id=[[yk_grade]],
		value=[[1]],
	},
	zsk_grade={
		desc=[[Lifetime Card Purchasing Level]],
		id=[[zsk_grade]],
		value=[[1]],
	},
}
