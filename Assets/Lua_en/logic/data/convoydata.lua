module(...)
--auto generate data
DATA={
	[1]={
		config={
			[1]={convoy_pool=1,init_weight=35,level=1,rewarid=1001,weight=0,},
			[2]={
				convoy_pool=2,
				init_weight=45,
				level=2,
				rewarid=1002,
				weight=25,
			},
			[3]={
				convoy_pool=3,
				init_weight=20,
				level=3,
				rewarid=1003,
				weight=25,
			},
			[4]={convoy_pool=4,init_weight=0,level=4,rewarid=1004,weight=25,},
			[5]={convoy_pool=5,init_weight=0,level=5,rewarid=1005,weight=25,},
		},
		id=1,
	},
}

RandomTalk={
	[1]={content=[[Jie Jie Jie ~ cute little heads ~]],id=1,},
	[2]={
		content=[[I hear the cry of the one God loves, can you lead me there?]],
		id=2,
	},
	[3]={content=[[Do you know where the priest is going?]],id=3,},
	[4]={content=[[Please save my life, save me.]],id=4,},
	[5]={content=[[Newbie, why you?]],id=5,},
	[6]={
		content=[[There is a treasure in the box ~ (the other side has a weird smile)]],
		id=6,
	},
	[7]={
		content=[[Young man, do you know a place where people won't be disturbed?]],
		id=7,
	},
	[8]={content=[[(Smirks) You really don't want my help?]],id=8,},
	[9]={content=[[Do you see my strong body?]],id=9,},
	[10]={
		content=[[The green fruit is like love, a sweet poison~]],
		id=10,
	},
	[11]={content=[[Barking ~~ (I'm a cat ~ meowmeowmeow)]],id=11,},
	[12]={content=[[Ooo ~]],id=12,},
	[13]={content=[[Meow ~~ Meow ~~ Meow ~~]],id=13,},
	[14]={content=[[DuDu DuDu ~~]],id=14,},
	[15]={
		content=[[Someone keeps staring at my new Armor, help me.]],
		id=15,
	},
	[16]={
		content=[[I don't need to be close, hurry and take me to a place where no one can find me.]],
		id=16,
	},
	[17]={content=[[I, I already have a boyfriend~]],id=17,},
	[18]={
		content=[[They don't learn my martial arts, please help me stop them.]],
		id=18,
	},
	[19]={
		content=[[Meow~ Tickets for Cute Kitty's Tea Party have sold out, but they're still chasing me.]],
		id=19,
	},
}

FollowTalk={
	[1002]={
		content={[1]=1,[2]=2,[3]=3,},
		id=1002,
		name=[[Demon Skull]],
		shape=1002,
		ui_talk={[1]=1,},
	},
	[1009]={
		content={[1]=4,[2]=5,[3]=6,},
		id=1009,
		name=[[Priest]],
		shape=1009,
		ui_talk={[1]=2,},
	},
	[1010]={
		content={[1]=7,[2]=8,[3]=9,},
		id=1010,
		name=[[Nun]],
		shape=1010,
		ui_talk={[1]=3,},
	},
	[1011]={
		content={[1]=10,[2]=11,[3]=12,},
		id=1011,
		name=[[Believer]],
		shape=1011,
		ui_talk={[1]=4,},
	},
	[1014]={
		content={[1]=13,[2]=14,[3]=15,},
		id=1014,
		name=[[QiaoYan]],
		shape=1014,
		ui_talk={[1]=5,},
	},
	[1016]={
		content={[1]=16,[2]=17,[3]=18,},
		id=1016,
		name=[[ShuiSheng]],
		shape=1016,
		ui_talk={[1]=6,},
	},
	[1018]={
		content={[1]=19,[2]=20,[3]=21,},
		id=1018,
		name=[[Gan]],
		shape=1018,
		ui_talk={[1]=7,},
	},
	[1020]={
		content={[1]=22,[2]=23,[3]=24,},
		id=1020,
		name=[[Soul Locker]],
		shape=1020,
		ui_talk={[1]=8,},
	},
	[1200]={
		content={[1]=25,[2]=26,[3]=27,},
		id=1200,
		name=[[Blue Wing]],
		shape=1200,
		ui_talk={[1]=9,},
	},
	[1201]={
		content={[1]=28,[2]=29,[3]=30,},
		id=1201,
		name=[[BiYi]],
		shape=1201,
		ui_talk={[1]=10,},
	},
	[1503]={
		content={[1]=31,[2]=32,[3]=33,},
		id=1503,
		name=[[XiaoBu Cat]],
		shape=1503,
		ui_talk={[1]=11,},
	},
	[1504]={
		content={[1]=34,[2]=35,[3]=36,},
		id=1504,
		name=[[Little Tiger Cat]],
		shape=1504,
		ui_talk={[1]=12,},
	},
	[1505]={
		content={[1]=37,[2]=38,[3]=39,},
		id=1505,
		name=[[XiaoYu Cat]],
		shape=1505,
		ui_talk={[1]=13,},
	},
	[1506]={
		content={[1]=40,[2]=41,[3]=42,},
		id=1506,
		name=[[Little Panther]],
		shape=1506,
		ui_talk={[1]=14,},
	},
	[1508]={
		content={[1]=43,[2]=44,[3]=45,},
		id=1508,
		name=[[XiaoHui]],
		shape=1508,
		ui_talk={[1]=15,},
	},
	[1509]={
		content={[1]=46,[2]=47,[3]=48,},
		id=1509,
		name=[[XiaoYu]],
		shape=1509,
		ui_talk={[1]=16,},
	},
	[1510]={
		content={[1]=49,[2]=50,[3]=51,},
		id=1510,
		name=[[JiaJIa]],
		shape=1510,
		ui_talk={[1]=17,},
	},
	[1750]={
		content={[1]=52,[2]=53,[3]=54,},
		id=1750,
		name=[[BaiPo]],
		shape=1750,
		ui_talk={[1]=18,},
	},
	[1752]={
		content={[1]=55,[2]=56,[3]=57,},
		id=1752,
		name=[[Cute Cat]],
		shape=1752,
		ui_talk={[1]=19,},
	},
}

TalkContent={
	[1]={content=[[Jie Jie Jie Jie ~]],id=1,},
	[2]={content=[[Fun, fun. Let them come. Jie Jie Jie Jie~]],id=2,},
	[3]={content=[[♬Yeah♪Yeah yeah♪♪Yeah yeah♬♫]],id=3,},
	[4]={content=[[My heart goes to you.]],id=4,},
	[5]={
		content=[[We are redeemed in love, our mistakes are pardoned.]],
		id=5,
	},
	[6]={
		content=[[Master, I have to use the diligence to express my thoughts to you.]],
		id=6,
	},
	[7]={content=[[Honor and glory have filled the earth.]],id=7,},
	[8]={
		content=[[I need to find the Priest, still can't return with them.]],
		id=8,
	},
	[9]={
		content=[[We need the owner to praise and love, he will also save the lost.]],
		id=9,
	},
	[10]={
		content=[[How should I do if I like Cute Cat Dumpling]],
		id=10,
	},
	[11]={content=[[Gu~~(╯﹏╰)b, it seems hungry.]],id=11,},
	[12]={
		content=[[Lucky will always be by your side to protect you.]],
		id=12,
	},
	[13]={content=[[Go faster]],id=13,},
	[14]={content=[[Are they still following?]],id=14,},
	[15]={
		content=[[Those people really don't want to live, they dare to chase after us here]],
		id=15,
	},
	[16]={
		content=[[Next time I will take you to see the beauty of the powerful ocean.]],
		id=16,
	},
	[17]={
		content=[[I'm not human, I'm the richest person in the Turtle Race.]],
		id=17,
	},
	[18]={
		content=[[The treasure in the box cannot be robbed.]],
		id=18,
	},
	[19]={
		content=[[The distant mountain entered the dream, but awakened you in the dream.]],
		id=19,
	},
	[20]={
		content=[[Red roses are flown from the dream, like love in the wind that makes people unable to turn hand.]],
		id=20,
	},
	[21]={
		content=[[Young man, what kind of books do you like to read?]],
		id=21,
	},
	[22]={content=[[Is my Ghost Sickle sharp.]],id=22,},
	[23]={
		content=[[Haha~ It's better to not touch me lightly.]],
		id=23,
	},
	[24]={
		content=[[My Ghost Sickle can tear every soul apart.]],
		id=24,
	},
	[25]={
		content=[[A strong body is an art, but love is its soul!]],
		id=25,
	},
	[26]={
		content=[[The embodiment of love and justice, I am the blue wings of lovers~]],
		id=26,
	},
	[27]={
		content=[[Ah, recently BiYi has lost a lot of weight.]],
		id=27,
	},
	[28]={
		content=[[The unfaithful person is an evil, and this arrow will enchant him~~]],
		id=28,
	},
	[29]={
		content=[[The embodiment of love and justice, I am the Scarlet Wing of Lovebirds ~]],
		id=29,
	},
	[30]={
		content=[[BaiPo once said, fashion is smooth lines, shiny muscles.]],
		id=30,
	},
	[31]={content=[[Wang ~]],id=31,},
	[32]={content=[[Wang Wang ~ (Go faster ~)]],id=32,},
	[33]={content=[[Barking ~~ (I'm a cat ~ meowmeowmeow)]],id=33,},
	[34]={content=[[Ow ~~~~]],id=34,},
	[35]={content=[[Ooo ~]],id=35,},
	[36]={content=[[Wow ~ oh ~]],id=36,},
	[37]={content=[[yuyuyu~yuyuyu~]],id=37,},
	[38]={content=[[Meow ~~ Meow ~~ Meow ~~]],id=38,},
	[39]={content=[[Meow meow ~]],id=39,},
	[40]={content=[[Gulugulu ~]],id=40,},
	[41]={content=[[DuDu DuDu ~~]],id=41,},
	[42]={content=[[Gu lu??]],id=42,},
	[43]={content=[[Is my Armor today good?]],id=43,},
	[44]={content=[[Slow down, I'm tired.]],id=44,},
	[45]={content=[[The weather is nice today ~ like me.]],id=45,},
	[46]={content=[[The sun is so bright today.]],id=46,},
	[47]={content=[[The wind has blown.]],id=47,},
	[48]={
		content=[[Do you think my new hairstyle looks good?]],
		id=48,
	},
	[49]={content=[[Slow, slow down. Tired, so tired]],id=49,},
	[50]={
		content=[[I, I'm not, stuttering, just overeating, hiccups.]],
		id=50,
	},
	[51]={
		content=[[Yesterday, yesterday, carelessly, ate a lot.]],
		id=51,
	},
	[52]={
		content=[[Young man, your clothes are very fashionable and have quality.]],
		id=52,
	},
	[53]={
		content=[[Young man, I see that you have a strange skeleton, very suitable for learning martial arts.]],
		id=53,
	},
	[54]={content=[[Ah, don't look down on old people.]],id=54,},
	[55]={content=[[Do you like dumplings made by Cute Cat?]],id=55,},
	[56]={
		content=[[Next time I will invite you to the Tea Party ~ Meow]],
		id=56,
	},
	[57]={
		content=[[Tea Party has a lot of delicious food and cakes.]],
		id=57,
	},
}

ConvoyPool={
	[1]={
		coin=[[20000]],
		convoy_pool={[1]=1506,[2]=1503,[3]=1504,[4]=1505,},
		exp=[[5200]],
		id=1,
	},
	[2]={
		coin=[[25000]],
		convoy_pool={[1]=1016,[2]=1200,[3]=1201,},
		exp=[[5400]],
		id=2,
	},
	[3]={
		coin=[[32000]],
		convoy_pool={[1]=1009,[2]=1018,[3]=1750,},
		exp=[[5600]],
		id=3,
	},
	[4]={
		coin=[[40000]],
		convoy_pool={[1]=1002,[2]=1010,[3]=1011,[4]=1508,[5]=1509,[6]=1510,},
		exp=[[5800]],
		id=4,
	},
	[5]={
		coin=[[50000]],
		convoy_pool={[1]=1014,[2]=1020,[3]=1752,},
		exp=[[6000]],
		id=5,
	},
}
