module(...)
--auto generate data
DATA={
	[1]={id=1,school=1,sex=1,shape=110,},
	[2]={id=2,school=1,sex=2,shape=120,},
	[3]={id=3,school=2,sex=1,shape=130,},
	[4]={id=4,school=2,sex=2,shape=140,},
	[5]={id=5,school=3,sex=1,shape=150,},
	[6]={id=6,school=3,sex=2,shape=160,},
}

--sex-school
MAP={[1]={[1]=1,[2]=3,[3]=5,},[2]={[1]=2,[2]=4,[3]=6,},}

BRANCH_TYPE={
	[1]={
		branch=1,
		create=1,
		desc=[[Single multiple damage]],
		name=[[All]],
		school=1,
		school_name=[[Soldier]],
		weapon=2500,
	},
	[2]={
		branch=2,
		create=0,
		desc=[[Taunt to absorb damage]],
		name=[[All]],
		school=1,
		school_name=[[Soldier]],
		weapon=2400,
	},
	[3]={
		branch=1,
		create=1,
		desc=[[Group-range damage]],
		name=[[All]],
		school=2,
		school_name=[[Sorcerer]],
		weapon=2000,
	},
	[4]={
		branch=2,
		create=0,
		desc=[[Reduce Sealed Control]],
		name=[[All]],
		school=2,
		school_name=[[Sorcerer]],
		weapon=2100,
	},
	[5]={
		branch=1,
		create=1,
		desc=[[Critical splash damage]],
		name=[[All]],
		school=3,
		school_name=[[Knight]],
		weapon=2300,
	},
	[6]={
		branch=2,
		create=0,
		desc=[[Auxiliary Healing Curse]],
		name=[[All]],
		school=3,
		school_name=[[Knight]],
		weapon=2200,
	},
}
FightAmount={
	[1]={level=1,pos=1,},
	[2]={level=1,pos=2,},
	[3]={level=1,pos=3,},
	[4]={level=1,pos=4,},
}
