module(...)
--auto generate data
Reward={
	[1]={
		desc=[[Top 1 rewards]],
		id=1,
		weeky_award={[1]={id=12004,num=5,},},
	},
	[2]={
		desc=[[Top 2 rewards]],
		id=2,
		weeky_award={[1]={id=12004,num=4,},},
	},
	[3]={
		desc=[[Top 3 rewards]],
		id=3,
		weeky_award={[1]={id=12004,num=3,},},
	},
	[4]={
		desc=[[Top 4-20 rewards]],
		id=4,
		weeky_award={[1]={id=12004,num=2,},},
	},
	[5]={
		desc=[[Top 21-50 reward]],
		id=5,
		weeky_award={[1]={id=12004,num=1,},},
	},
	[6]={
		desc=[[Top 50 Above]],
		id=6,
		weeky_award={[1]={id=12004,num=1,},},
	},
}
