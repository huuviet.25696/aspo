module(...)
--auto generate data
DATA={
	[1]={
		desc=[[Use martial arts to defend, use blood to open the path, the bones become demons, and if you resist me, you will die.]],
		icon=1,
		id=1,
		name=[[Soldier]],
	},
	[2]={
		desc=[[Strong but not sharp, arrows will deviate;]],
		icon=2,
		id=2,
		name=[[Sorcerer]],
	},
	[3]={
		desc=[[Keep the heart of the sage in the place closest to the god; clench fist, fight for the god.]],
		icon=3,
		id=3,
		name=[[Knight]],
	},
}
