module(...)
--magic editor build
DATA={
	[9999]={
		[1]={
			cmdList={
				[1]={
					args={
						[1]={[1]=[[大块头]],},
						[2]={[1]=1600,},
						[3]={[1]=35,[2]=8,},
						[4]={[1]=150,},
						[5]={[1]=1,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
				[2]={
					args={[1]={[1]=1,[2]=[[大块头]],},[2]={[1]=[[1111111]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
				[3]={
					args={[1]={[1]=1,[2]=[[大块头]],},[2]={[1]=37,[2]=7,},},
					func=[[PlayerRunto]],
					name=[[ Nhân vật di chuyển]],
				},
				[4]={
					args={
						[1]={[1]=[[小块头]],},
						[2]={[1]=1600,},
						[3]={[1]=35,[2]=8,},
						[4]={[1]=150,},
						[5]={[1]=2,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
				[5]={
					args={[1]={[1]=2,[2]=[[小块头]],},[2]={[1]=34,[2]=8,},},
					func=[[PlayerRunto]],
					name=[[ Nhân vật di chuyển]],
				},
			},
			delay=2,
			idx=1,
			startTime=0,
			type=[[player]],
		},
		[2]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[大块头]],},[2]={[1]=111,},},
					func=[[SetPlayerFaceTo]],
					name=[[ Đặt hướng ký tự]],
				},
				[2]={
					args={[1]={[1]=1,[2]=[[大块头]],},[2]={[1]=[[attack1]],},},
					func=[[PlayerDoAction]],
					name=[[ Nhân vật hành động]],
				},
			},
			delay=5,
			idx=2,
			startTime=2,
			type=[[player]],
		},
		[3]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[大块头]],},[2]={[1]=0,},},
					func=[[SetPlayerActive]],
					name=[[ Đặt xem nhân vật có hiển thị hay không]],
				},
			},
			delay=1,
			idx=3,
			startTime=7,
			type=[[player]],
		},
		[4]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[大块头]],},[2]={[1]=1,},},
					func=[[SetPlayerActive]],
					name=[[ Đặt xem nhân vật có hiển thị hay không]],
				},
			},
			delay=1,
			idx=4,
			startTime=8,
			type=[[player]],
		},
		[5]={cmdList={},delay=3,idx=5,startTime=9,type=[[player]],},
	},
	[10000]={[1]={cmdList={},delay=1,idx=1,startTime=0,type=[[player]],},},
	[10011]={[1]={cmdList={},delay=11,idx=1,startTime=0,type=[[player]],},},
	[10012]={[1]={cmdList={},delay=11,idx=1,startTime=0,type=[[player]],},},
	[10013]={
		[1]={
			cmdList={
				[1]={
					args={
						[1]={[1]=[[ Thẩm phán]],},
						[2]={[1]=1600,},
						[3]={[1]=16,[2]=13,},
						[4]={[1]=0,},
						[5]={[1]=3,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
			},
			delay=10,
			idx=1,
			startTime=0,
			type=[[player]],
		},
	},
	[10014]={
		[1]={
			cmdList={
				[1]={
					args={
						[1]={[1]=[[ Tên người chơi]],},
						[2]={[1]=1600,},
						[3]={[1]=16,[2]=14,},
						[4]={[1]=0,},
						[5]={[1]=1,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
			},
			delay=5,
			idx=1,
			startTime=0,
			type=[[player]],
		},
	},
	[10015]={
		[1]={
			cmdList={
				[1]={
					args={
						[1]={[1]=[[随便]],},
						[2]={[1]=1600,},
						[3]={[1]=27,[2]=17,},
						[4]={[1]=0,},
						[5]={[1]=1,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
				[2]={
					args={
						[1]={[1]=[[ Thẩm phán]],},
						[2]={[1]=1600,},
						[3]={[1]=16,[2]=13,},
						[4]={[1]=0,},
						[5]={[1]=3,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
			},
			delay=1,
			idx=1,
			startTime=0,
			type=[[player]],
		},
		[2]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[随便]],},[2]={[1]=[[随便说说]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
			},
			delay=5,
			idx=2,
			startTime=1,
			type=[[player]],
		},
		[3]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[随便]],},[2]={[1]=29,[2]=18,},},
					func=[[PlayerRunto]],
					name=[[ Nhân vật di chuyển]],
				},
			},
			delay=2,
			idx=3,
			startTime=6,
			type=[[player]],
		},
		[4]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[随便]],},[2]={[1]=[[attack1]],},},
					func=[[PlayerDoAction]],
					name=[[ Nhân vật hành động]],
				},
				[2]={
					args={
						[1]={[1]=[[ tính cách2]],},
						[2]={[1]=1600,},
						[3]={[1]=29,[2]=16,},
						[4]={[1]=0,},
						[5]={[1]=2,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
				[3]={
					args={[1]={[1]=2,[2]=[[ tính cách2]],},[2]={[1]=[[ tính cách说话。。。]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
			},
			delay=2,
			idx=4,
			startTime=8,
			type=[[player]],
		},
	},
	[10100]={
		[1]={
			cmdList={
				[1]={
					args={
						[1]={[1]=[[ Tên người chơi]],},
						[2]={[1]=1508,},
						[3]={[1]=33,[2]=18,},
						[4]={[1]=180,},
						[5]={[1]=1,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
				[2]={
					args={[1]={[1]=1,[2]=[[ Tên người chơi]],},[2]={[1]=36,[2]=10,},},
					func=[[PlayerRunto]],
					name=[[ Nhân vật di chuyển]],
				},
				[3]={
					args={
						[1]={[1]=[[ Hừ hừ]],},
						[2]={[1]=1600,},
						[3]={[1]=36,[2]=9,},
						[4]={[1]=0,},
						[5]={[1]=2,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
			},
			delay=7,
			idx=1,
			startTime=0,
			type=[[player]],
		},
		[2]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[ Tên người chơi]],},[2]={[1]=[[ tao ghét mày！]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
				[2]={
					args={[1]={[1]=2,[2]=[[ Hừ hừ]],},[2]={[1]=[[ Đừng ghét tôi]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
			},
			delay=3,
			idx=2,
			startTime=7,
			type=[[player]],
		},
		[3]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[ Tên người chơi]],},[2]={[1]=[[ chúng ta hãy chia tay]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
				[2]={
					args={[1]={[1]=2,[2]=[[ Hừ hừ]],},[2]={[1]=[[ Tuyệt quá]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
			},
			delay=3,
			idx=3,
			startTime=10,
			type=[[player]],
		},
		[4]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[ Tên người chơi]],},[2]={[1]=34,[2]=18,},},
					func=[[PlayerRunto]],
					name=[[ Nhân vật di chuyển]],
				},
				[2]={
					args={[1]={[1]=2,[2]=[[ Hừ hừ]],},[2]={[1]=[[ Thực ra tôi thích con trai]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
			},
			delay=8,
			idx=4,
			startTime=13,
			type=[[player]],
		},
	},
	[10101]={
		[1]={
			cmdList={
				[1]={
					args={
						[1]={[1]=[[张导演]],},
						[2]={[1]=1014,},
						[3]={[1]=21,[2]=14,},
						[4]={[1]=0,},
						[5]={[1]=1,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
				[2]={
					args={
						[1]={[1]=[[李总]],},
						[2]={[1]=1650,},
						[3]={[1]=20,[2]=13,},
						[4]={[1]=0,},
						[5]={[1]=2,},
					},
					func=[[AddPlayer]],
					name=[[ Tạo ký tự]],
				},
				[3]={
					args={[1]={[1]=1,[2]=[[张导演]],},[2]={[1]=180,},},
					func=[[SetPlayerFaceTo]],
					name=[[ Đặt hướng ký tự]],
				},
			},
			delay=4,
			idx=1,
			startTime=0,
			type=[[player]],
		},
		[2]={
			cmdList={
				[1]={
					args={[1]={[1]=2,[2]=[[李总]],},[2]={[1]=[[我的电影要内定女主11]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
				[2]={
					args={[1]={[1]=2,[2]=[[李总]],},[2]={[1]=[[哥斯拉baby一定要请到]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
			},
			delay=4,
			idx=2,
			startTime=4,
			type=[[player]],
		},
		[3]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[张导演]],},[2]={[1]=[[一定！您再加点投资吧]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
			},
			delay=4,
			idx=3,
			startTime=8,
			type=[[player]],
		},
		[4]={
			cmdList={
				[1]={
					args={[1]={[1]=2,[2]=[[李总]],},[2]={[1]=[[一个条件]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
				[2]={
					args={[1]={[1]=2,[2]=[[李总]],},[2]={[1]=[[今晚baby陪我吃顿饭]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
			},
			delay=4,
			idx=4,
			startTime=12,
			type=[[player]],
		},
		[5]={
			cmdList={
				[1]={
					args={[1]={[1]=1,[2]=[[张导演]],},[2]={[1]=[[我这就打电话给经纪人]],},},
					func=[[PlayerSay]],
					name=[[ Nhân vật nói]],
				},
			},
			delay=4,
			idx=5,
			startTime=16,
			type=[[player]],
		},
	},
}

CONFIG={PATH=[[/Lua/logic/data/npcdialogueanidata.lua]],}
