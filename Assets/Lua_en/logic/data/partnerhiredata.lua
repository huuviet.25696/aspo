module(...)
--auto generate data
DATA={
	[301]={
		desc=[[DPS
Single multiple hit - Critical Hit stack up]],
		full_size={[1]=567,[2]=722,},
		id=301,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=40,
	},
	[302]={
		desc=[[DPS/Debuffer
AOE damage - Inflict stun debuff]],
		full_size={[1]=218,[2]=767,},
		id=302,
		max_times=0,
		recommand_list={[1]=[[Piece]],},
		unlock_grade=10,
	},
	[303]={
		desc=[[DPS
Absorb HP single target - High damage when HP is low]],
		full_size={[1]=719,[2]=874,},
		id=303,
		max_times=0,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=10,
	},
	[305]={
		desc=[[DPS
Damage ignore DEF - Multiple damage dealt]],
		full_size={[1]=948,[2]=971,},
		id=305,
		max_times=0,
		recommand_list={[1]=[[Incattnating]],[2]=[[piece]],},
		unlock_grade=10,
	},
	[306]={
		desc=[[DPS
Single burst - Multiple hit]],
		full_size={[1]=461,[2]=835,},
		id=306,
		max_times=0,
		recommand_list={[1]=[[Incattnating]],[2]=[[piece]],},
		unlock_grade=10,
	},
	[308]={
		desc=[[Debuffer
Inflict Taunt - High defense when HP is low]],
		full_size={[1]=703,[2]=810,},
		id=308,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=45,
	},
	[311]={
		desc=[[Support
Revive single target -  Grant buff to allies]],
		full_size={[1]=757,[2]=744,},
		id=311,
		max_times=1,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=60,
	},
	[312]={
		desc=[[Support
Increase damaga when being silence - Restore single target]],
		full_size={[1]=437,[2]=515,},
		id=312,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=50,
	},
	[313]={
		desc=[[DPS
Multiple hit single target - High damage when HP is low]],
		full_size={[1]=923,[2]=885,},
		id=313,
		max_times=1,
		recommand_list={[1]=[[Login Gifts]],[2]=[[Soul Chests]],[3]=[[piece]],},
		unlock_grade=85,
	},
	[314]={
		desc=[[DPS
Multiple hit & burst single target]],
		full_size={[1]=746,[2]=790,},
		id=314,
		max_times=0,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=10,
	},
	[315]={
		desc=[[Support/Debuffer
Restore HP - Anti-Revive]],
		full_size={[1]=986,[2]=1015,},
		id=315,
		max_times=0,
		recommand_list={[1]=[[Incattnating]],[2]=[[piece]],},
		unlock_grade=10,
	},
	[316]={
		desc=[[DPS
Burst single target - High CRIT & DMG]],
		full_size={[1]=464,[2]=911,},
		id=316,
		max_times=0,
		recommand_list={[1]=[[Lifetime Cards]],[2]=[[Soul Chests]],[3]=[[piece]],},
		unlock_grade=10,
	},
	[401]={
		desc=[[Support
Grant shield to all aiilies - Damage mitigation]],
		full_size={[1]=612,[2]=970,},
		id=401,
		max_times=0,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=10,
	},
	[402]={
		desc=[[DPS
AOE damage - Damage dealt multiple]],
		full_size={[1]=884,[2]=771,},
		id=402,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=70,
	},
	[403]={
		desc=[[DPS
Inflict poison - Spread poison]],
		full_size={[1]=608,[2]=873,},
		id=403,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=9,
	},
	[404]={
		desc=[[Support
Increase SP]],
		full_size={[1]=603,[2]=620,},
		id=404,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=30,
	},
	[405]={
		desc=[[DPS/Support
Multiple shot attack - Zero SP cost - Absorb SP]],
		full_size={[1]=991,[2]=781,},
		id=405,
		max_times=1,
		recommand_list={[1]=[[Incattnating]],[2]=[[piece]],},
		unlock_grade=90,
	},
	[407]={
		desc=[[Support
Restore HP ally - Revive ally]],
		full_size={[1]=1026,[2]=536,},
		id=407,
		max_times=0,
		recommand_list={[1]=[[Lifetime Cards]],[2]=[[Soul Chests]],[3]=[[piece]],},
		unlock_grade=10,
	},
	[409]={
		desc=[[Attacker
Massive damage single target - Extra damage when enemy HP is low]],
		full_size={[1]=379,[2]=524,},
		id=409,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=65,
	},
	[410]={
		desc=[[Support/Debuffer
Grant SPD buff to ally - Debuff SPD to enemy]],
		full_size={[1]=379,[2]=841,},
		id=410,
		max_times=0,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=10,
	},
	[412]={
		desc=[[DPS
Massive damage single target - Restore HP - CRIT DMG stack up]],
		full_size={[1]=531,[2]=662,},
		id=412,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=55,
	},
	[413]={
		desc=[[DPS/Support
AOE damage - Convert damage deal to HP - Curse enemy  ]],
		full_size={[1]=345,[2]=420,},
		id=413,
		max_times=1,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=45,
	},
	[414]={
		desc=[[Debuffer
Grant SPD buff to ally - Debuff SPD to enemy]],
		full_size={[1]=756,[2]=508,},
		id=414,
		max_times=1,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=20,
	},
	[415]={
		desc=[[Support
Sharing damage - Damage mitigation - ATK down debuff]],
		full_size={[1]=293,[2]=409,},
		id=415,
		max_times=0,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=10,
	},
	[416]={
		desc=[[Debuffer
Inflict Sleep debuff - Extra damage - Summoner]],
		full_size={[1]=491,[2]=517,},
		id=416,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=70,
	},
	[417]={
		desc=[[Debuffer
Reflect debuff - Stun enemy - SP increase]],
		full_size={[1]=435,[2]=511,},
		id=417,
		max_times=0,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=10,
	},
	[418]={
		desc=[[DPS
AOE damage - Damage mitigation - Extra damage]],
		full_size={[1]=287,[2]=362,},
		id=418,
		max_times=0,
		recommand_list={[1]=[[Login rewards]],[2]=[[piece]],},
		unlock_grade=10,
	},
	[501]={
		desc=[[DPS
Single multiple attacks - High critical hit - Damage ignores DEF - Instant kill enemy]],
		full_size={[1]=565,[2]=849,},
		id=501,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=12,
	},
	[502]={
		desc=[[DPS
AOE damage - ATK buff stack up]],
		full_size={[1]=922,[2]=754,},
		id=502,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=5,
	},
	[503]={
		desc=[[DPS
AOE damage - 100% crit rate - Auto revive - Invulnerable]],
		full_size={[1]=835,[2]=1116,},
		id=503,
		max_times=0,
		recommand_list={[1]=[[Initial recharge]],[2]=[[piece]],},
		unlock_grade=10,
	},
	[504]={
		desc=[[DPS/Support
AOE damage - Remove SP - Damage over time - Reflect damage]],
		full_size={[1]=643,[2]=916,},
		id=504,
		max_times=0,
		recommand_list={[1]=[[Lifetime Cards]],[2]=[[Soul Chests]],[3]=[[piece]],},
		unlock_grade=10,
	},
	[505]={
		desc=[[DPS/Support
Multiple damage - Clease debuff]],
		full_size={[1]=965,[2]=719,},
		id=505,
		max_times=1,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=80,
	},
	[506]={
		desc=[[DPS/Support
AOE damage - Dispel enemy buff - Damage mitigation]],
		full_size={[1]=1168,[2]=1151,},
		id=506,
		max_times=0,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=10,
	},
	[507]={
		desc=[[DPS
Multiple hit single target - ATK increase stack up]],
		full_size={[1]=477,[2]=623,},
		id=507,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=40,
	},
	[508]={
		desc=[[Support
Summon being killing hero - Restore HP for self]],
		full_size={[1]=881,[2]=1017,},
		id=508,
		max_times=0,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=10,
	},
	[509]={
		desc=[[DPS/Debuffer
Inflict silence debuff - Extra damage when enenmy HP is low]],
		full_size={[1]=1090,[2]=739,},
		id=509,
		max_times=0,
		recommand_list={[1]=[[Soul Chests]],[2]=[[Piece]],},
		unlock_grade=10,
	},
	[510]={
		desc=[[DPS
AOE damage - Spread damage - Multiple hit when using normal attack]],
		full_size={[1]=1028,[2]=956,},
		id=510,
		max_times=0,
		recommand_list={[1]=[[Incattnating]],[2]=[[piece]],},
		unlock_grade=10,
	},
	[511]={
		desc=[[DPS/Debuffer
Restore HP - Disable Atifact effect - Disable Passive Skill]],
		full_size={[1]=582,[2]=680,},
		id=511,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=90,
	},
	[512]={
		desc=[[DPS
Multiple hit single target - Multiple hit when being attacked]],
		full_size={[1]=1296,[2]=1226,},
		id=512,
		max_times=0,
		recommand_list={[1]=[[Incattnating]],[2]=[[piece]],},
		unlock_grade=10,
	},
	[513]={
		desc=[[DPS
AOE damage - Dealt extra damage when being killing - High critical hit]],
		full_size={[1]=707,[2]=742,},
		id=513,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=50,
	},
	[514]={
		desc=[[DPS
AOE damage - Spread damage when killing target - Summoner killer]],
		full_size={[1]=1096,[2]=1198,},
		id=514,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=75,
	},
	[515]={
		desc=[[Debuffer
Convert damage to HP - Inflict Taunt - Restore HP ]],
		full_size={[1]=984,[2]=873,},
		id=515,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=75,
	},
	[516]={
		desc=[[Support
Grant ATK buff to all allies - Anti healing]],
		full_size={[1]=984,[2]=873,},
		id=516,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=75,
	},
	[517]={
		desc=[[DPS
AOE damage - Convert damage to HP - Damage ignore DEF]],
		full_size={[1]=984,[2]=873,},
		id=517,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=75,
	},
	[518]={
		desc=[[Debuffer
Inflict sleep debuff - When being defeated inflict sleep]],
		full_size={[1]=984,[2]=873,},
		id=518,
		max_times=1,
		recommand_list={[1]=[[Receive via ecruiting]],[2]=[[piece]],},
		unlock_grade=75,
	},
}

Config={
	[1]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=1000,
		level=1,
		parid=301,
		times=1,
	},
	[2]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=1200,
		level=1,
		parid=308,
		times=1,
	},
	[3]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=1500,
		level=1,
		parid=312,
		times=1,
	},
	[4]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=5000,
		level=1,
		parid=402,
		times=1,
	},
	[5]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=200,
		level=0,
		parid=403,
		times=1,
	},
	[6]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=200,
		level=1,
		parid=404,
		times=1,
	},
	[7]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=2600,
		level=1,
		parid=409,
		times=1,
	},
	[8]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=1800,
		level=1,
		parid=412,
		times=1,
	},
	[9]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=3500,
		level=1,
		parid=416,
		times=1,
	},
	[10]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=200,
		level=0,
		parid=501,
		times=1,
	},
	[11]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=100,
		level=0,
		parid=502,
		times=1,
	},
	[12]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=30000,
		level=1,
		parid=507,
		times=1,
	},
	[13]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=2200,
		level=1,
		parid=511,
		times=1,
	},
	[14]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=3000,
		level=1,
		parid=513,
		times=1,
	},
	[15]={
		arena_cost=0,
		coin_cost=0,
		goldcoin=500,
		level=1,
		parid=514,
		times=1,
	},
}
