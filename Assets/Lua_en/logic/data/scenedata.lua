module(...)
--auto generate data
DATA={
	[1001]={
		anlei=0,
		id=1001,
		map_id=101000,
		scene_name=[[Velator Academy]],
		transfers={},
	},
	[1002]={
		anlei=1,
		id=1002,
		map_id=200000,
		scene_name=[[Gragia Mountain]],
		transfers={
			[1]={target_scene=1003,target_x=6.4,target_y=19.9,x=13.0,y=1.0,},
		},
	},
	[1003]={
		anlei=1,
		id=1003,
		map_id=201000,
		scene_name=[[Port Eldora]],
		transfers={
			[1]={target_scene=1001,target_x=6.2,target_y=10.6,x=28.0,y=20.0,},
			[2]={target_scene=1002,target_x=12.9,target_y=3.3,x=5.0,y=20.8,},
			[3]={target_scene=1004,target_x=4.0,target_y=20.3,x=16.3,y=1.0,},
		},
	},
	[1004]={
		anlei=1,
		id=1004,
		map_id=202000,
		scene_name=[[Moriana Land]],
		transfers={
			[1]={target_scene=1003,target_x=20.6,target_y=2.7,x=1.0,y=22.0,},
			[2]={target_scene=1001,target_x=33.0,target_y=3.1,x=17.0,y=23.0,},
		},
	},
	[1005]={
		anlei=1,
		id=1005,
		map_id=204000,
		scene_name=[[Skadia Ruins]],
		transfers={
			[1]={target_scene=1006,target_x=23.2,target_y=5.5,x=7.5,y=3.7,},
		},
	},
	[1006]={
		anlei=1,
		id=1006,
		map_id=210400,
		scene_name=[[Eight Gates Village]],
		transfers={
			[1]={target_scene=1005,target_x=8.7,target_y=6.5,x=19.7,y=3.0,},
			[2]={target_scene=1001,target_x=46.3,target_y=3.4,x=4.0,y=1.0,},
		},
	},
	[1007]={
		anlei=0,
		id=1007,
		map_id=200100,
		scene_name=[[Nael Village]],
		transfers={},
	},
	[1008]={
		anlei=1,
		id=1008,
		map_id=206000,
		scene_name=[[Holy Temple]],
		transfers={
			[1]={target_scene=1005,target_x=20.8,target_y=10.8,x=26.0,y=3.7,},
		},
	},
	[1009]={
		anlei=1,
		id=1009,
		map_id=205000,
		scene_name=[[Migal Cavern]],
		transfers={
			[1]={target_scene=1001,target_x=22.5,target_y=21.5,x=6.2,y=2.5,},
			[2]={target_scene=1008,target_x=6.0,target_y=19.2,x=2.3,y=21.5,},
		},
	},
	[1020]={
		anlei=0,
		id=1020,
		map_id=208000,
		scene_name=[[Temple Basement]],
		transfers={},
	},
	[1021]={
		anlei=0,
		id=1021,
		map_id=501000,
		scene_name=[[House]],
		transfers={},
	},
	[1022]={
		anlei=0,
		id=1022,
		map_id=666666,
		scene_name=[[Display specialized map model]],
		transfers={},
	},
	[1023]={
		anlei=0,
		id=1023,
		map_id=261000,
		scene_name=[[Chaotic Boundary]],
		transfers={},
	},
	[1024]={
		anlei=0,
		id=1024,
		map_id=300000,
		scene_name=[[Secret Laboratory]],
		transfers={},
	},
	[1025]={
		anlei=0,
		id=1025,
		map_id=300100,
		scene_name=[[Arena]],
		transfers={},
	},
}
