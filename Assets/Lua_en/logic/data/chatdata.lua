module(...)
--auto generate data
HELP={
	[1]=[[The maximum number of team members is only 4 people, please divide it reasonably]],
	[2]=[[During automatic road finding, click on the map to stop!]],
	[3]=[[Click the map icon in the upper right corner of the main interface to view the map]],
	[4]=[[Do not disclose the account information, ensure safety, avoid heavy losses]],
}

CHATCONFIG={
	[1]={
		energy_cost=[[50]],
		grade_limit=[[10]],
		name=[[World]],
		sort=1,
		talk_gap=[[3]],
		talkable=1,
		voiceable=1,
	},
	[2]={
		energy_cost=[[0]],
		grade_limit=[[0]],
		name=[[Team]],
		sort=2,
		talk_gap=[[1]],
		talkable=1,
		voiceable=1,
	},
	[3]={
		energy_cost=[[0]],
		grade_limit=[[0]],
		name=[[Alliance]],
		sort=3,
		talk_gap=[[1]],
		talkable=1,
		voiceable=1,
	},
	[4]={
		energy_cost=[[0]],
		grade_limit=[[0]],
		name=[[Previous]],
		sort=4,
		talk_gap=[[1]],
		talkable=1,
		voiceable=1,
	},
	[5]={
		energy_cost=[[0]],
		grade_limit=[[0]],
		name=[[System]],
		sort=5,
		talk_gap=[[1]],
		talkable=0,
		voiceable=1,
	},
	[6]={
		energy_cost=[[0]],
		grade_limit=[[0]],
		name=[[Message]],
		sort=6,
		talk_gap=[[0]],
		talkable=0,
		voiceable=0,
	},
}

HORSESPEED={[1]={id=1,speed=150,},}

NormalMsg={
	[1]={
		content=[[Defeat the little monster first, then increase the BOSS's SP]],
		id=1,
	},
	[2]={
		content=[[The guild recruits people, the boss leads the team, let's dance together.]],
		id=2,
	},
	[3]={
		content=[[Find heroes who have full level, make  friends]],
		id=3,
	},
	[4]={content=[[There's only the princess in my heart]],id=4,},
}

ADWords={[1]={id=1,word=[[google]],},}
