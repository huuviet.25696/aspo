module(...)
--auto generate data
SceneData={
	[3000]={
		id=3000,
		leftpos={x=4,y=3,},
		map_id=300000,
		midscope={x=9,y=4,},
		rightpos={x=13,y=4,},
		scope={left=7,right=11,},
	},
}

SceneChat={
	[1]={chatA=[[Choose A]],chatB=[[Choose B]],id=1,},
	[2]={chatA=[[Left, left]],chatB=[[Right right]],id=2,},
	[3]={chatA=[[A]],chatB=[[B]],id=3,},
	[4]={
		chatA=[[Kid run after me!]],
		chatB=[[Kid run after me!]],
		id=4,
	},
	[5]={chatA=[[Am I handsome?]],chatB=[[Am I handsome?]],id=5,},
	[6]={chatA=[[Ah~ Hurry up]],chatB=[[B ~ fast]],id=6,},
	[7]={chatA=[[A]],chatB=[[B]],id=7,},
	[8]={chatA=[[Left Left]],chatB=[[Right right]],id=8,},
	[9]={chatA=[[Go left]],chatB=[[Go to the right]],id=9,},
	[10]={chatA=[[On the left]],chatB=[[It's on the right]],id=10,},
	[11]={
		chatA=[[Get your feet up!]],
		chatB=[[Get your feet up!]],
		id=11,
	},
	[12]={
		chatA=[[Listen to me, I know the answer]],
		chatB=[[Listen to me, I know the answer]],
		id=12,
	},
	[13]={chatA=[[Run with me]],chatB=[[Run with me]],id=13,},
	[14]={chatA=[[so hard]],chatB=[[so hard]],id=14,},
	[15]={chatA=[[Too simple]],chatB=[[Too simple]],id=15,},
	[16]={
		chatA=[[Who came up with this topic but it's not interesting at all]],
		chatB=[[Who came up with this topic but it's not interesting at all]],
		id=16,
	},
	[17]={
		chatA=[[What is this? Who knows the answer?]],
		chatB=[[What is this? Who knows the answer?]],
		id=17,
	},
	[18]={chatA=[[Where to go?]],chatB=[[Where to go?]],id=18,},
	[19]={chatA=[[left?]],chatB=[[left?]],id=19,},
	[20]={chatA=[[correct?]],chatB=[[correct?]],id=20,},
	[21]={chatA=[[Left or right?]],chatB=[[Left or right?]],id=21,},
	[22]={
		chatA=[[Ah, I know, it's over here!]],
		chatB=[[Ah, I know, it's over here!]],
		id=22,
	},
	[23]={
		chatA=[[Not right, not right, this side.]],
		chatB=[[Not right, not right, this side.]],
		id=23,
	},
	[24]={chatA=[[Look over here!]],chatB=[[Look over here!]],id=24,},
	[25]={
		chatA=[[Get out of the way, you guys are preventing me from being an excellent student.]],
		chatB=[[Get out of the way, you guys are preventing me from being an excellent student.]],
		id=25,
	},
	[26]={
		chatA=[[My left hand, my left hand, I can't control]],
		chatB=[[My right hand, my right hand, I can't control it.]],
		id=26,
	},
	[27]={
		chatA=[[Don't follow me, I don't know the answer.]],
		chatB=[[Don't follow me, I don't know the answer.]],
		id=27,
	},
}

UIAward={
	[1]={
		idx=1,
		reward={[1]={num=1,sid=[[10040]],},[2]={num=50000,sid=[[1002]],},},
	},
	[2]={idx=2,reward={[1]={num=45000,sid=[[1002]],},},},
	[3]={idx=3,reward={[1]={num=40000,sid=[[1002]],},},},
	[4]={idx=4,reward={[1]={num=35000,sid=[[1002]],},},},
	[5]={idx=5,reward={[1]={num=30000,sid=[[1002]],},},},
	[6]={idx=10,reward={[1]={num=25000,sid=[[1002]],},},},
	[7]={idx=20,reward={[1]={num=20000,sid=[[1002]],},},},
	[8]={idx=999,reward={[1]={num=10000,sid=[[1002]],},},},
	[9]={
		idx=10001,
		reward={[1]={num=1,sid=[[10040]],},[2]={num=50000,sid=[[1002]],},},
	},
	[10]={idx=10002,reward={[1]={num=45000,sid=[[1002]],},},},
	[11]={idx=10003,reward={[1]={num=40000,sid=[[1002]],},},},
	[12]={idx=10004,reward={[1]={num=35000,sid=[[1002]],},},},
	[13]={idx=10005,reward={[1]={num=30000,sid=[[1002]],},},},
	[14]={idx=10010,reward={[1]={num=25000,sid=[[1002]],},},},
	[15]={idx=10020,reward={[1]={num=20000,sid=[[1002]],},},},
	[16]={idx=10999,reward={[1]={num=10000,sid=[[1002]],},},},
}
