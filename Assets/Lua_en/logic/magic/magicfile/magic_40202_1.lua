module(...)
--magic editor build
DATA={
	atk_stophit=true,
	cmds={
		[1]={
			args={
				alive_time=1.5,
				bind_type=[[pos]],
				body_pos=[[waist]],
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_402/Prefabs/402_attack2.prefab]],
					preload=true,
				},
				excutor=[[atkobj]],
				height=0,
			},
			func_name=[[BodyEffect]],
			start_time=0,
		},
		[2]={
			args={action_name=[[runWar]],excutor=[[atkobj]],},
			func_name=[[PlayAction]],
			start_time=0,
		},
		[3]={
			args={action_name=[[attack2]],excutor=[[atkobj]],},
			func_name=[[PlayAction]],
			start_time=0,
		},
		[4]={
			args={sound_path=[[Magic/sound_magic_40202_1.wav]],sound_rate=1,},
			func_name=[[PlaySound]],
			start_time=0,
		},
		[5]={args={alive_time=0.5,},func_name=[[Name]],start_time=0,},
		[6]={
			args={
				alive_time=2.7,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_402/Prefabs/402_attack2_hit.prefab]],
					preload=true,
				},
				effect_dir_type=[[empty]],
				effect_pos={base_pos=[[vic_team_center]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[vicobjs]],
			},
			func_name=[[StandEffect]],
			start_time=1.2,
		},
		[7]={
			args={sound_path=[[Magic/sound_magic_40202_0.wav]],sound_rate=1,},
			func_name=[[PlaySound]],
			start_time=0.01,
		},
		[8]={args={},func_name=[[MagcAnimStart]],start_time=0.5,},
		[9]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=true,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=1.2,
		},
		[10]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=true,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=1.35,
		},
		[11]={
			args={shake_dis=0.05,shake_rate=20,shake_time=0.2,},
			func_name=[[ShakeScreen]],
			start_time=1.45,
		},
		[12]={args={},func_name=[[MagcAnimEnd]],start_time=1.5,},
		[13]={args={},func_name=[[End]],start_time=2,},
	},
	group_cmds={},
	magic_anim_end_time=1.5,
	magic_anim_start_time=0.5,
	pre_load_res={
		[1]=[[Effect/Magic/magic_eff_new_402/Prefabs/402_attack2.prefab]],
		[2]=[[Effect/Magic/magic_eff_new_402/Prefabs/402_attack2_hit.prefab]],
	},
	run_env=[[war]],
	type=1,
	wait_goback=true,
}
