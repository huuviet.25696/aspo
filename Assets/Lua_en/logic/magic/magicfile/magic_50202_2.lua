module(...)
--magic editor build
DATA={
	atk_stophit=false,
	cmds={
		[1]={
			args={
				alive_time=1.5,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_502/Prefabs/502_hit1.prefab]],
					preload=true,
				},
				effect_dir_type=[[forward]],
				effect_pos={base_pos=[[vic]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[vicobjs]],
			},
			func_name=[[StandEffect]],
			start_time=0.5,
		},
		[2]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=true,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=0.95,
		},
		[3]={args={},func_name=[[End]],start_time=1.5,},
	},
	group_cmds={},
	pre_load_res={[1]=[[Effect/Magic/magic_eff_new_502/Prefabs/502_hit1.prefab]],},
	run_env=[[war]],
	type=1,
	wait_goback=true,
}
