module(...)
--magic editor build
DATA={
	atk_stophit=true,
	cmds={
		[1]={
			args={action_name=[[attack2]],excutor=[[atkobj]],},
			func_name=[[PlayAction]],
			start_time=0,
		},
		[2]={
			args={
				alive_time=1.64,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_303/Prefabs/303_buff.prefab]],
					preload=true,
				},
				effect_dir_type=[[relative]],
				effect_pos={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[vicobj]],
				relative_dir={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=1,},
			},
			func_name=[[StandEffect]],
			start_time=1.1,
		},
		[3]={
			args={
				alive_time=2.64,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_303/Prefabs/303_attack2_avatar.prefab]],
					preload=true,
				},
				effect_dir_type=[[relative]],
				effect_pos={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[vicobj]],
				relative_dir={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=1,},
			},
			func_name=[[StandEffect]],
			start_time=1.1,
		},
		[4]={
			args={
				begin_type=[[current]],
				calc_face=true,
				ease_type=[[InOutSine]],
				end_relative={base_pos=[[vic]],depth=0,relative_angle=0,relative_dis=3,},
				end_type=[[end_relative]],
				excutor=[[atkobj]],
				look_at_pos=true,
				move_time=0.42,
				move_type=[[line]],
			},
			func_name=[[Move]],
			start_time=2.3,
		},
		[5]={args={alive_time=0.5,},func_name=[[Name]],start_time=2.3,},
		[6]={args={},func_name=[[MagcAnimStart]],start_time=3.32,},
		[7]={
			args={
				alive_time=2.64,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_303/Prefabs/303_attack2_1.prefab]],
					preload=true,
				},
				effect_dir_type=[[relative]],
				effect_pos={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[vicobj]],
				relative_dir={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=1,},
			},
			func_name=[[StandEffect]],
			start_time=3.43,
		},
		[8]={
			args={
				alive_time=1.64,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_303/Prefabs/303_land.prefab]],
					preload=true,
				},
				effect_dir_type=[[relative]],
				effect_pos={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[vicobj]],
				relative_dir={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=1,},
			},
			func_name=[[StandEffect]],
			start_time=3.43,
		},
		[9]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=true,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=3.6,
		},
		[10]={
			args={sound_path=[[Magic/sound_magic_50702_1.wav]],sound_rate=1,},
			func_name=[[PlaySound]],
			start_time=3.6,
		},
		[11]={
			args={
				alive_time=3,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_303/Prefabs/303_hit2.prefab]],
					preload=true,
				},
				effect_dir_type=[[relative]],
				effect_pos={base_pos=[[vic]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[vicobj]],
				relative_dir={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=1,},
			},
			func_name=[[StandEffect]],
			start_time=4,
		},
		[12]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=true,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=4.5,
		},
		[13]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=true,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=4.96,
		},
		[14]={args={},func_name=[[MagcAnimEnd]],start_time=5.8,},
		[15]={args={},func_name=[[End]],start_time=6.01,},
	},
	group_cmds={},
	magic_anim_end_time=5.8,
	magic_anim_start_time=3.32,
	pre_load_res={
		[1]=[[Effect/Magic/magic_eff_new_303/Prefabs/303_buff.prefab]],
		[2]=[[Effect/Magic/magic_eff_new_303/Prefabs/303_attack2_avatar.prefab]],
		[3]=[[Effect/Magic/magic_eff_new_303/Prefabs/303_attack2_1.prefab]],
		[4]=[[Effect/Magic/magic_eff_new_303/Prefabs/303_land.prefab]],
		[5]=[[Effect/Magic/magic_eff_new_303/Prefabs/303_hit2.prefab]],
	},
	run_env=[[war]],
	type=1,
	wait_goback=true,
}
