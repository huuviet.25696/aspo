module(...)
--magic editor build
DATA={
	atk_stophit=false,
	cmds={
		[1]={
			args={sound_path=[[Magic/sound_magic_3004_1.wav]],sound_rate=1,},
			func_name=[[PlaySound]],
			start_time=0,
		},
		[2]={args={},func_name=[[MagcAnimEnd]],start_time=0.3,},
		[3]={args={},func_name=[[End]],start_time=0.3,},
		[4]={
			args={
				alive_time=0.55,
				bind_type=[[pos]],
				body_pos=[[foot]],
				effect={
					is_cached=true,
					magic_layer=[[bottom]],
					path=[[Effect/Magic/magic_eff_new_110/Prefabs/110_attack3.prefab]],
					preload=true,
				},
				excutor=[[vicobjs]],
				height=0.4,
			},
			func_name=[[BodyEffect]],
			start_time=0.5,
		},
		[5]={
			args={
				alive_time=0.1,
				ease_hide_time=0.05,
				ease_show_time=0.05,
				excutor=[[vicobjs]],
				mat_path=[[Material/effect_Fresnel_red.mat]],
			},
			func_name=[[ActorMaterial]],
			start_time=0.5,
		},
		[6]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=false,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=0.5,
		},
		[7]={
			args={shake_dis=0.05,shake_rate=5,shake_time=0.2,},
			func_name=[[ShakeScreen]],
			start_time=0.55,
		},
	},
	group_cmds={
		["1"]={
			[1]={
				args={
					alive_time=2,
					bind_type=[[pos]],
					body_pos=[[foot]],
					effect={
						is_cached=true,
						magic_layer=[[center]],
						path=[[Effect/Magic/magic_eff_3004/Prefabs/magic_eff_3004_hit.prefab]],
						preload=true,
					},
					excutor=[[vicobjs]],
					height=0.2,
				},
				func_name=[[BodyEffect]],
				start_time=0,
			},
		},
		["2"]={
			[1]={
				args={
					alive_time=2,
					bind_type=[[pos]],
					body_pos=[[foot]],
					effect={
						is_cached=true,
						magic_layer=[[center]],
						path=[[Effect/Magic/magic_eff_3004/Prefabs/magic_eff_3004_hit_2.prefab]],
						preload=true,
					},
					excutor=[[vicobjs]],
					height=0.2,
				},
				func_name=[[BodyEffect]],
				start_time=0,
			},
		},
	},
	magic_anim_end_time=0.3,
	pre_load_res={[1]=[[Effect/Magic/magic_eff_new_110/Prefabs/110_attack3.prefab]],},
	run_env=[[war]],
	type=1,
	wait_goback=true,
}
