module(...)
--magic editor build
DATA={
	atk_stophit=true,
	cmds={
		[1]={
			args={action_name=[[attack2]],excutor=[[atkobj]],},
			func_name=[[PlayAction]],
			start_time=0,
		},
		[2]={
			args={
				excutor=[[atkobj]],
				face_to=[[fixed_pos]],
				pos={base_pos=[[vic]],depth=0,relative_angle=0,relative_dis=0,},
				time=0,
			},
			func_name=[[FaceTo]],
			start_time=0,
		},
		[3]={args={alive_time=0.5,},func_name=[[Name]],start_time=0,},
		[4]={args={},func_name=[[MagcAnimStart]],start_time=0,},
		[5]={
			args={
				alive_time=4,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_302/Prefabs/302_attack2_spin.prefab]],
					preload=true,
				},
				effect_dir_type=[[relative]],
				effect_pos={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[atkobj]],
				relative_dir={base_pos=[[atkobj]],depth=0,relative_angle=0,relative_dis=0,},
			},
			func_name=[[StandEffect]],
			start_time=0,
		},
		[6]={
			args={
				alive_time=4,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_302/Prefabs/302_attack2_spark.prefab]],
					preload=true,
				},
				effect_dir_type=[[relative]],
				effect_pos={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[atkobj]],
				relative_dir={base_pos=[[atkobj]],depth=0,relative_angle=0,relative_dis=0,},
			},
			func_name=[[StandEffect]],
			start_time=0.7,
		},
		[7]={
			args={
				alive_time=4,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_302/Prefabs/302_jump.prefab]],
					preload=true,
				},
				effect_dir_type=[[relative]],
				effect_pos={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[atkobj]],
				relative_dir={base_pos=[[atkobj]],depth=0,relative_angle=0,relative_dis=0,},
			},
			func_name=[[StandEffect]],
			start_time=3.7,
		},
		[8]={
			args={
				alive_time=1,
				bind_type=[[pos]],
				body_pos=[[waist]],
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_302/Prefabs/302_hit2.prefab]],
					preload=true,
				},
				excutor=[[vicobj]],
				height=0,
			},
			func_name=[[BodyEffect]],
			start_time=5.45,
		},
		[9]={
			args={
				alive_time=1,
				bind_type=[[pos]],
				body_pos=[[waist]],
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_302/Prefabs/302_hit2_2.prefab]],
					preload=true,
				},
				excutor=[[vicobj]],
				height=0,
			},
			func_name=[[BodyEffect]],
			start_time=5.45,
		},
		[10]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=true,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=5.45,
		},
		[11]={args={},func_name=[[MagcAnimEnd]],start_time=5.55,},
		[12]={args={},func_name=[[End]],start_time=6.24,},
	},
	group_cmds={},
	magic_anim_end_time=5.55,
	magic_anim_start_time=0,
	pre_load_res={
		[1]=[[Effect/Magic/magic_eff_new_302/Prefabs/302_attack2_spin.prefab]],
		[2]=[[Effect/Magic/magic_eff_new_302/Prefabs/302_attack2_spark.prefab]],
		[3]=[[Effect/Magic/magic_eff_new_302/Prefabs/302_jump.prefab]],
		[4]=[[Effect/Magic/magic_eff_new_302/Prefabs/302_hit2.prefab]],
		[5]=[[Effect/Magic/magic_eff_new_302/Prefabs/302_hit2_2.prefab]],
	},
	run_env=[[war]],
	type=1,
	wait_goback=true,
}
