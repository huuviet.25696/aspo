module(...)
--magic editor build
DATA={
	atk_stophit=true,
	cmds={
		[1]={
			args={action_name=[[attack1]],excutor=[[atkobj]],},
			func_name=[[PlayAction]],
			start_time=0,
		},
		[2]={args={alive_time=0.5,},func_name=[[Name]],start_time=0,},
		[3]={args={},func_name=[[MagcAnimStart]],start_time=0.2,},
		[4]={
			args={
				alive_time=1,
				bind_type=[[pos]],
				body_pos=[[waist]],
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_301/Prefabs/301_attack1.prefab]],
					preload=true,
				},
				excutor=[[atkobj]],
				height=0,
			},
			func_name=[[BodyEffect]],
			start_time=0.55,
		},
		[5]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=true,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=0.6,
		},
		[6]={
			args={
				alive_time=0.1,
				ease_hide_time=0.05,
				ease_show_time=0,
				excutor=[[vicobjs]],
				mat_path=[[Material/effect_Fresnel_zise.mat]],
			},
			func_name=[[ActorMaterial]],
			start_time=0.6,
		},
		[7]={
			args={
				alive_time=1,
				bind_type=[[pos]],
				body_pos=[[waist]],
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_301/Prefabs/301_hit1.prefab]],
					preload=true,
				},
				excutor=[[vicobjs]],
				height=0,
			},
			func_name=[[BodyEffect]],
			start_time=0.65,
		},
		[8]={args={},func_name=[[MagcAnimEnd]],start_time=1.4,},
		[9]={args={},func_name=[[End]],start_time=1.7,},
	},
	group_cmds={},
	magic_anim_end_time=1.4,
	magic_anim_start_time=0.2,
	pre_load_res={
		[1]=[[Effect/Magic/magic_eff_new_301/Prefabs/301_attack1.prefab]],
		[2]=[[Effect/Magic/magic_eff_new_301/Prefabs/301_hit1.prefab]],
	},
	run_env=[[war]],
	type=1,
	wait_goback=true,
}
