module(...)
--magic editor build
DATA={
	atk_stophit=true,
	cmds={
		[1]={args={alive_time=0.5,},func_name=[[Name]],start_time=0,},
		[2]={
			args={action_name=[[attack2]],excutor=[[atkobj]],},
			func_name=[[PlayAction]],
			start_time=0,
		},
		[3]={
			args={
				alive_time=4,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_510/Prefabs/510_attack2.prefab]],
					preload=true,
				},
				effect_dir_type=[[forward]],
				effect_pos={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[atkobj]],
			},
			func_name=[[StandEffect]],
			start_time=0,
		},
		[4]={
			args={sound_path=[[Magic/sound_magic_51002_1.wav]],sound_rate=1,},
			func_name=[[PlaySound]],
			start_time=0,
		},
		[5]={args={},func_name=[[MagcAnimStart]],start_time=0.2,},
		[6]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=false,
				hurt_delta=0,
				play_anim=false,
			},
			func_name=[[VicHitInfo]],
			start_time=3.7,
		},
		[7]={
			args={
				alive_time=0.15,
				ease_hide_time=0,
				ease_show_time=0,
				excutor=[[vicobj]],
				mat_path=[[Material/effect_Fresnel_Blue.mat]],
			},
			func_name=[[ActorMaterial]],
			start_time=3.7,
		},
		[8]={args={},func_name=[[MagcAnimEnd]],start_time=4.85,},
		[9]={args={},func_name=[[End]],start_time=5,},
	},
	group_cmds={
		["1"]={
			[1]={
				args={
					alive_time=4.5,
					effect={
						is_cached=true,
						magic_layer=[[bottom]],
						path=[[Effect/Magic/magic_eff_510/Prefabs/magic_eff_51002_hit.prefab]],
						preload=true,
					},
					effect_dir_type=[[forward]],
					effect_pos={base_pos=[[vic_team_center]],depth=0,relative_angle=0,relative_dis=0,},
					excutor=[[vicobj]],
				},
				func_name=[[StandEffect]],
				start_time=0,
			},
			[2]={
				args={
					alive_time=4.5,
					effect={
						is_cached=true,
						magic_layer=[[bottom]],
						path=[[Effect/Magic/magic_eff_510/Prefabs/magic_eff_51002_hit1.prefab]],
						preload=true,
					},
					effect_dir_type=[[forward]],
					effect_pos={base_pos=[[vic_team_center]],depth=0,relative_angle=0,relative_dis=0,},
					excutor=[[vicobj]],
				},
				func_name=[[StandEffect]],
				start_time=0,
			},
		},
		["2"]={
			[1]={
				args={
					alive_time=4.5,
					effect={
						is_cached=true,
						magic_layer=[[center]],
						path=[[Effect/Magic/magic_eff_510/Prefabs/magic_eff_51002_hit.prefab]],
						preload=true,
					},
					effect_dir_type=[[forward]],
					effect_pos={base_pos=[[vic_team_center]],depth=0,relative_angle=0,relative_dis=0,},
					excutor=[[vicobj]],
				},
				func_name=[[StandEffect]],
				start_time=0,
			},
		},
	},
	magic_anim_end_time=4.85,
	magic_anim_start_time=0.2,
	pre_load_res={[1]=[[Effect/Magic/magic_eff_new_510/Prefabs/510_attack2.prefab]],},
	run_env=[[war]],
	type=1,
	wait_goback=true,
}
