module(...)
--magic editor build
DATA={
	atk_stophit=true,
	cmds={
		[1]={
			args={action_name=[[attack2]],excutor=[[atkobj]],},
			func_name=[[PlayAction]],
			start_time=0,
		},
		[2]={
			args={
				alive_time=1.5,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_415/Prefabs/415_charge.prefab]],
					preload=true,
				},
				effect_dir_type=[[empty]],
				effect_pos={base_pos=[[atk]],depth=0.5,relative_angle=0,relative_dis=0,},
				excutor=[[atkobj]],
			},
			func_name=[[StandEffect]],
			start_time=0,
		},
		[3]={
			args={
				alive_time=3.5,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_415/Prefabs/415_attack2.prefab]],
					preload=true,
				},
				effect_dir_type=[[empty]],
				effect_pos={base_pos=[[atk]],depth=0.5,relative_angle=0,relative_dis=0,},
				excutor=[[atkobj]],
			},
			func_name=[[StandEffect]],
			start_time=1.5,
		},
		[4]={args={alive_time=0.5,},func_name=[[Name]],start_time=1.5,},
		[5]={
			args={sound_path=[[Magic/sound_magic_41502_1.wav]],sound_rate=1,},
			func_name=[[PlaySound]],
			start_time=1.5,
		},
		[6]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=true,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=1.9,
		},
		[7]={args={},func_name=[[End]],start_time=3.3,},
	},
	group_cmds={},
	pre_load_res={
		[1]=[[Effect/Magic/magic_eff_new_415/Prefabs/415_charge.prefab]],
		[2]=[[Effect/Magic/magic_eff_new_415/Prefabs/415_attack2.prefab]],
	},
	run_env=[[war]],
	type=1,
	wait_goback=true,
}
