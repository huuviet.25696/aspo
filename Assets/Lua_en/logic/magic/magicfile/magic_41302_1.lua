module(...)
--magic editor build
DATA={
	atk_stophit=true,
	cmds={
		[1]={args={},func_name=[[MagcAnimStart]],start_time=0,},
		[2]={args={alive_time=0.5,},func_name=[[Name]],start_time=0,},
		[3]={
			args={action_name=[[attack2]],excutor=[[atkobj]],},
			func_name=[[PlayAction]],
			start_time=0.19,
		},
		[4]={
			args={
				alive_time=1.5,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_413/Prefabs/413_attack2.prefab]],
					preload=true,
				},
				effect_dir_type=[[backward]],
				effect_pos={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=2,},
				excutor=[[atkobj]],
			},
			func_name=[[StandEffect]],
			start_time=1.12,
		},
		[5]={
			args={
				alive_time=3,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_413/Prefabs/413_attack2_2.prefab]],
					preload=true,
				},
				effect_dir_type=[[backward]],
				effect_pos={base_pos=[[atk]],depth=0,relative_angle=0,relative_dis=2,},
				excutor=[[atkobj]],
			},
			editor_is_ban=false,
			func_name=[[StandEffect]],
			start_time=1.92,
		},
		[6]={
			args={
				alive_time=3,
				effect={
					is_cached=true,
					magic_layer=[[center]],
					path=[[Effect/Magic/magic_eff_new_413/Prefabs/413_hit2.prefab]],
					preload=true,
				},
				effect_dir_type=[[empty]],
				effect_pos={base_pos=[[vic]],depth=0,relative_angle=0,relative_dis=0,},
				excutor=[[vicobjs]],
			},
			func_name=[[StandEffect]],
			start_time=3.32,
		},
		[7]={
			args={
				consider_hight=false,
				damage_follow=true,
				face_atk=true,
				hurt_delta=0,
				play_anim=true,
			},
			func_name=[[VicHitInfo]],
			start_time=3.42,
		},
		[8]={
			args={
				alive_time=0.2,
				ease_hide_time=0,
				ease_show_time=0,
				excutor=[[vicobjs]],
				mat_path=[[Material/effect_Fresnel_Green01.mat]],
			},
			func_name=[[ActorMaterial]],
			start_time=3.47,
		},
		[9]={args={},func_name=[[MagcAnimEnd]],start_time=4.3,},
		[10]={args={},func_name=[[End]],start_time=4.42,},
	},
	group_cmds={},
	magic_anim_end_time=4.3,
	magic_anim_start_time=0,
	pre_load_res={
		[1]=[[Effect/Magic/magic_eff_new_413/Prefabs/413_attack2.prefab]],
		[2]=[[Effect/Magic/magic_eff_new_413/Prefabs/413_attack2_2.prefab]],
		[3]=[[Effect/Magic/magic_eff_new_413/Prefabs/413_hit2.prefab]],
	},
	run_env=[[war]],
	type=1,
	wait_goback=true,
}
