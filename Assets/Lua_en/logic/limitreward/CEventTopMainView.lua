local CEventTopMainView = class("CEventTopMainView", CViewBase)

function CEventTopMainView.ctor(self, cb)
	CViewBase.ctor(self, "UI/Activity/LimitReward/EventTopMainView.prefab", cb)
	self.m_DepthType = "Dialog"
	self.m_GroupName = "main"
	self.m_ExtendClose = "Black"
	self.m_OpenEffect = "Scale"
end

function CEventTopMainView.OnCreateView(self)
	self.m_CloseBtn = self:NewUI(1, CButton)
	self.m_SideBtnGrid = self:NewUI(2, CGrid)
	self.m_PowerSupplyPage = self:NewPage(3,CPowerSupplyPage)
	self.m_BurningValleyPage = self:NewPage(4,CBurningValleyPage)
	self.m_PurgatoryPage = self:NewPage(5,CPurgatoryPage)
	self.m_FieryResolvePage = self:NewPage(6,CFieryResolvePage)
	self:InitContent()
end

function CEventTopMainView.InitContent(self)
	self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))
	self.m_BtnList = {}
	self.m_SideBtnGrid:InitChild(function (obj, index)
		local oBtn = CButton.New(obj)
		oBtn.m_Idx = index
		oBtn:SetGroup(self.m_SideBtnGrid:GetInstanceID())
		oBtn:AddUIEvent("click", callback(self, "OnSwitchPage", index))
		self.m_BtnList[index] = oBtn
		return oBtn
	end)
	local oBtn = self.m_SideBtnGrid:GetChild(1)
	self:OnSwitchPage(oBtn.m_Idx)
	--self:UpdateShow()
end
function CEventTopMainView.OnSwitchPage(self, index)
	if index == 1 then
		self:ShowPowerSupplyPage()
	elseif index == 2 then
		self:ShowBurningValleyPage()
	elseif index == 3 then
		self:ShowPurgatoryPage()
	elseif index == 4 then
		self:ShowFieryResolvePage()
	end
	if self.m_BtnList[index] then
		self.m_BtnList[index]:SetSelected(true)
	end
end
function CEventTopMainView.ShowPowerSupplyPage(self)
	self.m_PowerSupplyPage:RefreshPageData()
	self:ShowSubPage(self.m_PowerSupplyPage)
end
function CEventTopMainView.ShowBurningValleyPage(self)
	self.m_BurningValleyPage:RefreshPageData()
	self:ShowSubPage(self.m_BurningValleyPage)
end
function CEventTopMainView.ShowPurgatoryPage(self)
	self.m_PurgatoryPage:RefreshPageData()
	self:ShowSubPage(self.m_PurgatoryPage)
end
function CEventTopMainView.ShowFieryResolvePage(self)
	self.m_FieryResolvePage:RefreshPageData()
	self:ShowSubPage(self.m_FieryResolvePage)
end
function CEventTopMainView.UpdateShow(self)
	-- 	self.m_BtnList[1]:SetActive(true)
	-- 	self.m_BtnList[2]:SetActive(true)
	-- 	self.m_BtnList[3]:SetActive(true)
	-- 	self.m_BtnList[4]:SetActive(true)
	-- self.m_SideBtnGrid:Reposition()
end
return CEventTopMainView