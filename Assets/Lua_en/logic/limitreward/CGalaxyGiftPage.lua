local CGalaxyGiftPage = class("CGalaxyGiftPage", CPageBase)

function CGalaxyGiftPage.ctor(self, ob)
	CPageBase.ctor(self, ob)
end

function CGalaxyGiftPage.OnInitPage(self)
	self.m_CountDownLabel = self:NewUI(1, CCountDownLabel)
	self.m_InfoGrid = self:NewUI(2, CGrid)
	self.m_InfoBox = self:NewUI(3, CBox)
	self:InitContent()
	--self:SetData()
end

function CGalaxyGiftPage.InitContent(self)
	self.m_InfoBoxArr = {}
	g_GradeGiftCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnGradeGiftEvent"))
	self:SetData()
	self.m_InfoBox:SetActive(false)
	self.m_CountDownLabel:SetTickFunc(callback(self, "OnTick"))
	self.m_CountDownLabel:SetTimeUPCallBack(callback(self, "OnTimeUP"))
end

function CGalaxyGiftPage.GetTargetData(self, dData)
	-- local dData =  g_GradeGiftCtrl:GetListData()
	table.sort(dData, function (a, b)
		if a.free_gift.done == b.free_gift.done then
			return a.grade <= b.grade
		else
			return a.free_gift.done < b.free_gift.done
		end
	end)
	return dData
end

function CGalaxyGiftPage.SortInfoBoxes(self, infoBoxArr)
	table.sort(infoBoxArr, function (a, b)
		if a.m_Data.free_gift.done == b.m_Data.free_gift.done then
			return a.m_Data.grade <= b.m_Data.grade
		else
			return a.m_Data.free_gift.done < b.m_Data.free_gift.done
		end
	end)
	return infoBoxArr
end

function CGalaxyGiftPage.SetData(self)
	local oData = g_GradeGiftCtrl:GetListData()
	-- local oData = self:GetTargetData(g_GradeGiftCtrl:GetListData())

	-- printDebug("CGalaxyGiftPage->",oData)
	-- printerror()
	self.m_InfoBoxArr = {}
	self.m_InfoGrid:Clear()
	for i,v in ipairs(oData) do
		self.m_InfoBoxArr[i] = self:CreateInfoBox()
		if (self.m_giftInfoUpdate ~=nil and v.grade == self.m_giftInfoUpdate.grade) then 
			v.free_gift =  self.m_giftInfoUpdate.data.free_gift
		end
		self.m_InfoBoxArr[i]:SetData(v)
	end
	-- self.m_TimeLabel:SetText(g_WelfareCtrl:GetLoopPayTime())
	self:renderInfoBoxes()
end

function CGalaxyGiftPage.renderInfoBoxes(self)
	self.m_InfoBoxArr = self:SortInfoBoxes(self.m_InfoBoxArr)
	for i=1,#self.m_InfoBoxArr do
		if(self.m_InfoBoxArr[i] ~= nil) then
			self.m_InfoGrid:AddChild(self.m_InfoBoxArr[i])
			self.m_InfoBoxArr[i]:SetActive(true)
		end
	end

	if g_GradeGiftCtrl:GetStatus() == define.GradeGift.Status.Buying then
		self.m_CountDownLabel:BeginCountDown(g_GradeGiftCtrl:GetRestTime())
	elseif g_GradeGiftCtrl:GetStatus() == define.GradeGift.Status.Foretell then
		self.m_CountDownLabel:DelTimer()
		self.m_CountDownLabel:SetText(string.format("Level %s open", g_GradeGiftCtrl.m_Grade))
	end
	self:Refresh()
end

function CGalaxyGiftPage.OnTick(self, iValue)
	self.m_CountDownLabel:SetText(string.format("Gift bag left: %s", g_TimeCtrl:GetLeftTime(iValue)))
end

function CGalaxyGiftPage.OnTimeUP(self)
	self.m_CountDownLabel:SetText("be going to be refreshed")
end

function CGalaxyGiftPage.CreateInfoBox(self)
	local oInfoBox = self.m_InfoBox:Clone()
	oInfoBox.m_TitleLabel = oInfoBox:NewUI(1, CLabel)
	oInfoBox.m_Slider = oInfoBox:NewUI(2, CSlider)
	-- oInfoBox.m_Slider:SetActive(false)
	oInfoBox.m_ItemGrid = oInfoBox:NewUI(3, CGrid)
	oInfoBox.m_ItemTipsBox = oInfoBox:NewUI(4, CItemTipsBox)
	oInfoBox.m_ItemTipsBox:SetActive(false)
	oInfoBox.m_SubmitBtn = oInfoBox:NewUI(5, CButton)
	oInfoBox.m_GotMark = oInfoBox:NewUI(6, CSprite)
	oInfoBox.m_SubmitBtn:AddUIEvent("click", callback(self, "OnSubmit",oInfoBox))
	

	function oInfoBox.SetData(self, oData)
		--printDebug("oInfoBox",oData)
		oInfoBox.m_Data = oData
		self.m_TitleLabel:SetText(string.format("Level %s open", oData.grade))
		oInfoBox.m_Slider:SetValue(g_AttrCtrl.grade/oData.grade)
		oInfoBox.m_Slider:SetSliderText(string.format("%s/%s", g_AttrCtrl.grade,oData.grade))
		local data = oData.free_gift.items
		oInfoBox.m_SubmitBtn:SetActive(oData.free_gift.done== 1)
		-- --printDebug("reward",oData.reward)
		for i,v in ipairs(data) do
			local oItemBox = oInfoBox.m_ItemTipsBox:Clone()
			oInfoBox.m_ItemGrid:AddChild(oItemBox)
			oItemBox:SetActive(true)

			oItemBox:SetSid(v.sid, v.amount, {isLocal = true,  uiType = 1})
		end
		-- self.m_SubmitBtn:SetActive(g_GradeGiftCtrl.m_FreeGiftList.done ~= 1)
		
	end

	function oInfoBox.Refresh(self)
		local value = oInfoBox.m_Data.free_gift.done
		-- if value == 1 or (oInfoBox.m_Data.status ~= 0) then
		if value == 1 then
			oInfoBox.m_SubmitBtn:SetActive(false)
			oInfoBox.m_GotMark:SetActive(true)
		else
			oInfoBox.m_SubmitBtn:SetActive(true)
			oInfoBox.m_GotMark:SetActive(false)
			local boxCollider = oInfoBox.m_SubmitBtn:GetComponent(classtype.BoxCollider)
			oInfoBox.m_SubmitBtn:SetGreySprites(oInfoBox.m_Data.grade > g_AttrCtrl.grade)
			boxCollider.enabled = (oInfoBox.m_Data.grade <= g_AttrCtrl.grade)
		end
		-- oInfoBox.m_Slider:SetValue(value/oInfoBox.m_Data.condition)
		-- oInfoBox.m_Slider:SetSliderText(string.format("%s/%s", value, oInfoBox.m_Data.condition))
	end

	return oInfoBox
end

function CGalaxyGiftPage.Refresh(self)
	for i = 1, #self.m_InfoBoxArr do
		self.m_InfoBoxArr[i]:Refresh()
	end
 end
function CGalaxyGiftPage.OnSubmit(self,oInfoBox)
	if g_GradeGiftCtrl:GetStatus() == define.GradeGift.Status.Foretell then
		g_NotifyCtrl:FloatMsg("Level not enough")
		return
	end
	nethuodong.C2GSReceiveFreeGift(oInfoBox.m_Data.grade)
	oInfoBox.m_SubmitBtn:SetActive(false)
	oInfoBox.m_GotMark:SetActive(true)
end

function CGalaxyGiftPage.OnGradeGiftEvent(self, oCtrl)
	if oCtrl.m_EventID == define.GradeGift.Event.UpdataGradeGiftInfoOfGrade then
		self.m_giftInfoUpdate =  g_GradeGiftCtrl:GetGradeGiftInfoUpdate()
		-- for i = 1, #self.m_InfoBoxArr do
		-- 		local oInfoBox = self.m_InfoBoxArr[i]
		-- 		local infoData = oInfoBox.m_Data
		-- 		if (oInfoBox.m_Data.grade == self.m_giftInfoUpdate.grade) then 
		-- 			infoData.free_gift =  self.m_giftInfoUpdate.data.free_gift
		-- 			oInfoBox.m_Data = infoData
		-- 			-- oInfoBox:Refresh()
		-- 		end
		-- end
		self:SetData()
	end
end


return CGalaxyGiftPage