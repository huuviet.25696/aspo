local CPhantomRushPage = class("CPhantomRushPage", CPageBase)

function CPhantomRushPage.ctor(self, ob)
	CPageBase.ctor(self, ob)
end


function CPhantomRushPage.OnInitPage(self)
	self.m_LimitedLabel = self:NewUI(1, CCountDownLabel)
	self.m_InfoGrid = self:NewUI(2, CGrid)
	self.m_InfoBox = self:NewUI(3, CBox)
	self:InitContent()
	--self:SetData()
end

function CPhantomRushPage.InitContent(self)
	self.m_InfoBoxArr = {}
	g_GradeGiftCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnGradeGiftEvent"))
	self:SetData()
	self.m_InfoBox:SetActive(false)
	-- self.m_LimitedLabel:SetTickFunc(callback(self, "OnTick"))
	-- self.m_LimitedLabel:SetTimeUPCallBack(callback(self, "OnTimeUP"))
end

function CPhantomRushPage.SortInfoBoxes(self, infoBoxArr)
	table.sort(infoBoxArr, function (a, b)
		if a.m_Data.buy_gift.done == b.m_Data.buy_gift.done then
			return a.m_Data.grade <= b.m_Data.grade
		else
			return a.m_Data.buy_gift.done < b.m_Data.buy_gift.done
		end
	end)
	return infoBoxArr
end

function CPhantomRushPage.SetData(self)
	local oData = g_GradeGiftCtrl:GetListData()
	self.m_InfoBoxArr = {}
	self.m_InfoGrid:Clear()
	
	for i,v in ipairs(oData) do
		self.m_InfoBoxArr[i] = self:CreateInfoBox()
		-- self:RefreshItem(self.m_FreeGrid, v.free_gift.items)
		if (self.m_giftInfoUpdate ~=nil and v.grade == self.m_giftInfoUpdate.grade) then 
			v.buy_gift =  self.m_giftInfoUpdate.data.buy_gift
		end
		self.m_InfoBoxArr[i]:SetData(v)
	end
	-- if g_GradeGiftCtrl:GetStatus() == define.GradeGift.Status.Buying then
	-- 	self.m_CountDownLabel:BeginCountDown(g_GradeGiftCtrl:GetRestTime())
	-- elseif g_GradeGiftCtrl:GetStatus() == define.GradeGift.Status.Foretell then
	-- 	self.m_CountDownLabel:DelTimer()
	-- 	self.m_CountDownLabel:SetText(string.format("Level %s open", g_GradeGiftCtrl.m_Grade))
	-- end
	-- self.m_TimeLabel:SetText(g_WelfareCtrl:GetLoopPayTime())
	self:renderInfoBoxes()
end

function CPhantomRushPage.renderInfoBoxes(self)
	self.m_InfoBoxArr = self:SortInfoBoxes(self.m_InfoBoxArr)
	for i=1,#self.m_InfoBoxArr do
		if(self.m_InfoBoxArr[i] ~= nil) then
			self.m_InfoGrid:AddChild(self.m_InfoBoxArr[i])
			self.m_InfoBoxArr[i]:SetActive(true)
		end
	end

	if g_GradeGiftCtrl:GetStatus() == define.GradeGift.Status.Buying then
		self.m_CountDownLabel:BeginCountDown(g_GradeGiftCtrl:GetRestTime())
	elseif g_GradeGiftCtrl:GetStatus() == define.GradeGift.Status.Foretell then
		self.m_CountDownLabel:DelTimer()
		self.m_CountDownLabel:SetText(string.format("Level %s open", g_GradeGiftCtrl.m_Grade))
	end
	self:Refresh()
end

function CPhantomRushPage.Refresh(self)
	for i = 1, #self.m_InfoBoxArr do
		self.m_InfoBoxArr[i]:Refresh()
	end
end

function CPhantomRushPage.CreateInfoBox(self)
	local oInfoBox = self.m_InfoBox:Clone()
	oInfoBox.m_TitleLabel = oInfoBox:NewUI(1, CLabel)
	oInfoBox.m_OldCostLabel = oInfoBox:NewUI(2, CLabel)
	oInfoBox.m_NowCostLabel = oInfoBox:NewUI(7, CLabel)
	oInfoBox.m_TipsLabel = oInfoBox:NewUI(8, CLabel)
	oInfoBox.m_progressLabel = oInfoBox:NewUI(9, CLabel)
	oInfoBox.m_Slider = oInfoBox:NewUI(10, CSlider)
	-- oInfoBox.m_Slider:SetActive(false)
	oInfoBox.m_ItemGrid = oInfoBox:NewUI(3, CGrid)
	oInfoBox.m_ItemTipsBox = oInfoBox:NewUI(4, CItemTipsBox)
	oInfoBox.m_ItemTipsBox:SetActive(false)
	oInfoBox.m_SubmitBtn = oInfoBox:NewUI(5, CButton)
	oInfoBox.m_GotMark = oInfoBox:NewUI(6, CSprite)
	oInfoBox.m_SubmitBtn:AddUIEvent("click", callback(self, "OnBuy",oInfoBox))
	

	function oInfoBox.SetData(self, oData)
		oInfoBox.m_Data = oData
		self.m_TitleLabel:SetText(string.format("Level %s open", oData.grade))
		self.m_OldCostLabel:SetText(string.format("Price: %s", oData.old_price))
		self.m_TipsLabel:SetText(string.format("%s%s", (100 - oData.discount),"%"))
		-- oInfoBox.m_progressLabel:SetText(string.format("%s/%s", g_AttrCtrl.grade,oData.grade))
		oInfoBox.m_Slider:SetValue(g_AttrCtrl.grade/oData.grade)
		oInfoBox.m_Slider:SetSliderText(string.format("%s/%s", g_AttrCtrl.grade,oData.grade))
		self.m_NowCostLabel:SetText(oData.now_price)
		local data = oData.buy_gift.items
		oInfoBox.m_SubmitBtn:SetActive(oData.buy_gift.done== 1)
		for i,v in ipairs(data) do
			local oItemBox = oInfoBox.m_ItemTipsBox:Clone()
			oInfoBox.m_ItemGrid:AddChild(oItemBox)
			oItemBox:SetActive(true)

			oItemBox:SetSid(v.sid, v.amount, {isLocal = true,  uiType = 1})
		end
		
	end

	function oInfoBox.Refresh(self)
		local value = oInfoBox.m_Data.buy_gift.done
		--local endTime = oInfoBox.m_Data.endtime
		--local now = g_TimeCtrl:GetTimeS()
		-- if value == 1 or (oInfoBox.m_Data.status ~= 0) then
		if value == 1 then
			oInfoBox.m_SubmitBtn:SetActive(false)
			oInfoBox.m_GotMark:SetActive(true)
		else
			oInfoBox.m_SubmitBtn:SetActive(true)
			oInfoBox.m_GotMark:SetActive(false)
			local boxCollider = oInfoBox.m_SubmitBtn:GetComponent(classtype.BoxCollider)
			oInfoBox.m_SubmitBtn:SetGreySprites(oInfoBox.m_Data.grade > g_AttrCtrl.grade)
			boxCollider.enabled = (oInfoBox.m_Data.grade <= g_AttrCtrl.grade)
		end
		-- if value == 1 then
		-- 	oInfoBox.m_SubmitBtn:SetActive(false)
		-- 	oInfoBox.m_GotMark:SetActive(true)
		-- else
		-- 	oInfoBox.m_SubmitBtn:SetActive(true)
		-- 	oInfoBox.m_GotMark:SetActive(false)
		-- end
	end

	return oInfoBox
end

function CPhantomRushPage.OnBuy(self,oInfoBox)
	if (oInfoBox.m_Data.now_price > g_AttrCtrl.goldcoin) then 
		-- local args = 
		-- 	{
		-- 		msg = "Not enough Diamond to buy!",
		-- 		okCallback = function ()
		-- 		end
		-- 	}
		-- 	g_WindowTipCtrl:SetWindowConfirm(args)
		-- return
		g_NotifyCtrl:FloatMsg("Not enough Diamond to buy!")
		return
	end
	nethuodong.C2GSBuyGradeGift(oInfoBox.m_Data.grade)
	-- if g_GradeGiftCtrl:GetStatus() == define.GradeGift.Status.Foretell then
	-- 	g_NotifyCtrl:FloatMsg("Unopened gift bag")
	-- 	return
	-- end
	-- nethuodong.C2GSReceiveFreeGift(oInfoBox.m_Data.grade)
	-- oInfoBox.m_SubmitBtn:SetActive(false)
end

function CPhantomRushPage.OnGradeGiftEvent(self, oCtrl)
	if oCtrl.m_EventID == define.GradeGift.Event.UpdataGradeGiftInfoOfGrade then
		self.m_giftInfoUpdate =  g_GradeGiftCtrl:GetGradeGiftInfoUpdate()
		-- for i = 1, #self.m_InfoBoxArr do
		-- 		local oInfoBox = self.m_InfoBoxArr[i]
		-- 		local infoData = oInfoBox.m_Data
		-- 		if (oInfoBox.m_Data.grade == m_giftInfoUpdate.grade) then 
		-- 			-- oInfoBox.m_Data.buy_gift.done = m_giftInfoUpdate.data.buy_gift.done
		-- 			infoData.buy_gift =  m_giftInfoUpdate.data.buy_gift
		-- 			oInfoBox.m_Data = infoData
		-- 			oInfoBox:Refresh()
		-- 		end
		-- end
		self:SetData()
	end
end

return CPhantomRushPage