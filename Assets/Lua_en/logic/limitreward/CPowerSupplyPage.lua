local CPowerSupplyPage = class("CPowerSupplyPage", CPageBase)

function CPowerSupplyPage.ctor(self, ob)
	CPageBase.ctor(self, ob)
end

function CPowerSupplyPage.OnInitPage(self)
	self.m_ScrollView = self:NewUI(1, CRecyclingScrollView)
	self.m_ScrollWidget = self:NewUI(2, CWidget)
	-- self.m_infobar_level = self:NewUI(1, CLabel)
	-- self.m_infobar_cp = self:NewUI(2, CLabel)
	-- self.m_infobar_avatar = self:NewUI(3, CSprite)
	-- self.m_infobar_rank = self:NewUI(4, CSprite)
	-- self.m_infobar_name = self:NewUI(5, CLabel)
	self.m_player_level = self:NewUI(8, CLabel)
	self.m_player_cp = self:NewUI(9, CLabel)
	self.m_player_avatar = self:NewUI(6, CSprite)
	self.m_player_rank = self:NewUI(7, CSprite)
	self.m_player_name = self:NewUI(10, CLabel)
	self.m_player_rank_label = self:NewUI(11, CLabel)
	self.m_RankCellBox = self:NewUI(12, CPowerSupplyRankCellBox, true, self)

	---------------
	self:PlayCellInit()
	---------------

	g_RankCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnRankEvent"))

	self.rankListId = define.Rank.RankId.Power -- 103
	self.partnerType = nil
	
	self.myData = g_RankCtrl:GetPlayerRankData(self.rankListId, self.partnerType)
	self.m_Data = g_RankCtrl:GetRankData(self.rankListId, self.partnerType)
	self.m_ExtraData = g_RankCtrl:GetExtraData(self.rankListId, self.partnerType)

	if self.myData ~=nil  and self.m_Data ~=nil and self.m_ExtraData ~=nil and 
	   (next(self.myData) ~= nil) and (next(self.m_Data) ~= nil) and (next(self.m_ExtraData) ~= nil) 
	   then
			self:UpdateContent()
	else
		g_RankCtrl:GetDataFromServer(self.rankListId, 1, self.partnerType,0,false)
	end

end

function CPowerSupplyPage.RefreshPageData(self)
	if (self.rankListId == nil) then return end
	-- printDebug("CPowerSupplyPage.RefreshPageData")
	self.myData = g_RankCtrl:GetPlayerRankData(self.rankListId, self.partnerType)
	self.m_Data = g_RankCtrl:GetRankData(self.rankListId, self.partnerType)
	self.m_ExtraData = g_RankCtrl:GetExtraData(self.rankListId, self.partnerType)

	if self.myData ~=nil  and self.m_Data ~=nil and self.m_ExtraData ~=nil and 
	   (next(self.myData) ~= nil) and (next(self.m_Data) ~= nil) and (next(self.m_ExtraData) ~= nil) 
	   then
			self:UpdateContent()
	else
		g_RankCtrl:GetDataFromServer(self.rankListId, 1, self.partnerType,0,false)
	end
end

function CPowerSupplyPage.OnRankEvent(self,oCtrl)
	-- printDebug("OnRankEvent", oCtrl.m_EventID)
	self.myData = g_RankCtrl:GetPlayerRankData(self.rankListId, self.partnerType)
	self.m_Data = g_RankCtrl:GetRankData(self.rankListId, self.partnerType)
	self.m_ExtraData = g_RankCtrl:GetExtraData(self.rankListId, self.partnerType)

	-- if oCtrl.m_EventID == define.Rank.Event.ReceiveMyRank and oCtrl.m_EventData.ranklistId ==  self.rankListId then
	-- 	self.m_Data = g_RankCtrl:GetRankData(self.rankListId, self.partnerType)
	-- 	if (next(self.m_Data) ~= nil) then
	-- 		-- self:UpdateContent()
	-- 	end
	-- end
	-- if oCtrl.m_EventID == define.Rank.Event.ReceiveExtraData and oCtrl.m_EventData.ranklistId ==  self.rankListId then
	-- 	self.m_Data = g_RankCtrl:GetRankData(self.rankListId, self.partnerType)
	-- 	if (next(self.m_Data) ~= nil) then
	-- 		-- self:UpdateContent()
	-- 	end
	-- end

	-- printDebug(self.myData)
	-- printDebug(self.m_Data)
	-- printDebug(self.m_ExtraData)
	if self.myData ~=nil  and self.m_Data ~=nil and self.m_ExtraData ~=nil and 
	   (next(self.myData) ~= nil) and (next(self.m_Data) ~= nil) and (next(self.m_ExtraData) ~= nil) then
			self:UpdateContent()
	end
end

function CPowerSupplyPage.UpdateContent(self)
	

	-- printDebug("m_Data",self.m_Data)
	-- {
	-- 	consume = 0,
	-- 	grade = 98,
	-- 	level = 0,
	-- 	my_rank = 25,
	-- 	name = "Madeline Evans ",
	-- 	personal_points = 0,
	-- 	pid = 20026,
	-- 	point = 0,
	-- 	position = 0,
	-- 	rank = 25,
	-- 	school = 3,
	-- 	segment = 1,
	-- 	shape = 150,
	-- 	warpower = 28741
	--   }
	self.m_player_level:SetText("Level: " .. g_AttrCtrl.grade)
	self.m_player_cp:SetText("Total CP: " .. g_AttrCtrl:GetTotalPower())
	self.m_player_avatar:SetSpriteName(tostring(self.myData.shape))
	if (tonumber(self.myData.rank) ~=nil) and (tonumber(self.myData.rank) < 6) then
		self.m_player_rank:SetSpriteName(string.format("pic_st_%d", self.myData.rank))
		self.m_player_rank_label:SetText("")
		
	else
		self.m_player_rank:SetSpriteName("pic_topevent_transparent")
		self.m_player_rank_label:SetText(self.myData.rank)
	end
	self.m_player_name:SetText(g_AttrCtrl.name)

	--------------
	-- {
	-- 	attribute = { 8, 3, 7, 6 },
	-- 	content_xpos = { 440 },
	-- 	count = 100,
	-- 	des = 5001,
	-- 	dialog = { 1010, 1011, 1012 },
	-- 	file = "warpower",
	-- 	flag = 1,
	-- 	handle_id = 5,
	-- 	head_xpos = { 60, 265, 440 },
	-- 	id = 103,
	-- 	jump = 0,
	-- 	mien = 1,
	-- 	name = "Total CP",
	-- 	oneself = 1,
	-- 	parent_id = 3,
	-- 	per_page = 20,
	-- 	quick = 1,
	-- 	rank_find = { 1, 50 },
	-- 	refresh_time = 3600,
	-- 	refresh_tips = "Refresh every hour",
	-- 	reset_time = ""
	-- }
	self.m_RankInfo = g_RankCtrl:GetRankInfo(self.rankListId)
	-- printDebug(self.m_RankInfo)
	
	local rankCount = g_RankCtrl:GetRankCount(self.rankListId, nil)
	local maxIndex = math.min(self.m_RankInfo.per_page, rankCount)

	self.m_ScrollView:SetData(self.m_ScrollWidget, maxIndex, self.m_RankCellBox, callback(self, "CellInit"), callback(self, "CellSetData"))
	self.m_ScrollView:SetCrossPageEvent(self.m_RankInfo.per_page, callback(self, "GetPageData"))
	self.m_ScrollView:SetMaxIndex(rankCount)
end


function CPowerSupplyPage.PlayCellInit(self)
	self:CellInit(self.m_RankCellBox)
	self.m_RankCellBox:SetActive(false)
end

function CPowerSupplyPage.CellInit(self, oBox)
	oBox.m_ParentView = self
end

function CPowerSupplyPage.CellSetData(self, oBox, index)
	-- printDebug(index,self.m_Data[index])
	return oBox:SetData(self.m_Data[index], index)
end

function CPowerSupplyPage.GetPageData(self, page)
	if g_RankCtrl:GetDataFromServer(self.rankListId, page, self.partnerType,0,false) then
		self.m_Location = self.m_RankInfo.per_page * (page-1)
		self:ShowGettingData(page)
	end
end

function CPowerSupplyPage.ShowGettingData(self, page)
	if self.m_RankInfo.per_page * (page - 1) < g_RankCtrl:GetRankCount(self.rankListId, self.partnerType) then
		-- self.m_GetingDataSprite:SetActive(true)
		self.m_GetDataTimeUp = false
		self.m_GetDataCallbackData = nil
		self.m_TimerId = Utils.AddTimer(callback(self, "GetDataTimeUp"), 0, 0.5)
	end
end

function CPowerSupplyPage.GetDataTimeUp(self)
	self.m_GetDataTimeUp = true
	self.m_TimerId = nil
	self:AfterGetData(self.m_GetDataCallbackData)
end

function CPowerSupplyPage.AfterGetData(self, oData)
	if self.m_Location ~= nil then
		self.m_ScrollView:SetLocation(self.m_Location)
		self.m_Location = nil
	end
end

return CPowerSupplyPage