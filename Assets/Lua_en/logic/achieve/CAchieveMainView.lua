local CAchieveMainView = class("CAchieveMainView", CViewBase)

function CAchieveMainView.ctor(self, cb)
	CViewBase.ctor(self, "UI/Achieve/AchieveMainView.prefab", cb)
	--界面设置
	self.m_DepthType = "Dialog"
	self.m_ExtendClose = "Black"
	self.m_OpenEffect = "Scale"
	self.m_IsAlwaysShow = true
	
	self.m_Init = true
	self.m_CurDirectionBox = nil
	self.m_CurBelongBox = nil
	self.m_DirectionBoxDic = {}
	self.m_AchieveList = {}
end

function CAchieveMainView.OnCreateView(self)
	self.m_Container = self:NewUI(1, CBox)
	self.m_CloseBtn = self:NewUI(2, CButton)
	self.m_TipsBtn = self:NewUI(3, CButton)
	self.m_Reward = self:NewUI(4, CBox)--:Clone()
	self.m_AchieveInfoBox = self:NewUI(5, CAchieveInfoBox)
	self.m_DirectionBox = self:NewUI(6, CAchieveDirectionBox)
	-- self.m_AchievePointLabel=self:NewUI(8, CLabel)
	-- self.m_ItemGrid = self:NewUI(9, CGrid)
	-- self.m_ItemBox = self:NewUI(10, CItemTipsBox)
	
	--Reward 
	self.m_AchievePointLabel=self.m_Reward:NewUI(1, CLabel)
	self.m_AchieveSlider = self.m_Reward:NewUI(2, CSlider)
	self.m_AchieveRewardBtn = self.m_Reward:NewUI(3, CButton)
	self.m_RewardBox = self.m_Reward:NewUI(4, CBox)--:Clone()
	--RewardBox
	self.m_RewardBox.m_GetBtn = self.m_RewardBox:NewUI(1, CButton)
	self.m_RewardBox.m_ItemGrid = self.m_RewardBox:NewUI(3, CGrid)
	self.m_RewardBox.m_ItemBox = self.m_RewardBox:NewUI(4, CItemTipsBox)
	self.m_RewardBox.m_StateSprite = self.m_RewardBox:NewUI(5, CSprite)
	self.m_RewardBox.m_GetBtnMask = self.m_RewardBox:NewUI(6, CButton)
	
	self:InitContent()
end

function CAchieveMainView.InitContent(self)
	self.m_DirectionBox:SetParentView(self)
	self.m_AchieveInfoBox:SetParentView(self)
	self.m_AchieveRewardBtn.m_TweenRotation = self.m_AchieveRewardBtn:GetComponent(classtype.TweenRotation)
	self.m_CloseBtn:AddUIEvent("click", callback(self, "OnClose"))
	self.m_AchieveRewardBtn:AddUIEvent("click", callback(self, "OnAchieveRewardBtn"))

	-- self:RefreshAchieveSlider()
end

function CAchieveMainView.CloseView(self)
	netachieve.C2GSCloseMainUI()
	CViewBase.CloseView(self)
end

function CAchieveMainView.OnAchieveCtrl(self, oCtrl)
	if oCtrl.m_EventID == define.Achieve.Event.AchieveDone then
		self:RefreshAchieveSlider()
	--	g_AchieveCtrl:ForceShow(self:GetCurDirection(), self:GetCurBelong())
	elseif oCtrl.m_EventID == define.Achieve.Event.RedDot then
		self:CheckRedDot()
	elseif oCtrl.m_EventID == define.Achieve.Event.AchieveDegree then
		if oCtrl.m_EventData then
			self:RefreshAchieve(oCtrl.m_EventData)
		end
	end
end

function CAchieveMainView.OnAchieveRewardBtn(self, obj)
	if obj.get then
		g_AchieveCtrl:C2GSAchievePointReward(g_AchieveCtrl:GetCurRewardIdx())
		g_AchieveCtrl:C2GSAchieveMain()
	else
		CAchieveRewardView:ShowView()
	end
end

function CAchieveMainView.GetCurDirection(self)
	return self.m_DirectionBox:GetCurDirection()

end

function CAchieveMainView.GetCurBelong(self)
	return self.m_DirectionBox:GetCurBelong()
end

function CAchieveMainView.RefreshAchieveInfo(self, achlist)
	self.m_AchieveInfoBox:RefreshAchieveInfo(achlist)
end

function CAchieveMainView.RefreshAchieveSlider(self)
	local iCurRewardIdx = g_AchieveCtrl:GetCurRewardIdx()
	local curReward = data.achievedata.REWARDPOINT[iCurRewardIdx]
	local cur_point = g_AchieveCtrl:GetCurPoint()
	self.m_AchieveSlider:SetValue(cur_point / curReward.point)
	self.m_AchieveSlider:SetSliderText(string.format("%d/%d", cur_point, curReward.point))
	self.m_AchieveRewardBtn:SetActive(false)
	self.m_AchievePointLabel:SetText(string.format("%d/%d", cur_point,curReward.point))

	if cur_point >= curReward.point and not g_AchieveCtrl:IsAllRewardGet() then
		self.m_RewardBox.m_GetBtn:SetActive(true)
		self.m_RewardBox.m_GetBtnMask:SetActive(false)
	else
		self.m_RewardBox.m_GetBtn:SetActive(false)
		self.m_RewardBox.m_GetBtnMask:SetActive(true)
	end

	self.m_RewardBox:SetActive(false)
	self.m_RewardBox.m_ItemGrid:Clear()
	self.m_RewardBox.m_ItemGrid:RemoveChild(self.m_RewardBox.m_ItemBox,true)
	local children = self.m_RewardBox.m_ItemGrid:GetChildList()
	local len = #children
	for i = 1, len do
		self.m_RewardBox.m_ItemGrid:RemoveChild(children[i],false)
	end
	self.m_RewardBox.m_ItemGrid:Clear()

	local oBox = self:InitRewardBox(curReward)
	oBox:SetActive(true)

end

function CAchieveMainView.InitRewardBox(self, dReward)
	local oBox = self.m_RewardBox--:Clone()
	oBox.m_ItemGrid = oBox:NewUI(3, CGrid)
	oBox.m_ItemBox = oBox:NewUI(4, CItemTipsBox)
	oBox.m_GetBtn = oBox:NewUI(1, CButton)
	oBox.m_StateSprite = oBox:NewUI(5, CSprite)
	oBox.m_GetBtnMask = oBox:NewUI(6, CButton)
	oBox.m_ID = dReward.id
	oBox.m_ItemBox:SetActive(false)
	oBox.m_GetBtn:SetActive(false)
	oBox.m_StateSprite:SetActive(false)

	if dReward.get then
		oBox.m_StateSprite:SetActive(true)
	elseif g_AchieveCtrl:GetCurPoint() >= dReward.point then
		oBox.m_GetBtn:SetActive(true)
		oBox.m_GetBtnMask:SetActive(false)
	else
		oBox.m_GetBtn:SetActive(false)
		oBox.m_GetBtnMask:SetActive(true)
	end

	oBox.m_GetBtn:AddUIEvent("click", callback(self, "OnGet", oBox.m_ID))
	local rewardlist = dReward.rewarditem
	for i,v in ipairs(rewardlist) do
		local box = oBox.m_ItemBox:Clone()
		box:SetActive(true)
		if string.find(v.sid, "value") then
			local sid, value = g_ItemCtrl:SplitSidAndValue(v.sid)
			box:SetItemData(sid, value, nil, {isLocal = true})
		elseif string.find(v.sid, "partner") then
			local sid, parId = g_ItemCtrl:SplitSidAndValue(v.sid)
			box:SetItemData(sid, v.num, parId, {isLocal = true})
		else
			box:SetItemData(tonumber(v.sid), v.num, nil, {isLocal = true})
		end
		oBox.m_ItemGrid:AddChild(box)
	end
	oBox.m_ItemGrid:Reposition()
	return oBox
end

function CAchieveMainView.OnGet(self, id, obj)
	printc("Earn Achievement Points:", id)
	g_AchieveCtrl:C2GSAchievePointReward(id)
end

function CAchieveMainView.CheckRedDot(self)
	self.m_DirectionBox:CheckRedDot()
end

function CAchieveMainView.RefreshAchieve(self, info)
	self.m_AchieveInfoBox:RefreshAchieve(info)
end

function CAchieveMainView.DefaultSelect(self, iDirection, iBelong)
	self.m_DirectionBox:DefaultSelect(iDirection, iBelong)
end

return CAchieveMainView