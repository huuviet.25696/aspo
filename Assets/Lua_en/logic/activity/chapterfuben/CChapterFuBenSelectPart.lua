local CChapterFuBenSelectPart = class("CChapterFuBenSelectPart", CBox)

function CChapterFuBenSelectPart.ctor(self, obj, parentView)
	CBox.ctor(self, obj)
	self.m_ChapterNameLabel = self:NewUI(1, CLabel)
	self.m_XiaLaBtn = self:NewUI(2, CButton)
	self.m_ShangLaBtn = self:NewUI(3, CButton)
	self.m_ScrollView = self:NewUI(4, CScrollView)
	self.m_WrapContent = self:NewUI(5, CWrapContent)
	self.m_ChildBox = self:NewUI(6, CBox)
	self.m_ScrollDrag = self:NewUI(7, CSprite)
	self.m_Container = self:NewUI(8, CWidget)
	self.m_BgMask = self:NewUI(9, CSprite)
	self.m_ChapterTypeSpr = self:NewUI(10, CSprite)
	self.m_ChapterTypeLabel = self:NewUI(11, CLabel)
	self.m_SwitchBtn = self:NewUI(12, CButton)
	self.m_SwitchSpr = self:NewUI(13, CSprite)
	self.m_NewTipsSpr = self:NewUI(14, CSprite)
	self.m_RedDotSpr = self:NewUI(15, CSprite)
	self.m_ScrollView2 = self:NewUI(16, CScrollView)
	self.m_WrapContent2 = self:NewUI(17, CWrapContent)
	self.m_ChildBox2 = self:NewUI(18, CBox)
	self.m_SwitchSpr.m_TweenAlpha = self.m_SwitchSpr:GetComponent(classtype.TweenAlpha)
	self:InitContent()
end

function CChapterFuBenSelectPart.InitContent(self)
	UITools.ResizeToRootSize(self.m_Container)
	self.m_ParentView = nil
	self.m_ChapterType = nil
	self.m_ChapterID = nil
	self.m_SwitchSpr.m_TweenAlpha.enabled = false
	self.m_NewTipsSpr:SetActive(false)
	self.m_ChildBox:SetActive(false)
	self.m_ChildBox2:SetActive(false)
	self.m_ChapterNameLabel:AddUIEvent("click", callback(self, "OnChapterNameLabel"))
	self.m_XiaLaBtn:AddUIEvent("click", callback(self, "OnShowScrollView", true))
	self.m_ShangLaBtn:AddUIEvent("click", callback(self, "OnShowScrollView", false))
	self.m_BgMask:AddUIEvent("click", callback(self, "OnShowScrollView", false))
	self.m_SwitchBtn:AddUIEvent("click", callback(self, "OnSwitchBtn"))

	g_GuideCtrl:AddGuideUI("chaterfb_switch_btn", self.m_SwitchBtn)
	local guide_ui = {"chaterfb_switch_btn"}
	g_GuideCtrl:LoadTipsGuideEffect(guide_ui)

	self.m_WrapContent:SetCloneChild(self.m_ChildBox, 
		function(oChild)
			oChild.m_NameLabel = oChild:NewUI(1, CLabel)
			oChild.m_LockSpr = oChild:NewUI(2, CSprite)
			oChild.m_LockLabel = oChild:NewUI(3, CLabel)
			return oChild
		end)
	self.m_WrapContent2:SetCloneChild(self.m_ChildBox2, 
		function(oChild)
			oChild.m_NameLabel = oChild:NewUI(1, CLabel)
			oChild.m_LockSpr = oChild:NewUI(2, CSprite)
			oChild.m_LockLabel = oChild:NewUI(3, CLabel)
			return oChild
		end)

	local maxchapter = g_ChapterFuBenCtrl:GetCurMaxChapter(self.m_ChapterType)
	self.m_WrapContent:SetRefreshFunc(function(oChild, dData)
		if dData then
			oChild:SetActive(true)
			if dData.chapterid > maxchapter then
				oChild.m_NameLabel:SetText("")
				oChild.m_LockSpr:SetActive(true)
				oChild.m_LockLabel:SetText(string.format("pass chapter challenge  %s will unlock", dData.chapterid))
			else
				oChild.m_NameLabel:SetText(dData.chaptername)
				oChild.m_LockSpr:SetActive(false)
			end
			oChild:AddUIEvent("click", callback(self, "OnChildBox", dData.type, dData.chapterid))
		else	
			oChild:SetActive(false)
		end
	end)
	self.m_WrapContent2:SetRefreshFunc(function(oChild, dData)
		if dData then
			oChild:SetActive(true)
			if dData.chapterid > maxchapter then
				oChild.m_NameLabel:SetText("")
				oChild.m_LockSpr:SetActive(true)
				-- local dConfig = DataTools.GetChapterConfig(self.m_ChapterType, dData.chapterid, 1)
				-- local grade = 0
				-- for i,v in ipairs(dConfig.open_condition) do
				-- 	if string.find(v, "Level=") then
				-- 		for out in string.gmatch(v, "=(%w+)") do
				-- 			grade = tonumber(out)
				-- 			break
				-- 		end
				-- 	end
				-- end
				-- oChild.m_LockLabel:SetText(string.format("Level %s and passed corresponding Normal Mission", grade))
				oChild.m_LockLabel:SetText(string.format("Unlock after passed Hard Mission %s ", dData.chapterid))
			else
				oChild.m_NameLabel:SetText(dData.chaptername)
				oChild.m_LockSpr:SetActive(false)
			end
			oChild:AddUIEvent("click", callback(self, "OnChildBox", dData.type, dData.chapterid))
		else	
			oChild:SetActive(false)
		end
	end)
	local dChapterInfo = DataTools.GetChapterInfo(1)
	local dChapterInfo2 = DataTools.GetChapterInfo(2)
	local list = {}
	local list2 = {}
	local len = g_ChapterFuBenCtrl:GetMaxOpenChapter(1)
	local len2 = g_ChapterFuBenCtrl:GetMaxOpenChapter(2)
	for i=1,len do
		table.insert(list, dChapterInfo[i])
	end
	for i=1,len2 do
		table.insert(list2, dChapterInfo2[i])
	end
	self.m_WrapContent:SetData(list, true)
	self.m_WrapContent2:SetData(list2, true)
	self:OnShowScrollView(false)
	self.m_ScrollView:SetActive(false)
	self.m_ScrollView2:SetActive(false)

end

function CChapterFuBenSelectPart.SetParentView(self, oView)
	self.m_ParentView = oView
end

function CChapterFuBenSelectPart.RefreshWrapContent(self)
	-- local maxchapter = g_ChapterFuBenCtrl:GetCurMaxChapter(self.m_ChapterType)
	-- if self.m_ChapterType == define.ChapterFuBen.Type.Simple then
	-- 	self.m_WrapContent:SetRefreshFunc(function(oChild, dData)
	-- 		if dData then
	-- 			oChild:SetActive(true)
	-- 			if dData.chapterid > maxchapter then
	-- 				oChild.m_NameLabel:SetText("")
	-- 				oChild.m_LockSpr:SetActive(true)
	-- 				oChild.m_LockLabel:SetText(string.format("Unlock after passed Mission %s ", dData.chapterid))
	-- 			else
	-- 				oChild.m_NameLabel:SetText(dData.chaptername)
	-- 				oChild.m_LockSpr:SetActive(false)
	-- 			end
	-- 			oChild:AddUIEvent("click", callback(self, "OnChildBox", dData.type, dData.chapterid))
	-- 		else	
	-- 			oChild:SetActive(false)
	-- 		end
	-- 	end)
	-- elseif self.m_ChapterType == define.ChapterFuBen.Type.Difficult then
	-- 	self.m_WrapContent2:SetRefreshFunc(function(oChild, dData)
	-- 		if dData then
	-- 			oChild:SetActive(true)
	-- 			if dData.chapterid > maxchapter then
	-- 				oChild.m_NameLabel:SetText("")
	-- 				oChild.m_LockSpr:SetActive(true)
	-- 				local dConfig = DataTools.GetChapterConfig(self.m_ChapterType, dData.chapterid, 1)
	-- 				local grade = 0
	-- 				for i,v in ipairs(dConfig.open_condition) do
	-- 					if string.find(v, "Level=") then
	-- 						for out in string.gmatch(v, "=(%w+)") do
	-- 							grade = tonumber(out)
	-- 							break
	-- 						end
	-- 					end
	-- 				end
	-- 				oChild.m_LockLabel:SetText(string.format("Level %s and passed corresponding Normal Mission", grade))
	-- 			else
	-- 				oChild.m_NameLabel:SetText(dData.chaptername)
	-- 				oChild.m_LockSpr:SetActive(false)
	-- 			end
	-- 			oChild:AddUIEvent("click", callback(self, "OnChildBox", dData.type, dData.chapterid))
	-- 		else	
	-- 			oChild:SetActive(false)
	-- 		end
	-- 	end)
	-- end
	-- local dChapterInfo = DataTools.GetChapterInfo(self.m_ChapterType)
	-- local list = {}
	-- local len = g_ChapterFuBenCtrl:GetMaxOpenChapter(self.m_ChapterType)
	-- for i=1,len do
	-- 	table.insert(list, dChapterInfo[i])
	-- end
	-- self.m_WrapContent:SetData(list, true)
	self:OnShowScrollView(false)
end

function CChapterFuBenSelectPart.OnChildBox(self, type, chapterid)
	if self.m_ParentView then
		self.m_ParentView:ForceChapterInfo(type, chapterid)
		self:OnShowScrollView(false)
	end
end

function CChapterFuBenSelectPart.OnShowScrollView(self, bShow)
	if self.m_ChapterType == 1 then
		self.m_ScrollView:SetActive(bShow)
	else
		self.m_ScrollView2:SetActive(bShow)
	end
	self.m_XiaLaBtn:SetActive(not bShow)
	self.m_ShangLaBtn:SetActive(bShow)
	self.m_BgMask:SetActive(bShow)
end

function CChapterFuBenSelectPart.OnChapterNameLabel(self)
	local bAct = false
	if self.m_ChapterType == 1 then
		bAct = self.m_ScrollView:GetActive()
	else
		bAct = self.m_ScrollView2:GetActive()
	end
	self:OnShowScrollView(not bAct)
end

--chaptertype  chế độ： bình thường/ khó khăn，默认 bình thường
function CChapterFuBenSelectPart.SetChapter(self, chapterid, chaptertype)
	local bRefresh = self.m_ChapterType ~= chaptertype
	self.m_ChapterType = chaptertype or 1
	self.m_ChapterID = chapterid
	local dChapterInfo = DataTools.GetChapterInfo(self.m_ChapterType, self.m_ChapterID)

	


	self.m_ChapterNameLabel:SetText(dChapterInfo.chaptername)
	if bRefresh then
		self:RefreshChaptertype()
		self:RefreshWrapContent()
	end
	if self.m_ChapterType == define.ChapterFuBen.Type.Simple then
		self.m_SimpleID = self.m_ChapterID
	elseif self.m_ChapterType == define.ChapterFuBen.Type.Difficult then
		self.m_DifficultID = self.m_ChapterID
	end
end

function CChapterFuBenSelectPart.RefreshChaptertype(self)
	if self.m_ChapterType == define.ChapterFuBen.Type.Simple then
		self.m_ChapterTypeSpr:SetSpriteName("pic_tubiao_putong")
		self.m_ChapterTypeLabel:SetText("Normal")
		if g_ChapterFuBenCtrl:IsOpenChapterDifficult() then
			self.m_SwitchBtn:SetSpriteName("pic_qiehuan_kunnan")
		else
			self.m_SwitchBtn:SetSpriteName("pic_qiehuan_kunnan_jinzhi")
		end
		self.m_SwitchSpr:SetSpriteName("pic_jinruputongmoshi")
		self.m_SwitchSpr.m_TweenAlpha.enabled = true
		self.m_SwitchSpr.m_TweenAlpha:ResetToBeginning()
		self.m_NewTipsSpr:SetActive(IOTools.GetClientData("chapter_difficult_new") == true)
		self.m_RedDotSpr:SetActive(g_ChapterFuBenCtrl:HasRedDotDifficult())
	elseif self.m_ChapterType == define.ChapterFuBen.Type.Difficult then
		self.m_ChapterTypeSpr:SetSpriteName("pic_tubiao_kunnan")
		self.m_ChapterTypeLabel:SetText("Hard")
		self.m_SwitchBtn:SetSpriteName("pic_qiehuan_putong")
		self.m_SwitchSpr:SetSpriteName("pic_jinrukunnanmoshi")
		self.m_SwitchSpr.m_TweenAlpha.enabled = true
		self.m_SwitchSpr.m_TweenAlpha:ResetToBeginning()
		IOTools.SetClientData("chapter_difficult_new", false)
		self.m_NewTipsSpr:SetActive(false)
		self.m_RedDotSpr:SetActive(g_ChapterFuBenCtrl:HasRedDotSimple())
	end
	self.m_ChapterTypeSpr:MakePixelPerfect()
end

function CChapterFuBenSelectPart.OnSwitchBtn(self, oBtn)
	if self.m_ChapterType == define.ChapterFuBen.Type.Simple then
		if g_ChapterFuBenCtrl:IsOpenChapterDifficult() then
			g_GuideCtrl:ReqTipsGuideFinish("chaterfb_switch_btn")
			if self.m_DifficultID then
				self.m_ParentView:ForceChapterInfo(define.ChapterFuBen.Type.Difficult, self.m_DifficultID)
			else
				self.m_ParentView:DefaultChapterInfo(define.ChapterFuBen.Type.Difficult)
			end
		else
			local dData = DataTools.GetChapterConfig(2,1,1)
			if dData then
				for i,v in ipairs(dData.open_condition) do
					if string.split(v, "Pass the chap") then
						local sArr = string.split(v, "=")
						g_NotifyCtrl:FloatMsg(string.format("Pass Mission %s", sArr[2]))
						return
					end
				end
			else
				g_NotifyCtrl:FloatMsg("The difficult type has not been unlocked yet")
			end
		end
	elseif self.m_ChapterType == define.ChapterFuBen.Type.Difficult then
		if self.m_SimpleID then
			self.m_ParentView:ForceChapterInfo(define.ChapterFuBen.Type.Simple, self.m_SimpleID)
		else
			self.m_ParentView:DefaultChapterInfo(define.ChapterFuBen.Type.Simple)
		end
	end
end

return CChapterFuBenSelectPart
