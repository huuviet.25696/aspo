local CEqualArenaWorldPrepareView = class("CEqualArenaWorldPrepareView", CViewBase)

function CEqualArenaWorldPrepareView.ctor(self, ob)
	CViewBase.ctor(self, "UI/Activity/EqualArena/EqualArenaPrepareView.prefab", ob)
	self.m_GroupName = "main"
	-- self.m_ExtendClose = "Black"
	-- self.m_OpenEffect = "Scale"
end

function CEqualArenaWorldPrepareView.OnCreateView(self)
	self.m_SelectPage = self:NewPage(1, CEqualArenaWorldSelectPage)
	self.m_CombinePage = self:NewPage(2, CEqualArenaWorldCombinePage)
	self:InitContent()
end

function CEqualArenaWorldPrepareView.InitContent(self)
	g_EqualArenaWorldCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnEqualEvent"))
end

function CEqualArenaWorldPrepareView.ShowSelectPage(self)
	self:ShowSubPage(self.m_SelectPage)
end

function CEqualArenaWorldPrepareView.ShowCombinePage(self)
	self:ShowSubPage(self.m_CombinePage)
end

function CEqualArenaWorldPrepareView.OnEqualEvent(self, oCtrl)
	if oCtrl.m_EventID == define.EqualArenaWorld.Event.OnCombineStart then
		self:ShowCombinePage()
		local oView = CNotifyView:GetView()
		if oView then
			oView:HideHint()
		end
	elseif oCtrl.m_EventID == define.EqualArenaWorld.Event.OnCloseEqualArenaUI then
		self:CloseView()
	end
end

return CEqualArenaWorldPrepareView
