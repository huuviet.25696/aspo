CEqualArenaWorldCtrl = require "logic.activity.equalarenaworld.CEqualArenaWorldCtrl"
CEqualArenaWorldHistoryView = require "logic.activity.equalarenaworld.CEqualArenaWorldHistoryView"
CEqualArenaWorldMainPage = require "logic.activity.equalarenaworld.CEqualArenaWorldMainPage"

CEqualArenaWorldWatchView = require "logic.activity.equalarenaworld.CEqualArenaWorldWatchView"
CEqualArenaWorldChangePartnerPart = require "logic.activity.equalarenaworld.CEqualArenaWorldChangePartnerPart"

CEqualArenaWorldPrepareView = require "logic.activity.equalarenaworld.CEqualArenaWorldPrepareView"
CEqualArenaWorldCombinePage = require "logic.activity.equalarenaworld.CEqualArenaWorldCombinePage"
CEqualArenaWorldSelectPage = require "logic.activity.equalarenaworld.CEqualArenaWorldSelectPage"

CEqualWorldRewardTopView = require "logic.activity.equalarenaworld.CEqualWorldRewardTopView"
CCountDownWorldBox = require "logic.activity.equalarenaworld.CCountDownWorldBox"
CEqualWorldRewardView = require "logic.activity.equalarenaworld.CEqualWorldRewardView"