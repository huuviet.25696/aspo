local CEqualArenaWorldCtrl = class("CEqualArenaWorldCtrl", CCtrlBase)

function CEqualArenaWorldCtrl.ctor(self, ob)
	CCtrlBase.ctor(self, ob)
	self.m_TimeStr = {
		{value = 1, desc = " second(s) ago"},
		{value = 60, desc = " minute(s) ago"},
		{value = 60, desc = " hour(s) ago"},
		{value = 24, desc = " day(s) ago"},
		{value = 30, desc = " month(s) ago"},
		{value = 999, desc = " year(s) ago"},
	}
	self:ResetCtrl()
end

function CEqualArenaWorldCtrl.ResetCtrl(self)
	self.m_ArenaPoint = 0
	self.m_WeekyMedal = 0
	self.m_ResultPoint = 0
	self.m_ResultMedal = 0
	self.m_Colorpoint = 0
	self.m_Currentcolorpoint = 0
	self.m_Result = define.EqualArenaWorld.WarResult.NotReceive
	self.m_LeftTime = 0
	self.m_EnemyInfo = {}
	self.m_PlayerInfo = {}
	self.m_UsedPartnerID = {}
	self.m_ViewSide = 0
	self.m_WaitingResult = false
end

function CEqualArenaWorldCtrl.GetGradeDataByPoint(self, point)
	for i,v in ipairs(data.equalarenaworlddata.SortId) do
		if point >= data.equalarenaworlddata.DATA[v].basescore then
			return data.equalarenaworlddata.DATA[v]
		end
	end
	return data.equalarenaworlddata.DATA[data.equalarenaworlddata.SortId[#data.equalarenaworlddata.SortId]]
end

function CEqualArenaWorldCtrl.GetSortIds(self)
	return data.equalarenaworlddata.SortId
end

function CEqualArenaWorldCtrl.GetArenaGradeData(self, id)
	return data.equalarenaworlddata.DATA[id]
end

function CEqualArenaWorldCtrl.ShowArena(self)
	if g_AttrCtrl.grade >= data.globalcontroldata.GLOBAL_CONTROL.equalarenaworld.open_grade then
		netarena.C2GSOpenEqualArenaWorld()
	else
		g_NotifyCtrl:FloatMsg(string.format("Pilot reaches level %d will open the feature", data.globalcontroldata.GLOBAL_CONTROL.equalarenaworld.open_grade))
	end
	-- self:OnShowArena(0,0,false, {0,0})
end

function CEqualArenaWorldCtrl.OnShowArena(self, point, weekyMedal, openWatch, parid, number_battles,max_number_battles)
	self.m_ArenaPoint = point
	self.m_WeekyMedal = weekyMedal
	self.m_OpenWatch = openWatch
	self.m_ParIds = parid or {}
	self.m_Number_battles = number_battles
	self.m_Max_number_battles = max_number_battles
	self:MarkUsedParter()
	local oView = CArenaView:GetView()
	if oView then
		oView:ShowEqualArenaWorldPage()
	else
		CArenaView:ShowView(function (oView)
			oView:ShowEqualArenaWorldPage()
		end)
	end
end

function CEqualArenaWorldCtrl.GetParByPos(self, pos)
	if self.m_ParIds[pos] then
		return g_PartnerCtrl:GetPartner(self.m_ParIds[pos])
	end
end

function CEqualArenaWorldCtrl.Match(self)
	-- if g_PartnerCtrl.m_AccountType == "f2p" then
	-- 	g_NotifyCtrl:FloatMsg("You're in F2P mode. Convert to P2E mode to use this feature?")
	-- 	return
	-- end
	if g_TeamCtrl:GetMemberSize() > 1 then
		g_NotifyCtrl:FloatMsg("In Team State, can't pair up!")
	else
		self.m_Result = define.EqualArenaWorld.WarResult.NotReceive
		netarena.C2GSEqualArenaWorldMatch()
	end
end

--匹配操作反馈
function CEqualArenaWorldCtrl.OnReceiveMatchResult(self, result)
	self:OnEvent(define.EqualArenaWorld.Event.ReceiveMatchResult, result)
end

--匹配成功对手信息
function CEqualArenaWorldCtrl.OnReceiveMatchPlayer(self, data)
	self.m_EnemyInfo = data
	self:OnEvent(define.EqualArenaWorld.Event.ReceiveMatchPlayer, data)
end

--获取观战数据
function CEqualArenaWorldCtrl.OpenWatch(self)
	netarena.C2GSEqualArenaWorldOpenWatch()
end

--打开观战界面
function CEqualArenaWorldCtrl.OnReceiveWatch(self, data)
	self.m_WatchInfo = {}
	for k,v in pairs(data) do
		self.m_WatchInfo[v.stage] = v
	end
	-- self:OnEvent(define.EqualArenaWorld.Event.OpenWatchPage)
	CEqualArenaWorldWatchView:ShowView()
end

--打开回放记录界面
function CEqualArenaWorldCtrl.GetArenaHistory(self)
	netarena.C2GSEqualArenaWorldHistory()
end

function CEqualArenaWorldCtrl.OnReceiveArenaHistory(self, historyInfo, historyOnShow)
	self.m_HistoryInfo = {}
	self.m_HistoryInfoSort = {}
	for k,v in pairs(historyInfo) do
		self.m_HistoryInfo[v.fid] = v
		self.m_HistoryInfoSort[k] = v.fid
	end

	self.m_ShowingHistory = historyOnShow
	self:OnEvent(define.EqualArenaWorld.Event.OpenReplay)
	CEqualArenaWorldHistoryView:ShowView()
end

function CEqualArenaWorldCtrl.OnReceiveFightResult(self, point, medal, result, currentpoint, weekyMedal, infoList, colorpoint, currentcolorpoint)
	-- printc(string.format("OnReceiveFightResult: %s point %s medal %s result", point, medal, result))
	self.m_ArenaPoint = currentpoint
	self.m_WeekyMedal = weekyMedal
	self.m_ResultPoint = point
	self.m_ResultMedal = medal
	self.m_Colorpoint = colorpoint
	self.m_Currentcolorpoint = currentcolorpoint
	self.m_PlayerInfo = nil
	self.m_EnemyInfo = nil

	for k,v in pairs(infoList) do
		if self.m_ViewSide ~= 0 and v.camp == self.m_ViewSide and self.m_PlayerInfo == nil then
			self.m_PlayerInfo = v
		elseif v.pid == g_AttrCtrl.pid and self.m_PlayerInfo == nil then
			self.m_PlayerInfo = v
		else
			self.m_EnemyInfo = v
		end
	end
	if self.m_PlayerInfo.camp == result then
		self.m_Result = define.EqualArenaWorld.WarResult.Win
	else
		self.m_Result = define.EqualArenaWorld.WarResult.Fail
	end

	if self.m_Result == define.EqualArenaWorld.WarResult.Win then
		-- self.m_ArenaPoint = self.m_ArenaPoint + self.m_ResultPoint
		--self.m_Currentcolorpoint = self.m_Currentcolorpoint 
	else
		-- self.m_ArenaPoint = self.m_ArenaPoint - self.m_ResultPoint
		--self.m_Currentcolorpoint = self.m_Currentcolorpoint 
	end
	self:OnEvent(define.EqualArenaWorld.Event.OnWarEnd)
end

function CEqualArenaWorldCtrl.OnReceiveSetShowing(self, fid)
	self.m_ShowingHistory = self.m_HistoryInfo[fid]
	self:OnEvent(define.EqualArenaWorld.Event.SetShowing, fid)
end

function CEqualArenaWorldCtrl.OnReceiveLeftTime(self, ileft)
	self.m_LeftTime = ileft + g_TimeCtrl:GetTimeS()
	self:OnEvent(define.EqualArenaWorld.Event.OnReceiveLeftTime)
end

function CEqualArenaWorldCtrl.GetLeftTimeText(self)
	local leftTime = self.m_LeftTime - g_TimeCtrl:GetTimeS()
	if leftTime <= 0 then
		return nil
	end
	local hour = math.modf(leftTime / 3600)
	local min = math.modf((leftTime % 3600) / 60)
	local sec = leftTime % 60
	return string.format("%02d:%02d:%02d", hour, min, sec)
end

function CEqualArenaWorldCtrl.ShowWarStartView(self, infoList)
	--结算用缓存

	local oPlayerInfo = nil
	local oEnemyInfo = nil
	if (self.m_ViewSide ~= 0 and infoList[2].camp == self.m_ViewSide) 
		or (infoList[2].pid == g_AttrCtrl.pid) then
		oPlayerInfo = infoList[2]
		oEnemyInfo = infoList[1]
	else
		oPlayerInfo = infoList[1]
		oEnemyInfo = infoList[2]
	end

	self.m_PlayerInfo = self:CopyStartInfo(oPlayerInfo)
	self.m_EnemyInfo = self:CopyStartInfo(oEnemyInfo)
	CArenaWarStartView:ShowView(function (oView)
		oView:SetData({oPlayerInfo, oEnemyInfo})
	end)
end

function CEqualArenaWorldCtrl.CopyStartInfo(self, oInfo)
	local dInfo = {
		name = oInfo.name,
		shape = oInfo.shape,
		pid = oInfo.pid,
	}
	return dInfo
end

--主界面替换伙伴
function CEqualArenaWorldCtrl.ChangePartner(self, tPos, tParid)
	self.m_ParIds[tPos] = tParid
	if table.count(self.m_ParIds) < 2 then
		netarena.C2GSSetEqualArenaWorldPartner({tParid})
	else
		netarena.C2GSSetEqualArenaWorldPartner(self.m_ParIds)
	end
end
--主界面替换伙伴
function CEqualArenaWorldCtrl.OnChangePartner(self, parids)
	self.m_ParIds = parids or {}
	self:MarkUsedParter()
	self:OnEvent(define.EqualArenaWorld.Event.OnChangePartner)
end

function CEqualArenaWorldCtrl.MarkUsedParter(self)
	self.m_UsedPartnerID = {}
	for k,v in pairs(self.m_ParIds) do
		self.m_UsedPartnerID[v] = true
	end
	-- table.print(self.m_UsedPartnerID, "MarkUsedParter----------------->")
end

function CEqualArenaWorldCtrl.IsPartnerUsed(self, parid)
	return self.m_UsedPartnerID[parid]
end

function CEqualArenaWorldCtrl.OnSelectSection(self, info, fuwen, partner, operater, limit_partner, limit_fuwen, limit_ban_partner, left_time)
	-- printc("OnSelectSection")
	self.m_WaitingResult = false
	self.m_PlayerInfos = {}
	self.m_SelectingPartnerList = {}
	self.m_SelectingEquipList = {}
	self.m_SelectedPartnerList = {}
	self.m_SelectedEquipList = {}
	self.m_SelectingPartnerCount = 0
	self.m_SelectingEquipCount = 0
	self.m_SelectingPartnerRecord = {}
	self.m_SelectingEquipRecord = {}
	for k,v in pairs(info) do
		self.m_SelectingPartnerRecord[v.info.pid] = {}
		self.m_SelectingEquipRecord[v.info.pid] = {}
		self.m_PlayerInfos[v.info.pid] = v
		if v.info.pid == operater then
			for iPos,parID in ipairs(v.select_par) do
				self.m_SelectingPartnerRecord[v.info.pid][iPos] = parID
				self.m_SelectingPartnerList[parID] = true
				self.m_SelectingPartnerCount = self.m_SelectingPartnerCount + 1
			end
			for iPos,equipID in ipairs(v.select_item) do
				self.m_SelectingEquipRecord[v.info.pid][iPos] = equipID
				self.m_SelectingEquipList[equipID] = true
				self.m_SelectingEquipCount = self.m_SelectingEquipCount + 1
			end
		end
		for _,parID in pairs(v.selected_partner) do
			self.m_SelectedPartnerList[parID] = true
		end
		for _,equipID in pairs(v.selected_fuwen) do
			self.m_SelectedEquipList[equipID] = true
		end
	end
	self.m_PartnerEquipList = fuwen

	self.m_PartnerList = {}
	for i,v in ipairs(partner) do
		self.m_PartnerList[i] = v
	end

	self.m_CurrentOperater = operater
	self.m_CurrentNeedPartner = limit_partner + limit_ban_partner
	self.m_BanPartner = limit_ban_partner
	self.m_CurrentNeedEquip = limit_fuwen
	self.m_OverTime = g_TimeCtrl:GetTimeS() + left_time

	local viewObj = CEqualArenaWorldPrepareView:GetView()
	self:OnEvent(define.EqualArenaWorld.Event.OnSelectSection)
	
	if not viewObj then
		CEqualArenaWorldPrepareView:ShowView(function (oView)
			oView:ShowSelectPage()
		end)
	end
end

function CEqualArenaWorldCtrl.GetRestSelectTime(self)
	return self.m_OverTime - g_TimeCtrl:GetTimeS()
end

function CEqualArenaWorldCtrl.IsMyTurn(self)
	return self.m_CurrentOperater == g_AttrCtrl.pid
end

function CEqualArenaWorldCtrl.SetSelecting(self, index, itemType)
	if self.m_WaitingResult then
		-- g_NotifyCtrl:FloatMsg("操作过快")
		return
	end
	-- printc(string.format("SetSelecting index = %s, itemType = %s", index, itemType))
	-- table.print(self.m_SelectedEquipList, "self.m_SelectedEquipList")
	if g_EqualArenaWorldCtrl:IsMyTurn() then
		if itemType == define.EqualArenaWorld.SelectingType.Partner then
			if self.m_SelectedPartnerList[index] then
				g_NotifyCtrl:FloatMsg("Can not select duplicates")
				return
			end
			if self.m_SelectingPartnerList[index] then
				if g_NetCtrl:IsValidSession(netdefines.C2GS_BY_NAME["C2GSWorldSyncSelectInfo"]) then
					self.m_WaitingResult = true
					netarena.C2GSWorldSyncSelectInfo(itemType, index, define.EqualArenaWorld.MarkType.None)
				end
			else
				if self.m_SelectingPartnerCount < self.m_CurrentNeedPartner then
					if g_NetCtrl:IsValidSession(netdefines.C2GS_BY_NAME["C2GSWorldSyncSelectInfo"]) then
						self.m_WaitingResult = true
						netarena.C2GSWorldSyncSelectInfo(itemType, index, define.EqualArenaWorld.MarkType.Selecting)
					end
				else
					g_NotifyCtrl:FloatMsg("You've reached the maximum selection")
				end
			end
		elseif itemType == define.EqualArenaWorld.SelectingType.Equip then
			if self.m_SelectedEquipList[index] then
				g_NotifyCtrl:FloatMsg("Can not select duplicates")
				return
			end
			if self.m_SelectingEquipList[index] then
				if g_NetCtrl:IsValidSession(netdefines.C2GS_BY_NAME["C2GSWorldSyncSelectInfo"]) then
					self.m_WaitingResult = true
					netarena.C2GSWorldSyncSelectInfo(itemType, index, define.EqualArenaWorld.MarkType.None)
				end
			else
				if self.m_SelectingEquipCount < self.m_CurrentNeedEquip then
					if g_NetCtrl:IsValidSession(netdefines.C2GS_BY_NAME["C2GSWorldSyncSelectInfo"]) then
						self.m_WaitingResult = true
						netarena.C2GSWorldSyncSelectInfo(itemType, index, define.EqualArenaWorld.MarkType.Selecting)
					end
				else
					g_NotifyCtrl:FloatMsg("You've reached the maximum selection")
				end
			end
		end
	else
		g_NotifyCtrl:FloatMsg("It's not your turn yet")
	end
end

function CEqualArenaWorldCtrl.OnSetSelecting(self, operater, select_type, index, handle_type)
	-- printc(string.format("OnSetSelecting operater = %s, select_type = %s, index = %s, handle_type = %s", operater, select_type, index, handle_type))
	if select_type == define.EqualArenaWorld.SelectingType.Partner then
		if handle_type == define.EqualArenaWorld.MarkType.None then
			self.m_SelectingPartnerList[index] = nil
			self.m_SelectingPartnerCount = self.m_SelectingPartnerCount - 1
			for i,v in ipairs(self.m_SelectingPartnerRecord[operater]) do
				if v == index then
					table.remove(self.m_SelectingPartnerRecord[operater], i)
					break
				end
			end
		else
			table.insert(self.m_SelectingPartnerRecord[operater], index)
			self.m_SelectingPartnerCount = self.m_SelectingPartnerCount + 1
			self.m_SelectingPartnerList[index] = true
		end
	elseif select_type == define.EqualArenaWorld.SelectingType.Equip then
		if handle_type == define.EqualArenaWorld.MarkType.None then
			self.m_SelectingEquipList[index] = nil
			for i,v in ipairs(self.m_SelectingEquipRecord[operater]) do
				if v == index then
					table.remove(self.m_SelectingEquipRecord[operater], i)
					break
				end
			end
			self.m_SelectingEquipCount = self.m_SelectingEquipCount - 1
		else
			table.insert(self.m_SelectingEquipRecord[operater], index)
			self.m_SelectingEquipCount = self.m_SelectingEquipCount + 1
			self.m_SelectingEquipList[index] = true
		end
	end
	self.m_WaitingResult = false
	self:OnEvent(define.EqualArenaWorld.Event.OnSetSelecting, {pid = operater, selectType = select_type, idx = index, handleType = handle_type})
end

function CEqualArenaWorldCtrl.SubmitSelecting(self)
	if not self:IsMyTurn() then
		g_NotifyCtrl:FloatMsg("Please wait for the opponent to choose")
		return
	end
	local selectPar = {}
	local selectEquip = {}
	for k,v in pairs(self.m_SelectingPartnerList) do
		if v then
			table.insert(selectPar, k)
		end
	end
	for k,v in pairs(self.m_SelectingEquipList) do
		if v then
			table.insert(selectEquip, k)
		end
	end

	if #selectPar < self.m_CurrentNeedPartner or #selectEquip < self.m_CurrentNeedEquip then
		local msgStr = "Have not chosen yet, randomly select item"
		local t = {
			msg = msgStr,
			okStr = "Yes",
			cancelStr = "No",
			okCallback = callback(self, "RandomSelect", selectPar, selectEquip),
		}
		g_WindowTipCtrl:SetWindowConfirm(t)
	else
		netarena.C2GSSelectEqualArenaWorld(selectPar, selectEquip)
	end
end

function CEqualArenaWorldCtrl.RandomSelect(self, selectPar, selectEquip)
	local parList = self:GetRandomList(selectPar, self.m_CurrentNeedPartner)
	local equipList = self:GetRandomList(selectEquip, self.m_CurrentNeedEquip)
	netarena.C2GSSelectEqualArenaWorld(parList, equipList)
end

function CEqualArenaWorldCtrl.GetRandomList(self, list, count)
	local bRepeat = false
	while (#list < count) do
		local idx = Utils.RandomInt(1, 8)
		bRepeat = false
		for k,v in pairs(list) do
			if v == idx then
				bRepeat = true
			end
		end
		if not bRepeat then
			table.insert(list, idx)
		end
	end
	return list
end

function CEqualArenaWorldCtrl.OnCombineStart(self, pInfo, leftTime)
	self.m_CombineInfo = {}
	for k,v in pairs(pInfo) do
		self.m_CombineInfo[v.info.pid] = v
	end
	self.m_CombineOverTime = g_TimeCtrl:GetTimeS() + leftTime
	if CEqualArenaWorldPrepareView:GetView() then
		self:OnEvent(define.EqualArenaWorld.Event.OnCombineStart)
	else
		CEqualArenaWorldPrepareView:ShowView(function (oView)
			oView:ShowCombinePage()
		end)
	end
end

function CEqualArenaWorldCtrl.OnCombineSubmit(self, selectPar, selectItem)
	self:OnEvent(define.EqualArenaWorld.Event.OnCombineSubmit)
end

function CEqualArenaWorldCtrl.GetRestCombineTime(self)
	return self.m_CombineOverTime - g_TimeCtrl:GetTimeS()
end

function CEqualArenaWorldCtrl.OnCombineDone(self, pid)
	self:OnEvent(define.EqualArenaWorld.Event.OnCombineDone, pid)
end

function CEqualArenaWorldCtrl.ShowWarResult(self, oCmd)
	if CArenaWarResultView:GetView() == nil then
		CArenaWarResultView:ShowView()
	end
end
return CEqualArenaWorldCtrl