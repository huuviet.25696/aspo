local CArenaWarResultView = class("CArenaWarResultView", CViewBase)

function CArenaWarResultView.ctor(self, ob)
	CViewBase.ctor(self, "UI/Arena/ArenaWarResultView.prefab", ob)
	self.m_ExtendClose = "Black"
end

function CArenaWarResultView.OnCreateView(self)
	self.m_PointLabel = self:NewUI(1, CLabel)
	self.m_WinMark = self:NewUI(2, CTexture)
	self.m_LoseMark = self:NewUI(3, CTexture)
	self.m_RightInfoBox = self:NewUI(4, CBox)
	self.m_LeftInfoBox = self:NewUI(5, CBox)
	self.m_ScorePart = self:NewUI(6, CBox)
	self.m_PointASPOLabel = self:NewUI(7, CLabel)
	self.m_WinEffect = CEffect.New("Effect/UI/ui_eff_1159/Prefabs/ui_eff_1159_shengli.prefab", self:GetLayer(), false)
	self.m_WinEffect:SetParent(self.m_WinMark.m_Transform)
	self.m_FailEffect = CEffect.New("Effect/UI/ui_eff_1159/Prefabs/ui_eff_1159_shibai.prefab", self:GetLayer(), false)
	self.m_FailEffect:SetParent(self.m_LoseMark.m_Transform)
	self.m_WinEffect:SetLocalPos(Vector3.New(0, 0, 0))
	self.m_FailEffect:SetLocalPos(Vector3.New(0, 0, 0))

	self:InitContent()
end
function CArenaWarResultView.InitContent(self)
	self.m_Ctrl = nil
	if g_WarCtrl:GetWarType() == define.War.Type.EqualArena then
		self.m_Ctrl = g_EqualArenaCtrl
	elseif g_WarCtrl:GetWarType() == define.War.Type.EqualArenaWorld then
		self.m_Ctrl = g_EqualArenaWorldCtrl
	elseif g_WarCtrl:GetWarType() == define.War.Type.Arena then
		self.m_Ctrl = g_ArenaCtrl
	end
	
	self:InitInfoBox(self.m_LeftInfoBox)
	self:InitInfoBox(self.m_RightInfoBox)
	self.m_Ctrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnNotify"))
	self:SetData()
	g_WarCtrl:AddCtrlEvent(self:GetInstanceID(), callback(self, "OnWarEvnet"))
	netopenui.C2GSOpenInterface(define.OpenInterfaceType.WarResult)
end

function CArenaWarResultView.InitInfoBox(self, oInfoBox)
	oInfoBox.m_WinBg = oInfoBox:NewUI(1, CTexture)
	oInfoBox.m_AvatarTexture = oInfoBox:NewUI(2, CTexture)
	oInfoBox.m_LoseBg = oInfoBox:NewUI(3, CTexture)
	oInfoBox.m_NameLabel = oInfoBox:NewUI(4, CLabel)
	oInfoBox.m_LoseNameLabel = oInfoBox:NewUI(5, CLabel)
	oInfoBox.m_ParentView = self

	function oInfoBox.SetData(self, oData, isWinner)
		oInfoBox.m_ParentView:SetTexture(oInfoBox.m_AvatarTexture, oData.shape)
		oInfoBox.m_NameLabel:SetText(oData.name)
		oInfoBox.m_LoseNameLabel:SetText(oData.name)
		oInfoBox.m_WinBg:SetActive(isWinner)
		oInfoBox.m_LoseBg:SetActive(not isWinner)
		oInfoBox.m_AvatarTexture:SetGrey(not isWinner)
	end
end

function CArenaWarResultView.OnShowView(self)
	if self.m_Ctrl.m_Result == define.Arena.WarResult.NotReceive then
		self:SetActive(false)
	end
end

function CArenaWarResultView.SetData(self)
	self.m_IsPlayRecord = self.m_Ctrl.m_ViewSide ~= 0
	self.m_ScorePart:SetActive(not self.m_IsPlayRecord)
	local isWinner = false
	-- local strTemp = "[FFFAAF]Arena Point %d[ff311c](-%d)[-],[00FF00]%d[-] Honor, Honor this week %d/%d"
	local strTemp = "[FFFAAF]Point Archived %d[ff311c](-%d)[-]"
	local strASPOTemp = "[FFFAAF]#w2 %d[ff311c](-%d)"
	local strMedal = "[FFFAAF]#w4 %s[00cc00](+%s)"
	if self.m_Ctrl.m_Result == define.Arena.WarResult.NotReceive then
		self:SetActive(false)
		return
	elseif self.m_Ctrl.m_Result == define.Arena.WarResult.Win then
		isWinner = true
		-- strTemp = "[FFFAAF]Arena Point %d[00cc00](+%d)[-],[00FF00]%d[-] Honor, Honor this week %d/%d"
		strTemp = "[FFFAAF]Point Archived  %d[00cc00](+%d)[-]"
		strASPOTemp = "[FFFAAF]#w2 %s[00cc00](+%s)"
	else

	end
	self.m_WinMark:SetActive(isWinner)
	self.m_LoseMark:SetActive(not isWinner)
	--printDebug("CArenaWarResultView",self.m_Currentcolorpoint)
	-- self.m_PointLabel:SetText(string.format(strTemp, self.m_Ctrl.m_ArenaPoint, self.m_Ctrl.m_ResultPoint, self.m_Ctrl.m_ResultMedal, self.m_Ctrl.m_WeekyMedal, self.m_Ctrl:GetGradeDataByPoint(self.m_Ctrl.m_ArenaPoint).weeky_limit))
	self.m_PointLabel:SetText(string.format(strTemp, self.m_Ctrl.m_ArenaPoint, self.m_Ctrl.m_ResultPoint))
	if self.m_Ctrl.m_Currentcolorpoint then
		self.m_PointASPOLabel:SetActive(true)
		self.m_PointASPOLabel:SetText(string.format(strASPOTemp, tostring( self.m_Ctrl.m_Currentcolorpoint/10000), tostring( self.m_Ctrl.m_Colorpoint/10000)))
	else
		self.m_PointASPOLabel:SetActive(false)
	end
	if g_WarCtrl:GetWarType() == define.War.Type.EqualArenaWorld then
		self.m_PointASPOLabel:SetActive(true)
		self.m_PointASPOLabel:SetText(string.format(strMedal, tostring( g_AttrCtrl.arenamedal+self.m_Ctrl.m_ResultMedal), tostring( self.m_Ctrl.m_ResultMedal)))
	end
	self.m_LoadCount = 0
	self.m_LeftInfoBox:SetData(self.m_Ctrl.m_PlayerInfo, isWinner)
	self.m_RightInfoBox:SetData(self.m_Ctrl.m_EnemyInfo, false)
end

function CArenaWarResultView.SetTexture(self, oTexture, shape)
	oTexture:LoadArenaPhoto(shape, callback(self, "AfterLoadPhoto"))
end

function CArenaWarResultView.AfterLoadPhoto(self)
	self.m_LoadCount = self.m_LoadCount + 1
	if self.m_LoadCount >= 2 then
		self:SetActive(true)
	end
end

function CArenaWarResultView.Destroy(self)
	g_ViewCtrl:CloseInterface(define.OpenInterfaceType.WarResult)
	if not self.m_IsPlayRecord then
		g_MainMenuCtrl:SetMainViewCallback(function ()
			self.m_Ctrl:ShowArena()
		end)
	end
	CViewBase.Destroy(self)
end

function CArenaWarResultView.OnNotify(self, oCtrl)
	if oCtrl.m_EventID == define.Arena.Event.OnWarEnd or oCtrl.m_EventID == define.EqualArena.Event.OnWarEnd or oCtrl.m_EventID == define.EqualArenaWorld.Event.OnWarEnd then
		self:SetData()
	end
end

function CArenaWarResultView.CloseView(self)
	g_WarCtrl:SetInResult(false)
end

function CArenaWarResultView.OnWarEvnet(self, oCtrl)
	if oCtrl.m_EventID == define.War.Event.EndWar then
		CViewBase.CloseView(self)
	end
end

return CArenaWarResultView
