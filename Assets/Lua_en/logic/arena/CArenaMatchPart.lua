local CArenaMatchPart = class("CArenaMatchPart", CBox)

function CArenaMatchPart.ctor(self, cb)
	CBox.ctor(self, cb)
	self.m_MatchingPart = self:NewUI(1, CBox)
	self.m_CancelBtn = self:NewUI(2, CButton)
	self.m_ResultPart = self:NewUI(3, CBox)
	self.m_AvatarSprite = self:NewUI(4, CSprite)
	self.m_NameLabel = self:NewUI(5, CLabel)
	self.m_PointLabel = self:NewUI(6, CLabel)
	self.m_InfoLabel = self:NewUI(7, CLabel)
	--Successfully paired, immediately entered the combat

	self.m_CancelBtn:AddUIEvent("click", callback(self, "OnClickCancel"))
	self.m_timecountdow= 60
	self.m_timeCountdowHandle = nil
	self.isEqualWorld = false

end


function CArenaMatchPart.ShowMatching(self, isArena , isEqualWorld)
	self.m_IsArena = isArena
	self.isEqualWorld = isEqualWorld
	self:SetActive(true)
	self.m_MatchingPart:SetActive(true)
	self.m_ResultPart:SetActive(false)
	--printDebug("CArenaMatchPart.ShowMatching",isArena)
	if isArena == false then
		self.m_timecountdow= 60
		self.m_InfoLabel:SetText(string.format("Finding match %ss",self.m_timecountdow))
		self.m_timeCountdowHandle = Utils.AddTimer(callback(self, "Update"), 1, 0)
	end
end
function CArenaMatchPart.Update(self)
	self.m_timecountdow = self.m_timecountdow-1
	--printDebug("CArenaMatchPart.Update",self.m_timecountdow)
	self.m_InfoLabel:SetText(string.format("Finding match %ss",self.m_timecountdow))
	if self.m_timecountdow <= 0 then
		self:OnClickCancel()
	else
		self.m_timeCountdowHandle = Utils.AddTimer(callback(self, "Update"), 1, 1)
	end
end
function CArenaMatchPart.ShowResult(self, data)
	self:SetActive(true)
	self.m_MatchingPart:SetActive(false)
	self.m_ResultPart:SetActive(true)
	self.m_AvatarSprite:SetSpriteName(tostring(data.shape))
	self.m_NameLabel:SetText(data.name)
	self.m_PointLabel:SetText(data.point)
	if self.m_timeCountdowHandle ~= nil then
		Utils.DelTimer(self.m_timeCountdowHandle)
	end
end

function CArenaMatchPart.OnClickCancel(self)
	--发送取消请求
	if self.m_IsArena then
		netarena.C2GSArenaCancelMatch()
	else
		if self.isEqualWorld then
			netarena.C2GSEqualArenaWorldCancelMatch()
		else
			netarena.C2GSEqualArenaCancelMatch()
		end
	end
	-- self:SetActive(false)
	if self.m_timeCountdowHandle ~= nil then
		Utils.DelTimer(self.m_timeCountdowHandle)
	end
end

return CArenaMatchPart
