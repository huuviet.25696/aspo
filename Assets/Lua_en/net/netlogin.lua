module(..., package.seeall)

--GS2C--

function GS2CHello(pbdata)
	local time = pbdata.time
	--todo
	g_LoginCtrl:OnServerHello()
	g_TimeCtrl:ServerHelloTime(time)
end

function GS2CLoginError(pbdata)
	local pid = pbdata.pid
	local errcode = pbdata.errcode --1001  in_login 1002 in_logout
	--todo
	g_LoginCtrl:HideLoginTips()
	if errcode == 0 then
		return
	elseif errcode == 1 then
		g_NotifyCtrl:FloatMsg("Login failed")
	elseif errcode == 1001 then
		g_NotifyCtrl:FloatMsg("Login…")
	elseif errcode == 1002 then
		g_NotifyCtrl:FloatMsg("It's offline")
		g_NetCtrl:ReconnectConfirm()
	elseif errcode == 1003 then
		g_NotifyCtrl:FloatMsg("The character does not exist")
	elseif errcode == 1004 then
		g_NotifyCtrl:FloatMsg("Character name already exists")
	elseif errcode == 1005 then
		g_NotifyCtrl:FloatMsg("Maintaining")
		g_NetCtrl:ReconnectConfirm()
	elseif errcode == 1006 then
		main.ResetGame()
		local args ={
			title = "Unusual login",
			msg = "Your account is already logged in on another device, do you want to sign in again?",
			okCallback = function() g_LoginCtrl:Logout() end,
			okStr = "Login again",
			cancelCallback = function() g_LoginCtrl:Logout() end,
			cancelStr = "Cancel",
			forceConfirm = true,
		}
		g_WindowTipCtrl:SetWindowConfirm(args)
	elseif errcode == 1008 then
		g_NotifyCtrl:FloatMsg("Character's account reached limit")
	elseif errcode == 1011 then
		g_LoginCtrl:InitValue()
		g_NotifyCtrl:FloatMsg("Account link is invalid, reconnecting...")
		if g_SdkCtrl:IsLogin() then
			g_SdkCtrl:Logout()
		else
			g_SdkCtrl:Login()
		end
	elseif errcode == 1012 then
		g_NotifyCtrl:FloatMsg("Character connection failed, reconnecting")
		g_LoginCtrl.m_LoginInfo["role_token"] = nil
		g_LoginCtrl.m_ReconnectInfo["role_token"] = nil
		g_LoginCtrl.m_LoginInfo["pid"] = nil
		g_LoginCtrl.m_ReconnectInfo["pid"] = nil
		g_LoginCtrl:Reconnect()
	elseif errcode == 1014 then
		g_NotifyCtrl:FloatMsg("Game version is out of date, please update the new one")
	elseif errcode == 1017 then
		g_NotifyCtrl:FloatMsg("Incorrect account or password, please re-enter!")
	elseif errcode == 1015 then
		main.ResetGame()
		local args ={
			title = "Unusual login",
			msg = "Login error, do you want to login again?",
			okCallback = function() g_LoginCtrl:Logout() end,
			okStr = "Login again",
			forceConfirm = true,
		}
		g_WindowTipCtrl:SetWindowConfirm(args)
	elseif errcode == 2003 then
		main.ResetGame()
		local args ={
			title = "Unusual login",
			msg = "Congratulations! Your account has successfully promoted to P2E, please login again to access additional features.\nPlease exit game and relogin to continue!",
			okCallback = function()
				Utils.ApplicationQuit()
			end,
			hideCancel = true,
		}
		g_WindowTipCtrl:SetWindowConfirm(args)
		
	elseif errcode == 2001 then
		main.ResetGame()
		local args ={
			title = "Unusual login",
			msg = "Your account has less than 4 Valarion",
			okCallback = function() g_LoginCtrl:Logout() end,
			okStr = "Login again",
			cancelCallback = function() g_LoginCtrl:Logout() end,
			cancelStr = "Cancel",
			forceConfirm = true,
		}
		g_WindowTipCtrl:SetWindowConfirm(args)
	elseif errcode == 2002 then
		main.ResetGame()
		local args ={
			title = "Unusual login",
			msg = "Can only switch to another game zone (server) one time per day. All will be reset at 00:00 UTC time.",
			okCallback = function() g_LoginCtrl:Logout() end,
			okStr = "Login again",
			cancelCallback = function() g_LoginCtrl:Logout() end,
			cancelStr = "Cancel",
			forceConfirm = true,
		}
		g_WindowTipCtrl:SetWindowConfirm(args)
	elseif errcode == 1009 then
		local args ={
			title = "Unusual login",
			msg = "There's an abnormal login, please check and try again",
			okCallback = function()
				g_LoginCtrl:Logout()
				local function delay()
					ASPO.DuoSDK.Instance:showLogin()
				end
				Utils.AddTimer(delay, 0, 0)
			end,
			okStr = "Login again",
			forceConfirm = true,
			hideCancel = true,
		}
		g_WindowTipCtrl:SetWindowConfirm(args)
	elseif errcode == 1013 then
		local args ={
			title = "Unusual login",
			msg = "Too much players, the server is being busy!",
			okCallback = function()
				g_LoginCtrl:Logout()
				local function delay()
					ASPO.DuoSDK.Instance:showLogin()
				end
				Utils.AddTimer(delay, 0, 0)
			end,
			okStr = "Login again",
			forceConfirm = true,
			hideCancel = true,
		}
		g_WindowTipCtrl:SetWindowConfirm(args)
	elseif errcode == 9999 then
		local args ={
			title = "Unusual login",
			msg = "The server do not open, please go back a few moment later!",
			okCallback = function()
				g_LoginCtrl:Logout()
				local function delay()
					ASPO.DuoSDK.Instance:showLogin()
				end
				Utils.AddTimer(delay, 0, 0)
			end,
			okStr = "Login again",
			forceConfirm = true,
			hideCancel = true,
		}
		g_WindowTipCtrl:SetWindowConfirm(args)
	else
		local args ={
			title = "Unusual login",
			msg = "Login error, response code:"..tostring(errcode),
			okCallback = function()
				g_LoginCtrl:Logout()
				local function delay()
					ASPO.DuoSDK.Instance:showLogin()
				end
				Utils.AddTimer(delay, 0, 0)
			end,
			okStr = "Login again",
			forceConfirm = true,
			hideCancel = true,
		}
		g_WindowTipCtrl:SetWindowConfirm(args)
	end
	print("Login err", pid, errcode)
end

function GS2CLoginErrorNotify(pbdata)
	local cmd = pbdata.cmd
	--todo
	local args ={
		title = "Notification",
		msg = cmd,
		okCallback = function() end,
		okStr = "Confirm",
		forceConfirm = true,
		hideCancel = true
	}
	g_LoginCtrl:HideLoginTips()
	g_WindowTipCtrl:SetWindowConfirm(args)
end

function GS2CLoginAccount(pbdata)
	local account = pbdata.account
	local role_list = pbdata.role_list
	local channel = pbdata.channel
	g_PartnerCtrl.m_AccountType = pbdata.account_type
	--todo
	g_LoginCtrl:LoginAccountSuccess(account, role_list)
end

function GS2CLoginRole(pbdata)
	local account = pbdata.account
	local pid = pbdata.pid
	local role = pbdata.role
	local role_token = pbdata.role_token
	local is_gm = pbdata.is_gm
	local channel = pbdata.channel
	local xg_account = pbdata.xg_account --信鸽注册帐号
	--printDebug("GS2CLoginRole",pbdata)
	g_MainMenuCtrl.m_FreePlay = pbdata.free_play
	g_MainMenuCtrl:RevertSuccess()
	--todo
	-- local xg_token = C_api.XinGeSdk.RegisterWithAccount(xg_account)
	-- netother.C2GSSendXGToken(xg_token)
	if is_gm == 1 then
		Utils.IsDevUser = function() return true end
		local oView = CNotifyView:GetView()
		if oView then
			oView.m_OrderBtn:SetActive(true)
		end
		Utils.UpdateLogLevel()
	end
	if C_api.Utils.GetUpdateMode() ~= enum.UpdateMode.Update then
		Utils.UpdateLogLevel()
	end
	if g_CreateRoleCtrl:IsInCreateRole() then
		g_MagicCtrl:Clear("createrole")
		g_MapCtrl:AddLoadDoneCb(function()
			local oCam = g_CameraCtrl:GetMainCamera()
			if oCam:GetActive() then
				CMainMenuView:ShowView()
			end
		end)
		g_CreateRoleCtrl:EndCreateRole()
	else
		CMainMenuView:ShowView()
		g_LoginCtrl:ShowLoginTips("Loading map")
	end
	g_LoginCtrl:UpdateLoginInfo({["token"]=g_LoginCtrl.m_VerifyInfo.token ,["account"]= account, ["role_token"]=role_token, ["pid"]=pid})
	local dAttr = {}
	if role then
		local dDecode = g_NetCtrl:DecodeMaskData(role, "role")
	table.update(dAttr, dDecode)
	end
	dAttr.pid = pid
	local sTitle = g_ServerCtrl:GetCurServerName().."Account:"..account
	Utils.SetWindowTitle(sTitle)
	g_AttrCtrl:ResetAll()
	g_AttrCtrl:UpdateAttr(dAttr)
	g_TimeCtrl:StartBeat()
	g_ChatCtrl:StartHelpTip()
	g_ChatCtrl:InitAudioFilter()
	g_TeamCtrl:InitTeamSetting()
	g_TitleCtrl:LoginInit()
	g_CameraCtrl:InitCtrl()
	g_LoginCtrl:LoginRoleSuccess(pid)
	g_ScheduleCtrl:AutoCheckRegionSchedule()
	g_WarCtrl.m_AnimSpeed = IOTools.GetRoleData("war_speed") or 3

	if Utils.IsAndroid() then
		--TODO C_api.TalkingData.SetupAccount(tostring(pid))
	end
	Utils.AddTimer(function() g_NotifyCtrl:HideConnect() end, 0, 0)
end

function GS2CCreateRole(pbdata)
	local account = pbdata.account
	local role = pbdata.role
	local channel = pbdata.channel
	--todo
	g_LoginCtrl:CreateRoleSuccess(account, role)
end

function GS2CCheckInviteCodeResult(pbdata)
	local result = pbdata.result --0为没有有效邀请码，1为有
	local msg = pbdata.msg
	--todo
	g_LoginCtrl:CheckInviteCodeResult(result, msg)
end

function GS2CSetInviteCodeResult(pbdata)
	local result = pbdata.result --0为没有有效邀请码，1为有
	local msg = pbdata.msg
	--todo
	g_LoginCtrl:SetInviteCodeResult(result, msg)
end

function GS2CQueryLogin(pbdata)
	local delete_file = pbdata.delete_file --删除的导表资源文件名字
	local res_file = pbdata.res_file --新增或者改变的资源文件信息
	local code = pbdata.code --客户端在线更新代码
	--todo
	print("netlogin.GS2CQueryLogin-->")
	DataTools.UpdateData(delete_file, res_file)
	g_LoginCtrl:OnDataUpdateFinished()
	Utils.UpdateCode(code)
end


--C2GS--

function C2GSLoginAccount(account,pwd, token, device, platform, mac, client_svn_version, client_version, os, udid, imei, is_qrcode)
	local t = {
		account = account,
		password= pwd,
		token = token,
		device = device,
		platform = platform,
		mac = mac,
		client_svn_version = client_svn_version,
		client_version = client_version,
		os = os,
		udid = udid,
		imei = imei,
		is_qrcode = is_qrcode,
	}

	--printDebug("login ne",t)
	g_NetCtrl:Send("login", "C2GSLoginAccount", t)
end

function C2GSLoginRole(account, pid, scene_model)
	local t = {
		account = account,
		pid = pid,
		scene_model = scene_model,
	}
	g_NetCtrl:Send("login", "C2GSLoginRole", t)
end

function C2GSCreateRole(account, role_type, name)
	local t = {
		account = account,
		role_type = role_type,
		name = name,
	}
	g_NetCtrl:Send("login", "C2GSCreateRole", t)
end

function C2GSReLoginRole(pid, role_token, scene_model)
	local t = {
		pid = pid,
		role_token = role_token,
		scene_model = scene_model,
	}
	g_NetCtrl:Send("login", "C2GSReLoginRole", t)
end

function C2GSSetInviteCode(invitecode)
	local t = {
		invitecode = invitecode,
	}
	g_NetCtrl:Send("login", "C2GSSetInviteCode", t)
end

function C2GSLogoutByOneKey()
	local t = {
	}
	g_NetCtrl:Send("login", "C2GSLogoutByOneKey", t)
end

function C2GSChangeAccount()
	local t = {
	}
	g_NetCtrl:Send("login", "C2GSChangeAccount", t)
end

function C2GSQueryLogin(res_file_version)
	local t = {
		res_file_version = res_file_version,
	}
	--printDebug(res_file_version)
	g_NetCtrl:Send("login", "C2GSQueryLogin", t)
end

