﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Runtime.InteropServices;
using System;
using LuaInterface;

public class DuoSDK : MonoBehaviour
{
    //#if !UNITY_EDITOR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
#if (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
    const string LUADLL = "idsdk";

    [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
    public delegate int loginSuccessCallback();

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void setLoginSuccessCallback(IntPtr func);

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void nftsdk_init();

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void nftsdk_requestPairing();

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void nftsdk_changeDefaultServer(string url);

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void nftsdk_showPairing();

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void idsdk_showLoginNemo();

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void idsdk_hideLogin();

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern IntPtr nftsdk_getUserName();

    //-------------------------------------------------------//
    //[DllImport(LUADLL, CharSet = CharSet.Unicode, CallingConvention = CallingConvention.Cdecl)]
    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void idsdk_init(string appId);

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void idsdk_changeDefaultServer(string url);

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void idsdk_changeProxyServer(string url);

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void idsdk_dynamicBuyPackage(string jsonPackage);

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void idsdk_addIAPNemoProductIdentifier(string productId);

    private static loginSuccessCallback _loginSuccessCallback;
#endif
#if !UNITY_EDITOR && UNITY_IOS
    const string LUADLL = "__Internal";
    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern int showLoginIOS();

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void buyPackage(string jsonPackage);

    [DllImport(LUADLL, CallingConvention = CallingConvention.Cdecl)]
    public static extern void callAFTracking(string jsonContent);
    
#endif
    //DuoSDK
    public LuaFunction luaSdkLoginSuccess;
    public LuaFunction luaSdkLoginFail;
    public LuaFunction luaSdkRegisterSuccess;
    public LuaFunction luaSdkRegisterFail;
    public LuaFunction luaSdkLogout;
    public LuaFunction luaSdkPostSuccess;
    public LuaFunction luaSdkPostFail;
    public LuaFunction luaSdkGetSuccess;
    public LuaFunction luaSdkGetFail;

    public static Int64 active_voice_channel = 0;
    

    public static DuoSDK Instance
    {
        private set;
        get;
    }

    public static void CreateInstance()
    {
        if (Instance != null)
        {
            Debug.LogError("DuoSDK.Instance already exist");
            return;
        }

        GameObject go = new GameObject("DuoSDK");
        GameObject.DontDestroyOnLoad(go);
        Instance = go.AddComponent<DuoSDK>();
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    //[AOT.MonoPInvokeCallback (typeof(AceCallback))] instead of [MonoPInvokeCallback(typeof(AceCallback))] fixed the compilation error.
        //#if !UNITY_EDITOR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
#if (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
    [AOT.MonoPInvokeCallback(typeof(loginSuccessCallback))]
    public static int onLoginCallbackSuccess()
    {
        idsdk_hideLogin();
        IntPtr ptr = nftsdk_getUserName();
        string username = PtrToStringUtf8(ptr);
        Instance.onLoginSuccess(username);
        return 0;
    }
#endif


    /// <summary>
    /// onLoginSuccess
    /// </summary>
    /// <param name="reason"></param>
    void onLoginSuccess(string reason)
    {
        if (luaSdkLoginSuccess != null)
        {
            luaSdkLoginSuccess.BeginPCall();
            luaSdkLoginSuccess.Push(reason);
            luaSdkLoginSuccess.PCall();
            luaSdkLoginSuccess.EndPCall();
        }
    }

    /// <summary>
    /// onLoginSuccess
    /// </summary>
    /// <param name="reason"></param>
    void onLoginFail(string reason)
    {
        if (luaSdkLoginFail != null)
        {
            luaSdkLoginFail.BeginPCall();
            luaSdkLoginFail.Push(reason);
            luaSdkLoginFail.PCall();
            luaSdkLoginFail.EndPCall();
        }
    }

    /// <summary>
    /// onLoginSuccess
    /// </summary>
    /// <param name="reason"></param>
    void onRegisterSuccess(string reason)
    {
        if (luaSdkRegisterSuccess != null)
        {
            luaSdkRegisterSuccess.BeginPCall();
            luaSdkRegisterSuccess.Push(reason);
            luaSdkRegisterSuccess.PCall();
            luaSdkRegisterSuccess.EndPCall();
        }
    }

    /// <summary>
    /// onLoginSuccess
    /// </summary>
    /// <param name="reason"></param>
    void onRegisterFail(string reason)
    {
        if (luaSdkLoginSuccess != null)
        {
            luaSdkRegisterFail.BeginPCall();
            luaSdkRegisterFail.Push(reason);
            luaSdkRegisterFail.PCall();
            luaSdkRegisterFail.EndPCall();
        }
    }

    /// <summary>
    /// onLoginSuccess
    /// </summary>
    /// <param name="reason"></param>
    void onLogoutCallback(string reason)
    {
        if (luaSdkLogout != null)
        {
            luaSdkLogout.BeginPCall();
            luaSdkLogout.Push(reason);
            luaSdkLogout.PCall();
            luaSdkLogout.EndPCall();
        }
    }
    void onPostSuccess(string reason)
    {
        if (luaSdkPostSuccess != null)
        {
            luaSdkPostSuccess.BeginPCall();
            luaSdkPostSuccess.Push(reason);
            luaSdkPostSuccess.PCall();
            luaSdkPostSuccess.EndPCall();
        }
    }
    void onPostFail(string reason)
    {
        if (luaSdkPostFail != null)
        {
            luaSdkPostFail.BeginPCall();
            luaSdkPostFail.Push(reason);
            luaSdkPostFail.PCall();
            luaSdkPostFail.EndPCall();
        }
    }
    void onGetSuccess(string reason)
    {
        if (luaSdkGetSuccess != null)
        {
            luaSdkGetSuccess.BeginPCall();
            luaSdkGetSuccess.Push(reason);
            luaSdkGetSuccess.PCall();
            luaSdkGetSuccess.EndPCall();
        }
    }
    void onGetFail(string reason)
    {
        if (luaSdkGetFail != null)
        {
            luaSdkGetFail.BeginPCall();
            luaSdkGetFail.Push(reason);
            luaSdkGetFail.PCall();
            luaSdkGetFail.EndPCall();
        }
    }

    //https://stackoverflow.com/questions/27550280/unity-call-android-function-from-unity

    private void callAndroidMethod(string method, string data="")
    {
        using (AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            using (AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                //obj_Activity.Call(method, new object[] { data });
                //obj_Activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                //{
                //    if (data != "")
                //    {
                //        obj_Activity.Call(method, new object[] { data });
                //    }
                //    else
                //    {
                //        obj_Activity.Call(method);
                //    }
                //}));
                if (data != "")
                {
                    obj_Activity.Call(method, new object[] { data });
                }
                else
                {
                    obj_Activity.Call(method);
                }
            }
        }
        //AndroidJavaClass myClass = new AndroidJavaClass("com.companyName.productName.MyClass");

        //myClass.Call(method, new object[] { data });
    }
    public void initSDK(string appId, string server)
    {
        //GameDebug.LogError("initSDK:"+ appId +"," + server);
        //#if !UNITY_EDITOR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
#if (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
        idsdk_init(appId);
        idsdk_changeDefaultServer(server);
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
        string msg = "{\"app_id\":\"" + appId + "\",\"server\":\"" + server + "\"}";
        callAndroidMethod("initSDK",msg);
        return;
#endif
#if !UNITY_EDITOR && UNITY_IOS
        initSDK(appId,server);
        return;
#endif
    }
    public void addNemoPackage(string productId)
    {
        //#if !UNITY_EDITOR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
#if (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
        idsdk_addIAPNemoProductIdentifier(productId);
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
        callAndroidMethod("addIAPNemoProductIdentifier",productId);
        return;
#endif
#if !UNITY_EDITOR && UNITY_IOS
        addIAPNemoProductIdentifier(productId);
        return;
#endif
    }
    public void showLogin()
    {
#if !UNITY_EDITOR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
//#if (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
        
        //if (GameLauncher.Instance.isTest)
        //{
        //    nftsdk_changeDefaultServer("https://proxy-dev.aspo.world");
        //}
        //else
        //{
        //    nftsdk_changeDefaultServer("https://proxy.aspo.world");
        //}
        //nftsdk_changeDefaultServer("https://proxy-dev.aspo.world");
        
        //idsdk_init();
        //idsdk_changeDefaultServer("https://id.oneteam.vn");

        _loginSuccessCallback = new loginSuccessCallback(onLoginCallbackSuccess);
        IntPtr fn = Marshal.GetFunctionPointerForDelegate(_loginSuccessCallback);
        setLoginSuccessCallback(fn);

        //nftsdk_showPairing();
        idsdk_showLoginNemo();
        return;
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
        callAndroidMethod("showLogin");
        return;
#endif
#if !UNITY_EDITOR && UNITY_IOS
        showLoginIOS();
        return;
#endif
        //#else
        onLoginSuccess(""); //bypass if not android

    }

    public void callPayment(string msg)
    {
        GameDebug.LogError("callPayment:" + msg);
        //#if !UNITY_EDITOR && (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
#if (UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX)
        idsdk_dynamicBuyPackage(msg);
        return;
#endif
#if !UNITY_EDITOR && UNITY_ANDROID
                callAndroidMethod("buyPackage",msg);
                return;
#endif
#if !UNITY_EDITOR && UNITY_IOS
                buyPackage(msg);
                return;
#endif
    }

    public void callAFTracking(string msg)
    {
        GameDebug.LogError("callAFTracking:" + msg);
#if !UNITY_EDITOR && UNITY_ANDROID
                callAndroidMethod("callAFTracking",msg);
                return;
#endif
#if !UNITY_EDITOR && UNITY_IOS
                callAFTracking(msg);
                return;
#endif
    }

    public void FUNCTAG_SetSdkLoginSuccessCallback(LuaFunction func)
    {
        if (luaSdkLoginSuccess != null)
        {
            luaSdkLoginSuccess.Dispose();
        }
        luaSdkLoginSuccess = func;
    }

    public void FUNCTAG_SetSdkLoginFailCallback(LuaFunction func)
    {
        if (luaSdkLoginFail != null)
        {
            luaSdkLoginFail.Dispose();
        }
        luaSdkLoginFail = func;
    }

    public void FUNCTAG_SetSdkRegisterSuccessCallback(LuaFunction func)
    {
        if (luaSdkRegisterSuccess != null)
        {
            luaSdkRegisterSuccess.Dispose();
        }
        luaSdkRegisterSuccess = func;
    }

    public void FUNCTAG_SetSdkRegisterFailCallback(LuaFunction func)
    {
        if (luaSdkRegisterFail != null)
        {
            luaSdkRegisterFail.Dispose();
        }
        luaSdkRegisterFail = func;
    }

    public void FUNCTAG_SetSdkLogoutCallback(LuaFunction func)
    {
        if (luaSdkLogout != null)
        {
            luaSdkLogout.Dispose();
        }
        luaSdkLogout = func;
    }

    public void FUNCTAG_SetSdkPostSuccessCallback(LuaFunction func)
    {
        if (luaSdkPostSuccess != null)
        {
            luaSdkPostSuccess.Dispose();
        }
        luaSdkPostSuccess = func;
    }

    public void FUNCTAG_SetSdkPostFailCallback(LuaFunction func)
    {
        if (luaSdkPostFail != null)
        {
            luaSdkPostFail.Dispose();
        }
        luaSdkPostFail = func;
    }

    public void FUNCTAG_SetSdkGetSuccessCallback(LuaFunction func)
    {
        if (luaSdkGetSuccess != null)
        {
            luaSdkGetSuccess.Dispose();
        }
        luaSdkGetSuccess = func;
    }

    public void FUNCTAG_SetSdkGetFailCallback(LuaFunction func)
    {
        if (luaSdkGetFail != null)
        {
            luaSdkGetFail.Dispose();
        }
        luaSdkGetFail = func;
    }

    private static string PtrToStringUtf8(IntPtr ptr) // aPtr is nul-terminated
    {
        if (ptr == IntPtr.Zero)
            return "";
        int len = 0;
        while (System.Runtime.InteropServices.Marshal.ReadByte(ptr, len) != 0)
            len++;
        if (len == 0)
            return "";
        byte[] array = new byte[len];
        System.Runtime.InteropServices.Marshal.Copy(ptr, array, 0, len);
        return System.Text.Encoding.UTF8.GetString(array);
    }
}