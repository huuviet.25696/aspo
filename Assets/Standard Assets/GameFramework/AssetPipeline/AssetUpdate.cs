using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using LITJson;
using Priority_Queue;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using System.Text.RegularExpressions;
using System.Net;


#if UNITY_EDITOR
using UnityEditor;
#endif

namespace AssetPipeline
{
    public class AssetUpdate : MonoBehaviour
    {
        public static AssetUpdate Instance;
        private static bool _isQuit = false;

        public static void CreateInstance()
        {
            if (Instance != null)
            {
                GameDebug.LogError("AssetUpdate.Instance already exist");
                return;
            }

            GameObject go = new GameObject("AssetUpdate");
            Instance = go.AddComponent<AssetUpdate>();
        }

        private const int MAX_RETRYCOUNT = 3;
        private const int MAX_DOWNLOADCOUNT = 5;
		
        private static bool _cleanUpResFlag;

        private Action<string> _logMessageHandler;
        private Action<string> _loadErrorHandler;

        private List<string> _cdnUrls;
        private string _cdnUrlRoot;
        private int _cdnUrlIndex;
        private CLASSTAG_VersionConfig _localVersionConfig;
        public CLASSTAG_VersionConfig _versionConfig
        {
            private set;
            get;
        }

        public CLASSTAG_DllVersion _packageDllVersion
        {
            private set;
            get;
        }

        public CLASSTAG_DllVersion _dllVersion
        {
            private set;
            get;
        }

        public CLASSTAG_ResConfig _packageResConfig
        {
            private set;
            get;
        }

        public CLASSTAG_ResConfig _curResConfig
        {
            private set;
            get;
        }

		public internalResConfig _internalResConfig
		{
			private set;
			get;
		}

        private CLASSTAG_ScriptVersion _scriptPatchVersion;
        private CLASSTAG_StaticConfig _staticConfig;
        public string _staticConfigData;
        private CLASSTAG_MiniResConfig _miniResConfig;

        private CLASSTAG_LuaScript _curScript = null;

        //非WIFI情况下,询问玩家是否更新标记
        //当为true时,代表玩家已确认在非WIFI情况下更新数据了
        private bool _requestUpdateFlag;

        public CLASSTAG_VersionConfig LocalVersionConfig
        {
			get
			{
				if (_localVersionConfig == null)
				{
					_localVersionConfig = ReadLocalVersionConfig();
					if (_localVersionConfig != null)
					{
						GameDebug.Log("Local_VersionConfig:\n" + _localVersionConfig.ToString());
					}
					else
					{
						GameDebug.Log("Local_VersionConfig is null");
					}
				}
				return _localVersionConfig;
			}
        }


        void Awake()
        {

        }

        void OnApplicationQuit()
        {
            _isQuit = true;
            //GameDebug.Log("AssetUpdate OnApplicationQuit");
            //如果玩家直接终止游戏进程是不会触发OnApplicationQuit,
            //所以在资源更新时,玩家杀掉游戏进程,会导致玩家需要重新下载所有更新数据
            Dispose();
        }

        private void OnApplicationPause(bool paused)
        {
            //退出到后台时，判断在热更则保存ResConfig
            if (paused && isDownloadAsset)
            {
                SaveResConfig();
                SaveMiniResConfig();
            }
        }

		#region Resource management initialization process

        public void Setup(List<string> cdnUrls)
        {
            GameDebug.Log("Setup Server links");
            _cdnUrls = cdnUrls;
            _cdnUrlRoot = cdnUrls[0];
            _fatalError = false;
            _requestUpdateFlag = false;
        }

        public void SetLogHanlder(Action<string> logHandler, Action<string> onError)
        {
            _logMessageHandler = logHandler;
            _loadErrorHandler = onError;
        }

        public void FUNCTAG_StartLoadPackageGameConfig(Action onFinish)
        {
            StartCoroutine(FUNCTAG_LoadPackageGameConfig(onFinish));
        }

        public void LoadStaticConfig(bool isTestHttpRoot, Action onFinish, Action<string> onError)
        {
			Action<string> staticJsonProcess = (string json) =>
			{
				string savePath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.STATICCONFIG_FILE);
				CLASSTAG_FileHelper.SaveJsonText(json, savePath, false);

				this._staticConfig = JsonMapper.ToObject<CLASSTAG_StaticConfig>(json);
				if (this._staticConfig != null)
				{
					this._staticConfigData = json;

					CLASSTAG_GameSetting.SetupServerUrlConfig(this._staticConfig);
					Setup(CLASSTAG_GameSetting.PlatformResPathList);
					onFinish();
				}
				else
				{
					onError("Failed to load game configuration information");
				}
			};

			if (GameLauncher.andriodUpdateInfo != null && GameLauncher.andriodUpdateInfo.cndUrls != null)
			{
				GameDebug.Log("android has already downloaded and won't get it again");
				staticJsonProcess(GameLauncher.andriodUpdateInfo.cndUrls);
				return;
			}


            //http://374.gsscdn.com/b77/release/ios
            if (isTestHttpRoot)
            {
                CLASSTAG_GameSetting.CONFIG_SERVER = string.Format("{0}/{1}/{2}", CLASSTAG_GameSetting.TestHttpRoot, CLASSTAG_GameSetting.ResDir, CLASSTAG_GameSetting.PlatformTypeName);
            }
            else
            {
                CLASSTAG_GameSetting.CONFIG_SERVER = string.Format("{0}/{1}/{2}", CLASSTAG_GameSetting.HttpRoot, CLASSTAG_GameSetting.ResDir, CLASSTAG_GameSetting.PlatformTypeName);
            }


            LoadStaticConfig(CLASSTAG_GameResPath.STATICCONFIG_FILE, json =>
            {
                if (!string.IsNullOrEmpty(json))
                {
					staticJsonProcess(json);
                }
                else
                {
						onError("Failed to load game configuration information");
                }
            }, onError);
        }
        public void LoadStaticConfig(string configName, Action<string> onLoadFinish, Action<string> onError)
        {
			 string url = string.Format("{0}/servers/{1}?ver={2}", CLASSTAG_GameSetting.CONFIG_SERVER, configName, DateTime.Now.Ticks.ToString());
            //http://374.gsscdn.com/b77/release/ios/servers/staticconfig.txt?ver=637549837222662570
            GameDebug.Log("LoadStaticConfig url= " + url);
            CLASSTAG_HttpController.Instance.DownLoad(
                url,
                delegate(CLASSTAG_ByteArray byteArray)
                    {
                        string json = byteArray.FUNCTAG_ToUTF8String();
                        onLoadFinish(json);
                    },
                null,
                delegate(Exception obj)
                    {
					        onError(string.Format("Error loading {0} , please try again", url));
                    },
                false,
                SimpleWWW.ConnectionType.Short_Connect);
        }

        public IEnumerator FUNCTAG_LoadPackageGameConfig(Action onFinish)
        {
            string srcUrl = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_packageResUrlRoot, CLASSTAG_GameResPath.RESCONFIG_FILE);
            string dstUrl = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.RESCONFIG_FILE);
            
            if (CLASSTAG_FileHelper.IsExist(dstUrl))
            {
                byte[] data = CLASSTAG_FileHelper.ReadAllBytes(dstUrl);
                _curResConfig = CLASSTAG_ResConfig.FUNCTAG_ReadFile(data, true);
            }
            if (_curResConfig == null)
            {
                using (var www = new WWW(srcUrl))
                {
                    yield return www;
                    if (!string.IsNullOrEmpty(www.error))
                    {
                        ThrowFatalException(string.Format("Load url:{0}\nerror:{1}", srcUrl, www.error));
                    }
                    _curResConfig = CLASSTAG_ResConfig.FUNCTAG_ReadFile(www.bytes, true);
                    if (_curResConfig == null)
                    {
						ThrowFatalException("The resource configuration information in the package is lost");
                        yield break;
                    }
                    CLASSTAG_FileHelper.WriteAllBytes(dstUrl, www.bytes);
                }
            }

            dstUrl = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.SCRIPT_FILE);
            srcUrl = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_packageResUrlRoot, CLASSTAG_GameResPath.SCRIPT_FILE);

            if (!CLASSTAG_FileHelper.IsExist(dstUrl))
            {
                using (var www = new WWW(srcUrl))
                {
                    yield return www;
                    if (string.IsNullOrEmpty(www.error))
                    {
                        try
                        {
                            CLASSTAG_FileHelper.WriteAllBytes(dstUrl, www.bytes);
                        }
                        catch (Exception e)
                        {
                            ThrowFatalException("Copy Script Error: " + srcUrl + "\n" + e.Message);
                        }
                    }
                    else
                    {
                        ThrowFatalException("Load Script Error: " + www.error);
                    }
                }
            }

            onFinish();
        }

        public void StartCheckOutGameRes(Action<bool> onFinish)
        {
            StartCoroutine(CheckOutGameRes(onFinish));
        }

        /// <summary>
        /// 检查包外资源,如果包外缺失resConfig,dllVersion,miniResConfig将从包内拷贝至包外
        /// </summary>
        private IEnumerator CheckOutGameRes(Action<bool> onFinish)
        {
			yield return StartCoroutine(CheckInteralResConfig());
			PrintInfo("Verify the integrity of game resources");
            string packageResUrlRoot = CLASSTAG_GameResPath.FUNCTAG_packageResUrlRoot;
            string url = null;

            //1. Check DLL, only for Android, should disable
            /*
            var dllHasChanged = false;
            if ((GameLauncher.andriodUpdateInfo==null) && IsSupportUpdateDllPlatform())
            {
                url = Path.Combine(packageResUrlRoot, CLASSTAG_GameResPath.DLLVERSION_FILE);
                Debug.Log("AssetUpdate CheckOutGameRes: " + url);
                using (var www = new WWW(url))
                {
                    yield return www;

                    if (!string.IsNullOrEmpty(www.error))
                    {
                        ThrowFatalException(string.Format("Load url:{0}\nerror:{1}", url, www.error));
                    }

                    _packageDllVersion = JsonMapper.ToObject<CLASSTAG_DllVersion>(www.text);
                }

                var dllVersionPath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.DLLVERSION_FILE);
                GameDebug.Log("dllVersionPath is " + dllVersionPath);
                if (File.Exists(dllVersionPath))
                {
                    _dllVersion = CLASSTAG_FileHelper.ReadJsonFile<CLASSTAG_DllVersion>(dllVersionPath);
                    if (_dllVersion.Version < _packageDllVersion.Version)
                    {
                        CleanUpDllFolder();
						GameDebug.Log("The dll in the package is relatively new, you need to clear the dll outside the package");

                        if (onFinish != null)
                        {
                            onFinish(true);
                        }
                        yield break;
                    }

                    dllHasChanged = _dllVersion.Version != _packageDllVersion.Version;
                }
                else
                {
                    // 没陪dllversion，但是有dll的低概率事件，也做下清除以防万一
                    CleanUpDllFolder();
                    _dllVersion = _packageDllVersion;
                    SaveDllVersion();
                    GameDebug.Log("Copy the dllVersion in the package to the outside of the package");
                }
            }
            */

            //2. Check packageconfig resConfig.jz
            url = Path.Combine(packageResUrlRoot, CLASSTAG_GameResPath.RESCONFIG_FILE);
            GameDebug.Log("Check package config: " +url);
            using (var www = new WWW(url))
            {
                //Load ResConfig in the package
                yield return www;
                if (!string.IsNullOrEmpty(www.error))
                {
                    ThrowFatalException(string.Format("Load url:{0}\nerror:{1}", url, www.error));
                }

                _packageResConfig = CLASSTAG_ResConfig.FUNCTAG_ReadFile(www.bytes, true);
                if (_packageResConfig == null)
                {
                    ThrowFatalException("The resource configuration information in the package is lost");
                    yield break;
                }

                //Load ResConfig outside the package
                string configPath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.RESCONFIG_FILE);
                GameDebug.Log("package configPath: " + configPath);
                if (CLASSTAG_FileHelper.IsExist(configPath))
                {
                    byte[] data = CLASSTAG_FileHelper.ReadAllBytes(configPath);
                    _curResConfig = CLASSTAG_ResConfig.FUNCTAG_ReadFile(data, true);
                    if (_curResConfig == null)
                    {
                        GameDebug.LogError("The resConfig outside the package is damaged and will be copied from the package again");
                    }
                    else
                    {
                        GameDebug.Log("_curResConfig BuildTime: " + _curResConfig.BuildTime.ToString());
                    }
                }
                else
                {
                    GameDebug.Log("configPath does not exist: " + configPath);
                }
                if (IsNewerPackageRes())
                {
                    GameDebug.Log("Update the resources in the package, empty the resources outside the package, and re-copy the resConfig in the package to the outside of the package!!!!");
                    CleanUpBundleResFolder();
                    _curResConfig = CLASSTAG_ResConfig.FUNCTAG_ReadFile(www.bytes, true);
                    SaveResConfig();

                    //shoud disable anything involed with, because is is oly word on android
                    /*
                    if (dllHasChanged)
                    {
                        CleanUpDllFolder();
                        GameDebug.Log("The resources in the package are updated, the dll does not correspond, and it needs to be cleared and restarted");

                        if (onFinish != null)
                        {
                            onFinish(true);
                        }
                        yield break;
                    }
                    */
                }
            }

            //If the current resource is a small package, copy the miniResConfig in the package to the outside of the package
            //When the small package is upgraded to the whole package, the isMiniRes flag will be set to false
            if (_curResConfig.isMiniRes)
            {
                string miniResConfigPath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.MINIRESCONFIG_FILE);
                if (CLASSTAG_FileHelper.IsExist(miniResConfigPath))
                {
                    _miniResConfig = CLASSTAG_FileHelper.ReadJsonFile<CLASSTAG_MiniResConfig>(miniResConfigPath, true);
                }
                else
                {
                    url = Path.Combine(packageResUrlRoot, CLASSTAG_GameResPath.MINIRESCONFIG_FILE);
                    using (var www = new WWW(url))
                    {
                        yield return www;

						if (!string.IsNullOrEmpty (www.error)) {
							//ThrowFatalException(string.Format("Load url:{0}\nerror:{1}", url, www.error));
							Debug.Log (string.Format ("Load url:{0}\nerror:{1}", url, www.error));
						} else {

							_miniResConfig = CLASSTAG_FileHelper.ReadJsonBytes<CLASSTAG_MiniResConfig> (www.bytes);
							SaveMiniResConfig ();
							GameDebug.Log ("Copy the miniResConfig in the package to the outside of the package");
						}
                    }
                }
            }

            if (onFinish != null)
                onFinish(false);
        }
		//检测InteralResConfig
		public IEnumerator CheckInteralResConfig()
		{
			string packageResUrlRoot = CLASSTAG_GameResPath.FUNCTAG_packageResUrlRoot;
			var url = Path.Combine(packageResUrlRoot, CLASSTAG_GameResPath.INTERNAL_RESCONFIG_FILE);
			using (var www = new WWW(url))
			{
				//加载包内ResConfig
				yield return www;
				if (string.IsNullOrEmpty(www.error))
				{
					_internalResConfig = internalResConfig.ReadFile(www.bytes, true);
				}
				GameDebug.Log("CheckInteralResConfig:" + (_internalResConfig == null));
				
			}
		}

        public void UpdateGameVersion()
        {
            if (_curResConfig != null)
            {
                CLASSTAG_GameVersion.resVersion = _curResConfig.Version;
                CLASSTAG_GameVersion.svnVersion = _curResConfig.svnVersion;
            }
        }

        //If one of the following conditions is met, it is considered that the player is installing the game for the first time, and the resource directory outside the package needs to be cleared, and resConfig needs to be copied from the package to the outside of the package
        //1. Install the game for the first time or the user manually deletes the resConfig outside the package
        //2. The version number outside the package is lower than the version number in the package. In the most special case, when the user does not enter the game update, the old package is overwritten with the new installation package, so that the resources in the package are updated
        // Use buildtime to record package changes
        private bool IsNewerPackageRes()
        {
            return _curResConfig == null || (_packageResConfig.BuildTime != _curResConfig.BuildTime);
        }

		public bool CheckBlackListUpdate()
		{
			if (_staticConfig != null)
				return (_staticConfig.UpdateType == 1);
			return true;
		}

		public String GetCenterServerUrl()
		{
			if (_staticConfig != null)
				return _staticConfig.CenterServerUrl;
			return "";
		}
        #endregion

        #region 获取VersionConfig信息
        /// <summary>
        /// Obtain the specified VersionConfig information according to the version information type
        /// </summary>
        public void FetchVersionConfig(bool isTestVersionConfig, Action onFinish)
        {
            if (isTestVersionConfig)
            {
                StartCoroutine(DownloadVersionConfig(CLASSTAG_VersionConfig.GetTestFileName(), onFinish));
            }
            else
            {
                StartCoroutine(DownloadVersionConfig(CLASSTAG_VersionConfig.GetFileName(), onFinish));
            }
        }

        private IEnumerator DownloadVersionConfig(string versionConfigName, Action onFinish)
        {
            PrintInfo("Check for game version updates");

            string versionConfigUrl = string.Format("{0}?ver={1}", _cdnUrlRoot + "/" + versionConfigName, DateTime.Now.Ticks);
            GameDebug.Log("DownloadVersionConfig: " + versionConfigUrl);

            yield return StartCoroutine(DownloadByUnityWebRequest(versionConfigUrl, www =>
            {
                _versionConfig = JsonMapper.ToObject<CLASSTAG_VersionConfig>(www.downloadHandler.text);
                if (_versionConfig == null)
                {
                    ThrowFatalException("VersionConfig is null");
                }
                else
                {
                    GameDebug.Log("CDN_VersionConfig:\n" + _versionConfig.ToString());
                }

                //If the server version is different, record it so that you won’t get a different server version next time
                //if (IsChangeVersionConfigType())
                //{
                //    SaveLocalVersionConfig();
                //}

                if (onFinish != null)
                    onFinish();
            },
                msg =>
                {
                    _cdnUrlIndex += 1;
                    //int index = _cdnUrls.IndexOf(_cdnUrlRoot);
                    if (_cdnUrlIndex + 1 >= _cdnUrls.Count)
                    {
                        var log = string.Format("Failed to load game resources, please try again\n{0}", msg);
                        if (_loadErrorHandler != null)
                        {
                            _loadErrorHandler(log);
                        }
                        else
                        {
                            PrintInfo(log);
                        }
                        throw new Exception(msg);
                    }
                    else
                    {
                        CLASSTAG_CdnReportHelper.FUNCTAG_Report(versionConfigUrl);
                        _cdnUrlRoot = _cdnUrls[_cdnUrlIndex];
                        StartCoroutine(DownloadVersionConfig(versionConfigName, onFinish));
                    }
                }));
        }

        #endregion

        #region Dll update process

        /// <summary>
        /// Return true means that Dll does not need to be updated
        /// </summary>
        /// <param name="onFinish">Dll update completion callback, generally used to prompt the user to restart the game</param>
        /// <returns></returns>
        public bool ValidateDllVersion(Action onFinish)
        {
            if (!IsSupportUpdateDllPlatform())

                return true;

            //Under the general update process, the local version number is greater than or equal to the value of VersionConfig directly skipped
            if (_dllVersion.Version >= _versionConfig.dllVersion)
                return true;

            PrintInfo("Get program version information");
            string dllUrl = string.Format("{0}/{1}/{2}", _cdnUrlRoot, CLASSTAG_GameResPath.DLL_VERSION_ROOT, CLASSTAG_DllVersion.GetFileName(_versionConfig.dllVersion));

            StartCoroutine(DownloadByUnityWebRequest(dllUrl, www =>
            {
                var newDllVersion = JsonMapper.ToObject<CLASSTAG_DllVersion>(www.downloadHandler.text);
                if (newDllVersion == null)
                {
                    ThrowFatalException("DllVersion is null");
                    return;
                }

                //Build dll update download queue
                long totalFileSize = 0L;
                var downloadQueue = new Queue<CLASSTAG_DllInfo>(newDllVersion.Manifest.Count);
                foreach (var pair in newDllVersion.Manifest)
                {
                    CLASSTAG_DllInfo newDllInfo = pair.Value;
                    CLASSTAG_DllInfo oldDllInfo;
                    if (_dllVersion.Manifest.TryGetValue(pair.Key, out oldDllInfo))
                    {
                        if (oldDllInfo.MD5 != newDllInfo.MD5 && !File.Exists(GetBackupDllPath(newDllInfo)))
                        {
                            totalFileSize += newDllInfo.size;
                            downloadQueue.Enqueue(newDllInfo);
                            GameDebug.Log("Join the dll update queue " + newDllInfo.dllName);
                        }
                    }
                    else
                    {
                        totalFileSize += newDllInfo.size;
                        downloadQueue.Enqueue(newDllInfo);
                        GameDebug.Log("Join the dll update queue " + newDllInfo.dllName);
                    }
                }

                string netType = PlatformAPI.getNetworkType();
                if (netType == PlatformAPI.NET_STATE_WIFI || _requestUpdateFlag)
                {
                    StartCoroutine(UpdateDllFile(newDllVersion, downloadQueue, totalFileSize, onFinish));
                }
                else
                {
                    BuiltInDialogueViewController.FUNCTAG_OpenView(string.Format("There are {0} patches that need to be updated, whether to update", FormatBytes(totalFileSize)),
                        () =>
                        {
                            _requestUpdateFlag = true;
                            StartCoroutine(UpdateDllFile(newDllVersion, downloadQueue, totalFileSize, onFinish));
                        }, () =>
                        {
                            _loadErrorHandler(null);
                        }, UIWidget.Pivot.Left, "Update", "Exit");
                }
            }, ThrowFatalException));
            return false;
        }

        /// <summary>
        /// Update the dll according to the latest DllVersion information
        /// </summary>
        private IEnumerator UpdateDllFile(CLASSTAG_DllVersion newDllVersion, Queue<CLASSTAG_DllInfo> downloadQueue, long totalFileSize, Action onFinish)
        {
            if (ValidateStorageSpace(totalFileSize))
            {
                long remainingSize = totalFileSize;
                int totalCount = downloadQueue.Count;
                PrintInfo(string.Format("In the download engine, the remaining {0}({1}/{2})", FormatBytes(remainingSize), 0, totalCount));
                while (downloadQueue.Count > 0)
                {
                    var newDllInfo = downloadQueue.Dequeue();
                    string dllUrl = _cdnUrlRoot + "/" + CLASSTAG_GameResPath.DLL_FILE_ROOT + "/" + newDllInfo.ToFileName();
                    yield return StartCoroutine(DownloadByUnityWebRequest(dllUrl, www =>
                    {
                        // The backup is still named with md5, which is convenient to verify whether the file has been downloaded
                        var dllPath = GetBackupDllPath(newDllInfo);
                        try
                        {
                            CLASSTAG_FileHelper.WriteAllBytes(dllPath, www.downloadHandler.data);
                            remainingSize -= newDllInfo.size;
                            GameDebug.Log(string.Format("Write to dll file:{0}", dllPath));
                        }
                        catch (Exception e)
                        {
                            ThrowFatalException("Save Dll Error: " + dllPath + "\n" + e.Message);
                        }
                    }, ThrowFatalException));

                    yield return null;
                    PrintInfo(string.Format("Download engine resources, remaining{0}({1}/{2})", FormatBytes(remainingSize), totalCount - downloadQueue.Count, totalCount));
                }

                if (!_fatalError)
                {
                    try
                    {
                        UseNewDllFile(newDllVersion);
                    }
                    catch (Exception e)
                    {
                        ThrowFatalException(e.Message);
                    }
                }

                ClearBackupDllFile();

                //After downloading, save the latest DllVersion to the local
                PrintInfo("Engine update is complete, please restart the game");

                if (onFinish != null)
                    onFinish();
            }
        }


        private void UseNewDllFile(CLASSTAG_DllVersion newDllVersion)
        {
            // Recalculate here to prevent multiple copies of files
            foreach (var pair in newDllVersion.Manifest)
            {
                var newDllInfo = pair.Value;
                CLASSTAG_DllInfo oldDllInfo;
                _dllVersion.Manifest.TryGetValue(pair.Key, out oldDllInfo);
                if (oldDllInfo == null || oldDllInfo.MD5 != newDllInfo.MD5)
                {
                    // The dll server format is compressed first, then encrypted
                    // Here you have to decrypt the dll first, then decompress, and then encrypt
                    // Non-project dll is not encrypted
                    var fileBytes = CLASSTAG_FileHelper.ReadAllBytes(GetBackupDllPath(newDllInfo));
                    var zipBytes = CLASSTAG_DllHelper.FUNCTAG_IsProjectDll(newDllInfo.dllName) ? CLASSTAG_DllHelper.DecryptDll(fileBytes) : fileBytes;
                    var realBytes = CLASSTAG_ZipLibUtils.Uncompress(zipBytes);
                    var encryptBytes = CLASSTAG_DllHelper.FUNCTAG_IsProjectDll(newDllInfo.dllName) ? CLASSTAG_DllHelper.EncryptDll(realBytes) : realBytes;

                    var dllPath = CLASSTAG_GameResPath.dllRoot + "/" + newDllInfo.dllName + ".dll";
                    CLASSTAG_FileHelper.WriteAllBytes(dllPath, encryptBytes);
                }
                //else
                //{
                //    //GameDebug.LogError("It is impossible to add a new dll in the Dll update, please check: "+ pair.Key);
                //}
            }

            _dllVersion = newDllVersion;
            SaveDllVersion(true);
        }


        private string GetBackupDllPath(CLASSTAG_DllInfo info)
        {
            return CLASSTAG_GameResPath.dllBackupRoot + "/" + info.ToFileName();
        }

        /// <summary>
        /// When the guarantee has been updated, do the emptying process
        /// </summary>
        private void ClearBackupDllFile()
        {
            CLASSTAG_FileHelper.FUNCTAG_DeleteDirectory(CLASSTAG_GameResPath.dllBackupRoot, true);
        }


        #endregion

        #region Script update process
        public void UpdateScript(Action onFinish)
        {
            StartCoroutine(UpdateScriptFile(onFinish));
        }

        public IEnumerator FetchScriptVersion()
        {
            if (_scriptPatchVersion == null)
            {
                string url = string.Format("{0}/{1}/{2}?ver={3}", _cdnUrlRoot, CLASSTAG_GameResPath.SCRIPT_ROOT, CLASSTAG_ScriptVersion.GetFileName(), DateTime.Now.Ticks.ToString());
                GameDebug.Log("FetchScriptVersion: " + url); //http://374.gsscdn.com/b77/release/android/script/scriptVersion.json?ver=637561929936773090
                yield return StartCoroutine(DownloadByUnityWebRequest(url, www =>
                {
                    _scriptPatchVersion = JsonMapper.ToObject<CLASSTAG_ScriptVersion>(www.downloadHandler.text);
                    if (_scriptPatchVersion == null)
                    {
                        ThrowFatalException("Download ScriptVersion Error");
                    }
                }, ThrowFatalException));
            }
        }

        private IEnumerator UpdateScriptFile(Action onFinish)
        {
            string dstUrl = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.SCRIPT_FILE);
            string srcUrl = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_packageResUrlRoot, CLASSTAG_GameResPath.SCRIPT_FILE);
            GameDebug.Log("UpdateScriptFile dstUrl: " + dstUrl); ///Users/nhnhat/Library/Application Support/DUO/YLK/script
            GameDebug.Log("UpdateScriptFile srcUrl: " + srcUrl); //file:///Volumes/DATA/Programming/git/b77/b77_client_unity_2017/unity2017-client/Assets/StreamingAssets/script
            if (!File.Exists(dstUrl))
            {
                GameDebug.Log("No script outside the package, copy from the package");
                using (var www = new WWW(srcUrl))
                {
                    yield return www;
                    if (string.IsNullOrEmpty(www.error))
                    {
                        try
                        {
                            CLASSTAG_FileHelper.WriteAllBytes(dstUrl, www.bytes);
                        }
                        catch (Exception e)
                        {
                            ThrowFatalException("Copy Script Error: " + srcUrl + "\n" + e.Message);
                        }
                    }
                    else
                    {
                        ThrowFatalException("Load Script Error: " + www.error);
                    }
                }
            }

            try
            {
                _curScript = new CLASSTAG_LuaScript();
                _curScript.LoadFrom(dstUrl);
            }
            catch
            {
                _curScript = null;
            }
            if (_curScript == null)
            {
                GameDebug.Log(string.Format("Failed to load script file"));
                onFinish();
                yield break;
            }

            int curScriptVer = _curScript.scriptVersion;
            int newScriptVer = _versionConfig.scriptVersion;

            GameDebug.Log(string.Format("Check script version curScriptVer={0} newScriptVersion={1}", curScriptVer, newScriptVer));
            PrintInfo("Get patch information");

            yield return StartCoroutine(FetchScriptVersion());

            onFinish();
            // not want to patch scrpt, download new script at one
            /*
            if (newScriptVer > curScriptVer)   //Upgrade patch
            {
                long totalFileSize = 0L;

                List<int> patchVersionList = _scriptPatchVersion.GetPatchList(curScriptVer, newScriptVer);
                if (patchVersionList == null)
                {
                    //ThrowFatalException(string.Format("Failed to obtain patch information {0}->{1}", curScriptVer, newScriptVer));
                    GameDebug.Log(string.Format("Failed to obtain patch information {0}->{1}", curScriptVer, newScriptVer));
                    onFinish();
                    yield break;
                }


                var downloadQueue = new Queue<CLASSTAG_ScriptInfo>();
                for (int i = 1; i < patchVersionList.Count; i++)
                {
                    string patchName = string.Format("patch_{0}_{1}", patchVersionList[i - 1], patchVersionList[i]);
                    CLASSTAG_ScriptInfo patch;
                    if (_scriptPatchVersion.Patchs.TryGetValue(patchName, out patch))
                    {
                        totalFileSize += patch.size;
                        downloadQueue.Enqueue(patch);
                    }
                    else
                    {
                        //ThrowFatalException(string.Format("Unable to get {0} information", patchName));
                        GameDebug.Log(string.Format("Unable to get {0} information", patchName));
                        onFinish();
                        yield break;
                    }
                }

                string netType = PlatformAPI.getNetworkType();
                if (netType == PlatformAPI.NET_STATE_WIFI || _requestUpdateFlag)
                {
                    StartCoroutine(UpdateScriptFile(newScriptVer, downloadQueue, totalFileSize, onFinish));
                }
                else
                {
                    BuiltInDialogueViewController.FUNCTAG_OpenView(string.Format("There are {0} patches that need to be updated, whether to update", FormatBytes(totalFileSize)),
                        () =>
                        {
                            _requestUpdateFlag = true;
                            StartCoroutine(UpdateScriptFile(newScriptVer, downloadQueue, totalFileSize, onFinish));
                        },
                        () =>
                        {
                            _loadErrorHandler(null);
                        }, UIWidget.Pivot.Left, "Update", "Exit");
                }
            }
            else
            {
                onFinish();
            }
            */
        }

        private IEnumerator UpdateScriptFile(int newVerson, Queue<CLASSTAG_ScriptInfo> downloadQueue, long totalFileSize, Action onFinish)
        {
            GameDebug.Log("Check script update Number of patches = " + downloadQueue.Count);
            if (ValidateStorageSpace(totalFileSize))
            {
                long remainingSize = totalFileSize;
                int totalCount = downloadQueue.Count;
                PrintInfo(string.Format("Downloading the patch, remaining{0}({1}/{2})", FormatBytes(remainingSize), 0, totalCount));
                while (downloadQueue.Count > 0)
                {
                    CLASSTAG_ScriptInfo patchInfo = downloadQueue.Dequeue();
                    string url = string.Format("{0}/{1}/{2}", _cdnUrlRoot, CLASSTAG_GameResPath.SCRIPT_ROOT, patchInfo.name);
                    GameDebug.Log("UpdateScriptFile: " + url);
                    yield return StartCoroutine(DownloadByUnityWebRequest(url, www =>
                    {
                        try
                        {
                            string md5 = CLASSTAG_MD5Hashing.HashBytes(www.downloadHandler.data);
                            if (md5 != patchInfo.md5)
                            {
                                ThrowFatalException(string.Format("{0} MD5 Error! LocalMD5={1} RemoteMD5={2}", patchInfo.name, md5, patchInfo.md5));
                            }

                            CLASSTAG_LuaScript luaScript = new CLASSTAG_LuaScript();
                            luaScript.LoadFrom(www.downloadHandler.data);
                            _curScript.MergePatch(luaScript);
                        }
                        catch (Exception e)
                        {
                            ThrowFatalException("Merge Patch Error: " + patchInfo.name + "\n" + e.Message);
                        }
                        remainingSize -= patchInfo.size;

#if UNITY_EDITOR
                        string path = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, "patch/" + patchInfo.name);
                        CLASSTAG_FileHelper.WriteAllBytes(path, www.downloadHandler.data);
#endif
                    }, ThrowFatalException));

                    yield return null;
                    PrintInfo(string.Format("Downloading the patch, remaining{0}({1}/{2})", FormatBytes(remainingSize), totalCount - downloadQueue.Count, totalCount));
                }

                string scriptName = "script_" + newVerson;
                CLASSTAG_ScriptInfo scriptInfo = _scriptPatchVersion.Scripts[scriptName];
                string dstUrl = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.SCRIPT_FILE);
                byte[] data = _curScript.GetEncryptByte();
                string scriptmd5 = CLASSTAG_MD5Hashing.HashBytes(data);
                //if (scriptmd5 == scriptInfo.md5)
                //{
                CLASSTAG_FileHelper.WriteAllBytes(dstUrl, data);
                if (onFinish != null)
                {
                    onFinish();
                }
                //}
                //else
                //{
                //    ThrowFatalException(string.Format("{0} MD5 Error! LocalMD5={1} RemoteMD5={2}", scriptName, scriptmd5, scriptInfo.md5));
                //}
            }

            yield return null;
        }

        public void ValidateScript(Action onFinish)
        {
            StartCoroutine(ValidateScriptFile(onFinish));
        }

        public IEnumerator ValidateScriptFile(Action onFinish)
        {
            yield return StartCoroutine(FetchScriptVersion());

            int newScriptVer = _versionConfig.scriptVersion;
            // if forceUpdate, re download it
            if (_versionConfig.forceUpdate == false)
            {
                if (_curScript != null && _curScript.scriptVersion >= newScriptVer)
                {
                    PrintInfo("Starting game");
                    onFinish();
                    yield break;
                }
            }
            string dstUrl = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.SCRIPT_FILE);
            
            string scriptName = "script_" + newScriptVer;
            CLASSTAG_ScriptInfo scriptInfo = _scriptPatchVersion.Scripts[scriptName];
            PrintInfo("Check resource files");
            GameDebug.Log("ValidateScriptFile: " + scriptName);
            //////////////////////////////
            //////////////////////////////
            bool downloadScriptAgain = false;
            string dstMd5 = CLASSTAG_MD5Hashing.HashFile(dstUrl);
            if (dstMd5 != scriptInfo.md5 || _curScript.scriptVersion < newScriptVer || _versionConfig.forceUpdate)
            {
                downloadScriptAgain = true;
            }
            if (!downloadScriptAgain)
            {
                PrintInfo("Starting game");
                onFinish();
                yield break;
            }

            string url = string.Format("{0}/{1}/{2}", _cdnUrlRoot, CLASSTAG_GameResPath.SCRIPT_ROOT, scriptName);
            PrintInfo("Download the script file for new version");
            yield return StartCoroutine(DownloadByUnityWebRequest(url, www =>
            {
                CLASSTAG_FileHelper.WriteAllBytes(dstUrl, www.downloadHandler.data);
                string md5 = CLASSTAG_MD5Hashing.HashBytes(www.downloadHandler.data);
                if (md5 == scriptInfo.md5)
                {
                    CLASSTAG_FileHelper.WriteAllBytes(dstUrl, www.downloadHandler.data);
                    onFinish();
                }
                else
                {
                    ThrowFatalException(string.Format("Download {0} Error! LocalMD5={1} RemoteMD5={2}", scriptInfo.name, md5, scriptInfo.md5));
                }
            }, ThrowFatalException));
            //////////////////////////////
            //////////////////////////////
            //if (_scriptPatchVersion.Scripts.TryGetValue(scriptName, out scriptInfo))
            //{
            //    string md5 = CLASSTAG_MD5Hashing.HashFile(dstUrl);
            //    if (md5 == scriptInfo.md5)
            //    {
            //        onFinish();
            //    }
            //    else
            //    {
            //        string url = string.Format("{0}/{1}/{2}", _cdnUrlRoot, CLASSTAG_GameResPath.SCRIPT_ROOT, scriptName);
            //        PrintInfo("An error is detected, download the script file again");
            //        yield return StartCoroutine(DownloadByUnityWebRequest(url, www =>
            //        {
            //            md5 = CLASSTAG_MD5Hashing.HashBytes(www.downloadHandler.data);
            //            if (md5 == scriptInfo.md5)
            //            {
            //                CLASSTAG_FileHelper.WriteAllBytes(dstUrl, www.downloadHandler.data);
            //                onFinish();
            //            }
            //            else
            //            {
            //                ThrowFatalException(string.Format("Download {0} Error! LocalMD5={1} RemoteMD5={2}", scriptInfo.name, md5, scriptInfo.md5));
            //            }
            //        }, ThrowFatalException));
            //    }
            //}
            //else
            //{
            //    //ThrowFatalException(string.Format("Unable to get {0} information", scriptName));
            //    GameDebug.Log(string.Format("Unable to get {0} information", scriptName));
            //    onFinish();
            //    yield break;
            //}
        }

        #endregion

        #region GameRes update process
        public event Action OnBeginResUpdate;
        public event Action OnFinishResUpdate;

        public void UpdateGameRes(Action onFinish)
        {
            if (_versionConfig.forceUpdate == false)
            {
                if (_curResConfig.Version >= _versionConfig.resVersion)
                {
                    onFinish();
                    return;
                }
            }

            // if forceUpdate, download again
            StartCoroutine(FetchLastestResConfig(onFinish));
        }

        private IEnumerator FetchLastestResConfig(Action onFinish)
        {
            PrintInfo("Get the latest game resource version information");
            string resConfigUrl = _cdnUrlRoot + "/" + CLASSTAG_GameResPath.RESCONFIG_ROOT + "/" + CLASSTAG_ResConfig.GetRemoteFile(_versionConfig.resVersion);
            GameDebug.Log("FetchLastestResConfig: " + resConfigUrl);
            yield return StartCoroutine(DownloadByUnityWebRequest(resConfigUrl, www =>
            {
                var newResConfig = CLASSTAG_ResConfig.FUNCTAG_ReadFile(www.downloadHandler.data, true);
                if (newResConfig == null)
                {
                    ThrowFatalException("Fetch resConfig is null");
                    return;
                }

                //In the case of WIFI, do not make any prompt, update the resource directly
                //Under the mobile phone network, it prompts the resource update size, if it is forced to update, skip and exit the game directly
                //When the small package is not completely upgraded to the entire package, you must go through the game update process, because the information of the new PatchInfo needs to be overwritten to curResConfig, otherwise the player will download the old resources after entering the game
                string netType = PlatformAPI.getNetworkType();
                AutoUpgrade = netType == PlatformAPI.NET_STATE_WIFI ? AutoUpgradeType.WIFI : AutoUpgradeType.NONE;
                var patchInfo = GeneratePatchInfo(_curResConfig, newResConfig);
                if (patchInfo != null)
                {
					UploadDataManager.Instance.patchSize = patchInfo.TotalFileSize;
                    if (netType == PlatformAPI.NET_STATE_WIFI || _curResConfig.isMiniRes || _requestUpdateFlag)
                    {
                        StartDownloadRes(patchInfo, onFinish);
                    }
                    else
                    {
                        BuiltInDialogueViewController.FUNCTAG_OpenView(string.Format("There are {0} resources that need to be updated, whether to update", FormatBytes(patchInfo.TotalFileSize, _versionConfig.resSize)),
                            () =>
                            {
                                _requestUpdateFlag = true;
                                StartDownloadRes(patchInfo, onFinish);
                            }, () =>
                            {
                                _loadErrorHandler(null);
                            }, UIWidget.Pivot.Left, "Update", "Exit");
                    }
                }
                else
                {
                    PrintInfo("PatchInfo is empty, there are no resources to update");
                    if (onFinish != null)
                        onFinish();
                }
            }, ThrowFatalException));
        }

        /// <summary>
        /// The version update information between the two versions is generated when the game is running
        /// </summary>
        private CLASSTAG_ResPatchInfo GeneratePatchInfo(CLASSTAG_ResConfig oldResConfig, CLASSTAG_ResConfig newResConfig)
        {
            if (oldResConfig == null || newResConfig == null)
            {
                return null;
            }

            //No need to generate the current version of PatchInfo
            if (oldResConfig.Version == newResConfig.Version)
            {
                return null;
            }

            CLASSTAG_ResPatchInfo patchInfo = new CLASSTAG_ResPatchInfo
            {
                CurVer = oldResConfig.Version,
                CurSvnVer = oldResConfig.svnVersion,
                CurLz4CRC = oldResConfig.lz4CRC,
                CurLzmaCRC = oldResConfig.lzmaCRC,
                EndVer = newResConfig.Version,
                EndSvnVer = newResConfig.svnVersion,
                EndLz4CRC = newResConfig.lz4CRC,
                EndLzmaCRC = newResConfig.lzmaCRC
            };

            //Generate update list
            //If the CRC is not 0, and the CRC value is changed, add it to the update list
            //oldResConfig does not exist, directly add to the update list
            foreach (var newRes in newResConfig.Manifest)
            {
                if (oldResConfig.Manifest.ContainsKey(newRes.Key))
                {
                    if (oldResConfig.Manifest[newRes.Key].CRC != newRes.Value.CRC)
                    {
                        patchInfo.updateList.Add(newRes.Value);
                        patchInfo.TotalFileSize += newRes.Value.size;
                    }
                }
                else
                {
                    patchInfo.updateList.Add(newRes.Value);
                    patchInfo.TotalFileSize += newRes.Value.size;
                }
            }

            //Generate delete list
            //The key of oldResConfig cannot find the corresponding key in newResConfig, which proves that the resource has been deleted
            foreach (var oldRes in oldResConfig.Manifest)
            {
                if (!newResConfig.Manifest.ContainsKey(oldRes.Key))
                {
                    patchInfo.removeList.Add(oldRes.Key);
                }
            }

            return patchInfo;
        }

        private void StartDownloadRes(CLASSTAG_ResPatchInfo resPatchInfo, Action onFinish)
        {
            //如果获取的patchInfo版本号与本地resConfig的版本号不一致，直接忽略
            if (resPatchInfo.CurVer == _curResConfig.Version)
            {
                //对比版本号信息，patch最终版本号与本地资源版本号不一致，需要下载更新资源包
                if (resPatchInfo.EndVer != _curResConfig.Version)
                {
                    RemoveOutdatedAsset(resPatchInfo);

                    StartCoroutine(DownloadAssetBatch(resPatchInfo, onFinish));
                }
                else
                {
                    if (onFinish != null)
                        onFinish();
                }
            }
            else
            {
                GameDebug.LogError("The patchInfo obtained is inconsistent with the local resConfig version number. There may be a problem with the patchInfo process. Please check");
                if (onFinish != null)
                    onFinish();
            }
        }

        private void RemoveOutdatedAsset(CLASSTAG_ResPatchInfo resPatchInfo)
        {
            if (resPatchInfo.removeList.Count == 0) return;

            string resFileRoot = CLASSTAG_GameResPath.bundleRoot;
            //Delete old version redundant resources according to patchManifest information
            for (int i = 0; i < resPatchInfo.removeList.Count; i++)
            {
                string bundleName = resPatchInfo.removeList[i];
                CLASSTAG_ResInfo resInfo;
                if (_curResConfig.Manifest.TryGetValue(bundleName, out resInfo))
                {
                    string abFile = resInfo.GetABPath(resFileRoot);
                    if (CLASSTAG_FileHelper.IsExist(abFile))
                    {
                        File.Delete(abFile);
#if GAMERES_LOG
                    GameDebug.LogError(string.Format("AB Delete:{0}", abFile));
#endif
                    }
                    _curResConfig.Manifest.Remove(bundleName);
                }
            }
        }

        private int _finishedCount;
        private int _downloadingCount;
        private long _remainingSize;

        private float _updateResTime = 0f;
        private bool isDownloadAsset = false;

        private IEnumerator DownloadAssetBatch(CLASSTAG_ResPatchInfo resPatchInfo, Action onFinish)
        {
            PrintInfo("Ready to download the game resources");

            //Verify the amount of free space on the phone
            if (ValidateStorageSpace(resPatchInfo.TotalFileSize))
            {
                //TalkingData.OnEventStep("GameUpdate", "Begin");
                if (OnBeginResUpdate != null)
                    OnBeginResUpdate();

                var stopwatch = new Stopwatch();
                stopwatch.Start();

                string remoteRoot = GetCDNBundleRoot();
                string bundleRoot = CLASSTAG_GameResPath.bundleRoot;
                _finishedCount = 0;
                _downloadingCount = 0;
                _remainingSize = resPatchInfo.TotalFileSize;
                var downloadQueue = new Queue<CLASSTAG_ResInfo>(resPatchInfo.updateList.Count);

                int alreadyUpdateCount = 0;
                //Build a resource download queue, and ignore the downloaded resources directly
                for (int i = 0; i < resPatchInfo.updateList.Count; i++)
                {
                    CLASSTAG_ResInfo newResInfo = resPatchInfo.updateList[i];
                    string newResKey = newResInfo.bundleName;
                    CLASSTAG_ResInfo oldResInfo;
                    bool needDownload = true;
                    if (_curResConfig.Manifest.TryGetValue(newResKey, out oldResInfo))
                    {
                        //The version number is the same, the CRC is the same, and the file has been updated, no need to download repeatedly
                        //This situation only exists when the player exits the game during the resource update process. After re-entering the game, the updated resources should be ignored
                        if (oldResInfo.CRC == newResInfo.CRC)
                        {
                            var packageResInfo = _packageResConfig.GetResInfo(newResKey);
                            if (packageResInfo != null &&
                                packageResInfo.CRC == newResInfo.CRC)
                            {
                                //If the resource already exists in the package, there is no need to update it
                                //This situation is very special, only if the player installs the latest package without deleting the old package
                                needDownload = false;
                                ++alreadyUpdateCount;
                                _remainingSize -= newResInfo.size;
                            }
                            else
                            {
                                //If the ResConfig in the package does not have the resource, judge whether the file exists in the directory outside the package, and if there is, skip it without downloading
                                string filePath = newResInfo.GetABPath(bundleRoot);
                                if (CLASSTAG_FileHelper.IsExist(filePath))
                                {
                                    needDownload = false;
                                    ++alreadyUpdateCount;
                                    _remainingSize -= newResInfo.size;
                                }
                            }
                        }
                    }

                    //小包模式下跳过miniResConfig记录的资源ID,因为这部分资源可以游戏时动态下载
                    if (_curResConfig.isMiniRes && _miniResConfig.replaceResConfig.ContainsKey(newResKey))
                    {
                        needDownload = false;
                        _remainingSize -= newResInfo.size;
                        //小包未下载的资源,直接用最新版本的ResInfo覆盖,因为游戏运行时要根据这个ResInfo信息动态下载该资源
                        _curResConfig.Manifest[newResKey] = newResInfo;
                    }

                    if (needDownload)
                    {
                        downloadQueue.Enqueue(newResInfo);
                    }
                }

                int totalUpdateCount = downloadQueue.Count;
                GameDebug.Log(string.Format("Begin Download Asset:\n The number of updated resources: {0}\n The number of resources that need to be updated before entering the game: {1}\n The number of small package resources that need to be downloaded when the game is running: {2}\n The total number of updated resources:{3}\n",
                    alreadyUpdateCount,
                    totalUpdateCount,
                    _miniResConfig != null ? _miniResConfig.replaceResConfig.Count : 0,
                    resPatchInfo.updateList.Count));


                ZipManager.Instance.StarWork();
                //等待资源更新完毕
                while (_finishedCount < totalUpdateCount)
                {
                    while (downloadQueue.Count > 0 && _downloadingCount < MAX_DOWNLOADCOUNT)
                    {
                        var newResInfo = downloadQueue.Dequeue();
                        ++_downloadingCount;
                        StartCoroutine(DownloadAssetTask(remoteRoot, newResInfo));
                    }
                    yield return null;
                    PrintInfo(string.Format("Download resources, remaining{0}({1}/{2})", FormatBytes((long)_remainingSize, _versionConfig.resSize),
                        _finishedCount, totalUpdateCount));
                }
                ZipManager.Instance.StopWork();

                //更新过程中没有发生错误才将本地ResConfig版本号更新为最新版本
                if (!_fatalError)
                {
                    PrintInfo("Downloading resources is complete, and the game resources are being loaded");
                    _curResConfig.Version = resPatchInfo.EndVer;
                    _curResConfig.svnVersion = resPatchInfo.EndSvnVer;
                    _curResConfig.lz4CRC = resPatchInfo.EndLz4CRC;
                    _curResConfig.lzmaCRC = resPatchInfo.EndLzmaCRC;
                }

                //无论更新过程是否出错都需要保存一下ResConfig,否则重走更新流程时又需要重新下载一遍没有出错的资源了
                SaveResConfig(!_fatalError);
                if (_curResConfig.isMiniRes)
                {
                    SaveMiniResConfig();
                }
                isDownloadAsset = false;
                ////每次下载完新版本资源，清空Cache缓存，保证Cache的旧资源不会占用磁盘空间
                //GameDebug.Log(string.Format("GameResource:Clean Cache {0}", Caching.CleanCache()));

                stopwatch.Stop();
                var elapsed = stopwatch.Elapsed;
                GameDebug.Log(string.Format("Total time to download and update resources:{0:00}:{1:00}:{2:00}:{3:00}", elapsed.Hours, elapsed.Minutes, elapsed.Seconds, elapsed.Milliseconds / 10));

                if (!_fatalError)
                {
                    if (onFinish != null)
                        onFinish();
                }

                if (OnFinishResUpdate != null)
                    OnFinishResUpdate();

                //TalkingData.OnEventStep("GameUpdate", "Finish");
            }
        }

        private IEnumerator DownloadAssetTask(string remoteRoot, CLASSTAG_ResInfo newResInfo)
        {
            string url = newResInfo.GetRemotePath(remoteRoot);
            string outputDir = CLASSTAG_GameResPath.bundleRoot;

            yield return StartCoroutine(DownloadByUnityWebRequest(url, www =>
            {
                var fileBytes = www.downloadHandler.data;
                if (!string.IsNullOrEmpty(newResInfo.MD5))
                {
                    var fileMD5 = CLASSTAG_MD5Hashing.HashBytes(fileBytes);
                    if (fileMD5 != newResInfo.MD5)
                    {
                        OnDownloadAssetError(newResInfo, string.Format("url:{0} MD5 value does not match\n{1}|{2}", www.url, newResInfo.MD5, fileMD5));
                        return;
                    }
                }

                if (newResInfo.remoteZipType == CompressType.CustomZip)
                {
                    string outFolder = Path.GetDirectoryName(outputDir + "/" + newResInfo.bundleName);
                    var ms = new MemoryStream(fileBytes, false);
                    //资源解压更新完成，覆盖resConfig旧资源信息
                    ZipManager.Instance.Extract(url, outFolder, ms,
                        proxy => OnDownloadAssetFinish(newResInfo),
                        e => ThrowFatalException(string.Format("ZipExtract url:{0} error:{1}", url, e.Message)));
                }
                else
                {
                    //下载的是AssetBundle直接保存到指定目录，覆盖resConfig旧资源信息
                    string filePath = newResInfo.GetABPath(outputDir);
                    try
                    {
                        CLASSTAG_FileHelper.WriteAllBytes(filePath, fileBytes);
                        OnDownloadAssetFinish(newResInfo);
#if UNITY_EDITOR
                        GameDebug.Log(string.Format("Name:{0}\nFinish", filePath));
#endif
                    }
                    catch (Exception e)
                    {
                        ThrowFatalException("Save Asset Error: " + filePath + "\n" + e.Message);
                    }
                }
            }, e => OnDownloadAssetError(newResInfo, e)));
        }

        /// <summary>
        /// 更新资源失败,
        /// </summary>
        private void OnDownloadAssetError(CLASSTAG_ResInfo newResInfo, string error)
        {
            ++_finishedCount;
            --_downloadingCount;
            _remainingSize -= newResInfo.size;
            ThrowFatalException(error);
        }

        private void OnDownloadAssetFinish(CLASSTAG_ResInfo newResInfo)
        {
            newResInfo.isPackageRes = false;

            // 删除包内同key不同后缀资源
            CLASSTAG_ResInfo oldResInfo = null;
            if (_curResConfig.Manifest.TryGetValue(newResInfo.bundleName, out oldResInfo))
            {
                if (!oldResInfo.isPackageRes)
                {
                    var filePath = oldResInfo.GetABPath(CLASSTAG_GameResPath.bundleRoot);
                    try
                    {
                        CLASSTAG_FileHelper.DeleteFile(filePath);
                    }
                    catch (Exception e)
                    {
                        GameDebug.LogException(e);
                    }
                }
            }

            _curResConfig.Manifest[newResInfo.bundleName] = newResInfo;
            ++_finishedCount;
            --_downloadingCount;
            _remainingSize -= newResInfo.size;
        }


        public string GetCDNBundleRoot()
        {
            return _cdnUrlRoot + "/" + CLASSTAG_GameResPath.REMOTE_BUNDLE_ROOT;
        }

        #region 小包资源运行时下载
        private StringBuilder _minResDownloadInfo;

        public event Action OnMinResUpdateBegin;        //小包资源下载开始事件
        public event Action OnMinResUpdateFinish;       //小包资源下载成功事件
        public AutoUpgradeType AutoUpgrade = AutoUpgradeType.WIFI;             //小包自动下载资源开关
        public enum AutoUpgradeType
        {
            NONE,   //禁用小包资源自动下载
            WIFI,   //仅WIFI下自动下载
            ALL     //所有网络下自动下载
        }

        private long _totalMiniResSize;
        private long _remainMiniResSize;
        private Coroutine _upgradeTotalResTask;
        private HashSet<string> _miniResDownloadingSet;   //记录当前正在下载中的小包缺失资源Key

        /// <summary>
        /// 当前升级到整包缺少的资源总大小
        /// </summary>
        public long TotalMiniResSize
        {
            get { return _totalMiniResSize; }
        }

        /// <summary>
        /// 剩余小包资源大小
        /// </summary>
        public long RemainMiniResSize
        {
            get { return _remainMiniResSize; }
        }

        /// <summary>
        /// 标记是否正在升级为整包
        /// </summary>
        public bool IsUpgradeTotalRes
        {
            get { return _upgradeTotalResTask != null; }
        }

        /// <summary>
        /// 初始化小包资源下载信息,计算升级到整包还需下载多少资源
        /// </summary>
        public void SetupMiniResDownloadInfo()
        {
            if (_curResConfig == null || !_curResConfig.isMiniRes)
            {
                return;
            }

            _miniResDownloadingSet = new HashSet<string>();
            foreach (var pair in _miniResConfig.replaceResConfig)
            {
                var resInfo = _curResConfig.GetResInfo(pair.Key);
                if (resInfo != null)
                {
                    _totalMiniResSize += resInfo.size;
                }
                else
                {
                    GameDebug.LogError("The resource information of this small package does not exist in the current ResConfig, please check: " + pair.Key);
                }
            }

            _remainMiniResSize = _totalMiniResSize;
        }

        public bool StartUpgrade()
        {
            if (_upgradeTotalResTask != null)
                return false;

            _upgradeTotalResTask = StartCoroutine(UpgradeTotalRes());
            return true;
        }

        public bool StopUpgrade()
        {
            if (_upgradeTotalResTask == null)
                return false;

            StopCoroutine(_upgradeTotalResTask);
            _upgradeTotalResTask = null;
            return true;
        }

        /// <summary>
        /// 返回true,代表该资源是小包缺失资源,还没有下载下来
        /// </summary>
        /// <param name="bundleName"></param>
        /// <returns></returns>
        public bool ValidateMiniRes(string bundleName)
        {
            if (_miniResConfig != null)
            {
                if (_miniResConfig.replaceResConfig.ContainsKey(bundleName))
                {
                    return true;
                }

                var allDependencies = _curResConfig.FUNCTAG_GetAllDependencies(bundleName);
                for (int i = 0; i < allDependencies.Count; i++)
                {
                    string refBundleName = allDependencies[i];
                    if (_miniResConfig.replaceResConfig.ContainsKey(refBundleName))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// 返回true,代表该资源是小包资源,还没有下载下来,并返回其替代资源Key
        /// </summary>
        /// <param name="bundleName"></param>
        /// <param name="replaceKey"></param>
        /// <returns></returns>
        public bool TryGetReplaceRes(string bundleName, out string replaceKey)
        {
            replaceKey = "";
            return _miniResConfig != null && _miniResConfig.replaceResConfig.TryGetValue(bundleName, out replaceKey);
        }

        private IEnumerator UpgradeTotalRes()
        {
            var downloadQueue = new Queue<string>(_miniResConfig.replaceResConfig.Keys);
            string remoteRoot = GetCDNBundleRoot();

            while (downloadQueue.Count > 0)
            {
                while (downloadQueue.Count > 0 && _miniResDownloadingSet.Count < MAX_DOWNLOADCOUNT)
                {
                    string bundleName = downloadQueue.Dequeue();
                    StartCoroutine(DownloadMiniResTask(remoteRoot, bundleName));
                }
                yield return null;
            }

            while (_miniResConfig.replaceResConfig.Count > 0)
            {
                yield return new WaitForSeconds(0.5f);
            }

            //小包资源下载完毕,清空小包下载协程
            _upgradeTotalResTask = null;
            GameDebug.Log("Finish UpgradeTotalRes");
        }

        private IEnumerator DownloadMiniResTask(string remoteRoot, string bundleName)
        {
            //正在下载该小包资源或者已下载的直接跳过
            if (_miniResDownloadingSet.Contains(bundleName)
                || !_miniResConfig.replaceResConfig.ContainsKey(bundleName))
            {
                yield break;
            }

            var resInfo = _curResConfig.GetResInfo(bundleName);
            if (resInfo != null)
            {
                string url = resInfo.GetRemotePath(remoteRoot);
                _miniResDownloadingSet.Add(bundleName);
                yield return StartCoroutine(DownloadByUnityWebRequest(url,
                    www => SaveMiniRes(bundleName, www),
                    error =>
                    {
                        //下载失败从下载列表中移除
                        _miniResDownloadingSet.Remove(bundleName);
                        GameDebug.LogError("DownloadMinResTask: " + url + " failed");
                    }));
            }
            else
            {
                GameDebug.LogError("The resource information of this small package does not exist in the current ResConfig, please check: " + bundleName);
            }
        }

        /// <summary>
        /// 小包资源下载完毕后缓存到本地,并将小包信息从miniResConfig中移除
        /// </summary>
        /// <param name="bundleName"></param>
        /// <param name="www"></param>
        private void SaveMiniRes(string bundleName, UnityWebRequest www)
        {
            if (!_miniResConfig.replaceResConfig.ContainsKey(bundleName))
                return;

            var newResInfo = _curResConfig.GetResInfo(bundleName);
            var fileBytes = www.downloadHandler.data;
            if (!string.IsNullOrEmpty(newResInfo.MD5))
            {
                var fileMD5 = CLASSTAG_MD5Hashing.HashBytes(fileBytes);
                if (fileMD5 != newResInfo.MD5)
                {
                    _miniResDownloadingSet.Remove(bundleName);
                    GameDebug.LogError(string.Format("url:{0} MD5 value does not match\n{1}|{2}", www.url, newResInfo.MD5, fileMD5));
                    return;
                }
            }

            string outputDir = CLASSTAG_GameResPath.bundleRoot;
            if (newResInfo.remoteZipType == CompressType.CustomZip)
            {
                ZipManager.Instance.StarWork();
                string outFolder = Path.GetDirectoryName(outputDir + "/" + newResInfo.bundleName);
                var ms = new MemoryStream(fileBytes, false);
                ZipManager.Instance.Extract(www.url, outFolder, ms,
                    proxy => OnSaveMiniResFinish(newResInfo),
                    e =>
                    {
                        _miniResDownloadingSet.Remove(newResInfo.bundleName);
                        GameDebug.LogError(string.Format("ZipExtract url:{0}\nerror:{1}", www.url, e.Message));
                    });
            }
            else
            {
                string filePath = newResInfo.GetABPath(CLASSTAG_GameResPath.bundleRoot);
                FileThreadTask.StartTask(filePath, fileBytes, false,
                    () =>
                    {
                        OnSaveMiniResFinish(newResInfo);

                    },
                    (e) =>
                    {
                        _miniResDownloadingSet.Remove(newResInfo.bundleName);
                        Debug.LogError("Save MiniRes Error: " + filePath + "\n" + e.Message);
                    });
            }
        }

        public void SaveMiniRes(string bundleName, byte[] data, string url)
        {
            if (!_miniResConfig.replaceResConfig.ContainsKey(bundleName))
                return;

            var newResInfo = _curResConfig.GetResInfo(bundleName);
            var fileBytes = data;
            if (!string.IsNullOrEmpty(newResInfo.MD5))
            {
                var fileMD5 = CLASSTAG_MD5Hashing.HashBytes(fileBytes);
                if (fileMD5 != newResInfo.MD5)
                {
                    _miniResDownloadingSet.Remove(bundleName);
                    GameDebug.LogError(string.Format("url:{0} MD5 value does not match\n{1}|{2}", url, newResInfo.MD5, fileMD5));
                    return;
                }
            }

            string outputDir = CLASSTAG_GameResPath.bundleRoot;
            string filePath = newResInfo.GetABPath(CLASSTAG_GameResPath.bundleRoot);

            CLASSTAG_FileHelper.WriteAllBytes(filePath, fileBytes);
            OnSaveMiniResFinish(newResInfo);

        }

        /// <summary>
        /// 小包资源下载保存成功后,更新本地miniResConfig和resConfig的信息
        /// </summary>
        /// <param name="newResInfo"></param>
        private void OnSaveMiniResFinish(CLASSTAG_ResInfo newResInfo)
        {
            //小包资源下载完毕,将包内资源标志置为false,并从小包配置信息中移除
            _miniResConfig.replaceResConfig.Remove(newResInfo.bundleName);
            newResInfo.isPackageRes = false;
            isDownloadAsset = true;
#if UNITY_EDITOR
            if (_minResDownloadInfo == null)
                _minResDownloadInfo = new StringBuilder();
            _minResDownloadInfo.AppendLine(newResInfo.bundleName);
            GameDebug.Log(string.Format("=======Download the small package resource successfully:{0}", newResInfo.bundleName));
#endif

            long remainSize = _remainMiniResSize - newResInfo.size;
            _remainMiniResSize = remainSize > 0 ? remainSize : 0L;
            //每下载5个资源,保存一下配置,防止玩家意外终止游戏进程,丢失下载信息
            if (_miniResConfig.replaceResConfig.Count % 10 == 0)
            {
                //小包资源全部下载完毕,将当前小包标记置为false
                if (_miniResConfig.replaceResConfig.Count == 0)
                {
                    isDownloadAsset = false;
                    _curResConfig.isMiniRes = false;
                    ZipManager.Instance.StopWork();
                }
                SaveMiniResConfigAsync();
                SaveResConfigAsync();
            }

            if (OnMinResUpdateFinish != null)
                OnMinResUpdateFinish();

            //缓存完毕,从下载队列中移除
            _miniResDownloadingSet.Remove(newResInfo.bundleName);
        }

        /// <summary>
        /// 静默下载小包资源,等下载完毕后再次触发资源加载处理回调
        /// </summary>
        /// <param name="loadRequest"></param>
        public void SlientDownloadMiniRes(AssetLoadRequest loadRequest)
        {
            if (AutoUpgrade == AutoUpgradeType.NONE)
                return;
            if (AutoUpgrade == AutoUpgradeType.WIFI && PlatformAPI.getNetworkType() != PlatformAPI.NET_STATE_WIFI)
                return;
            if (_miniResDownloadingSet.Contains(loadRequest.bundleName))
                return;

            StartCoroutine(SlientDownloadMiniResTask(loadRequest));
        }

        private IEnumerator SlientDownloadMiniResTask(AssetLoadRequest loadRequest)
        {
            var newResInfo = _curResConfig.GetResInfo(loadRequest.bundleName);
            if (newResInfo != null)
            {
                if (OnMinResUpdateBegin != null)
                    OnMinResUpdateBegin();

                string remoteRoot = GetCDNBundleRoot();

                //必须先下载该资源,保证不会重复下载该资源,再下载其依赖资源
                StartCoroutine(DownloadMiniResTask(remoteRoot, loadRequest.bundleName));

                if (newResInfo.Dependencies.Count > 0)
                {
                    var allDependencies = _curResConfig.FUNCTAG_GetAllDependencies(loadRequest.bundleName);
                    for (int i = 0; i < allDependencies.Count; i++)
                    {
                        string refBundleName = allDependencies[i];
                        StartCoroutine(DownloadMiniResTask(remoteRoot, refBundleName));
                    }

                    //等待所有依赖资源下载完毕
                    while (allDependencies.Any(s => _miniResDownloadingSet.Contains(s)))
                    {
                        yield return null;
                    }
                }

                //等待该资源下载完毕
                while (_miniResDownloadingSet.Contains(loadRequest.bundleName))
                {
                    yield return null;
                }

                //所有相关资源下载完毕,重新触发整包资源加载流程
                AssetManager.Instance._loadingQueue.Enqueue(loadRequest.bundleName, 0);
                AssetManager.Instance.ProcessLoadQueue();
            }
        }

        private bool saveMiniResConfigMark = false;
        private void SaveMiniResConfigAsync()
        {
            if (_miniResConfig == null)
                return;
            if (saveMiniResConfigMark)
                return; ;

            string savePath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.MINIRESCONFIG_FILE);
            string json = JsonMapper.ToJson(_miniResConfig);
            byte[] bytes = Encoding.UTF8.GetBytes(json);

            saveMiniResConfigMark = true;
            FileThreadTask.StartTask(savePath, bytes, true,
                () =>
                {
                    saveMiniResConfigMark = false;
                },
                (e) =>
                {
                    saveMiniResConfigMark = false;
                    GameDebug.LogException(e);
                });
        }

        #endregion

        #endregion

        #region WWW,UnityWebRequest读取数据接口

        /// <summary>
        /// 通过WWW读取或者下载文件,可用于读取本地文件,也可以下载服务器上的
        /// </summary>
        public void LoadFileByWWW(string url, Action<WWW> onFinish, Action<string> onError = null, int maxRetry = 1, float retryDelay = 0.5f, bool converText = true)
        {
            if (string.IsNullOrEmpty(url))
            {
                if (onError != null)
                    onError("LoadFileByWWW:url is null");
                else
                    GameDebug.LogError("LoadFileByWWW:url is null");
                return;
            }
            StartCoroutine(DownloadByWWW(url, onFinish, onError, maxRetry, retryDelay, converText));
        }

        private IEnumerator DownloadByWWW(string url, Action<WWW> onFinish, Action<string> onError, int maxRetry = MAX_RETRYCOUNT, float retryDelay = 0.5f, bool converText = true)
        {
            int retry = 0;
            while (retry++ < maxRetry)
            {
                using (var www = new WWW(url))
                {
                    yield return www;

                    if (!string.IsNullOrEmpty(www.error))
                    {
                        if (retry >= maxRetry)
                        {
                            string error = string.Format("Load url error:{0}", www.url);
                            if (onError != null)
                                onError(error);
                            else
                                GameDebug.LogError(error);
                        }
                        else
                        {
                            GameDebug.Log(string.Format("Try again Link url : {0} , time : {1}", url, retry));
                            yield return new WaitForSeconds(retryDelay);
                        }
                    }
                    else
                    {
                        //转换为text需要转换文件编码非常耗时，某些操作下不需要
                        if (converText && www.text.StartsWith("<html>"))
                        {
                            string error = string.Format("Load url error:{0}", www.url);
                            if (onError != null)
                                onError(error);
                            else
                                GameDebug.LogError(error);
                        }
                        else
                        {
                            if (onFinish != null)
                                onFinish(www);
                        }

                        break; //跳出重试循环
                    }
                }
            }
        }

        /// <summary>
        /// 一般用于加载一些服务器上的临时资源,如:个人空间的图片,注意:不支持读取jar包内的文件
        /// </summary>
        public void LoadFileByUnityWebRequest(string url, Action<UnityWebRequest> onFinish, Action<string> onError)
        {
            LoadFileByUnityWebRequest(UnityWebRequest.Get(url), onFinish, onError);
        }

        public void LoadFileByUnityWebRequest(UnityWebRequest www, Action<UnityWebRequest> onFinish, Action<string> onError)
        {
            if (www == null || string.IsNullOrEmpty(www.url))
            {
                if (onError != null)
                    onError("LoadFileByWWW:url is null");
                else
                    GameDebug.LogError("LoadFileByWWW:url is null");
                return;
            }

            StartCoroutine(LoadFileByUnityWebRequestTask(www, onFinish, onError));
        }

        private IEnumerator LoadFileByUnityWebRequestTask(UnityWebRequest www, Action<UnityWebRequest> onFinish, Action<string> onError)
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                string error = string.Format("Load url:{0}\nerror:{1}", www.url, www.error);
                if (onError != null)
                    onError(error);
                else
                    GameDebug.LogError(error);
            }
            else
            {
                if (onFinish != null)
                    onFinish(www);
            }
        }

        public byte[] DownloadSync(string url)
        {
            WebRequest request = WebRequest.Create(url); 
            WebResponse response = request.GetResponse();
            Stream stream = response.GetResponseStream();

            int pos = 0;
            int count = 0;
            int byteLen = (int)response.ContentLength;
            byte[] data = new byte[response.ContentLength];
            byte[] buffer = new byte[1024];
            while ((count = stream.Read(buffer, 0, buffer.Length)) != 0)
            {
                Buffer.BlockCopy(buffer, 0, data, pos, count);
                pos += count;
            }

            stream.Close();
            response.Close();
            return data;
        }


        private IEnumerator DownloadByUnityWebRequest(string url, Action<UnityWebRequest> onFinish, Action<string> onError, int maxRetry = MAX_RETRYCOUNT, float retryDelay = 2.0f)
        {
            GameDebug.Log("DownloadByUnityWebRequest  " + url);
            int retry = 0;
            while (retry++ < maxRetry)
            {
                using (var www = UnityWebRequest.Get(url))
                {
                    yield return www.SendWebRequest();

                    if (www.isNetworkError)
                    {
                        if (retry >= maxRetry)
                        {
                            string error = string.Format("Load url error:{0}", www.url);
                            if (onError != null)
                                onError(error);
                            else
                                GameDebug.LogError(error);
                        }
                        else
                        {
                            GameDebug.Log(string.Format("Try again Link url : {0} , time : {1}", www.url, retry));
                            yield return new WaitForSeconds(retryDelay);
                        }
                    }
                    else
                    {
                        if (www.downloadHandler.text.StartsWith("<html>"))
                        {
                            string error = string.Format("Load url error:{0}", www.url);
                            if (onError != null)
                                onError(error);
                            else
                                GameDebug.LogError(error);
                        }
                        else
                        {
                            if (onFinish != null)
                                onFinish(www);
                        }

                        break; //跳出重试循环
                    }
                }
            }
        }

        #endregion
        
        public void Dispose()
        {
#if UNITY_EDITOR
            if (_minResDownloadInfo != null)
            {
                string logPath = Path.Combine(CLASSTAG_GameResPath.EXPORT_FOLDER, "minResLog.txt");
                CLASSTAG_FileHelper.WriteAllText(logPath, _minResDownloadInfo.ToString());
            }
#endif

            if (AssetManager.ResLoadMode != AssetManager.LoadMode.EditorLocal)
            {
                if (_cleanUpResFlag)
                {
                    CleanUpResFolder();
                }
                else
                {
                    SaveResConfig();
                    SaveMiniResConfig();
                }
            }

            //清空异步加载信息
            this.StopAllCoroutines();
            //if (_loadingQueue != null)
            //    _loadingQueue.Clear();
            //if (_loadRequestDic != null)
            //    _loadRequestDic.Clear();
            _curResConfig = null;
        }

        #region 清空包外更新资源

        /// <summary>
        ///     设置清空包外游戏资源标记，在退出游戏时做清空处理
        /// </summary>
        public void MarkCleanUpResFlag()
        {
            _cleanUpResFlag = true;
        }

        /// <summary>
        ///     清空persistentDataPath下的所有资源目录，以及Cache目录下的资源
        /// </summary>
        private void CleanUpResFolder()
        {
            _cleanUpResFlag = false;

            CleanUpDllFolder();
            CleanUpBundleResFolder();
        }

        public void CleanUpBundleResFolder()
        {
            try
            {
                //清空包外script
                string scriptPath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.SCRIPT_FILE);
                if (File.Exists(scriptPath))
                {
                    File.Delete(scriptPath);
                    GameDebug.Log("GameResource:Remove scriptPath File: " + scriptPath);
                }

                //清空包外data
                string dataPath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.DATA_SCRIPT_FILE);
                if (File.Exists(dataPath))
                {
                    File.Delete(dataPath);
                    GameDebug.Log("GameResource:Remove dataPath File: " + dataPath);
                }

                //清空包外VersionConfig
                string verConfigPath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.VERSIONCONFIG_FILE);
                if (File.Exists(verConfigPath))
                {
                    File.Delete(verConfigPath);
                    GameDebug.Log("GameResource:Remove VersionConfig File: " + verConfigPath);
                }

                //清空包外ResConfig
                string resConfigPath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.RESCONFIG_FILE);
                if (File.Exists(resConfigPath))
                {
                    File.Delete(resConfigPath);
                    GameDebug.Log("GameResource:Remove ResConfig File: " + resConfigPath);
                }

                //清空包外miniResConfig
                string miniResConfigPath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.MINIRESCONFIG_FILE);
                if (CLASSTAG_FileHelper.IsExist(miniResConfigPath))
                {
                    File.Delete(miniResConfigPath);
                    GameDebug.Log("GameResource:Remove MiniResConfig File: " + miniResConfigPath);
                }

                //清空包外资源Bundle目录
                string gameResDir = CLASSTAG_GameResPath.bundleRoot;
                if (Directory.Exists(gameResDir))
                {
                    Directory.Delete(gameResDir, true);
                    GameDebug.Log("GameResource:Remove GameRes Folder: " + gameResDir);
                }

                GameDebug.Log("GameResource:Clean Cache " + Caching.ClearCache());
            }
            catch (Exception e)
            {
                GameDebug.LogError(e.Message);
            }
        }


        public void CleanUpDllFolder()
        {
            try
            {

                //清空包外dllVersion
                string dllVersionPath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.DLLVERSION_FILE);
                if (CLASSTAG_FileHelper.IsExist(dllVersionPath))
                {
                    File.Delete(dllVersionPath);
                    GameDebug.Log("GameResource:Remove DllVersion File: " + dllVersionPath);
                }

                //清空包外备份dll更新目录
                var dllBackupRootDir = CLASSTAG_GameResPath.dllBackupRoot;
                if (Directory.Exists(dllBackupRootDir))
                {
                    Directory.Delete(dllBackupRootDir, true);
                    GameDebug.Log("GameResource:Remove DllBackupRoot Folder: " + dllBackupRootDir);
                }

                //清空包外dll更新目录
                string dllRootDir = CLASSTAG_GameResPath.dllRoot;
                if (Directory.Exists(dllRootDir))
                {
                    Directory.Delete(dllRootDir, true);
                    GameDebug.Log("GameResource:Remove DllRoot Folder: " + dllRootDir);
                }
            }
            catch (Exception e)
            {
                GameDebug.LogError(e.Message);
            }
        }
        #endregion

        #region Helper Func

        ///// <summary>
        ///// 判断是否使用包内资源
        ///// </summary>
        ///// <param name="bundleName"></param>
        ///// <returns></returns>
        //private bool UsePackageRes(string bundleName)
        //{
        //    if (_packageResConfig == null || _curResConfig == null)
        //        return false;

        //    var packageResInfo = _packageResConfig.GetResInfo(bundleName);
        //    if (packageResInfo != null)
        //    {
        //        var curResInfo = _curResConfig.GetResInfo(bundleName);
        //        //这种情况是玩家安装了最新版本的游戏包,且没有发生过资源版本更新.
        //        if (packageResInfo.CRC == curResInfo.CRC)
        //        {
        //            return curResInfo.isPackageRes;
        //        }
        //        else
        //        {
        //            if (_curResConfig.Version > _packageResConfig.Version)
        //            {
        //                //这种情况下是玩家安装了旧版本游戏包,并且通过版本更新升级到最新版本
        //                //所以包外资源打包时间比包内的新
        //                return false;
        //            }
        //            else
        //            {
        //                //这种情况最为特殊,即包内资源比包外新,只会发生在玩家安装了旧版本游戏包,进入过游戏,然后AFK了一段时间,
        //                //期间发生过版本更新,然后玩家没有通过进入游戏走游戏内的版本更新流程,而是直接安装了最新版本游戏包,进行覆盖安装时就会出现这种情况
        //                return true;
        //            }
        //        }
        //    }

        //    return false;
        //}

        private void SaveResConfig(bool saveServerType = false)
        {
            if (_curResConfig == null)
                return;

            string savePath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.RESCONFIG_FILE);
            _curResConfig.SaveFile(savePath, true);
        }

        private bool saveResConfigMark = false;
        private void SaveResConfigAsync(bool saveServerType = false)
        {
            if (_curResConfig == null)
                return;
            if (saveResConfigMark)
                return;

            string savePath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.RESCONFIG_FILE);
            byte[] fileBytes = _curResConfig.SerializeToMemoryStream();
            saveResConfigMark = true;
            FileThreadTask.StartTask(savePath, fileBytes, true,
                () =>
                {
                    saveResConfigMark = false;
                },
                (e) =>
                {
                    saveResConfigMark = false;
                    GameDebug.LogException(e);
                });

        }

        private void SaveDllVersion(bool saveServerType = false)
        {
            if (_dllVersion == null)
                return;

            string savePath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.DLLVERSION_FILE);
            CLASSTAG_FileHelper.SaveJsonObj(_dllVersion, savePath);
        }

        private void SaveMiniResConfig()
        {
            if (_miniResConfig == null)
                return;
            string savePath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.MINIRESCONFIG_FILE);
            CLASSTAG_FileHelper.SaveJsonObj(_miniResConfig, savePath, true);
        }

        public void SaveVersionConfig()
        {
            if (_versionConfig == null)
                return;

            string savePath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.VERSIONCONFIG_FILE);
            CLASSTAG_FileHelper.SaveJsonObj(_versionConfig, savePath);
        }

        /// <summary>
        /// 读取本地最近一次缓存的VersionConfig信息
        /// </summary>
        /// <returns></returns>
        private CLASSTAG_VersionConfig ReadLocalVersionConfig()
        {
            string savePath = Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, CLASSTAG_GameResPath.VERSIONCONFIG_FILE);
            if (CLASSTAG_FileHelper.IsExist(savePath))
            {
                return CLASSTAG_FileHelper.ReadJsonFile<CLASSTAG_VersionConfig>(savePath);
            }
            return null;
        }

        /// <summary>
        /// 判断当前运行时平台是否支持更新Dll
        /// </summary>
        /// <returns></returns>
        public static bool IsSupportUpdateDllPlatform()
        {
#if UNITY_IOS
            return false;
#else
            return true;
#endif
        }

        private bool ValidateStorageSpace(long needSize)
        {
            if (Application.isMobilePlatform)
            {
                long freeSpace = PlatformAPI.getExternalStorageAvailable() * 1024L;
                //if (freeSpace < needSize)
                //{
                //    if (_loadErrorHandler != null)
                //    {
                //        _loadErrorHandler(string.Format("当前剩余手机空间不足，需要{0}，请清理手机空间再尝试。", FormatBytes(needSize - freeSpace)));
                //    }
                //    return false;
                //}
            }

            return true;
        }

        #endregion


        #region Debug Func

        private bool _fatalError;

        private void ThrowFatalException(string msg)
        {
            _fatalError = true;
            if (_loadErrorHandler != null)
            {
                _loadErrorHandler(msg);
            }
            else
            {
                PrintInfo(msg);
            }
            throw new Exception(msg);
        }

        private void PrintInfo(string msg)
        {
            GameDebug.Log(msg);
            if (_logMessageHandler != null)
                _logMessageHandler(msg);
        }

        public static void LogMemory()
        {
            if (Application.isMobilePlatform)
            {
                long freeMemory = PlatformAPI.getFreeMemory() / 1024;
                long totalMemory = PlatformAPI.getTotalMemory() / 1024;
                GameDebug.Log("memory " + freeMemory + "/" + totalMemory);
            }
        }

        public static string FormatBytes(long bytes, float resRatio = 1f)
        {
            // Adjust the format string to your preferences. For example "{0:0.#}{1}" would
            // show a single decimal place, and no space.
            return string.Format("{0:0.##} MB", (float)bytes/resRatio/1024 / 1024);
        }

        #endregion
    }
}