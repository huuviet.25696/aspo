using System;
using System.Text;
using LITJson;
using UnityEngine;
using System.Collections.Generic;
using AssetPipeline;

public static class CLASSTAG_GameSetting
{
    public enum DebugInfoType
    {
        None,
        Default,
        Verbose
    }

    public enum UpdateMode
    {
        NoUpdate,
        TestUpdate,
        Update
    }

    //public enum PlatformType
    //{
    //    Win = 1,
    //    Android = 2,
    //    IOS = 3,
    //}

    public const string GameSettingFileName = "GameSettingDataV2";
    public const string Config_WritePathV2 = "Assets/StreamingAssets/" + GameSettingFileName;

    public static CLASSTAG_GameSettingData _gameSettingData;



    public static bool CheckUpdate
    { 
        get; 
        private set; 
    }

    public static UpdateMode updateMode;

    public static bool ShowUpdateLog
    { 
        get;
        private set;
    }


    /// <summary>
    ///额外配置的分区标识，通过ProjectIconSetting传入
    /// </summary>
    public static string ChannelAreaFlag = "";

    public static void FUNCTAG_Setup()
    {
        _gameSettingData = FUNCTAG_LoadGameSettingData();
        if (_gameSettingData != null)
        {
            //Platform = _gameSettingData.platformType;
            HttpRoot = _gameSettingData.httpRoot;
            TestHttpRoot = _gameSettingData.testHttpRoot;
            ResDir = _gameSettingData.resdir;
            Channel = _gameSettingData.channel;
            ShowUpdateLog = _gameSettingData.showUpdateLog;
            updateMode = _gameSettingData.updateMode;
            CLASSTAG_GameVersion.packageDllVersion = _gameSettingData.dllVersion;
            CLASSTAG_GameVersion.packageResVersion = _gameSettingData.resVersion;
            CLASSTAG_GameVersion.packageSvnVersion = _gameSettingData.svnVersion;
        }
        else
        {
            GameDebug.LogError(" GameSettingData Setup Error !!");
        }
    }

    /// <summary>
    /// 初始化服务器Http请求地址
    /// </summary>
    /// <param name="config"></param>
    public static void SetupServerUrlConfig(CLASSTAG_StaticConfig config)
    {
        if (config == null)
        {
            GameDebug.LogError("ServerUrlConfig is null ");
            return;
        }

        string resDir = ResDir;
        CDN_SERVER_LIST.Clear();
        CDN_SERVER_LIST.Add(config.masterCdnUrl + "/" + ResDir + "/" + PlatformTypeName);
        if (!string.IsNullOrEmpty(config.slaveCdnUrl))
        {
            CDN_SERVER_LIST.Add(config.slaveCdnUrl + "/" + ResDir + "/" + PlatformTypeName);
        }
        if (!string.IsNullOrEmpty(config.srcCdnUrl))
        {
            CDN_SERVER_LIST.Add(config.srcCdnUrl + "/" + ResDir + "/" + PlatformTypeName);
        }
        CDN_SERVER = CDN_SERVER_LIST[0];
    }


    public static CLASSTAG_GameSettingData FUNCTAG_LoadGameSettingData()
    {
        var json = AssetManager.LoadStreamingAssetsText(GameSettingFileName);
        if (json != null)
        {
            CLASSTAG_GameSettingData data = JsonMapper.ToObject<CLASSTAG_GameSettingData>(json);
            return data;
        }
        else
        {
            return null;
        }
    }

	public static string GetGameSettingJson()
	{
		if (_gameSettingData != null)
		{
			return JsonMapper.ToJson(_gameSettingData);
		}
		return "";
	}

    #region 平台相关属性


    public static string HttpRoot { get; set; }



    public static string TestHttpRoot { get; set; }

    /// <summary>
    ///     资源目录
    /// </summary>
    public static string ResDir { get; set; }


    /// <summary>
    ///     平台枚举值
    /// </summary>
    //public static PlatformType Platform { get; set; }
    
    public static string PlatformTypeName
    {
        get
        {
#if UNITY_ANDROID
            return "android";
#elif UNITY_IPHONE
        return "ios";
#elif UNITY_STANDALONE_OSX
        return "macos";
#else
        return "win";
#endif
        }
    }

    /// <summary>
    ///     融合sdk，运营商，如sm， demi， yijie
    /// </summary>
    public static string Channel = "";

    public static List<string> PlatformResPathList
    {
        get
        {
            List<string> list = new List<string>();
            for (int i = 0; i < CDN_SERVER_LIST.Count; i++)
            {
                list.Add(CDN_SERVER_LIST[i]);
            }
            return list;
        }
    }

    /// <summary>
    ///     配置后缀
    /// </summary>
    public static string ConfigSuffix
    {
        get
        {
            if (_gameSettingData == null)
            {
                return "";
            }
            else
            {
                string suffix = _gameSettingData.configSuffix;
                if (string.IsNullOrEmpty(suffix))
                {
                    suffix = "";
                }
                return suffix;
            }
        }
    }

    #endregion

    ///// <summary>
    /////     CONFIG服务器地址
    ///// </summary>
    private static string _CONFIG_SERVER = "";

    /// <summary>
    ///     CONFIG服务器地址
    /// </summary>
    public static string CONFIG_SERVER
    {
        get { return _CONFIG_SERVER; }
        set
        {
            _CONFIG_SERVER = value;
        }
    }

    ///// <summary>
    /////     CDN服务器地址列表，提供轮询
    ///// </summary>
    public static List<string> CDN_SERVER_LIST = new List<string>();

    ///// <summary>
    /////     CDN服务器地址
    ///// </summary>
    public static string CDN_SERVER = "";
}

public class CLASSTAG_GameSettingData
{
    //渠道
    public string channel = "";
    ////游戏类型
    public string gameType;
    ////域类型
    public string domainType;

    //正式环境入口域名
    public string httpRoot = "";

    //测试环境域名
    public string testHttpRoot = "";

    //资源目录
    public string resdir = "";

    //游戏名字
    public string gamename = "";

    //配置后缀
    public string configSuffix = "";

    public CLASSTAG_GameSetting.DebugInfoType logType;

    public bool showUpdateLog = false;

    public CLASSTAG_GameSetting.UpdateMode updateMode;


    public int frameVersion;

    public int dllVersion;

    public int resVersion;

    public int svnVersion;
	[LITJson.JsonIgnore]
	//替换c#代码
	public string oriClassTag
	{
		get
		{
			return PlayerPrefs.GetString("GameSettingData.oriClassTag");
		}
		set
		{
			PlayerPrefs.SetString("GameSettingData.oriClassTag", value);
		}
	}
	[LITJson.JsonIgnore]
	public string oriFuncTag
	{
		get
		{
			return PlayerPrefs.GetString("GameSettingData.oriFuncTag");
		}
		set
		{
			PlayerPrefs.SetString("GameSettingData.oriFuncTag", value);
		}
	}
	[LITJson.JsonIgnore]
	public string replaceClassTag
	{
		get
		{
			return PlayerPrefs.GetString("GameSettingData.replaceClassTag");
		}
		set
		{
			PlayerPrefs.SetString("GameSettingData.replaceClassTag", value);
		}
	}
	[LITJson.JsonIgnore]
	public string replaceFuncTag
	{
		get
		{
			return PlayerPrefs.GetString("GameSettingData.replaceFuncTag");
		}
		set
		{
			PlayerPrefs.SetString("GameSettingData.replaceFuncTag", value);
		}
	}

    /*public string loginTipText = @"本网络游戏适合年满16周岁以上的用户使用，请确定已如实进行实名注册，为了您的健康，请合理控制游戏时间。
抵制不良游戏，拒绝盗版游戏。注意自我保护，谨防受骗上当，适度游戏益脑，沉迷游戏伤身，合理安排时间，享受健康生活。
闽网文〔2015〕 1638-025号 文网游备字 〔2017〕Ｍ-RPG 1762号 新广出审【2017】9776号ISBN 978-7-498-02659-0
著作权人：上海赐麓网络科技有限公司 出版单位：三辰影库音像出版社有限公司";*/
    public string loginTipText = @"This online game is suitable for users over 18+ years of age. Please do not play over 180 minutes per day";

}
