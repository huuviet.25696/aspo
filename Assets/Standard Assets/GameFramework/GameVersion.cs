using AssetPipeline;
using System.IO;
using UnityEngine;


public class CLASSTAG_GameVersion
{
    //框架版本，谨改
    public static int frameworkVersion = 1;

    //当前dll版本
    public static int dllVersion = 1;

    //包外资源版本,不在此修改
    public static int resVersion;

    //包外svn版本,不在此修改
    public static int svnVersion;

    //包内dll版本,不在此修改
    public static int packageDllVersion;

    //包内资源版本,不在此修改
    public static int packageResVersion;

    //包内svn版本,不在此修改
    public static int packageSvnVersion;

     public static string PackageFullVersion
    {
        get
        {
            return string.Format("{0}.{1}.{2}", frameworkVersion, packageDllVersion, packageResVersion);

        }
    }

     public static string FullVersion
     {
         get
         {
             return string.Format("{0}.{1}.{2}", frameworkVersion, dllVersion, resVersion);
         }
     }

}

