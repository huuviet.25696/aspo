//#define DONT_USE_WEB_LIB

using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.Networking;
using BestHTTP;

public class SimpleWWW : MonoBehaviour
{
	/// <summary>
	/// HTTP连接模式
	/// 	Continued_Connect :　 持续的连接
	/// 	Short_Connect	  :   短连接
	/// </summary>
	public enum ConnectionType
	{
		Continued_Connect = 0,
		Short_Connect     = 1
	}

    public void Receive(CLASSTAG_Request request, System.Action<byte[]> bytesDlegate, System.Action<float> progressDlegate = null, System.Action<Exception> errorDlegate = null, bool uncompress = false,ConnectionType connetType = ConnectionType.Continued_Connect)
    {
        StartCoroutine(FUNCTAG__Receive(request, bytesDlegate, progressDlegate, errorDlegate, uncompress, connetType));
    }


	public void Send( CLASSTAG_Request request , byte[] data, System.Action action = null, System.Action<Exception> errorDlegate = null)
	{
		StartCoroutine(_Sent(request, data, action, errorDlegate));		
	}
	
	//最多重连数量
	private readonly int MAXIMUMREDIRECTS = 3;

	private Dictionary<CLASSTAG_Request, long> _RequestTimeoutMaps;

	void Awake()
	{
		_RequestTimeoutMaps = new Dictionary<CLASSTAG_Request, long>();
	}

	IEnumerator FUNCTAG__Receive(CLASSTAG_Request request, System.Action<byte[]> bytesDelegate, System.Action<float> progressDlegate = null, System.Action<Exception> errorDlegate = null,  bool uncompress = false,ConnectionType connetType = ConnectionType.Continued_Connect )
	{
		if ( request != null )
		{
			int index = AddRequstList(request);


            HTTPRequest www = null ;  
			int retry = 0;
			while (retry++ < MAXIMUMREDIRECTS)
			{
				request.progress = 0f;

				AddRequestTimeout(request);

				if( connetType == ConnectionType.Continued_Connect )
				{
					www = new HTTPRequest (new Uri( request.requestURL))/*, null, request.Headers)*/;
				}
				else
				{
					//var postHeader = new Dictionary<string, string>();
					Dictionary<string, string> postHeader = new Dictionary<string, string>();
					if (request.Headers != null)
					{
						foreach (var header in request.Headers)
						{
							postHeader[header.Key] = header.Value;
						}
					}
					//postHeader.Add("Connection", "close");
					
					//postHeader.Add("Content-Type", "text/json");
					www = new HTTPRequest(new Uri(request.requestURL))/*, null, postHeader )*/;
				}

                
                www.Send();
                bool timeOut = false;

				while (www.State < HTTPRequestStates.Finished)
				{
					
					yield return new WaitForEndOfFrame();
				}


				if( !timeOut && www.State == HTTPRequestStates.Finished )
				{
					if (www.Response.DataAsText.StartsWith("<html>"))
					{
						CLASSTAG_CdnReportHelper.FUNCTAG_Report(request.requestURL, www.Response.DataAsText);
						if (errorDlegate != null) errorDlegate( new Exception(www.Response.DataAsText));
					}
					else
					{
						request.bytes = www.Response.Data;

						if (uncompress)
						{
							request.FUNCTAG_Uncompress();
							while (!request.isDone)
							{
								yield return new WaitForEndOfFrame();
							}
						}					

						if (progressDlegate != null) progressDlegate(1.0f);
						if (bytesDelegate   != null) bytesDelegate(request.bytes);
					}

					break;
				}
				else
				{
					if( retry >= MAXIMUMREDIRECTS)
					{
						if (timeOut)
						{
							CLASSTAG_CdnReportHelper.FUNCTAG_Report(request.requestURL, "请求超时");
							if (errorDlegate != null) errorDlegate( new Exception( "请求超时" ));
						}
						else
						{
							CLASSTAG_CdnReportHelper.FUNCTAG_Report(request.requestURL, www.State.ToString());
							if (errorDlegate != null) errorDlegate( new Exception(www.State.ToString()));
						}
					}
					else
					{
						if (timeOut)
						{
							GameDebug.Log( string.Format( "TimeOut Try again Link url : {0} , time : {1}", request.requestURL, retry ));
						}
						else
						{
							GameDebug.Log( string.Format( "Try again Link url : {0} , time : {1}", request.requestURL, retry ));
						}
						yield return new WaitForSeconds( 1.0f );
					}
				}
			}

			if( www != null )
			{
				www.Dispose();
				www = null;				
			}
			RemoveRequestTimeout(request);
			ClearRequstList(index);
		}
	}

	private bool CheckRequestTimeout(CLASSTAG_Request request, float progress=0f)
	{
		if (_RequestTimeoutMaps.ContainsKey(request))
		{
			long time = _RequestTimeoutMaps[request];
			double passTime = (DateTime.Now.Ticks - time)/ 10000000.0; 
			float checkTime = 15f; //检测超时时间5秒
			if (request.progress != progress)
			{
				_RequestTimeoutMaps[request] = DateTime.Now.Ticks;
				request.progress = progress;
			}
			if (passTime > checkTime)
			{
				GameDebug.Log(string.Format("passTime={0} checkTime={1}", passTime, checkTime));
				return true;
			}
		}

		return false;
	}

	private void AddRequestTimeout(CLASSTAG_Request request)
	{
		if (_RequestTimeoutMaps.ContainsKey(request))
		{
			_RequestTimeoutMaps[request] = DateTime.Now.Ticks;
		}
		else
		{
			_RequestTimeoutMaps.Add(request, DateTime.Now.Ticks);
		}
	}

	private void RemoveRequestTimeout(CLASSTAG_Request request)
	{
		if (_RequestTimeoutMaps.ContainsKey(request))
		{
			_RequestTimeoutMaps.Remove(request);
		}
	}

	IEnumerator _Sent(CLASSTAG_Request request, byte[] data, System.Action action = null, System.Action<Exception> errorDlegate = null)
	{
		int index = AddRequstList(request);

		var postHeader = new Dictionary<string, string>();
		postHeader.Add("Content-Type", "application/octet-stream");
		postHeader.Add("Content-Length", data.Length.ToString());
		postHeader.Add("Connection", "close");

		WWW sendWWW = new WWW( request.requestURL, data, postHeader);


		yield return sendWWW;

		if( sendWWW.isDone && sendWWW.error == null )
		{
			if ( action != null ) action();
		}
		else
		{
			if (errorDlegate != null) errorDlegate(new Exception( sendWWW.error ));
			throw request.exception;
		}		

		ClearRequstList(index);
	}

	List<System.Action> onQuit = new List<System.Action>();
	public void OnQuit(System.Action fn)
	{
		onQuit.Add(fn);
	}

	CLASSTAG_Request[] requestList = new CLASSTAG_Request[20];
	public int AddRequstList( CLASSTAG_Request request )
	{
		for( int i = 0 ; i < requestList.Length ; i ++ )
		{
			if ( requestList[i] == null )
			{
				requestList[i] = request;
				return i;
			}
		}

		return -1;
	}

	public void ClearRequstList( int index )
	{
		if (index == -1) return;
		if (index < requestList.Length)
		{
			requestList[index] = null;
		}
	}

	void OnApplicationQuit()
	{
		for (int i = 0; i < requestList.Length; i++)
		{
			if (requestList[i] != null)
			{
				requestList[i].isAppQuit = true;
			}
		}
	}
}


