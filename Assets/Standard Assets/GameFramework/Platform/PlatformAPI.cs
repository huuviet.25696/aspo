using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;
using System.IO;
using System.Runtime.InteropServices;

public class PlatformAPI : MonoBehaviour
{
    public static int BATTERY_STATUS_CHARGING = 2; 
    public static int batteryLevelOfAndroid = 100;
    public static bool batteryChargingOfAndroid = false;

    public const string NET_STATE_NONE = "NONE";
    public const string NET_STATE_2G = "2G";
    public const string NET_STATE_3G = "3G/4G";
    public const string NET_STATE_WIFI = "WIFI";


    public static void Setup()
    {
        GameObject go = GameObject.Find("PlatformAPI");
        if (go == null)
        {
            go = new GameObject("PlatformAPI");
            DontDestroyOnLoad(go);
            go.AddComponent<PlatformAPI>();
        }

#if UNITY_ANDROID
        CLASSTAG_AndroidAPI.Setup();
#endif
    }

    public static void Init()
    {
        RegisterPower();
    }

    public static void Release()
    {
        UnregisterPower();
    }


    public static void RestartGame()
    {
#if UNITY_EDITOR
#elif UNITY_ANDROID
        CLASSTAG_AndroidAPI.RestartGame();
#elif UNITY_STANDALONE
        var curApp = System.Diagnostics.Process.GetCurrentProcess();
        System.Diagnostics.Process.Start(curApp.ProcessName + ".exe");
        Application.Quit();
#endif
    }

    public static int GetBatteryLevel()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return 100;
#elif UNITY_ANDROID
		return batteryLevelOfAndroid;
#elif UNITY_IPHONE
		return CLASSTAG_IosAPI.GetBatteryLevel();
#else
		return 100;
#endif
    }

    public static bool IsBattleCharging()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return false;
#elif UNITY_ANDROID
		return batteryChargingOfAndroid;
#elif UNITY_IPHONE
		return CLASSTAG_IosAPI.IsBattleCharging();
#else
		return false;
#endif
    }

    public static void RegisterPower()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return;
#elif UNITY_ANDROID
        CLASSTAG_AndroidAPI.RegisterPower(); 
#else
        return;
#endif
    }

    public static void UnregisterPower()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return;
#elif UNITY_ANDROID
        CLASSTAG_AndroidAPI.UnregisterPower(); 
#else
        return;
#endif
    }

    public static long getFreeMemory()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return 0;
#elif UNITY_ANDROID
        return CLASSTAG_AndroidAPI.getFreeMemory(); 
#elif UNITY_IPHONE
		return (long)CLASSTAG_IosAPI.GetFreeMemory();
#else
        return 0;
#endif
    }

    public static long getTotalMemory()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return 0;
#elif UNITY_ANDROID
        return CLASSTAG_AndroidAPI.getTotalMemory();
#elif UNITY_IPHONE
		return (long)CLASSTAG_IosAPI.GetTotalMemory();
#else
		return 0;
#endif
    }

    public static long getExternalStorageAvailable()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return 0;
#elif UNITY_ANDROID
        return CLASSTAG_AndroidAPI.getExternalStorageAvailable();
#elif UNITY_IPHONE
		long freeDisk = (long)CLASSTAG_IosAPI.GetFreeDiskSpaceInBytes ( );
		return freeDisk >> 10;
#else
		return 0;
#endif
    }

    /**
     * <pre>
     * 获取网络类型
     * 无网络: NONE
     * 未知类型: UNKNOWN
     * WIFI: WIFI
     * 2G: 2G
     * 3G: 3G
     * 4G: 4G
     * </pre>
     * @return 网络类型标识
     */
    public static string getNetworkType()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE || UNITY_IPHONE)
        NetworkReachability ability = Application.internetReachability;

        if (ability == NetworkReachability.NotReachable)
        {
            return NET_STATE_NONE;
        }
        else if (ability == NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            return NET_STATE_WIFI;
        }
        else
        {
            return NET_STATE_3G;
        }
#elif UNITY_ANDROID
        return CLASSTAG_AndroidAPI.getNetworkType();
#else
        return NET_STATE_WIFI;
#endif
    }

    public static int getWifiSignal()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return 100;
#elif UNITY_ANDROID
        return CLASSTAG_AndroidAPI.getWifiSignal();
#elif UNITY_IPHONE
		return 100;
#else
		return 100;
#endif
    }

    /**
     * 获取本机网卡Mac地址
     * @return
     */
    public static string getLocalMacAddress()
    {
#if UNITY_EDITOR || UNITY_STANDALONE || UNITY_IPHONE
        try
        {
            var interfaces = NetworkInterface.GetAllNetworkInterfaces();
            for (int i = 0; i < interfaces.Length; i++)
            {
                var inter = interfaces[i];
                if (!string.IsNullOrEmpty(inter.GetPhysicalAddress().ToString()))
                {
                    var mac = inter.GetPhysicalAddress().ToString();
                    for (int j = mac.Length - 1 - 1; j >= 1; j = j - 2)
                    {
                        mac = mac.Insert(j, "-");
                    }
                    return mac;
                }
            }
        }
        catch (Exception e)
        {
            GameDebug.LogException(e);
        }
#elif UNITY_ANDROID
        //return CLASSTAG_AndroidAPI.getLocalMacAddress();
#endif
        return "00-00-00-00-00";
    }

    /**
     * 取sdcard容量与本机容量, 返回字符串(sdcard容量|手机容量)
     * @return
     */
    public static string getStorageInfos()
    {
        GameDebug.Log("Unity3D getStorageInfos calling.....");
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return "1|1";

#elif UNITY_ANDROID
        return CLASSTAG_AndroidAPI.getStorageInfos();
#elif UNITY_IPHONE
		long freeDisk = (long)CLASSTAG_IosAPI.GetFreeDiskSpaceInBytes ( );
		freeDisk >>= 10;
		return "0|" + freeDisk.ToString( );
#else
		return "1|1";
#endif
    }



    public static string GetAndroidInternalPersistencePath()
    {
#if UNITY_EDITOR
#elif UNITY_ANDROID
        return CLASSTAG_AndroidAPI.GetAndroidInternalPersistencePath();
#endif
        return string.Empty;
    }

  


    public static string GetDeviceUID()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return SystemInfo.deviceUniqueIdentifier;
#elif UNITY_ANDROID
        return CLASSTAG_AndroidAPI.GetDeviceUID();
#elif UNITY_IPHONE
		return SystemInfo.deviceUniqueIdentifier;
#else
		return SystemInfo.deviceUniqueIdentifier;
#endif
    }

    /**
    * 获取当前屏幕亮度
    */
    public static int getScreenBrightness()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return 255;
#elif UNITY_ANDROID
        return CLASSTAG_AndroidAPI.getScreenBrightness();
#elif UNITY_IPHONE
		return CLASSTAG_IosAPI.GetBrightness(); 
#else
		return 255;
#endif
    }


    /**
        * 省电模式,设置亮度
        */
    public static void setBrightness(int brightness)
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return;
#elif UNITY_ANDROID
        CLASSTAG_AndroidAPI.setBrightness(brightness);
#elif UNITY_IPHONE
		CLASSTAG_IosAPI.SetBrightness(brightness);
#else
		return;
#endif
    }

    //设置剪贴板

    public static void SetClipBoardText(string text)
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        GUIUtility.systemCopyBuffer = text;
#elif UNITY_ANDROID
        CLASSTAG_AndroidAPI.SetClipBoardText(text);
#elif UNITY_IPHONE
		CLASSTAG_IosAPI.CopyToClipboard(text);
#else
		return;
#endif
    }


    public static string GetClipBoardText()
    {
#if (UNITY_EDITOR || UNITY_STANDALONE)
        return GUIUtility.systemCopyBuffer;
#elif UNITY_ANDROID
        return CLASSTAG_AndroidAPI.GetClipBoardText();
#else
		return null;
#endif
    }


    public void OnPower(string value)
    {
        int intValue = 0;
        int.TryParse(value, out intValue);
        int status = intValue / 1000;
        int power = intValue % 1000;

        if (status == BATTERY_STATUS_CHARGING)
        {
            PlatformAPI.batteryChargingOfAndroid = true;
        }
        else
        {
            PlatformAPI.batteryChargingOfAndroid = false;
        }

        PlatformAPI.batteryLevelOfAndroid = power;
    }

    public void OnXGRegisterResult(string flag)
    {
        string json = "{'type':'XGRegisterResult','code':0,'data':'" + flag + "'}";
        GameDebug.Log("OnXGRegisterResult json=" + json);
    }
    
    public void OnXGRegisterWithAccountResult(string flag)
    {
        string json = "{'type':'XGRegisterWithAccountResult','code':0,'data':'" + flag + "'}";
        GameDebug.Log("OnXGRegisterWithAccountResult json=" + json);
    }

    public void OnXinGeCallback(string json)
    {
        GameDebug.Log(string.Format("OnXinGeCallback data = {0}", json));
    }

    public void OnSdkCallback(string json)
    {
        //SPSDK.OnSdkCallback(json);
    }

    public void OnInputTextChanged(string text)
    {
        UnityEditDialog.OnInputTextChanged(text);
    }

    public void OnInputReturn()
    {
        UnityEditDialog.OnInputReturn();
    }

    public void OnEditDialogShow()
    {
        UnityEditDialog.OnDialogShow();
    }

    public void OnEditDialogHide()
    {
        UnityEditDialog.OnDialogHide();
    }

    public void OnSoftInputHeight(string height)
    {
        UnityEditDialog.OnSoftInputHeight(height);
    }
}
