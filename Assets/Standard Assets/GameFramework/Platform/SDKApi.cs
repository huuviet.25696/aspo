﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SDKApi : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public static void settup()
    {
        GameObject go = GameObject.Find("GosuSDK");
        if (go == null)
        {
            go = new GameObject("GosuSDK");
            DontDestroyOnLoad(go);
            go.AddComponent<SDKApi>();
            SDK_DeviceUtils = GetAndroidJavaClass(SDK_JAVA);
        }
    }
    private static AndroidJavaClass SDK_DeviceUtils;
    private const string SDK_JAVA = "com.duo.test.UnityPlayerActivity";

    public static AndroidJavaClass GetAndroidJavaClass(string path)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            try
            {
                return new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            }
            catch (Exception e)
            {
                Debug.Log("============================ LOI CALL =====================");
                GameDebug.LogException(e);
            }
        }

        return null;
    }

    public static long GosuLogin()
    {
       // Debug.LogError("cái quần què");
      
        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            //AndroidJavaClass jc = new AndroidJavaClass("great.good.com.rolauncher");
            var curActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
            if (curActivity!=null)
            {
                try
                {
                     curActivity.Call("ShowLoginSDK");
                }
                catch (Exception e)
                {
                    GameDebug.LogException(e);
                }
            }
            else
            {
                GameDebug.LogError("Cannot find javaClass with ShowLoginSDK");
            }
        }
            return 0;
    }

    public static T CallStatic<T>(AndroidJavaClass javaClass, string apiName, params object[] args)
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (javaClass != null)
            {
                try
                {
                    return javaClass.Call<T>(apiName, args);
                }
                catch (Exception e)
                {
                    GameDebug.LogException(e);
                }
            }
            else
            {
                GameDebug.LogError("Cannot find javaClass with " + apiName);
            }
        }

        return default(T);
    }

    public void loginCallback(string msg)
    {
        Debug.LogError("game callback nè: " + msg);
    }

}
