// **********************************************************************
// Copyright (c) 2016 cilugame. All rights reserved.
// File     : ByteArray.cs
// Author   : senkay <senkay@126.com>
// Created  : 1/28/2016 
// Porpuse  : 
// **********************************************************************
//

using System;
using System.Text;
using UnityEngine;

public class CLASSTAG_ByteArray
{
    private byte[] _bytes;

    private bool _unCompressFlag;

    public CLASSTAG_ByteArray(byte[] bytes)
    {
        _bytes = bytes;
    }

    public byte[] bytes
    {
        get { return _bytes; }
    }

    public int Length
    {
        get { return _bytes.Length; }
    }

    public void UnCompress()
    {
        if (_unCompressFlag == false)
        {
            _unCompressFlag = true;
            _bytes = CLASSTAG_ZipLibUtils.Uncompress(_bytes);
        }
    }

    public void Compress()
    {
        _unCompressFlag = false;
        _bytes = CLASSTAG_ZipLibUtils.Compress(_bytes);
    }

    public string ToBase64String()
    {
        return Convert.ToBase64String(_bytes);
    }

    public Texture2D ToTexture2D()
    {
        if (_bytes != null)
        {
            var tex = new Texture2D(0, 0, TextureFormat.ARGB32, false);
            tex.LoadImage(_bytes);
            return tex;
        }

        return null;
    }

    public string FUNCTAG_ToUTF8String()
    {
        return Encoding.UTF8.GetString(_bytes);
    }

    public string ToEncodingString()
    {
        return Encoding.Default.GetString(_bytes);
    }

    #region CreateFrom
    public static CLASSTAG_ByteArray CreateFromUtf8(string str)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(str);
        return new CLASSTAG_ByteArray(bytes);
    }

    public static CLASSTAG_ByteArray CreateFromBase64(string str)
    {
        byte[] bytes = Convert.FromBase64String(str);
        return new CLASSTAG_ByteArray(bytes);
    }

    public static CLASSTAG_ByteArray CreateFromWWW(WWW www)
    {
        if (www != null && string.IsNullOrEmpty(www.error) && www.isDone)
        {
            return new CLASSTAG_ByteArray(www.bytes);
        }

        return null;
    }
    #endregion
}