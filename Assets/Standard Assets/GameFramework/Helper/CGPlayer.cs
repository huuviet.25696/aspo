﻿using System;
using System.Collections;
using UnityEngine;
using LuaInterface;
using UnityEngine.Video;

public class CGPlayer : MonoBehaviour
{
	public static bool skipcg = false;
	public static bool IsCanPlayCG()
	{
		var path = System.IO.Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, "skip_cg");
		if(CLASSTAG_FileHelper.IsExist(path))
		{
			return false;
		}
		return true;
	}
	public static void PlayCG(string cgPath, LuaFunction luaCallback)
	{
		var go = new GameObject("CGPlayer");
		go.AddComponent<AudioSource>();
		var player = go.AddComponent<CGPlayer>();
		player.Setup(cgPath, () =>
		{
			if (luaCallback != null)
			{
				luaCallback.BeginPCall();
				luaCallback.PCall();
				luaCallback.EndPCall();
				luaCallback.Dispose();
			}
		});
		curPlayer = player;
		//SPSDK.startPlayCG(30000);
	}
	static public CGPlayer curPlayer;

	public static GameObject PlayCG(string cgPath, Action onFinish)
    {
		if (IsCanPlayCG())
		{
			var go = new GameObject("CGPlayer");
			go.AddComponent<AudioSource>();
			var player = go.AddComponent<CGPlayer>();
			player.Setup(cgPath, onFinish);
			curPlayer = player;
			//SPSDK.startPlayCG(30000);
			return go;
		}
		else
		{
			if (onFinish != null)
			{
				onFinish();
			}
			return null;
		}
    }

    private string _cgPath;
    private Action _onFinish;
#if UNITY_STANDALONE
    private VideoPlayer _movTexture;
    private bool _playing;
#endif

    private void Setup(string cgPath, Action onFinish)
    {
        _cgPath = cgPath;
        _onFinish = onFinish;
    }

    

    private void Start()
    {
        //TalkingData.FUNCTAG_OnEventStep("PlayCG", "Play");


#if UNITY_STANDALONE
		_movTexture = Resources.Load<VideoPlayer>(_cgPath);
		if (_movTexture == null)
        {
			StopMov();
            return;
        }
		var audioSource = this.gameObject.GetComponent<AudioSource>() as AudioSource;
		//audioSource.clip = _movTexture.audioClip;
		audioSource.Play();
        _playing = true;
        _movTexture.isLooping = false;
        _movTexture.Play();
#else
		StartCoroutine (PlayCGOnMobile ());
#endif
    }

#if UNITY_STANDALONE
    private void Update()
    {
		//if (_playing)
		//{
			//if (Input.GetMouseButtonDown(0))
			//{
				//TalkingData.OnEventStep("PlayCG", "Skip");
				//StopMov();
				//return;
			//}
		//}
        if (_movTexture != null && !_movTexture.isPlaying)
        {
            StopMov();
        }
    }

    private void OnGUI()
    {
        if (_playing)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), _movTexture.texture);
			if (GUI.Button(new Rect(Screen.width - 100, 20, 80, 40), "跳过"))
			{
				StopMov();
			}
        }
    }

#endif



#if !UNITY_STANDALONE
	static public UnityEngine.FullScreenMovieControlMode playMode = UnityEngine.FullScreenMovieControlMode.CancelOnInput;
	static public void SetPlayMode(UnityEngine.FullScreenMovieControlMode mode)
	{
		playMode = mode;
	}
    IEnumerator PlayCGOnMobile ()
	{
		Handheld.PlayFullScreenMovie (_cgPath, Color.black, playMode, FullScreenMovieScalingMode.Fill);
		yield return new WaitForEndOfFrame ();
		yield return new WaitForEndOfFrame ();
		StopMov();
	}
#endif

	public void StopMov()
    {
		Debug.Log("StopMov");
#if UNITY_STANDALONE
        if (_movTexture != null)
        {
            _movTexture.Stop();
        }
        _playing = false;
#endif
        //TalkingData.FUNCTAG_OnEventStep("PlayCG", "Finish");
        Destroy(gameObject);

		if (_onFinish != null)
		{
			_onFinish();
			_onFinish = null;
		}
            
    }
}