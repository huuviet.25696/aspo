//#define LOG_MODULE
//#define LOG_REDPOINT
//#define LOG_Fish
using System;
using System.Timers;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;


public static class CLASSTAG_GameUtil
{
    
    public static void LogFish(string msg){

        #if  LOG_Fish
        GameDebuger.LogError(msg);
        #endif
    }
    public static void LogRedPoint(string msg){

        #if  LOG_REDPOINT
        GameDebuger.LogError(msg);
        #endif
    }

    public static void LOGModule(string msg){
        #if LOG_MODULE
        GameDebuger.LogError(msg);
        #endif
    }
    public static void SafeRun(Action act,Action<Exception> onError = null){
        if (act == null) return;
        try{
            act();
        }catch(Exception e){
            GameDebug.LogException (e);
            if (onError !=null) onError(e);
        }
    }
        
    public static void SafeRun<T>(Action<T> act, T param, Action<Exception> onError = null){
        if (act == null) return;
        try{
            act(param);
        }catch(Exception e){
            GameDebug.LogException(e);
            if (onError !=null) onError(e);
        }
    }


    public static void SafeRun<T, R>(Action<T, R> act, T t, R r){
        if (act != null)
            act(t, r);
    }
    //public static List<T> FiltAndSort<T>(IEnumerable<T> dataSet, 
    //    Predicate<T> predicate
    //    , out int length
    //    , Comparison<T> com = null){
    //    length = 0;

    //    IEnumerable<T> temp = dataSet.Filter(predicate);
    //    if (temp == null)
    //        return null;
    //    else {
    //        List<T> tempSet = temp.ToList();
    //        length = tempSet.Count;
    //        if (com == null) {
    //            return temp.ToList();
    //        }
    //        else{
    //            tempSet.Sort(com);
    //            return tempSet;
    //        }
    //    } 
    //}


}

public static class CLASSTAG_CollectionExtension{
    public static void AddIfNotExist<T> (
        this List<T> dataSet
        , T t){
        if (dataSet == null || t == null) {
            return;
        }

        if (dataSet.IndexOf (t) < 0) {
            dataSet.Add (t);
        }
    }

    public static void ForEach<T>(this IEnumerable<T> dataset,Action<T> act)
    {
        if (dataset == null) return;
        foreach(var item in dataset)
        {
            CLASSTAG_GameUtil.SafeRun<T>(act, item);
        }
    }

    public static void ForEachI<T>(this IEnumerable<T> dataset, Action<T, int> act){
        if (dataset == null) return;
        if (act == null) return;

        int i = 0;
        foreach(var data in dataset)
        {
            CLASSTAG_GameUtil.SafeRun<T, int>(act, data, i);
            i++;
        }
    }

    private static T Find<T>(this IEnumerable<T> dataset, Predicate<T> predicate, out int idx)
    {
        idx = -1;

        if (dataset != null && predicate != null) {
            int i = 0;
            foreach(var item in dataset){
                if (predicate(item)){
                    idx = i;
                    return item;
                }
                ++i;
            }
        }

        return default(T);
    }

    public static int FindElementIdx<T>(this IEnumerable<T> dataset, Predicate<T> predicate)
    {
        int idx = -1;
        dataset.Find (predicate, out idx);
        return idx;
    }

    public static R Find<T, R>(this IEnumerable<T> dataset, Predicate<T> predicate, Func<T, R> action)
    {
        int idx = -1;
        var data = dataset.Find (predicate, out idx);
        return action(data);
    }

    public static T Find<T>(this IEnumerable<T> dataset, Predicate<T> predicate)
    {
        int idx = -1;
        return dataset.Find (predicate, out idx);
    }

    public static List<T> ToList<T>(this IEnumerable<T> dataset)
    {
        List<T> list = new List<T>(); 
        if (dataset != null)
            foreach(T item in dataset)
            {
                list.Add(item);
            }
        return list;
    }

    public static List<T2> GetNewList<T1, T2>(this List<T1> pList, Func<T1, T2> pFilteFunc)
    {
        List<T2> tNewList = new List<T2>();
        for (int i = 0; i < pList.Count; ++i)
        {
            tNewList.Add(pFilteFunc(pList[i]));
        }

        return tNewList;
    }

    public static bool Replace<T>(this List<T> dataset, Predicate<T> predicate, T t)
    {
        bool replaceSuccess = false;
        if (!dataset.IsNullOrEmpty () && predicate != null) {
            int idx = -1;
            var item = dataset.Find (predicate, out idx);
            if (item != null) {
                dataset [idx] = t;
                replaceSuccess = true;
            }
        }
        return replaceSuccess;
    }

    public static void ReplaceOrAdd<T>(this List<T> dataset, Predicate<T> predicate, T t)
    {
        if (t == null) return;
        bool isExist = dataset.Replace (predicate, t);
        if (!isExist)
            dataset.Add (t);
    }


    public static bool RemoveItem<T>(this List<T> dataSet, T t)
    {
        if (null == t || dataSet.IsNullOrEmpty())
            return false;
        return dataSet.Remove(t);
    }

    public static bool Remove<T>(this List<T> dataSet, Predicate<T> predicate){
        bool isChange = false;
        var item = dataSet.Find<T> (predicate);
        if (item != null) {
            dataSet.Remove (item);
            isChange = true;
        }
        return isChange;
    }

    //return old value if exists for key
    public static TValue Replace<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, TValue val){
        if (dict == null) return default(TValue);
        TValue old;
        if (dict.TryGetValue(key,out old)){
            dict.Remove(key);
        }
        dict.Add(key,val);
        return old;
    }

    //return true if key is not present and (key value) pair is added
//    public static bool ReplaceOrAdd<TKey, TValue>(this IDictionary<TKey, TValue> dict, TKey key, TValue val){
//        if (dict == null) return false;
//        bool result;
//        if (result == !dict.ContainsKey(key))
//            dict.Add(key,val);
//        return true;
//    }

    public static TValue CreateIfNotExist<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key, Func<TValue> createFunc)
        where TKey : class
        where TValue : class
    {
        if (dict == null || key == null)
        {
            return null;
        }

        if (!dict.ContainsKey(key))
        {
            if (createFunc != null)
            {
                var value = createFunc();
                dict.Add(key, value);
                return value;
            }
            else
                return null;
        }
        else if (dict[key] == null && createFunc != null)
        {
            var value = createFunc();
            dict[key] = value;
            return value;
        }
        else
            return dict[key];
    }

    public static R ShallowCopyCollection<T,R>(this  ICollection<T> dataset) where R :   ICollection<T>, new(){
        if (dataset == null) return default(R);
        R result = new R();
        foreach(var item in dataset){
            result.Add(item);
        }
        return result;
    }

    public static bool IsNullOrEmpty<T>(this List<T> dataSet)
    {
        return dataSet == null || dataSet.Count <= 0;
    }

    public static bool TryGetLength<T>(this List<T> set, out int length){
        length = 0;
        if (set == null) {
            return false;
        } else {
            length = set.Count;
            return true;
        }
    }

    public static bool TryGetValue<T>(this List<T> set, int index, out T value){
        value = default(T);
        if (!set.IsNullOrEmpty () && index < set.Count) {
            value = set [index];
            return true;
        } else {
            return false;
        }
    } 

    public static bool IsNullOrEmpty(this ArrayList array){
        return array == null || array.Count <= 0;
    }

}

public static class CLASSTAG_EnumParserHelper{
    public static T TryParse<T>(string value) where T : struct{
        T result = default(T);
        try {
            result = (T)Enum.Parse(typeof(T),value,true);
        } catch (Exception ex) {
            GameDebug.LogException(ex);
        }
        return result;
    }
	
    public static T? TryParseOptional<T>(string value) where T : struct{
		T? result = null;
        try {
            result = (T)Enum.Parse(typeof(T),value);
        } catch (Exception ex) {
            GameDebug.LogException(ex);
        }
        return result;
    }
}

public static class CLASSTAG_GameObjectExtension
{
    /// <summary>
    ///     Gets the missing component.
    /// </summary>
    public static T GetMissingComponent<T>(this GameObject go) where T : Component
    {
        var t = go.GetComponent<T>();
        if (t == null)
        {
            t = go.AddComponent<T>();
        }
        return t;
    }

    public static void RemoveChildren(this GameObject go)
    {
        if (go == null)
            return;

        go.transform.DestroyChildren();
    }

}
