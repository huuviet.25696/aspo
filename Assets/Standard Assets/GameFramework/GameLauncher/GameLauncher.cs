using System;
using LITJson;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using AssetPipeline;
using Debug = UnityEngine.Debug;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameLauncher : MonoBehaviour
{
    public bool playIntroMovie = true;
    public bool enableVoiceChat = false;
#if UNITY_EDITOR
    public AssetManager.LoadMode loadMode = AssetManager.LoadMode.EditorLocal;
    public bool testHotUpdate = false;
#endif
    //进游戏之前andriod更新的数据
    public class AndriodUpdateInfo
    {
        public string cndUrls;
        public int testMode = 0;
    }

    [SerializeField]
    private bool _isTest = true;

    public bool isTest
    {
        get
        {
            return _isTest;
        }
    }

    public static string andriodUpdateFileName = "andriodUpdateInfo.json";

    public static AndriodUpdateInfo andriodUpdateInfo
    {
        get;
        private set;
    }
    public static GameLauncher Instance
    {
        get;
        private set;
    }

    private void Awake()
    {
        Instance = this;
    }

    private void OnPlayCGFinish()
    {
        StartCoroutine(ShowLoading());
    }
    private void Start()
    {
        InitGameUpdateSetting();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BuiltInDialogueViewController.FUNCTAG_OpenView("Exit game", ExitGame, () =>
            {
                GameDebug.Log("Cancel ExitGame");
            });
        }
    }


    /// <summary>
    /// 防止意外界面大小不正常
    /// </summary>
    private void ResolutionCheck()
    {
        if (Application.platform == RuntimePlatform.WindowsPlayer)
        {
            //    // 临时定死值
            //    if (Screen.width < 500 || Screen.height < 300)
            //    {
            Screen.SetResolution(1334, 750, false);
            //    }
        }
    }


    /// <summary>
    ///     初始化游戏更新环境
    /// </summary>
    private void InitGameUpdateSetting()
    {
        //#if UNITY_STANDALONE && !UNITY_EDITOR
        //        var limitedNum = 2;
        //        if (GameLauncherMutex.GetMutexTag(GameSetting.GameName) > limitedNum)
        //        {
        //            ShowErroMessage("同时最多只能开启两个游戏客户端");
        //            return;
        //        }
        //#endif

        ResolutionCheck();
        /*if (Application.isMobilePlatform) */

        Screen.sleepTimeout = SleepTimeout.NeverSleep;  //游戏设置为不自动休眠屏幕
        Application.backgroundLoadingPriority = ThreadPriority.High;
        CLASSTAG_GameSetting.FUNCTAG_Setup();
        if (Time.frameCount % 30 == 0)
        {
            System.GC.Collect();
        }
        //GameDebug.Init(CLASSTAG_GameSetting.ShowUpdateLog);
        GameDebug.Init(false);
        PlatformAPI.Setup();
        SDKApi.settup();
        CLASSTAG_HttpController.Instance.FUNCTAG_Setup();
        UploadDataManager.CreateInstance();
        AssetManager.CreateInstance();
        AssetUpdate.CreateInstance();

        AssetUpdate.Instance.SetLogHanlder(ShowTips, ShowError);

        if (playIntroMovie && UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "main")
        {
#if UNITY_STANDALONE
            var player = CGPlayer.PlayCG("Movies/cg", OnPlayCGFinish);
#else
            var player = CGPlayer.PlayCG("Movies/cg.mp4", OnPlayCGFinish);
#endif
            if (player != null)
            {
                player.AddComponent<AudioListener>();
            }
        }
        else
        {
            OnPlayCGFinish();
        }
    }

    public IEnumerator ShowLoading()
    {
        GameLauncherViewController.FUNCTAG_OpenView("");
#if !UNITY_EDITOR && UNITY_ANDROID
        GameLauncherViewController._instance.ShowSplash();
        yield return new WaitForSeconds(1.0f);
#endif
        GameLauncherViewController._instance.ShowLoadingBg();
        ShowTips("Initialize the game");
        yield return null;
        JudgeNetExist();
    }


    private void JudgeNetExist()
    {
        if (Application.isMobilePlatform)
        {
            string curNetType = PlatformAPI.getNetworkType();
            if (curNetType == PlatformAPI.NET_STATE_NONE)
            {
                BuiltInDialogueViewController.FUNCTAG_OpenView("There is currently no network connection, please connect to the network and try again", JudgeNetExist, null, UIWidget.Pivot.Left, "reconnect");
                return;
            }
        }
        LoadAndriodUpdateInfo();
        CheckUpdate();
    }

    private void CheckUpdate()
    {
        bool tmp = true;
#if UNITY_EDITOR
        AssetManager.ResLoadMode = loadMode;
        if (testHotUpdate == false)
        {
            OnLoadCommonAssetFinish();
            return;
        }
        if (AssetManager.ResLoadMode == AssetManager.LoadMode.EditorLocal)
        {
            OnLoadCommonAssetFinish();
            return;
        }
#endif
        AssetUpdate.Instance.StartCheckOutGameRes(needRestart =>
        {
            GameDebug.Log("CheckUpdate result:" + needRestart.ToString());
            if (needRestart)
            {
                RestartGame();
            }
            else
            {
                //update from local resConfig.jz. In editor mode, it is Assets/StreamingAssets/resConfig.jz
                AssetUpdate.Instance.UpdateGameVersion();
                GameLauncherViewController._instance.ShowVersion(CLASSTAG_GameVersion.PackageFullVersion, CLASSTAG_GameVersion.packageSvnVersion, CLASSTAG_GameVersion.FullVersion, CLASSTAG_GameVersion.svnVersion);
                if (CLASSTAG_GameSetting.updateMode == CLASSTAG_GameSetting.UpdateMode.TestUpdate)
                {
                    if (andriodUpdateInfo != null && andriodUpdateInfo.testMode > 0)
                    {

                        //var andriodChooseMode = (UpdateSettingViewController.UpdateMode)andriodUpdateInfo.testMode;
                        //FUNCTAG_SetTestMode(andriodChooseMode);
                        FUNCTAG_SetTestMode(UpdateSettingViewController.UpdateMode.OFFICIAL);
                    }
                    else
                    {
                        //ShowUpdateSettingView();
                        FUNCTAG_SetTestMode(UpdateSettingViewController.UpdateMode.OFFICIAL);
                    }
                }
                else if (CLASSTAG_GameSetting.updateMode == CLASSTAG_GameSetting.UpdateMode.Update)
                {
                    LoadStaticConfig();
                }
                else if (CLASSTAG_GameSetting.updateMode == CLASSTAG_GameSetting.UpdateMode.NoUpdate)
                {
                    AssetUpdate.Instance.FUNCTAG_StartLoadPackageGameConfig(OnUpdateAssetFinish);
                }

            }
        });
        if (tmp) return;
        ////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////
        //AssetManager.ResLoadMode = AssetManager.LoadMode.Assetbundle;
        //     if (AssetManager.ResLoadMode == AssetManager.LoadMode.EditorLocal)
        //     {
        //         OnLoadCommonAssetFinish();
        //     }
        //     else if (AssetManager.ResLoadMode == AssetManager.LoadMode.Assetbundle)
        //     {
        ////FUNCTAG_SetTestMode(UpdateSettingViewController.UpdateMode.SKIP_UPDATE);
        ////return;
        //        AssetUpdate.Instance.StartCheckOutGameRes(needRestart =>
        //        {
        //var localVer = AssetUpdate.Instance.LocalVersionConfig != null ? AssetUpdate.Instance.LocalVersionConfig.ResVersion : "null";
        //UploadDataManager.Instance.StartGameUpload(localVer);
        //            if (needRestart)
        //            {
        //                RestartGame();
        //            }
        //            else
        //            {
        //                AssetUpdate.Instance.UpdateGameVersion();
        //                GameLauncherViewController._instance.ShowVersion(CLASSTAG_GameVersion.PackageFullVersion, CLASSTAG_GameVersion.packageSvnVersion, CLASSTAG_GameVersion.FullVersion, CLASSTAG_GameVersion.svnVersion);
        //                if (CLASSTAG_GameSetting.updateMode == CLASSTAG_GameSetting.UpdateMode.TestUpdate)
        //                {
        //		if (andriodUpdateInfo != null && andriodUpdateInfo.testMode != null)
        //		{

        //			var andriodChooseMode = (UpdateSettingViewController.UpdateMode)andriodUpdateInfo.testMode;
        //			FUNCTAG_SetTestMode(andriodChooseMode);
        //		}
        //		else
        //		{
        //			ShowUpdateSettingView();
        //		}
        //                }
        //                else if (CLASSTAG_GameSetting.updateMode == CLASSTAG_GameSetting.UpdateMode.Update)
        //                {
        //                    LoadStaticConfig();
        //                }
        //                else if (CLASSTAG_GameSetting.updateMode == CLASSTAG_GameSetting.UpdateMode.NoUpdate)
        //                {
        //                    AssetUpdate.Instance.FUNCTAG_StartLoadPackageGameConfig(OnUpdateAssetFinish);
        //                }
        //            }
        //        });
        //    }
    }

    private void LoadAndriodUpdateInfo()
    {
        var path = Application.persistentDataPath + "/" + andriodUpdateFileName;
        if (CLASSTAG_FileHelper.IsExist(path))
        {
            andriodUpdateInfo = CLASSTAG_FileHelper.ReadJsonFile<AndriodUpdateInfo>(path);
        }
        Debug.Log("LoadAndriodUpdateInfo :" + (andriodUpdateInfo != null));
    }

    private static bool isTestVersionConfig = false;
    private static bool isTestHttpRoot = false;


    private void ShowUpdateSettingView()
    {
        UpdateSettingViewController.OpenView((mode) =>
        {
            FUNCTAG_SetTestMode(mode);
        });
    }

    private void FUNCTAG_SetTestMode(UpdateSettingViewController.UpdateMode mode)
    {
        if (mode == UpdateSettingViewController.UpdateMode.DEV_TEST)
        {
            isTestVersionConfig = true;
            isTestHttpRoot = true;
        }
        else if (mode == UpdateSettingViewController.UpdateMode.OFFICIAL_TEST)
        {
            isTestVersionConfig = true;
            isTestHttpRoot = false;
        }
        else if (mode == UpdateSettingViewController.UpdateMode.OFFICIAL)
        {
            isTestVersionConfig = false;
            isTestHttpRoot = false;
        }
        else if (mode == UpdateSettingViewController.UpdateMode.SKIP_UPDATE)
        {
            AssetUpdate.Instance.FUNCTAG_StartLoadPackageGameConfig(OnUpdateAssetFinish);
            return;
        }
        LoadStaticConfig();
    }


    public static void ShowTips(string tips)
    {
        GameLauncherViewController.ShowTips(tips);
    }

    public void DestroyGameLoader()
    {
        gameObject.SetActive(false);
        Destroy(gameObject);
    }


    private void ExitGame()
    {
        Application.Quit();

#if UNITY_EDITOR
        EditorApplication.isPlaying = false;
#endif
    }

    private void ShowError(string msg)
    {
        if (string.IsNullOrEmpty(msg))
        {
            ExitGame();
        }
        else
        {
            BuiltInDialogueViewController.FUNCTAG_OpenView(msg, ExitGame, null, UIWidget.Pivot.Center);
        }
    }


    private void LoadStaticConfig()
    {
        ShowTips("Load game configuration information");
        AssetUpdate.Instance.LoadStaticConfig(isTestHttpRoot,
        () =>
        {
            AssetUpdate.Instance.Setup(CLASSTAG_GameSetting.CDN_SERVER_LIST);
            StartUpdateAsset();

            //         if (AssetUpdate.Instance.CheckBlackListUpdate()) //Update blacklist
            //         {
            //	//var localVer = AssetUpdate.Instance.LocalVersionConfig != null ? AssetUpdate.Instance.LocalVersionConfig.ResVersion : "null";
            //	//var curVer = AssetUpdate.Instance._versionConfig != null ? AssetUpdate.Instance._versionConfig.ResVersion : "null";
            //	//if (localVer != curVer)
            //	//{
            //		//UploadDataManager.Instance.StartUpdateUpload(localVer, curVer);
            //	//}
            //	StartUpdateAsset();
            //}
            //else
            //{
            //		Debug.Log("Update blacklist, skip update");
            //	AssetUpdate.Instance.FUNCTAG_StartLoadPackageGameConfig(OnUpdateAssetFinish);
            //}
        },
        (tips) =>
        {
            BuiltInDialogueViewController.FUNCTAG_OpenView(tips, LoadStaticConfig, ExitGame, UIWidget.Pivot.Left, "Retry", "Exit");
        });
    }

    /// <summary>
    ///     开始更新游戏资源
    /// </summary>
    private void StartUpdateAsset()
    {
        // TalkingData.FUNCTAG_OnEventStep("GameLauncher", "StartUpdateAsset");
        AssetUpdate.Instance.FetchVersionConfig(isTestVersionConfig, () =>
        {
            CheckClientUpdate();
        });
    }

    /// <summary>
    /// 检查客户端整包更新
    /// </summary>
    private void CheckClientUpdate()
    {
        GameDebug.Log("Check the client package update");
        //TalkingData.FUNCTAG_OnEventStep("GameLauncher", "CheckClientUpdate");
        var versionConfig = AssetUpdate.Instance._versionConfig;
        if (versionConfig == null)
        {
            BuiltInDialogueViewController.FUNCTAG_OpenView("Failed to get version information", StartUpdateAsset);
            return;
        }

        GameDebug.Log(string.Format("RemoteFrameworkVersion={0} LocalFrameworkVersion={1}", versionConfig.frameworkVer, CLASSTAG_GameVersion.frameworkVersion));
        //mean must update new version from store
        if (versionConfig.frameworkVer > CLASSTAG_GameVersion.frameworkVersion)
        {
            if (versionConfig.forceUpdate)
            {
                if (string.IsNullOrEmpty(versionConfig.helpUrl))
                {
                    BuiltInDialogueViewController.FUNCTAG_OpenView("The game provides a new version, you need to update to enter the game", CheckClientUpdate);
                }
                else
                {
                    BuiltInDialogueViewController.FUNCTAG_OpenView("The game provides a new version, you need to update to enter the game", () =>
                    {
                        Application.OpenURL(versionConfig.helpUrl);
                        CheckClientUpdate();
                    });
                }
            }
            else
            {
                //igonre update dll files
                BuiltInDialogueViewController.FUNCTAG_OpenView("A new version of the game is available. Will it be updated now?？", () =>
                {
                    Application.OpenURL(versionConfig.helpUrl);
                    CheckClientUpdate();
                }, /*UpdateDll*/UpdateGameRes, UIWidget.Pivot.Left, "Update Now", "Update Next");
            }
        }
        else
        {
            //igonre update dll files
            //UpdateDll();
            UpdateGameRes();
        }
    }


    private void RestartGame()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            BuiltInDialogueViewController.FUNCTAG_OpenView("Check to update, click to confirm and restart in a moment", PlatformAPI.RestartGame);
        }
        else
        {
            PlatformAPI.RestartGame();
        }
    }

    private void UpdateDll()
    {
#if UNITY_EDITOR
        UpdateGameRes();
#else
        if (GameLauncher.andriodUpdateInfo != null)
        {
        	UpdateGameRes();
        }
        else if (AssetUpdate.Instance.ValidateDllVersion(RestartGame))
        {
        	UpdateGameRes();
        }
#endif
    }

    private void UpdateGameRes()
    {
        AssetUpdate.Instance.UpdateGameRes(UpdateScript);
    }

    private void UpdateScript()
    {
        AssetUpdate.Instance.UpdateScript(ValidateScript);
    }

    private void ValidateScript()
    {
        AssetUpdate.Instance.ValidateScript(OnUpdateAssetFinish);
    }

    /// <summary>
    ///     资源更新完成
    /// </summary>
    private void OnUpdateAssetFinish()
    {
        AssetUpdate.Instance.UpdateGameVersion();
        if (Application.isMobilePlatform)
        {
            long freeMemory = PlatformAPI.getFreeMemory() / 1024;
            GameDebug.Log("PhoneFreeMemory : " + freeMemory);

#if UNITY_ANDROID
            if (freeMemory < 150L)
            {
                BuiltInDialogueViewController.FUNCTAG_OpenView("The remaining memory of your phone is low, and the game may crash when running. It is recommended to clear the memory first.",
                    ExitGame, OnUpdateAssetFinish,
                    UIWidget.Pivot.Left, "Exit", "Continue");
                return;
            }
#endif
        }

        //      string path = System.IO.Path.Combine(CLASSTAG_GameResPath.FUNCTAG_persistentDataPath, "firstrun");
        //      if (CLASSTAG_FileHelper.IsExist(path))
        //      {
        //          ShowTips("Loading...");
        //      }
        //      else
        //      {
        //          ShowTips("It may take a long time to load the game for the first time (this process does not consume data)");
        //          CLASSTAG_FileHelper.WriteAllText(path, "");
        //      }

        //var localVer = AssetUpdate.Instance.LocalVersionConfig != null ? AssetUpdate.Instance.LocalVersionConfig.ResVersion : "null";
        //      var curVer = AssetUpdate.Instance._versionConfig != null ? AssetUpdate.Instance._versionConfig.ResVersion : "null";
        //if (localVer != curVer)
        //{
        //	UploadDataManager.Instance.EndUpdateUpload(localVer, curVer);
        //}
        //TalkingData.FUNCTAG_OnEventStep("GameLauncher", "LoadCommonAsset");
        AssetManager.Instance.LoadCommonAsset(OnLoadCommonAssetFinish);
    }

    /// <summary>
    ///     加载公共资源完毕,加载GameRoot,启动AppGameManager
    /// </summary>
    private void OnLoadCommonAssetFinish()
    {
        //DuoSDK
        DuoSDK.CreateInstance();
        //
        //TalkingData.FUNCTAG_OnEventStep("GameLauncher", "OnLoadCommonAssetFinish");
        GameObject prefab = AssetManager.Instance.FUNCTAG_LoadAsset("UI/GameRoot.prefab") as GameObject;
        GameObject gameRoot = Instantiate(prefab);
        gameRoot.name = "GameRoot";
        AssetManager.Instance.UnloadAssetBundle("UI/GameRoot.prefab");
        DontDestroyOnLoad(gameRoot);
        var appGameType = Type.GetType("LuaMain, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null");
        gameRoot.AddComponent(appGameType);
        //SPSDK.stopPlayCG();
    }
}
