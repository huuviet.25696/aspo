using System;
using UnityEngine;
using System.Collections.Generic;

public class AnimEffect : MonoBehaviour, ISerializationCallbackReceiver
{
	public string animName = "";
	[SerializeField]
	private int effectLen = 0;
	public AnimEffectInfo[] infoArray;

	//序列化用数据
	[SerializeField]
	List<GameObject> serialList1;
	[SerializeField]
	List<string> serialList2;

	public int EffectLength
	{
		get
		{
			return effectLen;
		}
		set 
		{
			effectLen = Mathf.Max(0, value);
			var newArray = new AnimEffectInfo[effectLen];
			int start = 0;
			if (infoArray != null && infoArray.Length > 0)
			{
				int len = Mathf.Min(effectLen, infoArray.Length);
				Array.Copy(infoArray, newArray, len);
				start = len;
			}
			for (int i = start; i < effectLen; i++)
			{
				newArray[i] = new AnimEffectInfo(null, "");
			}
			infoArray = newArray;
		}
	}

	public void OnBeforeSerialize()
	{
		if (infoArray == null)
		{
			return;
		}
		serialList1 = new List<GameObject>();
		serialList2 = new List<string>();
		for (int i = 0; i < EffectLength; i++)
		{
			var info = infoArray[i];
			serialList1.Add(info.gameObject);
			serialList2.Add(info.path);
		}
	}

	public void OnAfterDeserialize()
	{
		if (serialList1 == null || serialList2 == null)
		{
			return;
		}
		infoArray = new AnimEffectInfo[EffectLength];
		for (int i = 0; i < EffectLength; i++)
		{
			var info = new AnimEffectInfo(serialList1[i], serialList2[i]);
			infoArray[i] = info;
		}
	}
	void OnGUI()
	{
	}
}
