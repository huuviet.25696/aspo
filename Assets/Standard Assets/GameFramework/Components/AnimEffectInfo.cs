using System;
using UnityEngine;

public class AnimEffectInfo
{
	public GameObject gameObject = null;
	public string path = "";

	public AnimEffectInfo(GameObject go, string path)
	{
		this.gameObject = go;
		this.path = path;
	}
}
