namespace Priority_Queue
{
    public class CLASSTAG_StablePriorityQueueNode : CLASSTAG_FastPriorityQueueNode
    {
        /// <summary>
        /// Represents the order the node was inserted in
        /// </summary>
        public long InsertionIndex { get; internal set; }
    }
}
