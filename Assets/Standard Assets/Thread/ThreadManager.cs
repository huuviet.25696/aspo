using System;
using System.Collections.Generic;
using System.Threading;

public class CLASSTAG_ThreadManager
{
	public const string ThreadManagerTimerTask = "ThreadManagerTimerTask";
	private bool _hasSetTimerTask;

	private static CLASSTAG_ThreadManager _instance;

	public static CLASSTAG_ThreadManager Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new CLASSTAG_ThreadManager();
			}

			return _instance;
		}
	}

	private List<CLASSTAG_ThreadTask> _threadList = new List<CLASSTAG_ThreadTask>();


	public void AddThread(CLASSTAG_ThreadTask task)
	{
		CheckTimerTask();

		_threadList.Add(task);
	}

	private void CheckTimerTask()
	{
		if (!_hasSetTimerTask)
		{
			_hasSetTimerTask = true;
			CSTimer.Instance.SetupTimer(ThreadManagerTimerTask, FUNCTAG_CheckSaveFileList);
		}
	}

	private void FUNCTAG_CheckSaveFileList()
	{
		for (int i = _threadList.Count - 1; i >= 0; i--)
		{
			var task = _threadList[i];
			switch (task.State)
			{
				case CLASSTAG_ThreadTask.ThreadState.None:
					{
						task.Start();
						break;
					}
				case CLASSTAG_ThreadTask.ThreadState.Starting:
					{
						break;
					}
				case CLASSTAG_ThreadTask.ThreadState.Finished:
					{
						task.ActiveFinished();
						break;
					}
				case CLASSTAG_ThreadTask.ThreadState.Dead:
					{
						_threadList.RemoveAt(i);
						break;
					}
			}
		}
	}
}


/// <summary>
/// Thread任务，通过继承来扩展
/// </summary>
public class CLASSTAG_ThreadTask
{
	public enum ThreadState
	{
		None,
		Starting,
		Finished,
		Dead,
	}
	protected ThreadState _state = ThreadState.None;

	public ThreadState State
	{
		get { return _state; }
	}

	protected Action<CLASSTAG_ThreadTask> _targetAction;
	protected Action _finishedAction;

	public CLASSTAG_ThreadTask(Action<CLASSTAG_ThreadTask> targetAction = null, Action finishedAction = null)
	{
		_targetAction = targetAction ?? (task => task.SetFinished()) ;
		_finishedAction = finishedAction;
	}

	public void Start()
	{
		if (_state != ThreadState.None)
		{
			return;
		}
		_state = ThreadState.Starting;

		var thread = new Thread(Starting);
		thread.Start();
	}


	private void Starting()
	{
		if (_targetAction != null)
		{
		    try
		    {
                _targetAction(this);
            }
            catch (Exception ex)
		    {
                GameDebug.LogException(ex);		        
		    }
		}
	}

	public void SetFinished()
	{
		_state = ThreadState.Finished;
	}

	public void ActiveFinished()
	{
		if (_state == ThreadState.Finished)
		{
			_state = ThreadState.Dead;
		    try
		    {
                if (_finishedAction != null)
                    _finishedAction();
            }
		    catch (Exception ex)
		    {
                GameDebug.LogException(ex);
		    }
		}
	}
}