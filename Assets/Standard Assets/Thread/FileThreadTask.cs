using System;
using AssetPipeline;
using UnityEngine;


internal class FileThreadTask : CLASSTAG_ThreadTask
{
    public static void StartTask(string filePath, byte[] fileBytes, bool isCompress, Action finish, Action<Exception> error)
    {
        FileThreadTask task = new FileThreadTask(filePath, fileBytes, isCompress, finish, error);
        CLASSTAG_ThreadManager.Instance.AddThread(task);
    }

    private byte[] fileBytes;
    private string filePath;
    private Exception exception;
    private Action finish;
    private Action<Exception> error;
    private bool isCompress;

    private FileThreadTask(string filePath, byte[] fileBytes, bool isCompress, Action finish, Action<Exception> error)
    {
        this.filePath = filePath;
        this.fileBytes = fileBytes;
        this.isCompress = isCompress;
        this.finish = finish;
        this.error = error;
        _finishedAction = FUNCTAG_FinishTask;
        _targetAction = RunTask;
    }

    private void RunTask(CLASSTAG_ThreadTask task)
    {
        try
        {
            byte[] bytes = isCompress ? CLASSTAG_ZipLibUtils.CompressThread(fileBytes) : fileBytes;
            CLASSTAG_FileHelper.WriteAllBytes(filePath, bytes);
            fileBytes = null;
            filePath = null;
            SetFinished();
        }
        catch (Exception ex)
        {
            exception = ex;
        }
        
    }
    private void FUNCTAG_FinishTask()
    {
        if (exception == null)
        {
            if (finish != null)
                finish();
        }
        else
        {
            if (error != null)
                error(exception);
            else
                GameDebug.LogException(exception);
        }
    }
}
