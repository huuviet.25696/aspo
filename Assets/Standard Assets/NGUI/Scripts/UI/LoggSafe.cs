﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LoggSafe : MonoBehaviour {


    [SerializeField]
    SAFE_ENUM mySafe;

    public SAFE_ENUM getSafe
    {
        get
        {
            return mySafe;
        }
    }

    private void Awake()
    {
        Debug.Log(Screen.safeArea);
        UIAnchor anchor = gameObject.GetComponent<UIAnchor>();
        if (anchor != null)
        {
            SetAnchorOffset(anchor);
            return;
        }

        // UIRectを使っている場合
        UIRect rect = gameObject.GetComponent<UIRect>();
        if (rect != null)
        {
            SetAnchorAbsolute(rect);
            return;
        }
    }
    // Use this for initialization
    void SetAnchorOffset(UIAnchor anchor)
    {
        Rect edge = Screen.safeArea;

        UIAnchor.Side side = anchor.side;
        Vector2 offset = anchor.pixelOffset;

        Debug.Log(side);
        switch (side)
        {
            //case UIAnchor.Side.Bottom:
            //case UIAnchor.Side.BottomLeft:
            //case UIAnchor.Side.BottomRight:
            //    if (!m_IsIgnoreBottom)
            //    {
            //        anchor.pixelOffset.Set(offset.x, offset.y + edge.bottom);
            //    }
            //    break;
            //case UIAnchor.Side.Top:
            //case UIAnchor.Side.TopLeft:
            //case UIAnchor.Side.TopRight:
            //    if (!m_IsIgnoreTop)
            //    {
            //        anchor.pixelOffset.Set(offset.x, offset.y - edge.top);
            //    }
            //    break;
        }
    }

    /// <summary>
    /// anchorのabsoluteを設定する
    /// </summary>
    /// <param name="rect">Rect.</param>
    void SetAnchorAbsolute(UIRect rect)
    {
        Rect edge = Screen.safeArea;
        Debug.Log(edge);
        // top基準:1 center基準:0.5 bottom基準:0
        float topRelative = rect.topAnchor.relative;
        float bottomRelative = rect.bottomAnchor.relative;

        float topMovingValue = 0f;
        float bottomMovingValue = 0f;

        if (topRelative == 1f && bottomRelative == 1f)
        {
            topMovingValue -= edge.yMin;
            bottomMovingValue -= edge.yMin;
        }
        else if (topRelative == 0f && bottomRelative == 0f)
        {
            topMovingValue += edge.yMax;
            bottomMovingValue += edge.yMax;
        }
        else if (topRelative == 1f && bottomRelative == 0f)
        {
            topMovingValue -= edge.yMax;
            bottomMovingValue += edge.yMax;
        }

        switch (mySafe)
        {

            case SAFE_ENUM.TOP:
                rect.topAnchor.absolute -= (int)edge.yMin / 2;
                rect.bottomAnchor.absolute -= (int)edge.yMin / 2;
                break;
            case SAFE_ENUM.BOTTOM:
                rect.bottomAnchor.absolute += (int)edge.yMin / 2;
                rect.topAnchor.absolute += (int)edge.yMin / 2;
                break;
            case SAFE_ENUM.LEFT:
                rect.leftAnchor.absolute += (int)edge.xMin / 2;
                rect.rightAnchor.absolute += (int)edge.xMin / 2;
                break;
            case SAFE_ENUM.RIGHT:
                rect.leftAnchor.absolute -= (int)edge.xMin / 2;
                rect.rightAnchor.absolute -= (int)edge.xMin / 2;
                break;
        }

        //if (!m_IsIgnoreTop)
        //{
        //    rect.topAnchor.absolute += (int)topMovingValue;
        //}
        //if (!m_IsIgnoreBottom)
        //{
        //    rect.bottomAnchor.absolute += (int)bottomMovingValue;
        //}
    }
}


public enum SAFE_ENUM
{
    NONE,
    TOP,
    BOTTOM,
    RIGHT,
    LEFT
}