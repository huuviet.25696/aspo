using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UITextTypeWriter : MonoBehaviour
{
    Text txt;
    RectTransform m_RectTransform;
    string story;
    public bool Play = false;
    public float Delay;

    void Start()
    {
        //Fetch the Text and RectTransform components from the GameObject
        //txt = GetComponent<Text>();
        //m_RectTransform = GetComponent<RectTransform>();
    }

    void Awake()
    {
        txt = GetComponent<Text>();
        story = txt.text;
        txt.text = "";
        m_RectTransform = GetComponent<RectTransform>();
        //if (PlayOnAwake == true)
        ChangeText(story, Delay);
    }

    public void changeFontSize(int size)
    {
        //Change the Font Size to 16
        txt.fontSize = size;

        //Change the RectTransform size to allow larger fonts and sentences
        //m_RectTransform.sizeDelta = new Vector2(850, 400);
        //m_RectTransform.sizeDelta.x = txt.fontSize * 10;
    }
    public void changeRectTransform(int w,int h)
    {
        //Change the Font Size to 16
        //txt.fontSize = size;

        //Change the RectTransform size to allow larger fonts and sentences
        m_RectTransform.sizeDelta = new Vector2(w, h);
        //m_RectTransform.sizeDelta.x = txt.fontSize * 10;
    }
    public void runText()
    {
        GameDebug.LogError(txt.text);
        ChangeText(story);
    }

    //Update text and start typewriter effect
    public void ChangeText(string _text, float _delay = 0f)
    {
        StopCoroutine(PlayText()); //stop Coroutime if exist
        story = _text;
        txt.text = ""; //clean text
        Invoke("Start_PlayText", _delay); //Invoke effect
    }

    void Start_PlayText()
    {
        StartCoroutine(PlayText());
    }

    IEnumerator PlayText()
    {
        if (Play == false)
        {
            yield return new WaitForSeconds(0.00005f);
        }
        foreach (char c in story)
        {
            if (c == '|')
            {
                yield return new WaitForSeconds(1f);
                txt.text = "";
                continue;
            }
            txt.text += c;
            yield return new WaitForSeconds(0.00005f);
        }
    }
}
