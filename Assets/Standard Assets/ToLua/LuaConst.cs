using UnityEngine;

public static class LuaConst
{
    public static string luaDir = Application.dataPath + "/Lua_en";                //lua逻辑代码目录
    public static string toluaDir = Application.dataPath + "/Lua_en";        //tolua lua文件目录
    public static string pbDir = Application.dataPath + "/Lua_en";                //proto目录
    public static string lastIndex = "Lua_en";                //proto目录

#if UNITY_STANDALONE_WIN
    public static string osDir = "Win";
#elif UNITY_STANDALONE_OSX
    public static string osDir = "MacOS";
#elif UNITY_ANDROID
    public static string osDir = "Android";            
#elif UNITY_IPHONE
    public static string osDir = "iOS";        
#else
    public static string osDir = "";        
#endif

    public static string luaResDir = string.Format("{0}/{1}/Lua_en", Application.persistentDataPath, osDir);      //手机运行时lua文件下载目录    

#if UNITY_EDITOR_WIN || NITY_STANDALONE_WIN
    public static string zbsDir = "D:/ZeroBraneStudio/lualibs/mobdebug";        //ZeroBraneStudio目录       
#elif UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
    public static string zbsDir = "/Applications/ZeroBraneStudio.app/Contents/ZeroBraneStudio/lualibs/mobdebug";
#else
    public static string zbsDir = luaResDir + "/mobdebug/";
#endif    

    public static bool openLuaSocket = true;            //是否打开Lua Socket库
    public static bool openZbsDebugger = false;         //是否连接ZeroBraneStudio调试
    public static bool openLuaDebugger = false;         //是否连接lua调试器
}